﻿using _123SendMailV2.Utitlies;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class VerifyController : BaseLangController
    {
        // GET: Verify
        string JsPara = ConfigurationManager.AppSettings["JsPara"];

        public ActionResult Index(string id)
        {
            string data = Utitlies.Utility.Decrypt(id, true);
            string[] arr = Regex.Split(data, "&");
            long UserId =Convert.ToInt64( arr[1]);
            ViewBag.ItemId = arr[0];
            ViewBag.JsPara = JsPara;
            return View();
        }

      
    }
}