﻿using _123SendMailV2.Utitlies;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class EmailEditorController : BaseController
    {
        string JsPara = ConfigurationManager.AppSettings["JsPara"];

        // GET: EmailEditor
        public ActionResult Index(int?Id,  string Type)
        {
            ViewBag.Id = Id;
            ViewBag.JsPara = JsPara;
            ViewBag.Type = Type;
            ViewBag.UserId = SesstionManager.UserInfo.UserId;
            return View();
        }
    }
}