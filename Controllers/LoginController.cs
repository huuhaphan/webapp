﻿using _123SendMailV2.Utitlies;
using _123SendMailV2.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class LoginController: BaseLangController
    {
        // GET: Login
        public ActionResult Index(int? id)
        {
            
            ViewBag.Id = id;
            if (SesstionManager.access_token_auto == null || SesstionManager.access_token_auto == "")
            {
                LoginSupport();
            }

            string ReturnUrl = string.Empty;
            if (Request.QueryString["returnUrl"] != null)
                ReturnUrl = Request.QueryString["returnUrl"];
            else ReturnUrl = "/Contacts";
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        public ActionResult LoginOnPage(int? id)
        {
            ViewBag.Id = id;
            return View();
        }

        public void Login(SessionUser user)
        {
            

            SesstionManager.UserInfo = user;
            ListPermission(user.FunctionList, user.ParentId);

            if (user.ExpireDate==null)
            {
                SesstionManager.UserInfo.ExpireDate = user.CreateDate.AddDays(Utility.DayTrial);
            }
            SesstionManager.access_token = user.Access_Token;

        }

        public void UpdateSession(SessionUser user)
        {
            SesstionManager.UserInfo.Access_Token = user.Access_Token;
            SesstionManager.UserInfo.Avatar = user.Avatar;
            SesstionManager.UserInfo.City = user.City;
            SesstionManager.UserInfo.Company = user.Company;
            SesstionManager.UserInfo.Company = user.Country;
            SesstionManager.UserInfo.CreateDate = user.CreateDate;
            SesstionManager.UserInfo.CurrentBalance = user.CurrentBalance;
            SesstionManager.UserInfo.Email = user.Email;
            SesstionManager.UserInfo.EnableSMS = user.EnableSMS;
            SesstionManager.UserInfo.Facebook = user.Facebook;
            SesstionManager.UserInfo.FirstName = user.FirstName;
            SesstionManager.UserInfo.FunctionList = user.FunctionList;
            SesstionManager.UserInfo.LastName = user.LastName;
            SesstionManager.UserInfo.Mobile = user.Mobile;
            SesstionManager.UserInfo.MonthlyPlanName = user.MonthlyPlanName;
            SesstionManager.UserInfo.ParentId = user.ParentId;
            SesstionManager.UserInfo.Quota = user.Quota;
            SesstionManager.UserInfo.Street = user.Street;
            SesstionManager.UserInfo.Twitter = user.Twitter;
            SesstionManager.UserInfo.UserId = user.UserId;
            ListPermission(user.FunctionList, user.ParentId);


        }

        public ActionResult LoginRequired()
        {
           
            return View();
        }

        public ActionResult SignOut()
        {
             Session.Remove("access_token");
             Session.Remove("UserInfo");
             Session.Remove("SessionPermisson");
             
            return RedirectToAction("Index", "Login");
        
        }

        public ActionResult Active(string Email)
        {
           
            ViewBag.Email = Email;
            return View();

        }

        public ActionResult Success()
        {
           
            return View();

        }

        public ActionResult Lock()
        {
           
            Session.Remove("access_token");
            return View();
        }

       

        /// <summary>
        /// change language
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        public ViewModels.SessionUser ChangeCurrentCulture(string id)
        {

            SessionUser result = new SessionUser();
            // Change the current culture for this user.
            //
            SesstionManager.CurrentCulture = id;
            //
            // Cache the new current culture into the user HTTP session. 
            //
            Session["CurrentCulture"] = id;
            //
            // Redirect to the same page from where the request was made! 
            //
            return result;
        }

        public static void LoginSupport()
        {
            string password= System.Configuration.ConfigurationManager.AppSettings["SupportPass"];
            string username= System.Configuration.ConfigurationManager.AppSettings["SupportUsr"];
            string grant_type = "password";
            var client = new HttpClient();
            string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
            client.BaseAddress = new Uri(domain);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            //var values = new Dictionary<string, string>();
            //values.Add("username", username);
            //values.Add("password", password);
            //values.Add("grant_type", grant_type);

            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>("username", username));
            values.Add(new KeyValuePair<string, string>("password", password));
            values.Add(new KeyValuePair<string, string>("grant_type", grant_type));

            var content = new FormUrlEncodedContent(values);
            HttpResponseMessage response = client.PostAsync("api/authtoken",content).Result;
            if (response.IsSuccessStatusCode)
            {

                var result = response.Content.ReadAsStringAsync().Result;
                DAL.JsonToken deserialized = JsonConvert.DeserializeObject<DAL.JsonToken>(result);
                SesstionManager.access_token_auto = deserialized.access_token;
                
            }
            
        }

        public static void ListPermission(List<Function> FunctionLists, int? ParentId)
        {
            Permission permission = new Permission();
            if (ParentId == null)
            {
                permission.IsParent = true;
            }else
            {
                permission.IsParent = false;
            }
            foreach (Function fc in FunctionLists)
            {
                switch(fc.FunctionId)
                {
                    case 1:
                        permission.Contacts = fc.AccessLevel;
                        break;
                    case 2:
                        permission.ContactActivity = fc.AccessLevel;
                        break;
                    case 3:
                        permission.Suppression = fc.AccessLevel;
                        break;
                    case 4:
                        permission.SMSCampaign = fc.AccessLevel;
                        break;
                    case 5:
                        permission.RocketCampaign = fc.AccessLevel;
                        break;
                    case 6:
                        permission.AutoCampaign = fc.AccessLevel;
                        break;
                    case 7:
                        permission.Statistic = fc.AccessLevel;
                        break;
                    case 8:
                        permission.Template = fc.AccessLevel;

                        break;
                    case 9:
                        permission.Exports = fc.AccessLevel;
                        break;
                    case 10:
                        permission.Account = fc.AccessLevel;
                        break;
                    case 11:
                        permission.ManageGroup = fc.AccessLevel;
                        break;
                    case 12:
                        permission.ManageContact = fc.AccessLevel;
                        break;
                    case 13:
                        permission.ManageForm = fc.AccessLevel;
                        break;
                    case 14:
                        permission.ManageCustomField = fc.AccessLevel;
                        break;
                    case 15:
                        permission.SettingAccount = fc.AccessLevel;
                        break;
                    case 16:
                        permission.SubAccount = fc.AccessLevel;
                        break;
                    case 17:
                        permission.FromMail = fc.AccessLevel;
                        break;
                    case 18:
                        permission.StatisticMoney = fc.AccessLevel;
                        break;
                    case 19:
                        permission.FromMobile = fc.AccessLevel;
                        break;
                    case 20:
                        permission.SpeicalDay = fc.AccessLevel;
                        break;
                    case 21:
                        permission.SMSUnSendLogs = fc.AccessLevel;
                        break;




                }
            }
            SesstionManager.SessionPermisson = permission;
        }
    }
}