﻿using _123SendMailV2.Utitlies;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class UpdateInfoController : BaseLangController
    {
        string JsPara = ConfigurationManager.AppSettings["JsPara"];
        // GET: UpdateInfo
        public ActionResult Index()
        {
            try
            {
                //string id = Request.QueryString[0];
                string id = Request.RawUrl.Replace(Request.Path+"?", "");
                string decrypt = Utility.Decrypt(id, true);
                string[] data = decrypt.Split('&');
                ViewBag.UserId = data[1];
                ViewBag.ParentId = data[0];
                ViewBag.Email = data[2];
                bool IsAccept = true;
                DateTime date = DateTime.ParseExact(data[3], "yyyy-MM-dd", CultureInfo.InvariantCulture);
                if (date.Date < DateTime.Now.Date)
                {
                    IsAccept = false;
                }
                ViewBag.IsAccept = IsAccept;
            }
            catch
            {
                ViewBag.IsAccept = false;
            }
           
            ViewBag.JsPara = JsPara;
            return View();
        }
    }
}