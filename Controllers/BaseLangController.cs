﻿using _123SendMailV2.Utitlies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class BaseLangController : Controller
    {

        protected override void ExecuteCore()
        {
            string culture = "vi-VN";
            if (this.Session["CurrentCulture"] == null)
            {
                culture= System.Configuration.ConfigurationManager.AppSettings["Culture"].ToString();
                this.Session["CurrentCulture"] = culture;
            }
            else
            {
                culture =this.Session["CurrentCulture"].ToString();
            }
            //
            SesstionManager.CurrentCulture = culture;
            //
            // Invokes the action in the current controller context.
            //
            base.ExecuteCore();
        }

        protected override bool DisableAsyncSupport
        {
            get { return true; }
        }
    }

}