﻿using _123SendMailV2.Utitlies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class ExternalViewController : Controller
    {
        // GET: ExternalView
        public ActionResult EmailInfo(string Id)
        {
            try
            {
                ViewBag.SentId = Utility.Decrypt(Id, true);
            }
            catch
            {
                ViewBag.SentId = "";
            }
            return View();
        }
        public ActionResult NewsletterInfo(string Id)
        {
            try
            {
                ViewBag.NewsletterId = Utility.Decrypt(Id, true);
            }
            catch
            {
                ViewBag.SentId = "";
            }
            return View();
        }
    }
}