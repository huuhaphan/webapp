﻿using _123SendMailV2.Utitlies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace _123SendMailV2.Controllers
{
    //public class BasePayLangController : AsyncController
    //{
    //    protected override void ExecuteCore()
    //    {
    //        int culture = 0;
    //        if (this.Session == null || this.Session["CurrentCulture"] == null)
    //        {
    //            int.TryParse(System.Configuration.ConfigurationManager.AppSettings["Culture"], out culture);
    //            this.Session["CurrentCulture"] = culture;
    //        }
    //        else
    //        {
    //            culture = (int)this.Session["CurrentCulture"];
    //        }
    //        //
    //        SesstionManager.CurrentCulture = culture;
    //        //
    //        // Invokes the action in the current controller context.
    //        //
    //        base.ExecuteCore();
    //    }

    //}

    public abstract class BasePayLangController : Controller
    {
        
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string culture = "vi-VN";
            if (this.Session == null || this.Session["CurrentCulture"] == null)
            {
                culture= System.Configuration.ConfigurationManager.AppSettings["Culture"].ToString();
                this.Session["CurrentCulture"] = culture;
            }
            else
            {
                culture = this.Session["CurrentCulture"].ToString();
            }
            //
            SesstionManager.CurrentCulture = culture;
            //
            // Invokes the action in the current controller context.
            //

            return base.BeginExecuteCore(callback, state);
        }
    }
}