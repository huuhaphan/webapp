﻿using _123SendMailV2.DAL;
using _123SendMailV2.Utitlies;
using _123SendMailV2.ViewModels;
using API_NganLuong;
using com.paypal.sdk.util;
using Newtonsoft.Json;
using PaypalCheckout;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class PaymentMobileController : Controller
    {

        public ActionResult Index(string a,string n,string e, string m)
        {
            ViewBag.Token = a;
            ViewBag.UserName = n;
            ViewBag.Email = e;
            ViewBag.Mobile = m;
            SessionUser ssuser = new SessionUser()
            {
                Email = e,
                Mobile = m,
                UserName = n,
            };
            SesstionManager.access_payment = a;
            SesstionManager.UserInfoPayment = ssuser;
            return View();
        }

        public ActionResult BankTransferBox(string code, DateTime? date, long? amount)
        {
            ViewBag.PaymentCode = code;
            ViewBag.Date = date;
            ViewBag.Amount = amount;
            return PartialView();
        }
        public ActionResult Cancel(string m)
        {
            ViewBag.Method = m;
            return View();
        }

        public ActionResult PaySuccess()
        {

            int LeastMoney = 0;
            var pay = false;

            if (SesstionManager.UserInfo.ExpireDate.Value.Date <= DateTime.Now.Date)
            {
                UpdateSessionUser();
                LeastMoney = Convert.ToInt32(ConfigurationManager.AppSettings["LastLimitRecharge"]);

                if (Convert.ToInt32(SesstionManager.UserInfo.CurrentBalance) >= LeastMoney)
                {
                    PayMaintenanceFee();
                    pay = true;
                }
            }
            if (SesstionManager.UserInfo.ExpireDate.Value.Date == SesstionManager.UserInfo.CreateDate.AddDays(7).Date)
            {
                LeastMoney = Convert.ToInt32(ConfigurationManager.AppSettings["FirstLimitRecharge"]);
                UpdateSessionUser();
                if (Convert.ToInt32(SesstionManager.UserInfo.CurrentBalance) >= LeastMoney)
                {
                    PayMaintenanceFee();
                    pay = true;
                }
            }
            ViewBag.PayMaintenanceFee = pay;
            ViewBag.LeastMoney = String.Format("{0:#,#.##}", LeastMoney);


            return View();
        }

        [HttpPost]
        public System.Web.Mvc.JsonResult NLProcess(Payment Payment)
        {
            string payment_method = Payment.Menthod;
            string str_bankcode = Payment.Bank;
            SessionUser ssuser = new SessionUser()
            {
                Email = Payment.Email,
                Mobile = Payment.Mobile,
                UserName = Payment.UserName,
            };
            SesstionManager.access_payment = Payment.AccessToken;
            SesstionManager.UserInfoPayment = ssuser;

            RequestInfo info = new RequestInfo();
            info.Merchant_id = System.Configuration.ConfigurationManager.AppSettings["NLMerchantSiteCode"];
            info.Merchant_password = System.Configuration.ConfigurationManager.AppSettings["NLSecurePass"];
            info.Receiver_email = System.Configuration.ConfigurationManager.AppSettings["NLReceiver"];
            info.cur_code = "vnd";
            info.bank_code = str_bankcode;

            info.Order_code = Payment.PayCode; //lấy từ api
            info.Total_amount = Payment.Amount;
            info.fee_shipping = "0";
            info.Discount_amount = "0";
            info.order_description = "Mua gói cước/Nạp tiền"; //truyền xuống luôn

            info.Payment_type = "1";//giao dịch ngay

            info.return_url = ConfigurationManager.AppSettings["NLSuccessUrl"];
            info.cancel_url = ConfigurationManager.AppSettings["NLCancelUrl"];

            if (Payment.Mobile == "")
            {
                info.Buyer_mobile = "0937010861";
            }
            else
            {
                info.Buyer_mobile = Payment.Mobile;
            }

            info.Buyer_fullname = Payment.UserName;
            info.Buyer_email = Payment.Email;

            APICheckoutV3 objNLChecout = new APICheckoutV3();
            ResponseInfo result = objNLChecout.GetUrlCheckout(info, payment_method);
            ResultPayment rp = new ResultPayment();
            if (result.Error_code == "00")
            {

                rp.Success = true;
                rp.Result = result.Checkout_url;
                return Json(rp, JsonRequestBehavior.AllowGet);
            }
            else
            {
                rp.Success = false;
                rp.Result = result.Description;
                return Json(rp, JsonRequestBehavior.AllowGet);
            }
            

        }

        public ActionResult NLResult()
        {
            string Token = Request["token"];
            RequestCheckOrder info = new RequestCheckOrder();
            info.Merchant_id = System.Configuration.ConfigurationManager.AppSettings["NLMerchantSiteCode"];
            info.Merchant_password = System.Configuration.ConfigurationManager.AppSettings["NLSecurePass"];
            info.Token = Token;
            APICheckoutV3 objNLChecout = new APICheckoutV3();
            ResponseCheckOrder result = objNLChecout.GetTransactionDetail(info);
            ViewBag.Error = result.errorCode;
            ReturnResultPayment m = new ReturnResultPayment();
            if (result.errorCode.Equals("00"))
            {
                string Status = result.transactionStatus;// 00 đã thanh toan, 01 đã thanh toán chờ xử lý, 02 chưa thanh toán
                ViewBag.Status = Status;

                switch (Status)
                {
                    case "00":
                    case "01":
                        string PayCode = result.order_code;
                        string TrackingId = result.transactionId;
                        string Note = result.description;

                        DAL.JsonResult jsonresult = DALPayment.UpdateData(PayCode, TrackingId, Note, SesstionManager.access_payment);
                        switch (jsonresult.ErrorCode)
                        {
                            case "104":
                                m.ErrorCode = 104;
                                m.ErrorMessage = "Thanh toán thành công, tuy nhiên đã có lỗi xảy ra khi cập nhật hệ thống. Vui lòng liên hệ để được hỗ trợ.";
                                break;
                            case "0":
                                m.ErrorCode = 0;
                                m.ErrorMessage = "Bạn đã thanh toán thành công.";
                                break;
                        }


                        break;
                    case "02":
                        {
                            m.ErrorCode = 0;
                            m.ErrorMessage = "Thanh toán đang được Review bên cổng thanh toán. Khi Review thành công thì hệ thống sẽ tự động cập nhật!";
                            break;
                        }


                }
            }
            else
            {
                m.ErrorCode = 102;
                switch (result.errorCode)
                {
                    case "00":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode00;
                        m.ErrorCode = 0;
                        break;
                    case "99":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode99;
                        break;
                    case "03":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode03;
                        break;
                    case "04":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode04;
                        break;
                    case "05":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode05;
                        break;
                    case "06":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode06;
                        break;
                    case "07":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode07;
                        break;
                    case "08":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode08;
                        break;
                    case "09":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode09;
                        break;
                    case "10":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode10;
                        break;
                    case "11":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode11;
                        break;
                    case "12":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode12;
                        break;
                    case "29":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode29;
                        break;
                    case "81":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode81;
                        break;
                    case "110":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode110;
                        break;
                    case "111":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode111;
                        break;
                    case "113":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode113;
                        break;
                    case "114":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode114;
                        break;
                    case "115":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode115;
                        break;
                    case "118":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode118;
                        break;
                    case "119":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode119;
                        break;
                    case "120":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode120;
                        break;
                    case "121":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode121;
                        break;
                    case "122":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode122;
                        break;
                    case "123":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode123;
                        break;
                    case "124":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode124;
                        break;
                    case "125":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode125;
                        break;
                    case "126":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode126;
                        break;
                    case "127":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode127;
                        break;
                    case "128":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode128;
                        break;
                    case "129":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode129;
                        break;
                    case "130":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode130;
                        break;
                    case "131":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode131;
                        break;
                    case "132":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode132;
                        break;
                    case "133":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode133;
                        break;
                    case "134":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode134;
                        break;
                    case "135":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode135;
                        break;
                    case "140":
                        m.ErrorMessage = Resources.PaymentResource.ErrorCode140;
                        break;
                }
            }
            return View(m);
        }

        [HttpPost]
        public System.Web.Mvc.JsonResult PPProcess(Payment payment)
        {
            SessionUser ssuser = new SessionUser()
            {
                Email = payment.Email,
                Mobile = payment.Mobile,
                UserName = payment.UserName,
            };
            SesstionManager.access_payment = payment.AccessToken;
            SesstionManager.UserInfoPayment = ssuser;
           

            string API_UserName = Paypal_Checkout.UserName;
            string API_Password = Paypal_Checkout.Password;
            string API_Signature = Paypal_Checkout.MerchantId;
            string ReturnUrl = Paypal_Checkout.SuccessUrl;
            string CancelUrl = Paypal_Checkout.CancelUrl;
            ResultPayment rp = new ResultPayment();

            NVPCodec Result = new NVPCodec();
            string TOKEN = Paypal_Checkout.createPayPalTOKEN(API_UserName, API_Password, API_Signature, Paypal_Checkout.PayPalEnviroment, ReturnUrl, CancelUrl, payment, out Result);

            if (TOKEN != "")
            {
                rp.Success = true;
                rp.Result = Paypal_Checkout.buildCheckoutUrl(Paypal_Checkout.PayPalEnviroment, TOKEN);
            }
            else
            {
                rp.Success = false;
                rp.Result = "Lỗi Paypal: " + Result.ToString();
            }

            return Json(rp, JsonRequestBehavior.AllowGet);

        }

        public ActionResult PPResult()
        {
            ReturnResultPayment m = new ReturnResultPayment();
            string TOKEN = Request.QueryString["TOKEN"];
            string PayerID, Total, TrackingID, OrderCode, Currency = "", PaymentType = "";
            Currency = "USD";
            PaymentType = "Sale";
            string API_UserName = Paypal_Checkout.UserName;
            string API_Password = Paypal_Checkout.Password;
            string API_Signature = Paypal_Checkout.MerchantId;
            NVPCodec decoder = new NVPCodec();
            bool Result = true;
            Result = Paypal_Checkout.GetExpressCheckoutDetail(API_UserName, API_Password, API_Signature, Paypal_Checkout.PayPalEnviroment, TOKEN, out PayerID, out Total, out TrackingID, out OrderCode, out decoder);

            if (Result)
            {
                string Error = "";
                //success
                if (Paypal_Checkout.DoExpressCheckout(API_UserName, API_Password, API_Signature, Paypal_Checkout.PayPalEnviroment, TOKEN, PayerID, Total, PaymentType, Currency, out Error))
                {

                    string Note = "Nạp tiền vào tài khoản";
                    DAL.JsonResult jsonresult = new DAL.JsonResult();
                    jsonresult = DALPayment.UpdateData(OrderCode, TrackingID, Note, SesstionManager.access_payment);
                    switch (jsonresult.ErrorCode)
                    {

                        case "0":
                            m.ErrorCode = 0;
                            m.ErrorMessage = "Bạn đã nạp tiền thành công";
                            break;
                        case "14":
                            m.ErrorCode = 100;
                            m.ErrorMessage = "Đơn hàng đã nạp thành công trước đó";
                            break;
                        default:
                            m.ErrorCode = 100;
                            m.ErrorMessage = "Thanh toán thành công, tuy nhiên đã có lỗi xảy ra khi cập nhật hệ thống. Vui lòng liên hệ để được hỗ trợ.";
                            break;
                    }
                }
                else
                {
                    m.ErrorCode = 101;
                    m.ErrorMessage = "Kết quả thanh toán không hợp lệ.";
                }
            }
            else
            {
                m.ErrorCode = 101;
                m.ErrorMessage = "Kết quả thanh toán không hợp lệ.";
            }
            return View(m);
        }

        public  System.Web.Mvc.JsonResult UpdateRechargeByCode(Payment payment)
        {

            Task.Run(async()=>await DALPayment.AddListPaid());
            Task.Run(async () => await DALPayment.CheckContactExist()); //kiểm tra tồn  tại rồi xóa
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckMoneyRecharge(long m)
        {
           
            bool accept = false;
            int Type = 0;
            int LeastMoney = 0;
            var user = DALPayment.GetInfoUser();

            if (SesstionManager.UserInfoPayment != null)
            {
                SesstionManager.UserInfoPayment.ExpireDate = user.Data.ExpireDate;
            }

            if (user.Data.ExpireDate == null)
            {
                user.Data.ExpireDate = user.Data.CreateDate.AddDays(7);
                SesstionManager.UserInfoPayment.ExpireDate= user.Data.CreateDate.AddDays(7);
            }
            if (user.Data.ExpireDate.Value.Date == user.Data.CreateDate.AddDays(7).Date)
            {
                if (m + Convert.ToInt32(user.Data.CurrentBalance) < Convert.ToInt32(ConfigurationManager.AppSettings["FirstLimitRecharge"]))
                {
                    accept = false;
                }
                else
                {
                    accept = true;
                }
                Type = 0;
                LeastMoney = Convert.ToInt32(ConfigurationManager.AppSettings["FirstLimitRecharge"]);
            }

            else if (user.Data.ExpireDate.Value.Date < DateTime.Now.Date && user.Data.CreateDate.AddDays(7).Date != user.Data.CreateDate.AddDays(7).Date)
            {
                if (m + Convert.ToInt32(user.Data.CurrentBalance) < Convert.ToInt32(ConfigurationManager.AppSettings["LastLimitRecharge"]))
                {
                    accept = false;
                }
                else
                {
                    accept = true;
                }
                Type = 2;
                LeastMoney = Convert.ToInt32(ConfigurationManager.AppSettings["LastLimitRecharge"]);
            }

            else
            {
                if (m < Convert.ToInt32(ConfigurationManager.AppSettings["LimitRecharge"]))
                {
                    accept = false;
                }
                else
                {
                    accept = true;
                }
                Type = 1;
                LeastMoney = Convert.ToInt32(ConfigurationManager.AppSettings["LimitRecharge"]);
            }
            Object info = new
            {
                Accept = accept,
                Type = Type,
                LeastMoney = LeastMoney,

            };
            return Json(info, JsonRequestBehavior.AllowGet);
        }


        public static bool UpdateSessionUser()
        {
            var client = new HttpClient();
            string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
            client.BaseAddress = new Uri(domain);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            string access_token = SesstionManager.access_token.ToString();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + access_token);
            var values = new Dictionary<string, string>();
            values.Add("access_token", access_token);
            var content = new FormUrlEncodedContent(values);
            HttpResponseMessage response = client.GetAsync("api/User/Info").Result;
            if (response.IsSuccessStatusCode)
            {

                var result = response.Content.ReadAsStringAsync().Result;
                DAL.JsonResult deserialized = JsonConvert.DeserializeObject<DAL.JsonResult>(result);

                if (deserialized.Data.ExpireDate == null)
                {
                    SesstionManager.UserInfoPayment.ExpireDate = deserialized.Data.CreateDate.AddDays(7);
                }
                else
                {
                    SesstionManager.UserInfoPayment.ExpireDate = deserialized.Data.ExpireDate;
                }
                SesstionManager.UserInfoPayment.CurrentBalance = deserialized.Data.CurrentBalance.ToString();

                return true;
            }
            return false;
        }

        public static bool PayMaintenanceFee()
        {
            var client = new HttpClient();
            string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
            client.BaseAddress = new Uri(domain);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string access_token = SesstionManager.access_token.ToString();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + access_token);
            var values = new Dictionary<string, string>();
            values.Add("access_token", access_token);
            var content = new FormUrlEncodedContent(values);
            HttpResponseMessage response = client.PostAsync("api/User/PayMaintenanceFee", content).Result;
            if (response.IsSuccessStatusCode)
            {

                var result = response.Content.ReadAsStringAsync().Result;
                DAL.JsonResult deserialized = JsonConvert.DeserializeObject<DAL.JsonResult>(result);
                SesstionManager.UserInfoPayment.ExpireDate = deserialized.Data.ExpireDate;


                return true;
            }
            return false;
        }


    }
}