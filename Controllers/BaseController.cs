﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _123SendMailV2.Utitlies;
using System.Web.Routing;

namespace _123SendMailV2.Controllers
{
    public class BaseController : Controller
    {

       
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string culture = "vi-VN";
            if (this.Session == null || this.Session["CurrentCulture"] == null)
            {
               culture= System.Configuration.ConfigurationManager.AppSettings["Culture"].ToString();
                this.Session["CurrentCulture"] = culture;
            }
            else
            {
                culture = this.Session["CurrentCulture"].ToString();
            }
            //
            SesstionManager.CurrentCulture = culture;
            //
            // Invokes the action in the current controller context.
            //

            base.OnActionExecuting(filterContext);

            if (SesstionManager.access_token == null || SesstionManager.UserInfo == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                      new RouteValueDictionary { { "controller", "Login" }, { "action", "Index" }, { "returnUrl", HttpContext.Request.Url } });
            }
            
        }

        protected override bool DisableAsyncSupport
        {
            get { return true; }
        }
    }
    
}