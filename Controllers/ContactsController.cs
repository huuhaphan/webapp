﻿
using _123SendMailV2.Utitlies;
using _123SendMailV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace _123SendMailV2.Controllers
{
    public class ContactsController : BaseController
    {
        string JsPara = ConfigurationManager.AppSettings["JsPara"];
        // GET: Contacts
        #region Views
        public ActionResult Index()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.JsPara = JsPara;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.ManageContact == 0)
                {
                    return  RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.JsPara = JsPara;
                    return View();
                }
            }

        }

        /// <summary>
        /// tạo từ menu bar
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateContact()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.JsPara = JsPara;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.ManageContact != 2)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.JsPara = JsPara;
                    return View();
                }
            }
        }

        public ActionResult ContactList(long gId,long? eId,string gName)
        {

            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.ContactGroupName = gName;
                ViewBag.ContactGroupId = gId;
                ViewBag.CampaignId = eId;
                ViewBag.JsPara = JsPara;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.ManageContact == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.ContactGroupName = gName;
                    ViewBag.ContactGroupId = gId;
                    ViewBag.CampaignId = eId;
                    ViewBag.JsPara = JsPara;
                    return View();
                }
            }

           
        }
        public ActionResult ImportExcel(long?gId,long?eId, string gName)
        {
            

            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.ContactGroupId = gId;
                ViewBag.ContactGroupName = gName;
                ViewBag.CampaignId = eId;
                ViewBag.JsPara = JsPara;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.ManageContact != 2)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.ContactGroupId = gId;
                    ViewBag.ContactGroupName = gName;
                    ViewBag.CampaignId = eId;
                    ViewBag.JsPara = JsPara;
                    return View();
                }
            }
        }

        public ActionResult ContactInfo(long cId)
        {
            ViewBag.ContactId = cId;
            return PartialView();
        }
        public ActionResult AddContact(long ContactGroupId, long? CampaignId)
        {
            ViewBag.CampaignId = CampaignId;
            ViewBag.ContactGroupId = ContactGroupId;
            return PartialView();
        }

        public ActionResult UpdateContact(long ContactGroupId,long ContactId, string ContactName, string Email, bool Active, string Mobile,string Note)
        {
            
            ContactViewModel contact =new ContactViewModel();
            contact.Active = Active;
            contact.ContactID = ContactId;
            contact.ContactName = ContactName;
            contact.Email = Email;
            contact.Mobile = Mobile;
            contact.Note = Note;
            ViewBag.Contact = contact;
            ViewBag.ContactGroupId = ContactGroupId;
            return PartialView(contact);
        }
        public ActionResult AddContactGroup()
        {

           
            return PartialView();
        }

        public ActionResult UpdateContactGroup(int contactGroupId ,string ContactGroupName ,string Description ,bool Active)
        {

            
            ContactGroupViewModel group=new ContactGroupViewModel();
            ViewBag.ContactGroupId = contactGroupId;
            ViewBag.ContactGroupName = ContactGroupName;
            ViewBag.Active = Active;
            ViewBag.Desciption = Description;
            
            return PartialView();
        }

        public ActionResult ManageCustomField(long gId, long? eId, string gName)
        {
            
            ViewBag.JsPara = JsPara;
            return PartialView();
        }

        public ActionResult CreateCustomField()
        {
            ViewBag.JsPara = JsPara;
            return PartialView();
        }

        public ActionResult ManageForm(long gId,long? eId ,string gName)
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.ContactGroupId = gId;
                ViewBag.ContactGroupName = gName;
                ViewBag.CampaignId = eId;
                ViewBag.JsPara = JsPara;
                return PartialView();
            }
            else
            {
                if (SesstionManager.SessionPermisson.ManageContact == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.ContactGroupId = gId;
                    ViewBag.ContactGroupName = gName;
                    ViewBag.CampaignId = eId;
                    ViewBag.JsPara = JsPara;
                    return PartialView();
                }
            }

            
        }

        public ActionResult Form(long fId,long gId,long? eId, string gName)
        {


            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.FormId = fId;
                ViewBag.CampaignId = eId;
                ViewBag.ContactGroupId = gId;
                ViewBag.ContactGroupName = gName;

                ViewBag.JsPara = JsPara;
                return PartialView();
            }
            else
            {
                if (SesstionManager.SessionPermisson.ManageContact == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.FormId = fId;
                    ViewBag.CampaignId = eId;
                    ViewBag.ContactGroupId = gId;
                    ViewBag.ContactGroupName = gName;
                    ViewBag.JsPara = JsPara;
                    return PartialView();
                }
            }

            
        }

        public ActionResult Preview(long gId,long fId)
        {

            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.ContactGroupId = gId;
                ViewBag.FormId = fId;
                ViewBag.JsPara = JsPara;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.ManageContact == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.ContactGroupId = gId;
                    ViewBag.FormId = fId;
                    ViewBag.JsPara = JsPara;
                    return View();
                }
            }

           
        }

        public ActionResult FormSetting(long gId,long fId,long?eId, string gName)
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.ContactGroupId = gId;
                ViewBag.CampaignId = eId;
                ViewBag.ContactGroupName = gName;
                ViewBag.FormId = fId;
                ViewBag.JsPara = JsPara;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.ManageContact == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.ContactGroupId = gId;
                    ViewBag.CampaignId = eId;
                    ViewBag.ContactGroupName = gName;
                    ViewBag.FormId = fId;
                    ViewBag.JsPara = JsPara;
                    return View();
                }
            }

            
        }

        public ActionResult PublishForm(long?eId, long? fId,long? gId ,int? width,int? height, string gName)
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.FormId = Utility.Encrypt(fId.ToString(), true);
                ViewBag.Form64 = Utility.Base64Encode(fId.ToString());
                ViewBag.CampaignId = eId;
                ViewBag.ContactGroupName = gName;
                ViewBag.ContactGroupId = gId;
                ViewBag.Width = width;
                ViewBag.height = height;
                ViewBag.JsPara = JsPara;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.ManageContact == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.FormId = Utility.Encrypt(fId.ToString(), true);
                    ViewBag.Form64 = Utility.Base64Encode(fId.ToString());
                    ViewBag.CampaignId = eId;
                    ViewBag.ContactGroupName = gName;
                    ViewBag.ContactGroupId = gId;
                    ViewBag.Width = width;
                    ViewBag.height = height;
                    ViewBag.JsPara = JsPara;
                    return View();
                }
            }

            
        }

        public ActionResult FormPayment(long gId)
        {
            ViewBag.ContactGroupId = gId;
            return PartialView();
        }

        public ActionResult QueryContact()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.JsPara = JsPara;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.ManageContact == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.JsPara = JsPara;
                    return View();
                }
            }

            
        }

        public ActionResult Setting()
        {
            return PartialView();
        }
        #endregion

    }
}
        
     
       

  