﻿using _123SendMailV2.Utitlies;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class AdvancedCampaignController : BaseController
    {
        string JsPara = ConfigurationManager.AppSettings["JsPara"];
        // GET: Autoresponders
        public ActionResult Index()
        {

            if (SesstionManager.SessionPermisson.IsParent)
            {
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.AutoCampaign == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    return View();
                }
            }

           
        }
        public ActionResult Create(int? Day, long? eId ,long? bId)
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.JsPara = JsPara;
                ViewBag.Date = Day;
                ViewBag.BaseId = bId;
                ViewBag.CampaignId = eId;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.AutoCampaign != 2)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.JsPara = JsPara;
                    ViewBag.Date = Day;
                    ViewBag.BaseId = bId;
                    ViewBag.CampaignId = eId;
                    return View();
                }
            }

           
        }

        public ActionResult CreateCampaign()
        {
            ViewBag.JsPara = JsPara;
            return PartialView();
        }

        public ActionResult ManageCampaign(long? eId)
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.JsPara = JsPara;
                ViewBag.CampaignId = eId;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.AutoCampaign == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.JsPara = JsPara;
                    ViewBag.CampaignId = eId;
                    return View();
                }
            }

           
        }

        //public ActionResult ManageContacts(long? eId,long? gId)
        //{
        //    ViewBag.JsPara = JsPara;
        //    ViewBag.CampaignId = eId;
        //    ViewBag.ContactGroupId = gId;
        //    return View();
        //}

        public ActionResult ManageNewsletters(long? eId)
        {
            ViewBag.JsPara = JsPara;
            ViewBag.CampaignId = eId;
            return View();
        }
        /// <summary>
        /// cài đặt email/sms / người gửi người nhận....
        /// </summary>
        /// <returns></returns>
        //public ActionResult Setting()
        //{
           
        //    return PartialView();
        //}
        /// <summary>
        /// nội dung email/sms
        /// </summary>
        /// <returns></returns>
        //public ActionResult Compose()
        //{
          
        //    return PartialView();
        //}

        //edit hoặc tạo mới nội dung của email
        public ActionResult EditEmailContent(long nId)
        {
            ViewBag.JsPara = JsPara;
            ViewBag.NewsletterId = nId;
            ViewBag.UserId= SesstionManager.UserInfo.UserId;
            return PartialView();
        }

        public ActionResult ListEmailCopy()
        {
           
            return PartialView();
        }



        public ActionResult ListEmail()
        {
           
            return PartialView();
        }

        //public ActionResult CreateNewsLetter(long? eId)
        //{
        //    ViewBag.JsPara = JsPara;
        //    ViewBag.CampaignId = eId;
        //    return PartialView();

        //}

    }
}