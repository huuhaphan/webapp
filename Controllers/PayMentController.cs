﻿using _123SendMailV2.DAL;
using _123SendMailV2.Utitlies;
using _123SendMailV2.ViewModels;
using API_NganLuong;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class PayMentController : AsyncController
    {
        // GET: PayMent
        //public async Task<ActionResult> Index()
        public async Task<ActionResult> Index()
        {
            if (SesstionManager.access_token == null || SesstionManager.UserInfo == null)
            {

                return Redirect("Login/LoginRequired");
            }

            string Token = Request["token"];
            RequestCheckOrder info = new RequestCheckOrder();
            info.Merchant_id = System.Configuration.ConfigurationManager.AppSettings["NLMerchantSiteCode"];
            info.Merchant_password = System.Configuration.ConfigurationManager.AppSettings["NLSecurePass"];
            info.Token = Token;
            APICheckoutV3 objNLChecout = new APICheckoutV3();
            ResponseCheckOrder result = objNLChecout.GetTransactionDetail(info);
            ViewBag.Error = result.errorCode;
            ViewBag.Status = result.transactionStatus;// 00 đã thanh toan, 01 đã thanh toán chờ xử lý, 02 chưa thanh toán



            switch (result.errorCode)
            {
                case "00":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode00;
                    break;
                case "99":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode99;
                    break;
                case "03":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode03;
                    break;
                case "04":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode04;
                    break;
                case "05":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode05;
                    break;
                case "06":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode06;
                    break;
                case "07":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode07;
                    break;
                case "08":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode08;
                    break;
                case "09":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode09;
                    break;
                case "10":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode10;
                    break;
                case "11":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode11;
                    break;
                case "12":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode12;
                    break;
                case "29":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode29;
                    break;
                case "81":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode81;
                    break;
                case "110":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode110;
                    break;
                case "111":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode111;
                    break;
                case "113":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode113;
                    break;
                case "114":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode114;
                    break;
                case "115":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode115;
                    break;
                case "118":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode118;
                    break;
                case "119":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode119;
                    break;
                case "120":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode120;
                    break;
                case "121":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode121;
                    break;
                case "122":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode122;
                    break;
                case "123":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode123;
                    break;
                case "124":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode124;
                    break;
                case "125":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode125;
                    break;
                case "126":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode126;
                    break;
                case "127":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode127;
                    break;
                case "128":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode128;
                    break;
                case "129":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode129;
                    break;
                case "130":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode130;
                    break;
                case "131":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode131;
                    break;
                case "132":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode132;
                    break;
                case "133":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode133;
                    break;
                case "134":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode134;
                    break;
                case "135":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode135;
                    break;
                case "140":
                    ViewBag.Error = Resources.PaymentResource.ErrorCode140;
                    break;
            }

            switch (result.transactionStatus)
            {
                case "00":
                    string PayCode = result.order_code;
                    string TrackingId = result.transactionId;
                    string Note = result.description;
                    string AccessToken = Session["access_token"].ToString();
                    //thực hiện lưu xuống
                    apiDAL dal = new apiDAL();
                    DAL.JsonResult jsonresult = await dal.UpdateData(PayCode, TrackingId, Note, AccessToken);
                    switch (jsonresult.ErrorCode)
                    {
                        case "104":
                            ViewBag.Status = "104";
                            break;
                        case "0":
                            ViewBag.Status = "0";
                            break;
                    }

                    //await UpdateData(PayCode, TrackingId, Note);
                    break;
                case "02":
                    ViewBag.Status = "02";
                    break;
                case "01":
                    ViewBag.Status = "01";
                    break;
            }

            return View();
        }

        public async System.Threading.Tasks.Task<ActionResult> UserUpgrade(string PlanId)
        {

            apiDAL dal = new apiDAL();
            string AccessToken = Session["access_token"].ToString();
            DAL.JsonResultUserPlan jsonresult = await dal.CreateUpdateData(PlanId, AccessToken);
            return Json(jsonresult, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> ComparePrice(string pId)
        {
            string IsUpgrade;
            string AccessToken = Session["access_token"].ToString();
            string Blance = SesstionManager.UserInfo.CurrentBalance;
            apiDAL dal = new apiDAL();
            DAL.JsonResultPlan jsonresult = await dal.IsUpgrade(pId, AccessToken);

            switch (Convert.ToInt32(jsonresult.ErrorCode))
            {
                case 0:
                    if (Convert.ToInt32(Blance) >= Convert.ToInt32(jsonresult.Data.Price))
                    {
                        IsUpgrade = "1";
                    }
                    else
                    {
                        IsUpgrade = "0";
                    }
                    break;
                default:
                    IsUpgrade = "-1";
                    break;
             }
             return Json(IsUpgrade, JsonRequestBehavior.AllowGet);

        }

    }
   
}