﻿using _123SendMailV2.Utitlies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class ResetPasswordController : BaseLangController
    {
        // GET: ResetPassword
        public ActionResult Index(string id)
        {
            string data = Utility.Decrypt(id, true);
            string[] arr = Regex.Split(data, ":&:");
            ViewBag.Email = arr[0];
           
            return View();
        }
    }
}