﻿using _123SendMailV2.Utitlies;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class SuppressionsController : BaseController
    {
        string JsPara = ConfigurationManager.AppSettings["JsPara"];
        // GET: Suppressions
        public ActionResult Index()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.Suppression == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    return View();
                }
            }

           
        }

        public ActionResult SpamReports()
        {

            ViewBag.JsPara = JsPara;
            return PartialView();
        }

        public ActionResult Bounces()
        {
            ViewBag.JsPara = JsPara;
            return PartialView();

        }
        public ActionResult Unsubcribes()
        {
            ViewBag.JsPara = JsPara;
            return PartialView();
        }

        public ActionResult Drop()
        {
            ViewBag.JsPara = JsPara;
            return PartialView();
        }
    }
}