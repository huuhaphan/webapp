﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class UtilitiesController : BaseController
    {
        string JsPara = ConfigurationManager.AppSettings["JsPara"];
        // GET: Utilities
        public ActionResult Mytemplates()
        {
            ViewBag.JsPara = JsPara;
            return View();
        }

       public ActionResult Export()
        {
            ViewBag.JsPara = JsPara;
            return View();
        }

      
        public ActionResult CreateTemplate(int? tId)
        {
            ViewBag.JsPara = JsPara;
            ViewBag.TemplateId = tId;
            return View();
        }
    }
}