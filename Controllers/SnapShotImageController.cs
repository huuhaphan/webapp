﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class SnapShotImageController : BaseController
    {
        // GET: SnapShotImage
        public ActionResult Index(long id, string t)
        {
            ViewBag.Id = id;
            ViewBag.Type = t;
            return View();
        }
    }
}