﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class NotHavePermissionController : BaseController
    {
        // GET: NotHavePermission
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Ajax()
        {
            return PartialView();
        }

        public ActionResult Expired()
        {
            return View();
        }
    }
}