﻿using _123SendMailV2.Utitlies;
using _123SendMailV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class HomeController : BaseController
    {
        string JsPara = ConfigurationManager.AppSettings["JsPara"];
        public ActionResult Index()
        {
            ViewBag.JsPara = JsPara;
            return View();
        }

        public ActionResult MenuProfile()
        {
            
            return PartialView();
        }

        public ActionResult LanguageBar()
        {
            
            return PartialView();
        }

        public ActionResult UseStats()
        {
            return PartialView();
        }

        public ActionResult PageSideBar()
        {
            return PartialView();
        }

        public ActionResult Notifycation()
        {
            return PartialView();
        }

    }
}
