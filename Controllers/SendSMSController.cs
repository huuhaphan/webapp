﻿using _123SendMailV2.Utitlies;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class SendSMSController : BaseController
    {
        string JsPara = ConfigurationManager.AppSettings["JsPara"];
        // GET: SendSMS
        public ActionResult Index()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                string eId = Request.QueryString["Id"];
                if (eId != "" && eId != null)
                {
                    ViewBag.SMSSendId = eId;
                }
                else
                {
                    ViewBag.SMSSendId = -1;
                }
                ViewBag.JsPara = JsPara;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.SMSCampaign == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    string eId = Request.QueryString["Id"];
                    if (eId != "" && eId != null)
                    {
                        ViewBag.SMSSendId = eId;
                    }
                    else
                    {
                        ViewBag.SMSSendId = -1;
                    }
                    ViewBag.JsPara = JsPara;
                    return View();
                }
            }


            
        }

        public ActionResult SMSCampaign()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.JsPara = JsPara;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.SMSCampaign == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.JsPara = JsPara;
                    return View();
                }
            } 
        }

        public ActionResult SMSUnsendLogs()
        {
           
            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.JsPara = JsPara;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.SMSUnSendLogs == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.JsPara = JsPara;
                    return View();
                }
            }
        }
    }
}