﻿using _123SendMailV2.Utitlies;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class StatisticsController : BaseController
    {
        // GET: Statistics
        string JsPara = ConfigurationManager.AppSettings["JsPara"];
        public ActionResult Index()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.Statistic == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    return View();
                }
            }

           
        }

        public ActionResult GeneralStatistics()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                string eId = Request.QueryString["eId"];
                if (eId != null)
                {
                    ViewBag.CampaignId = Convert.ToInt32(eId);
                }
                else ViewBag.CampaignId = -1;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.Statistic == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    string eId = Request.QueryString["eId"];
                    if (eId != null)
                    {
                        ViewBag.CampaignId = Convert.ToInt32(eId);
                    }
                    else ViewBag.CampaignId = -1;
                    return View();
                }
            }

           
        }

        public ActionResult TimeBased(long? eId, long?bId)
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.CampaignId = eId;
                ViewBag.BaseId = bId;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.Statistic == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.CampaignId = eId;
                    ViewBag.BaseId = bId;
                    return View();
                }
            }

           
        }

        public ActionResult ActionBased()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
               
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.Statistic == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                 
                    return View();
                }
            }
            
        }

        public ActionResult Message(long? SentId)
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.SentId = SentId;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.Statistic == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.SentId = SentId;
                    return View();
                }
            }

            
        }

        public ActionResult AutoresponderMessage(long EventId)
        {
            ViewBag.EventId = EventId;
            return PartialView();
        }

        public ActionResult PartialMessage()
        {
            return PartialView();
        }

       
    }
}