﻿using _123SendMailV2.Utitlies;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class BasicCampaignController : BaseController
    {
        string JsPara = ConfigurationManager.AppSettings["JsPara"];
        // GET: NewLettersEmail
        public ActionResult Index()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                ViewBag.JsPara = JsPara;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.RocketCampaign == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    ViewBag.JsPara = JsPara;
                    return View();
                }
            }

           
        }

        public ActionResult NotHavePermission()
        {
            return View();
        }

        public ActionResult Setting()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                string eId = Request.QueryString["eId"];
                if (eId != null)
                {
                    long n;
                    bool isNumeric = long.TryParse(eId, out n);
                    if (isNumeric)
                    {
                        ViewBag.CampaignId = n;
                    }
                    else
                    {
                        return View("NotHavePermission");
                    }
                }
                else ViewBag.CampaignId = -1;
                ViewBag.JsPara = JsPara;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.RocketCampaign == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    string eId = Request.QueryString["eId"];
                    if (eId != null)
                    {
                        long n;
                        bool isNumeric = long.TryParse(eId, out n);
                        if (isNumeric)
                        {
                            ViewBag.CampaignId = n;
                        }
                        else
                        {
                            return View("NotHavePermission");
                        }
                    }
                    else ViewBag.CampaignId = -1;
                    ViewBag.JsPara = JsPara;
                    return View();
                }
            }

            
        }

        public ActionResult PartialSetting()
        {
            return PartialView();
        }


        public ActionResult ReViewCampaign(long? cId)
        {
            ViewBag.JsPara = JsPara;
            ViewBag.CampaignId = cId;
            if (SesstionManager.SessionPermisson.IsParent)
            {
               
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.RocketCampaign == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    return View();
                }
            }
        }

        public ActionResult Content()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                string eId = Request.QueryString["eId"];
                if (eId != null)
                {
                    long n;
                    bool isNumeric = long.TryParse(eId, out n);
                    if (isNumeric)
                    {
                        ViewBag.CampaignId = n;
                    }
                    else
                    {

                        return View("NotHavePermission");
                    }
                }
                else ViewBag.CampaignId = -1;
                //ViewBag.NewsLetterId = eId;
                ViewBag.UserId = SesstionManager.UserInfo.UserId;
                ViewBag.JsPara = JsPara;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.RocketCampaign == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    string eId = Request.QueryString["eId"];
                    if (eId != null)
                    {
                        long n;
                        bool isNumeric = long.TryParse(eId, out n);
                        if (isNumeric)
                        {
                            ViewBag.CampaignId = n;
                        }
                        else
                        {

                            return View("NotHavePermission");
                        }
                    }
                    else ViewBag.CampaignId = -1;
                    //ViewBag.NewsLetterId = eId;
                    ViewBag.UserId = SesstionManager.UserInfo.UserId;
                    ViewBag.JsPara = JsPara;
                    return View();
                }
            }


            
        }

        public ActionResult PartialContent()
        {
            return PartialView();
        }

        public ActionResult Recipient()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                string eId = Request.QueryString["eId"];
                if (eId != null)
                {
                    long n;
                    bool isNumeric = long.TryParse(eId, out n);
                    if (isNumeric)
                    {
                        ViewBag.CampaignId = n;
                    }
                    else
                    {
                        return View("NotHavePermission");
                    }
                }
                else ViewBag.CampaignId = -1;
                ViewBag.JsPara = JsPara;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.RocketCampaign == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    string eId = Request.QueryString["eId"];
                    if (eId != null)
                    {
                        long n;
                        bool isNumeric = long.TryParse(eId, out n);
                        if (isNumeric)
                        {
                            ViewBag.CampaignId = n;
                        }
                        else
                        {
                            return View("NotHavePermission");
                        }
                    }
                    else ViewBag.CampaignId = -1;
                    ViewBag.JsPara = JsPara;
                    return View();
                }
            }

        }

        public ActionResult Confirm()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                string eId = Request.QueryString["eId"];
                if (eId != null)
                {
                    long n;
                    bool isNumeric = long.TryParse(eId, out n);
                    if (isNumeric)
                    {
                        ViewBag.CampaignId = n;
                    }
                    else
                    {
                        return View("NotHavePermission");
                    }
                }
                else ViewBag.CampaignId = -1;
                ViewBag.JsPara = JsPara;
                return View();
            }
            else
            {
                if (SesstionManager.SessionPermisson.RocketCampaign == 0)
                {
                    return RedirectToAction("Index", "NotHavePermission");
                }
                else
                {
                    string eId = Request.QueryString["eId"];
                    if (eId != null)
                    {
                        long n;
                        bool isNumeric = long.TryParse(eId, out n);
                        if (isNumeric)
                        {
                            ViewBag.CampaignId = n;
                        }
                        else
                        {
                            return View("NotHavePermission");
                        }
                    }
                    else ViewBag.CampaignId = -1;
                    ViewBag.JsPara = JsPara;
                    return View();
                }
            }


            
        }
        public ActionResult Schedule()
        {
            
            return PartialView();
        }


        public ActionResult ListEmailCopy()
        {
            
            return PartialView();
        }

        public ActionResult Send(long eId)
        {
           
            ViewBag.NewsLetterId = eId;
            return PartialView();
        }

        public ActionResult SelectContact(string lgId)
        {
            
            ViewBag.ListGroupId = lgId;
            return PartialView();
        }

         public ActionResult EditEmailContent()
         {
            return PartialView();
         }

        //public ActionResult PreViewCampaign(string eId)
        //{
            
        //    ViewBag.CampaignId = eId;
        //    return View();
        //}

        //public ActionResult PreviewEmailContent(string eId)
        //{
        //    ViewBag.CampaignId = eId;
        //    return PartialView();
        //}

        //public ActionResult PreviewSMSContent(string eId)
        //{
        //    ViewBag.CampaignId = eId;
        //    return PartialView();
        //}

        //public ActionResult PreviewEmailContentDesktop(string eId)
        //{
        //    ViewBag.CampaignId = eId;
        //    return PartialView();
        //}


    }
}