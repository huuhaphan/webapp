﻿using _123SendMailV2.DAL;
using _123SendMailV2.Utitlies;
using API_NganLuong;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class RechargeController : BasePayLangController
    {
        // GET: PayMent
        //public async Task<ActionResult> Index()
        public async Task<ActionResult> Index()
        {
            string Token = Request["token"];
            RequestCheckOrder info = new RequestCheckOrder();
            info.Merchant_id = System.Configuration.ConfigurationManager.AppSettings["NLMerchantSiteCode"];
            info.Merchant_password = System.Configuration.ConfigurationManager.AppSettings["NLSecurePass"];
            info.Token = Token;
            APICheckoutV3 objNLChecout = new APICheckoutV3();
            ResponseCheckOrder result = objNLChecout.GetTransactionDetail(info);
            ViewBag.Error = result.errorCode;

            if (result.errorCode.Equals("00"))
            {
                string Status = result.transactionStatus;// 00 đã thanh toan, 01 đã thanh toán chờ xử lý, 02 chưa thanh toán
                ViewBag.Status = Status;

                switch (Status)
                {
                    case "00":
                        string PayCode =  result.order_code;
                        string TrackingId = result.transactionId;
                        string Note = result.description;
                        string AccessToken = Session["access_token"].ToString();
                        //thực hiện lưu xuống
                        apiDAL dal = new apiDAL();

                        DAL.JsonResult jsonresult = await dal.UpdateData(PayCode, TrackingId, Note, AccessToken);
                        switch (jsonresult.ErrorCode)
                        {
                            case "104":
                                ViewBag.Status = @Resources.PaymentResource.Error104;
                                break;
                            case "0":
                                ViewBag.Status = @Resources.PaymentResource.Error0;
                                return RedirectToAction("Index", "RechargeSuccess");
                                break;
                        }


                        break;
                    case "02":
                        ViewBag.Status = @Resources.PaymentResource.Error02;
                        break;
                    case "01":
                        ViewBag.Status = @Resources.PaymentResource.Error01;
                        break;

                }
                return View();
            }
            else
            {
                string errorstatus = APICheckoutV3.GetErrorToken(result.errorCode);
                ViewBag.Status = errorstatus;
                return View();

            }
        }

        /// <summary>
        /// cập nhập danh sách trả tiền/đăng kí
        /// </summary>
        public async Task<System.Web.Mvc.JsonResult> UpdateRechargeByCode()
        {
           
            DAL.JsonResult paid = await DAL.apiDAL.AddListPaid();
            DAL.JsonResultFee check = await DAL.apiDAL.CheckContactExist(); //kiểm tra tồn  tại rồi xóa
            return Json(paid, JsonRequestBehavior.AllowGet);
        }

    }

}