﻿using _123SendMailV2.DAL;
using _123SendMailV2.Utitlies;
using com.paypal.sdk.util;
using PaypalCheckout;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class RechargePaypalController : BasePayLangController
    {
        // GET: RechargePaypal

        public async System.Threading.Tasks.Task<ActionResult> Index()
        {
            if (SesstionManager.access_token == null || SesstionManager.UserInfo == null)
            {
                return Redirect("Login/LoginRequired");
            }

            string Token = Request["token"];
            string PAYPAL_USERNAME = ConfigurationManager.AppSettings["PaypalUsername"];
            string PAYPAL_PASSWORD = ConfigurationManager.AppSettings["PaypalPassword"];
            string PAYPAL_SIGNATURE = ConfigurationManager.AppSettings["PaypalSignature"];
            string PayerID;
            string Total;
            string TrackingID;
            string OrderCode;
            NVPCodec Result;
            string Environment = "live";
            bool IsCheckout = Paypal_Checkout.GetExpressCheckoutDetail(PAYPAL_USERNAME, PAYPAL_PASSWORD, PAYPAL_SIGNATURE, Environment, Token, out PayerID, out Total, out TrackingID, out OrderCode, out Result);
            if (IsCheckout)
            {
                string Error;
                string Currency = "USD";
                string PaymentType = "SALE";
                string Amount = Total;
                bool IsSuccess = Paypal_Checkout.DoExpressCheckout(PAYPAL_USERNAME, PAYPAL_PASSWORD, PAYPAL_SIGNATURE, Environment, Token, PayerID, Amount, PaymentType, Currency, out Error);
                if (IsSuccess)
                {

                    string AccessToken = Session["access_token"].ToString();
                    string Note = "Nạp tiền vào tài khoản";
                    apiDAL api = new apiDAL();
                    DAL.JsonResult jsonresult = new DAL.JsonResult();
                    jsonresult = await api.UpdateData(OrderCode, TrackingID, Note, AccessToken);
                    switch (jsonresult.ErrorCode)
                    {

                        case "0":
                            ViewBag.Result = Resources.AccountResource.paymentsuccess;
                            return RedirectToAction("Index", "RechargeSuccess");
                            break;
                        default:
                            ViewBag.Result = Resources.AccountResource.updatedatapaymenterror;
                            break;
                    }
                }
                else
                {
                    ViewBag.Result = Resources.AccountResource.paymenterror;
                }
            }
            else
            {
                ViewBag.Result = Resources.AccountResource.tokenpaypalerror;
            }
            return View();
        }
    }
}