﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class UnsubscribeController : Controller
    {
        // GET: Unsubscribe
        public ActionResult Index(string Id, string Email)
        {
            ViewBag.Id = Id;
            ViewBag.Email = Email;
            return View();
        }
        public ActionResult Success()
        {
            return View();
        }
    }
}