﻿using _123SendMailV2.DAL;
using _123SendMailV2.Utitlies;
using _123SendMailV2.ViewModels;
using API_NganLuong;
using MobileCard;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using PaypalCheckout;
using com.paypal.sdk.util;

namespace _123SendMailV2.Controllers
{
    public class AccountController : BaseController 
    {
        string JsPara = ConfigurationManager.AppSettings["JsPara"];
        // GET: Account
        /// <summary>
        /// trang chi tiết tài khoản
        /// </summary>
        /// <returns></returns>
        public ActionResult UserProfile()
        {

           
            if (SesstionManager.access_token != null)
            {
                if (SesstionManager.SessionPermisson.IsParent)
                {
                    ViewBag.JsPara = JsPara;
                    return View();
                }
                else
                {
                    if (SesstionManager.SessionPermisson.Account == 0)
                    {
                        return RedirectToAction("Index", "NotHavePermission");
                    }
                    else
                    {
                        ViewBag.JsPara = JsPara;
                        return View();
                    }
                }
                
            }
            return RedirectToAction("LoginRequired", "Login");
        }

        public ActionResult Integrated()
        {
            return PartialView();
        }

        public ActionResult OverView()
        {
           
            return PartialView();
        }

        public ActionResult AccountSetting()
        {

            if (SesstionManager.SessionPermisson.IsParent)
            {
                return PartialView();
            }
            else
            {
                if (SesstionManager.SessionPermisson.SettingAccount == 0)
                {
                    return RedirectToAction("Ajax", "NotHavePermission");
                }
                else
                {
                    return PartialView();
                }
            }
           
        }

        public ActionResult SpecialDay()
        {

            if (SesstionManager.SessionPermisson.IsParent)
            {
                return PartialView();
            }
            else
            {
                if (SesstionManager.SessionPermisson.SpeicalDay == 0)
                {
                    return RedirectToAction("Ajax", "NotHavePermission");
                }
                else
                {
                    return PartialView();
                }
            }

        }

        public ActionResult FromMail()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                return PartialView();
            }
            else
            {
                if (SesstionManager.SessionPermisson.FromMail == 0)
                {
                    return RedirectToAction("Ajax", "NotHavePermission");
                }
                else
                {
                    return PartialView();
                }
            }
        }

        public ActionResult FromMobile()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                return PartialView();
            }
            else
            {
                if (SesstionManager.SessionPermisson.FromMobile == 0)
                {
                    return RedirectToAction("Ajax", "NotHavePermission");
                }
                else
                {
                    return PartialView();
                }
            }
        }

        public ActionResult UserPlan()
        {
            
            return PartialView();
        }

        public ActionResult Teammates()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                return PartialView();
            }
            else
            {
                if (SesstionManager.SessionPermisson.SubAccount == 0)
                {
                    return RedirectToAction("Ajax", "NotHavePermission");
                }
                else
                {
                    return PartialView();
                }
            }
        }


        public ActionResult Money()
        {

            if (SesstionManager.SessionPermisson.IsParent)
            {
                return PartialView();
            }
            else
            {
                if (SesstionManager.SessionPermisson.StatisticMoney == 0)
                {
                    return RedirectToAction("Ajax", "NotHavePermission");
                }
                else
                {
                    return PartialView();
                }
            }
        }

        public ActionResult Recharge()
        {
            if (SesstionManager.SessionPermisson.IsParent)
            {
                int LeastMoney = 0;
                var pay = false;

                if (SesstionManager.UserInfo.ExpireDate.Value.Date <= DateTime.Now.Date)
                {
                    UpdateSessionUser();
                    LeastMoney = Convert.ToInt32(ConfigurationManager.AppSettings["LastLimitRecharge"]);

                    if (Convert.ToInt32(SesstionManager.UserInfo.CurrentBalance) >= LeastMoney)
                    {
                        PayMaintenanceFee();
                        pay = true;
                    }
                }
                if (SesstionManager.UserInfo.ExpireDate.Value.Date == SesstionManager.UserInfo.CreateDate.AddDays(Utility.DayTrial).Date)
                {
                    LeastMoney = Convert.ToInt32(ConfigurationManager.AppSettings["FirstLimitRecharge"]);
                    UpdateSessionUser();
                    if (Convert.ToInt32(SesstionManager.UserInfo.CurrentBalance) >= LeastMoney)
                    {
                        PayMaintenanceFee();
                        pay = true;
                    }
                }
                ViewBag.PayMaintenanceFee = pay;
                ViewBag.LeastMoney = String.Format("{0:#,#.##}", LeastMoney);
                return PartialView();
            }
            else
            {
                if (SesstionManager.SessionPermisson.RechargeMoney == 0)
                {
                    return RedirectToAction("Ajax", "NotHavePermission");
                }
                else
                {
                    int LeastMoney = 0;
                    var pay = false;

                    if (SesstionManager.UserInfo.ExpireDate.Value.Date <= DateTime.Now.Date)
                    {
                        UpdateSessionUser();
                        LeastMoney = Convert.ToInt32(ConfigurationManager.AppSettings["LastLimitRecharge"]);

                        if (Convert.ToInt32(SesstionManager.UserInfo.CurrentBalance) >= LeastMoney)
                        {
                            PayMaintenanceFee();
                            pay = true;
                        }
                    }
                    if (SesstionManager.UserInfo.ExpireDate.Value.Date == SesstionManager.UserInfo.CreateDate.AddDays(Utility.DayTrial).Date)
                    {
                        LeastMoney = Convert.ToInt32(ConfigurationManager.AppSettings["FirstLimitRecharge"]);
                        UpdateSessionUser();
                        if (Convert.ToInt32(SesstionManager.UserInfo.CurrentBalance) >= LeastMoney)
                        {
                            PayMaintenanceFee();
                            pay = true;
                        }
                    }
                    ViewBag.PayMaintenanceFee = pay;
                    ViewBag.LeastMoney = String.Format("{0:#,#.##}", LeastMoney);
                    return PartialView();
                }
            }
            
        }


        public ActionResult CheckMoneyRecharge(long m)
        {
            bool accept = false;
            int Type = 0;
            int LeastMoney = 0;
            if (SesstionManager.UserInfo.ExpireDate.Value.Date == SesstionManager.UserInfo.CreateDate.AddDays(Utility.DayTrial).Date)
            {
                if (m+Convert.ToInt32(SesstionManager.UserInfo.CurrentBalance) < Convert.ToInt32(ConfigurationManager.AppSettings["FirstLimitRecharge"]))
                {
                    accept = false;
                }
                else
                {
                    accept = true;
                }
                Type = 0;
                LeastMoney = Convert.ToInt32(ConfigurationManager.AppSettings["FirstLimitRecharge"]);
            }

            else if (SesstionManager.UserInfo.ExpireDate.Value.Date < DateTime.Now.Date && SesstionManager.UserInfo.CreateDate.AddDays(Utility.DayTrial).Date != SesstionManager.UserInfo.CreateDate.AddDays(7).Date)
            {
                if (m + Convert.ToInt32(SesstionManager.UserInfo.CurrentBalance) < Convert.ToInt32(ConfigurationManager.AppSettings["LastLimitRecharge"]))
                {
                    accept = false;
                }
                else
                {
                    accept = true;
                }
                Type = 2;
                LeastMoney = Convert.ToInt32(ConfigurationManager.AppSettings["LastLimitRecharge"]);
            }

            else
            {
                if (m < Convert.ToInt32(ConfigurationManager.AppSettings["LimitRecharge"]))
                {
                    accept = false;
                }
                else
                {
                    accept = true;
                }
                Type = 1;
                LeastMoney = Convert.ToInt32(ConfigurationManager.AppSettings["LimitRecharge"]);
            }
            Object info = new
            {
                Accept = accept,
                Type = Type,
                LeastMoney= LeastMoney,

            };
            return Json(info, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// mua gói
        /// </summary>
        /// <param name="Payment"></param>
        /// <returns></returns>
        public ActionResult UserPayment(Payment Payment)
        {
            string payment_method = Payment.Menthod;
            string str_bankcode = Payment.Bank;
            string SuccessUrl= System.Configuration.ConfigurationManager.AppSettings["Payment_SuccessUrl"];
            string CancelUrl= System.Configuration.ConfigurationManager.AppSettings["Payment_CancalUrl"];
            RequestInfo info = new RequestInfo();
            info.Merchant_id = System.Configuration.ConfigurationManager.AppSettings["NLMerchantSiteCode"];
            info.Merchant_password = System.Configuration.ConfigurationManager.AppSettings["NLSecurePass"];
            info.Receiver_email = System.Configuration.ConfigurationManager.AppSettings["NLReceiver"];

            info.cur_code = "vnd";
            info.bank_code = str_bankcode;

            info.Order_code = Payment.PayCode; //lấy từ api
            info.Total_amount = getPrice(Payment.PlanId).ToString();    //truyền xuống
            info.fee_shipping = "0";
            info.Discount_amount = "0";
            info.order_description = "Mua gói cước/Nạp tiền"; //truyền xuống luôn

            info.Payment_type = "1";//giao dịch ngay

            info.return_url = SuccessUrl;
            info.cancel_url = CancelUrl;

            info.Buyer_fullname = Payment.UserName;
            info.Buyer_email = Payment.Email;
            info.Buyer_mobile = Payment.Mobile;

            APICheckoutV3 objNLChecout = new APICheckoutV3();
            ResponseInfo result = objNLChecout.GetUrlCheckout(info, payment_method);
            ResultPayment rp = new ResultPayment();
            if (result.Error_code == "00")
            {

                rp.Success = true;
                rp.Result = result.Checkout_url;
                return Json(rp, JsonRequestBehavior.AllowGet);
            }
            else
            {
                rp.Success = false;
                rp.Result = result.Description;
                return Json(rp, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// nạp tiền
        /// </summary>
        /// <param name="Payment"></param>
        /// <returns></returns>
        public ActionResult UserRecharge(Payment Payment)
        {
            string payment_method = Payment.Menthod;
            string str_bankcode = Payment.Bank;

            RequestInfo info = new RequestInfo();
            info.Merchant_id = System.Configuration.ConfigurationManager.AppSettings["NLMerchantSiteCode"];
            info.Merchant_password = System.Configuration.ConfigurationManager.AppSettings["NLSecurePass"];
            info.Receiver_email = System.Configuration.ConfigurationManager.AppSettings["NLReceiver"];
            info.cur_code = "vnd";
            info.bank_code = str_bankcode;

            info.Order_code = Payment.PayCode; //lấy từ api
            info.Total_amount = Payment.Amount;
            info.fee_shipping = "0";
            info.Discount_amount = "0";
            info.order_description = "Mua gói cước/Nạp tiền"; //truyền xuống luôn

            info.Payment_type = "1";//giao dịch ngay

            info.return_url = ConfigurationManager.AppSettings["Payment_SuccessUrl"]; 
            info.cancel_url = ConfigurationManager.AppSettings["Payment_CancelUrl"];

            if (SesstionManager.UserInfo.Mobile == "")
            {
                info.Buyer_mobile = "0937010861";
            }
            else
            {
                info.Buyer_mobile = SesstionManager.UserInfo.Mobile;
            }

            info.Buyer_fullname = SesstionManager.UserInfo.FirstName+ SesstionManager.UserInfo.LastName;
            info.Buyer_email = SesstionManager.UserInfo.Email;

            APICheckoutV3 objNLChecout = new APICheckoutV3();
            ResponseInfo result = objNLChecout.GetUrlCheckout(info, payment_method);
            ResultPayment rp = new ResultPayment();
            if (result.Error_code == "00")
            {

                rp.Success = true;
                rp.Result = result.Checkout_url;
                return Json(rp, JsonRequestBehavior.AllowGet);
            }
            else
            {
                rp.Success = false;
                rp.Result = result.Description;
                return Json(rp, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult UserRechargePaypal(Payment payment)
        {
            ResultPayment rp = new ResultPayment();
            string PAYPAL_USERNAME = ConfigurationManager.AppSettings["PaypalUsername"];
            string PAYPAL_PASSWORD = ConfigurationManager.AppSettings["PaypalPassword"];
            string PAYPAL_SIGNATURE = ConfigurationManager.AppSettings["PaypalSignature"];
            string SuccessUrl= ConfigurationManager.AppSettings["Payment_SuccessUrlPayPal"];
            string CancelUrl= ConfigurationManager.AppSettings["Payment_CancelUrl"];
            string Environment = "live";
            NVPCodec Result = new NVPCodec();
            string token = Paypal_Checkout.createPayPalTOKEN(PAYPAL_USERNAME, PAYPAL_PASSWORD, PAYPAL_SIGNATURE, Environment, SuccessUrl, CancelUrl, payment, out Result );
            if(token!=null)
            {
              var url= Paypal_Checkout.buildCheckoutUrl(Environment, token);
                if(url!=null)
                {
                    rp.Success = true;
                    rp.Result = url;

                }
                else
                {
                    rp.Success = false;
                    rp.Result = Resources.AccountResource.createlinkpaypalerror;
                }
            }
            else
            {
                rp.Success = false;
                rp.Result = Resources.AccountResource.createpaycodeerror;
                
            }
            return Json(rp, JsonRequestBehavior.AllowGet);

        }

        public static int getPrice(string PlanId)
        {
            var client = new HttpClient ();
            string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
            client.BaseAddress = new Uri(domain);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            string access_token = SesstionManager.access_token.ToString();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + access_token);
            HttpResponseMessage response = client.GetAsync("api/User/GetMontlyPlanInfo?PlanId="+PlanId).Result;        
            var price = 0;
            if (response.IsSuccessStatusCode)
            {

                var result = response.Content.ReadAsStringAsync().Result;
                JsonResultPlan deserialized = JsonConvert.DeserializeObject<JsonResultPlan>(result);
                price = deserialized.Data.Price;
            }
            return price;
        }

        public static bool PayMaintenanceFee()
        {
            var client = new HttpClient();
            string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
            client.BaseAddress = new Uri(domain);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string access_token = SesstionManager.access_token.ToString();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + access_token);
            var values = new Dictionary<string, string>();
            values.Add("access_token", access_token);
            var content = new FormUrlEncodedContent(values);
            HttpResponseMessage response = client.PostAsync("api/User/PayMaintenanceFee", content).Result;
            if (response.IsSuccessStatusCode)
            {

                var result = response.Content.ReadAsStringAsync().Result;
                DAL.JsonResult deserialized = JsonConvert.DeserializeObject<DAL.JsonResult>(result);
                SesstionManager.UserInfo.ExpireDate = deserialized.Data.ExpireDate;
                SesstionManager.UserInfo.CurrentBalance =deserialized.Data.CurrentBalance.ToString();

                return true;
            }
            return false ;
        }

        //update ngày hết hạn user
        public static bool UpdateSessionUser()
        {
            var client = new HttpClient();
            string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
            client.BaseAddress = new Uri(domain);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            string access_token = SesstionManager.access_token.ToString();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + access_token);
            var values = new Dictionary<string, string>();
            values.Add("access_token", access_token);
            var content = new FormUrlEncodedContent(values);
            HttpResponseMessage response = client.GetAsync("api/User/Info").Result;
            if (response.IsSuccessStatusCode)
            {

                var result = response.Content.ReadAsStringAsync().Result;
                DAL.JsonResult deserialized = JsonConvert.DeserializeObject<DAL.JsonResult>(result);
             
                if(deserialized.Data.ExpireDate==null)
                {
                    SesstionManager.UserInfo.ExpireDate = deserialized.Data.CreateDate.AddDays(Utility.DayTrial);
                }
                else
                {
                    SesstionManager.UserInfo.ExpireDate = deserialized.Data.ExpireDate;
                }
                SesstionManager.UserInfo.CurrentBalance = deserialized.Data.CurrentBalance.ToString();

                return true;
            }
            return false;
        }

       

    }

}