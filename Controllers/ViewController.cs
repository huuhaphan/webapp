﻿using _123SendMailV2.Utitlies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _123SendMailV2.Controllers
{
    public class ViewController : BaseController
    {
        // GET: View
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Newsletter(long nId)
        {
            ViewBag.NewsletterId = nId;
            return View();
        }

        public ActionResult Campaign(long cId)
        {
            ViewBag.CampaignId = cId;
            return View();
        }

        public ActionResult Message(long mId)
        {
            ViewBag.MessageId = mId;
            return View();
        }

        public ActionResult Email(long eId)
        {
            ViewBag.CampaignId = eId;
            return PartialView();
        }
        public ActionResult SMS(long eId)
        {
            ViewBag.CampaignId = eId;
            return PartialView();
        }

        public ActionResult Event(long bId)
        {
            ViewBag.BaseId = bId;
            return View();
        }

        public ActionResult RegBrandName()
        {
            return PartialView();
        }

        public ActionResult ViewForm(string fId)
        {
            try
            {

                ViewBag.FormId = Utility.Decrypt(fId, true);
            }
            catch
            {
                ViewBag.FormId = "";
            }
            return View();
        }
    }
}