﻿/// <reference path="find-in-json.js" />
/// <reference path="DefineConstant.js" />
/// <reference path="sms_counter.js" />
//lấy danh sách systemcustomfield
function appshare_LoadSystemField(apiDomain, access_token, message) {
    var data=$.ajax({
        url: apiDomain + 'api/CustomField/GetSystemList',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        async: false,
        success: function (r) {
        },
        error: function (r) {

        }
    }).responseJSON;
    return data;

};

function appshare_LoadField(apiDomain, access_token, message)
{
    var data = $.ajax({
        url: apiDomain + 'api/CustomField/GetList?Active=true',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        async: false,
        success: function (r) {
        },
        error: function (r) {

        }
    }).responseJSON;
    return data;
}


function appshare_LoadFullField(apiDomain, access_token, message) {
    var data = $.ajax({
        url: apiDomain + 'api/CustomField/GetFullList',
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        async: false,
        success: function (r) {
            
        },
        error: function (r) {
        }
    }).responseJSON;
    return data;
}


function appshare_OTPNumber(MsLength) {
    var Count = 1;

    if (MsLength <= 160) {
        Count = 1;
    }
    if (MsLength > 160 && MsLength <= 306) {
        Count = 2;
    }
    if(MsLength>306 &&MsLength<=422)
    {
        Count = 3;
    }
    return Count;
}

function appshare_BrandNumber(MsLength)
{
    var Count = Math.ceil(MsLength / LENGTH_SMS_BRANDNAME);
    if(Count==0)
    {
        Count = 1;
    }
    return Count;
}

function appshare_GateWayNumber(MsLength) {
    var Count = Math.ceil(MsLength / LENGTH_SMS);
    if (Count == 0) {
        Count = 1;
    }
    return Count;
}

function appshare_GateWayNumberAccented(MsLength) {
    var Count = Math.ceil(MsLength / LENGTH_SMS_ACCENTED);
    if (Count == 0) {
        Count = 1;
    }
    return Count;
}

function appshare_GateWayNumberAccented(smsinputtype,smsstr) {
    return SmsCounter.count(smsinputtype,smsstr);
}


function appshare_SMSSymbolLength(str) {
    var length = 0;
    length += (str.match(/\n/g) || []).length;
    length += (str.match(/\|/g) || []).length;
    length += (str.match(/\^/g) || []).length;
    length += (str.match(/\{/g) || []).length;
    length += (str.match(/\}/g) || []).length;
    length += (str.match(/\[/g) || []).length;
    length += (str.match(/\]/g) || []).length;
    length += (str.match(/\~/g) || []).length;
    length += (str.match(/\\/g) || []).length;

    var regex = new RegExp(/(#([^#])*#)/g)
    var per = str.match(regex); //str.match(/(#([^#]|# #)*#)/g);
   
    if (per != null) {
        $.each(per, function (i, o) {
            var customfield = o;
            
            var fullcustomfield = localStorage.getItem('FullCustomField');
            var json = JSON.parse(fullcustomfield);
            if (customfield == '#ContactName#')
                customfield = '#Name#';

            var object = getObjects(json, 'FieldName', customfield.replace(/#/g, ""));
            var maxLength = getValues(object, 'MaxLength')[0];

            length += maxLength - o.length;
        })
    }

    return length;
}


function appshare_MsgLenghtCount(type,message)
{
    var html = '<div class="margin-top-10 text-left">';
    var mesg = message.SMSNumerNomal;
    if(type==1)
    {
        mesg = message.SMSNumerNomal;
        html += '<a class="label label-danger" style="margin-right:5px;"  href="#modal-info-sms-lenght" data-toggle="modal" data-container="body" data-trigger="hover" data-placement="top" data-original-title="' + message.SMSNumerNomal + '" data-original-title1="' + message.CalSMSNumber + '"> ' + message.Note + '</a>';
    }
    else if (type == 2) {
        mesg = message.SMSNumberBrand;
        html += '<a class="label label-danger" style="margin-right:5px;" href="#modal-info-sms-lenght" data-toggle="modal" data-container="body" data-trigger="hover" data-placement="top" data-original-title="' + message.SMSNumberBrand + '" data-original-title1="' + message.CalSMSNumber + '"> ' + message.Note + '</a>';
    }
    else {
        mesg = message.SMSNumberGetaway;
        html += '<a class="label label-danger" style="margin-right:5px;" href="#modal-info-sms-lenght" data-toggle="modal" data-container="body" data-trigger="hover" data-placement="top" data-original-title1="' + message.SMSNumberGetaway + '"> ' + message.Note + ' </a>';

    }
    html += '<p class="margin-top-10">' + mesg + '</p></div>';
    return html;
}

function appshare_GetListDevice(apiDomain,access_token, messsage) {
   var data= $.ajax({
        url: apiDomain + 'api/User/GetDeviceList?IsConfirm=true',
        type: 'post',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },async: false,
        success: function (r) {
        },
        error: function (r) {

        }
    }).responseJSON;
    return data;
}

function appshare_GetGatewayConfigList(apiDomain, access_token, messsage) {
    var data = $.ajax({
        url: apiDomain + 'api/User/GetGatewayConfigList',
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        }, async: false,
        success: function (r) {
        },
        error: function (r) {

        }
    }).responseJSON;
    return data;
}

function appshare_GetBrandName(apiDomain, access_token, message) {
    var data = $.ajax({
        url: apiDomain + 'api/User/GetSMSBrandNames',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        async: false,
        success: function (r) {
        },
        error: function (r) {

        }
    }).responseJSON;
    return data;

}

function appshare_GetSMSServiceEnables(apiDomain, access_token, messsage) {
  $.ajax({
        url: apiDomain + 'api/User/GetSMSServiceEnables',
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        }, async: false,
        success: function (result) {
            switch (parseInt(result.ErrorCode)) {
                case 0:
                    var service = "";
                    var data = result.Data;
                    if (data.BrandNameOther == false || data.BrandNameViettel == false || data.BrandNameMobi == false || data.BrandNameVina == false || data.OTPMobi == false || data.OTPOther == false || data.OTPViettel == false || data.OTPVina == false) {
                        $('#smsserviceenable').show();
                        if (data.BrandNameOther == false) {
                            service += "BrandName Other, ";
                        } if (data.BrandNameViettel == false) {
                            service += "BrandName Viettel, ";
                        } if (data.BrandNameMobi == false) {
                            service += "BrandName Mobi, ";
                        } if (data.BrandNameVina == false) {
                            service += "BrandName Vina, ";
                        } if (data.OTPMobi == false) {
                            service += "OTP Mobi, ";
                        } if (data.OTPOther == false) {
                            service += "OTP Other, ";
                        } if (data.OTPViettel == false) {
                            service += "OTP Viettel ";
                        }
                        if (data.OTPVina == false) {
                            service += "OTP Vina, ";
                        }
                    }
                    $('#smsserviceenable-detail').text(service);



            }
        },
        error: function (r) {

        }
    })
   
}

