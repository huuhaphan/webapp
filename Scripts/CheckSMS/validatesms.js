

function countMT(msg,shortMax,longMax,prefix){
	if(isNaN(shortMax)) shortMax=0;
	if(isNaN(longMax)) longMax=0;
	if(isNaN(prefix)) prefix=0;
	//them phan tin ky tu dac biet
	var specialCharCount=calculateSpecialChar(msg);
	//
	var msgLength = msg.length + parseInt(prefix)+specialCharCount;
	
	var count = 0;
	if (msgLength <= shortMax)count = 1;
	else {
	if (msgLength%longMax == 0) count = msgLength/longMax;
	else count = Math.floor(msgLength/longMax)+1;
	}
	count += " |" + (msg.length+specialCharCount);
	return count;
	
}

//check has special char
function hasSpecialChar(message) {
	if (message == null || message.isEmpty) {
		return false;
	}
	var specialString = "|^{}[]~\\";
	var specialCharList = specialString.split('');
	var messageChar = message.split('');
	for (var mc in messageChar){
		
		// basic char
		var isGSM2Unicode = false;
		for(var i in GSM2Unicode) {
			if(GSM2Unicode[i] == messageChar[mc]){
			    isGSM2Unicode = true;
			    console.log(GSM2Unicode[i]);
				break;
			}
	    }
		
		if(!isGSM2Unicode) return true;
		
		// char extends
		for (var c in specialCharList){
			if (messageChar[mc] == specialCharList[c]) {
				return true;
			}
		}
	}
	return false;
}

//check has special char
function deleteSpecialChar(message) {
	
	if (message == null || message.isEmpty) {
		return message;
	}
	
	// convert VN string
	// message = vnConvert(message);
	
	var specialString = "";
	var specialCharList = specialString.split('');
	var messageChar = message.split('');
	var delSpecialCharMessage = '';
	
	for (var mc in messageChar){
		
		// basic char
		var isGSM2Unicode = false;
		for(var i in GSM2Unicode) {
			if(GSM2Unicode[i] == messageChar[mc]){
				isGSM2Unicode = true;
				break;
			}
	    }
		
		if(!isGSM2Unicode) continue;
		
		var isContinue = false;
		for (var c in specialCharList){
			if (messageChar[mc] == specialCharList[c]) {
				isContinue = true;
				break;
			}
		}
		
		if(isContinue) continue;
		
		delSpecialCharMessage += messageChar[mc];	
	}
	
	return delSpecialCharMessage;
}

function calculateSpecialChar(message) {
	if (message == null || message.isEmpty) {
		return 0;
	}
	var specialString = "\n|^{}[]~\\";
	var specialCharList = specialString.split('');
	var messageChar = message.split('');
	var countChar = 0;
	for (var mc in messageChar){
		for (var c in specialCharList){
			if (messageChar[mc] == specialCharList[c]) {
				countChar = countChar + 1;
				break;
			}
		}
	}
	return countChar;
}

function CheckValidUrl(strUrl){
    var RegexUrl = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return RegexUrl.test(strUrl);
}

function checkAllowCharacter(str){
	
	if (str == null || str.isEmpty) {
		return true;
	}
	
    for (var i = 0, n = str.length; i < n; i++) {
    	var isGSM2Unicode = false;
		for(var j in GSM2Unicode) {
			if(GSM2Unicode[j] == str[i]){
				isGSM2Unicode = true;
				break;
			}
	    }
		if(!isGSM2Unicode) return false;
    }
    
    return true;
}

function findSpecialCharacter(str){
	var result = "";
    for (var i = 0, n = str.length; i < n; i++) {
    	
    	var isGSM2Unicode = false;
    	for(var j in GSM2Unicode) {
			if(GSM2Unicode[j] == str[i]){
				isGSM2Unicode = true;
				break;
			}
	    }
		if(!isGSM2Unicode) {
        	result = result + (i + 1) + " ";
        }
    }
    return result;
}

function checkAllowCharacterTemplate(str){
	
	if (str == null || str.isEmpty) {
		return true;
	}
	
    for (var i = 0, n = str.length; i < n; i++) {
    	
    	if(str[i] == '}' || str[i] == '{') continue;
    	
    	var isGSM2Unicode = false;
		for(var j in GSM2Unicode) {
			if(GSM2Unicode[j] == str[i]){
				isGSM2Unicode = true;
				break;
			}
	    }
		if(!isGSM2Unicode) return false;
    }
    
    return true;
}

function findSpecialCharacterTemplate(str){
	var result = "";
    for (var i = 0, n = str.length; i < n; i++) {
    	
    	if(str[i] == '}' || str[i] == '{') continue;
    	
    	var isGSM2Unicode = false;
    	for(var j in GSM2Unicode) {
			if(GSM2Unicode[j] == str[i]){
				isGSM2Unicode = true;
				break;
			}
	    }
		if(!isGSM2Unicode) {
        	result = result + (i + 1) + " ";
        }
    }
    return result;
}
