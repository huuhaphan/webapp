﻿$(document).ready(function () {
    var script_tag = document.getElementById('script-payment');
    var query = script_tag.src.replace(/^[^\?]+\??/, '');
    // Parse the querystring into arguments and parameters
    var vars = query.split("&");
    var args = {};
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        // decodeURI doesn't expand "+" to a space.
        args[pair[0]] = decodeURI(pair[1]).replace(/\+/g, ' ');
    }
    var groupId = args['gId'];
    var formId = args['fId'];
    formpayment_GenCode(groupId,formId);

});
function formpayment_GenCode(groupId,formId) {
    var code =
       '<style>' +
         '#_' + groupId + ' .wideblock{' +
           'display:relative;' +
           'width:100%;' +
           'max-width:700px;' +
           'height:auto,' +
           'background-color:#fff;' +
           'border-radius:20px;' +
           'margin: 10px auto 10px;' +
           'padding:50px 0 90px;}' +

         '#_' + groupId + ' .biggerheadline{margin:30px 0 20px;font-size:40px;z-index:1;opacity:1;color:#000;text-shadow:0 0 20px rgba(255,255,255,0.3);font-weight:normal;}' +

         '#_' + groupId + ' .paymethoditem {display: inline-block;position: relative;margin: 15px 0 0 0;width: 150px;height: 150px;background: center top no-repeat;background-size: 100px 100px;}' +
         '#_' + groupId + ' .paymethoditemtext {position: absolute;top: 111px;width: 100%;text-align: center;color: #000;font-family: -apple-system,"lucida grande",tahoma,verdana,arial,sans-serif;font-size: 16px;line-height: 1.3;}' +
         '#_' + groupId + ' .biggerheadline {position: relative;width: auto;height: auto;text-align: center;color: #278FFF;margin-left: auto; margin-bottom: 50px;letter-spacing: -0.03em;font-family: calibri,-apple-system,"HelveticaNeue-Light","Helvetica Neue Light",verdana,arial;}' +

         '#_' + groupId + ' #payonlineatmbutton-main{display: inline-block;background-image: url("http://storage.googleapis.com/hochoc-2j859dmf9/buy/internetbanking.png")}' +

         '#_' + groupId + ' #paytranferbutton-main{display: inline-block;background-image:url("http://storage.googleapis.com/hochoc-2j859dmf9/buy/transfericon4.png")}' +
         '#_' + groupId + ' #paymobilecardmethodbutton-main{background-image: url("http://storage.googleapis.com/hochoc-2j859dmf9/buy/phonecardicon.png")}' +
         '#_' + groupId + ' #payonlinevisabutton-main{background-image: url("http://storage.googleapis.com/hochoc-2j859dmf9/buy/visaicon.png")}' +
       '</style>' +
       '<div id="_' + groupId + '" class="parent-form-payment" data-producid="' + groupId + '" data-formid="' + formId + '">' +
       '<div id="buydiv" class="wideblock">' +
       '<div id="payform" name="payform">' +
           '<input type="hidden" name="productid" value="">' +
           '<input type="hidden" name="subscriptionid" value="">' +
           '<input type="hidden" name="source" value="direct">' +
           '<input id="paymethod" type="hidden" name="paymethod" value="phonecard">' +
          ' <div class="biggerheadline">Điền mail nhận bài học</div>' +
           '<div style="position:relative;height:auto;width: 80%;margin:0 auto;">' +
              ' <input id="email" class="email-pay" type="email" style="width:100%;padding:10px 14px 11px 11px;border:1px solid #0165D7;border-radius:5px;" name="email" value="" autocapitalize="off" autocorrect="off" spellcheck="false" placeholder="Địa chỉ mail của bạn"><br>' +
           '</div>' +
           '<div class="biggerheadline">Chọn cách thanh toán</div>' +
           '<div style="width:100%;height:auto;text-align:center;">' +
               '<a id="menthod-onlineatm" onclick="select_paymnent_onlineatm();">' +
                   '<div id="payonlineatmbutton-main" class="paymethoditem payonlinebutton">' +
                      ' <div class="paymethoditemtext">Ngân hàng<br>trực tuyến</div>' +
                   '</div>' +
              ' </a>' +
               '<a id="menthod-phonecard" onclick="select_paymnent_transfermoney();">' +
               ' <div id="paytranferbutton-main" class="paymethoditem paytranferbutton" >' +
                       '<div class="paymethoditemtext">Gửi tiền vào<br>số tài khoản</div>' +
                  ' </div>' +
              ' </a>' +
               '<a id="menthod-transfermoney" onclick="select_paymnent_phonecard();">' +
                   '<div id="paymobilecardmethodbutton-main" class="paymethoditem paymobilecardmethodbutton">' +
                       '<div class="paymethoditemtext">Thẻ cào<br>các loại</div>' +
                   '</div>' +
               '</a>' +
               '<a id="menthod-onlinevisa" onclick="select_paymnent_onlinevisa();">' +
                   '<div id="payonlinevisabutton-main" class="paymethoditem payonlinebutton">' +
                       '<div class="paymethoditemtext">Visa<br>Mastercard</div>' +
                   '</div>' +
                '</a>' +
           '</div>' +
       '</div>' +
   '</div>'
   //+
//'</div>' +
//  '<script src="https://code.jquery.com/jquery-1.12.1.min.js" integrity="sha256-I1nTg78tSrZev3kjvfdM5A5Ak/blglGzlaZANLPDl3I=" crossorigin="anonymous"></script>' +
//    "<script>" +
//    "function select_paymnent_phonecard() {" +
//        "var productid = $('.parent-form-payment').data('producid');" +
//        "if (requied_payment()) {" +
//           "var buyer = $('.email-pay').val();" +
//            "var win = window.open('http://123pay.azurewebsites.net/Pay?buyer=' + buyer + '&payment=phonecard&menthod=phonecard&productid=' + productid, '_blank');" +
//            "if (win) {" +
//               " win.focus();" +
//            "} else {" +
//                "alert('Please allow popups for this site');" +
//           " }" +
//       " }" +
//    "}" +
//    "function select_paymnent_onlineatm() {" +
//       " var productid = $('.parent-form-payment').data('producid');" +
//        "if (requied_payment()) {" +
//           " var buyer = $('.email-pay').val();" +
//           " var win = window.open('http://123pay.azurewebsites.net/Pay?buyer=' + buyer + '&payment=onlineatm&menthod=atm&productid=' + productid, '_blank');" +
//    "  if (win) {" +
//             "   win.focus();" +
//          "  } else {" +
//          "  }" +
//          "}" +
//   " }" +
//   " function select_paymnent_transfermoney() {" +
//        "var productid = $('.parent-form-payment').data('producid');" +
//       " if (requied_payment()) {" +
//           " var buyer = $('.email-pay').val();" +
//           " var win = window.open('http://123pay.azurewebsites.net/Pay?buyer=' + buyer + '&payment=transfer&menthod=transfer&productid=' + productid, '_blank');" +
//           " if (win) {" +
//               " win.focus();" +
//           " } else {" +
//              "  alert('Please allow popups for this site');" +
//           " }" +
//        "}" +
//   " }" +

//   " function select_paymnent_onlinevisa() {" +
//       " var productid = $('.parent-form-payment').data('producid');" +
//        "if (requied_payment()) {" +
//         "   var buyer = $('.email-pay').val();" +
//          "  var win = window.open('http://123pay.azurewebsites.net/Pay?buyer=' + buyer + '&payment=onlinevisa&menthod=visa&productid=' + productid, '_blank');" +
//          "  if (win) {" +

//              "  win.focus();" +
//          "  } else {" +
//             "   alert('Please allow popups for this site');" +
//            "}" +
//      "  }" +
//   " }" +
//   " function requied_payment() {" +
//       " if (!$('.email-pay').val()) {" +
//           " alert('Hãy nhập địa chỉ email');" +
//         "   return false;" +
//      "  }" +
//     "   else return true;" +
//    "}" +
//    "</script>"
    $('#external-pay-hubly').append(code);
}