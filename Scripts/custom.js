﻿/// <reference path="CheckSMS/validatesms.js" />
//even input only number
function validate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}





//khai bao ngày
function datesingle() {
    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        orientation: "left",
        autoclose: true
    });
}
//khai báo ngày giờ
function datetimesingle() {
    $(".form_datetime").datetimepicker({
        autoclose: true,
        isRTL: App.isRTL(),
        format: "dd MM yyyy - hh:ii",
        pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
    });

    $(".form_advance_datetime").datetimepicker({
        isRTL: App.isRTL(),
        format: "dd MM yyyy - hh:ii",
        autoclose: true,
        todayBtn: true,
        startDate: "2013-02-14 10:00",
        pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
        minuteStep: 10
    });

    $(".form_meridian_datetime").datetimepicker({
        isRTL: App.isRTL(),
        format: "dd MM yyyy - HH:ii P",
        showMeridian: true,
        autoclose: true,
        pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
        todayBtn: true
    });
}

//click vào để edit sủ dụng bootrap editable
function custom_editAbleText(id) {
    $.fn.editable.defaults.mode = 'popup';
    $('#' + id).editable();



}

//validate
 function custom_Validate () {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation
    var form = $('#form');
    var error = $('.alert-danger', form);
    var success = $('.alert-success', form);

    form.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input
        messages: {
            payment: {
                maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                minlength: jQuery.validator.format("At least {0} items must be selected")
            },
            'checkboxes1[]': {
                required: 'Please check some options',
                minlength: jQuery.validator.format("At least {0} items must be selected"),
            },
            'checkboxes2[]': {
                required: 'Please check some options',
                minlength: jQuery.validator.format("At least {0} items must be selected"),
            }
        },
        rules: {
            name: {
                minlength: 2,
                required: true
            },
            Text: {
                minlength: 2,
                required: true
            },
            email: {
                required: true,
                email: true
            },
            Email: {
                required: true,
                email: true
            },
            url: {
                required: true,
                url: true
            },
            URL: {
                required: true,
                url: true
            },
            number: {
                required: true,
                number: true
            },
            Phone: {
                required: true,
                number: true
            },
            digits: {
                required: true,
                digits: true
            },
            creditcard: {
                required: true,
                creditcard: true
            },
            delivery: {
                required: true
            },
            payment: {
                required: true,
                minlength: 2,
                maxlength: 4
            },
            memo: {
                required: true,
                minlength: 10,
                maxlength: 40
            },
            'checkboxes1[]': {
                required: true,
                minlength: 2,
            },
            'checkboxes2[]': {
                required: true,
                minlength: 3,
            },
            radio1: {
                required: true
            },
            radio2: {
                required: true
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox')) {
                error.insertAfter(element.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline"));
            } else if (element.is(':radio')) {
                error.insertAfter(element.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline"));
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
        }
    });
 }

//hàm cảnh báo khi load trang khi đang ở trang tạo form và có ít nhất một fied được tạo.
 function custom_WarningLoad(id,mesage) {
     //document.querySelector('#'+id).addEventListener("click", function () {
     //    window.btn_clicked = true;
     //    console.log(window.btn_clicked);
     //});
     $(window).bind('beforeunload', function () {
         if (window.savedform==false & $('.items li').length >= 1) {
             if ($('.items li').length >= 1) {
                 return mesage;
             }
         }
     });
 }

//load list image
 function custom_ListImage ()
 {
 // init cubeportfolio
     $('#js-grid-juicy-projects').cubeportfolio({
         filters: '#js-filters-juicy-projects',
         loadMore: '#js-loadMore-juicy-projects',
         loadMoreAction: 'click',
         layoutMode: 'grid',
         defaultFilter: '*',
         animationType: 'quicksand',
         gapHorizontal: 35,
         gapVertical: 30,
         gridAdjustment: 'responsive',
         mediaQueries: [{
             width: 1500,
             cols: 6
         }, {
             width: 1100,
             cols: 5
         }, {
             width: 800,
             cols: 5
         }, {
             width: 480,
             cols: 2
         }, {
             width: 320,
             cols: 1
         }],
         caption: 'overlayBottomReveal',
         displayType: 'sequentially',
         displayTypeSpeed: 80,

         // lightbox
         lightboxDelegate: '.cbp-lightbox',
         lightboxGallery: true,
         lightboxTitleSrc: 'data-title',
         lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

         // singlePage popup
         singlePageDelegate: '.cbp-singlePage',
         singlePageDeeplinking: true,
         singlePageStickyNavigation: true,
         singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
         singlePageCallback: function(url, element) {
             // to update singlePage content use the following method: this.updateSinglePage(yourContent)
             var t = this;

             $.ajax({
                 url: url,
                 type: 'GET',
                 dataType: 'html',
                 timeout: 10000
             })
                 .done(function(result) {
                     t.updateSinglePage(result);
                 })
                 .fail(function() {
                     t.updateSinglePage('AJAX Error! Please refresh the page!');
                 });
         },
     });

 }





//setcookie 
 function custom_setCookie(cname, cvalue, exdays) {
     function setCookie(cname, cvalue, exdays) {
         var d = new Date();
         d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
         var expires = "expires=" + d.toUTCString();
         document.cookie = cname + "=" + cvalue + "; " + expires + "; " + "path=/";
     }
 }
 function custom_getCookie(cname) {
     var name = cname + "=";
     var ca = document.cookie.split(';');
     for(var i=0; i<ca.length; i++) {
         var c = ca[i];
         while (c.charAt(0)==' ') c = c.substring(1);
         if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
     }
     return "";
 }

// thông báo notify
 function custom_shownotification(state, message) {
     toastr[state](message)
     toastr.options = {
         "closeButton": true,
         "debug": false,
         "positionClass": "toast-top-right",
         "onclick": null,
         "showDuration": "1000",
         "hideDuration": "1000",
         "timeOut": "3000",
         "extendedTimeOut": "1000",
         "showEasing": "swing",
         "hideEasing": "linear",
         "showMethod": "fadeIn",
         "hideMethod": "fadeOut"
     }
     
 }

//get info, update session
 function custom_GetInfo(apiDomain, access_token,redirect) {
     $.ajax({
         url: apiDomain + 'api/User/Info?IsDirty=true',
         type: 'Get',
         contentType: 'application/json; charset=utf-8',
         beforeSend: function (request) {
             request.setRequestHeader("Authorization", "Bearer " + access_token);
         },
         success: function (result) {
             var user = (JSON.parse(JSON.stringify(result.Data)))
             
             var domain = window.location.hostname;
             var subdomain =domain.replace(domain.substring(0, domain.indexOf('.')),'');
             $.cookie('userId ', user.UserId, { path: '/', domain: subdomain });

             var frommail = {
                 FromMail: user.Email,
                 FromName: user.FirstName + ' ' + user.LastName,
             }
             var frommail = $.cookie('fromemail');
             if (frommail) {
                 login_AcctionAddFromEmail(apiDomain, access_token, JSON.parse(frommail));
             }

             //lưu session
             custom_UpdateSesionLogin(user, access_token, redirect);

         },
         error: function (x, s, e) {
             //loading 
             //$('#divLoading').hide();
             console.log(JSON.stringify(x.reponseText));
         }

     })
 };



//lưu session không cập nhập ngày hết hạn
 function custom_UpdateSesion(userinfo, access_token, redirect) {
     var user = (JSON.parse(JSON.stringify(userinfo)));
     var currentplan = JSON.parse(JSON.stringify(user.CurrentMonthlyPlan));
     var data = {
         UserId: user.UserId,
         Access_Token: access_token,
         FirstName: user.FirstName,
         LastName: user.LastName,
         Avatar: user.Avatar,
         Email: user.Email,
         Mobile: user.MobilePhone,
         City: user.City,
         Street: user.Street,
         Company: user.Company,
         Country: user.Country,
         Facebook: user.Facebook,
         Twitter: user.Twitter,
         CurrentBalance: user.CurrentBalance,
         CreateDate: user.CreateDate,
         EndDate: currentplan.EndDate,
         MonthlyPlanName: currentplan.MonthlyPlanName,
         Quota: currentplan.Quota,
         StarDate: currentplan.StarDate,
         EnableSMS: user.EnableSMS,
         ExpireDate: user.ExpireDate,
         FunctionList: user.FunctionList,
         ParentId:user.ParentId,

     };
     $.ajax({
         url: '/Login/UpdateSession',
         type: 'POST',
         data: JSON.stringify(data),
         contentType: 'application/json; charset=utf-8',
         success: function (result) {


             if (redirect != "none") { //login
                     //tắt trạng thái button login
                     $('#login_btn_submit').button('reset');
                     window.location.href =decodeURIComponent(redirect);
                     $('#divLoading').hide();
                 
             }

         },
         error: function (x, s, e) {
             //loading 
             $('#divLoading').hide();
             console.log(JSON.stringify( x.reponseText));
         }

     })
 };

//có cập nhập ngày hết hạn
 function custom_UpdateSesionLogin(userinfo, access_token, redirect) {
     var user = (JSON.parse(JSON.stringify(userinfo)));
     var currentplan = JSON.parse(JSON.stringify(user.CurrentMonthlyPlan));
     var data = {
         UserId: user.UserId,
         Access_Token: access_token,
         FirstName: user.FirstName,
         LastName: user.LastName,
         Avatar: user.Avatar,
         Email: user.Email,
         Mobile: user.MobilePhone,
         City: user.City,
         Street: user.Street,
         Company: user.Company,
         Country: user.Country,
         Facebook: user.Facebook,
         Twitter: user.Twitter,
         CurrentBalance: user.CurrentBalance,
         CreateDate: user.CreateDate,
         EndDate: currentplan.EndDate,
         MonthlyPlanName: currentplan.MonthlyPlanName,
         Quota: currentplan.Quota,
         StarDate: currentplan.StarDate,
         EnableSMS: user.EnableSMS,
         ExpireDate: user.ExpireDate,
         FunctionList: user.FunctionList,
         ParentId: user.ParentId,

     };
     $.ajax({
         url: '/Login/Login',
         type: 'POST',
         data: JSON.stringify(data),
         contentType: 'application/json; charset=utf-8',
         success: function (result) {


             if (redirect != "none") { //login
                 //tắt trạng thái button login
                 $('#login_btn_submit').button('reset');
                 window.location.href = decodeURIComponent(redirect);
                 $('#divLoading').hide();

             }

         },
         error: function (x, s, e) {
             //loading 
             $('#divLoading').hide();
             console.log(JSON.stringify(x.reponseText));
         }

     })
 };


//remove tất cả option của city
 function custom_RemoveOption()
 {
     $("#accountcity option").each(function () {
         $(this).remove();
     });
 }

//change language
 function custom_changeLanguage(lang) {
     $.ajax({
         url: '/Login/ChangeCurrentCulture?id='+lang,
         type: 'POST',
         contentType: 'application/json; charset=utf-8',
         success: function (result) {
             window.location.reload();
         }

     })
 }
//session timeout
 function custom_Timeout(message) {
     $.sessionTimeout({
         title: message.title,
         message: message.mess,
         keepAliveUrl: '',
         logoutButton: message.logButton,
         keepAliveButton: message.keepButton,
         redirUrl: '/Login',
         logoutUrl: '/Login',
         warnAfter: 2000, //warn after 5 seconds
         redirAfter: 10000, //redirect after 10 secons,
         countdownMessage: message.countDown + ' {timer} .',
         countdownBar: true,
         ignoreUserActivity:true
     });
 }


//ẩn menu 
 function custom_HideMenu() {
     $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
     $('body').addClass('page-sidebar-closed')
 };

 function custom_ReadTextFile(file) {
     var allText = '';
     var rawFile = new XMLHttpRequest();
     rawFile.open("GET", file, false);
     rawFile.onreadystatechange = function () {
         if (rawFile.readyState === 4) {
             if (rawFile.status === 200 || rawFile.status == 0) {
                  allText = rawFile.responseText;

             }
         }
     }
     rawFile.send(null);
     return allText
 }

//lấy giá trị đầu tiên của trong json 
 function custom_GetFisrtProJson(data) {
     var firstProp;
     for (var key in data) {
         if (data.hasOwnProperty(key)) {
             firstProp = data[key];
             break;
         }
     }
     return firstProp;
 };

//kiểm tra email hợp lệ
 function IsEmail(email) {
     var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
     return regex.test(email);
 }

//kiểm tra điện thoại hợp lệ
 function IsMobile(mobile) {
     var regex = /^((01){1}([0-9]{9})|(08){1}([0-9]){8}|(09){1}([0-9]){8}|(841){1}([0-9]){9}|(849){1}([0-9]){8}|(848){1}([0-9]){8}|(\+\849){1}([0-9]){8}|(\+\848){1}([0-9]){8})$/;
     return regex.test(mobile);
 }


//kiểm tra không dấu
 function IsMessage(message) {
     var regex = /^[a-zA-Z0-9.,-/()%*#'"_ ]+$/;
     return regex.test(message);
 };



//pad dung trong upload azure 
 function pad(number, length) {
     var str = '' + number;
     while (str.length < length) {
         str = '0' + str;
     }
     return str;
 }

//get campaignid, sentid from local
 function custom_GetCampaignId()
 {
     var CampaignId;
     if (localStorage.getItem('BasicCampaign')) {
         var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
         if (basiccampaign != '0' && basiccampaign != null) {
             CampaignId = basiccampaign.CampaignId;
         }
     }
     return CampaignId;
 }
 function custom_GetSentEmailId() {
     var SentEmailId;
     if (localStorage.getItem('BasicCampaign')) {
         var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
         if (basiccampaign != '0' && basiccampaign != null) {
             SendEmail=JSON.parse(JSON.stringify(basiccampaign.EmailSend))
             SentEmailId = SendEmail.SentId;
         }
     }
     return SentEmailId;
 }
 function custom_GetSentSMSId() {
     var SentSMSId;
     if (localStorage.getItem('BasicCampaign')) {
         var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
         if (basiccampaign != '0' && basiccampaign != null) {
             SendSMS = JSON.parse(JSON.stringify(basiccampaign.SMSSend))
             SentSMSId = SendSMS.SentId;
         }
     }
     return SentSMSId;
 }

//chuyển viết hoa thành viết thường ví dụ {"New":"new"}=>{"new":"new"} 
 function custom_ConverUpper(myVal) {
   var vbr=  JSON.stringify(myVal, function (key, value) {
         if (value && typeof value === 'object') {
             var replacement = {};
             for (var k in value) {
                 if (Object.hasOwnProperty.call(value, k)) {
                     replacement[k && k.charAt(0).toUpperCase() + k.substring(1)] = value[k];
                 }
             }
             return replacement;
         }
         return value;
   });
   return vbr;
 };

//tương tự với parse.json //chuyển Uppper để thành viết hoa
 function custom_ConverLower(myVal) {
   var vbr=  JSON.parse(text, function (key, value) {
         if (value && typeof value === 'object')
             for (var k in value) {
                 if (/^[A-Z]/.test(k) && Object.hasOwnProperty.call(value, k)) {
                     value[k.charAt(0).toLowerCase() + k.substring(1)] = value[k];
                     delete value[k];
                 }
             }
         return value;
   });
   return vbr;
 }




//action of col step
 function custom_ActionOfColStep(CampaignId,message)
 {
     if (CampaignId != 0) {
         //điều hướng của step
         $('#colSetting').click(function () {

             if (custom_CheckNewsLetterIdBeforeClick(CampaignId, message)) {
                 //$('#divLoading').show();
                 window.location.href = "/BasicCampaign/Setting?eId=" + CampaignId;
             }
         });
         $('#colContent').click(function () {
             if (custom_CheckNewsLetterIdBeforeClick(CampaignId, message)) {
                 //$('#divLoading').show();
                 window.location.href = "/BasicCampaign/Content?eId=" + CampaignId;
             }
         });
         $('#colRecipient').click(function () {
             if (custom_CheckNewsLetterIdBeforeClick(CampaignId, message)) {
                 //$('#divLoading').show();
                 window.location.href = "/BasicCampaign/Recipient?eId=" + CampaignId;
             }
         });
         $('#colConfirm').click(function () {
             if (custom_CheckNewsLetterIdBeforeClick(CampaignId, message)) {
                 //$('#divLoading').show();
                 window.location.href = "/BasicCampaign/Confirm?eId=" + CampaignId;
             }
         });
     }
     else
     {
         $('#divLoading').hide();
     }
 }

 function custom_CheckNewsLetterIdBeforeClick(CampaignId, message) {
     if (!CampaignId) {
         custom_shownotification('error', message);
         return false;
     }
     else return true;
 }



//sự kiên click pagetitlecampagn
//pagetitleclick
 function custom_PageTitleClick() {
     $('#pagetitleBasicCampaign').on('click', function () {
         window.location.href = '/BasicCampaign';
     })
 };

//bắt sự kiện click trên menu đê thêm class active
 $(document).ready(function () {
     $('.page-sidebar-menu .nav-item a').click(function (e) {

         $('.page-sidebar-menu .nav-item').removeClass('active');
       
     });
 });



 //
// var resBreakpointMd = App.getResponsiveBreakpoint('md');
// // Helper function to calculate sidebar height for fixed sidebar layout.
// var _calculateFixedSidebarViewportHeight = function () {
//     var sidebarHeight = App.getViewPort().height - $('.page-header').outerHeight(true);
//     if ($('body').hasClass("page-footer-fixed")) {
//         sidebarHeight = sidebarHeight - $('.page-footer').outerHeight();
//     }

//     return sidebarHeight;
// };

//function heighContent(){
// var content = $('.page-content');
// var sidebar = $('.page-sidebar');
// var body = $('body');
// var height;

// if (body.hasClass("page-footer-fixed") === true && body.hasClass("page-sidebar-fixed") === false) {
//     var available_height = App.getViewPort().height - $('.page-footer').outerHeight() - $('.page-header').outerHeight();
//     if (content.height() < available_height) {
//         content.attr('style', 'min-height:' + available_height + 'px');
//     }
// } else {
//     if (body.hasClass('page-sidebar-fixed')) {
//         height = _calculateFixedSidebarViewportHeight();
//         if (body.hasClass('page-footer-fixed') === false) {
//             height = height - $('.page-footer').outerHeight();
//         }
//     } else {
//         var headerHeight = $('.page-header').outerHeight();
//         var footerHeight = $('.page-footer').outerHeight();

//         if (App.getViewPort().width < resBreakpointMd) {
//             height = App.getViewPort().height - headerHeight - footerHeight;
//         } else {
//             height = sidebar.height() + 20;
//         }

//         if ((height + headerHeight + footerHeight) <= App.getViewPort().height) {
//             height = App.getViewPort().height - headerHeight - footerHeight;
//         }
//     }
//     var outerheight = height - 5;
//     content.attr('style', 'min-height:' + outerheight + 'px');

// }
// };

//table contact
 function resizeTableContact() {
    // heighContent();
     var height = parseInt($(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.portlet-title').outerHeight() - $('.page-footer').outerHeight() - 108);
     var heightscroll = height;
     return heightscroll;
 }



//contaclist
 function resizeTableContactList() {
    // heighContent();
     var height = parseInt($(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.portlet-title').outerHeight() -40 - $('.page-footer').outerHeight() - 108);
     var heightscroll = height;
     return heightscroll;
 }

//resizeTableSuppression
 function resizeTableSuppression() {
     // heighContent();
     var height = parseInt($(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight()  - $('.page-footer').outerHeight() -$('.nav.nav-tabs ').outerHeight- 138);
     var heightscroll = height;
     return heightscroll;
 }


// chỉnh width của contentForm 
function form_resizeContentForm() {
   // heighContent();
    $('#scroll-field-form').css({
        'height': parseInt($(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.page-footer').outerHeight() - $('#fieldForm .nav-tabs').outerHeight() - 30),
    }).addClass("scroller");

    $('#scroll-style-form').css({
        'height': parseInt($(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.page-footer').outerHeight() - $('#fieldForm .nav-tabs').outerHeight() - 30),
    }).addClass("scroller");

    $('#contentForm').css('width', '100%').css('width', '-='+($('#fieldForm').outerWidth()+10));
    //    'width':'100%').css('width', '-=100px'); parseInt(100 - $('#fieldForm').width() - 5),

    //});
    $('#scroll-form-content').css({
        'height': parseInt($(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.page-footer').outerHeight() - $('#contentForm .portlet-title').outerHeight() - 30),
    }).addClass("scroller");


};


//campaign
//resize height
function resizeTableBasicCampaign() {
    var height = parseInt($(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.portlet-title').outerHeight() - $('.page-footer').outerHeight()-105);
    var heightscroll = height;
    return heightscroll;

}

//table login log
function resizeTableLoginLog() {
   // heighContent();
    var height = parseInt($(window).outerHeight() - $('.page-bar').outerHeight() - $('.portlet-title').outerHeight() - 30)
    var heightscroll = height -40;
    return heightscroll;
}

function formatMoney(n, currency) {
    return n.toFixed(0).replace(/./g, function (c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    }) + " " +currency ;
}


//add text at caret
function custom_InsertTextAtCaret(areaId, text) {
    var txtarea = document.getElementById(areaId);
    if (!txtarea) { return; }

    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
        "ff" : (document.selection ? "ie" : false));
    if (br == "ie") {
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart('character', -txtarea.value.length);
        strPos = range.text.length;
    } else if (br == "ff") {
        strPos = txtarea.selectionStart;
    }

    var front = (txtarea.value).substring(0, strPos);
    var back = (txtarea.value).substring(strPos, txtarea.value.length);
    txtarea.value = front + text + back;
    strPos = strPos + text.length;
    if (br == "ie") {
        txtarea.focus();
        var ieRange = document.selection.createRange();
        ieRange.moveStart('character', -txtarea.value.length);
        ieRange.moveStart('character', strPos);
        ieRange.moveEnd('character', 0);
        ieRange.select();
    } else if (br == "ff") {
        txtarea.selectionStart = strPos;
        txtarea.selectionEnd = strPos;
        txtarea.focus();
    }

    txtarea.scrollTop = scrollPos;
}

function custom_bodauTiengViet(str) {

    str = compound2Unicode(str);
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/Đ/g, "D");
    str = str.replace(/€|~|ƒ|„|…|†|‡|ˆ|‰|Š|‹|Œ|Ž|'|'|“|”|•|–|—|˜|™|š|›|œ|ž|Ÿ|¢|¡|¢|£|¤|¥|¦|§|¨|©|ª|«|¬|®|¯||°|±|²|³|´|µ|¶|·|¸|¹|º|»|¼|½|¾|¿|Ä|Å|Æ|Ç|Ë|Ï|Ñ|Ö|×|Ø|Ü|Û||Ü|Þ|ß|ä|å|æ|ç|ë|î|ï|ð|ñ|ö|÷|ø|û|ü|þ|`/g, "");

    var delSpecialCharMsg = deleteSpecialChar(str);
    str = delSpecialCharMsg;
    
    return str;
}

function custom_matchTiengViet(str) {
    str = compound2Unicode(str);
    var res = str.match(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ|è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|ì|í|ị|ỉ|ĩ|Ì|Í|Ị|Ỉ|Ĩ|ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|ỳ|ý|ỵ|ỷ|ỹ|Ỳ|Ý|Ỵ|Ỷ|Ỹ|đ|Đ/g);
    return res;
}

function compound2Unicode(str) {
    str = str.replace(/\u0065\u0309/g, "\u1EBB"); //ẻ
    str = str.replace(/\u0065\u0301/g, "\u00E9"); //é
    str = str.replace(/\u0065\u0300/g, "\u00E8"); //è
    str = str.replace(/\u0065\u0323/g, "\u1EB9"); //ẹ
    str = str.replace(/\u0065\u0303/g, "\u1EBD"); //ẽ
    str = str.replace(/\u00EA\u0309/g, "\u1EC3"); //ể
    str = str.replace(/\u00EA\u0301/g, "\u1EBF"); //ế
    str = str.replace(/\u00EA\u0300/g, "\u1EC1"); //ề
    str = str.replace(/\u00EA\u0323/g, "\u1EC7"); //ệ
    str = str.replace(/\u00EA\u0303/g, "\u1EC5"); //ễ
    str = str.replace(/\u0079\u0309/g, "\u1EF7"); //ỷ
    str = str.replace(/\u0079\u0301/g, "\u00FD"); //ý
    str = str.replace(/\u0079\u0300/g, "\u1EF3"); //ỳ
    str = str.replace(/\u0079\u0323/g, "\u1EF5"); //ỵ
    str = str.replace(/\u0079\u0303/g, "\u1EF9"); //ỹ
    str = str.replace(/\u0075\u0309/g, "\u1EE7"); //ủ
    str = str.replace(/\u0075\u0301/g, "\u00FA"); //ú
    str = str.replace(/\u0075\u0300/g, "\u00F9"); //ù
    str = str.replace(/\u0075\u0323/g, "\u1EE5"); //ụ
    str = str.replace(/\u0075\u0303/g, "\u0169"); //ũ
    str = str.replace(/\u01B0\u0309/g, "\u1EED"); //ử
    str = str.replace(/\u01B0\u0301/g, "\u1EE9"); //ứ
    str = str.replace(/\u01B0\u0300/g, "\u1EEB"); //ừ
    str = str.replace(/\u01B0\u0323/g, "\u1EF1"); //ự
    str = str.replace(/\u01B0\u0303/g, "\u1EEF"); //ữ
    str = str.replace(/\u0069\u0309/g, "\u1EC9"); //ỉ
    str = str.replace(/\u0069\u0301/g, "\u00ED"); //í
    str = str.replace(/\u0069\u0300/g, "\u00EC"); //ì
    str = str.replace(/\u0069\u0323/g, "\u1ECB"); //ị
    str = str.replace(/\u0069\u0303/g, "\u0129"); //ĩ
    str = str.replace(/\u006F\u0309/g, "\u1ECF"); //ỏ
    str = str.replace(/\u006F\u0301/g, "\u00F3"); //ó
    str = str.replace(/\u006F\u0300/g, "\u00F2"); //ò
    str = str.replace(/\u006F\u0323/g, "\u1ECD"); //ọ
    str = str.replace(/\u006F\u0303/g, "\u00F5"); //õ
    str = str.replace(/\u01A1\u0309/g, "\u1EDF"); //ở
    str = str.replace(/\u01A1\u0301/g, "\u1EDB"); //ớ
    str = str.replace(/\u01A1\u0300/g, "\u1EDD"); //ờ
    str = str.replace(/\u01A1\u0323/g, "\u1EE3"); //ợ
    str = str.replace(/\u01A1\u0303/g, "\u1EE1"); //ỡ
    str = str.replace(/\u00F4\u0309/g, "\u1ED5"); //ổ
    str = str.replace(/\u00F4\u0301/g, "\u1ED1"); //ố
    str = str.replace(/\u00F4\u0300/g, "\u1ED3"); //ồ
    str = str.replace(/\u00F4\u0323/g, "\u1ED9"); //ộ
    str = str.replace(/\u00F4\u0303/g, "\u1ED7"); //ỗ
    str = str.replace(/\u0061\u0309/g, "\u1EA3"); //ả
    str = str.replace(/\u0061\u0301/g, "\u00E1"); //á
    str = str.replace(/\u0061\u0300/g, "\u00E0"); //à
    str = str.replace(/\u0061\u0323/g, "\u1EA1"); //ạ
    str = str.replace(/\u0061\u0303/g, "\u00E3"); //ã
    str = str.replace(/\u0103\u0309/g, "\u1EB3"); //ẳ
    str = str.replace(/\u0103\u0301/g, "\u1EAF"); //ắ
    str = str.replace(/\u0103\u0300/g, "\u1EB1"); //ằ
    str = str.replace(/\u0103\u0323/g, "\u1EB7"); //ặ
    str = str.replace(/\u0103\u0303/g, "\u1EB5"); //ẵ
    str = str.replace(/\u00E2\u0309/g, "\u1EA9"); //ẩ
    str = str.replace(/\u00E2\u0301/g, "\u1EA5"); //ấ
    str = str.replace(/\u00E2\u0300/g, "\u1EA7"); //ầ
    str = str.replace(/\u00E2\u0323/g, "\u1EAD"); //ậ
    str = str.replace(/\u00E2\u0303/g, "\u1EAB"); //ẫ
    str = str.replace(/\u0045\u0309/g, "\u1EBA"); //Ẻ
    str = str.replace(/\u0045\u0301/g, "\u00C9"); //É
    str = str.replace(/\u0045\u0300/g, "\u00C8"); //È
    str = str.replace(/\u0045\u0323/g, "\u1EB8"); //Ẹ
    str = str.replace(/\u0045\u0303/g, "\u1EBC"); //Ẽ
    str = str.replace(/\u00CA\u0309/g, "\u1EC2"); //Ể
    str = str.replace(/\u00CA\u0301/g, "\u1EBE"); //Ế
    str = str.replace(/\u00CA\u0300/g, "\u1EC0"); //Ề
    str = str.replace(/\u00CA\u0323/g, "\u1EC6"); //Ệ
    str = str.replace(/\u00CA\u0303/g, "\u1EC4"); //Ễ
    str = str.replace(/\u0059\u0309/g, "\u1EF6"); //Ỷ
    str = str.replace(/\u0059\u0301/g, "\u00DD"); //Ý
    str = str.replace(/\u0059\u0300/g, "\u1EF2"); //Ỳ
    str = str.replace(/\u0059\u0323/g, "\u1EF4"); //Ỵ
    str = str.replace(/\u0059\u0303/g, "\u1EF8"); //Ỹ
    str = str.replace(/\u0055\u0309/g, "\u1EE6"); //Ủ
    str = str.replace(/\u0055\u0301/g, "\u00DA"); //Ú
    str = str.replace(/\u0055\u0300/g, "\u00D9"); //Ù
    str = str.replace(/\u0055\u0323/g, "\u1EE4"); //Ụ
    str = str.replace(/\u0055\u0303/g, "\u0168"); //Ũ
    str = str.replace(/\u01AF\u0309/g, "\u1EEC"); //Ử
    str = str.replace(/\u01AF\u0301/g, "\u1EE8"); //Ứ
    str = str.replace(/\u01AF\u0300/g, "\u1EEA"); //Ừ
    str = str.replace(/\u01AF\u0323/g, "\u1EF0"); //Ự
    str = str.replace(/\u01AF\u0303/g, "\u1EEE"); //Ữ
    str = str.replace(/\u0049\u0309/g, "\u1EC8"); //Ỉ
    str = str.replace(/\u0049\u0301/g, "\u00CD"); //Í
    str = str.replace(/\u0049\u0300/g, "\u00CC"); //Ì
    str = str.replace(/\u0049\u0323/g, "\u1ECA"); //Ị
    str = str.replace(/\u0049\u0303/g, "\u0128"); //Ĩ
    str = str.replace(/\u004F\u0309/g, "\u1ECE"); //Ỏ
    str = str.replace(/\u004F\u0301/g, "\u00D3"); //Ó
    str = str.replace(/\u004F\u0300/g, "\u00D2"); //Ò
    str = str.replace(/\u004F\u0323/g, "\u1ECC"); //Ọ
    str = str.replace(/\u004F\u0303/g, "\u00D5"); //Õ
    str = str.replace(/\u01A0\u0309/g, "\u1EDE"); //Ở
    str = str.replace(/\u01A0\u0301/g, "\u1EDA"); //Ớ
    str = str.replace(/\u01A0\u0300/g, "\u1EDC"); //Ờ
    str = str.replace(/\u01A0\u0323/g, "\u1EE2"); //Ợ
    str = str.replace(/\u01A0\u0303/g, "\u1EE0"); //Ỡ
    str = str.replace(/\u00D4\u0309/g, "\u1ED4"); //Ổ
    str = str.replace(/\u00D4\u0301/g, "\u1ED0"); //Ố
    str = str.replace(/\u00D4\u0300/g, "\u1ED2"); //Ồ
    str = str.replace(/\u00D4\u0323/g, "\u1ED8"); //Ộ
    str = str.replace(/\u00D4\u0303/g, "\u1ED6"); //Ỗ
    str = str.replace(/\u0041\u0309/g, "\u1EA2"); //Ả
    str = str.replace(/\u0041\u0301/g, "\u00C1"); //Á
    str = str.replace(/\u0041\u0300/g, "\u00C0"); //À
    str = str.replace(/\u0041\u0323/g, "\u1EA0"); //Ạ
    str = str.replace(/\u0041\u0303/g, "\u00C3"); //Ã
    str = str.replace(/\u0102\u0309/g, "\u1EB2"); //Ẳ
    str = str.replace(/\u0102\u0301/g, "\u1EAE"); //Ắ
    str = str.replace(/\u0102\u0300/g, "\u1EB0"); //Ằ
    str = str.replace(/\u0102\u0323/g, "\u1EB6"); //Ặ
    str = str.replace(/\u0102\u0303/g, "\u1EB4"); //Ẵ
    str = str.replace(/\u00C2\u0309/g, "\u1EA8"); //Ẩ
    str = str.replace(/\u00C2\u0301/g, "\u1EA4"); //Ấ
    str = str.replace(/\u00C2\u0300/g, "\u1EA6"); //Ầ
    str = str.replace(/\u00C2\u0323/g, "\u1EAC"); //Ậ
    str = str.replace(/\u00C2\u0303/g, "\u1EAA"); //Ẫ

    return str;
}

function custom_bodaukeycode(str)
{
    str = str.toString();
    str = str.replace(/161|162|163|164|165|166|167|198|199|213|224|225|226|227|228|229/g, "97");//a
    str = str.replace(/128|129|130|131|132|133|134|135|192|193|194|195|196|197/g, "65");//A
    str = str.replace(/136|136|138|139|140|141|142|200|201|202|203/g, "69");//E
    str = str.replace(/168|169|170|171|172|173|174|232|234|235/g, "101");//e
    str = str.replace(/184|236|237|238|239/g, "105");//i
    str = str.replace(/152|155|204|205|206/g, "73");//I
    str = str.replace(/175|176|177|178|179|180|181|182|183|222|242|243|244|245|246|247|254|231/g, "111");//o
    str = str.replace(/143|144|145|146|147|148|149|150|151|153|154|160|179|180|210|211|212/g, "79");//O
    str = str.replace(/209|215|216|223|230|241|249|250|251|252/g, "117");//u
    str = str.replace(/156|157|158|185|186|187|188|191|217|218|255/g, "85");//U
    str = str.replace(/207|214|219|220|221|253/g, "121");//y
    str = str.replace(/159|221/g, "89");//Y
    str = str.replace(/240/g, "100");//đ
    str = str.replace(/208/g, "68");//Đ

    return str;

}

//vị trí contro chuot
var mousePos;
//document.onmousemove = handleMouseMove;
function handleMouseMove(event) {
    var dot, eventDoc, doc, body, pageX, pageY;

    event = event || window.event; // IE-ism

    // If pageX/Y aren't available and clientX/Y are,
    // calculate pageX/Y - logic taken from jQuery.
    // (This is to support old IE)
    if (event.pageX == null && event.clientX != null) {
        eventDoc = (event.target && event.target.ownerDocument) || document;
        doc = eventDoc.documentElement;
        body = eventDoc.body;

        event.pageX = event.clientX +
          (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
          (doc && doc.clientLeft || body && body.clientLeft || 0);
        event.pageY = event.clientY +
          (doc && doc.scrollTop || body && body.scrollTop || 0) -
          (doc && doc.clientTop || body && body.clientTop || 0);
    }

    mousePos = {
        x: event.pageX,
        y: event.pageY,
        m: $(window).width(),
        n: $(window).height(),
    };
}


//compare 2 array
Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time 
    if (this.length != array.length)
        return false;

    for (var i = 0, l = this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;
        }
        else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
}

//kiểm tra phần tử có trong mảng
Array.prototype.contains = function (thing) {
    // if the other array is a falsy value, return false
    if (!this)
        return false;

    //start by assuming the array doesn't contain the thing
    var result = false;
    for (var i = 0, l = this.length; i < l; i++) {
        //if anything in the array is the thing then change our mind from before

        if (this[i] instanceof Array) {
            if (this[i].equals(thing))
                result = true;
        }
        else
            if (this[i] === thing)
                result = true;


    }
    //return the decision we left in the variable, result
    return result;
}


function custom_LimitShowText(Id) {
    var limit = 50;
    var chars = $(Id).text();
    if (chars.length > limit) {
        var visiblePart = $("<span> " + chars.substr(0, limit - 1) + "</span>");
        var dots = $("<span class='dots'>... </span>");
        $(Id).empty()
            .append(visiblePart)
            .append(dots)
    }
}

function custom_dec2bin(dec) {
    return (dec >>> 0).toString(2);
}

function locdauExport(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g, "-");
    str = str.replace(/-+-/g, "-"); //thay thế 2- thành 1-
    str = str.replace(/^\-+|\-+$/g, "");//cắt bỏ ký tự - ở đầu và cuối chuỗi
    return  str;
}


var Base64 = {


    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",


    encode: function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },


    decode: function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = Base64._utf8_decode(output);

        return output;

    },

    _utf8_encode: function (string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    _utf8_decode: function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while (i < utftext.length) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }

}

function custom_local2utc(from, to) {
    //var start = new Date(from);
    // start.setHours(0, 0, 0, 0); //tạm không dùng giờ .
    //start.setHours(0, 0, 0, 0);
    //var end = new Date(to);
    // end.setHours(23, 59, 59, 999); //tạm không dùng giờ
    //end.setHours(0, 0, 0, 0);
    var date = {
        StartDate: moment(from).format('YYYY-MM-DD'),//moment.utc(start.toUTCString()).format(),
        EndDate: moment(to).format('YYYY-MM-DD'),//moment.utc(end.toUTCString()).format(),
    };
    return date;
};

function custom_GetTimeZone() {
    var d = new Date()
    var timezoneOffset = d.getTimezoneOffset() / 60; //múi giờ + thì offset là - và ngược lại
    if (timezoneOffset > 0) {
        timezoneOffset = -Math.abs(timezoneOffset); 
    }
    if (timezoneOffset < 0) {
        timezoneOffset = Math.abs(timezoneOffset);
    }
  
    return timezoneOffset;
  
}


//tắt popover khi click bên ngoài 
$('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).remove();
        }
    });
});






 
