﻿function actionbase_Action(apiDomain, access_token, CampaignId, message)
{
    actionbase_GetCampaignList(apiDomain, access_token, CampaignId, message)
    setTimeout(function () {
        actionbase_RenderTypeAction(apiDomain, access_token, message);
    }, 1);

    actionbase_ChangeCampaign(apiDomain, access_token, message);
 
}

function actionbase_GetCampaignList(apiDomain, access_token, CampaignId, message) {
    var data = {
        //Status: 1,  //sent
        IsAutoResponder: true,
    }
    $.ajax({
        url: apiDomain + 'api/BasicCampaign/GetList',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: data,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var option = [];
                    var data = r.Data;
                    $.each(data.reverse(), function (i, o) {
                        option.push({
                            id: o.CampaignId,
                            text: o.CampaignName,
                        });
                    });

                    $('#bs-select-campaign').select2({ data: option });
                    $('.select2.select2-container.select2-container--bootstrap.select2-container--below').css('width', 'auto !important');
                    if (CampaignId) {
                        $('#bs-select-campaign').select2('val', CampaignId);
                    }
                    var campaign = $("#bs-select-campaign").select2('data')[0];

                    if (campaign.id) {
                        actionbase_GetEventList(apiDomain, access_token, campaign.id, message);
                    }
                    break;
                default:
                    custom_shownotification('error', message.GetDataError)
                    break;
            }
        }
       , error: function (x, s, e) {
           custom_shownotification('error', message.GetDataError)
       }
    })
}

var type_actionbase = [];
function actionbase_RenderTypeAction(apiDomain,access_token,message)
{
    var data = [{ BaseType: 1, Name: 'Đã mở' }, { BaseType: 2, Name: 'Đã nhấp chuột' }]
    var option='';
    $.each(data, function (i, o) {
        option += '<option value="' + o.BaseType + '" selected>' + o.Name + '</option>';
    });
    $('#type-actionbase').children().remove();
    $('<select id="select-type-action" multiple="multiple" data-width="100%"></select>').append(option).appendTo('#type-actionbase');
    $('#select-type-action').multiselect({
        includeSelectAllOption: true,
        buttonWidth: '100%',

        selectAllValue: 'select-all-value',
        selectAllText: message.SelectAll,
        allSelectedText: message.SelectedAll,
        nonSelectedText: message.NotSelect,
        enableFiltering: true,
        onInitialized: function (select, container) {
            var value = $('#select-type-action option:selected');
            type_actionbase = [];
            $.each(value, function (i, o) {
                type_actionbase.push($(this).val());
            });
        },
        onDropdownHidden: function (event) {
            var value = $('#select-type-action option:selected');
            type_actionbase = [];
            $.each(value, function (i, o) {
                type_actionbase.push($(this).val());
            });
        }
    });
    $('#type-actionbase .multiselect').addClass('white');
    $('.multiselect-container').css('width', '100%');
}

function actionbase_GetEventList(apiDomain, access_token, CampaignId, message) {

    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/AutoCampaign/GetEventList?CampaignId=' + CampaignId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            $('#divLoading').hide();
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    if (data) {
                        var option;
                        $.each(data, function (i, o) {
                            option += '<option value="' + o.BaseId + '" selected>' + o.ResponderName + '</option>';
                        });
                        $('#autoresponder-list-statistic').children().remove();
                        $('<select id="autoresponder-list" multiple="multiple" data-width="100%"></select>').append(option).appendTo('#autoresponder-list-statistic');
                        $('#autoresponder-list').multiselect({
                            includeSelectAllOption: true,
                            buttonWidth: '100%',

                            selectAllValue: 'select-all-value',
                            selectAllText: message.SelectAll,
                            allSelectedText: message.SelectedAll,
                            nonSelectedText: message.NotSelect,
                            enableFiltering: true,
                            onInitialized: function (select, container) {
                                var value = $('#autoresponder-list option:selected');
                                newsletter_timebase = [];
                                $.each(value, function (i, o) {
                                    newsletter_timebase.push($(this).val());
                                });
                            },
                            onDropdownHidden: function (event) {
                                var value = $('#autoresponder-list option:selected');
                                newsletter_timebase = [];
                                $.each(value, function (i, o) {
                                    newsletter_timebase.push($(this).val());
                                });
                            }
                        });
                        $('#autoresponder-list-statistic .multiselect').addClass('white');
                        $('.multiselect-container').css('width', '100%');
                    }

                    break;
                default:
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification("error", message.HaveError);
        }
    })
}

function actionbase_ChangeCampaign(apiDomain, access_token, message) {
    $('#bs-select-campaign').on('select2:select', function (evt) {
        var campaign = $("#bs-select-campaign").select2('data')[0];
        actionbase_GetEventList(apiDomain, access_token, campaign.id, message);

    });
}