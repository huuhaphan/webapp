﻿//multi http://davidstutz.github.io/bootstrap-multiselect/#configuration-options-buttonText

var timebase_tab = 1; //1 total 2 open 3 click 4 bounce 5 unsub 6 complaint
var timebase_type = 1;//1 email , 2 sms
var auto_totaltype = 0;
var auto_opentype = 1;
var auto_openuniquetype = 0;
var auto_clicktype = 0;
var auto_sent = 0;
var auto_selectedlink = [];
var auto_clickuniquetype = 0;
var datasendat;
var datasendopenat;
var datasendclickat;

function timebase_Action(apiDomain, access_token, CampaignId,BaseId, message)
{
    timebase_IntializeTime();
    //load campaign list
    timebase_GetCampaignList(apiDomain, access_token, CampaignId,BaseId, message);

    //select campaign.
    timebasse_ChangeCampaign(apiDomain, access_token, message);

    //select tab report email or sms
    timebase_SelectEmailOrSMS(apiDomain, access_token, message);

    //select tab
    timebase_SelectTab(apiDomain, access_token, message);
    
    //select date
    timebase_SelectDateReport(apiDomain, access_token, CampaignId, BaseId, message);

    //select sum or unique total
    timebase_SwitchChangeTotalChart(apiDomain, access_token, message)
   
    //select type of open (sum, unique, not open)
    timebase_SelectTypeOpen(apiDomain, access_token, message);

    //select type of unique open( sum, hours)
    timebase_ChangeTypeUniqueOpen(apiDomain, access_token, message);
    
    //select type of click(sum, unique)
    timebase_ChangeTypeClick(apiDomain, access_token, message);

    //select type of unique click
    timebase_ChangeTypeUniqueClick(apiDomain, access_token, message);

    timebase_resizeSelect();

    timebase_ExportContact();

    timebase_SubmitExport(apiDomain, access_token, message);
    timebase_Export(apiDomain, access_token, message);
}

function timebase_ResetVarData()
{
    datasendat = null;
    datasendopenat = null;
    datasendclickat = null;
}

function timebase_IntializeTime()
{
    var datefromselect = moment().format('YYYY-MM-DD');
    var formdate = moment().subtract(7, 'days').format('YYYY-MM-DD');
    $("#select-date-from").datepicker("setDate", formdate);
    $('#select-date-to').datepicker("setDate", moment().format('YYYY-MM-DD'));
}

function timebase_SelectEmailOrSMS(apiDomain, access_token, message)
{
    $('#tab-autoresponder-sms a').on('click', function () {
        timebase_type = 2;
        timebase_LoadDataOfTab(apiDomain, access_token, message);
    });

    $('#tab-autoresponder-email a').on('click', function () {
        timebase_type = 1;
        timebase_LoadDataOfTab(apiDomain, access_token, message);
    });

}

function timebase_SelectDateReport(apiDomain, access_token, CampaignId, BaseId, message)
{
    $('#select-date-report-auto').on('click', function () {
       
        timebase_GetCampaignList(apiDomain, access_token, CampaignId, BaseId, message)
        //timebase_ResetVarData();
        //timebase_LoadDataOfTab(apiDomain, access_token, message);
        
    });
}

function timebase_GetCampaignList(apiDomain, access_token, CampaignId, BaseId, message) {
    var data = {
        //Status: 1,  //sent
        IsAutoResponder: true,
    }
    $.ajax({
        url: apiDomain + 'api/BasicCampaign/GetList',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: data,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var option = [];
                    var data = r.Data;
                    $.each(data.reverse(), function (i, o) {
                        var date = custom_local2utc(moment($('#select-date-from').val()).format('YYYY-MM-DD'), moment($('#select-date-to').val()).format('YYYY-MM-DD'));
                        if (moment(o.CreateDate).format('YYYY-MM-DD') >= date.StartDate || moment(o.CreateDate).format('YYYY-MM-DD') <= data.EndDate) {
                            option.push({
                                id: o.CampaignId,
                                text: o.CampaignName,
                            });
                        }
                    });

                    $('#bs-select-campaign').select2({ data: option });
                    $('.select2.select2-container.select2-container--bootstrap.select2-container--below').css('width', 'auto !important');

                    $("#bs-select-campaign").on("select2:open", function () {
                        $(".select2-search--dropdown .select2-search__field").attr("placeholder", "Search...");
                    });
                    $("#bs-select-campaign").on("select2:close", function () {
                        $(".select2-search--dropdown .select2-search__field").attr("placeholder", null);
                    });

                    if (option.length > 0) {
                        if (CampaignId) {
                            $('#bs-select-campaign').select2('val', CampaignId);
                        }
                        var campaign = $("#bs-select-campaign").select2('data')[0];

                        if (campaign.id) {
                            timebase_GetEventList(apiDomain, access_token, campaign.id, BaseId, message);
                        }
                    }
                    else
                    {
                        timebase_NoCampaign();
                    }
                    break;
                default:
                    timebase_NoCampaign();
                    custom_shownotification('error', message.GetDataError)
                    break;
            }
        }
       , error: function (x, s, e) {
           timebase_NoCampaign();
           custom_shownotification('error', message.GetDataError)
       }
    })
}

function timebase_resizeSelect()
{
    window.onresize = function () {
        $('.select2.select2-container.select2-container--bootstrap').css({ 'width': 'auto !important' });

    }
}

function timebasse_ChangeCampaign(apiDomain, access_token,  message)
{
    $('#bs-select-campaign').on('select2:select', function (evt) {
        var campaign = $("#bs-select-campaign").select2('data')[0];
        var BaseId = undefined;
        timebase_GetEventList(apiDomain, access_token, campaign.id, BaseId, message);
      
    });
}

var event_timebase = []; //chọn nhiều event
var event_timebaseall = false; //chọn tất cả event
function timebase_GetEventList(apiDomain, access_token, CampaignId, BaseId, message) {

    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/AutoCampaign/GetEventList?CampaignId=' + CampaignId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            $('#divLoading').hide();
            timebase_ResetVarData();
            switch (parseInt(r.ErrorCode)) {
                case 0:

                    var data = r.Data;
                    if (data.length>0) {
                        var option;
                        $.each(data, function (i, o) {
                            option += '<option value="' + o.BaseId + '" selected>' + o.ResponderName + '</option>';
                        });
                        $('#event-list-timebase').children().remove();
                        $('<select id="select-event-list-timebase" multiple="multiple" data-width="100%"></select>').append(option).appendTo('#event-list-timebase');
                        $('#select-event-list-timebase').multiselect({
                            includeSelectAllOption: true,
                            buttonWidth: '100%',

                            selectAllValue: 'select-all-value',
                            selectAllText: message.SelectAll,
                            allSelectedText: message.SelectedAll,
                            nonSelectedText: message.NotSelect,
                            enableFiltering: true,
                            onInitialized: function (select, container) {
                                var value = $('#select-event-list-timebase option:selected');
                                event_timebase = [];
                                $.each(value, function (i, o) {
                                    event_timebase.push($(this).val());
                                });
                            },
                            onDropdownHidden: function (event) {
                                var value = $('#select-event-list-timebase option:selected');
                                event_timebase = [];
                                $.each(value, function (i, o) {
                                    event_timebase.push($(this).val());
                                });

                                if (event_timebase.length < data.length) {
                                    event_timebaseall = false;
                                }
                                //reset biến lưu data
                                timebase_ResetVarData();
                                //load chart
                                timebase_LoadDataOfTab(apiDomain, access_token, message);
                                if (timebase_tab != 1) { //nếu bằng 1 thì timebase_GetSendReport đã được gọi trong hàm timebase_LoadDataOfTab
                                    timebase_CheckLoadSendReport(apiDomain, access_token, message);
                                }
                            },
                            onSelectAll: function () {
                                event_timebaseall = true;
                            },

                        });
                        $('#event-list-timebase .multiselect').addClass('white');
                        $('.multiselect-container').css('width', '100%');
                        if (BaseId) {
                            $('#select-event-list-timebase').multiselect('deselectAll', false);
                            event_timebase = [];
                            event_timebase.push(BaseId);
                            event_timebaseall = false;
                            var value = [];
                            value.push(BaseId);
                            $('#select-event-list-timebase').multiselect('select', value);
                        }

                        //reset biến luu data
                        timebase_ResetVarData();
                        //load chart
                        timebase_LoadDataOfTab(apiDomain, access_token, message);
                    
                        if (timebase_tab != 1) { //nếu bằng 1 thì timebase_GetSendReport đã được gọi trong hàm timebase_LoadDataOfTab
                            timebase_CheckLoadSendReport(apiDomain, access_token, message);
                        }

                    }
                   //nếu không có event nào 
                    else {
                        console.log('không có lenght');
                        timebase_LoadDataOfTab(apiDomain, access_token, message);

                        if (timebase_tab != 1) { //nếu bằng 1 thì timebase_GetSendReport đã được gọi trong hàm timebase_LoadDataOfTab
                            timebase_CheckLoadSendReport(apiDomain, access_token, message);
                        }
                    }

                    break;
                default:
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification("error", message.HaveError);
        }
    })
}

function timebase_LoadDataOfTab(apiDomain, access_token, message) {
    var EventBaseIds = [];
    if (!event_timebaseall) {
        EventBaseIds = event_timebase;
    }
    var date = custom_local2utc(moment($('#select-date-from').val()).format('YYYY-MM-DD'), moment($('#select-date-to').val()).format('YYYY-MM-DD'));

    var data = {
        CampaignId: $('#bs-select-campaign').val(),
        StartDate: date.StartDate,
        EndDate: date.EndDate,
        EventBaseIds: EventBaseIds,
    }
    switch (timebase_type) {
        case 1:
            if (event_timebase.length > 0) {


                switch (timebase_tab) {

                    case 1:
                        timebase_CheckLoadSendReport(apiDomain, access_token, message);
                        break;
                    case 2:
                        timebase_CheckLoadOpenChart(apiDomain, access_token, message);
                        break;
                    case 3:
                        timebase_CheckLoadClickChart(apiDomain, access_token, message);
                        break;
                    case 4:
                        if (datasendat != null && datasendat != undefined) {
                            timebase_GenUnsubsChart(datasendat, message);
                        } else {
                            timebase_UnsubcribedChart(apiDomain, access_token, message);
                        }
                        break;
                    case 5:
                        if (datasendat != undefined && datasendat != null) {
                            timebase_GenBounceChart(datasendat, message);
                        } else {
                            timebase_BounceChart(apiDomain, access_token, message);
                        }
                        break;
                    case 6:
                        if (datasendat != undefined && datasendat != null) {

                            timebase_GenComplaintChart(datasendat, message);
                        } else {
                            timebase_ComplaintsChart(apiDomain, access_token, message);
                        }
                        break;
                }
            }
            else {
                timebase_ResetDataOfHead();
                switch (timebase_tab) {
                    case 1:
                        $('#nodata-total').removeClass('hide');
                        $('#hasdata-total-sum').addClass('hide')
                        $('#hasdata-total-chart').addClass('hide')
                        break;
                    case 2:
                        $('#nodata-open').removeClass('hide');
                        $('#chartdiv-open').addClass('hide')
                        if (!$('#div-open-type-of-unique').hasClass('hide')) {
                            $('#div-open-type-of-unique').addClass('hide');
                        }
                        $('.table-show-open').children().remove();
                        break;
                    case 3:
                        $('#nodata-click').removeClass('hide');
                        $('#chartdiv-click').addClass('hide');
                        if (!$('#div-type-of-unique-click').hasClass('hide')) {
                            $('#div-type-of-unique-click').addClass('hide');
                        }
                        $('#show-table-click').children().remove();
                        break;
                    case 4:
                        $('#nodata-unsubcribed').removeClass('hide');
                        $('#chartdiv-unsubcribed').addClass('hide');
                        break;
                    case 5:
                        $('#nodata-bounce').removeClass('hide');
                        $('#chartdiv-bounce').addClass('hide');
                        break;
                    case 6:
                        $('#nodata-complaints').removeClass('hide');
                        $('#chartdiv-complaints').addClass('hide');
                        break;

                }
            }
           

            break;
        case 2:
            timebase_GetReportSMS(apiDomain, access_token, message);
            break;
    }
}

function timebase_LoadDataOfHead(data, message) {

    $('#head-total-sent').text(data.Total);

    auto_sent = data.Sent;

    if (data.Open > 0) {
        $('#head-open').text(data.Open);
    }
    else {
        $('#head-open').text('0');
        $('.table-show-open').children().remove();
    }
    if (data.Click > 0) {
        $('#head-click').text(data.Click);
    }
    else {
        $('#head-click').text('0');
        $('#show-table-click').children().remove();
    }
    if (data.Unsubscribe > 0) {
        $('#head-unsubcribed').text(data.Unsubscribe);
    }
    else {
        $('#head-unsubcribed').text('0');
    }
    if (data.Bounce > 0) {
        $('#head-bounced').text(data.Bounce);
    }
    else {
        $('#head-bounced').text('0');
    }
    if (data.SpamComplaint > 0) {
        $('#head-complaints').text(data.SpamComplaint);
    }
    else {
        $('#head-complaints').text('0');
    }


}

function timebase_ResetDataOfHead() {
    $('#head-total-sent').text(0);

    $('#head-open').text(0);
    $('#head-click').text(0);
    $('#head-unsubcribed').text(0);

    $('#head-bounced').text(0);


    $('#head-complaints').text(0);
}


function timebase_ClickReport(apiDomain, access_token, message) {
    $('#divLoading').show();
    var EventBaseIds = [];
    if (!event_timebaseall) {
        EventBaseIds = event_timebase;
    }

    var date = custom_local2utc(moment($('#select-date-from').val()).format('YYYY-MM-DD'), moment($('#select-date-to').val()).format('YYYY-MM-DD'));
    var timezone = custom_GetTimeZone();
    var data = {
        CampaignId: $('#bs-select-campaign').val(),
        StartDate: date.StartDate,
        EndDate: date.EndDate,
        EventBaseIds: EventBaseIds,
    }

    var data = $.ajax({
        url: apiDomain + 'api/report/Auto/ClickReport',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        async: false,

    }).responseJSON;

    return data;
}

function timebase_LoadSelectLink(apiDomain, access_token, message) {

    if (event_timebase.length > 0) {
        $('#divLoading').show();
        var EventBaseIds = [];
        if (!event_timebaseall) {
            EventBaseIds = event_timebase;
        }

        var date = custom_local2utc(moment($('#select-date-from').val()).format('YYYY-MM-DD'), moment($('#select-date-to').val()).format('YYYY-MM-DD'));
        var timezone = custom_GetTimeZone();
        var data = {
            CampaignId: $('#bs-select-campaign').val(),
            StartDate: date.StartDate,
            EndDate: date.EndDate,
            EventBaseIds: EventBaseIds,
        }

        $.ajax({
            url: apiDomain + 'api/report/Auto/ClickReport',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data),
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
                request.setRequestHeader("timezone", timezone);
            },
            success: function (r) {
                switch (parseInt(r.ErrorCode)) {
                    case 0:  //lấy data thành công

                        var data = r.Data;
                        
                        //gán data vào biến toàn cục
                        datasendclickat = r;

                        $('#div-select-link-click-chart').children().remove();
                        $('#select-link').addClass('hide');
                        if (data.TotalClick > 0) {

                            var links = _.uniq(data.Links);
                            console.log(links);
                            if (links) {
                                $('#div-btn-link-click-chart').removeClass('hide');
                                $('#select-link').removeClass('hide');
                                var option;

                                $.each(links, function (i, o) {
                                    option += '<option value="' + i + '" selected>' + o + '</option>';
                                });
                                $('<select id="select-link-click-chart" multiple="multiple" data-width="100%"></select>').append(option).appendTo('#div-select-link-click-chart');
                                $('#select-link-click-chart').multiselect({
                                    includeSelectAllOption: true,
                                    buttonWidth: '100%',
                                    selectAllValue: 'select-all-value',
                                    selectAllText: message.SelectAll,
                                    allSelectedText: message.SelectedAll,
                                    onInitialized: function (select, container) {
                                        var value = $('#div-select-link-click-chart #select-link-click-chart option:selected');
                                        auto_selectedlink = [];
                                        $.each(value, function (i, o) {
                                            auto_selectedlink.push($(this).text());
                                        });
                                    },
                                    onDropdownHidden: function (event) {
                                        var value = $('#div-select-link-click-chart #select-link-click-chart option:selected');
                                        auto_selectedlink = [];
                                        $.each(value, function (i, o) {
                                            auto_selectedlink.push($(this).text());
                                        });
                                        timebase_CheckLoadClickChart(apiDomain, access_token, message);
                                    }
                                });

                                $('.multiselect-container').css('width', '100%');
                                $('.multiselect .caret').css('display', 'none');


                            } else {

                                $('#div-btn-link-click-chart').addClass('hide');
                                $('#select-link').addClass('hide');

                            }
                        } else {
                            $('#div-btn-link-click-chart').addClass('hide');
                            $('#select-link').addClass('hide');
                        }
                        timebase_CheckLoadClickChart(apiDomain, access_token, message);
                        $('#divLoading').hide();
                        break;
                    default:
                        custom_shownotification('error', message.GetDataError);
                        $('#divLoading').hide();
                        break;
                }
            }, error: function (x, s, e) {
                custom_shownotification('error', message.GetDataError);
                $('#divLoading').hide();
            }
        });

    }

}

function timebase_SelectTab(apiDomain, access_token, message) {
    $('.custom-nav-tabs-statistic li a').on('click', function () {
        timebase_tab = $(this).data('tab');
        //$('#divLoading').show();
        timebase_LoadDataOfTab(apiDomain, access_token, message);

    });

}

function timebase_SwitchChangeTotalChart(apiDomain, access_token, message) {
    $('#tab-total-summary').on('click', function () {
        auto_totaltype = 0;
        timebase_CheckLoadSendReport(apiDomain, access_token, message);
    });
    $('#tab-total-unique').on('click', function () {
        auto_totaltype = 1;
        timebase_CheckLoadSendReport(apiDomain, access_token, message);
    })

}


function timebase_CheckLoadSendReport(apiDomain, access_token, message) {
    if (datasendat != undefined && datasendat != null) {
        timebase_TotalChart(datasendat, message);
    } else {
        timebase_GetSendReport(apiDomain, access_token, message);
    }
}

function timebase_GetSendReport(apiDomain, access_token, message) {
    if (event_timebase.length > 0) {
        var EventBaseIds = [];
        if (!event_timebaseall) {
            EventBaseIds = event_timebase;
        }
        var date = custom_local2utc(moment($('#select-date-from').val()).format('YYYY-MM-DD'), moment($('#select-date-to').val()).format('YYYY-MM-DD'));

        var data = {
            CampaignId: $('#bs-select-campaign').val(),
            StartDate: date.StartDate,
            EndDate: date.EndDate,
            EventBaseIds: EventBaseIds,
        }

        timebase_SendReport(apiDomain, access_token, data, message);
    }
    else {
        timebase_ResetDataOfHead();
    }
}

//function timebase_AjaxSendReport(apiDomain, access_token, data, message)
//{
//    $('#divLoading').show();
//    var timezone = custom_GetTimeZone();
//    var r = $.ajax({
//        url: apiDomain + 'api/report/Auto/SendReport',
//        type: 'POST',
//        contentType: 'application/json; charset=utf-8',
//        data: JSON.stringify(data),
//        beforeSend: function (request) {
//            request.setRequestHeader("Authorization", "Bearer " + access_token);
//            request.setRequestHeader("timezone", timezone);
//        },
//        async: false,

//    }).responseJSON;

//    return r;
//}

function timebase_SendReport(apiDomain, access_token, data, message) {

    $('#divLoading').show();
    var timezone = custom_GetTimeZone();
    var r = $.ajax({
        url: apiDomain + 'api/report/Auto/SendReport',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success: function (r) {

            //gán data vào biến
            datasendat = r;
            timebase_TotalChart(r, message);
            $('#divLoading').hide();

        }, error: function (r) {
            $('#divLoading').hide();
            custom_shownotification('error', message.GetDataError);
        }
    });
};

function timebase_TotalChart(r,message)
{
    switch (parseInt(r.ErrorCode)) {
        case 0:

            var data = r.Data;
            auto_sent = data.Sent;

            if (data.Sent > 0) {
                if (!$('#nodata-total').hasClass('hide')) {
                    $('#nodata-total').addClass('hide');
                }
                if ($('#hasdata-total-sum').hasClass('hide')) {
                    $('#hasdata-total-sum').removeClass('hide')
                }
                if ($('#hasdata-total-chart').hasClass('hide')) {
                    $('#hasdata-total-chart').removeClass('hide')
                }

                var para;
                var valueopen;
                //sumary
                if (auto_totaltype == 0) {

                    var rateopen = data.Open * 100 / data.Sent;
                    $('#sum-rate-open').text(rateopen.toFixed(2) + '%');

                    var rateclick = data.Click * 100 / data.Sent;
                    $('#sum-rate-click').text(rateclick.toFixed(2) + '%');

                    var rateunsub = data.Unsubscribe * 100 / data.Sent;
                    $('#sum-rate-unsub').text(rateunsub.toFixed(2) + '%');

                    var ratebounce = data.Bounce * 100 / data.Total;
                    $('#sum-rate-bounce').text(ratebounce.toFixed(2) + '%');

                    var ratecomplaint = data.SpamComplaint * 100 / data.Sent;
                    $('#sum-rate-complaint').text(ratecomplaint.toFixed(2) + '%');

                    valueopen = data.Open;
                    if (parseInt(data.Sent) - parseInt(data.Open) > 0) {
                        para = parseInt(data.Sent) - parseInt(data.Open);

                    }
                    else {
                        para = 0;
                    }
                }
                    //unique
                else if (auto_totaltype == 1) {

                    var rateopen = data.UniqueOpen * 100 / data.Sent;
                    $('#sum-rate-open').text(rateopen.toFixed(2) + '%');

                    var rateclick = data.UniqueClick * 100 / data.Sent;
                    $('#sum-rate-click').text(rateclick.toFixed(2) + '%');

                    var rateunsub = data.Unsubscribe * 100 / data.Sent;
                    $('#sum-rate-unsub').text(rateunsub.toFixed(2) + '%');

                    var ratebounce = data.Bounce * 100 / data.Sent;
                    $('#sum-rate-bounce').text(ratebounce.toFixed(2) + '%');

                    var ratecomplaint = data.SpamComplaint * 100 / data.Sent;
                    $('#sum-rate-complaint').text(ratecomplaint.toFixed(2) + '%');

                    valueopen = data.UniqueOpen;
                    if (parseInt(data.Sent) - parseInt(data.UniqueOpen) > 0) {
                        para = parseInt(data.Sent) - parseInt(data.UniqueOpen);
                    }
                    else {
                        para = 0;
                    }
                }

                var title = message.TotalSent;


                // Make monochrome colors and set them as default for all pies
                Highcharts.getOptions().plotOptions.pie.colors = (function () {
                    var colors = ["rgb(99, 181, 18)", "rgb(204, 204, 204)"],
                        base = Highcharts.getOptions().colors[0],
                        i;

                    for (i = 0; i < 10; i += 1) {
                        // Start out with a darkened base color (negative brighten), and end
                        // up with a much brighter color
                        colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
                    }
                    return colors;
                }());

                // Build the chart
                $('#chartdiv-total').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: title + ': ' + data.Sent
                    },
                    tooltip: { enabled: false },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: '',
                        data: [
                            { name: 'Open', y: valueopen },
                            { name: '', y: para },

                        ]
                    }]
                });

                setTimeout(function () {
                    var chart = $('#chartdiv-total').highcharts();

                    console.log('redraw');
                    var w = $('#chartdiv-total').innerWidth();
                    var h = $('#chartdiv-total').innerHeight();
                    // setsize will trigger the graph redraw 
                    chart.setSize(
                        w, h, false
                    );
                }, 10);

            }
            else {

                // $('#head-total-sent').text('0');

                $('#nodata-total').removeClass('hide');
                if (!$('#hasdata-total-sum').hasClass('hide')) {
                    $('#hasdata-total-sum').addClass('hide')
                }
                if (!$('#hasdata-total-chart').hasClass('hide')) {
                    $('#hasdata-total-chart').addClass('hide')
                }

            }

            timebase_LoadDataOfHead(data, message);

            $('#divLoading').hide();
            break;
        default: $('#divLoading').hide();
            custom_shownotification('error', message.GetDataError);
            $('#nodata-total').removeClass('hide');
            $('#nodata-total h1').text('Lấy thông tin không thành công');
            if (!$('#hasdata-total-sum').hasClass('hide')) {
                $('#hasdata-total-sum').addClass('hide')
            }
            if (!$('#hasdata-total-chart').hasClass('hide')) {
                $('#hasdata-total-chart').addClass('hide')
            }
            break;
    }
  
}

function timebase_SelectTypeOpen(apiDomain, access_token, message) {
    $('#tab-open-opened').on('click', function () {
        auto_opentype = 1;
        if (!$('#div-open-type-of-unique').hasClass('hide')) {
            $('#div-open-type-of-unique').addClass('hide');
        }
        timebase_CheckLoadOpenChart(apiDomain, access_token, message);
    })
    $('#tab-open-uniqueopened').on('click', function () {
     
        auto_opentype = 2;
        timebase_CheckLoadOpenChart(apiDomain, access_token, message);
    })
    $('#tab-open-unopened').on('click', function () {
        auto_opentype = 3;
        if (!$('#div-open-type-of-unique').hasClass('hide')) {
            $('#div-open-type-of-unique').addClass('hide');
        }
        timebase_CheckLoadOpenChart(apiDomain, access_token, message);
    })
}

function timebase_ChangeTypeUniqueOpen(apiDomain, access_token, message) {
    $('#tab-open-unique-summary').on('click', function () {
        auto_openuniquetype = 0;
        timebase_CheckLoadOpenChart(apiDomain, access_token, message);
    });
    $('#tab-open-unique-hour').on('click', function () {
        auto_openuniquetype = 1;
        timebase_CheckLoadOpenChart(apiDomain, access_token, message);
    });
}

function timebase_CheckLoadOpenChart(apiDomain, access_token, message) {
    if (datasendopenat != undefined && datasendopenat != null) {
        timebase_GenDataOpenChart(datasendopenat, message);
    } else {
        timebase_OpenChart(apiDomain, access_token, message);
    }
}

function timebase_OpenChart(apiDomain, access_token, message) {
    if (event_timebase.length > 0) {
        var EventBaseIds = [];
        if (!event_timebaseall) {
            EventBaseIds = event_timebase;
        };
        var date = custom_local2utc(moment($('#select-date-from').val()).format('YYYY-MM-DD'), moment($('#select-date-to').val()).format('YYYY-MM-DD'));

        var data = {
            CampaignId: $('#bs-select-campaign').val(),
            StartDate: date.StartDate,
            EndDate: date.EndDate,
            EventBaseIds: EventBaseIds,
        }
        timebase_ActionOpenChart(apiDomain, access_token, data, message)
    }
    else
    {
        $('#chartdiv-open').addClass('hide');
        $('#nodata-open').removeClass('hide');
        $('.table-show-open').children().remove();
    }

}

function timebase_ActionOpenChart(apiDomain, access_token, data, message)
{
    $('#divLoading').show();
    var timezone = custom_GetTimeZone();
    $.ajax({
        url: apiDomain + 'api/report/Auto/OpenReport',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success: function (r) {
            datasendopenat = r;
            //gen data chart
            timebase_GenDataOpenChart(r, message);
            $('#divLoading').hide();
        },
        error: function () {
            custom_shownotification('error', message.GetDataError)
            $('#divLoading').hide();
        }
    });
}

function timebase_GenDataOpenChart(r,message){
    if (event_timebase.length > 0) {
        switch (parseInt(r.ErrorCode)) {
            case 0:  //lấy data thành công


                if (JSON.parse(JSON.stringify(r.Data)).TotalOpen == 0) {
                    $('#nodata-open').removeClass('hide');
                    $('#chartdiv-open').addClass('hide');

                    if (!$('#div-open-type-of-unique').hasClass('hide')) {
                        $('#div-open-type-of-unique').addClass('hide');
                    }

                    $('.table-show-open').children().remove();
                }
                else {
                    if (!$('#nodata-open').hasClass('hide')) {
                        $('#nodata-open').addClass('hide');
                    }
                    if ($('#chartdiv-open').hasClass('hide')) {
                        $('#chartdiv-open').removeClass('hide');
                    }

                    //setheader opend 
                    $('#head-open').text(r.Data.TotalOpen);
                    var data = JSON.parse(JSON.stringify(r.Data)).Events;
                    bevent = new Array();
                    event = new Array();

                    //var StartDate = $('#select-date-from').val(); //$('#bs-select-campaign option:selected').data('subtext'),
                    //var EndDate = $('#select-date-to').val();
                    var day = (moment(data[data.length - 1].TimeStamp).diff(data[0].TimeStamp, 'days'));

                    // opened----------------------
                    if (auto_opentype == 1) {
                        $.each(data, function (i, o) {
                            bevent.push({ 'value': '1', 'date': moment(o.TimeStamp).format('YYYY-MM-DD') });

                        });
                        var a = _(bevent).groupBy(function (o) {
                            return o.date;
                        })

                        for (i = 0; i <= day; i++) {
                            var value = 0;
                            var startdate = new Date(data[0].TimeStamp);
                            var dateofevent = moment(startdate.setDate(startdate.getDate() + i)).format('YYYY-MM-DD');
                            var e1 = { 'value': 0, 'date': dateofevent }
                            $.each(a, function (i, o) {
                                if (moment(dateofevent).format('YYYY-MM-DD') == moment(i).format('YYYY-MM-DD')) {
                                    e1.value = o.length
                                }
                            });
                            event.push(e1);

                        }

                        //chart
                        ChartOpen(event);
                        //table show data
                        timebase_TableOpenTotal(r.Data);
                    }

                    //unique opened---------------------
                    if (auto_opentype == 2) {

                        //show nút chọn summary or hour
                        if ($('#div-open-type-of-unique').hasClass('hide')) {
                            $('#div-open-type-of-unique').removeClass('hide');
                        }

                        //summary 
                        
                        if (auto_openuniquetype == 0) {
                            var listunique2 = data.uniqueobj();

                            var groups = _.groupBy(listunique2, function (value) {
                                return moment(value.TimeStamp).format('YYYY-MM-DD');
                            });

                            var bevent = _.map(groups, function (group) {
                                return {
                                    Date: moment(group[0].TimeStamp).format('YYYY-MM-DD'),
                                    Count: _.pluck(group, 'ContactId')
                                }
                            });
                            //   console.log('b:'+ JSON.stringify( bevent));

                            for (i = 0; i <= day; i++) {
                                var value = 0;
                                var startdate = new Date(data[0].TimeStamp);
                                var dateofevent = moment(startdate.setDate(startdate.getDate() + i)).format('YYYY-MM-DD');
                                var e2 = { 'value': 0, 'date': dateofevent }
                                $.each(bevent, function (i, o) {
                                    if (moment(dateofevent).format('YYYY-MM-DD') == moment(o.Date).format('YYYY-MM-DD')) {
                                        var a = o.Count;

                                        e2.value = a.length;
                                    }
                                });
                                event.push(e2);
                            }

                            //chart
                            ChartOpen(event);
                            //table
                            timebase_TableOpenTotal(r.Data);
                        }
                            //hour
                        else if (auto_openuniquetype == 1) {

                            var hour = [00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
                            var listunique = data.uniqueobj();

                            //   console.log('gg' + JSON.stringify( listunique));

                            var groups = _.groupBy(listunique, function (value) {
                                var h = new Date(value.TimeStamp);
                                return h //moment(new Date(value.TimeStamp)).format('hh');
                            });

                            //   console.log('g' +JSON.stringify( groups));

                            var bevent = _.map(groups, function (group) {
                                return {
                                    Hour: new Date(group[0].TimeStamp).getHours(),//moment(new Date(group[0].TimeStamp)).format('hh'),
                                    Count: _.pluck(group, 'ContactId')
                                }
                            });
                            //    console.log('g2:'+ JSON.stringify( bevent));

                            for (i = 0; i < hour.length; i++) {
                                var value = 0;

                                var e2 = { 'value': 0, 'hour': hour[i] }
                                $.each(bevent, function (idx, o) {
                                    if (parseInt(hour[i]) == parseInt(o.Hour)) {
                                        value += o.Count.length;


                                    }
                                });
                                e2.value = value;
                                event.push(e2);

                            }
                            //     console.log(event);
                            //char
                            CharOpenUniqueHour(event);
                            //table
                            timebase_TableUniqueOpenHour(event);
                        }


                    }

                    //noopen-------------------------------------
                    if (auto_opentype == 3) {

                        //list unique
                        var listunique3 = data.uniqueobj();



                        var groups = _.groupBy(listunique3, function (value) {
                            return moment(value.TimeStamp).format('YYYY-MM-DD');
                        });

                        var bevent = _.map(groups, function (group) {
                            return {
                                Date: moment(group[0].TimeStamp).format('YYYY-MM-DD'),
                                Count: _.pluck(group, 'ContactId')
                            }
                        });

                        //tạo list unopen
                        temple = 0;
                        var total = auto_sent;

                        for (i = 0; i <= day; i++) {
                            var startdate = new Date(data[0].TimeStamp);
                            var dateofevent = moment(startdate.setDate(startdate.getDate() + i)).format('YYYY-MM-DD');


                            var e3 = { 'date': dateofevent, 'value': total - temple };

                            $.each(bevent, function (i, o) {
                                if (dateofevent == moment(o.Date).format('YYYY-MM-DD')) {

                                    e3.value = total - o.Count.length - temple;
                                    temple = total - e3.value;
                                }
                            })

                            event.push(e3);
                        }

                        //chart
                        ChartOpen(event);
                        //table
                        timebase_TableOpenTotal(r.Data);
                    }
                }

                $('#divLoading').hide();
                break;
            default:
                custom_shownotification('error', message.GetDataError)
                $('#divLoading').hide();
                break;
        }
       
    }
    else {
        $('#chartdiv-open').addClass('hide');
        $('#nodata-open').removeClass('hide');
        $('.table-show-open').children().remove();
    }
}

function ChartOpen(event) {
    var chart = AmCharts.makeChart("chartdiv-open", {
        "type": "serial",
        "theme": "light",
        "marginRight": 40,
        "marginLeft": 40,
        "autoMarginOffset": 20,
        "mouseWheelZoomEnabled": false,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id": "v1",
            "axisAlpha": 0,
            "position": "left",
            "ignoreAxisWidth": true
        }],
        "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0
        },
        "graphs": [{
            "id": "g1",
            "balloon": {
                "drop": true,
                "adjustBorderColor": false,
                "color": "#ffffff"
            },
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "title": "red line",
            "useLineColorForBulletBorder": true,
            "valueField": "value",
            "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
        }],
        "chartScrollbar": {
            "graph": "g1",
            "oppositeAxis": false,
            "offset": 30,
            "scrollbarHeight": 30,
            "backgroundAlpha": 0,
            "selectedBackgroundAlpha": 0.1,
            "selectedBackgroundColor": "#888888",
            "graphFillAlpha": 0,
            "graphLineAlpha": 0.5,
            "selectedGraphFillAlpha": 0,
            "selectedGraphLineAlpha": 1,
            "autoGridCount": true,
            "color": "#AAAAAA"
        },
        "chartCursor": {
            "pan": false,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "cursorAlpha": 1,
            "cursorColor": "#258cbb",
            "limitToGraph": "g1",
            "valueLineAlpha": 0.2,
            "valueZoomable": false,

        },
        "valueScrollbar": {
            "oppositeAxis": false,
            "offset": 50,
            "scrollbarHeight": 10
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true
        },
        "export": {
            "enabled": true
        },
        "dataProvider": event
    });

    chart.addListener("rendered", zoomChart);

    zoomChart();

    function zoomChart() {
        chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
    }
}
function CharOpenUniqueHour(event) {
    var chart = AmCharts.makeChart("chartdiv-open", {
        "theme": "light",
        "type": "serial",
        "dataProvider": event,
        "valueAxes": [{
            "title": "",
           
        }],
        "graphs": [{
            "balloonText": "Xem lúc [[category]]: Số lượng [[value]]",
            "fillAlphas": 1,
            "lineAlpha": 0.2,
            "title": "Income",
            "type": "column",
            "valueField": "value"
        }],
        "depth3D": 0,
        "angle": 0,
        "rotate": true,
        "categoryField": "hour",
        "categoryAxis": {
           
            "gridPosition": "start",
            "fillAlpha": 0.05,
            "position": "left"
        },
        "export": {
            "enabled": true
        }
    });

}


function timebase_TableOpenTotal(data) {
    $('.table-show-open').children().remove();
    var html = "";
    var dt = JSON.parse(JSON.stringify(data));
    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">' +
'<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-center font-green-jungle"><h4><b>' + $('#bs-select-campaign option:selected').text() + '</b></h4> </div>' +
'<div class="divTableCell text-center"> <h4>Action</h4></div>' +
'</div>' +
'<div class="divTableRow">' +
'<div class="divTableCell text-center"><h4>' + dt.TotalOpen + ' Total opened</h4> </div>' +
'<div class="divTableCell text-center"><h4><a id="show-opened-contact"><span class="icon-users"></span> Show contacts</a><h4></div>' +
'</div>' +
'<div class="divTableRow">' +
'<div class="divTableCell text-center"><h4>' + dt.TotalUniqueOpen + ' Unique opened </h4> </div>' +
'<div class="divTableCell text-center"><h4><a id="show-uniqueopened-contact"><span class="icon-users"></span> Show contacts</a><h4></div>' +
'</div>' +
'<div class="divTableRow">' +
'<div class="divTableCell text-center"><h4>' + (auto_sent - dt.TotalUniqueOpen) + ' Not opened </h4> </div>' +
'<div class="divTableCell text-center">&nbsp;</div>' +
'</div>' +
'</div>' +
'</div>'

    $('.table-show-open').append(html);

    $('.table-show-open #show-opened-contact').on('click', function () {
        $('#modal-show-contact #table-show-contact').children().remove();
        timebase_ShowOpenContact(data)
    });

    $('.table-show-open #show-uniqueopened-contact').on('click', function () {
        $('#modal-show-contact #table-show-contact').children().remove();
        timebase_ShowUniqueOpenContact(data)
    });

}

function timebase_ShowOpenContact(data) {
    var event = data.Events;
    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">' +
        '<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-left"><h4 class="nomargin">Email</h4> </div>' +
'<div class="divTableCell text-center"> <h4 class="nomargin">Date</h4></div>' +
'</div>';
    $.each(event, function (i, o) {
        html += '<div class="divTableRow">' +
'<div class="divTableCell text-center"><span>' + o.Email + '</span> </div>' +
'<div class="divTableCell text-center"><span>' +moment(moment.utc(o.TimeStamp).toDate()).format('YYYY-MM-DD HH:mm:ss')+ '</span> </div>' +
'</div>'
    });
    html += '</div>' + '</div>'
    $('#table-show-contact').append(html);
    $('#modal-show-contact').modal('toggle');
    data_advance_export=event;
}

function timebase_ShowUniqueOpenContact(data) {
    var event = data.Events.uniqueobj();
    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">' +
        '<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-left"><h4 class="nomargin">Email</h4> </div>' +
'<div class="divTableCell text-center"> <h4 class="nomargin">Date</h4></div>' +
'</div>';
    $.each(event, function (i, o) {
        html += '<div class="divTableRow">' +
'<div class="divTableCell text-center"><span>' + o.Email + '</span> </div>' +
'<div class="divTableCell text-center"><span>' + moment(moment.utc(o.TimeStamp).toDate()).format('YYYY-MM-DD HH:mm:ss') + '</span> </div>' +
'</div>'
    });
    html += '</div>' + '</div>'
    $('#table-show-contact').append(html);
    $('#modal-show-contact').modal('toggle');
    data_advance_export=event;
}

function timebase_TableUniqueOpenHour(event) {
    $('.table-show-open').children().remove();
    var html = "";

    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">' +
'<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-center font-green-jungle"><h4>Hours</h4> </div>' +
'<div class="divTableCell text-center"> <h4>Opens</h4></div>' +
'</div>'
    $.each(event, function (i, o) {
       
        html += '<div class="divTableRow">' +
'<div class="divTableCell text-center"><h4>' + o.hour + '</h4> </div>' +
'<div class="divTableCell text-center"><h4>' + o.value + '<h4></div>' +
'</div>'
    });

    html += '</div>' + '</div>'
    $('.table-show-open').append(html);


}



//-------------------click

function timebase_ChangeTypeClick(apiDomain, access_token, message) {
    $('#tab-click-summary').on('click', function () {
        auto_clicktype = 0;
        timebase_CheckLoadClickChart(apiDomain, access_token, message)
    });
    $('#tab-click-unique').on('click', function () {
        auto_clicktype = 1;
        timebase_CheckLoadClickChart(apiDomain, access_token, message);
    })
}

function timebase_ChangeTypeUniqueClick(apiDomain, access_token, message) {
  
    $('#tab-click-unique-summary').on('click', function () {
        auto_clickuniquetype = 0;
        timebase_CheckLoadClickChart(apiDomain, access_token, message);
    });
    $('#tab-click-unique-hour').on('click', function () {
        auto_clickuniquetype = 1;
        timebase_CheckLoadClickChart(apiDomain, access_token, message);
    })
}

function timebase_CheckLoadClickChart(apiDomain, access_token, message)
{
    if (datasendclickat != undefined && datasendclickat != null) {
        timebase_GenDataClickChart(datasendclickat, message);
    } else {
        timebase_ClickChart(apiDomain, access_token, message);
    }
}

function timebase_ClickChart(apiDomain, access_token, message) {
    if (event_timebase.length > 0) {
        $('#divLoading').show();
        var EventBaseIds = [];
        if (!event_timebaseall) {
            EventBaseIds = event_timebase;
        }

        var date = custom_local2utc(moment($('#select-date-from').val()).format('YYYY-MM-DD'), moment($('#select-date-to').val()).format('YYYY-MM-DD'));
        var timezone = custom_GetTimeZone();
        var data = {
            CampaignId: $('#bs-select-campaign').val(),
            StartDate: date.StartDate,
            EndDate: date.EndDate,
            EventBaseIds: EventBaseIds,
        }

        $.ajax({
            url: apiDomain + 'api/report/Auto/ClickReport',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data),
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
                request.setRequestHeader("timezone", timezone);
            },
            success: function (r) {
                datasendclickat = r;

                //gen data chart
                timebase_GenDataClickChart(r, message);
                $('#divLoading').hide();

            }, error: function (x, s, e) {
                custom_shownotification('error', message.GetDataError)
                $('#divLoading').hide();
            }
        });
    }
    else {

        $('#nodata-click').removeClass('hide');

        $('#chartdiv-click').addClass('hide');
    }
}

function timebase_GenDataClickChart(r, message) {
    if (event_timebase.length > 0) {
        switch (parseInt(r.ErrorCode)) {
            case 0:  //lấy data thành công
                var object = r.Data;
                if (object.TotalClick == 0) {
                    if ($('#nodata-click').hasClass('hide')) {
                        $('#nodata-click').removeClass('hide');
                    }
                    if (!$('#chartdiv-click').hasClass('hide')) {
                        $('#chartdiv-click').addClass('hide')
                    }

                    $('#show-table-click').children().remove();
                }
                else {
                    if (!$('#nodata-click').hasClass('hide')) {
                        $('#nodata-click').addClass('hide');
                    }
                    if ($('#chartdiv-click').hasClass('hide')) {
                        $('#chartdiv-click').removeClass('hide')
                    }

                    //setheader click
                    $('#head-click').text(object.TotalClick);

                    var bdata = object.Events;

                    var data = [];
                    for (var j = 0; j < bdata.length; j++) {
                        for (var i = 0 ; i < auto_selectedlink.length; i++) {
                            if ((auto_selectedlink[i].toLowerCase().localeCompare(bdata[j].Link.toLowerCase())) == 0) {
                                data.push(bdata[j]);
                            }
                        }
                    }

                    //console.log('l' + auto_selectedlink);
                    //  console.log('c' + JSON.stringify(data));
                    var bevent = new Array();
                    var event = new Array();

                    //var StartDate = $('#select-date-from').val();
                    //var EndDate = $('#select-date-to').val();
                    var day = (moment(data[data.length-1].TimeStamp).diff(data[0].TimeStamp, 'days'));

                    //clicked
                    if (auto_clicktype == 0) {

                        if (!$('#div-type-of-unique-click').hasClass('hide')) {
                            $('#div-type-of-unique-click').addClass('hide')
                        }


                        var a = _(data).groupBy(function (o) {
                            return moment(o.TimeStamp).format('YYYY-MM-DD');
                        })
                        //   console.log('c1' + JSON.stringify(a));
                        for (i = 0; i <= day; i++) {
                            var value = 0;
                            var startdate = new Date(data[0].TimeStamp);
                            var dateofevent = moment(startdate.setDate(startdate.getDate() + i)).format('YYYY-MM-DD');
                            var e = { 'value': 0, 'date': dateofevent }
                            $.each(a, function (i, o) {
                                if (moment(dateofevent).format('YYYY-MM-DD') == i) {
                                    e.value = o.length
                                }
                            });
                            event.push(e);


                        }
                        //chart
                        ChartClick(event)
                        //table show data
                        timebase_TableClick(object);

                    }
                        //uniqueclick
                    else if (auto_clicktype == 1) {

                        
                        $('#div-type-of-unique-click').removeClass('hide')
                       
                        //summary
                        if (auto_clickuniquetype == 0) {

                            

                            var uniquedata = data.uniqueobj();
                            var a = _(uniquedata).groupBy(function (o) {
                                return moment(o.TimeStamp).format('YYYY-MM-DD');
                            })
                            //      console.log('c2' + JSON.stringify(a));
                            for (i = 0; i <= day; i++) {
                                var value = 0;
                                var startdate = new Date(data[0].TimeStamp);
                                var dateofevent = moment(startdate.setDate(startdate.getDate() + i)).format('YYYY-MM-DD');
                                var e1 = { 'value': 0, 'date': dateofevent }
                                $.each(a, function (i, o) {
                                    if (moment(dateofevent).format('YYYY-MM-DD') == i) {
                                        e1.value = o.length
                                    }
                                });
                                event.push(e1);


                            }
                            //chart
                            ChartClick(event)
                            //table show data
                            timebase_TableUniqueClick(object);
                        }
                            //hour
                        else if (auto_clickuniquetype == 1) {
                            var hour = [00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
                            var listunique = data.uniqueobj();

                            var groups = _.groupBy(listunique, function (value) {
                                var h = new Date(value.TimeStamp);
                                return h //moment(new Date(value.TimeStamp)).format('hh');
                            });

                            //       console.log('g' + JSON.stringify(groups));

                            var bevent = _.map(groups, function (group) {

                                return {
                                    Hour: new Date(group[0].TimeStamp).getHours(),//moment(new Date(group[0].TimeStamp)).format('hh'),
                                    Count: _.pluck(group, 'ContactId')
                                }
                            });
                            //        console.log('g2:' + JSON.stringify(bevent));

                            for (i = 0; i < hour.length; i++) {
                                var value = 0;

                                var e2 = { 'value': 0, 'hour': hour[i] }
                                $.each(bevent, function (idx, o) {
                                    if (parseInt(hour[i]) == parseInt(o.Hour)) {
                                        value += o.Count.length;


                                    }
                                });
                                e2.value = value;
                                event.push(e2);

                            }
                            //          console.log(event);
                            //char
                            CharClickUniqueHour(event);
                            //table
                            timebase_TableUniqueClickHour(event);
                        }


                    }
                }

                $('#divLoading').hide();
                break;
            default: break;
                custom_shownotification('error', message.GetDataError)
                $('#divLoading').hide();
        }
      
    } else {

        $('#nodata-click').removeClass('hide');

        $('#chartdiv-click').addClass('hide');
    }
}

function ChartClick(event) {
    var chart = AmCharts.makeChart("chartdiv-click", {
        "type": "serial",
        "theme": "light",
        "marginRight": 40,
        "marginLeft": 40,
        "autoMarginOffset": 20,
        "mouseWheelZoomEnabled": false,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id": "v1",
            "axisAlpha": 0,
            "position": "left",
            "ignoreAxisWidth": true
        }],
        "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0
        },
        "graphs": [{
            "id": "g1",
            "balloon": {
                "drop": true,
                "adjustBorderColor": false,
                "color": "#ffffff"
            },
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "title": "red line",
            "useLineColorForBulletBorder": true,
            "valueField": "value",
            "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
        }],
        "chartScrollbar": {
            "graph": "g1",
            "oppositeAxis": false,
            "offset": 30,
            "scrollbarHeight": 30,
            "backgroundAlpha": 0,
            "selectedBackgroundAlpha": 0.1,
            "selectedBackgroundColor": "#888888",
            "graphFillAlpha": 0,
            "graphLineAlpha": 0.5,
            "selectedGraphFillAlpha": 0,
            "selectedGraphLineAlpha": 1,
            "autoGridCount": true,
            "color": "#AAAAAA"
        },
        "chartCursor": {
            "pan": true,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "cursorAlpha": 1,
            "cursorColor": "#258cbb",
            "limitToGraph": "g1",
            "valueLineAlpha": 0.2,
            "valueZoomable": true
        },
        "valueScrollbar": {
            "oppositeAxis": false,
            "offset": 50,
            "scrollbarHeight": 10
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true
        },
        "export": {
            "enabled": true
        },
        "dataProvider": event
    });

    chart.addListener("rendered", zoomChart);

    zoomChart();

    function zoomChart() {
        chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
    }
}

function CharClickUniqueHour(event) {
    var chart = AmCharts.makeChart("chartdiv-click", {
        "theme": "light",
        "type": "serial",
        "dataProvider": event,
        "valueAxes": [{
            "title": ""
        }],
        "graphs": [{
            "balloonText": "Xem lúc [[category]] h : Số lượng [[value]]",
            "fillAlphas": 1,
            "lineAlpha": 0.2,
            "title": "Income",
            "type": "column",
            "valueField": "value"
        }],
        "depth3D": 0,
        "angle": 0,
        "rotate": true,
        "categoryField": "hour",
        "categoryAxis": {
            "gridPosition": "start",
            "fillAlpha": 0.05,
            "position": "left"
        },
        "export": {
            "enabled": true
        }
    });

}


function timebase_TableClick(data) {
    $('#show-table-click').children().remove();
    var html = "";
    var dt = _(data.Events).groupBy(function (o) {
        return o.Link.toLowerCase();
    });
    // console.log('table:' + JSON.stringify(dt));

    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">' +
'<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-center font-green-jungle"><h4><b>' + $('#bs-select-campaign option:selected').text() + '</b></h4> </div>' +
'<div class="divTableCell text-center"> <h4>Action</h4></div>' +
'</div>'

    $.each(dt, function (i, o) {
        html += '<div class="divTableRow">' +
'<div class="divTableCell text-left"><h4>' + o.length + ' Click on</h4><h4 id="linkontable">' + i + '</h4></div>' +
'<div class="divTableCell text-center"><h4><a id="show-click-contact"><span class="icon-users"></span> Show contacts</a><h4></div>' +
'</div>'

    });
    html += '</div>' + '</div>';

    $('#show-table-click').append(html);


    $('#show-table-click #show-click-contact').on('click', function () {
        var link = $('#show-table-click').children().find('#linkontable').text();
        $('#modal-show-contact #table-show-contact').children().remove();
        timebase_ShowClickContact(data, link);
    });

}

function timebase_TableUniqueClick(data) {
    $('#show-table-click').children().remove();
    var html = "";
    var dt = _(data.Events).groupBy(function (o) {
        return o.Link.toLowerCase();
    });
    //  console.log('table:' + JSON.stringify(dt));

    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">' +
'<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-center font-green-jungle"><h4><b>' + $('#bs-select-campaign option:selected').text() + '</b></h4> </div>' +
'<div class="divTableCell text-center"> <h4>Action</h4></div>' +
'</div>'

    $.each(dt, function (i, o) {
        html += '<div class="divTableRow">' +
'<div class="divTableCell text-left"><h4>' + o.uniqueobj().length + ' Click on</h4><h4 id="linkuniqueontable">' + i + '</h4></div>' +
'<div class="divTableCell text-center"><h4><a id="show-click-unique-contact"><span class="icon-users"></span> Show contacts</a><h4></div>' +
'</div>'

    });
    html += '</div>' + '</div>';

    $('#show-table-click').append(html);


    $('#show-table-click #show-click-unique-contact').on('click', function () {
        var link = $('#show-table-click').children().find('#linkuniqueontable').text();
        $('#modal-show-contact #table-show-contact').children().remove();
        timebase_ShowClickUniqueContact(data, link);
    });

}

function timebase_ShowClickContact(data, link) {
    var html = "";
    var contact = new Array();
    $.each(data.Events, function (i, o) {
        if ((o.Link.toLowerCase().localeCompare(link)) == 0) {
            contact.push(o);
        }
    });

    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">' +
        '<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-left"><h4 class="nomargin">Email</h4> </div>' +
'<div class="divTableCell text-center"> <h4 class="nomargin">Date</h4></div>' +
'</div>';
    $.each(contact, function (i, o) {
        html += '<div class="divTableRow">' +
'<div class="divTableCell text-center"><span>' + o.Email + '</span> </div>' +
'<div class="divTableCell text-center"><span>' + moment(moment.utc(o.TimeStamp).toDate()).format('YYYY-MM-DD HH:mm:ss') + '</span> </div>' +
'</div>'
    });
    html += '</div>' + '</div>'
    $('#table-show-contact').append(html);
    $('#modal-show-contact').modal('toggle');
    data_advance_export=contact;
}

function timebase_ShowClickUniqueContact(data, link) {
    var html = "";
    var contact = new Array();
    $.each(data.Events, function (i, o) {
        if ((o.Link.toLowerCase().localeCompare(link)) == 0) {
            contact.push(o);
        }
    });

    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">' +
        '<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-left"><h4 class="nomargin">Email</h4> </div>' +
'<div class="divTableCell text-center"> <h4 class="nomargin">Date</h4></div>' +
'</div>';
    var uniquecontact = contact.uniqueobj();
    $.each(uniquecontact, function (i, o) {
        html += '<div class="divTableRow">' +
'<div class="divTableCell text-center"><span>' + o.Email + '</span> </div>' +
'<div class="divTableCell text-center"><span>' + moment(moment.utc(o.TimeStamp).toDate()).format('YYYY-MM-DD HH:mm:ss') + '</span> </div>' +
'</div>'
    });
    html += '</div>' + '</div>'
    $('#table-show-contact').append(html);
    $('#modal-show-contact').modal('toggle');
    data_advance_export=uniquecontact;
}

function timebase_TableUniqueClickHour(event) {
    $('#show-table-click').children().remove();
    var html = "";

    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">' +
'<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-center font-green-jungle"><h4>Hours</h4> </div>' +
'<div class="divTableCell text-center"> <h4>Clicks</h4></div>' +
'</div>';
    $.each(event, function (i, o) {
        html += '<div class="divTableRow">' +
'<div class="divTableCell text-center"><h4>' + o.hour + '</h4> </div>' +
'<div class="divTableCell text-center"><h4>' + o.value + '<h4></div>' +
'</div>';
    });

    html += '</div>' + '</div>'
    $('#show-table-click').append(html);


}


var data_advance_export=[];
function timebase_ExportContact() {
    $('#export-advance-statistic').on('click', function () {
        $('#divLoading').show();
        var list = [];
        var head = ["Email", "Date"];
        list.push(head);
        $.each(data_advance_export, function (i, o) {
            var obj = [];
            obj.push(o.Email);
            obj.push(moment(moment.utc(o.TimeStamp).toDate()).format('YYYY-MM-DD HH:mm:ss'));
            list.push(obj);
        })
        write_exportContact(list);
    });
}


//-----------end click------------------

//-----------bounce---------------------
function timebase_BounceChart(apiDomain, access_token, message) {
    var EventBaseIds = [];
    if (!event_timebaseall) {
        EventBaseIds = event_timebase;
    }
    var date = custom_local2utc(moment($('#select-date-from').val()).format('YYYY-MM-DD'), moment($('#select-date-to').val()).format('YYYY-MM-DD'));

    var data = {
        CampaignId: $('#bs-select-campaign').val(),
        StartDate: date.StartDate,
        EndDate: date.EndDate,
        EventBaseIds: EventBaseIds,
    }
    $('#divLoading').show();
    var timezone = custom_GetTimeZone();
    var r = $.ajax({
        url: apiDomain + 'api/report/Auto/SendReport',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success: function (r) {
            datasendat = r;
            timebase_GenBounceChart(r, message)
        },
        error: function () {
            $('#divLoading').hide();
        }
    });

}

function timebase_GenBounceChart(r, message) {
    switch (r.ErrorCode) {
        case 0:
            var data = r.Data;
            if (parseInt(data.Bounce) > 0) {

                if (!$('#nodata-bounce').hasClass('hide')) {
                    $('#nodata-bounce').addClass('hide');
                }
                if ($('#chartdiv-bounce').hasClass('hide')) {
                    $('#chartdiv-bounce').removeClass('hide')
                }
                if ($('#total-bounce').hasClass('hide')) {
                    $('#total-bounce').removeClass('hide')
                }

                $('#total-bounce').text(data.Bounce);
                $('#head-bounced').text(data.Unsubscribe);
                //var bevent = _(data.Reasons).groupBy(function (o) {
                //    return o;
                //})

                var title = message.TotalBounce;

                Highcharts.getOptions().plotOptions.pie.colors = (function () {
                    var colors = ["#FF0000", "rgb(204, 204, 204)"],
                        base = Highcharts.getOptions().colors[0],
                        i;

                    for (i = 0; i < 5; i += 1) {
                        // Start out with a darkened base color (negative brighten), and end
                        // up with a much brighter color
                        colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
                    }
                    return colors;
                }());
                // Build the chart
                $('#chartdiv-bounce').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: title + ': ' + data.Bounce
                    },
                    tooltip: {
                        pointFormat: '<b>SL: {point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false,
                                format: '<b>{point.name}</b>:  {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: '',
                        data: [{ name: 'Unsubcribed', y: data.Bounce }, { name: 'Khác', y: data.Total - data.Bounce }, ]
                    }]

                });
                setTimeout(function () {
                    var chart = $('#chartdiv-bounce').highcharts();

                    console.log('redraw');
                    var w = $('#chartdiv-bounce').innerWidth();
                    var h = $('#chartdiv-bounce').innerHeight();
                    // setsize will trigger the graph redraw 
                    chart.setSize(
                        w, h, false
                    );
                }, 10);
            }
            else {
              
                if ($('#nodata-bounce').hasClass('hide')) {
                    $('#nodata-bounce').removeClass('hide');
                }
                if (!$('#chartdiv-bounce').hasClass('hide')) {
                    $('#chartdiv-bounce').addClass('hide');
                }
                if (!$('#total-bounce').hasClass('hide')) {
                    $('#total-bounce').addClass('hide');
                }
            }

            timebase_LoadDataOfHead(data, message);

            $('#divLoading').hide();
            break;
        default:
            $('#divLoading').hide();
            if ($('#nodata-bounce').hasClass('hide')) {
                $('#nodata-bounce').removeClass('hide');
            }
            if (!$('#chartdiv-bounce').hasClass('hide')) {
                $('#chartdiv-bounce').addClass('hide');
            }
            if (!$('#total-bounce').hasClass('hide')) {
                $('#total-bounce').addClass('hide');
            }
    }
}
//-----------end bounce

//-----------complaint-------------------
function timebase_ComplaintsChart(apiDomain, access_token, message) {
    var EventBaseIds = [];
    if (!event_timebaseall) {
        EventBaseIds = event_timebase;
    }
    var date = custom_local2utc(moment($('#select-date-from').val()).format('YYYY-MM-DD'), moment($('#select-date-to').val()).format('YYYY-MM-DD'));

    var data = {
        CampaignId: $('#bs-select-campaign').val(),
        StartDate: date.StartDate,
        EndDate: date.EndDate,
        EventBaseIds: EventBaseIds,
    }
    $('#divLoading').show();
    var timezone = custom_GetTimeZone();
    var r = $.ajax({
        url: apiDomain + 'api/report/Auto/SendReport',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success: function (r) {
            datasendat = r;
            timebase_GenComplaintChart(r, message);
            
        },
        error: function (x, s, r) {
            $('#divLoading').hide();
        }
    });
}

function timebase_GenComplaintChart(r, message) {
    switch (r.ErrorCode) {
        case 0:
            var data = r.Data;

            //gán data vào biến
           

            if (data.SpamComplaint > 0) {

                if (!$('#nodata-complaints').hasClass('hide')) {
                    $('#nodata-complaints').addClass('hide');
                }
                if ($('#chartdiv-complaints').hasClass('hide')) {
                    $('#chartdiv-complaints').remove('hide')
                }

                $('#head-complaints').text(data.SpamComplaint);

                Highcharts.getOptions().plotOptions.pie.colors = (function () {
                    var colors = ["#FF0000", "rgb(204, 204, 204)"],
                        base = Highcharts.getOptions().colors[0],
                        i;

                    for (i = 0; i < 10; i += 1) {
                        // Start out with a darkened base color (negative brighten), and end
                        // up with a much brighter color
                        colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
                    }
                    return colors;
                }());
                // Build the chart
                $('#chartdiv-complaints').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: '' // message.TotatBounce + data.TotalBounce
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: '',
                        data: [
                                           { name: 'Khác', y: data.Total - data.SpamComplaint },
                                           { name: 'Spam complaint', y: data.SpamComplaint }, ]
                    }]
                });
                setTimeout(function () {
                    var chart = $('#chartdiv-complaints').highcharts();

                    console.log('redraw');
                    var w = $('#chartdiv-complaints').innerWidth();
                    var h = $('#chartdiv-complaints').innerHeight();
                    // setsize will trigger the graph redraw 
                    chart.setSize(
                        w, h, false
                    );
                }, 10);
            }
            else {
                //    console.log(JSON.parse(JSON.stringify(r.Data)));
                if ($('#nodata-complaints').hasClass('hide')) {
                    $('#nodata-complaints').removeClass('hide');
                }
                if (!$('#chartdiv-complaints').hasClass('hide')) {
                    $('#chartdiv-complaints').addClass('hide');
                }
            }

            timebase_LoadDataOfHead(data, message);

            $('#divLoading').hide();
            break;
        default:
            $('#divLoading').hide();
            if ($('#nodata-complaints').hasClass('hide')) {
                $('#nodata-complaints').removeClass('hide');
            }
            if (!$('#chartdiv-complaints').hasClass('hide')) {
                $('#chartdiv-complaints').addClass('hide');
            }
            break;
    }
}
//-----------end complain----------------

//-----------unsubs----------------------
function timebase_UnsubcribedChart(apiDomain,access_token, message) {
    var EventBaseIds = [];
    if (!event_timebaseall) {
        EventBaseIds = event_timebase;
    }
    var date = custom_local2utc(moment($('#select-date-from').val()).format('YYYY-MM-DD'), moment($('#select-date-to').val()).format('YYYY-MM-DD'));

    var data = {
        CampaignId: $('#bs-select-campaign').val(),
        StartDate: date.StartDate,
        EndDate: date.EndDate,
        EventBaseIds: EventBaseIds,
    }
    $('#divLoading').show();
    var timezone = custom_GetTimeZone();
    var r = $.ajax({
        url: apiDomain + 'api/report/Auto/SendReport',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success: function (r) {
            datasendat = r;
            timebase_GenUnsubsChart(r, message);
            $('#divLoading').hide();
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
        }
    });
    

}

function timebase_GenUnsubsChart(r,message)
{
    switch (r.ErrorCode) {
        case 0:
            var data = r.Data;
            if (data.Unsubscribe > 0) {

                if (!$('#nodata-unsubcribed').hasClass('hide')) {
                    $('#nodata-unsubcribed').addClass('hide');
                }
                if ($('#chartdiv-unsubcribed').hasClass('hide')) {
                    $('#chartdiv-unsubcribed').remove('hide')
                }

                $('#head-unsubcribed').text(data.Unsubscribe);

                var event = { 'key': 'Unsubscribe', 'value': data.Unsubscribe }
                //var chart = AmCharts.makeChart("chartdiv-unsubcribed", {
                //    "type": "pie",
                //    "theme": "light",
                //    "dataProvider": event,
                //    "valueField": "value",
                //    "titleField": "key",
                //    "outlineAlpha": 0,
                //    "colors": ["rgb(99, 181, 18)", "rgb(204, 204, 204)", ],
                //    "depth3D": 0,
                //    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                //    "angle": 0,
                //    "export": {
                //        "enabled": true
                //    }
                //});
                Highcharts.getOptions().plotOptions.pie.colors = (function () {
                    var colors = ["#FF0000", "rgb(204, 204, 204)"],
                        base = Highcharts.getOptions().colors[0],
                        i;

                    for (i = 0; i < 10; i += 1) {
                        // Start out with a darkened base color (negative brighten), and end
                        // up with a much brighter color
                        colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
                    }
                    return colors;
                }());
                // Build the chart
                $('#chartdiv-unsubcribed').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Total: ' + data.Unsubscribe // message.TotatBounce + data.TotalBounce
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: '',
                        data: [{ name: 'Unsubcribed', y: data.Unsubscribe }, { name: 'Khác', y: data.Total - data.Unsubscribe }, ]
                    }]
                });

                setTimeout(function () {
                    var chart = $('#chartdiv-unsubcribed').highcharts();

                    console.log('redraw');
                    var w = $('#chartdiv-unsubcribed').innerWidth();
                    var h = $('#chartdiv-unsubcribed').innerHeight();
                    // setsize will trigger the graph redraw 
                    chart.setSize(
                        w, h, false
                    );
                }, 10);

            }
            else {
                //    console.log(JSON.parse(JSON.stringify(r.Data)));
                if ($('#nodata-unsubcribed').hasClass('hide')) {
                    $('#nodata-unsubcribed').removeClass('hide');
                }
                if (!$('#chartdiv-unsubcribed').hasClass('hide')) {
                    $('#chartdiv-unsubcribed').addClass('hide');
                }
            }

            timebase_LoadDataOfHead(data, message);

            $('#divLoading').hide();
            break;
        default:
            $('#divLoading').hide();
            if ($('#nodata-unsubcribed').hasClass('hide')) {
                $('#nodata-unsubcribed').removeClass('hide');
            }
            if (!$('#chartdiv-unsubcribed').hasClass('hide')) {
                $('#chartdiv-unsubcribed').addClass('hide');
            }
            break;
    }
}
//-----------end unsub--------------------

//report sms---------------------------
function timebase_GetReportSMS(apiDomain, access_token, message) {
    var CampaignId = $('#bs-select-campaign').val();
    var EventBaseIds = [];
    if (event_timebaseall){
        EventBaseIds = [];
    }
    else{
        event_timebase
    }

    var date = custom_local2utc(moment($('#select-date-from').val()).format('YYYY-MM-DD'), moment($('#select-date-to').val()).format('YYYY-MM-DD'));
    var timezone = custom_GetTimeZone();

    var data={
        CampaignId:CampaignId,
        EventBaseIds: EventBaseIds,
        StartDate: date.StartDate,
        EndDate: date.EndDate,
    };
    $.ajax({
        url: apiDomain + 'api/report/Auto/SMSReport',
        type: 'POST',
        data: JSON.stringify(data),
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        //data: data,
        //contentType: 'application/x-www-form-urlencoded',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    if (data.Total > 0) {

                        timebase_GenChar(data,message);
                        $('#sms-nodata-open').addClass('hide');
                        $('#sms-havedata').show();
                        $('#div-sent-sms-status').hide();
                    }
                    else {
                        $('#sms-nodata-open').removeClass('hide');
                        $('#sms-havedata').hide();
                        $('#div-sent-sms-status').hide();
                    }
                    
                   // timebase_ListMbile(data);
                    break;
                default:
                    custom_shownotification('error', message.GetDataError);
                    break;
            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.GetDataError);
        }
    })
}

function timebase_GenChar(data, message) {
    var dataprovider = [];
    //var Sent = data.TotalSent;
    //var Fail = data.Total - Sent;
    var Sent = data.Total
    dataprovider = [{ 'Status': message.SMSSent, 'Value': Sent }, ];
        $('#sms-nodata-open').addClass('hide');
        $('#sms-havedata').show();
    
   
    var chart = AmCharts.makeChart("chartdiv-sendsms", {
        "type": "pie",
        "startDuration": 0,
        "theme": "light",

        "addClassNames": true,
        "legend": {
            "position": "right",
            "marginRight": 100,
            "autoMargins": false
        },
        "innerRadius": "30%",
        "defs": {
            "filter": [{
                "id": "shadow",
                "width": "200%",
                "height": "200%",
                "feOffset": {
                    "result": "offOut",
                    "in": "SourceAlpha",
                    "dx": 0,
                    "dy": 0
                },
                "feGaussianBlur": {
                    "result": "blurOut",
                    "in": "offOut",
                    "stdDeviation": 5
                },
                "feBlend": {
                    "in": "SourceGraphic",
                    "in2": "blurOut",
                    "mode": "normal"
                }
            }]
        },
        "dataProvider": dataprovider,
        "balloonText": "[[title]]<br><span style='font-size:14px'>" + message.Number + ": <b>[[value]]</b> ([[percents]]%)</span>",
        "valueField": 'Value',
        "titleField": "Status",
        "export": {
            "enabled": true
        }
    });

    chart.validateData();

    chart.addListener("init", handleInit);

    chart.addListener("rollOverSlice", function (e) {
        handleRollOver(e);
    });

    function handleInit() {
        chart.legend.addListener("rollOverItem", handleRollOver);
    }

    function handleRollOver(e) {
        var wedge = e.dataItem.wedge.node;
        wedge.parentNode.appendChild(wedge);
    }
}


function timebase_ListMbile(data, message) {
    var senttype = "";
    var pendingtype = "";
    var failtype = "";
    var status = 0;
    var sent = [];

    var pending = [];

    var fail = [];

    $.each(data, function (i, o) {
        if (parseInt(o.SMSStatus) == 0) {
            sent.push(o.Mobile);
            senttype += o.Mobile + ', ';
        }
        if (parseInt(o.SMSStatus) == 1) {
            pending.push(o.Mobile);
            pendingtype += o.Mobile + ', ';
        }
        if (parseInt(o.SMSStatus) == 2) {
            fail.push(o.Mobile);
            failtype += o.Mobile + ', ';
        }

    });
    switch (status) {
        case 0:

            timebase_AddList(senttype, message)

            break;
        case 1:
            timebase_AddList(pendingtype, message)

            break;
        case 2:
            timebase_AddList(failtype, message)
            break;
    }

    $('#tab-message-sent').on('click', function () {
        status = 0;
        timebase_AddList(senttype, message)
    });
    $('#tab-message-pending').on('click', function () {
        status = 1;
        timebase_AddList(pendingtype, message)
    });
    $('#tab-message-fail').on('click', function () {
        status = 2;
        timebase_AddList(failtype, message)
    })

}

function timebase_AddList(data,message) {
    $('#list-mobil-sent').children().remove();
    var portletheight = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.page-footer').outerHeight();
    $('#portlet-body-timebase-sms').css('height', portletheight - 20);

    if (data != "") {
        var height = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('#chartdiv-sendsms').outerHeight() - $('#div-sent-sms-status').outerHeight();
        var html = '<span>' + data + '</span>'
        $('#list-mobil-sent').append(html);
        $('#list-mobil-sent').slimScroll({
            height: height - 120,
            width: '100%',
            alwaysVisible: true
        });

    }
    else {
        $('#list-mobil-sent').append('<div style="text-align:center;"><span class="note note-info"> ' + message.NoMobile + ' </span></div>');
    }
}


function timebase_NoCampaign()
{
    $('#select-event-autoresponder').hide(); //hide autoresponder
    $('#statistic-timebase-tab').hide();
    $('#nodata-statistic').show();
}

//uniquelist theo email
Array.prototype.uniqueobj = function uniqueobj() {
    var flags = [], output = [], l = this.length, i;
    for (i = 0; i < l; i++) {
        if (flags[this[i].Email]) continue;
        flags[this[i].Email] = true;
        output.push(this[i]);
    }
    //   console.log('fsfas:' + output);
    return output;
}



function timebase_Export(apiDomain, access_token, message) {
    $('#select-date-export').on('click', function () {
        $('#file-export-name').prop('disabled', false);
        $('#submit-export-statistic-contact').prop('disabled', false);
        $('#table-file-export').children().remove();
        var name = 'Export autoresponder ' + moment().format('YYYY-MM-DD-HH-mm');
        $('#file-export-name').val(name);
        $('#modal-export-statistic-contact').modal('toggle');
    });
}

var checkfileexport;
function timebase_CreateExport(apiDomain, access_token, message) {

    var getdate = custom_local2utc($('#select-date-from').val(), $('#select-date-to').val());
    var timezone = custom_GetTimeZone();
    var FileName = $('#file-export-name').val();
    var EventBaseIds = [];
    if (event_timebaseall){
        EventBaseIds = [];
    }
    else{
        event_timebase;
    }
    var data = {
        FileType: 'xlsx',
        FileName: FileName.replace(/\s/g, "-"),
        CampaignId: $('#bs-select-campaign').val(),
        StartDate: getdate.StartDate,
        EndDate: getdate.EndDate,
        EventBaseIds: EventBaseIds,
    };
    $.ajax({
        url: apiDomain + 'api/report/CreateAutoCampaignReportExport',
        type: 'post',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
            request.setRequestHeader('timezone', timezone);
        },
        success: function (r) {

            switch (parseInt(r.ErrorCode)) {

                case 0:
                    {
                        var data = r.Data;
                        var ExportId = data.ExportId;
                        switch (parseInt(data.Status)) {
                            case 0: //pendding
                                checkfileexport = setInterval(function () { timebase_CheckExportComplete(apiDomain, access_token, ExportId, message); }, 3000);
                                break;
                            case 1:
                                var html = '<div><a href=' + data.FilePath + '><i class="fa fa-cloud-download font-green-jungle"></i> ' + message.DownLoad + '</div>';
                                setTimeout(function () {
                                    $('#status-export').children().remove();
                                    $('#status-export').append(html)
                                }, 3000);
                                break;
                            case 2:
                                var html = '<div><i class="fa fa-remove font-red"></i> ' + message.ExportError + ' </div>';
                                $('#status-export').children().remove();
                                $('#status-export').append(html);
                                break;

                        }
                    }
                    break;
                default:
                    custom_shownotification("error", message.ExportError);
                    break;


            }
        },
        error: function (x, s, e) {
            custom_shownotification("error", message.ExportError);
        }
    })
}

function timebase_CheckExportComplete(apiDomain, access_token, ExportId, message) {
    $.ajax({
        url: apiDomain + 'api/report/GetContactExport?ExportId=' + ExportId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);

        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    console.log(data);
                    switch (parseInt(data.Status)) {
                        case 1:
                            var html = '</div><a href=' + data.FilePath + '><i class="fa fa-cloud-download font-green-jungle"></i>' + message.DownLoad + ' </a></div>';
                            setTimeout(function () {
                                $('#status-export').children().remove();
                                $('#status-export').append(html);
                            }, 3000);
                            clearInterval(checkfileexport);
                            break;
                        case 2:
                            var html = '<div><i class="fa fa-remove font-red"></i>' + message.ExportError + '</div>';
                            $('#status-export').children().remove()
                            $('#status-export').append(html);
                            clearInterval(checkfileexport);
                            break;

                    }
                    break;

            }
        },
        error: function (x, s, e) {
            custom_shownotification("error", 'Có lỗi xảy ra');
            clearInterval(checkfileexport);
        }
    })
}


function timebase_SubmitExport(apiDomain, access_token, message) {
    $('#submit-export-statistic-contact').on('click', function () {

        $('#table-file-export').children().remove();
        if ($('#file-export-name').val()) {

            var html = '<table style="width:100%;" class="table table-striped">' +
                                   ' <thead>' +
                                       ' <tr>' +
                                       '   <td>' +
                                           message.FileName +
                                           ' </td>' +
                                          '  <td>' +
                                           message.Status +
                                                    '</td>' +
                                                    '<td>' +
                                                     message.CreateDate +
                                                  '  </td>' +
                                             '   </tr>' +
                                           '  </thead>' +
                                          '  <tbody>' +
                                              '  <tr>' +
                                                  '  <td>' +
                                                          $('#file-export-name').val() +
                                                  '  </td>' +
                                                 '   <td id="status-export">' +
                                                      '<div><i class="fa fa-spinner fa-pulse fa-fw"></i> ' + message.Processing + ' </div>' +
                                                   ' </td>' +
                                                 '   <td>' +
                                                     moment().format('YYYY-MM-DD HH:mm') +
                                                   ' </td>' +
                                              '  </tr>' +
                                           ' </tbody>' +
                                      '  </table>';
            $('#table-file-export').append(html);
            $('#file-export-name').prop('disabled', true);
            $('#submit-export-suppression-contact').prop('disabled', true);
            timebase_CreateExport(apiDomain, access_token, message);


        }
        else {
            custom_shownotification("error", message.FileNameRequire);
        }
    })
}




