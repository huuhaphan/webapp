﻿function statisticmessage_Action(apiDomain,access_token,SentId,message)
{
    statisticmessage_GetSMSCampaign(apiDomain, access_token, SentId, message);
    statisticmesssage_ChangeCampaign(apiDomain, access_token, message);
}


function statisticmessage_GetSMSCampaign(apiDomain,access_token,SentId,message)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/SmsSend/GetListForSelect?IsSent=true',
        type: 'GET',
        contentType: 'application/x-www-form-urlencoded',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var option = [];
                    var data = r.Data;
                   
                        $.each(data.reverse(), function (i, o) {
                            if (!o.MessageName) {
                                o.MessageName = message.NoCampaignName;
                            }
                            option.push({
                                id: o.SentId,
                                text: o.MessageName,
                            });
                        });

                        $('#campaignsms-list-statistic').select2({ data: option });
                        $("#campaignsms-list-statistic").on("select2:open", function () {
                            $(".select2-search--dropdown .select2-search__field").attr("placeholder", "Search...");
                        });
                        $("#campaignsms-list-statistic").on("select2:close", function () {
                            $(".select2-search--dropdown .select2-search__field").attr("placeholder", null);
                        });
                        $('.select2.select2-container.select2-container--bootstrap.select2-container--below').css('width', 'auto !important');
                        if (data.length > 0) {
                            if (SentId) {
                                $('#campaignsms-list-statistic').select2('val', SentId);
                            }
                            else {
                                var Id = option[0].id;
                                $('#campaignsms-list-statistic').select2('val', Id);
                            }
                            var sent = $("#campaignsms-list-statistic").select2('val');

                            statisticmessage_GetReport(apiDomain, access_token, sent, message);
                        }
                        else
                        {
                            statisticmessage_NoCampaign();
                        }
                    break;
                default:
                    statisticmessage_NoCampaign();
                    custom_shownotification('error', message.GetDataError);
                    break;
            }
            $('#divLoading').hide();
        },
        error: function (x, s, e) {
            statisticmessage_NoCampaign();
            custom_shownotification('error', message.GetDataError);
            $('#divLoading').hide();
        }
    });
}

function statisticmesssage_ChangeCampaign(apiDomain, access_token, message) {
    $('#campaignsms-list-statistic').on('select2:select', function (evt) {
  
        var sent = $("#campaignsms-list-statistic").select2('val');
        statisticmessage_GetReport(apiDomain, access_token, sent, message);

    });
}

function statisticmessage_GetReport(apiDomain,access_token,SentId, message)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/SmsSend/GetSendReport?SMSSentId=' + SentId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success:function(r)
        {
            switch(parseInt(r.ErrorCode))
            {
                case 0:
                    var data = r.Data;
                    statisticmessage_GenChar(data.SendContacts, message);
              
                    break;
                default:
                    custom_shownotification('error', message.GetDataError);
                    break;
            }
            $('#divLoading').hide();
        },
        error:function(x,s,e)
        {
            custom_shownotification('error', message.GetDataError);
            $('#divLoading').hide();
        }
    })
}

function statisticmessage_GenChar(data,message)
{
    var dataprovider = [];
    var Sent=0;
    if (data.length > 0) {
        Sent = data.length;
        dataprovider = [{ 'Status': message.SMSSent, 'Value': Sent }];
        $('#sms-nodata-open').addClass('hide');
        $('#sms-havedata').show();
    }
    else
    {

        dataprovider = [{ 'Status': 'No have data', 'Value': 1 }];
        $('#sms-nodata-open').removeClass('hide');
        $('#sms-havedata').hide();
    }
    var chart = AmCharts.makeChart("chartdiv-sendsms", {
        "type": "pie",
        "startDuration": 0,
        "theme": "light",
           
        "addClassNames": true,
        "legend": {
            "position": "right",
            "marginRight": 100,
            "autoMargins": false
        },
        "innerRadius": "30%",
        "defs": {
            "filter": [{
                "id": "shadow",
                "width": "200%",
                "height": "200%",
                "feOffset": {
                    "result": "offOut",
                    "in": "SourceAlpha",
                    "dx": 0,
                    "dy": 0
                },
                "feGaussianBlur": {
                    "result": "blurOut",
                    "in": "offOut",
                    "stdDeviation": 5
                },
                "feBlend": {
                    "in": "SourceGraphic",
                    "in2": "blurOut",
                    "mode": "normal"
                }
            }]
        },
        "dataProvider": dataprovider,
        "balloonText": "[[title]]<br><span style='font-size:14px'>"+message.Number+": <b>[[value]]</b> ([[percents]]%)</span>",
        "valueField": 'Value',
        "titleField": "Status",
        "export": {
            "enabled": true
        }
    });

    chart.validateData();

    chart.addListener("init", handleInit);

    chart.addListener("rollOverSlice", function (e) {
        handleRollOver(e);
    });

    function handleInit() {
        chart.legend.addListener("rollOverItem", handleRollOver);
    }

    function handleRollOver(e) {
        var wedge = e.dataItem.wedge.node;
        wedge.parentNode.appendChild(wedge);
    }
}




function statisticmessage_NoCampaign()
{
    $('#have-campaign-statistic').hide();
    $('#nodata-statistic').show();
}


