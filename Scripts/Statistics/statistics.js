﻿/// <reference path="../custom.js" />
/// <reference path="statisticmessage.js" />
var tab = 0;
var opentype = 0;
var selectedlink = [];
var sent = 0;
var totaltype = 0;
var clicktype = 0;
var clickuniquetype = 0;
var openuniquetype = 0;
var datasendts; //=r.Data
var datasendclickts; //=r
var datasendopendts; //=r
var datasendbounce; //r.Data


function statistic_Action(apiDomain, access_token, CampaignId ,message)
{
    statistic_Intial();

    GetCampaignList(apiDomain, access_token, CampaignId, message);
  
    statistic_SelectChangeCampaign(apiDomain, access_token, message);

    statistic_SwitchChangeTotalChart(apiDomain, access_token, message);
   
    statistic_SelectDateReport(apiDomain, access_token, CampaignId, message);
    
    //chọn tabopen 
    statistic_SelectTabOpen(apiDomain, access_token, message);

    //chọn kiểu open
    statistic_SelectTypeOpen(apiDomain, access_token, message);
    //chọn kểu của unique open
    statistic_ChangeTypeUniqueOpen(apiDomain, access_token, message);

    //chọn kiểu click
    statistic_ChangeTypeClick(apiDomain, access_token, message);

    //chọn kiểu unique click
    statistic_ChangeTypeUniqueClick(apiDomain, access_token, message);

    statistic_ExportContact();

    statistic_Export(apiDomain, access_token, message);//chung 
    statistic_SubmitExport(apiDomain, access_token, message);
}

function statistic_Intial()
{
    $("#select-date-from").datepicker("setDate", moment().subtract(30, 'days').format('YYYY-MM-DD'));
    $('#select-date-to').datepicker("setDate", moment().format('YYYY-MM-DD'));
}

function GetCampaignList(apiDomain, access_token, CampaignId, message) {
   
    var data = {
        Status: 1,  //sent
        IsAutoResponder: false,
    }
    $.ajax({
        url: apiDomain + 'api/BasicCampaign/GetList',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: data,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var option = '';
                    var data = r.Data;
                    var databydate = [];
                    $.each(r.Data.reverse(), function (i, o) {
                        var date = custom_local2utc(moment($('#select-date-from').val()).format('YYYY-MM-DD'), moment($('#select-date-to').val()).format('YYYY-MM-DD'));
                        if (moment(o.SentDate).format('YYYY-MM-DD') >= date.StartDate || moment(o.SentDate).format('YYYY-MM-DD') <= data.EndDate) {
                            option += '<option data-subtext="' + moment(moment.utc(o.SentDate).toDate()).format('YYYY-MM-DD') + '" value="' + o.CampaignId + '">' + o.CampaignName + '</option>';
                            databydate.push(o);
                        }
                    });
                    $('#campaign-list-statistic').children().remove();
                    $('<select id="bs-select-campaign" class="bs-select form-control" data-show-subtext="true" data-style="#EEF1F5"></select>').append(option).appendTo('#campaign-list-statistic');
                    $('#bs-select-campaign').selectpicker('render');
                    if (databydate.length != 0) {
                        if (parseInt(CampaignId) != -1) {
                            $('#bs-select-campaign').selectpicker('val', CampaignId);
                        }

                        //var datefromselect = $('#bs-select-campaign option:selected').data('subtext');
                        //var formdate = moment(datefromselect).format('YYYY-MM-DD');

                        //statistic_TotalChart(apiDomain, access_token, message);
                        statistic_ResetVarDataSave();
                        LoadDataOfHead(apiDomain, access_token, message);
                        LoadDataOfTab(apiDomain, access_token, message);


                        //thống kê sms
                        if (parseInt(CampaignId) != -1) {
                            statistics_GetCampaignInfo(apiDomain, access_token, CampaignId, message);
                        }
                        else {
                            statistics_GetCampaignInfo(apiDomain, access_token, $('#bs-select-campaign').val(), message);
                        }
                        
                        
                    }
                    else {

                        custom_shownotification('error', message.NotData)
                        statistics_NoCampaign();
                    }
                    break;

                default:
                    custom_shownotification('error', message.GetDataError);
                    statistics_NoCampaign();
                    break;
            }
            
        },
        error: function (x, s, e) {
           
            statistics_NoCampaign();
            custom_shownotification('error', message.GetDataError)
        }
    })
}
function statistic_TotalChart(apiDomain, access_token, message) {
    $('#divLoading').show();
    var getdate = custom_local2utc($('#select-date-from').val(), $('#select-date-to').val());
    var timezone = custom_GetTimeZone();
    var data = {
        CampaignId: $('#bs-select-campaign').val(),
        StartDate: getdate.StartDate, //$('#bs-select-campaign option:selected').data('subtext'),
        EndDate: getdate.EndDate,
    }

    $.ajax({
        url: apiDomain + 'api/Report/SendReport',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: data,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success: function (r) {
            datasendts = r;
            //tạo totalchar
            statistic_GenTotalChart(datasendts, message);
        }
        , error: function (x, s, e) {
            custom_shownotification('error', message.GetDataError)
            $('#divLoading').hide();
        }
    });
}

//load chart total
function statistic_GenTotalChart(r, message) {
    $('#divLoading').show();
    switch (parseInt(r.ErrorCode)) {
        case 0:

            var data = r.Data;
            statistic_ReLoadHeadNum(data, message);

            //tạo totalchar
            if (data.Sent > 0) {

                //$('#head-total-sent').text(data.Sent);

                if (!$('#nodata-total').hasClass('hide')) {
                    $('#nodata-total').addClass('hide');
                }
                if ($('#hasdata-total-sum').hasClass('hide')) {
                    $('#hasdata-total-sum').removeClass('hide')
                }
                if ($('#hasdata-total-chart').hasClass('hide')) {
                    $('#hasdata-total-chart').removeClass('hide')
                }

                var para;
                var valueopen;
                sent = data.Sent;
                //sumary
                if (totaltype == 0) {


                    //$('#total-sent').text(data.Sent);


                    var rateopen = data.Open * 100 / data.Sent;
                    $('#sum-rate-open').text(rateopen.toFixed(2) + '%');

                    var rateclick = data.Click * 100 / data.Sent;
                    $('#sum-rate-click').text(rateclick.toFixed(2) + '%');

                    var rateunsub = data.Unsubscribe * 100 / data.Sent;
                    $('#sum-rate-unsub').text(rateunsub.toFixed(2) + '%');

                    var ratebounce = data.Bounce * 100 / data.Total;
                    $('#sum-rate-bounce').text(ratebounce.toFixed(2) + '%');

                    var ratecomplaint = data.SpamComplaint * 100 / data.Sent;
                    $('#sum-rate-complaint').text(ratecomplaint.toFixed(2) + '%');

                    valueopen = data.Open;
                    if (parseInt(data.Sent) - parseInt(data.Open) > 0) {
                        para = parseInt(data.Sent) - parseInt(data.Open);

                    }
                    else {
                        para = 0;
                    }
                }
                    //unique
                else if (totaltype == 1) {

                    //$('#total-sent').text(data.Sent);

                    var rateopen = data.UniqueOpen * 100 / data.Sent;
                    $('#sum-rate-open').text(rateopen.toFixed(2) + '%');

                    var rateclick = data.UniqueClick * 100 / data.Sent;
                    $('#sum-rate-click').text(rateclick.toFixed(2) + '%');

                    var rateunsub = data.Unsubscribe * 100 / data.Sent;
                    $('#sum-rate-unsub').text(rateunsub.toFixed(2) + '%');

                    var ratebounce = data.Bounce * 100 / data.Sent;
                    $('#sum-rate-bounce').text(ratebounce.toFixed(2) + '%');

                    var ratecomplaint = data.SpamComplaint * 100 / data.Sent;
                    $('#sum-rate-complaint').text(ratecomplaint.toFixed(2) + '%');

                    valueopen = data.UniqueOpen;
                    if (parseInt(data.Sent) - parseInt(data.UniqueOpen) > 0) {
                        para = parseInt(data.Sent) - parseInt(data.UniqueOpen);
                    }
                    else {
                        para = 0;
                    }
                }

                var title = message.TotalSent;


                // Make monochrome colors and set them as default for all pies
                Highcharts.getOptions().plotOptions.pie.colors = (function () {
                    var colors = ["rgb(99, 181, 18)", "rgb(204, 204, 204)"],
                        base = Highcharts.getOptions().colors[0],
                        i;

                    for (i = 0; i < 10; i += 1) {
                        // Start out with a darkened base color (negative brighten), and end
                        // up with a much brighter color
                        colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
                    }
                    return colors;
                }());

                // Build the chart
                $('#chartdiv-total').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: title + ': ' + data.Sent
                    },
                    tooltip: { enabled: false },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: '',
                        data: [
                            { name: 'Open', y: valueopen },
                            { name: '', y: para },

                        ]
                    }]
                });

                setTimeout(function () {
                    var chart = $('#chartdiv-total').highcharts();

                    console.log('redraw');
                    var w = $('#chartdiv-total').innerWidth();
                    var h = $('#chartdiv-total').innerHeight();
                    // setsize will trigger the graph redraw 
                    chart.setSize(
                        w, h, false
                    );
                }, 10);
            }
            else {

                // $('#head-total-sent').text('0');

                $('#nodata-total').removeClass('hide');
                if (!$('#hasdata-total-sum').hasClass('hide')) {
                    $('#hasdata-total-sum').addClass('hide')
                }
                if (!$('#hasdata-total-chart').hasClass('hide')) {
                    $('#hasdata-total-chart').addClass('hide')
                }

            }

            $('#divLoading').hide();
            break;
        case 101:

            // $('#head-total-sent').text('0');

            $('#nodata-total').removeClass('hide');
            if (!$('#hasdata-total-sum').hasClass('hide')) {
                $('#hasdata-total-sum').addClass('hide')
            }
            if (!$('#hasdata-total-chart').hasClass('hide')) {
                $('#hasdata-total-chart').addClass('hide')
            }


            $('#divLoading').hide();
            break;
        default:
            $('#divLoading').hide();
            custom_shownotification('error', message.GetDataError)
            break;
    }
    $('#divLoading').hide();
   
}

//load headban đầu.
function statistic_ReLoadHeadNum(data , message)
{
 
    $('#head-total-sent').text(data.Total);

    sent = data.Sent;

    if (data.Open > 0) {
        $('#head-open').text(data.Open);
    }
    else {
        $('#head-open').text('0');
        $('.table-show-open').children().remove();
    }
    if (data.Click > 0) {
        $('#head-click').text(data.Click);
    }
    else {
        $('#head-click').text('0');
        $('#show-table-click').children().remove();
    }
    if (data.Unsubscribe > 0) {
        $('#head-unsubcribed').text(data.Unsubscribe);
    }
    else {
        $('#head-unsubcribed').text('0');
    }
    if (data.Bounce > 0) {
        $('#head-bounced').text(data.Bounce);
    }
    else {
        $('#head-bounced').text('0');
    }
    if (data.SpamComplaint > 0) {
        $('#head-complaints').text(data.SpamComplaint);
    }
    else {
        $('#head-complaints').text('0');
    }
}

function statistic_LoadHeadNum(apiDomain, access_token,message)
{
    $('#divLoading').show();
    var getdate = custom_local2utc($('#select-date-from').val(), $('#select-date-to').val());
    var timezone = custom_GetTimeZone();
    var data = {
        CampaignId: $('#bs-select-campaign').val(),
        StartDate: getdate.StartDate, //$('#bs-select-campaign option:selected').data('subtext'),
        EndDate: getdate.EndDate,
    }
    $.ajax({
        url: apiDomain + 'api/Report/SendReport',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: data,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:

                    var data = JSON.parse(JSON.stringify(r.Data));
                        $('#head-total-sent').text(data.Total);
                     
                        sent = data.Sent;
                        datasendts = r;
                        statistics_GenHeadNum(data, message)
                   
                    $('#divLoading').hide();
                    break;
                default:
                    $('#divLoading').hide();
                    custom_shownotification('error', message.GetDataError)
                    break;
            }
        }, error: function () {
            $('#divLoading').hide();
            custom_shownotification('error', message.GetDataError)
        }
    });
}

function statistics_GenHeadNum(data, message) {
    if (data.Open > 0) {
        $('#head-open').text(data.Open);
    }
    else {
        $('#head-open').text('0');
        $('.table-show-open').children().remove();
    }
    if (data.Click > 0) {
        $('#head-click').text(data.Click);
    }
    else {
        $('#head-click').text('0');
        $('#show-table-click').children().remove();
    }
    if (data.Unsubscribe > 0) {
        $('#head-unsubcribed').text(data.Unsubscribe);
    }
    else {
        $('#head-unsubcribed').text('0');
    }
    if (data.Bounce > 0) {
        $('#head-bounced').text(data.Bounce);
    }
    else {
        $('#head-bounced').text('0');
    }
    if (data.SpamComplaint > 0) {
        $('#head-complaints').text(data.SpamComplaint);
    }
    else {
        $('#head-complaints').text('0');
    }
}

//function statistics_SendClick(apiDomain, access_token, message) {
//    $('#divLoading').show();
//    var getdate = custom_local2utc($('#select-date-from').val(), $('#select-date-to').val());
//    var timezone = custom_GetTimeZone();
//    var data = {
//        CampaignId: $('#bs-select-campaign').val(),
//        StartDate: getdate.StartDate, //$('#bs-select-campaign option:selected').data('subtext'),
//        EndDate: getdate.EndDate,
//    }
//    var data = $.ajax({
//        url: apiDomain + 'api/Report/SendClick',
//        type: 'POST',
//        contentType: 'application/x-www-form-urlencoded',
//        data: data,
//        beforeSend: function (request) {
            
//            request.setRequestHeader("Authorization", "Bearer " + access_token);
//            request.setRequestHeader("timezone", timezone);
//        },
//        async: false,

//    }).responseJSON;

//    return data;
//}

function LoadSelectLink(apiDomain, access_token, message) {
    
    $('#divLoading').show();
    var getdate = custom_local2utc($('#select-date-from').val(), $('#select-date-to').val());
    var timezone = custom_GetTimeZone();
    var data = {
        CampaignId: $('#bs-select-campaign').val(),
        StartDate: getdate.StartDate, //$('#bs-select-campaign option:selected').data('subtext'),
        EndDate: getdate.EndDate,
    }
    var data = $.ajax({
        url: apiDomain + 'api/Report/SendClick',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: data,
        beforeSend: function (request) {

            request.setRequestHeader("Authorization", "Bearer " + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success: function (r) {

            switch (parseInt(r.ErrorCode)) {
                case 0:  //lấy data thành công
                    //console.log(r.Data);
                    var data = JSON.parse(JSON.stringify(r.Data));

                    //gán data vào biến
                    datasendclickts = r;
                    // console.log('data' + data);
                    $('#div-select-link-click-chart').children().remove();
                    $('#select-link').addClass('hide');
                    if (data.TotalClick > 0) {

                        var object = r.Data;
                        if (object.Links) {
                            $('#div-btn-link-click-chart').removeClass('hide');
                            $('#select-link').removeClass('hide');
                            var option;
                            var links = _.uniq(object.Links);
                            $.each(links, function (i, o) {
                                option += '<option value="' + i + '" selected>' + o + '</option>';
                            });
                            $('<select id="select-link-click-chart" multiple="multiple" data-width="100%"></select>').append(option).appendTo('#div-select-link-click-chart');
                            $('#select-link-click-chart').multiselect({
                                includeSelectAllOption: true,
                                buttonWidth: '100%',
                                selectAllValue: 'select-all-value',
                                onInitialized: function (select, container) {
                                    var value = $('#div-select-link-click-chart #select-link-click-chart option:selected');
                                    selectedlink = [];
                                    $.each(value, function (i, o) {
                                        selectedlink.push($(this).text());
                                    });
                                },
                                onDropdownHidden: function (event) {
                                    var value = $('#div-select-link-click-chart #select-link-click-chart option:selected');
                                    selectedlink = [];
                                    $.each(value, function (i, o) {
                                        selectedlink.push($(this).text());
                                    });
                                    if (datasendclickts != undefined && datasendclickts != null) {
                                        statistic_PrepareDataSendClick(datasendclickts, message);

                                    } else {
                                        statistic_ClickChart(apiDomain, access_token, message);
                                    }
                                }
                            });

                            $('.multiselect .caret').css('display', 'none');
                            //load dropdown link
                            if (datasendclickts != undefined && datasendclickts != null) {
                                statistic_PrepareDataSendClick(datasendclickts, message);

                            } else {
                                statistic_ClickChart(apiDomain, access_token, message);
                            }
                        }
                        else {
                            $('#div-btn-link-click-chart').addClass('hide');
                        }
                    }
                    else {
                        if (datasendclickts != undefined && datasendclickts != null) {
                            statistic_PrepareDataSendClick(datasendclickts, message);

                        } else {
                            statistic_ClickChart(apiDomain, access_token, message);
                        }
                    }
                    $('#divLoading').hide();
                    break;
                case 101:
                    if ($('#nodata-click').hasClass('hide')) {
                        $('#nodata-click').removeClass('hide');
                    }
                    if (!$('#chartdiv-click').hasClass('hide')) {
                        $('#chartdiv-click').addClass('hide')
                    }
                    break;

                default:

                    $('#divLoading').hide();
                    custom_shownotification('error', message.GetDataError)
                    break;
            }
        }
    });
}

function statistic_SwitchChangeTotalChart(apiDomain, access_token,message)
{
    $('#tab-total-summary').on('click', function () {
        totaltype = 0;
        $('#divLoading').show();
        if (datasendts != undefined&&datasendts!=null) {
            statistic_GenTotalChart(datasendts, message);
            $('#divLoading').hide();
        } else {
            statistic_TotalChart(apiDomain, access_token, message);
           
        }
    });
    $('#tab-total-unique').on('click', function () {
        totaltype = 1;
        $('#divLoading').show();
        if (datasendts != undefined&&datasendts!=null) {
            statistic_GenTotalChart(datasendts, message);
            $('#divLoading').hide();
        } else {
            statistic_TotalChart(apiDomain, access_token, message);

        }
    })

    //$('#change-sumorunique-totalchart').on('switchChange.bootstrapSwitch', function (event, state) {
    //    $('#divLoading').show();
    //    statistic_TotalChart(apiDomain, access_token,message);
    //});
}

function statistic_SelectChangeCampaign(apiDomain, access_token, message)
{
 
    $('#campaign-list-statistic').on('changed.bs.select', '#bs-select-campaign', function (e, clickedIndex, newValue, oldValue) {
        $('#divLoading').show();
      
        //var datefromselect = $('#bs-select-campaign option:selected').data('subtext');
        //var formdate = moment(datefromselect).format('YYYY-MM-DD');

        //$("#select-date-from").datepicker("setDate", formdate);
        //$('#select-date-to').datepicker("setDate", moment().format('YYYY-MM-DD'));
        //tạm che statistic_LoadHeadNum(apiDomain, access_token,message);
        //tạm che LoadSelectLink(apiDomain, access_token,message);

        //reset lại biến lưu data;
        statistic_ResetVarDataSave();

        LoadDataOfTab(apiDomain, access_token, message);
        LoadDataOfHead(apiDomain, access_token, message);
        //thống kê sms
        var CampaignId = $('#bs-select-campaign').val();
        statistics_GetCampaignInfo(apiDomain, access_token, CampaignId, message);


    });
}


function statistic_ResetVarDataSave()
{
    datasendts = null;
    datasendclickts = null;
    datasendopendts = null;
    datasendbounce = null;


}

function statistic_SelectDateReport(apiDomain, access_token, CampaignId, message)
{
    $('#select-date-report').on('click', function () {
        $('#divLoading').show();
        GetCampaignList(apiDomain, access_token, CampaignId, message);
        //reset biến lưu data
        
    })
}


function LoadDataOfTab(apiDomain, access_token,message)
{
    switch (tab) {
        case 0:
        case 1:
            if (datasendts != undefined &&datasendts!=null) {
                statistic_GenTotalChart(datasendts, message)
            } else {
                statistic_TotalChart(apiDomain, access_token, message);
            }
            break;
        case 2:
            if (datasendopendts != undefined && datasendopendts != null) {
                statistic_GenOpenChart(datasendopendts, message);
            }
            else {
                statistic_OpenChart(apiDomain, access_token, message);
            }
            
            break;
        case 3:
            if (datasendclickts != undefined && datasendclickts != null) {
                statistic_PrepareDataSendClick(datasendclickts,message);
            }
            else {
                LoadSelectLink(apiDomain, access_token, message);
            }

            break;
           
        case 4:
            if (datasendts != undefined && datasendts != null) {
                statistic_GenUnsubcribedChart(datasendts, message);
            } else {
                statistic_UnsubcribedChart(apiDomain, access_token, message);
            }
            break;

        case 5:
            if (datasendbounce != undefined && datasendbounce != null) {
                statistic_GenBounceChart(datasendbounce, message);
            } else {
                statistic_BounceChart(apiDomain, access_token, message);
            }
            break;

        case 6:
            if (datasendts != undefined && datasendts != null) {
                statistic_GenComplaintsChart(datasendts, message);
            } else {
                statistic_ComplaintsChart(apiDomain, access_token, message);
            }
            break;

    }
}

function LoadDataOfHead(apiDomain, access_token, message)
{
    switch (tab) {
        case 0:

            break;
        case 1:
            break;
        default:
            if (datasendts != null && datasendts != undefined) {
                statistics_GenHeadNum(datasendts, message);
            } else {
                statistic_LoadHeadNum(apiDomain, access_token, message);
            }
            break;

    }
}

function statistic_SelectTabOpen(apiDomain, access_token,message)
{
    $('#tab-total-statistic').on('click', function () {
        tab = 1;
        
        if (datasendts != undefined) {
            statistic_GenTotalChart(datasendts, message);
  
        } else {
            statistic_TotalChart(apiDomain, access_token, message);
           
        }
    })

    $('#tab-open-statistic').on('click', function () {
        tab = 2;

        if (datasendopendts != undefined && datasendopendts != null) {
            statistic_GenOpenChart(datasendopendts, message);
        }
        else {
            statistic_OpenChart(apiDomain, access_token, message);
        }
    });

    $('#tab-click-statistic').on('click', function () {
        tab = 3;
      
        if (datasendclickts != undefined && datasendclickts != null) {
            statistic_PrepareDataSendClick(datasendclickts, message);
        } else {
            LoadSelectLink(apiDomain, access_token, message);
        }
    });

    $('#tab-unsubcribed-statistic').on('click', function () {
        tab = 4;
       
        if (datasendts != undefined && datasendts != null) {
            statistic_GenUnsubcribedChart(datasendts, message);
        } else {
            statistic_UnsubcribedChart(apiDomain, access_token, message);
        }
    });
    $('#tab-bounce-statistic').on('click', function () {
        tab = 5;
        if (datasendbounce != undefined && datasendbounce != null) {
            statistic_GenBounceChart(datasendbounce, message);
        }
        else {
            statistic_BounceChart(apiDomain, access_token, message);
        }
    });
    

    $('#tab-complaints-statistic').on('click', function () {
        tab = 6;
        if (datasendts != undefined && datasendts != null) {
            statistic_GenComplaintsChart(datasendts, message);
        }
        else {
            statistic_ComplaintsChart(apiDomain, access_token, message);
        }
    });

    
}



function statistic_SelectTypeOpen(apiDomain, access_token,message)
{
    $('#tab-open-opened').on('click', function () {
        $('#divLoading').show();
        opentype = 1;
        if (!$('#div-open-type-of-unique').hasClass('hide')) {
            $('#div-open-type-of-unique').addClass('hide');
        }
        if (datasendopendts != undefined && datasendopendts != null) {
            statistic_GenOpenChart(datasendopendts, message);
            $('#divLoading').hide();
        }
        else {
            statistic_OpenChart(apiDomain, access_token, message);
        }
    })
    $('#tab-open-uniqueopened').on('click', function () {
        $('#divLoading').show();
        opentype = 2;
        if (datasendopendts != undefined && datasendopendts != null) {
            statistic_GenOpenChart(datasendopendts, message);
            $('#divLoading').hide();
        }
        else {
            statistic_OpenChart(apiDomain, access_token, message);
        }
    })
    $('#tab-open-unopened').on('click', function () {
        $('#divLoading').show();
        opentype = 3;
        if (!$('#div-open-type-of-unique').hasClass('hide')) {
            $('#div-open-type-of-unique').addClass('hide');
        }
        if (datasendopendts != undefined && datasendopendts != null) {
            statistic_GenOpenChart(datasendopendts, message);
            $('#divLoading').hide();
        }
        else {
            statistic_OpenChart(apiDomain, access_token, message);
        }
    })
}


function statistic_ChangeTypeUniqueOpen(apiDomain, access_token,message)
{
    //$('#change-type-of-unique').on('switchChange.bootstrapSwitch', function (event, state) {
    //    $('#divLoading').show();
    //    statistic_OpenChart(apiDomain, access_token,message);
    //});
    $('#tab-open-unique-summary').on('click', function () {
        openuniquetype = 0;
        if (datasendopendts != undefined && datasendopendts != null) {
            statistic_GenOpenChart(datasendopendts, message);

        } else {
            statistic_OpenChart(apiDomain, access_token, message);
        }
    });
    $('#tab-open-unique-hour').on('click', function () {
        openuniquetype = 1;
        if (datasendopendts != undefined && datasendopendts != null) {
            statistic_GenOpenChart(datasendopendts, message);

        } else {
            statistic_OpenChart(apiDomain, access_token, message);
        }
    })
}

function statistic_OpenChart(apiDomain, access_token,message) {
    $('#divLoading').show();
    var getdate = custom_local2utc($('#select-date-from').val(), $('#select-date-to').val());
    var timezone = custom_GetTimeZone();
    var data = {
        CampaignId: $('#bs-select-campaign').val(),
        StartDate: getdate.StartDate, //$('#bs-select-campaign option:selected').data('subtext'),
        EndDate: getdate.EndDate,
    }
    $.ajax({
        url: apiDomain + 'api/Report/SendOpen',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: data,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success: function (r) {
            datasendopendts = r;
            statistic_GenOpenChart(r, message);
            $('#divLoading').hide();
        },
        error: function () {
            custom_shownotification('error', message.GetDataError)
            $('#divLoading').hide();
        }
    });

}

function statistic_GenOpenChart(r, message) {
    switch (parseInt(r.ErrorCode)) {
        case 0:

            statistic_TableOpenTotal(r.Data);

            if (JSON.parse(JSON.stringify(r.Data)).TotalOpen == 0) {
                $('#nodata-open').removeClass('hide');
                $('#chartdiv-open').addClass('hide');

                $('.table-show-open').children().remove();

            }
            else {
                if (!$('#nodata-open').hasClass('hide')) {
                    $('#nodata-open').addClass('hide');
                }
                if ($('#chartdiv-open').hasClass('hide')) {
                    $('#chartdiv-open').removeClass('hide');
                }

                // console.log(JSON.parse(JSON.stringify(r.Data)));
                var data = JSON.parse(JSON.stringify(r.Data)).Events;
                bevent = new Array();
                event = new Array();

                //var StartDate = $('#select-date-from').val(); //$('#bs-select-campaign option:selected').data('subtext'),
                //var EndDate = $('#select-date-to').val();
                

                // opened----------------------
                if (opentype == 0 || opentype == 1) {
                    $.each(data, function (i, o) {
                        bevent.push({ 'value': '1', 'date': moment(o.TimeStamp).format('YYYY-MM-DD') });

                    });
                    var a = _(bevent).groupBy(function (o) {
                        return o.date;
                    })
                    var day = (moment(data[0].TimeStamp).diff(data[data.length - 1].TimeStamp, 'days'));
                    for (i = 0; i <= day; i++) {
                        var value = 0;
                        var startdate = new Date(data[data.length - 1].TimeStamp);
                        var dateofevent = moment(startdate.setDate(startdate.getDate() + i)).format('YYYY-MM-DD');
                        var e1 = { 'value': 0, 'date': dateofevent }
                        $.each(a, function (i, o) {
                            if (moment(dateofevent).format('YYYY-MM-DD') == moment(i).format('YYYY-MM-DD')) {
                                e1.value = o.length
                            }
                        });
                        event.push(e1);

                    }

                   
                   


                    //chart
                    ChartOpen(event);
                    //table show data
                    statistic_TableOpenTotal(r.Data);
                }

                //unique opened---------------------
                if (opentype == 2) {

                    //summary 
                    if ($('#div-open-type-of-unique').hasClass('hide')) {
                        $('#div-open-type-of-unique').removeClass('hide');
                    }
                    if (openuniquetype == 0) {
                        var listunique2 = data.uniqueobj();

                        var groups = _.groupBy(listunique2, function (value) {
                            return moment(value.TimeStamp).format('YYYY-MM-DD');
                        });

                        var bevent = _.map(groups, function (group) {
                            return {
                                Date: moment(group[0].TimeStamp).format('YYYY-MM-DD'),
                                Count: _.pluck(group, 'ContactId')
                            }
                        });
                        //   console.log('b:'+ JSON.stringify( bevent));

                        for (i = 0; i <= day; i++) {
                            var value = 0;
                            var startdate = new Date(data[data.length - 1].TimeStamp);
                            var dateofevent = moment(startdate.setDate(startdate.getDate() + i)).format('YYYY-MM-DD');
                            var e2 = { 'value': 0, 'date': dateofevent }
                            $.each(bevent, function (i, o) {
                                if (moment(dateofevent).format('YYYY-MM-DD') == moment(o.Date).format('YYYY-MM-DD')) {
                                    var a = o.Count;

                                    e2.value = a.length;
                                }
                            });
                            event.push(e2);
                        }

                        //chart
                        ChartOpen(event);
                        //table
                        statistic_TableOpenTotal(r.Data);
                    }
                        //hour
                    else if (openuniquetype == 1) {

                        var hour = [00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
                        var listunique = data.uniqueobj();

                        //   console.log('gg' + JSON.stringify( listunique));

                        var groups = _.groupBy(listunique, function (value) {
                            var h = new Date(value.TimeStamp);
                            console.log('h' + h);
                            return h
                        });



                        var bevent = _.map(groups, function (group) {
                            return {
                                Hour: new Date(group[0].TimeStamp).getHours(),//moment(new Date(group[0].TimeStamp)).format('hh'),
                                Count: _.pluck(group, 'ContactId')
                            }
                        });
                        console.log('bevent:' + JSON.stringify(bevent));

                        for (i = 0; i < hour.length; i++) {
                            var value = 0;

                            var e2 = { 'value': 0, 'hour': hour[i] }
                            $.each(bevent, function (idx, o) {
                                if (parseInt(hour[i]) == parseInt(o.Hour)) {
                                    value += o.Count.length;


                                }
                            });
                            e2.value = value;

                            event.push(e2);

                        }
                        //     console.log(event);
                        //char
                        CharOpenUniqueHour(event);
                        //table
                        statistic_TableUniqueOpenHour(event);
                    }


                }

                //noopen-------------------------------------
                if (opentype == 3) {

                    //list unique
                    var listunique3 = data.uniqueobj();



                    var groups = _.groupBy(listunique3, function (value) {
                        return moment(value.TimeStamp).format('YYYY-MM-DD');
                    });

                    var bevent = _.map(groups, function (group) {
                        return {
                            Date: moment(group[0].TimeStamp).format('YYYY-MM-DD'),
                            Count: _.pluck(group, 'ContactId')
                        }
                    });

                    //tạo list unopen
                    template = 0;
                    var total = sent;

                    for (i = 0; i <= day; i++) {
                        var startdate = new Date(data[0].TimeStamp);
                        var dateofevent = moment(startdate.setDate(startdate.getDate() + i)).format('YYYY-MM-DD');


                        var e3 = { 'date': dateofevent, 'value': total - template };

                        $.each(bevent, function (i, o) {
                            if (dateofevent == moment(o.Date).format('YYYY-MM-DD')) {

                                e3.value = total - o.Count.length - template;
                                template = total - e3.value;
                            }
                        })

                        event.push(e3);
                    }

                    //chart
                    ChartOpen(event);
                    //table
                    statistic_TableOpenTotal(r.Data);
                    $('#divLoading').hide();
                }
            }
            $('#divLoading').hide();
            break;
        case 101:
            $('#nodata-open').removeClass('hide');
            $('#chartdiv-open').addClass('hide');
            $('#divLoading').hide();
            break;
        default:
            custom_shownotification('error', message.GetDataError)
            $('#divLoading').hide();
            break;
    }
}


function ChartOpen(event)
{
    var chart = AmCharts.makeChart("chartdiv-open", {
        "type": "serial",
        "theme": "light",
        "marginRight": 40,
        "marginLeft": 40,
        "autoMarginOffset": 20,
        "mouseWheelZoomEnabled": false,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id": "v1",
            "axisAlpha": 0,
            "position": "left",
            "ignoreAxisWidth": true
        }],
        "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0
        },
        "graphs": [{
            "id": "g1",
            "balloon": {
                "drop": true,
                "adjustBorderColor": false,
                "color": "#ffffff"
            },
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "title": "red line",
            "useLineColorForBulletBorder": true,
            "valueField": "value",
            "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
        }],
        "chartScrollbar": {
            "graph": "g1",
            "oppositeAxis": false,
            "offset": 30,
            "scrollbarHeight": 30,
            "backgroundAlpha": 0,
            "selectedBackgroundAlpha": 0.1,
            "selectedBackgroundColor": "#888888",
            "graphFillAlpha": 0,
            "graphLineAlpha": 0.5,
            "selectedGraphFillAlpha": 0,
            "selectedGraphLineAlpha": 1,
            "autoGridCount": true,
            "color": "#AAAAAA"
        },
        "chartCursor": {
            "pan": true,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "cursorAlpha": 1,
            "cursorColor": "#258cbb",
            "limitToGraph": "g1",
            "valueLineAlpha": 0.2,
            "valueZoomable": true
        },
        "valueScrollbar": {
            "oppositeAxis": false,
            "offset": 50,
            "scrollbarHeight": 10
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true
        },
        "export": {
            "enabled": true
        },
        "dataProvider": event
    });

    chart.addListener("rendered", zoomChart);

    zoomChart();

    function zoomChart() {
        chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
    }
}
function CharOpenUniqueHour(event)
{
    var chart = AmCharts.makeChart("chartdiv-open", {
        "theme": "light",
        "type": "serial",
        "mouseWheelZoomEnabled": false,
        "dataProvider": event,
        "valueAxes": [{
            "title": "",
          
        }],
        "graphs": [{
            "balloonText": "Xem lúc [[category]]h: Số lượng [[value]]",
            "fillAlphas": 1,
            "lineAlpha": 0.2,
            "title": "Income",
            "type": "column",
            "valueField": "value"
        }],
        "depth3D": 0,
        "angle": 0,
        "rotate": true,
        "categoryField": "hour",
        "categoryAxis": {
            "gridPosition": "start",
            "fillAlpha": 0.05,
            "position": "left"
        },
        "export": {
            "enabled": true
        }
    });
   
}

function statistic_TableOpenTotal(data) {
    $('.table-show-open').children().remove();
    var html = "";
    var dt = JSON.parse(JSON.stringify(data));
    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">' +
'<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-center font-green-jungle"><h4><b>' + $('#bs-select-campaign option:selected').text() + '</b></h4> </div>' +
'<div class="divTableCell text-center"> <h4>Action</h4></div>' +
'</div>' +
'<div class="divTableRow">' +
'<div class="divTableCell text-center"><h4>' + dt.TotalOpen + ' Total opened</h4> </div>' +
'<div class="divTableCell text-center"><h4><a id="show-opened-contact"><span class="icon-users"></span> Show contacts</a><h4></div>' +
'</div>' +
'<div class="divTableRow">' +
'<div class="divTableCell text-center"><h4>' + dt.TotalUniqueOpen + ' Unique opened </h4> </div>' +
'<div class="divTableCell text-center"><h4><a id="show-uniqueopened-contact"><span class="icon-users"></span> Show contacts</a><h4></div>' +
'</div>' +
'<div class="divTableRow">' +
'<div class="divTableCell text-center"><h4>' + (sent - dt.TotalUniqueOpen) + ' Not opened </h4> </div>' +
'<div class="divTableCell text-center">&nbsp;</div>' +
'</div>' +
'</div>' +
'</div>'

    $('.table-show-open').append(html);

    $('.table-show-open #show-opened-contact').on('click', function () {
        $('#modal-show-contact #table-show-contact').children().remove();
        statistic_ShowOpenContact(data)
    });

    $('.table-show-open #show-uniqueopened-contact').on('click', function () {
        $('#modal-show-contact #table-show-contact').children().remove();
        statistic_ShowUniqueOpenContact(data)
    });

}

function statistic_ShowOpenContact(data)
{
   var event = data.Events;
    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">'+
        '<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-left"><h4 class="nomargin">Email</h4> </div>' +
'<div class="divTableCell text-center"> <h4 class="nomargin">Date</h4></div>' +
'</div>';
    $.each(event, function (i, o) {
        html += '<div class="divTableRow">' +
'<div class="divTableCell text-center"><span>' + o.Email + '</span> </div>' +
'<div class="divTableCell text-center"><span>' + moment(moment.utc(o.TimeStamp).toDate()).format('YYYY-MM-DD HH:mm:ss') + '</span> </div>' +
'</div>'
    });
    html += '</div>' +'</div>'
    $('#table-show-contact').append(html);
    $('#modal-show-contact').modal('toggle');
    data_general_export = event;
}

function statistic_ShowUniqueOpenContact(data)
{
    var event = data.Events.uniqueobj();
    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">'+
        '<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-left"><h4 class="nomargin">Email</h4> </div>' +
'<div class="divTableCell text-center"> <h4 class="nomargin">Date</h4></div>' +
'</div>';
    $.each(event, function (i, o) {
        html += '<div class="divTableRow">' +
'<div class="divTableCell text-center"><span>' + o.Email + '</span> </div>' +
'<div class="divTableCell text-center"><span>' + moment(moment.utc(o.TimeStamp).toDate()).format('YYYY-MM-DD HH:mm:ss') + '</span> </div>' +
'</div>'
    });
    html += '</div>' +'</div>'
    $('#table-show-contact').append(html);
    $('#modal-show-contact').modal('toggle');
    data_general_export = event;
}

function statistic_TableUniqueOpenHour(event) {
    $('.table-show-open').children().remove();
    var html = "";
   
    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">' +
'<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-center font-green-jungle"><h4>Hours</h4> </div>' +
'<div class="divTableCell text-center"> <h4>Opens</h4></div>' +
'</div>'
    $.each(event, function (i, o) {
        html += '<div class="divTableRow">' +
'<div class="divTableCell text-center"><h4>' + o.hour + '</h4> </div>' +
'<div class="divTableCell text-center"><h4>' + o.value + '<h4></div>' +
'</div>'
    });

    html += '</div>' +'</div>'
    $('.table-show-open').append(html);

  
}


function statistic_ChangeTypeClick(apiDomain, access_token,message) {
    //$('#change-clickorunique-clickchart').on('switchChange.bootstrapSwitch', function (event, state) {
    //    $('#divLoading').show();
    //    statistic_ClickChart(apiDomain, access_token,message);
    //});
    $('#tab-click-summary').on('click', function () {
        clicktype = 0;
       
        if (datasendclickts != undefined && datasendclickts != null) {
            statistic_PrepareDataSendClick(datasendclickts,message);

        } else {
            statistic_ClickChart(apiDomain, access_token, message);
        }
    });
    $('#tab-click-unique').on('click', function () {
        clicktype = 1;
      
        if (datasendclickts != undefined && datasendclickts != null) {
            statistic_PrepareDataSendClick(datasendclickts,message);

        } else {
            statistic_ClickChart(apiDomain, access_token, message);
        }
    })
}

function statistic_ChangeTypeUniqueClick(apiDomain, access_token,message) {
    //$('#change-type-of-unique-click').on('switchChange.bootstrapSwitch', function (event, state) {
    //    $('#divLoading').show();
    //    statistic_ClickChart(apiDomain, access_token,message);
    //});
    var data = null;
    $('#tab-click-unique-summary').on('click', function () {
        clickuniquetype = 0;
       
        if (datasendclickts != undefined && datasendclickts != null) {
            statistic_PrepareDataSendClick(datasendclickts,message);
           
        } else {
            statistic_ClickChart(apiDomain, access_token, message);
        }
    });
    $('#tab-click-unique-hour').on('click', function () {
        clickuniquetype = 1;
      
        if (datasendclickts != undefined && datasendclickts != null) {
            statistic_PrepareDataSendClick(datasendclickts,message);
            
        } else {
            statistic_ClickChart(apiDomain, access_token, message);
        }
    })
}

function statistic_ClickChart(apiDomain, access_token, message) {
   
        $('#divLoading').show();
        var getdate = custom_local2utc($('#select-date-from').val(), $('#select-date-to').val());
        var timezone = custom_GetTimeZone();
        var data = {
            CampaignId: $('#bs-select-campaign').val(),
            StartDate: getdate.StartDate, //$('#bs-select-campaign option:selected').data('subtext'),
            EndDate: getdate.EndDate,
        }
        $.ajax({
            url: apiDomain + 'api/Report/SendClick',
            type: 'POST',
            contentType: 'application/x-www-form-urlencoded',
            data: data,
            beforeSend: function (request) {

                request.setRequestHeader("Authorization", "Bearer " + access_token);
                request.setRequestHeader("timezone", timezone);
            },
            success: function (r) {
                datasendclickts = r;
                statistic_PrepareDataSendClick(datasendclickts,message);
            },
            error: function (r) {
                custom_shownotification('error', message.GetDataError)
                $('#divLoading').hide();
            }
        });

    
}


function statistic_PrepareDataSendClick(data,message)
{
    switch (parseInt(data.ErrorCode)) {
        case 0:  //lấy data thành công
            //  console.log(r.Data);
            var object = data.Data;
            if (object.TotalClick == 0) {
                if ($('#nodata-click').hasClass('hide')) {
                    $('#nodata-click').removeClass('hide');
                }
                if (!$('#chartdiv-click').hasClass('hide')) {
                    $('#chartdiv-click').addClass('hide')
                }

                $('#show-table-click').children().remove();
            }
            else {
                if (!$('#nodata-click').hasClass('hide')) {
                    $('#nodata-click').addClass('hide');
                }
                if ($('#chartdiv-click').hasClass('hide')) {
                    $('#chartdiv-click').removeClass('hide')
                }



                var bdata = object.Events;
                //  console.log(JSON.stringify(bdata));
                var data = [];
                for (var j = 0; j < bdata.length; j++) {
                    for (var i = 0 ; i < selectedlink.length; i++) {
                        if ((selectedlink[i].toLowerCase().localeCompare(bdata[j].Link.toLowerCase())) == 0) {
                            data.push(bdata[j]);
                        }
                    }
                }

                console.log('data', JSON.stringify(data));

                var bevent = new Array();
                var event = new Array();

                //var StartDate = $('#select-date-from').val();
                //var EndDate = $('#select-date-to').val();
                var day = (moment(data[0].TimeStamp).diff(data[data.length - 1].TimeStamp, 'days'));

                //clicked
                console.log(clicktype);
                if (clicktype == 0) {

                    if (!$('#div-type-of-unique-click').hasClass('hide')) {
                        $('#div-type-of-unique-click').addClass('hide')
                    }


                    var a = _(data).groupBy(function (o) {
                        return moment(o.TimeStamp).format('YYYY-MM-DD');
                    });
                    //   console.log('c1' + JSON.stringify(a));
                    console.log('a ' + JSON.stringify(a));
                    for (i = 0; i <= day; i++) {
                        var value = 0;
                        var startdate = new Date(data[data.length - 1].TimeStamp);
                        var dateofevent = moment(startdate.setDate(startdate.getDate() + i)).format('YYYY-MM-DD');
                        console.log('console.log(dateofevent);:' + dateofevent);
                        var e = { 'value': 0, 'date': dateofevent }

                        $.each(a, function (i, o) {
                            if (moment(dateofevent).format('YYYY-MM-DD') == i) {
                                value = o.length
                            }
                        });
                        e.value = value;
                        event.push(e);


                    };
                    //chart

                    statistic_ChartClick(event)
                    //table show data
                    statistic_TableClick(object);
                    console.log(event);




                }
                    //uniqueclick
                else if (clicktype == 1) {

                    //summary
                    if (clickuniquetype == 0) {

                        if ($('#div-type-of-unique-click').hasClass('hide')) {
                            $('#div-type-of-unique-click').removeClass('hide')
                        }

                        var uniquedata = data.uniqueobj();
                        var a = _(uniquedata).groupBy(function (o) {
                            return moment(o.TimeStamp).format('YYYY-MM-DD');
                        })
                        //      console.log('c2' + JSON.stringify(a));
                        for (i = 0; i <= day; i++) {
                            var value = 0;
                            var startdate = new Date(data[data.length - 1].TimeStamp);
                            var dateofevent = moment(startdate.setDate(startdate.getDate() + i)).format('YYYY-MM-DD');
                            var e1 = { 'value': 0, 'date': dateofevent }
                            $.each(a, function (i, o) {
                                if (moment(dateofevent).format('YYYY-MM-DD') == i) {
                                    value += o.length
                                }
                            });
                            e1.value = value;
                            event.push(e1);


                        }
                        //chart
                        statistic_ChartClick(event)
                        //table show data
                        statistic_TableUniqueClick(object);
                    }
                        //hour
                    else if (clickuniquetype == 1) {
                        var hour = [00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
                        var listunique = data.uniqueobj();

                        var groups = _.groupBy(listunique, function (value) {
                            var h = new Date(value.TimeStamp);
                            return h //moment(new Date(value.TimeStamp)).format('hh');
                        });

                        //       console.log('g' + JSON.stringify(groups));

                        var bevent = _.map(groups, function (group) {
                            return {
                                Hour: new Date(group[0].TimeStamp).getHours(),//moment(new Date(group[0].TimeStamp)).format('hh'),
                                Count: _.pluck(group, 'ContactId')
                            }
                        });
                        //        console.log('g2:' + JSON.stringify(bevent));

                        for (i = 0; i < hour.length; i++) {
                            var value = 0;

                            var e2 = { 'value': 0, 'hour': hour[i] }
                            $.each(bevent, function (idx, o) {
                                if (parseInt(hour[i]) == parseInt(o.Hour)) {
                                    value += o.Count.length;


                                }
                            });
                            e2.value = value;
                            event.push(e2);

                        }
                        //          console.log(event);
                        //char
                        CharClickUniqueHour(event);
                        //table
                        statistic_TableUniqueClickHour(event);
                    }


                }

            }
            $('#divLoading').hide();
            break;
        case 101:
            //không có gửi email
            if ($('#nodata-click').hasClass('hide')) {
                $('#nodata-click').removeClass('hide');
            }
            if (!$('#chartdiv-click').hasClass('hide')) {
                $('#chartdiv-click').addClass('hide')
            }
            $('#divLoading').hide();
            break;
        default:
            custom_shownotification('error', message.GetDataError)
            $('#divLoading').hide();
            break;
    }
}

function statistic_ChartClick(event)
{
    var chart = AmCharts.makeChart("chartdiv-click", {
        "type": "serial",
        "theme": "light",
        "marginRight": 40,
        "marginLeft": 40,
        "autoMarginOffset": 20,
        "mouseWheelZoomEnabled": false,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id": "v1",
            "axisAlpha": 0,
            "position": "left",
            "ignoreAxisWidth": true
        }],
        "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0
        },
        "graphs": [{
            "id": "g1",
            "balloon": {
                "drop": true,
                "adjustBorderColor": false,
                "color": "#ffffff"
            },
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "title": "red line",
            "useLineColorForBulletBorder": true,
            "valueField": "value",
            "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
        }],
        "chartScrollbar": {
            "graph": "g1",
            "oppositeAxis": false,
            "offset": 30,
            "scrollbarHeight": 30,
            "backgroundAlpha": 0,
            "selectedBackgroundAlpha": 0.1,
            "selectedBackgroundColor": "#888888",
            "graphFillAlpha": 0,
            "graphLineAlpha": 0.5,
            "selectedGraphFillAlpha": 0,
            "selectedGraphLineAlpha": 1,
            "autoGridCount": true,
            "color": "#AAAAAA"
        },
        "chartCursor": {
            "pan": true,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "cursorAlpha": 1,
            "cursorColor": "#258cbb",
            "limitToGraph": "g1",
            "valueLineAlpha": 0.2,
            "valueZoomable": true
        },
        "valueScrollbar": {
            "oppositeAxis": false,
            "offset": 50,
            "scrollbarHeight": 10
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true
        },
        "export": {
            "enabled": true
        },
        "dataProvider": event
    });

    chart.addListener("rendered", zoomChart);

    zoomChart();

    function zoomChart() {
        chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
    }

  
}

function CharClickUniqueHour(event) {
    var chart = AmCharts.makeChart("chartdiv-click", {
        "theme": "light",
        "type": "serial",
        "mouseWheelZoomEnabled": false,
        "dataProvider": event,
        "valueAxes": [{
            "title": "",
        }],
        "graphs": [{
            "balloonText": "Xem lúc [[category]]h : Số lượng [[value]]",
            "fillAlphas": 1,
            "lineAlpha": 0.2,
            "title": "Income",
            "type": "column",
            "valueField": "value"
        }],
        "depth3D": 0,
        "angle": 0,
        "rotate": true,
        "categoryField": "hour",
        "categoryAxis": {
         
            "gridPosition": "start",
            "fillAlpha": 0.05,
            "position": "left"
        },
        "export": {
            "enabled": true
        }
    });

}


function statistic_TableClick(data) {
    $('#show-table-click').children().remove();
    var html = "";
    var dt = _(data.Events).groupBy(function (o) {
        return o.Link.toLowerCase();
    });
   // console.log('table:' + JSON.stringify(dt));

    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">' +
'<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-center font-green-jungle"><h4><b>' + $('#bs-select-campaign option:selected').text() + '</b></h4> </div>' +
'<div class="divTableCell text-center"> <h4>Action</h4></div>' +
'</div>'

    $.each(dt, function (i, o) {
            html+='<div class="divTableRow">' +
'<div class="divTableCell text-left"><h4>' + o.length + ' Click on</h4><h4 id="linkontable">'+i+'</h4></div>' +
'<div class="divTableCell text-center"><h4><a id="show-click-contact"><span class="icon-users"></span> Show contacts</a><h4></div>' +
'</div>' 
 
    });
    html += '</div>' +'</div>';

    $('#show-table-click').append(html);


    $('#show-table-click #show-click-contact').on('click', function () {
        var link = $('#show-table-click').children().find('#linkontable').text();
        $('#modal-show-contact #table-show-contact').children().remove();
        statistic_ShowClickContact(data, link);
    });

}

function statistic_TableUniqueClick(data) {
    $('#show-table-click').children().remove();
    var html = "";
    var dt = _(data.Events).groupBy(function (o) {
        return o.Link.toLowerCase();
    });
  //  console.log('table:' + JSON.stringify(dt));

    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">' +
'<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-center font-green-jungle"><h4><b>' + $('#bs-select-campaign option:selected').text() + '</b></h4> </div>' +
'<div class="divTableCell text-center"> <h4>Action</h4></div>' +
'</div>'

    $.each(dt, function (i, o) {
        html += '<div class="divTableRow">' +
'<div class="divTableCell text-left"><h4>' + o.uniqueobj().length + ' Click on</h4><h4 id="linkuniqueontable">' + i + '</h4></div>' +
'<div class="divTableCell text-center"><h4><a id="show-click-unique-contact"><span class="icon-users"></span> Show contacts</a><h4></div>' +
'</div>'

    });
    html += '</div>' + '</div>';

    $('#show-table-click').append(html);


    $('#show-table-click #show-click-unique-contact').on('click', function () {
        var link = $('#show-table-click').children().find('#linkuniqueontable').text();
        $('#modal-show-contact #table-show-contact').children().remove();
        statistic_ShowClickUniqueContact(data, link);
    });

}

function  statistic_ShowClickContact(data, link)
{
    var html = "";
    var contact = new Array();
    $.each(data.Events, function (i, o) {
        if ((o.Link.toLowerCase().localeCompare(link)) == 0)
        {
            contact.push(o);
        }
    });
    
    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">' +
        '<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-left"><h4 class="nomargin">Email</h4> </div>' +
'<div class="divTableCell text-center"> <h4 class="nomargin">Date</h4></div>' +
'</div>';
    $.each(contact, function (i, o) {
        html += '<div class="divTableRow">' +
'<div class="divTableCell text-center"><span>' + o.Email + '</span> </div>' +
'<div class="divTableCell text-center"><span>' + moment(moment.utc(o.TimeStamp).toDate()).format('YYYY-MM-DD HH:mm:ss') + '</span> </div>' +
'</div>'
    });
    html += '</div>' + '</div>'
    $('#table-show-contact').append(html);
    $('#modal-show-contact').modal('toggle');
    data_general_export = contact;
}

function statistic_ShowClickUniqueContact(data, link) {
    var html = "";
    var contact = new Array();
    $.each(data.Events, function (i, o) {
        if ((o.Link.toLowerCase().localeCompare(link)) == 0) {
            contact.push(o);
        }
    });

    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">' +
        '<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-left"><h4 class="nomargin">Email</h4> </div>' +
'<div class="divTableCell text-center"> <h4 class="nomargin">Date</h4></div>' +
'</div>';
    var uniquecontact = contact.uniqueobj();
    $.each(uniquecontact, function (i, o) {
        html += '<div class="divTableRow">' +
'<div class="divTableCell text-center"><span>' + o.Email + '</span> </div>' +
'<div class="divTableCell text-center"><span>' + moment(moment.utc(o.TimeStamp).toDate()).format('YYYY-MM-DD HH:mm:ss') + '</span> </div>' +
'</div>'
    });
    html += '</div>' + '</div>'
    $('#table-show-contact').append(html);
    $('#modal-show-contact').modal('toggle');
    data_general_export = uniquecontact;
}


var data_general_export = [];
function statistic_ExportContact() {
    $('#export-general-statistic').on('click', function () {
        $('#divLoading').show();
        var list = [];
        var head = ["Email", "Date"];
        list.push(head);
        $.each(data_general_export, function (i, o) {
            var obj = [];
            obj.push(o.Email);
            obj.push(moment(moment.utc(o.TimeStamp).toDate()).format('YYYY-MM-DD HH:mm:ss'));
            list.push(obj);
        })
        write_exportContact(list);
    });
}


function statistic_BounceChart(apiDomain, access_token,message) {
    $('#divLoading').show();
    var getdate = custom_local2utc($('#select-date-from').val(), $('#select-date-to').val());
    var timezone = custom_GetTimeZone();
    var data = {
        CampaignId: $('#bs-select-campaign').val(),
        StartDate: getdate.StartDate, //$('#bs-select-campaign option:selected').data('subtext'),
        EndDate: getdate.EndDate,
    };
    $.ajax({
        url: apiDomain + 'api/Report/SendBounce',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: data,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success: function (r) {
            datasendbounce = r;
            statistic_GenBounceChart(datasendbounce, message);
           
        }, error: function () {
            custom_shownotification('error', message.GetDataError)
            $('#divLoading').hide();
        }
    });

}

function statistic_GenBounceChart(r, message) {
    switch (parseInt(r.ErrorCode)) {
        case 0:

            var data = JSON.parse(JSON.stringify(r.Data));

            if (data.TotalBounce > 0) {

                if (!$('#nodata-bounce').hasClass('hide')) {
                    $('#nodata-bounce').addClass('hide');
                }
                if ($('#chartdiv-bounce').hasClass('hide')) {
                    $('#chartdiv-bounce').removeClass('hide')
                }
                if ($('#total-bounce').hasClass('hide')) {
                    $('#total-bounce').removeClass('hide')
                }

                $('#total-bounce').text(data.TotalBounce);

                //gen chart
                var title = message.TotalBounce;
                Highcharts.getOptions().plotOptions.pie.colors = (function () {
                    // var colors = ["#FF0000"],
                    var colors = ["#FF0000", "rgb(204, 204, 204)"],
                        base = Highcharts.getOptions().colors[0],
                        i;

                    for (i = 0; i < 5; i += 1) {
                        // Start out with a darkened base color (negative brighten), and end
                        // up with a much brighter color
                        colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
                    }
                    return colors;
                }());
                // Build the chart
                $('#chartdiv-bounce').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: title + ': ' + data.TotalBounce
                    },
                    tooltip: {
                        pointFormat: '<b>số lượng {point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false,
                                format: '<b>{point.name}</b>:  {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: '',
                        data: [
                            { name: 'Bounce', y: data.TotalBounce },
                            { name: 'Khác', y: data.Total - data.TotalBounce }
                        ]
                    }]
                });

            }
            else {
                //       console.log(JSON.parse(JSON.stringify(r.Data)));
                if ($('#nodata-bounce').hasClass('hide')) {
                    $('#nodata-bounce').removeClass('hide');
                }
                if (!$('#chartdiv-bounce').hasClass('hide')) {
                    $('#chartdiv-bounce').addClass('hide');
                }
                if (!$('#total-bounce').hasClass('hide')) {
                    $('#total-bounce').addClass('hide');
                }
            }
            $('#divLoading').hide();
            break;
        case 101:
            //chỉ gửi sms nên không có thông kê email
            if ($('#nodata-bounce').hasClass('hide')) {
                $('#nodata-bounce').removeClass('hide');
            }
            if (!$('#chartdiv-bounce').hasClass('hide')) {
                $('#chartdiv-bounce').addClass('hide');
            }
            if (!$('#total-bounce').hasClass('hide')) {
                $('#total-bounce').addClass('hide');
            }
            $('#divLoading').hide();
            break;
        default:
            custom_shownotification('error', message.GetDataError)
            $('#divLoading').hide();
            break;

    }
   
}


function statistic_ComplaintsChart(apiDomain, access_token, message) {
    $('#divLoading').show();
    var getdate = custom_local2utc($('#select-date-from').val(), $('#select-date-to').val());
    var timezone = custom_GetTimeZone();
    var data = {
        CampaignId: $('#bs-select-campaign').val(),
        StartDate: getdate.StartDate, //$('#bs-select-campaign option:selected').data('subtext'),
        EndDate: getdate.EndDate,
    }
    $.ajax({
        url: apiDomain + 'api/Report/SendReport',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: data,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success: function (r) {
            datasendts = r;
            statistic_GenComplaintsChart(datasendts, message);
           
        }, error: function () {
            custom_shownotification('error', message.GetDataError)
            $('#divLoading').hide();
        }
    });
}


function statistic_GenComplaintsChart(r, message) {
    var data = JSON.parse(JSON.stringify(r.Data));
    if (data.SpamComplaint > 0) {

        if (!$('#nodata-complaints').hasClass('hide')) {
            $('#nodata-complaints').addClass('hide');
        }
        if ($('#chartdiv-complaints').hasClass('hide')) {
            $('#chartdiv-complaints').remove('hide')
        }


        //gen chart
        Highcharts.getOptions().plotOptions.pie.colors = (function () {
            var colors = ["#FF0000", "rgb(204, 204, 204)"],
                base = Highcharts.getOptions().colors[0],
                i;

            for (i = 0; i < 10; i += 1) {
                // Start out with a darkened base color (negative brighten), and end
                // up with a much brighter color
                colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
            }
            return colors;
        }());
        // Build the chart
        $('#chartdiv-complaints').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: '' // message.TotatBounce + data.TotalBounce
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}</b>: {point.y} ',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: '',
                data: [
                    { name: 'Khác', y: data.Total - data.SpamComplaint },
                    { name: 'Spam complaint', y: data.SpamComplaint }, ]
            }]
        });
        setTimeout(function () {
            var chart = $('#chartdiv-complaints').highcharts();

            console.log('redraw');
            var w = $('#chartdiv-complaints').innerWidth();
            var h = $('#chartdiv-complaints').innerHeight();
            // setsize will trigger the graph redraw 
            chart.setSize(
                w, h, false
            );
        }, 10);

    }
    else {
        //    console.log(JSON.parse(JSON.stringify(r.Data)));
        if ($('#nodata-complaints').hasClass('hide')) {
            $('#nodata-complaints').removeClass('hide');
        }
        if (!$('#chartdiv-complaints').hasClass('hide')) {
            $('#chartdiv-complaints').addClass('hide');
        }
    }
   
}

function statistic_UnsubcribedChart(apiDomain, access_token, message) {
    $('#divLoading').show();
    var getdate = custom_local2utc($('#select-date-from').val(), $('#select-date-to').val());
    var timezone = custom_GetTimeZone();
    var data = {
        CampaignId: $('#bs-select-campaign').val(),
        StartDate: getdate.StartDate, //$('#bs-select-campaign option:selected').data('subtext'),
        EndDate: getdate.EndDate,
    }
    $.ajax({
        url: apiDomain + 'api/Report/SendReport',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: data,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success: function (r) {
            datasendts = r;
            statistic_GenUnsubcribedChart(datasendts, message);
          
        }, error: function () {
            custom_shownotification('error', message.GetDataError)
            $('#divLoading').hide();
        }
    });


}

function statistic_GenUnsubcribedChart(r, message) {
    switch (parseInt(r.ErrorCode)) {
        case 0:


            var data = JSON.parse(JSON.stringify(r.Data));

            //gán data vào biến

            if (data.Unsubscribe > 0) {

                if (!$('#nodata-unsubcribed').hasClass('hide')) {
                    $('#nodata-unsubcribed').addClass('hide');
                }
                if ($('#chartdiv-unsubcribed').hasClass('hide')) {
                    $('#chartdiv-unsubcribed').remove('hide')
                }

                //gen chart
                var event = { 'key': 'Unsubscribe', 'value': data.Unsubscribe }
                Highcharts.getOptions().plotOptions.pie.colors = (function () {
                    var colors = ["#FF0000", "rgb(204, 204, 204)"],
                        base = Highcharts.getOptions().colors[0],
                        i;

                    for (i = 0; i < 10; i += 1) {
                        // Start out with a darkened base color (negative brighten), and end
                        // up with a much brighter color
                        colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
                    }
                    return colors;
                }());
                // Build the chart
                $('#chartdiv-unsubcribed').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Total: ' + data.Unsubscribe // message.TotatBounce + data.TotalBounce
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: '',
                        data: [{ name: 'Unsubcribed', y: data.Unsubscribe }, { name: 'Khác', y: data.Total - data.Unsubscribe }, ]
                    }]
                });

                setTimeout(function () {
                    var chart = $('#chartdiv-unsubcribed').highcharts();

                    console.log('redraw');
                    var w = $('#chartdiv-unsubcribed').innerWidth();
                    var h = $('#chartdiv-unsubcribed').innerHeight();
                    // setsize will trigger the graph redraw 
                    chart.setSize(
                        w, h, false
                    );
                }, 10);

            }
            else {
                //    console.log(JSON.parse(JSON.stringify(r.Data)));
                if ($('#nodata-unsubcribed').hasClass('hide')) {
                    $('#nodata-unsubcribed').removeClass('hide');
                }
                if (!$('#chartdiv-unsubcribed').hasClass('hide')) {
                    $('#chartdiv-unsubcribed').addClass('hide');
                }
            }
            $('#divLoading').hide();
            break;
        case 101:
            if ($('#nodata-unsubcribed').hasClass('hide')) {
                $('#nodata-unsubcribed').removeClass('hide');
            }
            if (!$('#chartdiv-unsubcribed').hasClass('hide')) {
                $('#chartdiv-unsubcribed').addClass('hide');
            }

            $('#divLoading').hide();
            break;
        default:
            custom_shownotification('error', message.GetDataError)
            $('#divLoading').hide();
            break;

    }

   
}

function statistic_TableUniqueClickHour(event) {
    $('#show-table-click').children().remove();
    var html = "";

    var html = '<div class="divTable" style="width: 100%;">' +
'<div class="divTableBody">' +
'<div class="divTableRow divHeadTableRow">' +
'<div class="divTableCell text-center font-green-jungle"><h4>Hours</h4> </div>' +
'<div class="divTableCell text-center"> <h4>Opens</h4></div>' +
'</div>'
    $.each(event, function (i, o) {
        html += '<div class="divTableRow">' +
'<div class="divTableCell text-center"><h4>' + o.hour + '</h4> </div>' +
'<div class="divTableCell text-center"><h4>' + o.value + '<h4></div>' +
'</div>'
    });

    html += '</div>' + '</div>'
    $('#show-table-click').append(html);


}


function statistics_GetCampaignInfo(apiDomain, access_token, CampaignId, message)
{
    $.ajax({
        url: apiDomain + 'api/BasicCampaign/GetInfo?CampaignId='+CampaignId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success:function(r)
        {
            switch(parseInt(r.ErrorCode))
            {
                case 0:
                    var data = r.Data;
                    if (parseInt(data.SendMethod) == 2 || parseInt(data.SendMethod) == 1) { //có gửi sms

                        if (parseInt(data.SendMethod) == 1) {
                            $('#tab-statistics-email').hide();
                            $('#tab-statistics-sms').show();
                            $('#tab-statistics-sms a').trigger('click');

                        }
                        else {
                            $('#tab-statistics-sms').show();
                            $('#tab-statistics-email').show();
                            $('#tab-statistics-email a').trigger('click');
                        }

                        var SentId = data.SMSSend.SentId;
                        statisticmessage_GetReport(apiDomain, access_token, SentId, message);
                       
                        var height = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.portlet-title').outerHeight() - $('#campaign-list-statistic').outerHeight() - $('.tabbable-line').outerHeight() - $('#chartdiv-sendsms').outerHeight() - $('#div-sent-sms-status').outerHeight() - $('.page-footer').outerHeight();
                        $('#list-mobil-sent').slimScroll({
                            height: height - 120,
                            width: '100%',
                            alwaysVisible: true
                        });
                    }
                    else
                    {
                        $('#tab-statistics-email a').trigger('click');
                        $('#tab-statistics-sms').hide();
                    }
                    break;
                default:
                    $('#tab-statistics-email a').trigger('click');
                    $('#tab-statistics-sms').hide();
                    break;
            }
        },
        error:function(x,s,e)
        {
            $('#tab-statistics-email a').trigger('click');
            $('#tab-statistics-sms').hide();
        }
    })
}

function statistic_Export(apiDomain, access_token, message)
{
    $('#select-date-export').on('click', function () {
        $('#file-export-name').prop('disabled', false);
        $('#submit-export-statistic-contact').prop('disabled', false);
        $('#table-file-export').children().remove();
        var name = 'Export rocket ' + moment().format('YYYY-MM-DD-HH-mm');
        $('#file-export-name').val(name);
        $('#modal-export-statistic-contact').modal('toggle');
    });
}

var checkfileexport;
function statistic_CreateExport(apiDomain, access_token, message) {

    var getdate = custom_local2utc($('#select-date-from').val(), $('#select-date-to').val());
    var timezone = custom_GetTimeZone();
    var FileName = $('#file-export-name').val();
    var data = {
        FileType: 'xlsx',
        FileName: FileName.replace(/\s/g, "-"),
        CampaignId: $('#bs-select-campaign').val(),
        StartDate: getdate.StartDate, //$('#bs-select-campaign option:selected').data('subtext'),
        EndDate: getdate.EndDate,

    };
    $.ajax({
        url: apiDomain + 'api/report/CreateCampaignReportExport',
        type: 'post',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
            request.setRequestHeader('timezone', timezone);
        },
        success: function (r) {

            switch (parseInt(r.ErrorCode)) {

                case 0:
                    {
                        var data = r.Data;
                        var ExportId = data.ExportId;
                        switch (parseInt(data.Status)) {
                            case 0: //pendding
                                checkfileexport = setInterval(function () { statistic_CheckExportComplete(apiDomain, access_token, ExportId, message); }, 3000);
                                break;
                            case 1:
                                var html = '<div><a href=' + data.FilePath + '><i class="fa fa-cloud-download font-green-jungle"></i> ' + message.DownLoad + '</div>';
                                setTimeout(function () {
                                    $('#status-export').children().remove();
                                    $('#status-export').append(html)
                                }, 3000);
                                break;
                            case 2:
                                var html = '<div><i class="fa fa-remove font-red"></i> ' + message.ExportError + ' </div>';
                                $('#status-export').children().remove();
                                $('#status-export').append(html);
                                break;

                        }
                    }
                    break;
                default:
                    custom_shownotification("error", message.ExportError);
                    break;


            }
        },
        error: function (x, s, e) {
            custom_shownotification("error", message.ExportError);
        }
    })
}

function statistic_CheckExportComplete(apiDomain, access_token, ExportId, message) {
    $.ajax({
        url: apiDomain + 'api/report/GetContactExport?ExportId=' + ExportId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);

        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    console.log(data);
                    switch (parseInt(data.Status)) {
                        case 1:
                            var html = '</div><a href=' + data.FilePath + '><i class="fa fa-cloud-download font-green-jungle"></i>' + message.DownLoad + ' </a></div>';
                            setTimeout(function () {
                                $('#status-export').children().remove();
                                $('#status-export').append(html);
                            }, 3000);
                            clearInterval(checkfileexport);
                            break;
                        case 2:
                            var html = '<div><i class="fa fa-remove font-red"></i>' + message.ExportError + '</div>';
                            $('#status-export').children().remove()
                            $('#status-export').append(html);
                            clearInterval(checkfileexport);
                            break;

                    }
                    break;

            }
        },
        error: function (x, s, e) {
            custom_shownotification("error", 'Có lỗi xảy ra');
            clearInterval(checkfileexport);
        }
    })
}


function statistic_SubmitExport(apiDomain, access_token, message) {
    $('#submit-export-statistic-contact').on('click', function () {
      
        $('#table-file-export').children().remove();
        if ($('#file-export-name').val()) {

            var html = '<table style="width:100%;" class="table table-striped">' +
                                   ' <thead>' +
                                       ' <tr>' +
                                       '   <td>' +
                                           message.FileName +
                                           ' </td>' +
                                          '  <td>' +
                                           message.Status +
                                                    '</td>' +
                                                    '<td>' +
                                                     message.CreateDate +
                                                  '  </td>' +
                                             '   </tr>' +
                                           '  </thead>' +
                                          '  <tbody>' +
                                              '  <tr>' +
                                                  '  <td>' +
                                                          $('#file-export-name').val() +
                                                  '  </td>' +
                                                 '   <td id="status-export">' +
                                                      '<div><i class="fa fa-spinner fa-pulse fa-fw"></i> ' + message.Processing + ' </div>' +
                                                   ' </td>' +
                                                 '   <td>' +
                                                     moment().format('YYYY-MM-DD HH:mm') +
                                                   ' </td>' +
                                              '  </tr>' +
                                           ' </tbody>' +
                                      '  </table>';
            $('#table-file-export').append(html);
            $('#file-export-name').prop('disabled', true);
            $('#submit-export-suppression-contact').prop('disabled', true);
            statistic_CreateExport(apiDomain, access_token, message);


        }
        else {
            custom_shownotification("error", message.FileNameRequire);
        }
    })
}


////unique in array
//Array.prototype.unique = function unique() {
//    var arr = [];
//    for (var i = 0; i < this.length; i++) {
//        if (!arr.contains(this[i])) {
//            arr.push(this[i]);
//        }
//    }
//    return arr;
//}

//Array.prototype.contains = function (v) {
//    for (var i = 0; i < this.length; i++) {
//        if (this[i] === v) return true;
//    }
//    return false;
//};

//unique object

//uniquelist theo email

function statistics_NoCampaign()
{
    $('#statistics-campaign-tab').hide();
    $('#nodata-statistic').show();
}

Array.prototype.uniqueobj = function uniqueobj()
{
    var flags = [], output = [], l = this.length, i;
    for (i = 0; i < l; i++) {
        if (flags[this[i].Email]) continue;
        flags[this[i].Email] = true;
        output.push(this[i]);
    }
 //   console.log('fsfas:' + output);
    return output;
}
