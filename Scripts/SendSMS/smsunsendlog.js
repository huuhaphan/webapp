﻿function smsunsend_Action(apiDomain,access_token,message)
{
    smsunsend_GetLogsUnsend(apiDomain, access_token, message);
    smsunsend_ChangeDate(apiDomain, access_token, message);
}

function smsunsend_GetLogsUnsend(apiDomain, access_token, message) {
    var data = {
        
        StartDate:moment($('#select-date-from').datepicker('getUTCDate')).format('YYYY-MM-DD'),
        EndDate: moment($('#select-date-to').datepicker('getUTCDate')).format('YYYY-MM-DD'),
        IsPaid: null,
    }
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/User/GetSMSUnsendLogs?StartDate='+data.StartDate+'&EndDate='+data.EndDate+'&IsPaid='+data.IsPaid,
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            $('#tbody-smsunsentlogs').children().remove();
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    $('#divLoading').hide();
                    if (r.Data.length > 0) {
                        var html = '';
                        $('#total-smsunsend').text(r.Data.length);
                        list_query_export = r.Data;
                        $.each(r.Data, function (i, o) {
                            var char = o.Mobile.charAt(0);
                            var randomColor = Math.floor(Math.random() * 16777215).toString(16);
                            var isOk = /^#[0-9A-F]{6}$/i.test('#' + randomColor);
                            if (isOk == true) {
                                var Mobile = '<div class="email-width-bkg"><span class="imgletter" style="text-transform: uppercase; ;background-color:#' + randomColor + ';margin-right:10px;">' + char + '</span><span style="font-weight:500;font-size:15px;">' + o.Mobile + '</span></div>';
                            }
                            else {

                                var Mobile = '<div class="email-width-bkg"><span class="imgletter" style="text-transform: uppercase; ;background-color:#7d44c6;margin-right:10px;">' + char + '</span><span style="font-weight:500;font-size:15px;">' + o.Mobile + '</span></div>';

                            }
                            var IsRefund = "";
                            if (o.IsRefund) {
                                IsRefund = '<i class="fa fa-check font-green"></i>';
                            } else {
                                IsRefund = '<i class="fa fa-remove font-red"></i>';
                            }
                            html += '<tr>' +
                                '<td style="width:15%;font-weight:500;font-size:15px !important;"> ' + o.Mobile + ' </td>' +
                                 '<td style="width:15%;" class="text-center"> ' + o.Carrier + ' </td>' +
                                '<td style="width:35%;"> ' + o.Message + ' </td>' +
                                '<td style="width:15%;"> ' + moment(moment.utc(o.CreateDate).toDate()).format('YYYY-MM-DD HH:mm') + ' </td>' +
                                '<td style="width:10%;" class="text-center"> ' + o.Amount + ' </td>' +
                                '<td style="width:10%;" class="text-center"> ' + IsRefund + ' </td>' +
                               
                            '</tr>';

                        });

                    }
                    else {
                        html += '<tr style="text-align:center;">' +
                                '<td colspan="3">' + message.NoData + '</td>' +

                            '</tr>';
                        $('#total-smsunsend').text('0');
                    }
                    var height = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.portlet-title').outerHeight() - $('#divAction-toolbar').outerHeight() - $('.page-footer').outerHeight() - 110;
                    $('.inner_table').css({
                        'height': height - 65,
                    })
                    $('#tbody-smsunsentlogs').append(html);
                    break;
                default:
                    custom_shownotification("error", message.HaveError);
                    $('#divLoading').hide();
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification("error", message.HaveError);
        }

    })
}

function smsunsend_ChangeDate(apiDomain, access_token, message) {
    $('#select-date-unsendlog').on('click', function () {
        smsunsend_GetLogsUnsend(apiDomain, access_token, message);
    })
}