﻿/// <reference path="../appshare.js" />
/// <reference path="../custom.js" />

function sendsms_Action(apiDomain, access_token, SMSSendId, IdAdd, message)
{
    sendsms_LoadFullCustomField(apiDomain, access_token, message);
    appshare_GetSMSServiceEnables(apiDomain, access_token, message);
    sendsms_GetBrandName(apiDomain, access_token, message);
    sendsms_GetDevice(apiDomain, access_token, message);
    sendsms_LoadContactGroup(apiDomain, access_token, message);
    sendsms_LoadInfo(apiDomain, access_token, SMSSendId, message); 
    sendsms_AddPersonalize(IdAdd, message)
    sendsms_SelectContactGroup(apiDomain, access_token, message);
    sendsms_ResetContact();
    sendsms_UpdateMessageCount(message);
    sendsms_SMSBind(message);
    sendsms_ShowBrandName(message);
    sendsms_Save(apiDomain, access_token, SMSSendId, message);
    sendsms_Send(apiDomain, access_token, SMSSendId, message);

    sendsms_SelectSchedule(message);
    sendsms_ActionSchedule(apiDomain, access_token, SMSSendId, message);

    sendsms_OpenModalAddFromContacts();
    sendsms_OpenModalAddFromExcel();
    sendsms_TagInput();
    sendsms_GenTimeSelect();

}


function sendsms_LoadFullCustomField(apiDomain, access_token, message) {
    var data = appshare_LoadFullField(apiDomain, access_token, message);
    switch (data.ErrorCode) {
        case 0:
            localStorage.setItem('FullCustomField',JSON.stringify(data.Data));
            break;
    }
}


function sendsms_SelectSchedule(message) {
    $('#btn-sendsms-schedule').on('click', function () {
      
        if (sendsms_CheckInfo(message)) {
            var date = new Date();
            //setdate = sendsms_dateAdd(date, 5);
            //var hour = setdate.getHours();
           
            //var minus = setdate.getMinutes();
            //if (parseInt(hour) < 10) {
            //    if (minus < 10) {
            //        $('#time-schedule-send-sms').val('0' + hour + ':' + '0' + minus);
            //    }
            //    else
            //    {
            //        $('#time-schedule-send-sms').val('0' + hour + ':' + minus);
            //    }
            //}
            //else {
            //    if (minus < 10) {
            //        $('#time-schedule-send-sms').val(hour + ':' + '0' + minus);
            //    }
            //    else {
            //        $('#time-schedule-send-sms').val(hour + ':' +  minus);
            //    }
            //}
            //
            var dateformat = moment().format('MM-DD-YYYY');
            $('#date-schedule-send-sms').datepicker('update', dateformat);
            $("#modal-schedule-send-sms").modal('toggle');
        }
    });

    
};

//add minus
function sendsms_dateAdd(date, units) {
    var ret = new Date(date);
    ret.setTime(ret.getTime() + units * 60000);

    return ret;
}

function sendsms_LoadInfo(apiDomain, access_token, SMSSendId, message)
{
    

    //nếu có sentid thì load không có thì không load
    if (SMSSendId != '-1') {
      
        $.ajax({
            url: apiDomain + 'api/SmsSend/GetInfo?SentId=' + SMSSendId,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            success: function (r) {
               
                switch (parseInt(r.ErrorCode)) {
                    case 0:
                        var data = JSON.parse(JSON.stringify(r.Data));

                        if (parseInt(data.SMSTypeId) == 2) {
                            $("#smstypeadvanced").prop("checked", true);
                            $('#select-sendsms-brandname').val(data.BrandNameId);
                            $('#sendsms-brandname').removeClass('hide');
                        }
                        if (parseInt(data.SMSTypeId) == 3) {
                            $('#smstypegetway').prop("checked", true);
                            $('#sendsms-device').removeClass('hide');
                            $('#select-sendsms-device').val(data.GatewayConfigId);
                            
                            $('#select-sms-input-codau').show();
                            if (custom_matchTiengViet(data.Message) != null) { //có dấu
                                $('input#smscodau').prop('checked', true);
                            }
                        }

                        $('#content-sms').val(data.Message);
                        $('#sendsms-campaign-name').val(data.MessageName);
                        sendsms_UpdateMessageCount(message);

                        $.each(data.SendContacts, function (index, obj) {
                            $('#sendsms-list-mobile-number').tagsinput('add', obj.Mobile);
                        });
                        if (r.Data.ContactGroupId != null && r.Data.ContactGroupId != "")
                        {
                            ContactGroupSendSMS=r.Data.ContactGroupId;
                           
                        }

                        break;
                    default:
                        break;
                }
            },
         
            error: function (x, s, e) {
                custom_shownotification('error', x.statusText);
            }
        });
    } else {
        //set mặt định
        $('#smstypebasic').prop('checked', true).change();
        $("#content-sms").val("");
        $('#sendsms-campaign-name').val('Campaign SMS ' + moment().format('DD-MM-YYYY HH:mm:ss'));
        $('input#smskhongdau').prop('checked', true);
    }
}


function sendsms_LoadContactGroup(apiDomain, access_token) {
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/ContactGroup/GetListForSMS',
        contenType: 'application/json; charset=utf-8',
        type: 'GET',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {

            var data = r.Data;
            var option = [];
            if (data.length > 0) {
                $.each(data, function (index, obj) {
                    option.unshift({
                        id: obj.ContactGroupId,
                        text: obj.ContactGroupName,
                        total: obj.TotalSMS,
                        vina: obj.TotalVinaphone,
                        vt: obj.TotalViettel,
                        vn: obj.TotalVietnamobile,
                        sf: obj.TotalSphone,
                        mb: obj.TotalMobifone,
                        gm: obj.TotalGmobile
                    });

                });
            }
            $('#sendsms-select-contact-group').select2({ data: option });
            $("#sendsms-select-contact-group").on("select2:open", function () {
                $(".select2-search--dropdown .select2-search__field").attr("placeholder", "Search...");
            });
            $("#sendsms-select-contact-group").on("select2:close", function () {
                $(".select2-search--dropdown .select2-search__field").attr("placeholder", null);
            });
            if (ContactGroupSendSMS != '') {
                $('#sendsms-select-contact-group').select2('val', ContactGroupSendSMS);
                $('.select2-selection').css({ 'background': '#26c281' });
                $('#select2-sendsms-select-contact-group-container').css({ 'color': 'white' });
               
                sendsms_NumberofMobile();
            }

        },
        complete: function () {

            $('#divLoading').hide();

        },
        error: function (x, s, e) {
            // $('#divLoading').hide();
            custom_shownotification('error', x.statusText);
        }
    })
}

var ContactGroupSendSMS = '';
function sendsms_SelectContactGroup(apiDomain,access_token,message)
{
    $('#sendsms-btn-contact-group').on('click', function () {

        if (parseInt($('#sendsms-select-contact-group option:selected').val()) == -1) {
            custom_shownotification("error", message.NoSelectGroup);
        }
        else {

            //var ContactGroupId = $('#sendsms-select-contact-group option:selected').val();
            //sendsms_ImportContact(apiDomain, access_token, ContactGroupId, message);
            $('.select2-selection').css({ 'background': '#26c281' });
            $('#select2-sendsms-select-contact-group-container').css({ 'color': 'white' });
            ContactGroupSendSMS = $('#sendsms-select-contact-group').val();
            sendsms_NumberofMobile();
        }

    });

    $('#sendsms-btn-cancel-contact-group').on('click', function () {
        $('.select2-selection').css({ 'background': '' });
        $('#select2-sendsms-select-contact-group-container').css({ 'color': '' });
        ContactGroupSendSMS = '';
        $('#sendsms-select-contact-group').select2('val', '-1');
        sendsms_NumberofMobile();
    });

    $('#sendsms-select-contact-group').on('select2:select', function (evt) {
        $('.select2-selection').css({ 'background': '' });
        $('#select2-sendsms-select-contact-group-container').css({ 'color': '' });
        ContactGroupSendSMS = '';

        var input = $('#sendsms-list-mobile-number').tagsinput('items');
        var totalsend = input.length;
        $('#total-sms-cansend').text(formatMoney(totalsend, ''));
    });
}

//custom sự kiện add của input tag
function sendsms_TagInput()
{
    $('#sendsms-list-mobile-number').on('beforeItemAdd', function (event) {
        if (!IsMobile(event.item.trim()))
        {
            custom_shownotification('warning', event.item+' không đúng định dạng');
            event.cancel = true;
           
        }
        
    });
    $("#sendsms-list-mobile-number").on('itemAdded', function (event) {

        sendsms_NumberofMobile();
      
    });

    $("#sendsms-list-mobile-number").on('itemRemoved', function (event) {
        sendsms_NumberofMobile();
    });
}


function sendsms_NumberofMobile() {
    var input = $('#sendsms-list-mobile-number').tagsinput('items');
    try {
        var data = $("#sendsms-select-contact-group").select2('data')[0];
    }
    catch (ex) {
        console.log(ex);
        var data = { total: 0 };
    }
    var totalsend = 0;
    if ((parseInt($("input:radio[name='smssendtype']:checked").val()) == 1)) //gửi thường trừ số viettel ra 
    {

        if (parseInt(data.id) != -1) {
            totalsend = input.length + data.total; //- data.vt;
        }
        else {
            totalsend = input.length;
        }

    }
    else if ((parseInt($("input:radio[name='smssendtype']:checked").val()) == 2)) {
        if (parseInt(data.id) != -1) {
            totalsend = input.length + data.total;
        }
        else {
            totalsend = input.length;
        }

    }
    else if ((parseInt($("input:radio[name='smssendtype']:checked").val()) == 3)) {
        if (parseInt(data.id) != -1) {
            totalsend = input.length + data.total;
        }
        else {
            totalsend = input.length;
        }

    }
    $('#total-sms-cansend').text(formatMoney(totalsend, ''));
    console.log(totalsend);

}

function sendsms_ResetContact()
{
    $('#sendsms-btn-contact-group-reset').on('click', function () {
        $('#sendsms-list-mobile-number').tagsinput('removeAll');
        $('#sendsms-select-contact-group').select2('val','-1');
        $('.select2-selection').css({ 'background': '' });
        $('#select2-sendsms-select-contact-group-container').css({ 'color': '' });
        sendsms_NumberofMobile();
    })
}


function sendsms_SMSBind(message) {

    //$("#content-sms").bind("keydown", function (e) {
    //    if ((parseInt($("input:radio[name='smsinputtype']:checked").val()) == 1)) {
    //        if (parseInt(e.which) > 127 || parseInt(e.keyCode) > 127) {

    //            var kd = custom_bodaukeycode(e.keyCode);
    //            custom_InsertTextAtCaret('content-sms', String.fromCharCode(kd));
    //            e.preventDefault();
    //        }

    //    }
    //});

    $("#content-sms").bind("keyup", function (e) {
        sendsms_ChangeAccent();
        sendsms_UpdateMessageCount(message);
    });


};


var MsgCount;
var AllowMsgCount = true;
function sendsms_UpdateMessageCount(message) {

    sendsms_GetMessageLength(message);

    $("input:radio[name='smsinputtype']").change(function () {
        sendsms_ChangeAccent();
        sendsms_GetMessageLength(message);
    });

};

function sendsms_ChangeAccent()
{
    var value = $("#content-sms").val();

    $("#content-sms").empty();
    console.log($("#content-sms").val());
    var smsinputtype = parseInt($('input[name=smsinputtype]:checked').val());
    if (smsinputtype == 1) {
        $("#content-sms").val(custom_bodauTiengViet(value));
    }
    else {
        $("#content-sms").val(value);
    }
}

var numberOfLineBreaks = 0;
function sendsms_GetMessageLength(message) {
    //var smstype = smsinputtype
    var $this = $("#content-sms");
    var smsinputtype = parseInt($('input[name=smsinputtype]:checked').val());
    charLength = $this.val().length + appshare_SMSSymbolLength($this.val());
    //$.each(LISTPERSOLINATE, function (i, o) {
    //    if ($this.val().indexOf(o) != -1) {
    //        var re = new RegExp(o, 'g');
    //        var number = $this.val().match(re).length;
    //        charLength = charLength - number * o.length + number * LENGTH_PERSOLINATE_SMS;
    //    }
    //});


    if (parseInt($("input:radio[name='smssendtype']:checked").val()) == 1) {
        MsgCount = appshare_OTPNumber(charLength)
        if (charLength > LENGTHMAXOTP)
        {
            AllowMsgCount = false;
            custom_shownotification('error', 'Tin nhắn tối đa 422 kí tự');
            
        } else {
            AllowMsgCount = true;
        }
        var html = appshare_MsgLenghtCount(1, message);
        $("#MsgLengthCount").html("<b style='color:#8EBC00'>" + charLength + "</b> " + message.Character + " = <b style='color:#E84444'>" + MsgCount + "</b> " + message.SMS + html);

    }
    else if (parseInt($("input:radio[name='smssendtype']:checked").val()) == 2) {
        MsgCount = appshare_BrandNumber(charLength);
        if (charLength > LENGTHMAXBRAND) {
            custom_shownotification('error', 'Tin nhắn tối đa 611 kí tự');
            AllowMsgCount = false;
        } else {
            AllowMsgCount = true;

        }
        var html = appshare_MsgLenghtCount(2, message);
        $("#MsgLengthCount").html("<b style='color:#8EBC00'>" + charLength + "</b> " + message.Character + " = <b style='color:#E84444'>" + MsgCount + "</b> " + message.SMS + html);

    }
    else {
        var html = appshare_MsgLenghtCount(3, message);
        var smsinputtype =parseInt( $("input:radio[name='smsinputtype']:checked").val());
        var obj = appshare_GateWayNumberAccented(smsinputtype,$this.val());
            MsgCount = obj.messages;
            $("#MsgLengthCount").html("<b style='color:#8EBC00'>" + charLength + " / "+obj.per_message+"</b> " + message.Character + " = <b style='color:#E84444'>" + MsgCount + " </b>" + message.SMS + html);

    }
    $('.popovers').popover();
}

function sendsms_AddPersonalize(IdAdd, message) {
    $('#div-personalize-subject-sms').on('click', '.data-personalize', function () {
        var personalize = $(this).text();
        custom_InsertTextAtCaret(IdAdd, personalize);

        //nếu là sms thì update sms
        sendsms_UpdateMessageCount(message);
    });

}

//function onInput() {
//    if (parseInt($("input:radio[name='smsinputtype']:checked").val()) == 1) {
//        var st = $('#content-sms').val();
//        for (var i = 0, len = st.length; i < len; i++) {
//            if (parseInt(st.charCodeAt(st.length - i)) > 127) {
//                return [[st.length - i, st.length - i + 1]];
//            }
//        }
//    }
//    else return [];

//}


function sendsms_GetBrandName(apiDomain,access_token,message)
{
    $.ajax({
        url: apiDomain + 'api/User/GetSMSBrandNames',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            var code = parseInt(r.ErrorCode);
            switch (code) {
                case 0:
                    if (r.Data.length > 0) {
                        var html = "";
                        $.each(r.Data, function (i, o) {
                            html += '<option value=' + o.BrandNameId + '>' + o.BrandName + '</option>';
                        });
                        $('#select-sendsms-brandname').append(html);
                        $('#select-brandname-type').removeClass("hide"); //nut chọn kiểu brand
                    }
                    else
                    {
                        var html = '<option value="-1">' + message.NoBrandName + '</option>'
                        $('#select-sendsms-brandname').append(html);
                        $('#register-brandname').show();
                    }
                    
                    break;
                default:
                    break;

            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', x.statusText);
        }

    })
}


function sendsms_GetDevice(apiDomain, access_token, message)
{
    var data = appshare_GetGatewayConfigList(apiDomain, access_token, message);
    if (data.Data.length > 0) {
        $('#select-devicenumber').removeClass('hide');
        var html = '';
        var obj = data.Data;
        console.log(obj);
        $.each(obj, function (i, o) {
            html += '<option value="' + o.ItemId + '" data-default="' + o.DefaultDeviceToken.ItemId + '" data-mobile="' + o.MobifoneDeviceToken.ItemId + '" data-vina="' + o.VinaphoneDeviceToken.ItemId + '"  data-viettel="' + o.ViettelDeviceToken.ItemId + '">' + o.ConfigName + '</option>';
        });
        
        $('#select-sendsms-device').append(html);
        //$('#select-sendsms-device-vina').append(html);
        //$('#select-sendsms-device-viettel').append(html);
    }
    else {
        var html = '<option value=-1>'+message.NoConfig+'</option>';
        $('#select-sendsms-device').append(html);
    }
   
    //else {
    //    $('#select-devicenumber').hide();
    //}
}


function sendsms_ShowBrandName(message) {
    $('#Performance-sendsms').text('40-70%');
    $('#divsmsservice').show();
    $("input:radio[name='smssendtype']").change(function () {
        if (parseInt($("input:radio[name='smssendtype']:checked").val()) == 2) {
            $('#Performance-sendsms').text('85-95%');
            $('#select-sms-input-codau').hide();
            $('input#smskhongdau').trigger('click');
            $('#divsmsservice').show();

            if ($('#sendsms-brandname').hasClass('hide')) {
                $('#sendsms-brandname').removeClass("hide");
                $('.alert-sms-not-send').show();
            }
            if (!$('#sendsms-device').hasClass('hide')) {
                $('#sendsms-device').addClass("hide");
            }

            $('#note-send-webapp').show();
            $('#note-send-getway').hide();

        }
        if (parseInt($("input:radio[name='smssendtype']:checked").val()) == 1) {
            $('#Performance-sendsms').text('40-70%');
            $('#select-sms-input-codau').hide();
            $('input#smskhongdau').trigger('click');
            $('#divsmsservice').show();
            if (!$('#sendsms-brandname').hasClass('hide')) {
                $('#sendsms-brandname').addClass('hide');
                $('.alert-sms-not-send').show();
                
            }

            if (!$('#sendsms-device').hasClass('hide')) {
                $('#sendsms-device').addClass("hide");
            }

            $('#note-send-webapp').show();
            $('#note-send-getway').hide();

           
        }

        //gửi getway
        if (parseInt($("input:radio[name='smssendtype']:checked").val()) == 3) {
            $('#divsmsservice').hide();
            $('#select-sms-input-codau').show();

            if (!$('#sendsms-brandname').hasClass('hide')) {
                $('#sendsms-brandname').addClass('hide');
                $('.alert-sms-not-send').show();
            }

            if ($('#sendsms-device').hasClass('hide')) {
                $('#sendsms-device').removeClass("hide");
                }

            $('#note-send-webapp').hide();
            $('#note-send-getway').show();


        }

        //tính số mobile gửi 
        sendsms_NumberofMobile();
        sendsms_SMSBind(message);
        sendsms_GetMessageLength(message);
        
    });
}

function sendsms_Save(apiDomain, access_token, SMSSendId, message)
{
    $('#btn-sendsms-save').on('click', function () {
        var data;
        if (ContactGroupSendSMS != '') {
            var ContactGroupId = ContactGroupSendSMS;
        }
        
        var datacompo = sendsms_GetDataCompo();
        if (parseInt($("input:radio[name='smssendtype']:checked").val()) == 1)
        {
            
            data = {
                Message: datacompo.Message,
                MessageName: datacompo.MessageName,
                MessageLength: datacompo.MessageLength,
                SMSTypeId: datacompo.SMSTypeId,
                MobileNumbers: datacompo.MobileNumbers,
                IsSent: false,
                InputType: datacompo.InputType,
                ContactGroupId: ContactGroupId,
         
            }
        }
        if (parseInt($("input:radio[name='smssendtype']:checked").val()) == 2)
        {
            data = {
                Message: datacompo.Message,
                MessageLength: datacompo.MessageLength,
                MessageName: datacompo.MessageName,
                SMSTypeId: datacompo.SMSTypeId,
                BrandName: datacompo.BrandName,
                BrandNameId: datacompo.BrandNameId,
                MobileNumbers: datacompo.MobileNumbers,
                IsSent: false,
                InputType: datacompo.InputType,
                ContactGroupId: ContactGroupId,


            }
        }
        else
        {
            data = {
                Message: datacompo.Message,
                MessageName: datacompo.MessageName,
                MessageLength: datacompo.MessageLength,
                SMSTypeId: datacompo.SMSTypeId,
                MobileNumbers: datacompo.MobileNumbers,
                IsSent: false,
                InputType: datacompo.InputType,
                ContactGroupId: ContactGroupId,
                DeviceTokenItemId: datacompo.DeviceTokenItemId,
                DeviceTokenItemIdMobifone: datacompo.DeviceTokenItemIdMobifone,
                DeviceTokenItemIdVinaphone: datacompo.DeviceTokenItemIdVinaphone,
                DeviceTokenItemIdViettel: datacompo.DeviceTokenItemIdViettel,
                GatewayConfigId: datacompo.GatewayConfigId,
            }


        }
       
        if(sendsms_CheckInfo(message))
        {
            var IsSchedule = false;
            var IsSend = false;
            if (SMSSendId == "-1")//lưu mới
            {
                sendsms_ActionSend(apiDomain, access_token, data, IsSchedule,IsSend, SMSSendId, message);
            }
            else //lưu lần nữa 
            {
                data.SMSSentId = SMSSendId;
                sendsms_ActionSend(apiDomain, access_token, data, IsSchedule,IsSend, SMSSendId, message);
            }
        }
    })
}

function sendsms_Send(apiDomain, access_token, SMSSendId, message) {
    //modal xác nhận
    $('#btn-sendsms-send').on('click', function () {
        if (sendsms_CheckInfo(message)) {
            $('#modelConfirmSend').modal('toggle');
        }
    });
    
    //gửi 
    $('#btnConfirmSave').on('click', function () {
        if (sendsms_CheckInfo(message)) {
            var data;
            if (ContactGroupSendSMS != '') {
                var ContactGroupId = ContactGroupSendSMS;
            }

            var datacompo = sendsms_GetDataCompo();
            if (parseInt($("input:radio[name='smssendtype']:checked").val()) == 1) {
                data = {
                    Message: datacompo.Message,
                    MessageName: datacompo.MessageName,
                    MessageLength: datacompo.MessageLength,
                    SMSTypeId: datacompo.SMSTypeId,
                    MobileNumbers: datacompo.MobileNumbers,
                    IsSent: true,
                    InputType: datacompo.InputType,
                    ContactGroupId: ContactGroupId,
                  
                }
            }
            if (parseInt($("input:radio[name='smssendtype']:checked").val()) == 2) {
                data = {
                    Message: datacompo.Message,
                    MessageName: datacompo.MessageName,
                    MessageLength: datacompo.MessageLength,
                    SMSTypeId: datacompo.SMSTypeId,
                    MobileNumbers: datacompo.MobileNumbers,
                    IsSent: true,
                    BrandNameId: datacompo.BrandNameId,
                    BrandName: datacompo.BrandName,
                    InputType: datacompo.InputType,
                    ContactGroupId: ContactGroupId,
                    
                }
            } else {
                data = {
                    Message: datacompo.Message,
                    MessageName: datacompo.MessageName,
                    MessageLength: datacompo.MessageLength,
                    SMSTypeId: datacompo.SMSTypeId,
                    MobileNumbers: datacompo.MobileNumbers,
                    IsSent: true,
                    InputType: datacompo.InputType,
                    ContactGroupId: ContactGroupId,
                    DeviceTokenItemId: datacompo.DeviceTokenItemId,
                    DeviceTokenItemIdMobifone: datacompo.DeviceTokenItemIdMobifone,
                    DeviceTokenItemIdVinaphone: datacompo.DeviceTokenItemIdVinaphone,
                    DeviceTokenItemIdViettel: datacompo.DeviceTokenItemIdViettel,
                    GatewayConfigId: datacompo.GatewayConfigId,
                }
            }
            var IsSchedule = false;
            var IsSend = true;

            if (SMSSendId == '-1') {  // gửi mới
                sendsms_ActionSend(apiDomain, access_token, data, IsSchedule,IsSend, SMSSendId, message);
            }
            else  //gửi lại cái chưa gửi
            {
                data.SMSSentId = SMSSendId;
                sendsms_ActionSend(apiDomain, access_token, data, IsSchedule,IsSend, SMSSendId, message);
            }
        }
    });
   
}

function sendsms_ActionSchedule(apiDomain, access_token, SMSSendId, message)
{
    $('#confirm-sendsms-nocampaign-schedule').on('click', function () {
        var data;
       
        if (ContactGroupSendSMS != '') {
            var ContactGroupId = ContactGroupSendSMS;
        }

        var scheduledate = moment(moment($('#date-schedule-send-sms').datepicker('getDate')).format('YYYY-MM-DD') + ' ' + $('#select-hour-schuedule option:selected').val()+':'+ $('#select-minus-schuedule option:selected').val() + ':00').utc().format('YYYY-MM-DD HH:mm:ss');
        var dateschedule = scheduledate.trim().replace(' ', 'T');

        var datacompo = sendsms_GetDataCompo();

        if (parseInt($("input:radio[name='smssendtype']:checked").val()) == 1) {
            data = {
                Message: datacompo.Message,
                MessageName: datacompo.MessageName,
                MessageLength:  datacompo.MessageLength,
                SMSTypeId: datacompo.SMSTypeId,
                MobileNumbers: datacompo.MobileNumbers,
                IsScheduled: true,
                IsSent: true,
                ScheduledDate: dateschedule,
                InputType: datacompo.InputType,
                ContactGroupId: ContactGroupId,
      
            }
        }
        if (parseInt($("input:radio[name='smssendtype']:checked").val()) == 2) {
            data = {
                Message: datacompo.Message,
                MessageName: datacompo.MessageName,
                MessageLength: datacompo.MessageLength,
                SMSTypeId: datacompo.SMSTypeId,
                MobileNumbers: datacompo.MobileNumbers,
                BrandNameId: datacompo.BrandNameId,
                BrandName: datacompo.BrandName,
                IsScheduled: true,
                IsSent: true,
                ScheduledDate: dateschedule,
                InputType: datacompo.InputType,
                ContactGroupId: ContactGroupId,
            }
        }
        else {
            data = {
                Message: datacompo.Message,
                MessageName: datacompo.MessageName,
                MessageLength: datacompo.MessageLength,
                SMSTypeId: datacompo.SMSTypeId,
                MobileNumbers: datacompo.MobileNumbers,
                IsScheduled: true,
                IsSent: true,
                ScheduledDate: dateschedule,
                InputType: datacompo.InputType,
                ContactGroupId: ContactGroupId,
                DeviceTokenItemId: datacompo.DeviceTokenItemId,
                DeviceTokenItemIdMobifone: datacompo.DeviceTokenItemIdMobifone,
                DeviceTokenItemIdVinaphone: datacompo.DeviceTokenItemIdVinaphone,
                DeviceTokenItemIdViettel: datacompo.DeviceTokenItemIdViettel,
                GatewayConfigId: datacompo.GatewayConfigId,
            }
        }

    

        if (sendsms_CheckInfo_Schedule(message)) {
            var IsSchedule = true;
            var IsSend = false;
            if (SMSSendId == '-1') {  //đặt lịch mới

                sendsms_ActionSend(apiDomain, access_token, data, IsSchedule,IsSend, SMSSendId, message);
            }
            else  //đặt lịch lại
            {
                data.SMSSentId = SMSSendId;
                sendsms_ActionSend(apiDomain, access_token, data, IsSchedule,IsSend, SMSSendId, message)
            }
        }
    })
}

function sendsms_ActionSend(apiDomain, access_token, data, IsSchedule,IsSend, SMSSenId, message) {
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/SmsSend/Send',
        contentType: 'application/json; charset=utf-8',
        type: 'POST',
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);

        },
        success: function (r) {
            $('#divLoading').hide();
            if (IsSchedule) {
                $("#modal-schedule-send-sms").modal('toggle');
            }
            if (IsSend) {
                $('#modelConfirmSend').modal('toggle');
            }
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    custom_shownotification('success', message.Success);
                    setTimeout(function () { window.location.href = "/SendSMS/"; }, 1000);

                    break;
                case 104:
                    custom_shownotification('error', message.MoneyError);

                    break;
                case 106:
                    custom_shownotification('error', r.ErrorMessage);

                    break;
                default:
                    custom_shownotification('error', message.Error);
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification('error', message.Error);
        }
    });
}

function sendsms_GetDataCompo()
{
    var mobilelist = [];
    $.each($('#sendsms-list-mobile-number').tagsinput('items'), function (i, mobile) {
        if (parseInt(mobile.substr(0, 2)) == 84) {
            mobile = '0' + mobile.replace(mobile.substr(0, 2), '').trim();

        }
        if (mobile.substr(0, 3) == '+84') {
            mobile = '0' + mobile.replace(mobile.substr(0, 3), '').trim();
        }
        mobilelist.push(mobile);
    });

    var message = custom_bodauTiengViet($('#content-sms').val());
    if (parseInt($('input[name=smssendtype]:checked').val()) ==3 && parseInt($('input[name=smsinputtype]:checked').val()) == 2) {
        message = $('#content-sms').val();
    }
    var data = {
        Message: message,
        MessageName: $('#sendsms-campaign-name').val(),
        SMSTypeId: parseInt($("input:radio[name='smssendtype']:checked").val()),
        BrandNameId: parseInt($('#select-sendsms-brandname option:selected').val()),
        BrandName: $('#select-sendsms-brandname option:selected').text(),
        InputType: $("input:radio[name='smsinputtype']:checked").val(),
        MobileNumbers: mobilelist,
        MessageLength: MsgCount,
        DeviceTokenItemId: parseInt($('#select-sendsms-device option:selected').attr('data-default')),
        DeviceTokenItemIdMobifone: parseInt($('#select-sendsms-device option:selected').attr('data-mobile')),
        DeviceTokenItemIdVinaphone: parseInt($('#select-sendsms-device option:selected').attr('data-vina')),
        DeviceTokenItemIdViettel: parseInt($('#select-sendsms-device option:selected').attr('data-viettel')),
        GatewayConfigId: parseInt($('#select-sendsms-device option:selected').val()),

    }
    return data;
}

function sendsms_CheckInfo(message)
{
    if (!AllowMsgCount) {
        custom_shownotification('error', 'Quá số lượng kí tự cho phép');
        return false;
    }
    if($('#content-sms').val()=="")
    {
        custom_shownotification('error', message.MessageError);
        return false;
    }
    if ($('#sendsms-list-mobile-number').tagsinput('items').length == 0  && ContactGroupSendSMS=='') {
        custom_shownotification('error', message.MobileError);
        return false;
    }
    if ($('#sendsms-list-mobile-number').tagsinput('items').length == 0 && $("#sendsms-select-contact-group").select2('data')[0].total==0) {
        custom_shownotification('error', 'Danh sách liên hệ không có số điện thoại nào');
        return false;
    }

    if (parseInt($("input:radio[name='smssendtype']:checked").val()) == 2 && parseInt($('#select-sendsms-brandname').val()) == -1) {
        custom_shownotification('error', message.NoBrandName);
        return false;
    }
    if (parseInt($("input:radio[name='smssendtype']:checked").val()) == 3 && parseInt($('#select-sendsms-device').val()) == -1) {
        custom_shownotification('error', message.NoConfig);
        return false;
    }

    if (parseInt($("input:radio[name='smssendtype']:checked").val()) == 3 && isNaN(parseInt($("#select-sendsms-device").val()))) {

        custom_shownotification('error', message.NoConfig);
        $('#divLoading').hide();
        return false;
    }
   
    else return true;
}

function sendsms_CheckInfo_Schedule(message) {
    if (!AllowMsgCount) {
        custom_shownotification('error', 'Quá số lượng kí tự cho phép');
        return false;
    }
    if ($('#content-sms').val() == "") {
        custom_shownotification('error', message.MessageError);
        return false;
    }
    if ($('#sendsms-list-mobile-number').tagsinput('items').length == 0 && ContactGroupSendSMS == '') {
        custom_shownotification('error', message.MobileError);
        return false;
    }

    if ($('#sendsms-list-mobile-number').tagsinput('items').length == 0 && $("#sendsms-select-contact-group").select2('data')[0].total == 0) {
        custom_shownotification('error', 'Danh sách liên hệ không có số điện thoại nào');
        return false;
    }

    if ($('#date-schedule-send-sms').datepicker('getDate') == null || $('#date-schedule-send-sms').datepicker('getDate') == "" || $('#select-hour-schuedule option:selected').val() == "" || $('#select-minus-schuedule option:selected').val() == '') {
        //loading
        $('#divLoading').hide();

        custom_shownotification('error', message.SelectDateScheduleError);
        return false;
    }
    
    //if (parseInt($("input:radio[name='smsinputtype']:checked").val()) == 1) {
    //    var st = $('#content-sms').val()
    //    for (var i = 0, len = st.length; i < len; i++) {
    //        if (parseInt(st.charCodeAt(st.length - i)) > 127) {
    //            var message = "Tin nhắn có kí tự không cho phép tại kí tự : " + st.substring(st.length - i, st.length - i + 1);
    //            custom_shownotification('error', message);
    //            return false;
    //        }
    //    }
    //}

    else {

        var date = moment($('#date-schedule-send-sms').datepicker('getDate')).format('YYYY-MM-DD');

        var now = new Date();
        var datenow = moment().format('YYYY-MM-DD');
        var time = [$('#select-hour-schuedule option:selected').val(), $('#select-minus-schuedule option:selected').val()] //$('#time-schedule-send-sms').val().split(/:/);

        if (new Date(datenow).getTime() > new Date(date).getTime()) {
            $('#divLoading').hide();
            custom_shownotification('error', message.SelectDayError);
            return false;
        }
        if (date == datenow && parseInt(time[0]) == now.getHours() && parseInt(time[1]) < parseInt(now.getMinutes()) || date == datenow && parseInt(time[0]) < now.getHours()) {
            $('#divLoading').hide();
            custom_shownotification('error', message.SelectTimeError);
            return false;
        }

        else {
            return true;
        }

    }
    
}


function sendsms_CampaignAction_Draft(apiDomain, access_token, message, tableIddraft) {
   
    sendsms_Edit(tableIddraft);

}

function sendsms_CampaignAction(apiDomain,access_token,message,tableId)
{
   
    sendsms_Info(apiDomain, access_token, tableId);
    sendsms_CancelSchedule(apiDomain, access_token, message, tableId);
    sendsms_Report(tableId);
}

function sendsms_Info(apiDomain,access_token, tableId)
{
   
    //lick info trên lưới
    $(tableId).on('click', 'tbody #smsinfo', function () {
        if ($.fn.DataTable.isDataTable(tableId)) {

            $('#mobile-of-sms').children().remove();
            $('#sms-content').val("");
            $('#contactnumber').val("");
            $('#createdate').val("");
            $('#sentdate').val("");
            var tablerow = $(tableId).DataTable();
            var rowData = tablerow.row($(this).parents('tr')).data();
            $.ajax({
                url: apiDomain + 'api/SmsSend/GetInfo?SentId=' + rowData.sentId,
                type: 'GET',
                contentType: 'application/json; charset=utf-8',
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + access_token);
                },
                success: function (r) {
                    switch (parseInt(r.ErrorCode)) {

                        case 0:
                            $('#divinfobrandname').hide();
                            $('#divinfogatewayname').hide();
                            var data = JSON.parse(JSON.stringify(r.Data));

                            $('#divcampaignname').text(data.MessageName)
                            $('#divinfosmstype').show();
                            if (parseInt(data.SMSTypeId) == 2) {
                                $('#smstype').text('Tin nhắn thương hiệu (Brand name)');
                                $('#infobrandname').text(data.BrandName);
                                $('#divinfobrandname').show();
                            }
                            else if (parseInt(data.SMSTypeId) == 1) {
                                $('#smstype').text('Tin nhắn ngẫu nhiên (OTP)');
                            }
                            else {
                                $('#divinfogatewayname').show();
                                $('#smstype').text('Tin nhắn từ số cá nhân');
                                var gateway = appshare_GetGatewayConfigList(apiDomain, access_token);
                                var gatewayname = "";
                                if (gateway != undefined) {
                                    var gatewaylist = gateway.Data;
                                    $.each(gatewaylist, function (i, o) {
                                        if (o.ItemId == data.GatewayConfigId) {
                                            gatewayname = o.ConfigName;
                                        }
                                    });
                                }
                                $('#infogatewayname').text(gatewayname);
                            }

                            $('#sms-contentlength').text(data.MessageLength);
                            if (data.SMSTypeId == 3) {
                                
                                $('#totalmoneysms').text('nhà mạng trừ vào sim');
                            }else{
                                $('#totalmoneysms').text(formatMoney(data.AdditionalCost, ""));
                            }

                            $('#sms-content').text(data.Message);
                            $('#createdate').text(moment(moment.utc(data.CreateDate).toDate()).format('YYYY-MM-DD HH:mm'));
                            if (data.IsSent == false && data.IsScheduled == true) {
                                $('#sentdate').text(moment(moment.utc(data.ScheduleDate).toDate()).format('YYYY-MM-DD HH:mm'));
                                
                            }
                            else {
                                $('#sentdate').text(moment(moment.utc(data.SendDate).toDate()).format('YYYY-MM-DD HH:mm'));
                                
                            }
                            $('#contactnumber').text(data.TotalContact);


                            $.each(data.SendContacts, function (index, obj) {
                                $('#mobile-of-sms').append('<span style="padding:2px;">' + obj.Mobile + ' ,</span>');
                            });
                            $('#infosmsCampaign').modal('toggle');
                            break;
                        default:
                            break;
                    }
                },
                error: function (x, s, e) {

                }
            });
        }
        
    });

    
}

function  sendsms_Edit(tableIddraft)
{
   

    $(tableIddraft).on('click', 'tbody #smsedit', function () {
        if ($.fn.DataTable.isDataTable(tableIddraft)) {
            var tablerow = $(tableIddraft).DataTable();
            var rowData = tablerow.row($(this).parents('tr')).data();
            window.location.href = "/SendSMS?Id=" + rowData.sentId;
        }
        });
    
}

function sendsms_CancelSchedule(apiDomain, access_token, message, tableId) {
    

    $(tableId).on('click',  'tbody #smscancelschedule', function () {
        if ($.fn.DataTable.isDataTable(tableId)) {
            var tablerow = $(tableId).DataTable();
            var rowData = tablerow.row($(this).parents('tr')).data();
            var nowdate = Date.parse(moment().format());
            var scheduledate = Date.parse(moment(moment.utc(rowData.scheduleDate).toDate()).format());
            if (nowdate > scheduledate) {
                custom_shownotification('error', message.CancelScheduleWarning);
            }
            else {
                $.ajax({
                    url: apiDomain + 'api/SmsSend/CancelSend?SMSSentId=' + rowData.sentId,
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", "Bearer " + access_token);

                    },
                    success: function (r) {
                        switch (parseInt(r.ErrorCode)) {
                            case 0:
                                custom_shownotification('success', message.CancelScheduleSuccess);
                                $(tableId).dataTable().fnDraw();
                                break;
                            default:
                                custom_shownotification('error', message.HaveUnknownError);
                                break;
                        }
                    },
                    error: function (x, s, e) {
                        custom_shownotification('error', message.HaveUnknownError);
                    }


                });
            }
        }
        });
    
}

function  sendsms_ReloadTable(tableId, tableIddraft)
{
    $('#btn-reload-tableSMS').on('click', function () {
        var Status;
        if (localStorage.getItem('StatusSMSCampaign')) {
            Status = localStorage.getItem('StatusSMSCampaign');
            if (Status == 'IsSent') {
                $(tableId).dataTable().fnDraw();
            }
            else if (Status == 'Draft') {
                $(tableIddraft).dataTable().fnDraw();
            }
        }

    })
}


function sendsms_OpenModalAddFromContacts()
{
    $('#sendsms-btn-import-from-list').on('click', function () {

        $('#modal-import-mobile-from-list').modal('toggle');
    });
}

function sendsms_OpenModalAddFromExcel()
{
    $('#sendsms-btn-import-from-excel').on('click', function () {
        $('#modal-import-mobile-from-excel').modal('toggle');
    })
}

function sendsms_Report(tableId)
{
    $(tableId).on('click', 'tbody #smsreport', function () {
        if ($.fn.DataTable.isDataTable(tableId)) {
            var tablerow = $(tableId).DataTable();
            var rowData = tablerow.row($(this).parents('tr')).data();
            window.location.href = "/Statistics/Message?SentId=" + rowData.sentId;
        }
    });
}


function sendsms_GenTimeSelect()
{
    $('#select-hour-schuedule').children().remove();
    var option = '<option value="">Giờ</option>';
    for (var i = 0; i < 24; i++) {
        if (i < 10) {
            option += '<option value="0' + i + '">0' + i + '</option>';
        } else {
            option += '<option value="' + i + '">' + i + '</option>';
        }
    }
    $('#select-hour-schuedule').append(option);

    $('#select-minus-schuedule').children().remove();
    var opt = '<option value="">Phút</option>';
    for (var i = 0; i < 60; i++) {
        if (i < 10) {
            opt += '<option value="0'+i+'">0'+i+'</option>';
        } else {
            opt += '<option value="' + i + '">' + i + '</option>';
        }
        
    }
    $('#select-minus-schuedule').append(opt);
}






