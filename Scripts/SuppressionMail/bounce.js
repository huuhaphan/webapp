﻿/// <reference path="suppression.js" />
function bounce_Action(apiDomain, access_token, tableId, message) {
    bounce_Delete(apiDomain, access_token, tableId, message);
    bounce_CheckBox(apiDomain, access_token, tableId, message);
    bounce_ActionDelete(apiDomain, access_token, tableId, message);
    bounce_DeleteAll(apiDomain, access_token, tableId, message);
    bounce_ReloadTable(tableId);
    boune_Export();
}

function bounce_CheckBox(apiDomain, access_token, tableId, message) {
    var table = $(tableId).DataTable();
    //các sự kiện với checkbox ,select all
    $(tableId + ' tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');

        // Get row data
        var data = table.row($row).data();

        //tạo list data để move copy;


        // Get row ID
        var rowId = data.itemId; //custom_GetFisrtProJson(data)

        // Determine whether row ID is in the list of selected row IDs 
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            $(this).parent().addClass('checked')
            rows_selected.push(rowId);


            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            $(this).parent().removeClass('checked')
            rows_selected.splice(index, 1);

        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }


        if (rows_selected.length > 0) {
            $('#btn-deleteall-bounce').removeClass('hide');


        }
        else {
            $('#btn-deleteall-bounce').addClass('hide');
        }

        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrl(table);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });


    // Handle click on "Select all" control
    $('input[name="select_all"]').on('click', function (e) {
        if (this.checked) {
            $(tableId + ' tbody input[type="checkbox"]:not(:checked)').trigger('click');
            $('#btn-deleteall-bounce').removeClass('hide');

        } else {
            $(tableId + ' tbody input[type="checkbox"]:checked').trigger('click');
            $('#btn-deleteall-bounce').addClass('hide');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    table.on('draw', function () {
        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrl(table);
    });
}


function bounce_Delete(apiDomain, access_token, tableId, message) {
    var tablerow = $(tableId).DataTable();
    $(tableId + ' tbody').on('click', '#delete', function () {
        var rowData = tablerow.row($(this).parents('tr')).data();
        var ItemId = rowData.itemId;
        $('#btnDeleteBounce').attr('data-uid', ItemId);
        $('#modelConfirmDeleteBounce').modal('toggle');

    });
};

function bounce_ActionDelete(apiDomain, access_token, tableId, message) {

    $('#btnDeleteBounce').on('click', function () {
        $('#divLoading').show();

        $('#modelConfirmDeleteBounce').modal('toggle');

        var ItemId = $('#btnDeleteBounce').attr('data-uid');
        $.ajax({
            url: apiDomain + 'api/EmailSend/DeleteSuppressMail?ItemId=' + ItemId,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            success: function (r) {
                $('#divLoading').hide();
                switch (parseInt(r.ErrorCode)) {
                    case 0:
                        custom_shownotification("success", message.DeleteSuccess);
                        $(tableId).dataTable().fnDraw();
                        break;
                    default:
                        custom_shownotification("error", message.DeleteError);
                        break;
                }
            },
            error: function (x, s, e) {
                $('#divLoading').hide();
                custom_shownotification("error", message.DeleteError);
            }
        });
    })
}

function bounce_DeleteAll(apiDomain, access_token, tableId, message) {
    var request = 0;
    $('#btn-deleteall-bounce').on('click', function () {
        if (rows_selected.length > 0) {
            $('#divLoading').show();
            var iderror = [];
            $.each(rows_selected, function (i, o) {
                $.ajax({
                    url: apiDomain + 'api/EmailSend/DeleteSuppressMail?ItemId=' + o,
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", "Bearer " + access_token);
                    },
                    success: function (r) {

                       
                        switch (parseInt(r.ErrorCode)) {
                            case 0:
                                if (request == rows_selected.length - 1) {
                                    $('#divLoading').hide();

                                    $('#btn-deleteall-bounce').addClass('hide');
                                    rows_selected = [];
                                    $(tableId).dataTable().fnDraw();
                                    if (iderror.length > 0) {
                                        custom_shownotification("error", 'Đã xóa xong. Lỗi xóa: ' + iderror.length + '/' + rows_selected.length);
                                    }
                                    else {
                                        custom_shownotification("success", message.DeleteSuccess);


                                    }

                                }
                                break;
                            default:
                                iderror.push(o);
                                if (request == rows_selected.length - 1) {
                                    $('#divLoading').hide();

                                    $('#btn-deleteall-bounce').addClass('hide');
                                    rows_selected = [];
                                    $(tableId).dataTable().fnDraw();
                                    if (iderror.length > 0) {
                                        custom_shownotification("error", 'Đã xóa xong. Lỗi xóa: ' + iderror.length + '/' + rows_selected.length);
                                    }
                                    else {
                                        custom_shownotification("error", message.DeleteError);


                                    }

                                }
                                break;
                        }
                        request++;

                    },
                    error: function (x, s, e) {
                        
                        iderror.push(o);
                        if (request == rows_selected.length - 1) {
                            $('#divLoading').hide();

                            $('#btn-deleteall-bounce').addClass('hide');
                            rows_selected = [];
                            $(tableId).dataTable().fnDraw();
                            if (iderror.length > 0) {
                                custom_shownotification("error", 'Đã xóa xong. Lỗi xóa: ' + iderror.length + '/' + rows_selected.length);
                            }
                            else {
                                custom_shownotification("error", message.DeleteError);


                            }
                        }
                        request++;
                    }
                });

            })
        }
    })

};
function bounce_ReloadTable(tableId) {
    $('#btn-reload-table-bounce').on('click', function () {
        $(tableId).dataTable().fnDraw();
    })
}

function boune_Export() {
    $('#export-bounce').on('click', function () {

        console.log('gs');
        $('#file-export-name').prop('disabled', false);
        $('#submit-export-suppression-contact').prop('disabled', false);
        $('#submit-export-suppression-contact').attr('data-type', 1);
        $('#table-file-export').children().remove();
        var name = 'Export bounce contact ' + moment().format('YYYY-MM-DD-HH-mm');
        $('#file-export-name').val(name);
        $('#modal-export-suppression-contact').modal('toggle');

    });

}





