﻿function suppression_Action(apiDomain, access_token, message) {
    suppressiontab.bounce = true;
    suppression_Bounces(apiDomain, access_token, message);
    suppression_OpenTabBounce(apiDomain, access_token, message)
    suppression_OpenTabDrop(apiDomain, access_token, message);
    suppression_OpenTabUnsubscribe(apiDomain, access_token, message);
    suppression_OpenTabSpamReport(apiDomain, access_token, message);
    suppression_SubmitExport(apiDomain, access_token, message);
}

var suppressiontab = {
    bounce: false,
    drop: false,
    unsubscribe: false,
    spamreport:false,
}

function suppression_Bounces(apiDomain, access_token, mesage)
{
    $.ajax({
        url: '/Suppressions/Bounces',
        type: 'GET',
        success:function(r)
        {
            $('#tab_Bounces').html(r);
        }
    })
}
function suppression_Drop(apiDomain, access_token, mesage) {
    $.ajax({
        url: '/Suppressions/Drop',
        type: 'GET',
        success: function (r) {
            $('#tab_Drop').html(r);
        }
    })
}
function suppression_Unsubscribes(apiDomain, access_token, mesage) {
    $.ajax({
        url: '/Suppressions/Unsubcribes',
        type: 'GET',
        success: function (r) {
            $('#tab_Unsubscribe').html(r);
        }
    })
}
function suppression_SpamReports(apiDomain, access_token, mesage) {
    $.ajax({
        url: '/Suppressions/SpamReports',
        type: 'GET',
        success: function (r) {
            $('#tab_SpamReport').html(r);
        }
    })
}

function suppression_OpenTabDrop(apiDomain, access_token, mesage)
{
    $('#open-tab-drop').on('click', function () {
        if (!suppressiontab.drop) {
            suppressiontab.drop = true;
            suppression_Drop(apiDomain, access_token, mesage);
        }
    })
}
function suppression_OpenTabBounce(apiDomain, access_token, mesage) {
    $('#open-tab-bounce').on('click', function () {
        if (!suppressiontab.bounce) {
            suppressiontab.bounce = true;
            suppression_Bounces(apiDomain, access_token, mesage);
        }
    })
}
function suppression_OpenTabSpamReport(apiDomain, access_token, mesage) {
    $('#open-tab-spamreport').on('click', function () {
        if (!suppressiontab.spamreport) {
            suppressiontab.spamreport = true;
            suppression_SpamReports(apiDomain, access_token, mesage);
        }
    })
}
function suppression_OpenTabUnsubscribe(apiDomain, access_token, mesage) {
    $('#open-tab-unsubscribe').on('click', function () {
        if (!suppressiontab.unsubscribe) {
            suppressiontab.unsubscribe = true;
            suppression_Unsubscribes(apiDomain, access_token, mesage);
        }
    })
}


var checkfileexport;
function suppression_CreateExport(apiDomain, access_token, message, type) {


    var FileName = $('#file-export-name').val();
    var data = {
        FileType: 'xlsx',
        FileName: FileName.replace(/\s/g, "-"),
        Type: type,
    }
    $.ajax({
        url: apiDomain + 'api/report/CreateSupressionListExport',
        type: 'post',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);

        },
        success: function (r) {

            switch (parseInt(r.ErrorCode)) {

                case 0:
                    {
                        var data = r.Data;
                        var ExportId = data.ExportId;
                        switch (parseInt(data.Status)) {
                            case 0: //pendding
                                checkfileexport = setInterval(function () { suppression_CheckExportComplete(apiDomain, access_token, ExportId, message); }, 3000);
                                break;
                            case 1:
                                var html = '<div><a href=' + data.FilePath + '><i class="fa fa-cloud-download font-green-jungle"></i> ' + message.DownLoad + '</div>';
                                setTimeout(function () {
                                    $('#status-export').children().remove();
                                    $('#status-export').append(html)
                                }, 3000);
                                break;
                            case 2:
                                var html = '<div><i class="fa fa-remove font-red"></i> ' + message.ExportError + ' </div>';
                                $('#status-export').children().remove();
                                $('#status-export').append(html);
                                break;

                        }
                    }
                    break;
                default:
                    custom_shownotification("error", message.ExportError);
                    break;


            }
        },
        error: function (x, s, e) {
            custom_shownotification("error", message.ExportError);
        }
    })
}

function suppression_CheckExportComplete(apiDomain, access_token, ExportId, message) {
    $.ajax({
        url: apiDomain + 'api/report/GetContactExport?ExportId=' + ExportId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);

        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    console.log(data);
                    switch (parseInt(data.Status)) {
                        case 1:
                            var html = '</div><a href=' + data.FilePath + '><i class="fa fa-cloud-download font-green-jungle"></i>' + message.DownLoad + ' </a></div>';
                            setTimeout(function () {
                                $('#status-export').children().remove();
                                $('#status-export').append(html);
                            }, 3000);
                            clearInterval(checkfileexport);
                            break;
                        case 2:
                            var html = '<div><i class="fa fa-remove font-red"></i>' + message.ExportError + '</div>';
                            $('#status-export').children().remove()
                            $('#status-export').append(html);
                            clearInterval(checkfileexport);
                            break;

                    }
                    break;

            }
        },
        error: function (x, s, e) {
            custom_shownotification("error", 'Có lỗi xảy ra');
            clearInterval(checkfileexport);
        }
    })
}


function suppression_SubmitExport(apiDomain, access_token, message) {
    $('#submit-export-suppression-contact').on('click', function () {
        var type = $(this).attr('data-type');
        $('#table-file-export').children().remove();
        if ($('#file-export-name').val()) {

            var html = '<table style="width:100%;" class="table table-striped">' +
                                   ' <thead>' +
                                       ' <tr>' +
                                       '   <td>' +
                                           message.FileName +
                                           ' </td>' +
                                          '  <td>' +
                                           message.Status +
                                                    '</td>' +
                                                    '<td>' +
                                                     message.CreateDate +
                                                  '  </td>' +
                                             '   </tr>' +
                                           '  </thead>' +
                                          '  <tbody>' +
                                              '  <tr>' +
                                                  '  <td>' +
                                                          $('#file-export-name').val() +
                                                  '  </td>' +
                                                 '   <td id="status-export">' +
                                                      '<div><i class="fa fa-spinner fa-pulse fa-fw"></i> ' + message.Processing + ' </div>' +
                                                   ' </td>' +
                                                 '   <td>' +
                                                     moment().format('YYYY-MM-DD HH:mm') +
                                                   ' </td>' +
                                              '  </tr>' +
                                           ' </tbody>' +
                                      '  </table>';
            $('#table-file-export').append(html);
            $('#file-export-name').prop('disabled', true);
            $('#submit-export-suppression-contact').prop('disabled', true);
            suppression_CreateExport(apiDomain, access_token, message, type = 1);


        }
        else {
            custom_shownotification("error", message.FileNameRequire);
        }
    })
}