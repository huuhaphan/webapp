﻿var UserLock = function () {
    return {
        //main function to initiate the module
        init: function () {

            $.backstretch([
                "/Content/Images/login-background.jpg",
               
            ], {
                fade: 1000,
                duration: 8000
            });
        }

    };

}();

function Relogin(apiDomain, username, message)
{
    var data = {
        password: $('#relog_password').val(),
        username: username,
        grant_type: 'password',
    }
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/authtoken',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: data,
        error: function (x, s, e) {
            $('#divLoading').hide();
            var error = parseInt(getValues(JSON.parse(x.responseText), 'error')[0]);
            switch (error) {
                case 104:
                    custom_shownotification('error', message.error104);
                    break;
                case 101:
                    custom_shownotification('error', message.error101);
                    break;

            }
        },
        success: function (r) {
            var object = JSON.parse(JSON.stringify(r));
            var access_token = getValues(object, 'access_token')[0];
            var token_type = getValues(object, 'token_type')[0];
            var expies_in = parseInt(getValues(object, 'expires_in')[0]);
            var redirect='/Contacts'
            custom_GetInfo(apiDomain, access_token,redirect);
          

        },
    });
}

function lock_Relogin(apiDomain,username,message) {
    $('#relog_submit').on('click', function () {
        Relogin(apiDomain, username, message);
    });
    $('#relogin-form input').keypress(function (e) {
        if (e.which == 13) {
           Relogin(apiDomain, username,message);
           
        }
    });
}

jQuery(document).ready(function () {
    UserLock.init();
});