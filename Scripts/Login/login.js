﻿/// <reference path="../custom.js" />
function login_Register(apiDomain, access_token_auto, campaignId, groupId, message) {
    if (jQuery.cookie('tooid') != null && jQuery.cookie('tooid') != undefined && jQuery.cookie('tooid') != '') {
        var AffId = parseInt(jQuery.cookie('tooid'));
    }
    else {
        var AffId = null;
    }
    var model = {
        FirstName: $('input[name=firstname]').val(),
        LastName: $('input[name=lastname]').val(),
        MobilePhone: $('input[id=register_mobile]').val(),
        PassWord: $('input[id=register_password]').val(),
        Email: $('input[id=register_email]').val(),
        AffiliateId: AffId,

    }

    var data = {
        ContactGroupId: groupId,
        CampaignId: campaignId,
        ContactName: $('input[name=lastname]').val() + ' ' + $('input[name=firstname]').val(),
        Email: $('input[id=register_email]').val(),
        Mobile: $('input[id=register_mobile]').val(),
        Note: '',
        Active: true,
    }

    $.ajax({
        url: apiDomain + 'api/User/SignUp',
        type: 'POST',
        data: JSON.stringify(model),
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            var user = result.Data;
            switch (result.ErrorCode) {
                case 102:
                    $('.alert-danger', $('.register-form')).show();
                    $('.alert-danger span', $('.register-form')).text(message.error102);
                    break;
                case 0:
                    var frommail = {
                        FromMail: user.Email,
                        FromName: user.FirstName+' '+user.LastName,
                    }
                    jQuery.cookie('fromemail', JSON.stringify(frommail));

                    //add to contact when register success
                    $.when(login_AddRegisterToo(apiDomain, access_token_auto, data)).then(function () {
                        if (user.Active == false) {

                            window.location.href = '/Login/Active?Email=' + model.Email
                        }
                        else {
                            window.location.href = '/Login/Success'
                        }
                    });
                    break;
                default:
                    $('.alert-danger', $('.register-form')).show();
                    $('.alert-danger span', $('.register-form')).text(message.hasError);
                    break;
            }
        },
        error:function(x,s,e)
        {
            $('.alert-danger', $('.register-form')).show();
            $('.alert-danger span', $('.register-form')).text(message.hasError);
        }


    })
}

function login_AddRegisterToo(apiDomain, access_token, data)
{
    $.ajax({
        url: apiDomain + 'api/Contact/Create',
        type: 'POST',
        data: data,
        contentType: 'application/x-www-form-urlencoded',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {

        },
        error: function (x, s, e) {
           
        },
    });
}

function login_AcctionAddFromEmail(apiDomain, access_token, data) {
    

    $.ajax({
        url: apiDomain + 'api/User/CreateFromMail',
        type: 'post',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            $.cookie("fromemail", null, { path: '/' });
        },
        error: function (x, s, e) {
           
        }
    })
}
   
function login_SignIn(apiDomain, message, returnurl,data)
{
   
    $.ajax({
        url: apiDomain+'api/authtoken',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: data,
        error: function (x, s, e) {
            //loading 
            $('#divLoading').hide();
            var error =parseInt( getValues(JSON.parse(x.responseText), 'error')[0]);
            
            $('.alert-danger', $('.login-form')).show();
            $('#login_btn_submit').button('reset');
            switch(error)
            {
                case 104:
                    $('.alert-danger span', $('.login-form')).text(message.error104);
                    break;
                case 101:
                    $('.alert-danger span', $('.login-form')).text(message.error101);
                    break;
                default:
                    $('.alert-danger span', $('.login-form')).text("Có lỗi xảy ra");
                    break;
                        
            }
        },
        success: function (r) {
            var object = JSON.parse(JSON.stringify(r));
            var access_token = object.access_token;
            var token_type = object.token_type;
            var expies_in = object.expires_in;
            var domain = window.location.hostname;
            var subdomain = domain.replace(domain.substring(0, domain.indexOf('.')), '');
            $.cookie('bearer', access_token, { path: '/', domain: subdomain });
            
            //lấy thông tin user và update
            var redirect =returnurl;
            custom_GetInfo(apiDomain, access_token, redirect);
        },
    })
}

function Login(apiDomain, access_token_auto, campaignId, groupId, message, returnurl) {
  
    DetectFormLogin();
    LoginProccess(apiDomain, message, returnurl);
    ForgetPasswordProccess(apiDomain, message);
    RegisterProccess(apiDomain,access_token_auto,campaignId,groupId, message);
    
}

function DetectFormLogin() {
    if (window.location.hash) {
        var hash = (window.location.hash);
        switch (hash.toLowerCase()) {
            case '#login':
                $('.login .content .login-form').show();
                $('.login .content-register .register-form').hide();
                break;
            case '#register':
                $('.login .content-register .register-form').show();
                $('.login .content .login-form').hide();
                break;
        }
    }
}

function LoginProccess(apiDomain, message, returnurl) {
    $('.login-form').validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {
            login_email: {
                required: true
            },
            login_password: {
                required: true,
                minlength: 6,
            },
            remember: {
                required: false
            }
        },

        messages: {
            username: {
                required: "Email is required."
            },
            password: {
                required: "Password is required."
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            $('.alert-danger', $('.login-form')).show();
            $('#login_btn_submit').button('reset');
            //loading 
            $('#divLoading').hide();
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },

        errorPlacement: function (error, element) {
            error.insertAfter(element.closest('.input-icon'));
            $('#login_btn_submit').button('reset');
            //loading 
            $('#divLoading').hide();
        },

        submitHandler: function (form) {
            $('#login_btn_submit').button('loading');
            //loading 
            $('#divLoading').show();
            var data = {
                password: $('input[id=login_password]').val(),
                username: $('input[id=login_email]').val().trim(),
                grant_type: 'password',
            };
            login_SignIn(apiDomain, message, returnurl,data);
        }
    });

    $('.login-form input').keypress(function (e) {
        if (e.which == 13) {
            //loading 
            if ($('.login-form').validate().form()) {
                $('#login_btn_submit').button('loading');
                $('#divLoading').show();
                var data = {
                    password: $('input[id=login_password]').val(),
                    username: $('input[id=login_email]').val(),
                    grant_type: 'password',
                };
                login_SignIn(apiDomain, message, returnurl,data);
            }
            return false;
        }
    });
}

function ForgetPasswordProccess(apiDomain, message) {
    $('.forget-form').validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            email: {
                required: true,
                email: true
            }
        },

        messages: {
            email: {
                required: message.errorEmail
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   

        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },

        errorPlacement: function (error, element) {
            error.insertAfter(element.closest('.input-icon'));
        },

        submitHandler: function (form) {
            //  form.submit();
            login_RequestForgetPass(apiDomain, message);
        }
    });

    $('.forget-form input').keypress(function (e) {
        if (e.which == 13) {
            if ($('.forget-form').validate().form()) {
                //$('.forget-form').submit();
                login_RequestForgetPass(apiDomain, message);
            }
            return false;
        }
    });

    jQuery('#forget-password').click(function () {
        jQuery('.login-form').hide();
        jQuery('.forget-form').show();
        $('#langualebar').hide();
    });

    jQuery('#back-btn').click(function () {
        jQuery('.login-form').show();
        jQuery('.forget-form').hide();
        $('#langualebar').show();
    });

}
function RegisterProccess(apiDomain,access_token_auto,campaignId,groupId, message) {

    $('#register-submit-btn').on('click', function () {
        if (registervalidate(message)) {
            login_Register(apiDomain,access_token_auto,campaignId,groupId, message);
        }
    })

    $('.register-form input').keypress(function (e) {
        if (e.which == 13) {
            if (registervalidate(message)) {
                login_Register(apiDomain,access_token_auto,campaignId,groupId, message);
            }
            return false;
        }
    });

    jQuery('#register-btn').click(function () {
        jQuery('.login-form').hide();
        jQuery('.content').hide();
        jQuery('.register-form').show();
        $('#langualebar').hide();
    });

    jQuery('#register-back-btn').click(function () {
        jQuery('.content').show();
        jQuery('.login-form').show();
        jQuery('.register-form').hide();
        $('#langualebar').show();
    });


}

function registervalidate(message)
{
    if(!$('#rig_firstname').val())
    {
        $('#rig_alert').text(message.errorFirstname);
        $('.alert.alert-danger').removeClass('display-hide').css('display', '');
        return false;
    }
    if (!$('#rig_latsname').val())
    {
        $('#rig_alert').text(message.errorLastname);
        $('.alert.alert-danger').removeClass('display-hide').css('display', '');
        return false;
    }

     if (!$('#register_mobile').val()) {
         $('#rig_alert').text(message.errorMobile);
         $('.alert.alert-danger').removeClass('display-hide').css('display', '');
        return false;
    }
    if ($('#register_mobile').val() && !IsMobile($('#register_mobile').val())) {
        $('#rig_alert').text(message.errorFormatMobile);
        $('.alert.alert-danger').removeClass('display-hide').css('display', '');
        return false;
    }

    if (!$('#register_email').val()) {
        $('#rig_alert').text(message.errorEmail);
        $('.alert.alert-danger').removeClass('display-hide').css('display', '');
        return false;
    }
    if ($('#register_email').val() && !IsEmail($('#register_email').val())) {
        $('#rig_alert').text(message.errorFormatEmail);
        $('.alert.alert-danger').removeClass('display-hide').css('display', '');
        return false;
    }
   
    

    if (!$('#register_password').val()) {
        $('#rig_alert').text(message.errorPass);
        $('.alert.alert-danger').removeClass('display-hide').css('display', '');
        return false;
    }
    if (!$('#rpassword').val()) {
        $('#rig_alert').text(message.errorRePass);
        $('.alert.alert-danger').removeClass('display-hide').css('display', '');
        return false;
    }
    if ($('#rpassword').val() && parseInt($('#rpassword').val().length)<6) {
        $('#rig_alert').text(message.errorLeastPass);
        $('.alert.alert-danger').removeClass('display-hide').css('display', '');
        return false;
    }
    if($('#register_password').val()&&$('#rpassword').val()&& $('#rpassword').val().localeCompare($('#register_password').val())!=0)
    {
        $('#rig_alert').text(message.errorComparePass);
        $('.alert.alert-danger').removeClass('display-hide').css('display', '');
        return false;
    }
    if($('#rig_tnc').prop('checked')!=true)
    {
        $('#rig_alert').text(message.errorNTC);
        $('.alert.alert-danger').removeClass('display-hide').css('display', '');
        return false;
    }
    
    return true;


}

function backstretch() {
    // init background slide images
    $.backstretch([
        "/Content/Images/login-background.jpg",
    ], {
        fade: 1000,
        duration: 8000
    }
    );
}
jQuery(document).ready(function () {
    backstretch();
});

function login_RequestForgetPass(apiDomain, message)
{
    $('#divLoading').show();
    $('.alert-danger', $('.forget-form')).hide();
    $.ajax({
        url: apiDomain + 'api/User/RequestPassword?Email=' + $('#email-forget-pass').val(),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            $('#divLoading').hide();
            $('.alert-danger', $('.forget-form')).show();
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    $('.alert-danger span', $('.forget-form')).text(message.sendmailresetpass);
                    
                    break;
                case 101:
                    $('.alert-danger span', $('.forget-form')).text(message.emailNotFound);
                    
                    break;
                default:
                    $('.alert-danger span', $('.forget-form')).text('Có lỗi xảy ra');
                    break;
            }
        },
        error: function () {
            $('#divLoading').hide();
            $('.alert-danger', $('.forget-form')).show();
            $('.alert-danger span', $('.forget-form')).text(message.hasError);
        }
    });
}





