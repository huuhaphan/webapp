﻿document.addEventListener("DOMContentLoaded", function (event) {
    removeElementsByClass('custom-toomarketer-register');
    toomarketer_rig_intialize();

});

var button = '<div class="el-content" style="display: inline-block; width: auto; height: auto; min-width: 350px; min-height: 57px;"><a onclick="toomarketer_reg()"  class="ib2-button"><i class="fa fa-external-link"></i> Dùng thử miễn phí</a></div>';
function toomarketer_rig_intialize() {
    removeElementsByClass('custom-toomarketer-register');
    form_loadjscssfile("http://beta.toomarketer.com/Content/custom-register-popup.css", "css");
    //form_loadjscssfile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css", "css");
    form_loadjscssfile("https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css", "css");

    var html =
         '<div class="custom-toomarketer-register" id="popup-custom-toomarketer-register">' +
    '<div class="row" id="toomarketer-content" style="max-width:990px;width:100%;margin:0 auto;">' +
       ' <div class="col-md-12" style="padding:0;" id="toomarketer-shark">' +
           ' <div class="portlet light bordered" style="padding:0;margin:0;">' +
               ' <div class="portlet-title bg-green-jungle">' +
               '<div class="text-center  bg-font-green-jungle " style="width:100%; padding:10px 10px;font-size:17px;pading-left:20px;">' +
                      '  Đăng ký Toomarketer' +
                      '  <div class="caption pull-right" style="margin-right:15px;">' +
                          '  <a onclick="close_toomarketer_reg()" style="padding:10px;cursor: pointer;"><i class="fa fa-close font-white" style="font-size:20px;margin-top: 5px;color:white;"></i></a>' +
                          '  </div>' +
                   ' </div>' +

               ' </div>' +
                '<div class="portlet-body" style="padding-top:0;">' +
                   ' <div class="row" style="margin:0;">' +
                       ' <div class="col-md-12">' +
                          '  <div class="col-md-6 col-sm-6 hidden-xs">' +
                              '  <p style="font-size:23px;">' +
                                  '   Phần mềm gửi SMS dễ dàng nhất thế giới.' +
                                '</p>' +
                               ' <p style="font-size:18px;">' +
                                  '  Không cần cài đặt, sử dụng dễ dàng, tiết kiệm chi phí.' +
                                '</p>' +
                                '<div class="text-center">' +
                                   ' <img src="http://www.smstudong.com/wp-content/uploads/2017/08/toomarketer-feature.png" />' +
                                '</div>' +
                                '<p style="text-align: center; margin-top: 20px;font-size:15px;">' +
                                   ' * Hỗ trợ đăng ký: 0902620761' +
                              '  </p>' +
                           ' </div>' +
                           ' <div class="col-md-6 col-sm-6 col-xs-12" style="pading:0 5px;">' +
                                '<div class="alert alert-danger" id="toomarketer-alert-danger" style="display: none;">' +
                                '<button class="close" data-close="alert" onclick="toomarketer_reg_close_error()"></button>' +
                                    '<span id="toomarketer-show-alert-message"></span>' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<label class="bold">Họ *</label>' +
                                    '<div class="input-icon">' +
                                      '  <i class="fa fa-font"></i>' +
                                        '<input class="form-control placeholder-no-fix input-md" type="text" autofocus="" id="reg_toomarketer_lastname">' +
                                   ' </div>' +
                               ' </div>' +
                                '<div class="form-group">' +
                                   ' <label class="bold">Tên *</label>' +
                                   ' <div class="input-icon">' +
                                        '<i class="fa fa-font"></i>' +
                                        '<input class="form-control placeholder-no-fix input-md" type="text" autofocus="" id="reg_toomarketer_firstname">' +
                                   ' </div>' +
                               ' </div>' +
                               ' <div class="form-group">' +
                                   ' <label class="bold">Số điện thoại *</label>' +
                                   ' <div class="input-icon">' +
                                       ' <i class="fa fa-mobile-phone"></i>' +
                                       ' <input class="form-control placeholder-no-fix input-md" type="text" autofocus="" id="reg_toomarketer_mobile">' +
                                   ' </div>' +
                                  '</div>' +
                                ' <div class="form-group">' +
                                  ' <label class="bold">Email *</label>' +
                                    '<div class="input-icon">' +
                                        '<i class="fa fa-envelope"></i>' +
                                        '<input class="form-control placeholder-no-fix input-md" type="email" autofocus="" id="reg_toomarketer_email">' +
                                   ' </div>' +
                               ' </div>' +
                              '  <div class="form-group">' +
                                   ' <label class="bold">Mật khẩu *</label>' +
                                   ' <div class="input-icon">' +
                                       ' <i class="fa fa-key"></i>' +
                                       ' <input class="form-control placeholder-no-fix " type="password" autofocus="" id="reg_toomarketer_password">' +
                                 '   </div>' +
                               ' </div>' +
                               ' <div class="form-group">' +
                                 '   <label class="bold">Xác nhận mật khẩu *</label>' +
                                    '<div class="input-icon">' +
                                       ' <i class="fa fa-key"></i>' +
                                        '<input class="form-control placeholder-no-fix " type="password" autofocus="" id="reg_toomarketer_repassword">' +
                                    '</div>' +
                               ' </div>' +
                                    '<div class="form-group text-center">' +
                                '  <a class="btn btn-md green-jungle" onclick="toomarketeter_reg_submit()" ><i class="fa fa-sign-in"></i> Đăng kí tài khoản</a>' +
                              '  </div>' +
                          '  </div>' +
                       ' </div>' +
                  '  </div>' +
               ' </div>' +
           ' </div>' +
    '</div>' +
   ' </div>' +
'</div>';

    var div = document.getElementById('custom-toomarketer-register');
    div.insertAdjacentHTML('beforeend', html);
}

function toomarketer_popup_margintop() {
    var wh = parseInt(window.innerHeight);
    var fh = parseInt(document.getElementById('toomarketer-content').offsetHeight);
    console.log(wh + ',' + fh);
    if (fh <= wh) {
        document.getElementById('toomarketer-content').style.marginTop = (wh - 20 - fh) / 2 + 'px';
    }

    else {
        document.getElementById('toomarketer-content').style.marginTop = 20 + 'px';
    }
}

function toomarketer_reg() {
    toomarketer_popup_margintop();
  
    var parent = toomarketer_findAncestor(document.getElementById('custom-toomarketer-register'), 'ib2-section-el');
    if (parent != null && parent != undefined) {
        parent.style.zIndex = "1000000000";
    }

    var parent = toomarketer_findAncestor(document.getElementById('custom-toomarketer-register'), 'ib2-wsection-el');
    if (parent != null && parent != undefined) {
        parent.style.zIndex = "1000000000";
    }

    document.getElementById('popup-custom-toomarketer-register').style.opacity = 1;
    document.getElementById('popup-custom-toomarketer-register').style.visibility = 'visible';
    document.getElementById('toomarketer-shark').style.animation = 'toomarketershake 0.82s cubic-bezier(.36,.07,.19,.97) both';
    document.getElementById('toomarketer-shark').style.translate3d = '(0, 0, 0)';

}

function toomarketer_findAncestor(el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls));
    return el;
}

function close_toomarketer_reg() {
    document.getElementById('popup-custom-toomarketer-register').style.opacity = 0;
    document.getElementById('popup-custom-toomarketer-register').style.visibility = 'hidden';
    document.getElementById('toomarketer-shark').style.animation = '';
    document.getElementById('toomarketer-shark').style.translate3d = '';

}

function removeElementsByClass(className) {
    var elements = document.getElementsByClassName(className);
    while (elements.length > 0) {
        elements[0].parentNode.removeChild(elements[0]);
    }
}
function form_loadjscssfile(filename, filetype) {
    if (filetype == "js") { //if filename is a external JavaScript file
        var fileref = document.createElement('script')
        fileref.setAttribute("type", "text/javascript")
        fileref.setAttribute("src", filename)
    }
    else if (filetype == "css") { //if filename is an external CSS file
        var fileref = document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
    }
    document.body.appendChild(fileref);

}

function toomarketer_reg_close_error() {
    document.getElementById('toomarketer-alert-danger').style.display = "none";
}

function toomarketeter_reg_submit() {
    if (toomarketer_reg_check()) {
        var data = toomarketer_reg_data();
        console.log(data);
        xhr = new XMLHttpRequest();
        xhr.open('POST', 'http://api.toomarketer.com/api/User/SignUp');
        xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
        xhr.onload = function () {
            if (xhr.status === 200) {
                var obj = JSON.parse(xhr.response);
                var errorcode = parseInt(obj.ErrorCode)
                switch (errorcode) {
                    case 0:
                        {
                            var cusfield = [{ CustomFieldId: 2, CustomFieldValue: data.FirstName + ' ' + data.LastName }, { CustomFieldId: 1, CustomFieldValue: data.Email }, { CustomFieldId: 10, CustomFieldValue: data.MobilePhone }];
                            var dataform = {
                                FormId: 10110,
                                CustomFields: cusfield,
                            }
                            xhrform = new XMLHttpRequest();
                            xhrform.open('POST', 'http://api.toomarketer.com/api/Contact/CreateFromForm');
                            xhrform.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                            xhrform.onload = function () {
                                if (xhrform.status === 200) {
                                    var obj = JSON.parse(xhrform.response);
                                    var errorcode = parseInt(obj.ErrorCode)
                                    switch (errorcode) {
                                        case 0:
                                            window.location.href = "http://www.smstudong.com/dang-ky-thanh-cong/";
                                            break;
                                        default:
                                            window.location.href = "http://www.smstudong.com/dang-ky-thanh-cong/";
                                            break;
                                    }
                                } else {

                                    window.location.href = "http://www.smstudong.com/dang-ky-thanh-cong/";
                                }
                            };
                            xhrform.send(serialize_toomarketer(dataform));
                        }
                        break;
                    case 102:
                        document.getElementById('toomarketer-alert-danger').style.display = "block";
                        document.getElementById('toomarketer-show-alert-message').innerText = "Email đã được đăng kí trước đó";
                        break;
                    default:
                        document.getElementById('toomarketer-alert-danger').style.display = "block";
                        document.getElementById('toomarketer-show-alert-message').innerText = "Có lỗi xảy ra. Đăng kí không thành công";
                        break;

                }
            } else {
                document.getElementById('toomarketer-alert-danger').style.display = "block";
                document.getElementById('toomarketer-show-alert-message').innerText = "Có lỗi xảy ra. Đăng kí không thành công";
            }
        };
        xhr.send(JSON.stringify(data));
    }
}

var serialize_toomarketer = function (obj, prefix) {
    var str = [], p;
    for (p in obj) {
        if (obj.hasOwnProperty(p)) {
            var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
            str.push((v !== null && typeof v === "object") ?
              serialize_toomarketer(v, k) :
              encodeURIComponent(k) + "=" + encodeURIComponent(v));
        }
    }
    return str.join("&");
}

function toomarketer_check_mail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function toomarketer_reg_data() {
    var data = {
        FirstName: document.getElementById('reg_toomarketer_firstname').value,
        LastName: document.getElementById('reg_toomarketer_lastname').value,
        MobilePhone: document.getElementById('reg_toomarketer_mobile').value,
        PassWord: document.getElementById('reg_toomarketer_password').value,
        Email: document.getElementById('reg_toomarketer_email').value.trim(),

    }
    return data;
}

function toomarketer_reg_check() {
    if (!document.getElementById('reg_toomarketer_lastname').value) {

        document.getElementById('toomarketer-alert-danger').style.display = "block";
        document.getElementById('toomarketer-show-alert-message').innerText = "Vui lòng nhập họ";
        return false;
    }
    if (!document.getElementById('reg_toomarketer_firstname').value) {
        document.getElementById('toomarketer-alert-danger').style.display = "block";
        document.getElementById('toomarketer-show-alert-message').innerText = "Vui lòng nhập tên"
        return false;
    }
    if (!document.getElementById('reg_toomarketer_mobile').value) {
        document.getElementById('toomarketer-alert-danger').style.display = "block";
        document.getElementById('toomarketer-show-alert-message').innerText = "Vui lòng nhập số điện thoại"
        return false;
    }
    if (!document.getElementById('reg_toomarketer_email').value) {
        document.getElementById('toomarketer-alert-danger').style.display = "block";
        document.getElementById('toomarketer-show-alert-message').innerText = "Vui lòng nhập email"
        return false;
    }

    if (!toomarketer_check_mail(document.getElementById('reg_toomarketer_email').value.trim())) {
        document.getElementById('toomarketer-alert-danger').style.display = "block";
        document.getElementById('toomarketer-show-alert-message').innerText = "Email không đúng định dạng"
        return false;
    }

    if (!document.getElementById('reg_toomarketer_password').value) {
        document.getElementById('toomarketer-alert-danger').style.display = "block";
        document.getElementById('toomarketer-show-alert-message').innerText = "Vui lòng nhập mật khẩu"
        return false;
    }
    if (!document.getElementById('reg_toomarketer_repassword').value) {
        document.getElementById('toomarketer-alert-danger').style.display = "block";
        document.getElementById('toomarketer-show-alert-message').innerText = "Vui lòng nhập xác thực mật khẩu"
        return false;
    }
    if (document.getElementById('reg_toomarketer_repassword').value.localeCompare(document.getElementById('reg_toomarketer_repassword').value) == -1) {
        document.getElementById('toomarketer-alert-danger').style.display = "block";
        document.getElementById('toomarketer-show-alert-message').innerText = "Xác thực mật khẩu không chính xác"
        return false;
    }

    else return true;

}