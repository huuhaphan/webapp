﻿function updateinfo_Action(apiDomain, userid, parentid, email, message) {
    backstretch();
    updateinfo_UpdateSubUser(apiDomain, userid, parentid, email, message);
}

function updateinfo_UpdateSubUser(apiDomain, userid, parentid, email, message) {
    $('#update-info-subuser').on('click', function () {
        if( updateinfo_CheckData(message))
        {
            updateinfo_ActionUpdate(apiDomain, userid, parentid, email, message)
        }
    })
};


function updateinfo_ActionUpdate(apiDomain, userid, parentid, email, message)
{
    $('#divLoading').show();
    var mobile = $('#txtupdatesub-mobile').val().trim();
    if (mobile) {
        if (parseInt(mobile.substr(0, 2)) == 84) {
            mobile = '0' + mobile.replace(mobile.substr(0, 2), '');
        }
        if (mobile.substr(0, 3) == '+84') {
            mobile = '0' + mobile.replace(mobile.substr(0, 3), '');
        }
    }
    var objdata = {
        UserId: userid,
        ParentId: parentid,
        Password: $('#txtupdatesub-password').val(),
        FirstName: $('#txtupdatesub-firstname').val(),
        LastName: $('#txtupdatesub-lastname').val(),
        MobilePhone: mobile,
    };
    $.ajax({
        url: apiDomain + 'api/User/CompleteSubUserInfo',
        type: 'post',
        data: JSON.stringify(objdata),
        contentType: 'application/json; charset=utf-8',
        success:function(r)
        {
            $('#divLoading').hide();
            switch(parseInt(r.ErrorCode))
            {
                case 0:
                   
                    var returnurl = '/Contacts';
                    var data = {
                        password: objdata.Password,
                        username: email,
                        grant_type: 'password',
                    }
                    login_SignIn(apiDomain, message, returnurl, data);
                    break;
                default:
                    custom_shownotification('error', message.hasError);
                    break;
            }
        },error:function(x,s,e)
        {
            $('#divLoading').hide();
            custom_shownotification('error', message.hasError);
        }
    })
}

function updateinfo_CheckData(message)
{
    if ($('#txtupdatesub-firstname').val() == '') {
        custom_shownotification('error', message.errorFirstname);
        return false;
    }
    if ($('#txtupdatesub-lastname').val() == '') {
        custom_shownotification('error', message.errorLastname);
        return false;
    }
    if ($('#txtupdatesub-mobile').val() == '') {
        custom_shownotification('error', message.errorMobile);
        return false;
    }
    if ($('#txtupdatesub-mobile').val() != '' && !IsMobile($('#txtupdatesub-mobile').val())) {
        custom_shownotification('error', message.errorFormatMobile);
        return false;
    }

    if ($('#txtupdatesub-password').val() == '') {
        custom_shownotification('error', message.errorPass);
        return false;
    }
    var pass = $('#txtupdatesub-password').val();
    if (pass != '' && (pass.length < 6 || pass.length > 100)) {
        custom_shownotification('error', message.errorLeastPass);
        return false;
    }
    if ($('#txtupdatesub-repassword').val() == '') {
        custom_shownotification('error', message.errorRePass);
        return false;
    }
    if ($('#txtupdatesub-repassword').val() != $('#txtupdatesub-password').val()) {
        custom_shownotification('error', message.errorComparePass);
        return false;
    }
    else return true;

}
function backstretch() {
    // init background slide images
    $.backstretch([
        "/Content/Images/login-background.jpg",
    ], {
        fade: 1000,
        duration: 8000
    }
    );
}
