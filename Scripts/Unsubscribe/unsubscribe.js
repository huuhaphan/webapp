﻿function unsubscribe_Action(apiDomain,Email,Id, message)
{
    UserLock.init();
    unsubscribe_EventUnsub(apiDomain,Email,Id, message);
}

function unsubscribe_EventUnsub(apiDomain,Email,Id,message)
{
    $('#btn-unsubscribe').on('click', function () {
        $.ajax({
            url: apiDomain+'api/Contact/UnsubscribeFromUser?Email=' + Email + '&Id=' + Id,
            type: 'Post',
            contentType: 'application/json; charset=utf-8',
            success:function(r)
            {
                switch(parseInt(r.ErrorCode))
                {
                    case 0:
                        window.location.href = '/unsubscribe/success';
                        break;
                    default:
                        custom_shownotification('error', 'Có lỗi xảy ra. Vui lòng thử lại sau');

                        break;
                        
                }
            },
            error:function()
            {
                custom_shownotification('error', 'Có lỗi xảy ra. Vui lòng thử lại sau');

            }
        })
    });
}

var UserLock = function () {
    return {
        //main function to initiate the module
        init: function () {

            $.backstretch([
                "/Content/Images/login-background.jpg",

            ], {
                fade: 1000,
                duration: 8000
            });
        }

    };

}();
