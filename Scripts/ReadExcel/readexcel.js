﻿/// <reference path="../appshare.js" />
function readexcelEvent(apiDomain, access_token, ContactGroupId, CampaignId,gName, message) {

    readexcel_CheckSMSGateWay(apiDomain, access_token, CampaignId, message)
    readexcel_LoadFullField(apiDomain, access_token, message);
    ActionImport(apiDomain, access_token, ContactGroupId, CampaignId, gName, message);
    readexcel_GoToContactList(ContactGroupId, CampaignId, gName, message);

    var X = XLSX;
    var XW = {
        /* worker message */
        msg: 'xlsx',
        /* worker scripts */
        rABS: '/Scripts/ReadExcel/xlsxworker2.js',
        norABS: '/Scripts/ReadExcel/xlsxworker1.js',
        noxfer: '/Scripts/ReadExcel/xlsxworker.js'
    };

    var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";
    if (!rABS) {
        document.getElementsByName("userabs")[0].disabled = true;
        document.getElementsByName("userabs")[0].checked = false;
    }

    var use_worker = typeof Worker !== 'undefined';
    if (!use_worker) {
        document.getElementsByName("useworker")[0].disabled = true;
        document.getElementsByName("useworker")[0].checked = false;
    }

    var transferable = use_worker;
    if (!transferable) {
        document.getElementsByName("xferable")[0].disabled = true;
        document.getElementsByName("xferable")[0].checked = false;
    }

    var wtf_mode = false;

    function fixdata(data) {
        var o = "", l = 0, w = 10240;
        for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
        o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
        return o;
    }

    function ab2str(data) {
        var o = "", l = 0, w = 10240;
        for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w, l * w + w)));
        o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w)));
        return o;
    }

    function s2ab(s) {
        var b = new ArrayBuffer(s.length * 2), v = new Uint16Array(b);
        for (var i = 0; i != s.length; ++i) v[i] = s.charCodeAt(i);
        return [v, b];
    }

    function xw_noxfer(data, cb) {
        var worker = new Worker(XW.noxfer);
        worker.onmessage = function (e) {
            switch (e.data.t) {
                case 'ready': break;
                case 'e': console.error(e.data.d); break;
                case XW.msg: cb(JSON.parse(e.data.d)); break;
            }
        };
        var arr = rABS ? data : btoa(fixdata(data));
        worker.postMessage({ d: arr, b: rABS });
    }

    function xw_xfer(data, cb) {
        var worker = new Worker(rABS ? XW.rABS : XW.norABS);
        worker.onmessage = function (e) {
            switch (e.data.t) {
                case 'ready': break;
                case 'e': console.error(e.data.d);
                    custom_shownotification("error", message.FileNotSupport);
                    $('#divLoading').hide();
                    break;
                default:
                    var xx = ab2str(e.data).replace(/\n/g, "\\n").replace(/\r/g, "\\r");
                    //console.log(xx);
                    console.log("done");
                    cb(JSON.parse(xx)); break;
            }
        };
        if (rABS) {
            var val = s2ab(data);
            worker.postMessage(val[1], [val[1]]);
        } else {
            worker.postMessage(data, [data]);
        }
    }

    function xw(data, cb) {
        transferable = document.getElementsByName("xferable")[0].checked;
        if (transferable) xw_xfer(data, cb);
        else xw_noxfer(data, cb);
    }

    function get_radio_value(radioName) {
        var radios = document.getElementsByName(radioName);
        for (var i = 0; i < radios.length; i++) {
            if (radios[i].checked || radios.length === 1) {
                return radios[i].value;
            }
        }
    }

    function to_json(workbook, listcols, range) {
        //var Sheet1 = workbook.SheetNames['Sheet1'];
        //if (Sheet1) {
        //    var first_sheet_name = workbook.SheetNames['Sheet1'];
        //}
        //else {
        var first_sheet_name = workbook.SheetNames[0];

        //}
        var result = [];
        var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[first_sheet_name], { header: listcols, range: range });
        if (roa.length > 0) {
            result = roa;
        }
        return result;
    }

    var address_of_cell_start = 0;
    var listcols;
    var listitem;
    function to_mapping(workbook) {
        listcols = [];
        listitem = [];
        //var Sheet1 = workbook.SheetNames['Sheet1'];
        //if (Sheet1) {
        //    var first_sheet_name = workbook.SheetNames['Sheet1'];
        //}
        //else {
        var first_sheet_name = workbook.SheetNames[0];

        //}

        var wr = workbook.Sheets[first_sheet_name];

        var col = wr["!ref"];
        if (col != undefined) {
            //console.log(col);
            listcols = _buildColumnsArray(col);
            address_of_cell_start = parseInt(col.split(':')[0].replace(/\D+/g, ''));

            for (var i = 0; i < listcols.length; i++) {
                var item = [];

                for (var j = address_of_cell_start; j < address_of_cell_start + 4; j++) {
                    var desired_value = '';
                    var desired_cell = wr[listcols[i] + j];
                    if (desired_cell) {
                        desired_value = desired_cell.w;
                    }
                    item.push(desired_value);
                }
                listitem.push(item);

            }

            $('#list-has-column-name').prop('checked', true)
            readexcel_TableMapping(listcols, listitem, true);
            $('#table-before-import-contact').show();
            $('#footer-table-before-import').show();
            $('#table-info-import-contact').hide();
            $('#footer-table-info-import').hide();
            $('#modal-mapping-contact').modal('toggle');
        } else {
            custom_shownotification("error", "Nội dung file không đúng định dạng");
        }
    }

    function to_csv(workbook) {
        var result = [];
        workbook.SheetNames.forEach(function (sheetName) {
            var csv = X.utils.sheet_to_csv(workbook.Sheets[sheetName]);
            if (csv.length > 0) {
                result.push("SHEET: " + sheetName);
                result.push("");
                result.push(csv);
            }
        });
        return result.join("\n");
    }

    function to_formulae(workbook) {
        var result = [];
        workbook.SheetNames.forEach(function (sheetName) {
            var formulae = X.utils.get_formulae(workbook.Sheets[sheetName]);
            if (formulae.length > 0) {
                result.push("SHEET: " + sheetName);
                result.push("");
                result.push(formulae.join("\n"));
            }
        });
        return result.join("\n");
    }

    var process_wb_data;
    function process_wb(wb) {

        to_mapping(wb);
        process_wb_data = wb;
        readexcel_BackImport();
        if (typeof console !== 'undefined') {
            $('#divLoading').hide();
        }
    }

    var drop = document.getElementById('drop');
    function handleDrop(e) {
        e.stopPropagation();
        e.preventDefault();
        rABS = document.getElementsByName("userabs")[0].checked;
        use_worker = document.getElementsByName("useworker")[0].checked;
        var files = e.dataTransfer.files;
        var f = files[0];
        {
            var reader = new FileReader();
            var name = f.name;
            reader.onload = function (e) {
                if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
                var data = e.target.result;
                if (use_worker) {
                    xw(data, process_wb);
                } else {
                    var wb;
                    if (rABS) {
                        wb = X.read(data, { type: 'binary' });
                    } else {
                        var arr = fixdata(data);
                        wb = X.read(btoa(arr), { type: 'base64' });
                    }
                    process_wb(wb);
                }
            };
            if (rABS) reader.readAsBinaryString(f);
            else reader.readAsArrayBuffer(f);
        }
    }

    function handleDragover(e) {
        e.stopPropagation();
        e.preventDefault();
        e.dataTransfer.dropEffect = 'copy';
    }

    if (drop.addEventListener) {
        drop.addEventListener('dragenter', handleDragover, false);
        drop.addEventListener('dragover', handleDragover, false);
        drop.addEventListener('drop', handleDrop, false);
    }

    var xlf = document.getElementById('xlf');
    function handleFile(e) {
        rABS = document.getElementsByName("userabs")[0].checked;
        use_worker = document.getElementsByName("useworker")[0].checked;
        var files = e.target.files;
        var f = files[0];
        {
            var reader = new FileReader();
            var name = f.name;
            reader.onload = function (e) {
                if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
                var data = e.target.result;
                if (use_worker) {
                    $('#divLoading').show();
                    xw(data, process_wb);
                } else {
                    var wb;
                    if (rABS) {
                        wb = X.read(data, { type: 'binary' });
                    } else {
                        var arr = fixdata(data);
                        wb = X.read(btoa(arr), { type: 'base64' });
                    }
                    $('#divLoading').show();
                    process_wb(wb);
                }
            };
            if (rABS) reader.readAsBinaryString(f);
            else reader.readAsArrayBuffer(f);
        }
    }

   
        $('#btn-import-contact').on('click', function () {

            var listcols = [];
            var listaddcolum=[];
            var ls = $('#tbody-import-contact .select-field-import-contact');
            if (ls.length > 0) {
                $.each(ls, function (i, o) {
                    listcols.push(o.value);
                    listaddcolum.push(o.value);
                });

                for (var i = listaddcolum.length - 1; i--;) {
                    if (listaddcolum[i] === "-1") listaddcolum.splice(i, 1);
                }
                var unique = _.uniq(listaddcolum);
                if (unique.length != listaddcolum.length) {
                    custom_shownotification("error", "Mỗi cột import chỉ chọn một lần");
                }
                else {

                    var startjson = 0;
                    if ($('#list-has-column-name').prop('checked')) {
                        startjson = 1;
                    }
                    else {
                        startjson = 0;
                    }
                    var output = JSON.stringify(to_json(process_wb_data, listcols, startjson), 2, 2);
                    ProcessJsonOfExcel(output, message);
                }
            } else {
                custom_shownotification("error", "Không có dữ liệu import");
            }

            //readexcel_Knob();
        });
    

    $("#xlf").on("change", function (e) { handleFile(e) });



    $('#list-has-column-name').on('change', function () {
        readexcel_TableMapping(listcols, listitem,false);
    });

    $('#btn-cancel-import').on('click', function () {
        ContactList = [];
        countInfile = 0;
        countIncorrect = 0;
        var control = $('#xlf');
        control.replaceWith(control = control.clone(true)); //reset input file
    });
}

function _buildColumnsArray(range) {
    var i,
        res = [],
        rangeNum = range.split(':').map(function (val) {
            return alphaToNum(val.replace(/[0-9]/g, ''));
        }),
        start = rangeNum[0],
        end = rangeNum[1] + 1;

    for (i = start; i < end ; i++) {
        res.push(numToAlpha(i));
    }

    return res;
}

function alphaToNum(alpha) {

    var i = 0,
        num = 0,
        len = alpha.length;

    for (; i < len; i++) {
        num = num * 26 + alpha.charCodeAt(i) - 0x40;
    }
    return num - 1;
}

function numToAlpha(num) {

    var alpha = '';

    for (; num >= 0; num = parseInt(num / 26, 10) - 1) {
        alpha = String.fromCharCode(num % 26 + 0x41) + alpha;
    }

    return alpha;
}


function readexcel_TableMapping(row,data, first)
{
    var hasselect = [];
    var listhasselect = $('#tbody-import-contact .select-field-import-contact');
    if (listhasselect) {
        $.each(listhasselect, function (i, o) {
            hasselect.push($(this).val());
        });
    }

    $('#tbody-import-contact').children().remove();
    $('#thead-import-contact').children().remove();

    var html = ''
    var td = '';
    var thead = '';

    
       thead+= '<tr>' +
               '<th>' +
                 'Chọn cột cần import' +
               '</th>' +
               '<th>' + 
                 'Tên cột trong file excel' +
               '</th>' +
               '<th>' +
                 'Giá trị 4 dòng đầu trong file excel' +
               '</th>' +
         ' </tr>';
   
    $('#thead-import-contact').append(thead);

    var select = '<select class="select-field-import-contact form-control" style="width:100%">' +
        listsystemfield+
        '</select>'
    for (var i = 0; i < row.length; i++) {
        var td = '<td class="row-select-field text-left">' + select + '</td>';
        var valuetd = '';
        for(var j=0;j<data[i].length;j++)
        {
            if ($('#list-has-column-name').prop('checked')) {
                if (j != 0) {
                    valuetd += data[i][j] + ' , ';
                }

            } else {
                    valuetd += data[i][j] + ' , ';

            }
            
        }
        if (!$('#list-has-column-name').prop('checked')) {
            td += '<td class="row-defaul-value text-left">Cột ' +numToAlpha(i)+ '</td>';
        } else {
            td += '<td class="row-defaul-value text-left">' + data[i][0] + '</td>';
        }
        td += '<td class="row-defaul-value text-left">' + valuetd + '</td>';
        html += '<tr>' +td+'</tr>';
    }
    $('#tbody-import-contact').append(html);

    var ls = $('#tbody-import-contact .select-field-import-contact');
    if (first) { //nếu mới thì chọn mặc định (tạm thời không dùng)
        //$.each(ls, function (i, o) {
        //    switch (i) {
        //        case 0:
        //            $(this).val('Name');
        //            break;
        //        case 1:
        //            $(this).val('Email');
        //            break;
        //        case 2:
        //            $(this).val('Mobile');
        //            break;

        //    }

        //});
    } else { //chọn lại những cột đã chọn trước đó 

        $.each(ls, function (i, o) {
            $(this).val(hasselect[i]);
        });


    }
    
    //console.log(parseInt(parseInt($(window).innerHeight()) * 80 / 100));
    //console.log($('#table-before-import-contact').outerHeight());
    if (row.length>10) {
        var height = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.portlet-title').outerHeight() - $('#divAction-toolbar').outerHeight() - $('.page-footer').outerHeight() - 200;
        $('.inner_table').css({
            'height': 350,
        });
    }
    else {
        $('.inner_table').css({
            'height': 'auto',
        });
    }
}


function readexcel_BackImport() {
    $('#btn-back-import-contact').on('click', function () {
        $('#table-before-import-contact').show();
        $('#footer-table-before-import').show();
        $('#table-info-import-contact').hide();
        $('#footer-table-info-import').hide();
    });
}

var listsystemfield;
function readexcel_LoadFullField(apiDomain, access_token, message) {
    var json = appshare_LoadFullField(apiDomain, access_token, message);
    switch (parseInt(json.ErrorCode)) {
        case 0: {
            var notused = '<option value="-1">Không sử dụng</option>';
            var first = '';
            $.each(json.Data, function (i, o) {
                if (parseInt(o.FieldId) == 1 || parseInt(o.FieldId) == 2 || parseInt(o.FieldId) == 10 ) {
                    
                    switch (parseInt(o.FieldId))
                    {
                        case 1:
                            {
                                var sp = '<option value="' + o.FieldName + '" data-fieldname="'+o.FieldName+'" data-maxlength="'+o.MaxLength+'">' + o.FieldName + ' ('+o.MaxLength+' ' +message.Character+')</option>';
                                first = sp + first;
                            }
                            break;
                        case 2:
                            {
                                var sp = '<option value="' + o.FieldName + '" data-fieldname="' + o.FieldName + '" data-maxlength="' + o.MaxLength + '">' + o.FieldName + ' (' + o.MaxLength + ' ' + message.Character + ')</option>';
                                first = sp + first;
                            }
                            break;
                        case 10:
                            {
                                var sp = '<option value="' + o.FieldName + '" data-fieldname="' + o.FieldName + '" data-maxlength="' + o.MaxLength + '">' + o.FieldName + ' (' + o.MaxLength + ' ' + message.Character + ')</option>';
                                first = first + sp;
                            }
                            break;
                    }
                   
                } else {
                    if (parseInt(o.FieldId) != 10078) {
                        option += '<option value="' + o.FieldId + '" data-fieldname="' + o.FieldName + '" data-maxlength="' + o.MaxLength + '">' + o.FieldName + ' (' + o.MaxLength + ' ' + message.Character + ')</option>';
                    }
                }
            });
            listsystemfield = notused +first+ option;
        }
            break;
        default: {
            var option = '<option value="-1">Không sử dụng</option>';
        }
            break;
    }
};



//import mobile number
//function readexcelEventGetMobile(message) {
//    var X = XLSX;
//    var XW = {
//        /* worker message */
//        msg: 'xlsx',
//        /* worker scripts */
//        rABS: '/Scripts/ReadExcel/xlsxworker2.js',
//        norABS: '/Scripts/ReadExcel/xlsxworker1.js',
//        noxfer: '/Scripts/ReadExcel/xlsxworker.js'
//    };

//    var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";
//    if (!rABS) {
//        document.getElementsByName("userabs")[0].disabled = true;
//        document.getElementsByName("userabs")[0].checked = false;
//    }

//    var use_worker = typeof Worker !== 'undefined';
//    if (!use_worker) {
//        document.getElementsByName("useworker")[0].disabled = true;
//        document.getElementsByName("useworker")[0].checked = false;
//    }

//    var transferable = use_worker;
//    if (!transferable) {
//        document.getElementsByName("xferable")[0].disabled = true;
//        document.getElementsByName("xferable")[0].checked = false;
//    }

//    var wtf_mode = false;

//    function fixdata(data) {
//        var o = "", l = 0, w = 10240;
//        for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
//        o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
//        return o;
//    }

//    function ab2str(data) {
//        var o = "", l = 0, w = 10240;
//        for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w, l * w + w)));
//        o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w)));
//        return o;
//    }

//    function s2ab(s) {
//        var b = new ArrayBuffer(s.length * 2), v = new Uint16Array(b);
//        for (var i = 0; i != s.length; ++i) v[i] = s.charCodeAt(i);
//        return [v, b];
//    }

//    function xw_noxfer(data, cb) {
//        var worker = new Worker(XW.noxfer);
//        worker.onmessage = function (e) {
//            switch (e.data.t) {
//                case 'ready': break;
//                case 'e': console.error(e.data.d); break;
//                case XW.msg: cb(JSON.parse(e.data.d)); break;
//            }
//        };
//        var arr = rABS ? data : btoa(fixdata(data));
//        worker.postMessage({ d: arr, b: rABS });
//    }

//    function xw_xfer(data, cb) {
//        var worker = new Worker(rABS ? XW.rABS : XW.norABS);
//        worker.onmessage = function (e) {
//            switch (e.data.t) {
//                case 'ready': break;
//                case 'e': console.error(e.data.d);
//                    custom_shownotification("error", message.FileNotSupport);
//                    $('#divLoading').hide();
//                    break;
//                default: xx = ab2str(e.data).replace(/\n/g, "\\n").replace(/\r/g, "\\r"); console.log("done"); cb(JSON.parse(xx)); break;
//            }
//        };
//        if (rABS) {
//            var val = s2ab(data);
//            worker.postMessage(val[1], [val[1]]);
//        } else {
//            worker.postMessage(data, [data]);
//        }
//    }

//    function xw(data, cb) {
//        transferable = document.getElementsByName("xferable")[0].checked;
//        if (transferable) xw_xfer(data, cb);
//        else xw_noxfer(data, cb);
//    }

//    function get_radio_value(radioName) {
//        var radios = document.getElementsByName(radioName);
//        for (var i = 0; i < radios.length; i++) {
//            if (radios[i].checked || radios.length === 1) {
//                return radios[i].value;
//            }
//        }
//    }

//    function to_json(workbook) {
//        var result = {};
//        workbook.SheetNames.forEach(function (sheetName) {
//            var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
//            if (roa.length > 0) {
//                result[sheetName] = roa;
//            }
//        });
//        return result;
//    }

//    function to_csv(workbook) {
//        var result = [];
//        workbook.SheetNames.forEach(function (sheetName) {
//            var csv = X.utils.sheet_to_csv(workbook.Sheets[sheetName]);
//            if (csv.length > 0) {
//                result.push("SHEET: " + sheetName);
//                result.push("");
//                result.push(csv);
//            }
//        });
//        return result.join("\n");
//    }

//    function to_formulae(workbook) {
//        var result = [];
//        workbook.SheetNames.forEach(function (sheetName) {
//            var formulae = X.utils.get_formulae(workbook.Sheets[sheetName]);
//            if (formulae.length > 0) {
//                result.push("SHEET: " + sheetName);
//                result.push("");
//                result.push(formulae.join("\n"));
//            }
//        });
//        return result.join("\n");
//    }


//    function process_wb(wb) {

//        var output = JSON.stringify(to_json(wb), 2, 2);

//        ProcessJsonOfExcelNumber(output, message);
//        if (typeof console !== 'undefined') {
//            $('#divLoading').hide();
//        }
//    }

//    var drop = document.getElementById('dropmobile');
//    function handleDrop(e) {
//        e.stopPropagation();
//        e.preventDefault();
//        rABS = document.getElementsByName("userabs")[0].checked;
//        use_worker = document.getElementsByName("useworker")[0].checked;
//        var files = e.dataTransfer.files;
//        var f = files[0];
//        {
//            var reader = new FileReader();
//            var name = f.name;
//            reader.onload = function (e) {
//                if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
//                var data = e.target.result;
//                if (use_worker) {
//                    xw(data, process_wb);
//                } else {
//                    var wb;
//                    if (rABS) {
//                        wb = X.read(data, { type: 'binary' });
//                    } else {
//                        var arr = fixdata(data);
//                        wb = X.read(btoa(arr), { type: 'base64' });
//                    }
//                    process_wb(wb);
//                }
//            };
//            if (rABS) reader.readAsBinaryString(f);
//            else reader.readAsArrayBuffer(f);
//        }
//    }

//    function handleDragover(e) {
//        e.stopPropagation();
//        e.preventDefault();
//        e.dataTransfer.dropEffect = 'copy';
//    }

//    if (drop.addEventListener) {
//        drop.addEventListener('dragenter', handleDragover, false);
//        drop.addEventListener('dragover', handleDragover, false);
//        drop.addEventListener('drop', handleDrop, false);
//    }



//    var xlf = document.getElementById('xlfmobile');
//    function handleFile(e) {
//        rABS = document.getElementsByName("userabs")[0].checked;
//        use_worker = document.getElementsByName("useworker")[0].checked;
//        var files = e.target.files;
//        var f = files[0];
//        {
//            var reader = new FileReader();
//            var name = f.name;
//            reader.onload = function (e) {
//                if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
//                var data = e.target.result;
//                if (use_worker) {
//                    $('#divLoading').show();
//                    xw(data, process_wb);
//                } else {
//                    var wb;
//                    if (rABS) {
//                        wb = X.read(data, { type: 'binary' });
//                    } else {
//                        var arr = fixdata(data);
//                        wb = X.read(btoa(arr), { type: 'base64' });
//                    }
//                    $('#divLoading').show();
//                    process_wb(wb);
//                }
//            };
//            if (rABS) reader.readAsBinaryString(f);
//            else reader.readAsArrayBuffer(f);
//        }
//    }

//    $("#xlfmobile").on("change", function (e) { handleFile(e) });
//}



var ContactList;
var countInfile;
var countIncorrect;

//duyệt lại list json lấy từ excel chuyển sang dang object và kiểm tra lỗi định dạng
function ProcessJsonOfExcel(json, message) {
    ContactList = [];
    countInfile = 0;
    countIncorrect = 0;

    var formats = [
    moment.ISO_8601,
    "YYYY/MM/DD"
    ];
    var listjson = JSON.parse(json);
    if (listjson.length>0) {

        $.each(listjson, function (idx, obj) {

            //if (obj[5] && obj[5][0]!='' && !moment(obj[5][0], formats, true).isValid()) {
            //    custom_shownotification("error", "Chọn cột giá trị cho Birthday không phù hợp");
            //    return false;
            //}
            //else {
            var accept = false;
            var Email = '';
            var Mobile = '';
                if (obj.Email || obj.Mobile) {
                    accept = true;
                    if (obj.Email && !IsEmail(obj.Email.trim())) {
                        accept = false;
                    }
                    if (obj.Mobile) {
                        // Mobile= obj.Mobile.replace(/\s/g, "").trim();
                        var smobile = obj.Mobile.replace(/\s/g, "").trim();
                       
                            if (smobile && parseInt(smobile.substr(0, 1)) != 0) {
                                if (smobile.length > 10) {
                                    if (parseInt(smobile.substr(0, 2)) == 84) {
                                        Mobile = '0' + smobile.replace((smobile).substr(0, 2), '').trim();
                                    }
                                    if (smobile.substr(0, 3) == '+84') {
                                        Mobile = '0' + smobile.replace(smobile.substr(0, 3), '').trim();
                                    }
                                } else {
                                    Mobile = '0' + smobile.replace(/\s/g, "").trim();


                                }
                            } else {
                                Mobile = smobile.replace(/\s/g, "").trim();

                            }

                            if (!IsMobile(Mobile.replace(/\s/g, "").trim())) {
                                accept = false;
                            } 
                    }
                }
                if (!accept) {
                    console.log(obj.Mobile);
                }
                if (accept) {
                    var customfield = []
                    if (obj.Email) {
                        Email = obj.Email.trim();
                    }

                    var ContactName = obj.Name;
                    delete obj.Name;
                    delete obj.Mobile;
                    delete obj.Email;

                    var NameLength = parseInt($('.select-field-import-contact option[value=Name]').attr('data-maxlength'));
                    var EmailLength = parseInt($('.select-field-import-contact option[value=Email]').attr('data-maxlength'));
                    if (ContactName && ContactName.length <= NameLength || Email && Email.length <= EmailLength) {
                        $.each(obj, function (i, o) {
                            if (o.length <= parseInt($('.select-field-import-contact option[value=' + i + ']').attr('data-maxlength'))) {
                                cus = { CustomFieldId: i, CustomFieldValue: o, CustomFieldName: $('.select-field-import-contact option[value=' + i + ']').attr('data-fieldname') };
                                customfield.push(cus);
                            }
                        });
                        var data = { ContactName: ContactName, Email: Email, Mobile: Mobile, CustomFields: customfield };
                        ContactList.push(data);
                    }
                    
                   
                }

                $('#table-before-import-contact').hide();
                $('#footer-table-before-import').hide();
                $('#table-info-import-contact').show();

                if (ContactList.length > 100 && isgateway==true) {
                    $('#has-sms-gateway').show();
                } else {
                    $('#has-sms-gateway').hide();
                }
                $('#footer-table-info-import').show();
            //}
        });
        countInfile = ContactList.length;
        countIncorrect = listjson.length - countInfile;
        console.log('data ' + ContactList);
        console.log('data ' + JSON.stringify(ContactList));
        $('#total-query-contact').text(countInfile);

        if (countIncorrect > 0){
            $('#info-total-not-query').show();
            $('#total-not-query-contact').text(countIncorrect);
        } else {
            $('#info-total-not-query').hide();
            $('#total-not-query-contact').text(0);
        }

        if (countInfile == 0) {
            readexcel_DisabelSubmit(true);
        } else {
            readexcel_DisabelSubmit(false);
        }
        
        $('#divLoading').hide();
    }
    else {
        custom_shownotification("error", 'Vui lòng chọn các cột với giá trị tương ứng');
        $('#divLoading').hide();
    }
}



//var countInfileMobile;
//var MobileList = new Array();
//function ProcessJsonOfExcelNumber(json, message) {

//    var listjson = JSON.parse(json).Sheet1;
//    if (listjson) {
//        //$.each(listjson, function (i, o)
//        //{
//        //    if (!o.Mobile) {
//        //        custom_shownotification("error", 'Không có cột Mobile');
//        //        $('#divLoading').hide();
//        //        return false;
//        //    }
//        //    else
//        //    {
//        //        MobileList.push(o.Mobile);
//        //    }
//        //})
//        countInfileMobile = listjson.length;
//        if (countInfileMobile > 0) {
//            $('#countInfileMobile').text(countInfileMobile);
//            $('#modal-import-mobile-from-excel').modal('toggle');
//            $('#modalBeforeImportMobile').modal('toggle');
//        }
//        $('#divLoading').hide();
//    }
//    else {
//        custom_shownotification("error", message.FormatError);
//        $('#divLoading').hide();
//    }
//}

//var count = 0;
//var countafter = 0;
//var $table = $('#BeforeSendContact');
//var $tableAfter = $('#AfterSendContact');

//tạo sumilator ajax load dữ liệu
//function ContactToImport(allobj,tableId)
//{
//    var dataTable = $(tableId);
//    $.mockjax({
//        url: '/contact/'+count,
//        responseTime: 200,
//        responseText: { aaData: allobj }
//    });

//    if(count==0)
//    {
//        TableContactBeforeImport()
//    }
//    if(count!=0)
//    {
//        readexel_RefreshTable(tableId, '/contact/' + count);
//    }
//    count++;
  
//}
//var countafter = 0;
//function ContactAfterImport (allobj,tableId)
//{
//    if (countafter == 0)
//    {
//        TableContactAfterImport(allobj, tableId)
//    }
//    if (countafter != 0)
//    {
//        readexel_RefreshTable(tableId, allobj);
//    }
//    countafter++;
//}



//refesh table khi show poup//
function readexel_RefreshTable(tableId, allobj) {
        table = $(tableId).dataTable();
        oSettings = table.fnSettings();
        table.fnClearTable(this);

        for (var i = 0; i < allobj.length; i++) {
            table.oApi._fnAddData(oSettings, allobj[i]);
        }

        oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
        table.fnDraw();
}

//edit trực tiếp trên table vào cập nhập xuống list object

//function EditInlineRow() {

//    $("#BeforeSendContact tbody").on('click', 'td:not(:last-child)', function (e) {

//        var tablerow = $('#BeforeSendContact').DataTable();
//        var cellrow = tablerow.cell($(this)).index().row;
//        var cellcolumn = tablerow.cell($(this)).index().column;
//        if ($(this).find('input').length) {
//            return;
//        }
//        var input = $("<input type='text' style='width:100%;'/>").val($(this).text());
//        $(this).empty().append(input);
//        input.focus();
//        $(this).find('input')
//        .focus(function (e) {
//            if ($(this).val() == '' || $(this).val() == '') {
//                $(this).val('');
//            }
//        }).blur(function (e) {
//            if ($(this).val() != "") { //có giá trị nhập vào
//                //kiểm tra và update vào json obj,td
//                switch (cellcolumn) {
//                    case 0: //cột tên
//                        $(this).parent('td').text($(this).val());
//                        listobj[cellrow].ContactName = $(this).val();
//                        break;
//                    case 1: //cột email
//                        $(this).parent('td').text($(this).val());  //update text vào td của table
//                        if (!IsEmail($(this).val())) {  //nếu email không đúng
//                            custom_shownotification("error", "Email không đúng định dạng");
//                            listobj[cellrow].Success = false;
//                            tablerow.cell({ row: cellrow, column: 3 }).data("false").draw();
//                        }
//                        else { //email đúng
//                            listobj[cellrow].Email = $(this).val(); //update xuống list json
//                            if (getObjects(listobj, 'Email', $(this).val()).length >1)//nếu mail đã tồn tại
//                            {
//                                custom_shownotification("error", "Email đã có trong file excel");
//                                listobj[cellrow].Success = false;
//                                tablerow.cell({ row: cellrow, column: 3 }).data("false").draw();
//                            }
//                            if (IsMobile(listobj[cellrow].Mobile) && listobj[cellrow].ContactName != 'required' && getObjects(listobj, 'Email', listobj[cellrow].Email).length == 1)
//                            {
//                                listobj[cellrow].Success = true;
//                                //update tables
//                                tablerow.cell({ row: cellrow, column: 3 }).data("true").draw();
//                                listobj[cellrow].ErrorMessage = "";
//                                tablerow.cell({ row: cellrow, column: 4 }).data("").draw();
//                                //ContactToImport(listobj)
                               
//                            }
//                        }
//                        break;
//                    case 2:
//                        $(this).parent('td').text($(this).val()); //update td của table
//                        if (!IsMobile($(this).val())) { //số điện thoại sai
//                            custom_shownotification("error", "Mobile không đúng định dạng");
//                            listobj[cellrow].Success = false;
//                            ContactToImport(listobj)
//                        }
//                        else {
//                            listobj[cellrow].Mobile = $(this).val();
//                            if (IsEmail(listobj[cellrow].Email) && listobj[cellrow].ContactName != 'required' && getObjects(listobj, 'Email', listobj[cellrow].Email).length == 1) {
//                                listobj[cellrow].Success = true;
//                                //update tables
//                                tablerow.cell({ row: cellrow, column: 3 }).data("true").draw();
//                                listobj[cellrow].ErrorMessage = "";
//                                tablerow.cell({ row: cellrow, column: 4 }).data("").draw();
//                                //ContactToImport(listobj)
                              
//                            }
//                        }
                        
//                        break;
//                };
//            }
//            else {
//                $(this).parent('td').text("");
//            }
//        });
//    });

//}




//reset list khi thoát popup hay
function readexcel_Reset(id,input)
{
    $(id).on('click', function () {
         ContactList = [];
         countInfile = 0;
         var control = $(input);
        control.replaceWith(control = control.clone(true)); //reset input file
      

    });
}


function readexcel_DisabelSubmit(bool)
{
    $('#btn-action-import-contact').prop("disabled", bool);
}

//click để chọn file excel
function readexcel_DropClick(iddiv,idinput)
{
    $(iddiv).on('click', function () {
        $(idinput).val("");
        $(idinput).trigger('click');
        
    });
}

function readexcel_Knob()
{
    $(".knob").knob({
        'dynamicDraw': true,
        'thickness': 0.2,
        'tickColorizeValues': true,
        'skin': 'tron'
    });
}

//import lên server
//var itemsProcessed = 0;
var requestimport;
var HasImportError;
function ActionImport(apiDomain, access_token, ContactGroupId, CampaignId, gName, message) {
    
    $('#btn-action-import-contact').on('click', function () {
        HasImportError = 0;
        requestimport = 0;
        readexcel_DisabelSubmit(true);
        $('#divLoading').show();
        $('#knob-process-import').show();
        var size = 1000;
        var newList = [];
        var loop = ContactList.length / size;
        for (var i = 0; i < Math.ceil(loop) ; i++) {    //chia thành nhiều lần gửi

            newList = ContactList.slice(i * size, i * size + size);
            var data = {
                CampaignId:CampaignId,
                ContactGroupId: ContactGroupId,
                ContactList: newList,
                Note: $('#txtNoteImport').val(),
            };
            requestimport++;

            $.ajax({
                url: apiDomain + 'api/Contact/Import',
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                data: data,
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + access_token);
                },
                success: function (r) {
                    switch (parseInt(r.ErrorCode)) {
                        case 0:
                            if (r.Data != null) {
                                HasImportError += r.Data.length;
                            }
                            break;
                        default:
                            {
                                HasImportError += size;
                            }
                            break;

                    }
                    
                    requestimport--;
                    if (requestimport == 0) {
                        custom_shownotification("success", "Đã import xong");
                        $('#divLoading').hide();
                        
                        $('#modal-mapping-contact').modal('toggle');
                        $('#modal-info-after-import').modal('toggle');//show kết quả
                        $('#number-contact-sent').text(ContactList.length) //số liên hệ gửi lên server
                        $('#number-contact-import').text(ContactList.length - HasImportError) //số liên hệ import đc
                        $('#number-contact-remove').text(HasImportError);
                        //setTimeout(function () { window.location.href = '/Contacts/ContactList?gId=' + encodeURI(ContactGroupId + '&eId=' + CampaignId + '&gName=' + gName, "UTF-8") }, 500);
                        rows_selected = [];
                        ContactList = [];
                        countInfile = 0;
                        HasImportError = 0;

                    }
                },
                error: function (x, s, e) {
                    requestimport--;
                    HasImportError += size;
                    if (requestimport == 0) {
                        custom_shownotification("success", "Đã import xong");
                        $('#modal-mapping-contact').modal('toggle');
                        $('#modal-info-after-import').modal('toggle');//show kết quả
                        $('#number-contact-sent').text(ContactList.length) //số liên hệ gửi lên server
                        $('#number-contact-import').text(ContactList.length - HasImportError) //số liên hệ import đc
                        $('#number-contact-remove').text(HasImportError); // số liên hệ bị loại bỏ
                        $('#divLoading').hide();
                       // setTimeout(function () { window.location.href = '/Contacts/ContactList?gId='+encodeURI(ContactGroupId + '&eId=' + CampaignId + '&gName='+gName , "UTF-8") }, 500)
                        ContactList = [];
                        rows_selected = [];
                        countInfile = 0;
                        HasImportError = 0;


                    }
                }

            });


        }
    });
}

function readexcel_GoToContactList(ContactGroupId, CampaignId, gName, message)
{
    $('#go-to-contactlist').on('click', function () {
        window.location.href = '/Contacts/ContactList?gId=' + encodeURI(ContactGroupId + '&eId=' + CampaignId + '&gName=' + gName, "UTF-8")
    });
}


//thêm danh sách mobile vào list boostrap input tag
//function ActionImportMobile(apiDomain, access_token, inputId) {
//    $('#btnActionImportMobile').on('click', function () {
     
//        if (MobileList.length < 5000) {
//            var importmobile = 0;
//            $('#modalBeforeImportMobile').modal('toggle');
//            $('#divLoading').show();
//            setTimeout(function () {
//                $.each(MobileList, function (i, o) {
//                    $(inputId).tagsinput('add', o);
//                    if (i == MobileList.length - 1) {
//                        $('#divLoading').hide();
//                        MobileList = [];
//                    }
//                });
//            }, 1000);
//        }
//        else {
//            custom_shownotification("error", "Số lượng vượt quá 2000 số");
//        }

//    })
    

//};

var isgateway = false;
function readexcel_CheckSMSGateWay(apiDomain,access_token,campaignid,message)
{
    if (campaignid != null && campaignid != undefined && campaignid!="") {
       $.ajax({
            url: apiDomain + 'api/AutoCampaign/CheckCampaignHasSMSGateway?CampaignId=' + campaignid,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },

            success: function (r) {
                switch (parseInt(r.ErrorCode)) {
                    case 0:
                        isgateway = r.Data;
                        break;
                }

            },
            error: function (r) {

            }
        });
    }
}
