﻿function createtemplate_Action(apiDomain, access_token, TemplateId, message)
{
    createtemplate_Intialize();
    myteamplate_SysTemplateCube();
    mytemplate_LoadSysTemplate(apiDomain, access_token, message);
    mytemplate_FixShowCube();

    createtemplate_ShowModalCreate();

    createtemplate_Create(apiDomain, access_token, message);
}

function createtemplate_Intialize()
{
    var height = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.page-footer').outerHeight()-20;
    $('#portlet-create-email-content').css('height', height);
    $('#content-scroll-sys-template').slimScroll({
        height: height - 70,
        alwaysVisible: true
    })
}



function createtemplate_IframeBuilder(Id, Type) {
    $('#divLoading').show();
    var height = parseInt($(window).height());
    $('#iframe-emailbuilder').css('height', height);
    $('#iframe-emailbuilder').attr('src', 'http://builder.toomarketer.com:3001/editor.html?t=' + Type + '&id=' + Id);
    $('#iframe-emailbuilder').load(function () {
        $('#divLoading').hide();
    });
}

function createtemplate_IframeEditor(Id, Type) {
    $('#divLoading').show();
    var height = parseInt($(window).height());
    $('#iframe-emaileditor').css('height', height);
    $('#iframe-emaileditor').attr('src', '/EmailEditor?Type=' + Type + '&Id=' + Id);
    $('#iframe-emaileditor').load(function () {
        $('#divLoading').hide();
    });
}

var CreateLastId = null;
function mytemplate_LoadSysTemplate(apiDomain, access_token, message) {
    var Count = 20;
    $.ajax({
        url: apiDomain + 'api/template/GetSystemList?Count=' + Count + '&LastTemplateId=' + CreateLastId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    if (r.Data.length > 0) {
                        var htmlContent = '';
                        $.each(r.Data, function (i, o) {
                            if (i == r.Data.length - 1) {
                                CreateLastId = o.TemplateId;
                            }

                            htmlContent += '<div class="cbp-item" data-tid=' + o.TemplateId + '>' +
                                       '<div class="cbp-caption">' +
                                       '<div class="cbp-caption-defaultWrap">' +
                                            '<img src="' + o.SnapShotUrl + '" alt="">' +
                                       '</div>' +
                                      ' <div class="cbp-caption-activeWrap">' +
                                      ' <div class="cbp-l-caption-alignCenter">' +
                                       '<div class="cbp-l-caption-body">' +
                                      ' <a id="select-use-template" data-id="' + o.TemplateId + '" class="cbp-l-caption-buttonLeft btn green-jungle uppercase"><i class="fa fa-hand-pointer-o"></i>'+message.Select+'</a>' +
                                       '<a href="' + o.SnapShotUrl + '" class="cbp-lightbox cbp-l-caption-buttonRight btn blue" data-title="System template<br>' + o.TemplateName + '"><i class="fa fa-eye"></i>'+message.View+'</a>' +
                                      ' </div>' +
                                       '</div>' +
                                      ' </div>' +
                                      ' </div>' +
                                       '<div class="cbp-l-grid-projects-title uppercase text-center uppercase text-center">' + o.TemplateName + '</div>' +
                                     '  </div>';



                        });
                        $('#js-grid-sys-template').cubeportfolio('appendItems', htmlContent);
                        $('#js-grid-sys-template #loadMore-noMoreLoading').hide();
                        $('#js-loadMore-sys-template').show();
                        $('#warning-not-systemplate').hide();
                        if (r.Data.length < Count) {
                            $('#js-loadMore-sys-template').hide();
                        }
                    }
                    else {

                        if (LastSysTemId == null) {
                            $('#warning-not-systemplate').show();
                            $('#js-loadMore-sys-template .cbp-l-loadMore-link').hide();
                        }
                    }


                    $('#divLoading').hide();
                    break;
                default:
                    custom_shownotification('error', 'Lỗi lấy danh sách template hệ thống');
                    break;

            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', 'Lỗi lấy danh sách template hệ thống');
        }
    })
}




function myteamplate_SysTemplateCube() {

    //systemplate
    $('#js-grid-sys-template').cubeportfolio({
        filters: '#js-filters-juicy-projects',
        loadMore: '#js-loadMore-sys-template',
        loadMoreAction: 'click',
        layoutMode: 'grid',
        defaultFilter: '*',
        animationType: 'quicksand',
        gapHorizontal: 35,
        gapVertical: 30,
        gridAdjustment: 'responsive',
        mediaQueries: [{
            width: 1500,
            cols: 6
        }, {
            width: 1100,
            cols: 6
        }, {
            width: 800,
            cols: 5
        }, {
            width: 480,
            cols: 4
        }, {
            width: 320,
            cols: 2
        }],
        caption: 'overlayBottomReveal',
        displayType: 'sequentially',
        displayTypeSpeed: 80,

        // lightbox
        lightboxDelegate: '.cbp-lightbox',
        lightboxGallery: true,
        lightboxTitleSrc: 'data-title',
        lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

    });

}

function mytemplate_FixShowCube() {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(window).trigger('resize');
    });
}

var typetemplate;
function createtemplate_ShowModalCreate()
{
    $('.type-input-dragg a').on('click', function () {
        typetemplate = 0;
        $('#modal-create-template-htm').modal('toggle');
    });

    $('.type-input-text a').on('click', function () {
        typetemplate = 1;
        $('#modal-create-template-htm').modal('toggle');
    });

    $('#content-scroll-sys-template').on('click', '#select-use-template', function () {
        typetemplate = 2;
        var TemplateId = $(this).data('id');
        $('#confirm-create-template').data('id', TemplateId);
        $('#modal-create-template-htm').modal('toggle');
    });
}

function createtemplate_Create(apiDomain, access_token, message) {
    $('#confirm-create-template').on('click', function () {
        var TemplateSys = $(this).data('id');
        if (createtemplate_CheckTemplate(message)) {
            $('#divLoading').show();
            $('#modal-create-template-htm').modal('toggle');
            var data = {
                TemplateName: $('#template-name').val(),
                Subject: $('template-subject').val(),
            };
            $.ajax({
                url: apiDomain + 'api/template/Create',
                type: 'post',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + access_token);
                },
                success: function (r) {
                    switch (parseInt(r.ErrorCode)) {
                        case 0:
                            if (typetemplate == 0) {
                                var data = r.Data;
                                var TemplateId = data.TemplateId;
                                $('#modalLoadEmailBuilderContent').show();
                                var Type = 't'; //template;

                                createtemplate_IframeBuilder(TemplateId, Type);
                            }
                            else if (typetemplate == 1)
                            {
                                var data = r.Data;
                                var TemplateId = data.TemplateId;
                                $('#modalLoadEmailEditorContent').show();
                                var Type = 't'; //template;
                                createtemplate_IframeEditor(TemplateId, Type);
                            }

                            else if (typetemplate == 2) {
                                var data = r.Data;
                                var TemplateId = data.TemplateId;
                                createtemplate_CreateFromTemplate(apiDomain, access_token, TemplateSys, TemplateId, message);
                                
                            }
                            break;
                        default:
                            custom_shownotification('error', message.CreateTemplateError);
                            $('#divLoading').show();
                            break;
                    }
                },
                error: function (x, s, e) {
                    $('#divLoading').show();
                    custom_shownotification('error', x.statusText);
                }
            })
        }
    });
}

function createtemplate_CheckTemplate(message) {
    if ($('#template-name').val() == "" || $('#template-name').val() == null) {
        custom_shownotification('error', message.TemplateNameRequire);
        return false;
    }
    else return true;
}

function createtemplate_CreateFromTemplate(apiDomain, access_token, TemplateSys, TemplateSave, message)
{
    
    var apiUrl = 'api/template/GetInfo?TemplateId=' + +TemplateSys;
    $.ajax({
        url: apiDomain + apiUrl,
        type: 'GET',
        contentType: 'application/json; charset-utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    console.log('data: ' + JSON.stringify(data));
                    createtemplate_UpdateTemplate(apiDomain, access_token, data, TemplateSave, message)
                    break;
                default:
                    break;
            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', 'Có lỗi xảy ra');
        },

    });
}

function createtemplate_UpdateTemplate(apiDomain, access_token, data, TemplateSave, message)
{
    var para = {
        TemplateId: TemplateSave,
        HTML: data.HTML,
        JSONData: data.JSONData,
        SnapShotUrl:data.SnapShotUrl,
        IsMailBuilder: true,
    }
    $.ajax({
        url: apiDomain + 'api/template/UpdateContent',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(para),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            $('#divLoading').hide();
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    $('#modalLoadEmailBuilderContent').show();
                    var Type = 't'; //template;
                    createtemplate_IframeBuilder(TemplateSave, Type);
                    break;
                case 101:
                    custom_shownotification('error', 'Có lỗi xảy ra');
                default:
                    custom_shownotification('error', 'Có lỗi xảy ra');
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification('error', 'Có lỗi xảy ra');
        }
    });
}