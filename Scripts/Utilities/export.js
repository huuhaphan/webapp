﻿function export_Action(apiDomain,access_token,message)
{
    export_GetList(apiDomain,access_token,message)
}
function export_GetList(apiDomain, access_token, message) {
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/report/GetContactExportList',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);

        },
        success: function (r) {
            switch(parseInt(r.ErrorCode))
            {
                case 0:
                    var data = r.Data;
                    if (data.length > 0)
                    {
                        var html = "";
                        $.each(r.Data, function (i, o) {
                            html += '<tr>' +
                                           ' <td style="width:40%;"><i class="fa fa-file-excel-o font-green-jungle fa-2x"></i> ' + o.FileName + ' </td>' +
                                            '<td style="width:30%;"> ' + moment(moment.utc(o.FinishDate).toDate()).format('YYYY-MM-DD HH:mm') + '  </td>' +
                                            '<td style="width:30%;"> ' + moment(moment.utc(o.ExpireDate).toDate()).format('YYYY-MM-DD') + '  </td>' +
                                           ' <td style="width:20%;" class="text-center"> <a href="' + o.FilePath + '"><i class="fa fa-download font-green-jungle fa-2x"></i></a> </td>' +

                                       ' </tr>'
                        });
                        
                    }
                    else
                    {
                        html += '<tr class="text-center">' +
                                           ' <td colspan="4" >'+message.NoData+'</td>' +
                                           
                                       ' </tr>'

                    }
                    $('#tbody-queryexport').append(html);
                        var height = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.portlet-title').outerHeight() - $('.page-footer').outerHeight()-70;
                        $('.inner_table').css({
                            'height': height,
                        })
                    $('#divLoading').hide();
                    break;
                default:
                    $('#divLoading').hide();
                    custom_shownotification("error", message.GetListError);
                    break;
            }
        },error:function(x,s,e)
        {
            custom_shownotification("error", x.statusText);
            $('#divLoading').hide();
        }
    })
}