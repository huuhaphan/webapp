﻿function mytemplate_Action(apiDomain, access_token, message, permissiontable) {
    mytemplate_SetHeight();
    mytemplate_Library(apiDomain, access_token, message);
    mytemplate_LoadTemplate(apiDomain, access_token, message, permissiontable);
    mytemplate_LoadMore(apiDomain, access_token, message, permissiontable);
    mytemplate_OpenModalDeleteTemplate(apiDomain, access_token, message);
    mytemplate_DelelteTemplate(apiDomain, access_token, message, permissiontable);
    mytemplate_EditTemplate(apiDomain, access_token, message);
    mytemplate_AutoScroll(permissiontable);
  
}

function mytemplate_ReloadContent(apiDomain, access_token, message, permissiontable) {
    myteamplate_ReloadPage(apiDomain, access_token, message,permissiontable)
    
}

function mytemplate_SetHeight()
{
    var height=$(window).height()-$('.page-header').outerHeight()-$('.page-bar').outerHeight()-$('.portlet-title').outerHeight()-$('.page-footer').outerHeight();
    $('#content-mytemplate').css('height', height);
 
    $('#content-mytemplate').slimScroll({
        height: height-25,
        alwaysVisible: true,
        width:'100%'
    });
}
var IsScroll = true;
var LastMyTemplateId = null;
function mytemplate_LoadTemplate(apiDomain, access_token, message, permissiontable) {
    var Count = 20;
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/template/GetList?Publish=-1&Count=' + Count + '&LastTemplateId=' + LastMyTemplateId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {

            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var htmlContent = '';
                    if (r.Data.length > 0) {
                        $.each(r.Data, function (i, o) {
                          
                            if (i == r.Data.length - 1) {
                                LastMyTemplateId = o.TemplateId;
                            }
                            var SnapShotUrl = '/Content/Images/thumbnail-generator.png';
                            if (o.SnapShotUrl != null && o.SnapShotUrl != '') {
                                SnapShotUrl = o.SnapShotUrl;
                            }

                            var edit = '<a id="select-edit-template" data-id="' + o.TemplateId + '" class="cbp-l-caption-buttonLeft btn btn-sm green-jungle"><i class="fa fa-eye"></i>' + message.Edit + ' <a>';
                            var del = '<a id="select-delete-template"  data-id="' + o.TemplateId + '" class="cbp-l-caption-buttonRight btn btn-sm red"><i class="fa fa-remove"></i>' + message.Delete + ' <a>';
                            var tool = '';
                            if (permissiontable.IsParent) {
                                tool = del + edit;
                            } else {
                                if (parseInt(permissiontable.Template) == 2) {
                                    tool = del + edit;
                                }
                            }

                            htmlContent += '<div class="cbp-item" data-tid=' + o.TemplateId + '>' +
                                       '<div class="cbp-caption">' +
                                       '<div class="cbp-caption-defaultWrap">' +
                                            '<img id="img' + o.TemplateId + '" style="cursor: pointer;"  href="' + SnapShotUrl + '" class="cbp-lightbox" src="' + SnapShotUrl + '" alt="">' +

                                       '</div>' +
                                      ' <div class="cbp-caption-activeWrap">' +
                                      ' <div class="cbp-l-caption-alignCenter">' +
                                       '<div class="cbp-l-caption-body">' +
                                         tool+
                                     
                                        //'<a id="select-copy-template" data-id="' + o.TemplateId + '" class="cbp-l-caption-buttonLeft btn yellow-gold uppercase"><i class="fa fa-clone"></i> Sao chép <a>' +
                                //       '<a href="' + o.SnapShotUrl + '" class="cbp-lightbox cbp-l-caption-buttonRight btn blue uppercase" data-title="My template<br>' + o.TemplateName + '"><i class="fa fa-eye"></i>Xem</a>' +
                                      ' </div>' +
                                       '</div>' +
                                      ' </div>' +
                                      ' </div>' +
                                       '<div class="cbp-l-grid-projects-title text-center" title="'+ o.TemplateName +'">' + o.TemplateName + '</div>' +
                                     '  </div>';



                        });
                        
                       
                        $('#utilities-myteamplate-gird').cubeportfolio('appendItems', htmlContent);
                        $('#utilities-myteamplate-gird #loadMore-noMoreLoading').hide();
                        $('#utilities-myteamplate-loadmore').show();
                        $('#warning-not-mytemplate').hide();
                        if (r.Data.length < Count) {
                            IsScroll = false;
                            $('#utilities-myteamplate-loadmore').hide();
                        }
                   
                    }
                    else {
                        IsScroll = false;
                        if (LastMyTemplateId == null) {
                            $('#warning-not-mytemplate').show();
                            $('#utilities-myteamplate-loadmore .cbp-l-loadMore-link').hide();
                        }
                      
                    }
                    
                    $('#divLoading').hide();
                    break;
                default:
                    custom_shownotification('error', message.LoadTemplateError);
                    $('#divLoading').hide();
                    break;

            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', x.statusText);
        }
    })
}

function mytemplate_LoadMore(apiDomain, access_token, message, permissiontable) {
    $('#utilities-myteamplate-loadmore').on('click', function () {

        mytemplate_LoadTemplate(apiDomain, access_token, message, permissiontable)
    })

}

function mytemplate_OpenModalDeleteTemplate(apiDomain, access_token, message) {

    $('#utilities-myteamplate-gird').on('click', '#select-delete-template', function () {

        var TemplateId = $(this).attr('data-id');
        $('#confirm-delete-email-template').attr('data-id', TemplateId);
        $('#modal-warning-delete-template').modal('toggle');
    });
}

function mytemplate_DelelteTemplate(apiDomain, access_token, message, permissiontable) {
    $('#confirm-delete-email-template').on('click', function () {
        var TemplateId = $(this).attr('data-id');
        $('#divLoading').show();
        $('#modal-warning-delete-template').modal('toggle');

        $.ajax({
            url: apiDomain + 'api/template/Delete?TemplateId=' + TemplateId,
            type: 'post',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader('Authorization', 'Bearer ' + access_token);
            },
            success: function (r) {
                $('#divLoading').hide();
                switch (parseInt(r.ErrorCode)) {
                    case 0:

                        $('#img' + TemplateId).closest('.cbp-item').remove();
                        myteamplate_ReloadPage(apiDomain, access_token, message, permissiontable);
                        custom_shownotification('success', message.DeleteTemplatesuccess);
                        break;
                    default:
                        custom_shownotification('error', message.DeleteTemplateError);
                        break;

                }
            },
            error: function (x, s, e) {
                $('#divLoading').hide();
                custom_shownotification('error', x.statusText);
            }
        })
    });

}

function myteamplate_ReloadPage(apiDomain, access_token, message, permissiontable)
{
    $('#divLoading').show();
    mytemplate_DestroyLibrary();
    LastMyTemplateId = null;
    IsScroll = true;
    $.when(
        mytemplate_Library(apiDomain, access_token, message)
        ).then(function (data, textStatus, jqXHR) {
            setTimeout(function () {
               
                mytemplate_LoadTemplate(apiDomain, access_token, message, permissiontable);
            }, 2000);
            $('#divLoading').hide();
        });
}



function mytemplate_IframeBuilder(Id, Type) {
    $('#divLoading').show();
    var height = parseInt($(window).height());
    $('#iframe-emailbuilder').css('height', height);
    $('#iframe-emailbuilder').attr('src', 'http://builder.toomarketer.com:3001/editor.html?t=' + Type + '&id=' + Id);
    $('#iframe-emailbuilder').load(function () {
        $('#divLoading').hide();
    });
}

function mytemplate_IframeEditor(Id, Type) {
    $('#divLoading').show();
    var height = parseInt($(window).height());
    $('#iframe-emaileditor').css('height', height);
    $('#iframe-emaileditor').attr('src', '/EmailEditor?Type=' + Type + '&Id=' + Id);
    $('#iframe-emaileditor').load(function () {
        $('#divLoading').hide();
    });
}

function mytemplate_IframeEditor(Id, Type) {
    $('#divLoading').show();
    var height = parseInt($(window).height());
    $('#iframe-emaileditor').css('height', height);
    $('#iframe-emaileditor').attr('src', '/EmailEditor?Type=' + Type + '&Id=' + Id);
    $('#iframe-emaileditor').load(function () {
        $('#divLoading').hide();
    });
}

function mytemplate_EditTemplate(apiDomain, access_token, message) {

    $('#utilities-myteamplate-gird').on('click', '#select-edit-template', function () {
        //$('#divLoading').show();
        var Id = $(this).attr('data-id');
        mytemplate_Info(apiDomain, access_token, Id, message);
        
    })
}

function mytemplate_Info(apiDomain, access_token,Id, message) {

    $.ajax({
        url: apiDomain + 'api/template/GetInfo?TemplateId=' + +Id,
        type: 'GET',
        contentType: 'application/json; charset-utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    var Type = 't';
                    if (data.JSONData != '' && data.JSONData != null) {
                        $('#modalLoadEmailBuilderContent').show();
                        mytemplate_IframeBuilder(data.TemplateId, Type);
                    }
                    else {
                        $('#modalLoadEmailEditorContent').show();
                        mytemplate_IframeEditor(data.TemplateId, Type);
                    }

                    break;
                default:
                    break;
            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', 'Có lỗi xảy ra');
        },

    });
}

function mytemplate_DestroyLibrary() {

    $('#utilities-myteamplate-gird').cubeportfolio('destroy');
    $('#utilities-myteamplate-gird').children().remove();
}

function mytemplate_Library(apiDomain, access_token, message)
{
    // init cubeportfolio
    $('#utilities-myteamplate-gird').cubeportfolio({
        filters: '#utilities-myteamplate-filter',
        loadMore: '#utilities-myteamplate-loadmore',
        loadMoreAction: 'click',
        layoutMode: 'grid',
        defaultFilter: '*',
        animationType: 'quicksand',
        gapHorizontal: 35,
        gapVertical: 30,
        gridAdjustment: 'responsive',
        mediaQueries: [{
            width: 1500,
            cols: 7
        }, {
            width: 1100,
            cols: 6
        }, {
            width: 800,
            cols: 5
        }, {
            width: 480,
            cols: 4
        }, {
            width: 320,
            cols: 2
        }],
        caption: 'overlayBottomReveal',
        displayType: 'sequentially',
        displayTypeSpeed: 80,

        // lightbox
        lightboxDelegate: '.cbp-lightbox',
        lightboxGallery: true,
        lightboxTitleSrc: 'data-title',
        lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

    });
}


function mytemplate_AutoScroll(permissiontable) {
    $('#content-mytemplate').bind('slimscroll', function (e, pos) {
       
        if (pos == 'bottom') {
            if (IsScroll) {
               
                mytemplate_LoadTemplate(apiDomain, access_token, message, permissiontable);
            }

        }
    })
    ;

}

function mytemplate_FixShowCube() {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(window).trigger('resize');
    });
}
