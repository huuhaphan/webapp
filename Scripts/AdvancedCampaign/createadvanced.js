﻿/// <reference path="../Newsletters/partialsetting.js" />
/// <reference path="../Newsletters/partialcontent.js" />
//tạo event

function createadvanced_Action(apiDomain,apidBuilder, access_token, CampaignId, BaseId, IdAdd, IdAddSMS, message,Day)
{

    
    //createadvance_GetFormList(apiDomain, access_token, message).done(function () {
    //    createadvance_GetSpecialDays(apiDomain, access_token, message).done(function () {
    //        createadvance_LoadEvent(apiDomain, access_token, BaseId, message, Day);

    //    });
    //});

    createadvance_GetSpecialDays(apiDomain, access_token, message).done(function () {
            createadvance_LoadEvent(apiDomain, access_token, BaseId, message, Day);

    });


    //chọn  thời điểm gửi 
    createadvance_Select();

    createadvaced_ChangeEventDate();

    //chọn gửi email hay sms hay sms,email
    createadvance_SelectMethod();

    //mở modal tạo newsletter
    createadvance_OpenModalCreateEmailAdvance(apiDomain, access_token, message);

    //mở modal edit newsletter
    createadvance_OpenModalEditEmailAdvance(apiDomain,access_token,message);

    //đóng mdal tạo newslettter
    createadvance_CloseModalCreateEmailAdvance(apiDomain, access_token,CampaignId, message);

    //slide 
    createadvance_SlideTemplateSMS();


    //mở modal tạo sms
    createadvance_OpenModalCreateSMSAdvance(apiDomain, access_token, message);

    //mở modal tạo chọn template
    createadvance_OpenModalTemplate();

    //mở modal chọn template sms
    createadvance_OpenModalTemplateSMS();


    //chọn kiểu timbase, openbase;...
    createadvance_SelectBaseType(message);

    //save 
    createadvance_Save(apiDomain, access_token, CampaignId, BaseId, message);

    //chọn resendtype
    createadvance_ConfirmSavePublish(apiDomain, access_token, CampaignId, message);

    //confirm warning gateway
    createadvance_ConfirmWarningGateWay(apiDomain, access_token, CampaignId, message);

    //save và publish
    createadvance_SaveAndPublish(apiDomain, access_token, CampaignId, BaseId, message);

    //hủy
    createadvaced_Cancel(CampaignId, message);

    createadvaced_LoadNewsletter(apiDomain,access_token, CampaignId,message);
   
    //select another newsletter
    createadvance_SelectNewsletter(apiDomain, access_token, message);

    //tạo newsletter---------------------------------------------------------------------------------
    
    //style template
    emailcontent_TemplateCube();

    //style template
    emailcontent_SysTemplateCube();

    //fix show list template
    emailcontent_FixShowCube();

    //create newsleter by editor
    createadvance_LoadEditorNewsletter();

    //sự kiện chọn link
    createadvance_SelectLink();

    //save info setting newsletter 
    createadvanced_SavaSettingNewletter(apiDomain, access_token, CampaignId, message);

    //dùng template
    createadvance_UseTemplate(apiDomain,apidBuilder, access_token, CampaignId, message)

    emailcontent_CloseTemplate();


    emailcontent_LoadMyTemplate(apiDomain, access_token, message);

    emailcontent_LoadSysTemplate(apiDomain, access_token, message);

    //load more template
    emailcontent_LoadMore(apiDomain, access_token, message);

    // load more auto 
    partialcontent_AutoScroll(apiDomain, access_token, message);

    //open emailbuilder newsletter
    createadvance__OpenTemplate(apiDomain, apidBuilder, access_token, CampaignId);

    //mở modal email builder edit newsletter
    createadvance_EditByEmailBuilder(apiDomain,apidBuilder, access_token, message);

    //modal confirm change html newsletter
    createadvance_OpenModalConfirmTryHtml();

    //confirm html newletter
    createadvance_ChangeHtmlEmail(apiDomain, access_token);

    //modal confirm reset newletter
    createadvance_OpenModalResetNewsletter();

    //confirm reset newsletter
    createadvance_ResetGoToBuilder(apiDomain, access_token, message)

    //new autoresponder có newsletter thì load info
    //createadvanced_LoadNewsletter(apiDomain, access_token, message);

    //mở modal chọn email click
    createadvance_SelectEmailClick(apiDomain, access_token, CampaignId, message);

    //đóng modal chọn email click
    createadvance_CloseSelectEmailClick();

    //createadvance_SelectEmailOpen(apiDomain, access_token, CampaignId, message);

    //createadvance_CloseSelectEmailOpen();

    //chọn email trong sự kiên open
    //createadvance_SelectNewsletterOPen(apiDomain, access_token, message);

    //chọn email trong sự kiện click và open
    createadvance_SelectNewsletterClickOpen(apiDomain, access_token, message);

    //chọn cá nhân hóa
    createadvance_AddPersonalizeSMS(IdAddSMS, message);

    //load customefield date
    //createadvanced_LoadCustomFieldDate(apiDomain, access_token, message);
   
    

    //tạo sms -----------------------------------------------------------------------------------
    appshare_GetSMSServiceEnables(apiDomain, access_token, message);

    createadvance_LoadSMS(apiDomain, access_token, CampaignId, message);

    //close modal sms
    createadvance_CloseModalCreateSMSAdvance(apiDomain, access_token, CampaignId, message);

  

    createadvance__EditSMS(message);

    createadvance_ChangeTypeInput(message);

    createadvance_AddPersonalize(IdAdd, message)

    //list brand name
    createadvance_GetSMSBrandName(apiDomain, access_token);

    //list device
    createadvance_GetDeviceId(apiDomain, access_token, message);

    //hiện thị brand name khi chọn kiểu tin sms brand
    createadvance_ChangeShowBrandName();

    createadvance_SaveSMS(apiDomain, access_token,CampaignId, message);
    createadvance_SaveAndCloseSMS(apiDomain, access_token, CampaignId, message);
    createadvance_SelectSMS(apiDomain, access_token, message);

    //edit sms event
    createadvance_OpenModalEditSMSAdvance(apiDomain, access_token, message);
  
    //close all popup
    $(document).on('focus', ':not(.popovers)', function () {
        $('.popovers').popover('hide');
    });

}



// sự kiện chọn thời điểm  gửi
function createadvance_Select()
{
    createadvance_SetSelectClick();
    $('#select-type-time-click').on('change',function () {
        createadvance_SetSelectClick();
    });
}


function createadvance_SetSelectClick()
{
    if ($('#select-type-time-click').val() == 1) {
        $('#div-time-click').hide();
        $('#div-hours-click').hide();
        
        $('#div-time-click-type').hide();
        $('#div-time-click-date').hide();
        $('#div-time-click-span').hide();
        $('#timesendevent').css({ 'width': '500px' });
    }
    if ($('#select-type-time-click').val() == 2) {
        $('#div-time-click').show();
        $('#div-hours-click').hide();
        $('#div-time-click-type').hide();
        $('#div-time-click-date').hide();
        $('#div-time-click-span').hide();
        $('#timesendevent').css({ 'width': '520px' });
        //nếu là ngày đặt biệt thì mới cho chọn độ trễ theo ngày giờ trước đó sau đó
        if (EvenBaseTypes == 4)
        {
            $('#timesendevent').css({ 'width': '750px' });
            $('#div-time-click-type').show();
            $('#div-time-click-date').show();
            $('#div-time-click-span').show();
        }
        
    }
    if ($('#select-type-time-click').val() == 3) {
        $('#div-time-click').hide();
        $('#div-hours-click').show();
     
        $('#div-time-click-type').hide();
        $('#div-time-click-date').hide();
        $('#div-time-click-span').hide();
        $('#timesendevent').css({ 'width': '500px' });
    }
   
}


function createadvance_SelectMethod()
{

    createadvance_Method();
    $('#advance-method-sms').click(function (e) {
        
        createadvance_Method();
       

    });
    $('#advance-method-email').change(function (e) {
        createadvance_Method();
    })
}

function createadvance_Method()
{
    var email = $('#advance-method-email').prop('checked');
    var sms = $('#advance-method-sms').prop('checked');
    if (sms == true &&email==false) {
        
        $('.advance-sms').removeClass('hide');
        $('.advance-email').addClass('hide');
        //   $('#advance-method-sms').prop('disabled', true);
        $('.select-type-advance').css('display', '');
        $('.select-type-advance').css('width', '240');
    }

   
    if (email == true &&sms==true) {
        
        $('.advance-email').removeClass('hide');
        $('.advance-sms').removeClass('hide');
        //$('#advance-method-sms').prop('disabled', false);
        //$('#advance-method-email').prop('disabled', false);
        $('.select-type-advance').css('display', '');
        
        $('.select-type-advance').css('width', '480');

    }
    if (email == true && sms == false) {
        $('.select-type-advance').css('width', '240');
        $('.select-type-advance').css('border', 'none');
        $('.advance-email').removeClass('hide');
        $('.advance-sms').addClass('hide');
        $('.select-type-advance').css('display', '');
       
//        $('#advance-method-email').prop('disabled', true);

    }
    else if(email == false   && sms == false){
       // $('.select-type-advance').css('display', 'none');
        $('.advance-sms').addClass('hide');
        $('.advance-email').addClass('hide');
       
    }
    
}




function createadvance_OpenModalTemplate()
{
    

    $('#image-preview-newsletter-select').on('click', function () {
       
        $('#modalLoadTemplateAdvance').show();
        if ($('.slide-theme-email-advance').children().length > 0) {
            $('.slide-theme-email-advance').slick("setPosition", 0);
        }
    });

     $('#btCloseTemplatEemail').click(function () {
         $('#modalLoadTemplateAdvance').hide();
    });
    
}

function createadvance_OpenModalTemplateSMS() {

    $('.image-privew-sms').on('click', function () {

        $('#modalLoadTemplateSMSAdvance').show();
        if ($('.slide-theme-sms-advance').children().length > 0) {
            $('.slide-theme-sms-advance').slick("setPosition", 0);
        }
    });

    $('#btCloseTemplatSMS').click(function () {
        $('#modalLoadTemplateSMSAdvance').hide();
    });


  
}

function createadvance_LoadEditorNewsletter()
{
    $('.type-input-content.type-input-text , #editbyinput').on('click', function () {

        createadvance_IframEditor();
    });
}

function createadvance_IframEditor()
{
    $('#modalLoadEmailEditorEvent').show();
    var height = parseInt($(window).height() - $('#header-email-builder').outerHeight());
    $('#iframe-emaileditor-event').css('height', height);
    $('#iframe-emaileditor-event').attr('src', '/EmailEditor?Type=n&Id='+ EventNewsletterId);
}

function createadvance_OpenModalResetNewsletter() {
    $('#resetallcontent').on('click', function () {
        $('#modal-warning-before-reset-email').modal('toggle');
    })
}

function createadvance_ResetGoToBuilder(apiDomain, access_token, message) {
    $('#confirm-reset-emailcontent').on('click', function () {
        $('#modal-warning-before-reset-email').modal('toggle');
        var isave = 0; //click save 1 lần thôi

        if (isave == 0) {
            $('#divLoading').show();
            var data = {
                NewsletterId: EventNewsletterId,
                HTML: "",
                JSONData: "",
                IsEmailBuilder: false,
                SnapShotUrl: "",

            }
            $.ajax({
                url: apiDomain + 'api/newsletter/UpdateMailContent',
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                data: data,
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + access_token);
                },
                success: function (r) {
                    $('#divLoading').hide();
                    switch (parseInt(r.ErrorCode)) {
                        case 0:
                            isave++;
                            var IsCreate = false;
                            
                            createadvance_ShowInputType(apiDomain, access_token, IsCreate, message);
                            //set default tab is mailbuilder
                            $('#open-tab-email-builder').trigger('click');
                            break;
                        default:
                            custom_shownotification('error', message.HaveError);
                            break;
                    }
                },
                error: function (x, s, e) {
                    $('#divLoading').hide();
                    custom_shownotification('error', message.HaveError);
                }
            });
        }
    })
}

function createadvance__OpenTemplate(apiDomain, apidBuilder, access_token, CampaignId) {
    $('.type-input-content.type-input-dragg').on('click', function () {

        $('#modalLoadEmailBuilderEvent').show();
        var Type = 'n';
        createadvance_IframeBuilder(apidBuilder,EventNewsletterId, Type);
    })
}

function createadvance_SlideTemplateSMS() {
    $('.slide-theme-sms-advance').slick({
        dots: false,
        infinite: false,
        slidesToShow: 5,
        slidesToScroll: 5,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 1281,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5,
                    infinite: true,
                    dots: false
                }
            },
          {
              breakpoint: 1025,
              settings: {
                  slidesToShow: 4,
                  slidesToScroll: 4,
                  infinite: true,
                  dots: false
              }
          },
          {
              breakpoint: 600,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
              }
          },
          {
              breakpoint: 480,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
              }
          }
        ]
    });

}
function createadvance_SlideTemplate()
{
    $('.slide-theme-email-advance , .slide-email-advance-clickopen').slick({
        dots: false,
        infinite: false,
        slidesToShow: 5,
        slidesToScroll: 5,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 1281,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5,
                    infinite: true,
                    dots: false
                }
            },
          {
              breakpoint: 1025,
              settings: {
                  slidesToShow: 4,
                  slidesToScroll: 4,
                  infinite: true,
                  dots: false
              }
          },
          {
              breakpoint: 600,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
              }
          },
          {
              breakpoint: 480,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
              }
          }
        ]
    });

}

//Tham số chung
var EvenBaseTypes = 1 //ban đầu là TimeBase .Opened:2,  Clicked	3, SpecialDay 4
function createadvance_SelectBaseType(message)
{
    $('.tabbable-line.tab-select-autoresponder #time').on('click', function () {
        EvenBaseTypes = 1;
        createadvance_SelectTimeSend($('.imgbackg-basetime input').val(), EvenBaseTypes, 1);
        $('#enable-on-day').show();
    });
    $('.tabbable-line.tab-select-autoresponder #open').on('click', function () {
        EvenBaseTypes = 2;
        createadvance_SelectTimeSend($('.imgbackg-basetime input').val(), EvenBaseTypes,1);
        $('#title-email-click-open').text(message.Opens);
        $('#base-link-event').hide();
        $('#enable-on-day').show();

    });
    $('.tabbable-line.tab-select-autoresponder #click').on('click', function () {
        EvenBaseTypes = 3;
        createadvance_SelectTimeSend($('.imgbackg-basetime input').val(), EvenBaseTypes,1);
        $('#title-email-click-open').text(message.Clicks);
        if (EventBaseNewsletterId) {
            $('#base-link-event').show();
        }
        $('#enable-on-day').show();
    });

    

    $('.tabbable-line.tab-select-autoresponder #special').on('click', function () {
        EvenBaseTypes = 4;
        createadvance_SelectTimeSend($('.imgbackg-basetime input').val(), EvenBaseTypes, 1);
        $('#enable-on-day').show();
    });

    //$('.tabbable-line.tab-select-autoresponder #multiinput').on('click', function () {
    //    EvenBaseTypes = 5;
    //    createadvance_SelectTimeSend($('.imgbackg-basetime input').val(), EvenBaseTypes, 1);
    //    $('#enable-on-day').hide();
    //});
}


var EventNewsletterId;
var EventSMSMessageId;
var EventBaseNewsletterId;
var EventBaseLink;
function createadvance_Save(apiDomain,access_token,CampaignId,BaseId,message)
{
    $('#btn-next-create-advanced').on('click', function () {
        var ResendType = 2;
        if (createadvance_BeforeSave(message)) {
            if (BaseId) { //cập nhập
                var data = createadvance_GetDataSave(CampaignId, BaseId, EventIsPublish, ResendType);
                createadvance_UpdateEvent(apiDomain, access_token, data, CampaignId, message);
                
            }
            else { //tạo mới
                var data = createadvance_GetDataSave(CampaignId, BaseId, EventIsPublish, ResendType);
                createadvance_CreateEvent(apiDomain, access_token, data, CampaignId, message);
            }
        }
    });
}



function createadvance_SaveAndPublish(apiDomain, access_token, CampaignId, BaseId, message) {
    $('#btn-next-create-publish-advanced').on('click', function () {

        if (createadvance_BeforeSave(message)) {
            if (parseInt(EvenBaseTypes) == 1) {
                if (parseInt($('#advance-base-day').val())==0) {
                    $('#modal-warning-publish-event').modal('toggle');
                    $('#confirm-publish-event').data('uid', BaseId);
                    if (IsSMSGetaWay && $('#advance-method-sms').prop('checked') == true)
                    {
                        $('#warning-publish-gateway').show();
                    } else {
                        $('#warning-publish-gateway').hide();
                    }
                }
                else
                {
                    if (IsSMSGetaWay && $('#advance-method-sms').prop('checked') == true) {
                        $('#modelWarningSMSGateWay').modal('toggle');
                        $('#btn-waring-beforepublish').data('uid', BaseId);
                    }
                    else {
                        var ResendType = 2;
                        createadvance_PublishType(apiDomain, access_token, CampaignId, BaseId, message);
                    }
                }
            }
            else
            {
                if (IsSMSGetaWay && $('#advance-method-sms').prop('checked') == true) {
                    $('#modelWarningSMSGateWay').modal('toggle');
                    $('#btn-waring-beforepublish').data('uid', BaseId);
                } else {
                    var ResendType = 2;
                    createadvance_PublishType(apiDomain, access_token, CampaignId, BaseId, message);
                }
            }
            //if (BaseId) { //cập nhập
            //    var Publish = true;
            //    var data = createadvance_GetDataSave(CampaignId, BaseId, Publish);
            //    createadvance_UpdateEvent(apiDomain, access_token, data, CampaignId, message);

            //}
            //else { //tạo mới
            //    var Publish = true;
            //    var data = createadvance_GetDataSave(CampaignId, BaseId, Publish);
            //    createadvance_CreateEvent(apiDomain, access_token, data, CampaignId, message);
            //}
        }
    });
}

function createadvance_ConfirmWarningGateWay(apiDomain, access_token, CampaignId, message)
{
    $('#btn-waring-beforepublish').on('click', function () {
        $('#modelWarningSMSGateWay').modal('toggle');
        var ResendType = 2;
        var BaseId = $('#btn-waring-beforepublish').data('uid');
        createadvance_PublishType(apiDomain, access_token, CampaignId, BaseId, message);
    });
}

function createadvance_ConfirmSavePublish(apiDomain, access_token, CampaignId, message)
{
    $('#confirm-publish-event-unsent').on('click', function () {

        var BaseId = $('#confirm-publish-event').data('uid');
        var ResendType = 2;
        createadvance_PublishType(apiDomain, access_token, CampaignId, BaseId, message, ResendType)
    });

    $('#confirm-publish-event-all').on('click', function () {

        var BaseId = $('#confirm-publish-event').data('uid');
        var ResendType = 1;
        createadvance_PublishType(apiDomain, access_token, CampaignId, BaseId, message, ResendType)
    })

    $('#confirm-publish-event-none').on('click', function () {

        var BaseId = $('#confirm-publish-event').data('uid');
        var ResendType = 0;
        createadvance_PublishType(apiDomain, access_token, CampaignId, BaseId, message, ResendType)
    })
}

function createadvance_PublishType(apiDomain, access_token, CampaignId, BaseId, message, ResendType)
{
  
    if (BaseId) { //cập nhập
        var Publish = true;
        var data = createadvance_GetDataSave(CampaignId, BaseId, Publish, ResendType);
        createadvance_UpdateEvent(apiDomain, access_token, data, CampaignId, message);

    }
    else { //tạo mới
        var Publish = true;
        var data = createadvance_GetDataSave(CampaignId, BaseId, Publish, ResendType);
        createadvance_CreateEvent(apiDomain, access_token, data, CampaignId, message);
    }
}

function createadvance_CreateEvent(apiDomain, access_token, data, CampaignId, message) {
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/AutoCampaign/CreateEvent',
        type: 'Post',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {

            request.setRequestHeader("Authorization", "Bearer " + access_token);

        },
        success: function (r) {
      
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    custom_shownotification('success', message.CreateSuccess);
                    setTimeout(function () {
                        window.location.href = "/advancedcampaign/managecampaign?eId=" + CampaignId;
                    }, 500)
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
            $('#divLoading').hide();
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
            $('#divLoading').hide();
        }
    });
}

function createadvance_UpdateEvent(apiDomain, access_token, data, CampaignId, message) {
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/AutoCampaign/UpdateEvent',
        type: 'Post',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {

            request.setRequestHeader("Authorization", "Bearer " + access_token);

        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    custom_shownotification('success', message.UpdateSuccess);
                    setTimeout(function () {
                       window.location.href = "/advancedcampaign/managecampaign?eId=" + CampaignId;
                    }, 500)
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
            $('#divLoading').hide();
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
            $('#divLoading').hide();
        }
    });
}

function createadvance_GetDataSave(CampaignId, BaseId, Publish, ResendType)
{
    var AdvSendMethod;
    if ($('#advance-method-email').prop('checked') == true && $('#advance-method-sms').prop('checked') == true) {
        AdvSendMethod = 2;
    }
    if ($('#advance-method-email').prop('checked') == true && $('#advance-method-sms').prop('checked') == false) {
        AdvSendMethod = 0;
    }
    if ($('#advance-method-email').prop('checked') == false && $('#advance-method-sms').prop('checked') == true) {
        AdvSendMethod = 1;
    }

    
    var SendTimeTypes = $('#select-type-time-click').val();

    
    var Timer;
    switch (parseInt(SendTimeTypes)) {
        case 1:
            break;
        case 2:
            if (EvenBaseTypes == 4)
                if ($('#time-click-type').val() == 0) { //trước đó
                    Timer = -(parseInt($('#time-click-date').val()) * 24 + parseInt($('#time-click').val()));
                } else { //sau
                    Timer = parseInt($('#time-click-date').val()) * 24 + parseInt($('#time-click').val());
                }
                
            else
                Timer=parseInt($('#time-click').val());
            break;
        case 3:
            var timezone= new Date();
            var currentTimeZoneOffsetInHours = timezone.getTimezoneOffset() / 60 + parseInt($('#div-hours-click #hours-click').val());
            console.log(currentTimeZoneOffsetInHours);
            if (currentTimeZoneOffsetInHours < 0)
            {
                Timer = 24 + currentTimeZoneOffsetInHours;
            }
            else
            {
                Timer = currentTimeZoneOffsetInHours;
            }
            console.log(Timer);
            break;
    }

    var WeekEnable = 0;
    $.each($("input[name='enableweekdate']:checked"), function () {
        WeekEnable += parseInt($(this).val());

    });
    var BaseSpecialDay;
    var BaseCustomFieldId = 5//mặc định là 5 birthday, BaseSpecialDay=0 //không dùng nữa dùng BaseSpecialDay âm là customfiled dương là specialday
    if (EvenBaseTypes == 4) {
        BaseSpecialDay = $('#specialdate-click').val();
    }

    //var Form = $('#multiinput-click').val();

    var ResendWhenReregister = $('#advance-allow-rensend-register').prop('checked');
    var BaseDay = parseInt($('#advance-base-day').val());
    var ResponderName = $('#advance-autoresponder-name').val();
    var BaseLink = $('#select-link-newsletter option:selected').text();
    var SendTimeType = $('#select-type-time-click').val();

    if (BaseId) { //cập nhập
        var data = {
            BaseId: parseInt(BaseId),
            BaseType: EvenBaseTypes,
            BaseDay: BaseDay,
            ResponderName: ResponderName,
            SendMethod: AdvSendMethod,
            NewsletterId: EventNewsletterId,
            BaseNewsletterId: EventBaseNewsletterId,
            SMSMessageId: EventSMSMessageId,
            BaseLink: BaseLink,
            SendTimeType: SendTimeType,
            Timer: Timer,
            EnableWeekDay: WeekEnable,
            Publish: Publish,
           // BaseCustomFieldId: BaseCustomFieldId,
            BaseSpecialDay:BaseSpecialDay,
            ResendType: ResendType, //gửi toàn bộ đã có,
            ResendWhenReregister: ResendWhenReregister,
            
        };
    }
    else  //tạo mới
    {
        var data = {
            CampaignId: CampaignId,
            BaseType: EvenBaseTypes,
            BaseDay: BaseDay,
            ResponderName: ResponderName,
            SendMethod: AdvSendMethod,
            NewsletterId: EventNewsletterId,
            BaseNewsletterId: EventBaseNewsletterId,
            SMSMessageId: EventSMSMessageId,
            BaseLink: BaseLink,
            SendTimeType: SendTimeType,
            Timer: Timer,
            EnableWeekDay: WeekEnable,
           // BaseCustomFieldId: BaseCustomFieldId,
            BaseSpecialDay:BaseSpecialDay,
            Publish: Publish,
            ResendType: ResendType,
            ResendWhenReregister: ResendWhenReregister,
        };
    }
    return data;
}

function createadvance_BeforeSave(message) {
    var email=$('#advance-method-email').prop('checked');
    var sms=$('#advance-method-sms').prop('checked');
    EventBaseLink = $('#select-link-newsletter option:selected').text();
    var BaseSpecialDay = $('#specialdate-click').val();
    //var Form = $('#multiinput-click').val();
    if (email == true && sms == true) {
        if (!EventNewsletterId) {
            custom_shownotification('error', message.NoEmail);
            return false;
        }
        if (!EventSMSMessageId) {
            custom_shownotification('error', message.NoSMS);
            return false;
        }
    }
    if (email == true && sms == false) {
        if (!EventNewsletterId) {
            custom_shownotification('error', message.NoEmail);
            return false;
        }
    }
    if (email == false && sms == true) {
        if (!EventSMSMessageId) {
            custom_shownotification('error', message.NoSMS);
            return false;
        }
    }
    if (email == false && sms == false) {
        custom_shownotification('error', message.NoMethod);
            return false;
        
    }

    var WeekEnable = [];
    $.each($("input[name='enableweekdate']:checked"), function () {
        WeekEnable.push($(this).val());

    });
    if (WeekEnable.length == 0) {
        custom_shownotification('error', message.NoDayOfWeek);
        return false;
    }
    if (!$('#advance-autoresponder-name').val()) {
        custom_shownotification('error', message.NoAutoresponderName);
        return false;
    }

    switch (parseInt(EvenBaseTypes)) {
        case 1:
            if ($('#advance-base-day').val() == null || $('#advance-base-day').val() == '') {
                custom_shownotification('error', message.NoBaseDay);
                $('#advance-base-day').focus();
                return false;
            }
            if (isNaN($('#advance-base-day').val())) {
                custom_shownotification('error', message.BaseDayInteger);
                $('#advance-base-day').val('');
                $('#advance-base-day').focus();
                return false;
            }
            break;
        case 2:
            if (!EventBaseNewsletterId) {
                custom_shownotification('error', message.NoEmailOpen);
                return false;
            }
            break;
        case 3:
            if (!EventBaseLink) {
                custom_shownotification('error', message.NoEmailLink);
                return false;
            }
            if (!EventBaseNewsletterId) {
                custom_shownotification('error', message.NoEmailClick);
                return false;
            }
            break;
        
        case 4:
            {
                if (!BaseSpecialDay) {
                    custom_shownotification('error', message.NotSelectSpecialDay);
                    return false;
                }
            }
            break;
        //case 5:
        //    {
        //        if (!Form || Form == []) {
        //            custom_shownotification('error', message.NotSelectForm);
        //            return false;
        //        }
        //    }
        //    break;

    }
    return true;
};

function createadvaced_Cancel(CampaignId,message)
{
    $('#btn-back-create-advanced').on('click', function () {
        var r = confirm(message.ConfirmPrevious);
        if (r == true) {
            window.location.href = "/advancedcampaign/managecampaign?eId=" + CampaignId;
        } 
    })
}

function createadvaced_LoadNewsletter(apiDomain,access_token, CampaignId,message)
{
 

    $.ajax({
        url: apiDomain + 'api/newsletter/GetListActive?CampaignId=' + CampaignId,
        type: 'GET',
        //contentType: 'application/x-www-form-urlencoded',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    $('.slide-theme-email-advance').children().remove();
                    $('.slide-theme-email-advance').removeClass('slick-initialized').removeClass('slick-slider');
                    $('.slide-email-advance-clickopen').children().remove();
                    $('.slide-email-advance-clickopen').removeClass('slick-initialized').removeClass('slick-slider');
                    if (r.Data.length > 0) {
                        var html = "";
                        $.each(r.Data, function (i, o) {
                            var image = o.SnapShotUrl;
                            if (o.SnapShotUrl == "") {
                                image = '/Content/Images/thumbnail-generator.png';
                            }
                            var name = o.MessageName;
                            if (name.length > 20)
                            {
                                name = name.substring(0, 20) + '...';
                            }
                            html += '<div class="div-img-template-advance">' +
                                   '<div class="select-newsletter-in-slick">' +
                                   '<div class="template-newsletter-name" title="' + o.MessageName + '">' + name + '</div>' +
                                  ' <img class="img-template-advance " src="' + image + '" title="' + o.MessageName + '" />' +
                                  ' <span class="label" id="select-newsletter-event" data-uid="' + o.NewsletterId + '">' + message.ChooseEmail + '</span>' +
                                  '</div>'+
                                  '<div><a class="priview-newsletter-select" href="/View/Newsletter?nId='+o.NewsletterId+'" target="_blank" title="'+message.View+'"><i class="fa fa-eye font-white" style="font-size:18px;"></i></a></div>'+
                               '</div>';

                        });

                        $('.slide-theme-email-advance').append(html);

                        $('.slide-email-advance-clickopen').append(html);
                        createadvance_SlideTemplate();

                    }
                    else {
                        var html = '<div class="text-center font-lg font-white">' + message.NoEmailToSelect + '</div>';
                        $('.slide-theme-email-advance').append(html);
                        $('.slide-email-advance-clickopen').append(html);
                    }
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }

        },
        error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
        }

    });
}

function createadvance_SelectNewsletter(apiDomain, access_token, message)
{
    $('.slide-theme-email-advance').on('click', '#select-newsletter-event', function () {
        EventNewsletterId = $(this).data('uid');
      
        $('#modalLoadTemplateAdvance').hide();
        createadvance_UpdateSnapShotEvent(apiDomain, access_token, message);
    });
}

//function createadvance_SelectNewsletterOPen(apiDomain, access_token, message) {
//    $('.slide-theme-email-advance-open').on('click', '#select-newsletter-event', function () {
//        EventBaseNewsletterId = $(this).data('uid');

//        $('#modalLoadTemplateAdvanceOpen').hide();
//        createadvance_UpdateSnapShotBaseOpen(apiDomain, access_token, message);
//    });
//}

function createadvance_SelectNewsletterClickOpen(apiDomain, access_token, message) {
    $('.slide-email-advance-clickopen').on('click', '#select-newsletter-event', function () {
        EventBaseNewsletterId = $(this).data('uid'); //để truyền lên server
       
        $('#modalLoadTemplateAdvanceClickOpen').hide();
        createadvance_SnapShotBaseClickOpen(apiDomain, access_token, message);
    });
}

function createadvance_LoadEvent(apiDomain,access_token,BaseId,message,Day)
{
    if (BaseId) {
        $('#divLoading').show();
        $.ajax({
            url: apiDomain + 'api/AutoCampaign/GetEventInfo?BaseId=' + BaseId,
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader('Authorization', 'Bearer ' + access_token);
            },
            success: function (r) {
                switch(parseInt(r.ErrorCode))
                {
                    case 0:
                        var data = r.Data;
                        createadvance_LoadData(apiDomain, access_token, message, data);

                        break;
                    default:
                        custom_shownotification('error', message.HaveError);
                        break;
                }
                $('#divLoading').hide();
            },
            error: function (x, s, e) {
                custom_shownotification('error', message.HaveError);
                $('#divLoading').hide();
            }
        })
    }
    else
    {
        $('#advance-method-email').trigger('click'); //mặt định ban đầu thì check gửi email
        createadvance_SelectTimeSend(Day, 1, 1); //data 0, eventbase=1
        var name = message.Autoresponder + ' ' + moment().format('YYYY-MM-DD HH:mm');
        $('#advance-autoresponder-name').val(name); //set tên ban đầu mặc đinh, người dùng tự sửa
        $('#select-email-clickopen-event span').show();
        $('#image-preview-message-select span').show();
        $('#image-preview-newsletter-select span').show();

        $('#btn-next-create-publish-advanced').removeClass('hide'); //chưa có event thì cho save and publish
    }
}


var EventIsPublish = false;
function createadvance_LoadData(apiDomain, access_token, message,data)
{
    $('#advance-autoresponder-name').val(data.ResponderName);
    
    var timezone = new Date();
    var currentTimeZoneOffsetInHours = Math.abs(timezone.getTimezoneOffset() / 60) + parseInt(data.Timer);
    if (currentTimeZoneOffsetInHours >= 24)
    {
        var timer = currentTimeZoneOffsetInHours - 24;
    }
    else
    {
        var timer = currentTimeZoneOffsetInHours;
    }
    $('#hours-click').selectpicker('val', timer);

    //load select time
    //$('#select-type-time-click').selectpicker('val', data.SendTimeType);
    //createadvance_SetSelectClick();
    //$('#select-type-time-click').val(data.SendTimeType).change();
    

    EventNewsletterId = data.NewsletterId;
    EventBaseLink = data.BaseLink;
    EventSMSMessageId = data.SMSMessageId;
    EventBaseNewsletterId = data.BaseNewsletterId;
    createadvance_Select();

    if (data.SendMethod == 0)
    {
        //$('#advance-method-sms').prop('checked', false);
        //$('#advance-method-email').prop('checked', true);
        
        $('#advance-method-email').trigger('click');
    }
    if (data.SendMethod == 1)
    {
       
       // $('#advance-method-sms').prop('checked', true);
        // $('#advance-method-email').prop('checked', false);
        $('#advance-method-sms').trigger('click');
      
    }

    if (data.SendMethod == 2) {
       // $('#advance-method-email').prop('checked', true);
        // $('#advance-method-sms').prop('checked', true);
        $('#advance-method-email').trigger('click');
        $('#advance-method-sms').trigger('click');
    }

    //createadvance_Method();

    if (data.BaseType == 1) //time
    {
        $('.tabbable-line.tab-select-autoresponder #time').trigger('click');
    }
    if (data.BaseType == 2) //open
    {
        $('.tabbable-line.tab-select-autoresponder #open').trigger('click');

    }
    if (data.BaseType == 3) //click
    {
    
        $('.tabbable-line.tab-select-autoresponder #click').trigger('click');
        
    }
    if (data.BaseType == 4) //ngày đặc biệt
    {

        $('.tabbable-line.tab-select-autoresponder #special').trigger('click');

    }

    //if (data.BaseType == 5) {
    //    $('.tabbable-line.tab-select-autoresponder #multiinput').trigger('click');
    //}

    //cho phép gửi liên hệ nhập nhiều lần hay không 
    $('#advance-allow-rensend-register').prop('checked', data.ResendWhenReregister);
    
    //thời gian gửi
    createadvance_SelectTimeSend(data.BaseDay, data.BaseType, data.SendTimeType);

    if (EvenBaseTypes == 4) {
        $('#time-click').val((Math.abs(data.Timer) % 24));
        $('#time-click-date').val(Math.floor(Math.abs(data.Timer)/24));
    } else {
        $('#time-click').val(Math.abs(data.Timer));
    }

    $('#specialdate-click').selectpicker('val', data.BaseSpecialDay);

    //$('#multiinput-click').selectpicker('val', data.FormId); //che tạm chờ api

    var EnableWeekDay = custom_dec2bin(data.EnableWeekDay);
    $('.md-checkbox-inline input[name="enableweekdate"]').prop('checked', false);
    for (var i=EnableWeekDay.length-1; i>=0; i--)
    {
        console.log(Math.pow(2, EnableWeekDay.length - 1 - i));
        if(parseInt(EnableWeekDay[i])==1)
        {
            
            $('input[name="enableweekdate"][value="' + Math.pow(2, EnableWeekDay.length-1- i) + '"]').prop('checked', true);
        }
        else
        {
            $('input[name="enableweekdate"][value="' + Math.pow(2, EnableWeekDay.length-1-i) + '"]').prop('checked', false);
        }
    }

    if (!data.Publish) { //nếu có event mà chưa publish  thì hiện nút save and publish
        $('#btn-next-create-publish-advanced').removeClass('hide');
    }
    else {
        $('#btn-next-create-publish-advanced').addClass('hide');
    }

    EventIsPublish = data.Publish;
   

    createadvance_UpdateSnapShotEvent(apiDomain, access_token, message);

    createadvance_UpdateMessageEvent(apiDomain, access_token, message);

    createadvance_SnapShotBaseClickOpen(apiDomain, access_token, message);

}

function createadvance_SelectTimeSend(BaseDay, EventBaseType, SendTimeType) {
    //day 0 disable select time
   
    if (BaseDay == null) {
        BaseDay = 0;
    }
       
        
        if (parseInt(BaseDay) == 0 && parseInt(EventBaseType) == 1) {
            //ngày 0 mặt định sentimetype=1;
            SendTimeType = 1;
            $('#select-type-time-click').val(SendTimeType).change();
            $('#select-type-time-click').selectpicker('val', SendTimeType);
            //sự kiện change value thời điểm gửi
            //createadvance_SetSelectClick();
            if (SendTimeType == 1) {
                $('#select-type-time-click').prop('disabled', true);
                $('#select-type-time-click').selectpicker('refresh');
            }
            else {
                $('#select-type-time-click').prop('disabled', false);
                $('#select-type-time-click').selectpicker('refresh');
            }
            //show option resend
            $('#option-allowresend').show();
        }
        else {
            //sự kiện change value thời điểm gửi
            $('#select-type-time-click').val(SendTimeType).change();
            
            //createadvance_SetSelectClick();

            $('#select-type-time-click').prop('disabled', false);
            $('#select-type-time-click').selectpicker('refresh');

            //show option resend
            $('#option-allowresend').hide();
        }

        $('.imgbackg-basetime input').val(BaseDay);
        
}

function createadvaced_ChangeEventDate()
{
    $('#advance-base-day').change(function () {
        var BaseDay = $('.imgbackg-basetime input').val();
        var SentTimeType=$('#select-type-time-click').val();
        createadvance_SelectTimeSend(BaseDay, EvenBaseTypes, SentTimeType);
    })
}


function createadvance_UpdateSnapShotEvent(apiDomain, access_token, message)
{
    $('#state-loading-image').show();
    var NewsletterId = EventNewsletterId;
    var r = createadvance_NewLetterInfo(apiDomain, access_token, NewsletterId, message);
    switch(r.ErrorCode)
    {
        case 0:
            var data = r.Data;
            
            setTimeout(function () {
                $('#state-loading-image').hide();
                $('#image-preview-newsletter-select').css('background', 'url(' + data.SnapShotUrl + ')');
                $('#image-preview-newsletter-select').css('background-size', 'contain');
            }, 1000);

            if (EventNewsletterId) {
                $('#btn-edit-email-advance').show();
                //if (EvenBaseTypes == 3) {
                //    $('#btn-edit-email-advance').show();
                //}
                //else
                //{
                //    $('#btn-edit-email-advance').hide();
                //}
                $('#image-preview-newsletter-select span').text(message.AnotherMessage);
                $('#image-preview-newsletter-select').mouseenter(function () {
                    $('#image-preview-newsletter-select span').show();
                }).mouseleave(function () {
                    $('#image-preview-newsletter-select span').hide();
                });
            }
            else
            {
                $('#btn-edit-email-advance').hide();
            }
            break;
        default:
           
            setTimeout(function () {
                $('#state-loading-image').hide();
                $('#image-preview-newsletter-select').css('background', 'url(/Content/Images/thumbnail-generator.png');
                $('#image-preview-newsletter-select').css('background-size', 'contain');
            }, 1000)
            break;
    }
}

//function createadvance_UpdateSnapShotBaseOpen(apiDomain, access_token, message) {
//    $('#state-loading-image-open').show();
//    var r = createadvance_NewLetterInfo(apiDomain, access_token, message);
//    switch (r.ErrorCode) {
//        case 0:
//            var data = r.Data;

//            setTimeout(function () {
//                $('#state-loading-image-open').hide();
//                $('#select-email-open-event').css('background', 'url(' + data.SnapShotUrl + ')');
//                $('#select-email-open-event').css('background-size', 'contain');
//            }, 1000)
//            break;
//        default:

//            setTimeout(function () {
//                $('#state-loading-image-open').hide();
//                $('#select-email-open-event').css('background', 'url(/Content/Images/thumbnail-generator.png');
//                $('#select-email-open-event').css('background-size', 'contain');
//            }, 1000)
//            break;
//    }
//}

function createadvance_SnapShotBaseClickOpen(apiDomain, access_token, message) {
    $('#state-loading-image-clickopen').show();
    var NewsletterId = EventBaseNewsletterId;
    var r = createadvance_NewLetterInfo(apiDomain, access_token, NewsletterId, message);
    switch (r.ErrorCode) {
        case 0:
            var data = r.Data;

            setTimeout(function () {
                $('#state-loading-image-clickopen').hide();
                $('#select-email-clickopen-event').css('background', 'url(' + data.SnapShotUrl + ')');
                $('#select-email-clickopen-event').css('background-size', 'contain');
            }, 1000);
            if (EventBaseNewsletterId) {
                if (EvenBaseTypes == 1 || EvenBaseTypes == 2) {
                    $('#base-link-event').hide();
                }
                else {
                    $('#base-link-event').show();
                }
                $('#select-email-clickopen-event span').text(message.AnotherMessage);
                $('#select-email-clickopen-event').mouseenter(function () {
                    $('#select-email-clickopen-event span').show();
                }).mouseleave(function () {
                    $('#select-email-clickopen-event span').hide();
                });
            }


            //select lilnk

            var option = '';
            var links = data.Links;
            var BaseLinkId;
            $('#select-link-newsletter').selectpicker('destroy');
            if (links.length > 0) {
                option = '';
                $.each(links, function (i, o) {
                    if (o.LinkUrl != "") {
                        option += '<option value="' + o.LinkId + '">' + o.LinkUrl + '</option>';
                    }
                    if(o.LinkUrl==EventBaseLink)
                    {
                        BaseLinkId = o.LinkId;
                    }
                });
                if (option =='') {
                    option += '<option>' + message.NoLink + '</option>';
                }
            }
            else {
                var option = '<option>' + message.NoLink + '</option>';
                EventBaseLink = null;
            }
            $('<select class="form-control bs-select" id="select-link-newsletter" data-style="btn-btdefault" />').append(option).appendTo('#div-select-link-newsletter');

            $('#select-link-newsletter').selectpicker('render');
            if (EventBaseLink) {
                $('#select-link-newsletter').selectpicker('val', BaseLinkId);
            }
            break;
        default:

            setTimeout(function () {
                $('#state-loading-image-clickopen').hide();
                $('#select-email-clickopen-event').css('background', 'url(/Content/Images/thumbnail-generator.png');
                $('#select-email-clickopen-event').css('background-size', 'contain');
            }, 1000)
            break;
    }
}

function createadvance_SelectLink()
{
    $('#div-select-link-newsletter').on('changed.bs.select', '#select-link-newsletter', function (e, clickedIndex, newValue, oldValue) {
        var link = $('#select-link-newsletter option:selected').text();
        console.log('link: ' + link);
        EventBaseLink = link;
    })
}

function createadvance_UseTemplate(apiDomain,apidBuilder, access_token, CampaignId, message) {
    $('#js-grid-juicy-projects').on('click', '#select-use-template', function () {

        $('#divLoading').show();
        var TemplateId = $(this).attr('data-id');
        var data = emailcontent_GetTemplateInfo(apiDomain, access_token, TemplateId, message);
        if (parseInt(data.ErrorCode) == 0) {
            var obj = data.Data;
            if (obj.JSONData != null && obj.JSONData != '') {
                IsMailBuilder = true;

            }
            else IsMailBuilder = false;
            createadvance_UpdateContent(apiDomain,apidBuilder, access_token, CampaignId, data.Data, IsMailBuilder, message);
        }
        else {
            $('#divLoading').show();
            custom_shownotification('error', message.HaveError);
        }
    });

    $('#js-grid-sys-template').on('click', '#select-use-template', function () {

        $('#divLoading').show();
        var TemplateId = $(this).attr('data-id');
        var data = emailcontent_GetTemplateInfo(apiDomain, access_token, TemplateId, message);
        if (parseInt(data.ErrorCode) == 0) {

            var obj = data.Data;
            if (obj.JSONData != null && obj.JSONData != '') {
                IsMailBuilder = true;

            }
            else IsMailBuilder = false;
            createadvance_UpdateContent(apiDomain,apidBuilder, access_token, CampaignId, data.Data, IsMailBuilder, message);
        }
        else {
            $('#divLoading').show();
            custom_shownotification('error', message.HaveError);
        }
    })
}

function createadvance_UpdateContent(apiDomain,apidBuilder, access_token, CampaignId, datatemplate, IsMailBuilder, message) {
    var obj = datatemplate;

    var data = {
        NewsletterId: EventNewsletterId,
        HTML: obj.HTML,
        JSONData: obj.JSONData,
        SnapShotUrl: obj.SnapShotUrl,
        IsMailBuilder: true,
    }
    $.ajax({
        url: apiDomain + 'api/newsletter/UpdateMailContent',
        type: 'Post',
        contentType: 'application/json; chartset=utf-8',
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            $('#divLoading').hide();
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var Type = 'n';
                    if (IsMailBuilder)
                    {
                        $('#modalLoadEmailBuilderEvent').show();
                        createadvance_IframeBuilder(apidBuilder,EventNewsletterId, Type);
                    }
                    else
                    {
                        createadvance_IframEditor();
                    }
                   
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification('error', message.HaveError);
        }

    })
}

function createadvanced_SavaSettingNewletter(apiDomain, access_token, CampaignId, message) {
    $('#btn-save-email-advance-setting').on('click', function () {
        var frommail = $("#txtSettingEmailSend").select2('data')[0];
        var replymail = $("#txtSettingEmailReply").select2('data')[0];
     
        if (createadvaced_BeforeSaveNewsletter(message)) {
            var data = {
                CampaignId: CampaignId,
                NewsletterId:EventNewsletterId,
                MessageName: $('#txtSettingName').val(),
                Subject: $('#txtSettingSubject').val(),
                FromName: frommail.name,
                FromEmail: frommail.email,
                ReplyToEmail: replymail.text,
                EmbedMailUrl: false,
                ShowPermissionReminder: false,
                ShowUnsubscribeButton: true,
                Active: true,
            };
            if (IsCreateNewLetterId)

                createadvance_UpdateSettingLetter(apiDomain, access_token, CampaignId,data, message);
            else
                createadvance_SaveSetting(apiDomain, access_token, CampaignId, data, message);

           
        }
    });
}

function createadvance_SaveSetting(apiDomain, access_token, CampaignId,data, message)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/newsletter/Create',
        type: 'post',
        data: JSON.stringify(data),
        contentType: 'application/json;charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            $('#divLoading').hide();
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    EventNewsletterId = data.NewsletterId;
                    $('#tab-create-email-advance-template').unbind('click', createadvance_DisableLink);
                    $("#tab-create-email-advance-template").trigger("click");
                    break;

                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        }, error: function (x, s, r) {
            $('#divLoading').hide();
            custom_shownotification('error', message.HaveError);
        }
    });
}
function createadvance_UpdateSettingLetter(apiDomain, access_token, CampaignId, data, message)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/newsletter/UpdateInfo',
        type: 'post',
        data: JSON.stringify(data),
        contentType: 'application/json;charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            $('#divLoading').hide();
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    EventNewsletterId = data.NewsletterId;
                    $("#tab-create-email-advance-template").trigger("click");
                    break;

                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        }, error: function (x, s, r) {
            $('#divLoading').hide();
            custom_shownotification('error', message.HaveError);
        }
    });
}


function createadvaced_BeforeSaveNewsletter(message)
{
    if(!$('#txtSettingName').val())
    {
        custom_shownotification('error', message.EmailNameRequire);
        return false;
    }
    if( !$('#txtSettingSubject').val())
    {
        custom_shownotification('error', message.SubjectRequire);
        return false;
    }

    if (!$('#txtSettingEmailSend').val()) {
        custom_shownotification('error', message.EmailRequire);
        return false;
    }

    else {
        return true;
    }
}

function createadvance_ReLoadContentNewsletter(apiDomain, access_token, message) {
    var NewsletterId = EventNewsletterId;
    var html = createadvance_NewLetterInfo(apiDomain, access_token, NewsletterId, message);
    var height = 500;
    $('.img-loading-content').show();
    $('#div-img-snapshot-content-email').css('height', '400');
    switch (parseInt(html.ErrorCode)) {
        case 0:

            var data = html.Data;
            if (data.HTML != "" && data.HTML != "<br>") {
                $('#porlet-edit-email-content').show();
                $('#portlet-create-email-content').hide();
                $('#content-email-to-div').slimScroll({ destroy: true });
                $('#content-email-to-div').slimScroll({
                    alwaysVisible: true
                });
                setTimeout(function () {
                    $('#img-snapshot-content-email').attr('src', data.SnapShotUrl);

                    //snapshot select newsletter event
                    $('#image-preview-newsletter-select').css('background', 'url(' + data.SnapShotUrl + ')');
                    $('#image-preview-newsletter-select').css('background-size', 'contain');

                    $('.img-loading-content').hide();
                }, 3000);

                $('#img-snapshot-content-email').bind('error', function () {
                    $(this).attr("src", "/Content/Images/thumbnail-generator.png");
                });

            }
            else {
                $('#porlet-edit-email-content').hide();
                $('#portlet-create-email-content').show();
                $('#tab-select-type-edit-email').css('height', height - 50);
            }
           if ((data.IsMailBuilder === true || data.IsMailBuilder === 'true') && data.JSONData!="") {
                $('#editbydragg').css('display', 'block');
                $('#tryeditbyinput').css('display', 'block');
                $('#resetallcontent').css('display', 'block');
                $('#editbyinput').css('display', 'none');
            }
            else {
                $('#editbyinput').css('display', 'block');
                $('#resetallcontent').css('display', 'block');
                $('#editbydragg').css('display', 'none');
                $('#tryeditbyinput').css('display', 'none');


            }

           


            break;
        default:
            $('#porlet-edit-email-content').hide();
            $('#portlet-create-email-content').show();
            $('#tab-select-type-edit-email').css('height', height - 50);
            $('.img-loading-content').hide();
            break;
    }


}

function createadvance_ShowInputType(apiDomain, access_token,IsCreate, message) {
    var height = 500;
    $('#basicemailcontent').show();
    $('body .btn.imgecontentemail').css({ 'width': 'auto', 'height': height - 100 + 'px' });
    $('#basicemailcontent').children('.portlet-body').css('height', parseInt(height - 30));

    //mặt định tên khi tạo
    var newslettername = 'Newsletters ' + moment().format('YYYY-MM-DD HH:mm');
    $('#txtSettingName').val(newslettername);

    
    $('#content-scroll-template').slimScroll({
        height: height - 70,
        alwaysVisible: true
    });
    $('#content-scroll-sys-template').slimScroll({
        height: height - 70,
        alwaysVisible: true
    });


    

    if (!IsCreate) {
        var NewsletterId = EventNewsletterId;
        var html = createadvance_NewLetterInfo(apiDomain, access_token, NewsletterId, message);
       
        
    }
    else {
        var html = []; html.ErrorCode = 104;
        
    }
    //select email from
    createadvance_LoadFromEmail(apiDomain, access_token, html, IsCreate, message);

    switch (parseInt(html.ErrorCode)) {
        case 0:
            var data = html.Data;
            $('#txtSettingName').val(data.MessageName);
            $('#txtSettingSubject').val(data.Subject);
            $('#txtSettingSender').val(data.FromName);
            $('#txtSettingEmailSend').val(data.FromEmail);
            $('#txtSettingEmailReply').val(data.ReplyToEmail);
            if (data.HTML != "" && data.HTML != "<br>") {
                $('#img-snapshot-content-email').attr('src', data.SnapShotUrl);
                $('#img-snapshot-content-email').bind('error', function () {
                    $(this).attr("src", "/Content/Images/thumbnail-generator.png");
                });
                $('#porlet-edit-email-content').show();
                $('#portlet-create-email-content').hide();
                $('#content-email-to-div').slimScroll({ destroy: true });
                $('#content-email-to-div').slimScroll({
                    alwaysVisible: true
                });



            }
            else {
                $('#porlet-edit-email-content').hide();
                $('#portlet-create-email-content').show();
                $('#tab-select-type-edit-email').css('height', height - 50);
            }
            if ((data.IsMailBuilder === true || data.IsMailBuilder === 'true') && data.JSONData!="") {
                $('#editbydragg').css('display', 'block');
                $('#tryeditbyinput').css('display', 'block');
                $('#resetallcontent').css('display', 'block');
            }
            else {
                $('#editbyinput').css('display', 'block');
                $('#resetallcontent').css('display', 'block');


            }

            break;
        default:
            $('#porlet-edit-email-content').hide();
            $('#portlet-create-email-content').show();
            $('#tab-select-type-edit-email').css('height', height - 50);

            break;
    }

    $('#div-img-snapshot-content-email').css('height', '400');
    if(IsCreate)
        $('#tab-create-email-advance-template').bind('click', createadvance_DisableLink);
    else
        $('#tab-create-email-advance-template').unbind('click', createadvance_DisableLink);
}

function createadvance_DisableLink(e) {
    // cancels the event
    e.preventDefault();

    return false;
}

function createadvance_NewLetterInfo(apiDomain, access_token, NewsletterId, message) {
    if (NewsletterId) {
        var html = $.ajax({
            url: apiDomain + 'api/newsletter/GetInfo?NewsletterId=' + NewsletterId,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            async: false,

        }).responseJSON;

        return html;
    }
    else {
        var html = [];
        html.ErrorCode = 104;
        return html;
    }

}


function createadvance_EditByEmailBuilder(apiDomain, apidBuilder, access_token, message) {
    $('#editbydragg').on('click', function () {
        $('#modalLoadEmailBuilderEvent').show();
        var Type = 'n'; //newsletters;
        createadvance_IframeBuilder(apidBuilder,EventNewsletterId, Type);
    });
}

function createadvance_IframeBuilder(apidBuilder,Id, Type) {
    $('#divLoading').show();
    var height = parseInt($(window).height());
    $('#iframe-emailbuilder-event').css('height', height);
    $('#iframe-emailbuilder-event').attr('src', apidBuilder+ 'editor.html?t=' + Type + '&id=' + Id);
    $('#iframe-emailbuilder-event').load(function () {
        $('#divLoading').hide();
    });
}

function createadvance_OpenModalConfirmTryHtml() {
    $('#tryeditbyinput').on('click', function () {
        $('#modal-warning-before-change-html').modal('show');
    });
}

function createadvance_ChangeHtmlEmail(apiDomain, access_token) {
    $('#confirm-change-html-emailcontent').on('click', function () {
        $('#modal-warning-before-change-html').modal('toggle');
        createadvance_IframEditor();
    })
}

var IsCreateNewLetterId;
function createadvance_OpenModalCreateEmailAdvance(apiDomain, access_token, message) {
    $('#btn-create-email-advance').on('click', function () {
        var IsCreate = true;
        IsCreateNewLetterId = null;
        createadvance_ShowInputType(apiDomain, access_token, IsCreate, message);
        $('#modalEditContentEmailAdvance').modal('toggle');
    })
}

function createadvance_CloseModalCreateEmailAdvance(apiDomain, access_token, CampaignId, message) {
    $('#close-modal-create-newsletter').on('click', function () {
        createadvance_ResetContentModalEmail();
        $('#modalEditContentEmailAdvance').modal('toggle');
        createadvance_UpdateSnapShotEvent(apiDomain, access_token, message);
        createadvaced_LoadNewsletter(apiDomain, access_token, CampaignId, message);
    });
}


function createadvance_ResetContentModalEmail() {
    var height = $(window).height() - 100;
    $('#txtSettingName').val('');
    $('#txtSettingSubject').val('');
    $('#txtSettingSender').val('');
    $('#txtSettingEmailSend').val('');
    $('#txtSettingEmailReply').val('');
    $('#porlet-edit-email-content').hide();
    $('#portlet-create-email-content').show();
    $('#tab-select-type-edit-email').css('height', height - 50);
    $('#img-snapshot-content-email').attr('src', '');
    $('#editbydragg').css('display', 'none');
    $('#tryeditbyinput').css('display', 'none');
    $('#resetallcontent').css('display', 'none');
    $('#editbyinput').css('display', 'none');
    $("#tab-create-email-advance-setting").trigger("click");
}

function createadvance_OpenModalEditEmailAdvance(apiDomain, access_token, message) {
    $('#btn-edit-email-advance').on('click', function () {
        var IsCreate = false;
        IsCreateNewLetterId = EventNewsletterId;
      createadvance_ShowInputType(apiDomain, access_token, IsCreate, message);
        $('#modalEditContentEmailAdvance').modal('toggle');
    });
}


function createadvance_SelectEmailClick(apiDomain, access_token, CampaignId, message)
{
    $('#select-email-clickopen-event').on('click', function () {
        $('#modalLoadTemplateAdvanceClickOpen').show();
        if ($('.slide-email-advance-clickopen').children().length > 0) {
            $('.slide-email-advance-clickopen').slick("setPosition", 0);
        }
       
    });
}

function createadvance_CloseSelectEmailClick()
{
    $('#btCloseEemailClick').on('click', function () {
        $('#modalLoadTemplateAdvanceClickOpen').hide();
    });
}


//function createadvance_SelectEmailOpen(apiDomain, access_token, CampaignId, message) {
//    $('#select-email-open-event').on('click', function () {
//        $('#modalLoadTemplateAdvanceOpen').show();
//        $('.slide-theme-email-advance-open').slick("setPosition", 0);
//    });
//}

//function createadvance_CloseSelectEmailOpen() {
//    $('#btCloseEemailOpen').on('click', function () {
//        $('#modalLoadTemplateAdvanceOpen').hide();
//    });
//}


function createadvance_OpenModalCreateSMSAdvance(apiDomain, access_token, message) {
    $('#btn-create-sms-advance').on('click', function () {
        var IsCreate = true;
        var SMSSendId ='';
        createadvance_ShowMessageForEdit(apiDomain, access_token, IsCreate, SMSSendId, message)
        $('#modalEditContentSMSAdvance').modal('toggle');
    })
}

function createadvance_LoadSMS(apiDomain, access_token, CampaignId, message)
{
    $.ajax({
        url: apiDomain + 'api/smsmessage/GetListActive?CampaignId=' + CampaignId,
        type: 'GET',
        //contentType: 'application/x-www-form-urlencoded',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            $('.slide-theme-sms-advance').children().remove();
            $('.slide-theme-sms-advance').removeClass('slick-initialized').removeClass('slick-slider');
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    
                    if (r.Data.length > 0) {
                        var html = "";
                        $.each(r.Data, function (i, o) {
                            var image = o.SnapShotUrl;
                            if (o.SnapShotUrl == "") {
                                image = '/Content/Images/thumbnail-generator.png';
                            }
                            if (o.Message.length > 0 && o.Message.length > 220)
                            {
                                o.Message = o.Message.substring(0, 220) + ' ...';
                            }
                            var MessageName=o.MessageName;
                            if (o.MessageName.length > 0 && o.MessageName.length > 20) {
                                MessageName = o.MessageName.substring(0, 20) + ' ...';
                            }

                            html += '<div class="div-img-template-advance">' +
                                    '<div class="select-sms-in-slick">' +
                                     '<div class="template-newsletter-name" title="' + o.MessageName + '">' + MessageName + '</div>' +
                                  ' <img class="img-template-advance bg-white"/>' +
                                   '<div class="select-message-event-show">'+
                                    o.Message+'</div>' +
                                  //'<div class="img-template-advance bg-white">' + o.Message + '</div>'
                                  ' <span class="label" id="select-sms-event" data-uid="' + o.MessageId + '">'+message.ChooseSMS+'</span>' +

                               '</div>' +
                                 '<div><a class="priview-newsletter-select" href="/View/Message?mId=' + o.MessageId + '" target="_blank" title="'+message.View+'"><i class="fa fa-eye font-white" style="font-size:18px;"></i></a></div>' +
                               '</div>';
                        });

                        $('.slide-theme-sms-advance').append(html);
                        createadvance_SlideTemplateSMS();
                    }
                    else
                    {
                        var html = '<div class="text-center font-lg font-white">'+message.NoMessageToSelect+'</div>';
                        $('.slide-theme-sms-advance').append(html);
                    }
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }

        },
        error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
        }

    });
}

function createadvance_SelectSMS(apiDomain, access_token, message)
{

    $('.slide-theme-sms-advance').on('click', '#select-sms-event', function () {
        EventSMSMessageId = $(this).data('uid');

        $('#modalLoadTemplateSMSAdvance').hide();
        createadvance_UpdateMessageEvent(apiDomain, access_token, message);
    });
}

var IsSMSGetaWay = false; // có gửi sms getaway không
function createadvance_UpdateMessageEvent(apiDomain, access_token, message)
{
    $('#state-loading-message').show();
    var r = createadvance_MesageInfo(apiDomain, access_token, message);
    switch (r.ErrorCode) {
        case 0:
            var data = r.Data;
            if (data.SMSTypeId == 3) {
                IsSMSGetaWay = true;
            } else {
                IsSMSGetaWay = false;
            }
            setTimeout(function () {
                $('#state-loading-message').hide();
                $('#image-preview-message-select').children('.select-message-event').remove();
                $('#image-preview-message-select').css('background', 'url()');
                if (data.Message.length > 0 && data.Message.length > 330) {
                    data.Message = data.Message.substring(0, 300) + ' ...';
                }
                var html = '<div class="select-message-event">' + data.Message + '</div>'
                $('#image-preview-message-select').append(html);
            }, 1000);
            
          
            if (EventSMSMessageId) {
                $('#btn-edit-sms-advance').show();
                $('#image-preview-message-select span').text(message.AnotherMessage);
                $('#image-preview-message-select').mouseenter(function () {
                    $('#image-preview-message-select span').show();
                }).mouseleave(function () {
                    $('#image-preview-message-select span').hide();
                });
                $('#btn-edit-sms-advance').show();
            }
            else
            {
                $('#image-preview-message-select span').show();
            }

           
            break;
        default:
            $('#image-preview-message-select span').show();
            $('#image-preview-message-select').mouseenter(function () {
                $('#image-preview-message-select span').show();
            }).mouseleave(function () {
                $('#image-preview-message-select span').hide();
            });
            setTimeout(function () {
                $('#state-loading-message').hide();
                $('#image-preview-message-select').children('.select-message-event').remove();
                $('#image-preview-message-select').css('background', '/Content/Images/thumnail-sms.png');
                $('#image-preview-message-select').css('background-size', 'contain');

            }, 1000)
            break;
    }
}

function createadvance_MesageInfo(apiDomain, access_token, message)
{
    if (EventSMSMessageId) {
        var sms = $.ajax({
            url: apiDomain + 'api/smsmessage/GetInfo?SMSMessageId=' + EventSMSMessageId,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            async: false,

        }).responseJSON;

        return sms;
    }
    else {
        var sms = [];
        sms.ErrorCode = 104;
        return sms;
    }
}

function createadvance_CloseModalCreateSMSAdvance(apiDomain, access_token, CampaignId, message) {
    $('#close-modal-create-sms').on('click', function () {
        $('#modalEditContentSMSAdvance').modal('toggle');
    });
}

function createadvance_LoadUpdateMesssage(apiDomain, access_token, CampaignId, message) {
    $('#modalEditContentSMSAdvance').modal('toggle');
    createadvance_UpdateMessageEvent(apiDomain, access_token, message);
    createadvance_LoadSMS(apiDomain, access_token, CampaignId, message);
}

var MsgCount_event;
var AllowMsgCount = true;
function createadvance_UpdateMessageCount(message) { 
    var $this = $("#Message-event");
    var smsinputtype = parseInt($('input[name=smsinputtype]:checked').val());
    charLength = $this.val().length + appshare_SMSSymbolLength($this.val())
    //$.each(LISTPERSOLINATE, function (i, o) {
    //    if ($this.val().indexOf(o) != -1) {
    //        var re = new RegExp(o, 'g');
    //        var number = $this.val().match(re).length;
    //        charLength = charLength - number * o.length + number * LENGTH_PERSOLINATE_SMS;
    //    }
    //});

    if (parseInt($("input:radio[name='smstype-event']:checked").val()) == 1) {
        MsgCount_event = appshare_OTPNumber(charLength);
        if (charLength > LENGTHMAXOTP) {
            custom_shownotification('error', 'Tin nhắn tối đa 422 kí tự');
            AllowMsgCount = false;
        } else {
            AllowMsgCount = true;

        }
        var html = appshare_MsgLenghtCount(1, message);
        $("#MsgLengthCount-event").html("<b style='color:#8EBC00'>" + charLength + "</b> " + message.Character + " = <b style='color:#E84444'>" + MsgCount_event + "</b> " + message.Message + html);

    }
    else if (parseInt($("input:radio[name='smstype-event']:checked").val()) == 2) {
        MsgCount_event = appshare_BrandNumber(charLength);
        if (charLength > LENGTHMAXBRAND) {
            custom_shownotification('error', 'Tin nhắn tối đa 611 kí tự');
            AllowMsgCount = false;
        } else {
            AllowMsgCount = true;

        }
        var html = appshare_MsgLenghtCount(2, message);
        $("#MsgLengthCount-event").html("<b style='color:#8EBC00'>" + charLength + "</b> " + message.Character + " = <b style='color:#E84444'>" + MsgCount_event + "</b> " + message.Message + html);

    }
    else {
        var html = appshare_MsgLenghtCount(3, message);
        
            //MsgCount_event = appshare_GateWayNumberAccented(charLength);
            //$("#MsgLengthCount-event").html("<b style='color:#8EBC00'>" + charLength + "</b> " + message.Character + " = <b style='color:#E84444'>" + MsgCount_event + "</b> " + message.Message + html);
            var smsinputtype = parseInt($("input:radio[name='smsinputtype']:checked").val());
            var obj = appshare_GateWayNumberAccented(smsinputtype, $this.val());
            MsgCount_event = obj.messages;
            $("#MsgLengthCount-event").html("<b style='color:#8EBC00'>" + charLength + " / " + obj.per_message + "</b> " + message.Character + " = <b style='color:#E84444'>" + MsgCount_event + "</b> " + message.Message + html);

    }


    $('.popovers').popover();
};


function createadvance__EditSMS(message) {
    $("#Message-event").bind("keypress, keyup", function (e) {
        createadvance_ChangeAccent();
        createadvance_UpdateMessageCount(message);
    });
}

function createadvance_ChangeTypeInput(message) {
    $("input:radio[name='smsinputtype']").change(function () {
        createadvance_ChangeAccent();
        createadvance_UpdateMessageCount(message);
    });
}

function createadvance_ChangeAccent()
{
    var value = $("#Message-event").val();
    $("#Message-event").empty();
    var smsinputtype = parseInt($('input[name=smsinputtype]:checked').val());
    if (smsinputtype == 1) {
        $("#Message-event").val(custom_bodauTiengViet(value))
    }
    else {
        $("#Message-event").val(value);
    }
}



function createadvance_AddPersonalize(IdAdd, message) {
    $('#div-personalize-subject').on('click', '.data-personalize', function () {
        var personalize = $(this).text();
        custom_InsertTextAtCaret(IdAdd, personalize);
    });

}

function createadvance_AddPersonalizeSMS(IdAddSMS, message) {
    $('#div-personalize-subject-sms').on('click', '.data-personalize', function () {
        var personalize = $(this).text();
        custom_InsertTextAtCaret(IdAddSMS, personalize);

        //nếu là sms thì update sms
        createadvance_UpdateMessageCount(message);
    });

}


function createadvance_GetDeviceId(apiDomain, access_token, message) {
    var data = appshare_GetGatewayConfigList(apiDomain, access_token, message);
    if (data.Data.length > 0) {
        $('#select-devicenumber').removeClass('hide');
        var html = '';
        var obj = data.Data;
        console.log(obj);
        $.each(obj, function (i, o) {
            html += '<option value="' + o.ItemId + '" data-default="' + o.DefaultDeviceToken.ItemId + '" data-mobile="' + o.MobifoneDeviceToken.ItemId + '" data-vina="' + o.VinaphoneDeviceToken.ItemId + '"  data-viettel="' + o.ViettelDeviceToken.ItemId + '">' + o.ConfigName + '</option>';
        });

        $('#select-gateway-event').append(html);
       
    } else {
        var html = '<option  value="-1">' + message.NoConfig + '</option>';
        $('#select-gateway-event').append(html);
        //$('#select-gateway-type-event').hide();
    }
}

function createadvance_GetSMSBrandName(apiDomain, access_token) {
    $.ajax({
        url: apiDomain + 'api/User/GetSMSBrandNames',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            var code = parseInt(r.ErrorCode);
            switch (code) {
                case 0:
                    if (r.Data.length > 0) {
                        var html = "";
                        $.each(r.Data, function (i, o) {
                            html += '<option value=' + o.BrandNameId + ' data-typeid="' + o.BrandType+ '">' + o.BrandName + '</option>';
                        });
                        $('#select-brandname-event').append(html);
                    }
                    else {
                        var html = '<option value="-1">' + message.NoBrandName + '</option>'
                        $('#select-brandname-event').append(html);
                        //$('#select-brandname-type-event').addClass('hide');
                    }
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;

            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
        }

    })
}

function createadvance_ChangeShowBrandName() {
    createadvance_ShowBrandName();
    $("input:radio[name='smstype-event']").change(function () {
        createadvance_ShowBrandName();
        createadvance_UpdateMessageCount(message);
    })
}

function  createadvance_ShowBrandName ()
{
    $('#Performance-sendsms').text('40-70%');
    $('#divsmsservice').show();
    if (parseInt($("input:radio[name='smstype-event']:checked").val()) == 2) {
        $('#Performance-sendsms').text('85-95%');
        $('#select-sms-input-codau').hide();
        $('input#smskhongdau').trigger('click');
        $('#divsmsservice').show();
        $('#sms-brand-name-event').removeClass('hide');
        if (!$('#sms-gateway-event').hasClass('hide')) {
            $('#sms-gateway-event').addClass('hide');

        }
        $('#note-send-webapp').show();
        $('#note-send-getway').hide();
    }
    if (parseInt($("input:radio[name='smstype-event']:checked").val()) == 1) {
        $('#Performance-sendsms').text('40-70%');
        $('#select-sms-input-codau').hide();
        $('input#smskhongdau').trigger('click');
        $('#divsmsservice').show();
        if (!$('#sms-brand-name-event').hasClass('hide')) {
            $('#sms-brand-name-event').addClass('hide');
        }
        if (!$('#sms-gateway-event').hasClass('hide')) {
            $('#sms-gateway-event').addClass('hide');

        }
        $('#note-send-webapp').show();
        $('#note-send-getway').hide();
    }
    if (parseInt($("input:radio[name='smstype-event']:checked").val()) == 3) {

        $('#select-sms-input-codau').show();
        $('#divsmsservice').hide();
        if (!$('#sms-brand-name-event').hasClass('hide')) {
            $('#sms-brand-name-event').addClass('hide');

        }
        $('#sms-gateway-event').removeClass('hide');
        $('#note-send-webapp').hide();
        $('#note-send-getway').show();
    }
}

var SMSSendId;
function createadvance_OpenModalEditSMSAdvance(apiDomain, access_token, message) {
    $('#btn-edit-sms-advance').on('click', function () {
        var IsCreate = false;
        SMSSendId = EventSMSMessageId;
        createadvance_ShowMessageForEdit(apiDomain, access_token, IsCreate, SMSSendId, message)
        $('#modalEditContentSMSAdvance').modal('toggle');
    });
}


function createadvance_ShowMessageForEdit(apiDomain, access_token, IsCreate, SMSSendId, message)
{
    if(!IsCreate)
    {
        var sms = createadvance_MesageInfo(apiDomain, access_token, message);
        switch(sms.ErrorCode)
        {
            case 0:
                var data = sms.Data;
                $('#mesage-event-name').val(data.MessageName);
                $('#Message-event').val(data.Message);
                $('input[name="smstype-event"][value="' + data.SMSTypeId + '"]').trigger('click');
                if (data.SMSTypeId==2)
                {
                    $('#select-brandname-event').val(data.BrandNameId);
                }
                if (data.SMSTypeId == 3) {
                  
                    $('#select-gateway-event').val(data.GatewayConfigId);
                    if (custom_matchTiengViet(data.Message) != null) { //có dấu
                        $('input#smscodau').trigger('click');
                    }
                }
                break;
        }
    }
    else
    {
        var messagedefault="Message " +moment().format('YYYY-MM-DD HH:mm:ss')
        $('#mesage-event-name').val(messagedefault);
        $('#Message-event').val('');
        $('input[name="smstype-event"][value="1"]').trigger('click');
    }

    createadvance_UpdateMessageCount(message);
}

function createadvance_SaveSMS(apiDomain,access_token,CampaignId , mesage)
{
    $('#save-sms-event').on('click', function () {
      
            if (createadvance_CheckMessage(mesage)) {
                var data = createadvance_DataSMS(CampaignId);
                var IsClose = false;
                if (!SMSSendId)
                    createadvance_CreateSMS(apiDomain, access_token,CampaignId, data, IsClose, message);
                else
                    createadvance_UpdateSMS(apiDomain, access_token,CampaignId, data, IsClose, message);

            }
    });
}

function createadvance_SaveAndCloseSMS(apiDomain, access_token, CampaignId, mesage) {
    $('#save-close-sms-event').on('click', function () {
        if (createadvance_CheckMessage(mesage)) {
            var data = createadvance_DataSMS(CampaignId);
            var IsClose = true;
            if (!SMSSendId)
                createadvance_CreateSMS(apiDomain, access_token, CampaignId, data, IsClose, message);
            else
                createadvance_UpdateSMS(apiDomain, access_token, CampaignId, data, IsClose, message);

        }
    });
}

function createadvance_CreateSMS(apiDomain, access_toke, CampaignId, data, IsClose, message)
{
    
    $.ajax({
        url: apiDomain + 'api/smsmessage/Create',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify( data),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    EventSMSMessageId = r.Data.MessageId;
                    custom_shownotification('success', message.UpdateSuccess);
                    if (IsClose) {
                        //$('#close-modal-create-sms').trigger('click');
                        createadvance_LoadUpdateMesssage(apiDomain, access_token, CampaignId, message)
                    }

                    break;
                
                default:
                    $('#divLoading').hide();
                    custom_shownotification('error', message.HaveError);
                    break;
            }


        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification('error', message.HaveError);
        }
    });
}

function createadvance_UpdateSMS(apiDomain, access_token,CampaignId, data, IsClose, message) {

    $.ajax({
        url: apiDomain + 'api/smsmessage/UpdateInfo',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    EventSMSMessageId = r.Data.MessageId;
                    custom_shownotification('success', message.UpdateSuccess);
                    if (IsClose) {
                        //$('#close-modal-create-sms').trigger('click');
                        createadvance_LoadUpdateMesssage(apiDomain, access_token, CampaignId, message)
                    }
                    break;
                
                default:
                    $('#divLoading').hide();
                    custom_shownotification('error', message.HaveError);
                    break;
            }


        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification('error', message.HaveError);
        }
    });
}

function createadvance_DataSMS(CampaignId) {
    var TypeId = parseInt($("input:radio[name='smstype-event']:checked").val());
    var datacompo = {
        MessageName: $('#mesage-event-name').val(),
        Message: $('#Message-event').val(),
        SMSTypeId: parseInt($("input:radio[name='smstype-event']:checked").val()),
        BrandName: $('#select-brandname-event option:selected').text(),
        BrandNameId: parseInt($('#select-brandname-event option:selected').val()),
        BrandType: parseInt($('#select-brandname-event option:selected').data('typeid')),
        DeviceTokenItemId: parseInt($('#select-gateway-event option:selected').attr('data-default')),
        DeviceTokenItemIdMobifone: parseInt($('#select-gateway-event option:selected').attr('data-mobile')),
        DeviceTokenItemIdVinaphone: parseInt($('#select-gateway-event option:selected').attr('data-vina')),
        DeviceTokenItemIdViettel: parseInt($('#select-gateway-event option:selected').attr('data-viettel')),
        GatewayConfigId: parseInt($('#select-gateway-event option:selected').val()),
    }
    if (TypeId == 1) {
        if (!SMSSendId) {
            var data = {
                CampaignId: CampaignId,
                MessageName: datacompo.MessageName,
                Message:datacompo.Message,
                SMSTypeId: datacompo.SMSTypeId,
                MessageLength: MsgCount_event,
                Active: true,

            };
        }
        else {
            var data = {
                SMSMessageId: SMSSendId,
                MessageName: datacompo.MessageName,
                Message: datacompo.Message,
                MessageLength: MsgCount_event,
                SMSTypeId: datacompo.SMSTypeId,
                Active: true,
            };
        }
    }
    else if (TypeId == 2) {
        if (!SMSSendId) {
            var data = {
                CampaignId: CampaignId,
                MessageName: datacompo.MessageName,
                Message: datacompo.Message,
                MessageLength: MsgCount_event,
                SMSTypeId: datacompo.SMSTypeId,
                BrandName: datacompo.BrandName,
                BrandNameId: datacompo.BrandNameId,
                BrandType: datacompo.BrandType,
                Active: true,
            };
        }
        else {
            var data = {
                SMSMessageId: SMSSendId,
                MessageName: datacompo.MessageName,
                Message: datacompo.Message,
                MessageLength: MsgCount_event,
                SMSTypeId: datacompo.SMSTypeId,
                BrandName: datacompo.BrandName,
                BrandNameId: datacompo.BrandNameId,
                BrandType: datacompo.BrandType,
                Active: true,
            };
        }
    }
    else {

        if (!SMSSendId) {
            var data = {
                CampaignId: CampaignId,
                MessageName: datacompo.MessageName,
                Message: datacompo.Message,
                MessageLength: MsgCount_event,
                SMSTypeId: datacompo.SMSTypeId,
                DeviceTokenItemId: datacompo.DeviceTokenItemId,
                DeviceTokenItemIdMobifone: datacompo.DeviceTokenItemIdMobifone,
                DeviceTokenItemIdVinaphone: datacompo.DeviceTokenItemIdVinaphone,
                DeviceTokenItemIdViettel: datacompo.DeviceTokenItemIdViettel,
                GatewayConfigId: datacompo.GatewayConfigId,
                Active: true,
            };
        }
        else {
            var data = {
                SMSMessageId: SMSSendId,
                MessageName: datacompo.MessageName,
                Message: datacompo.Message,
                MessageLength: MsgCount_event,
                SMSTypeId: datacompo.SMSTypeId,
                DeviceTokenItemId: datacompo.DeviceTokenItemId,
                DeviceTokenItemIdMobifone: datacompo.DeviceTokenItemIdMobifone,
                DeviceTokenItemIdVinaphone: datacompo.DeviceTokenItemIdVinaphone,
                DeviceTokenItemIdViettel: datacompo.DeviceTokenItemIdViettel,
                GatewayConfigId: datacompo.GatewayConfigId,
                Active: true,
            };
        }
    }
    return data;
}

function createadvance_CheckMessage(message) {
    if (!AllowMsgCount) {
        custom_shownotification('error', 'Quá số lượng kí tự cho phép');
        return false;
    }
    if (!$('#mesage-event-name').val())
    {
        custom_shownotification('error',massage.MessageNameRequire);
        return false;
    }
    var str = $('#Message-event').val();
    if ($('#Message-event').val()) {
        if (parseInt($("input:radio[name='smstype-event']:checked").val()) == 2 &&parseInt( $("#select-brandname-event").val())==-1) {
                custom_shownotification('error', message.NoBrandName);
                $('#divLoading').hide();
                return false;
           
        }
        if (parseInt($("input:radio[name='smstype-event']:checked").val()) == 3 &&parseInt($("#select-gateway-event").val()) == -1) {

                custom_shownotification('error', message.NoConfig);
                $('#divLoading').hide();
                return false;
        }


        if (parseInt($("input:radio[name='smstype-event']:checked").val()) == 3 && isNaN( parseInt($("#select-gateway-event").val()))) {

            custom_shownotification('error',  message.NoConfig);
            $('#divLoading').hide();
            return false;
        }
        //if (parseInt($("input:radio[name='smstype-event']:checked").val())) {

        //    for (var i = 0, len = str.length; i < len; i++) {
        //        if (parseInt(str[i].charCodeAt(0)) > 127) {
        //            custom_shownotification('error', message.CharacterNotAllow);
        //            $('#divLoading').hide();
        //            return false;
        //        }
        //    }
        //    return true;
        //}
        else {
            return true;
        }

    }
    else {
        custom_shownotification('error', message.NoContentMessage);
        $('#divLoading').hide();
        return false;
    }
}


function createadvance_LoadFromEmail(apiDomain, access_token, datanewsletter , IsCreate, message) {
    var data;
    $.ajax({
        url: apiDomain + 'api/User/GetFromMailList',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    createadvance_LoadSelectEmailFrom(datanewsletter,IsCreate, data,message);
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
        }
    })
}

function createadvance_LoadSelectEmailFrom(datanewsletter,IsCreate, data,message) {
   
        var option = [];
        var optionreply = [];
        var ItemIdDefault;
        $.each(data, function (index, obj) {
            if (obj.IsDefault) {
                ItemIdDefault = obj.ItemId;
            }
            if (obj.IsVerify) {
                option.push({
                    id: obj.ItemId,
                    text: obj.FromName + ' <' + obj.FromMail + '>',
                    name: obj.FromName,
                    email: obj.FromMail,
                });

                optionreply.push({
                    id: obj.ItemId,
                    text: obj.FromMail,
                    name: obj.FromName,
                    email: obj.FromMail,
                });
            }

        });
        if ($('#txtSettingEmailSend').data('select2')) {

            $('#txtSettingEmailSend').select2('destroy');
            $('#txtSettingEmailSend').children().remove();
        }
        if ($('#txtSettingEmailReply').data('select2')) {
            $('#txtSettingEmailReply').select2('destroy');
            $('#txtSettingEmailReply').children().remove();
        }
       

        if (data.length == 0) {
            $('#txtSettingEmailSend').select2({
                data: option,
                "noResults": function () {
                    return "<a href='/Account/UserProfile#FromEmail' target='_blank' class='list-group-item bg-green bg-font-green' style=padding:5px;>" + message.AddNewFromEmail + "</a>";
                }
            });
            $('#txtSettingEmailReply').select2({
                data: optionreply,
                "noResults": function () {
                    return "<a href='/Account/UserProfile#FromEmail' target='_blank' class='list-group-item bg-green bg-font-green' style=padding:5px;>" + message.AddNewFromEmail + "</a>";
                }
            });
        }
        else
        {
            $('#txtSettingEmailSend').select2({
                data: option,
               
            });
            $('#txtSettingEmailReply').select2({
                data: optionreply,
               
            });

        }


        if (IsCreate) {
            $('#txtSettingEmailSend').select2('val', ItemIdDefault);
            $('#txtSettingEmailReply').select2('val', ItemIdDefault);
        }
        else {

            switch (parseInt(datanewsletter.ErrorCode)) {
                case 0:
                    var newsletter = datanewsletter.Data;
                    var text = newsletter.FromName + ' <' + newsletter.FromEmail + '>';
                    var send = $("#txtSettingEmailSend option:contains('" + text + "')").val();

                    $('#txtSettingEmailSend').select2('val', send);

                    var reply = ($("#txtSettingEmailReply option:contains('" + newsletter.ReplyToEmail + "')").val());
                    $('#txtSettingEmailReply').select2('val', reply);
                    break;
                default:
                    $('#txtSettingEmailSend').select2('val', ItemIdDefault);
                    $('#txtSettingEmailReply').select2('val', ItemIdDefault);
                    break;
            }
        }
    //placeholder search
        $("#txtSettingEmailSend").on("select2:open", function () {
            $(".select2-search--dropdown .select2-search__field").attr("placeholder", "Search...");
        });
        $("#txtSettingEmailSend").on("select2:close", function () {
            $(".select2-search--dropdown .select2-search__field").attr("placeholder", null);
        });

        $("#txtSettingEmailReply").on("select2:open", function () {
            $(".select2-search--dropdown .select2-search__field").attr("placeholder", "Search...");
        });
        $("#txtSettingEmailReply").on("select2:close", function () {
            $(".select2-search--dropdown .select2-search__field").attr("placeholder", null);
        });


    
}



//function createadvanced_LoadCustomFieldDate(apiDomain,access_token,message)
//{
//    $.ajax({
//        url: apiDomain + 'api/CustomField/GetListForBirthdayEvent',
//        type: 'GET',
//        contentType: 'application/json; charset=utf-8',
//        beforeSend:function(request)
//        {
//            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
//        },
//        success:function(r)
//        {
//            switch(parseInt(r.ErrorCode))
//            {
//                case 0:
//                    var option = '';
//                    if (r.Data.length > 0)
//                    {
                       
//                        $.each(r.Data, function (i,o) {
//                            option += '<option value="' + o.FieldId + '">' + o.FieldName + '</option>';
//                        });
//                    }
                    
//                    $('#birthday-click').append(option);
//                    $('#birthday-click').selectpicker('render');

//                    break;
//                default:
//                    break;
//            }
//        },
//        error:function()
//        {

//        }
//    })
//}

function createadvance_GetSpecialDays(apiDomain, access_token, message) {
    var dfrd1 = $.Deferred();
    var data = {
        IsPredefine: null,
        Active: true,
    }
    $.ajax({
        url: apiDomain + 'api/AutoCampaign/GetSpecialDays',
        type: 'Post',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    {
                        var field = createadvance_GetListForBirthdayEvent(apiDomain, access_token, message);
                        switch (parseInt(field.ErrorCode)) {
                            case 0:
                                var option = ''; //'<option value="0">Birthday</option>';
                                var customfield = field.Data;
                                $.each(customfield, function (i, o) {
                                   
                                    if (parseInt(o.FieldId) == 5) {
                                       var value = 0;
                                    } else {
                                       var value = -parseInt(o.FieldId);
                                    }
                                    option += '<option value="' + value + '">' + o.FieldName + '</option>';
                                });


                                var special = r.Data;
                                $.each(special, function (i, o) {
                                    option += '<option value="' + o.ItemId + '">' + o.ItemName + '</option>';
                                });
                                
                                $('#specialdate-click').append(option);
                                $('#specialdate-click').selectpicker('render');
                                break;
                        }

                    }

                    break;
                default:
                     $('#specialdate-click').selectpicker('render');
                    break;
            }
            dfrd1.resolve();

        },
        error: function (x, s, e) {
            $('#specialdate-click').selectpicker('render');
            dfrd1.resolve();
        }
    });

    return $.when(dfrd1).done(function () {
    }).promise();
}

function createadvance_GetListForBirthdayEvent(apiDomain, access_token, message) {
    var data = $.ajax({
        url: apiDomain + 'api/CustomField/GetListForBirthdayEvent',
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (rf) {

        },

        async: false,
        success: function (r) {
        },
        error: function (r) {

        }
    }).responseJSON;
    return data;
}

//function createadvance_GetFormList(apiDomain, access_token, message) {
//    var dfrd1 = $.Deferred();
//    var data = {
//        IsPredefine: null,
//        Active: true,
//    }
//    $.ajax({
//        url: apiDomain + 'api/Form/Web/GetList?draw=1&start=0&length=1000000&ContactGroupId=12548',
//        type: 'get',
//        data: JSON.stringify(data),
//        contentType: 'application/json; charset=utf-8',
//        beforeSend: function (request) {
//            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
//        },
//        success: function (r) {

//            var option = '';
//            if (r.data.length > 0) {

//                $.each(r.data, function (i, o) {
//                    option += '<option value="' + o.formId + '">' + o.formName + '</option>';
//                });
//            }
//            $('#multiinput-click').append(option);
//            $('#multiinput-click').selectpicker('render');
//            dfrd1.resolve();
//        },
//        error: function (x, s, e) {
//            $('#multiinput-click').selectpicker('render');
//            dfrd1.resolve();
//        }
//    });

//    return $.when(dfrd1).done(function () {
//    }).promise();
//}
















