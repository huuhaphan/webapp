﻿function advandceEditor_ActionButton(apiDomain, access_token,UserId, NewsletterId, imgUrl, message) {

    advanceEditor_LoadInfo(apiDomain, access_token, UserId, NewsletterId);

    advandceEditor_ActionBack();

    //lưu nội dung email
    advandceEditor_SaveEmailContent(apiDomain, access_token, NewsletterId,message);

    //sự kiện inset image 
    advandceEditor_ActionUploadImage(apiDomain, access_token, UserId, imgUrl)

    advandceEditor_SaveAndExit(apiDomain, access_token, NewsletterId,message);
}


//summer editor
var advandceEditor_EmailEditor = function () {
    var handleSummernote = function () {
        var height = parseInt($(window).height() - 100);
        $('#event_contentInput').summernote({
            toolbar: [
              // [groupName, [list of button]]
              ['style', ['bold', 'italic', 'underline', 'clear']],
              ['font', ['strikethrough', 'superscript', 'subscript']],
              ['fontsize', ['fontsize']],
              ['color', ['color']],
              ['para', ['style', 'ul', 'ol', 'paragraph']],
              ['height', ['height']],
              ['inset', ['link', 'table', 'hr']],
              ['misc', ['codeview', 'undo', 'redo', 'help']],
              ['insert', ['image']],
              ['personalize', ['contactname', 'email']],

            ],

            minHeight: height,


        });

        //API:
        //var sHTML = $('#summernote_1').code(); // get code
        //$('#summernote_1').destroy(); // destroy
    }

    return {
        //main function to initiate the module
        init: function () {
            handleSummernote();
        }
    };

}();


function advanceEditor_LoadInfo(apiDomain, access_token, UserId, NewsletterId)
{
    if(NewsletterId)
    {
        $.ajax({
            url: apiDomain + 'api/newsletter/GetInfo?NewsletterId=' + NewsletterId,
            type: 'GET',
            contentType: 'application/json; charset-utf-8',
            beforeSend: function (request) {
                request.setRequestHeader('Authorization', 'Bearer ' + access_token);
            },
            success:function(r)
            {
                switch(parseInt(r.ErrorCode))
                {
                    case 0:
                        var data=r.Data;
                        $('#event_contentInput').summernote('code', data.HTML);
                        break;
                    default:
                        break;
                }
            },
            error: function(x,s,e)
            {
                custom_shownotification('error', 'Có lỗi xảy ra');
            },
            
        });
    }
}

//quay lại
function advandceEditor_ActionBack(CampaignId) {
    $("#close-edit-email-content").on("click", function () {
        window.parent.postMessage("ClosePopup", "*");
    });
}

//lưu nội dung của email trong campaign
function advandceEditor_SaveEmailContent(apiDomain, access_token, NewsletterId,message) {
    var isave = 0; //click save 1 lần thôi
    $('#btnsaveemailcontent').on('click', function () {

        if (isave == 0) {

            var html = $('#event_contentInput').summernote('code');
            if (html) {
                var data = {
                    NewsletterId: NewsletterId,
                    HTML: html,
                    JSONData: "",
                    IsMailBuilder: false,


                }
                $('#divLoading').show();
                $.ajax({
                    url: apiDomain + 'api/newsletter/UpdateMailContent',
                    type: 'POST',
                    contentType: 'application/x-www-form-urlencoded',
                    data: data,
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", "Bearer " + access_token);
                    },
                    success: function (r) {
                        $('#divLoading').hide();
                        switch (parseInt(r.ErrorCode)) {
                            case 0:
                                isave++;
                                custom_shownotification('success', 'Lưu nội dung email thành công');
                                break;
                            case 101:
                                custom_shownotification('error', 'Có lỗi xảy ra');
                            default:
                                custom_shownotification('error', 'Có lỗi xảy ra');
                                break;
                        }
                    },
                    error: function (x, s, e) {
                        $('#divLoading').hide();
                        custom_shownotification('error', 'Có lỗi xảy ra');
                    }
                });
            }
            else {
                custom_shownotification('error', 'Chưa nhập nội dung email');

            }
        }


    });

};

function advandceEditor_SaveAndExit(apiDomain, access_token, NewsletterId,message) {
    var isave = 0; //click save 1 lần thôi
    $('#btnsaveandexitemailcontent').on('click', function () {
        if (isave == 0) {
            $('#divLoading').show();
            var html = $('#event_contentInput').summernote('code');
            console.log(html);
            if (html) {
                var data = {
                    NewsletterId: NewsletterId,
                    HTML: html,
                    JSONData: "",
                    IsMailBuilder: false,


                }
                $.ajax({
                    url: apiDomain + 'api/newsletter/UpdateMailContent',
                    type: 'POST',
                    contentType: 'application/x-www-form-urlencoded',
                    data: data,
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", "Bearer " + access_token);
                    },
                    success: function (r) {
                        $('#divLoading').hide();
                        switch (parseInt(r.ErrorCode)) {
                            case 0:
                                isave++;
                                window.parent.postMessage("ClosePopup", "*");
                                break;
                            case 101:
                                custom_shownotification('error', 'Có lỗi xảy ra');
                            default:
                                custom_shownotification('error', 'Có lỗi xảy ra');
                                break;
                        }
                    },
                    error: function (x, s, e) {
                        $('#divLoading').hide();
                        custom_shownotification('error', 'Có lỗi xảy ra');
                    }
                });
            }
            else {
                custom_shownotification('error', 'Chưa nhập nội dung email');
                $('#divLoading').hide();
            }
        }
    })
}

var file;
var blobName;
function advandceEditor_ActionUploadImage(apiDomain, access_token, userId, imgUrl) {
    //button trong editor click trigger input file bên ngoài
    $('#insertImage').on('change', function (e) {
        //sự kiện chọn hình, upload hình
        var files = e.target.files;
        var name = files[0].name;
        file = files[0];
        var extend = name.split('.').pop();
        console.log(extend);
        blobName = userId + moment().format('YYYYMMDDHHmmss') + '.' + extend;
        $.ajax({
            url: apiDomain + 'api/EmailSend/GetSASImage?BlobName=' + blobName,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            success: function (r) {
                uri = r.Data;
                advandceEditor_uploadToAzureEmailContent(apiDomain, access_token, file, uri, blobName, imgUrl)
            }
        });
    });
}

//upload lên azure và server
function advandceEditor_uploadToAzureEmailContent(apiDomain, access_token, file, uri, blobName, imgUrl) {

    var maxBlockSize = 2 * 1024 * 1024;
    var fileSize = file.size;
    if (fileSize < maxBlockSize) {
        maxBlockSize = fileSize;
    }
    var blockIds = new Array();
    var blockIdPrefix = "block-";
    var currentFilePointer = 0;
    var totalBytesRemaining = fileSize;
    var numberOfBlocks = 0;
    var bytesUploaded = 0;

    if (fileSize % maxBlockSize == 0) {
        numberOfBlocks = fileSize / maxBlockSize;
    } else {
        numberOfBlocks = parseInt(fileSize / maxBlockSize, 10) + 1;
    }

    var reader = new FileReader();
    reader.onloadend = function (evt) {
        if (evt.target.readyState == FileReader.DONE) { // DONE == 2
            var r = uri + '&comp=block&blockid=' + blockIds[blockIds.length - 1];
            var requestData = new Uint8Array(evt.target.result);
            $.ajax({
                url: r,
                type: "PUT",
                data: requestData,
                processData: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('x-ms-blob-type', 'BlockBlob');
                    //    xhr.setRequestHeader('Content-Length', requestData.length);
                },
                success: function (data, status) {
                    advandceEditor_uploadFileInBlocks();
                },
                error: function (xhr, desc, err) {
                }
            });
        }
    };


    function advandceEditor_uploadFileInBlocks() {

        if (totalBytesRemaining > 0) {
            var fileContent = file.slice(currentFilePointer, currentFilePointer + maxBlockSize);
            var blockId = blockIdPrefix + pad(blockIds.length, 6);
            console.log("block id = " + blockId);
            blockIds.push(btoa(blockId));
            reader.readAsArrayBuffer(fileContent);
            currentFilePointer += maxBlockSize;
            totalBytesRemaining -= maxBlockSize;
            if (totalBytesRemaining < maxBlockSize) {
                maxBlockSize = totalBytesRemaining;
            }
        } else {
            advandceEditor_commitBlockList();
        }
    }

    advandceEditor_uploadFileInBlocks();

    function advandceEditor_commitBlockList() {

        var submituri = uri + '&comp=blocklist';
        var requestBody = '<?xml version="1.0" encoding="utf-8"?><BlockList>';
        for (var i = 0; i < blockIds.length; i++) {
            requestBody += '<Latest>' + blockIds[i] + '</Latest>';
        }
        requestBody += '</BlockList>';
        $.ajax({
            url: submituri,
            type: "PUT",
            data: requestBody,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('x-ms-blob-content-type', file.type);
                xhr.setRequestHeader('Content-Length', requestBody.length);
            },
            success: function (data, status) {

                //$('#editemail_contentInput').summernote('insertImage', imgUrl + blobName, '');
                $('#event_contentInput').summernote('insertImage', imgUrl + blobName, function ($image) {
                    $image.css('max-width', $image.width());
                    $image.css('width', '100%');
                    $image.attr('data-filename', '');
                });
            },
            error: function (xhr, desc, err) {
                custom_shownotification('error', 'Lỗi upload hình');
            }
        });

    }
}
