﻿function managecontact_Action(apiDomain, access_token, tableId, ContactGroupId,message)
{
    //checkbox on table
    managecontact_Checkbok(apiDomain, access_token, tableId,ContactGroupId, message);

    //export excel
    managecontact_ExportExcel(apiDomain, access_token, tableId, ContactGroupId, message);
}
function managecontact_Checkbok(apiDomain,access_token,tableId,ContactGroupId,message)
{
    var table = $(tableId).DataTable();
    //các sự kiện với checkbox ,select all
    $(tableId + ' tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');

        // Get row data
        var data = table.row($row).data();

        // Get row ID
        var rowId = custom_GetFisrtProJson(data)

        // Determine whether row ID is in the list of selected row IDs 
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            $(this).parent().addClass('checked')
            rows_selected.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            $(this).parent().removeClass('checked')
            rows_selected.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }

        //show delete all
        //if (rows_selected.length > 0) {
        //    $('#btn-deleteall-group').removeClass('hide');
        //}
        //else {
        //    $('#btn-deleteall-group').addClass('hide');
        //}

        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrl(table);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on "Select all" control
    $('input[name="select_all"]').on('click', function (e) {
        if (this.checked) {
            $(tableId + ' tbody input[type="checkbox"]:not(:checked)').trigger('click');
            //show deleteall
            //$('#btn-deleteall-group').removeClass('hide');
        } else {
            $(tableId + ' tbody input[type="checkbox"]:checked').trigger('click');
            //ẩn delete all
            //$('#btn-deleteall-group').addClass('hide');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    table.on('draw', function () {
        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrl(table);
    });
}

function managecontact_ExportExcel(apiDomain, access_token, tableId, ContactGroupId, message) {
    $('#btn-export-contact').on('click', function () {
        $('#divLoading').show();
        $.ajax({
            url: apiDomain + 'api/Contact/Web/GetList?draw=1&start=0&length=0&ContactGroupId=' + ContactGroupId,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            success: function (r) {

                if (r.data.length > 0) {
                    var list = [];
                    var head = ["ContactName", "Email", "Mobile"];
                    list.push(head);
                    $.each(r.data, function (i, o) {
                        var obj = [];
                        obj.push(o.contactName);
                        obj.push(o.email);
                        obj.push(o.mobile);
                        list.push(obj);
                    });

                    write_exportContact(list);
                }
                else {
                    $('#divLoading').hide();
                    custom_shownotification("error", "Không có dữ liệu");
                }
            },
            error: function () {
                $('#divLoading').hide();
                custom_shownotification("error", "Có lỗi xảy ra");
            }
        })
    })
}
