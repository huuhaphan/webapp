﻿function index_Action(apiDomain, access_token, tableId, message) {
    index_EditAdvanceCampaign(tableId, message);
    index_UpdateAdvanceCampaign(apiDomain, access_token, tableId, message);
    index_ManageAdvanceCampaign(tableId, message);
    index_ContactAdvanceCampaign(tableId, message);
    index_DeleteAdvanceCampaign(tableId, message);
    index_Delete(apiDomain, access_token, tableId, message);
    index_NewslettersAdvanceCampaign(tableId, message);
    indexadvance_CheckBox(apiDomain, access_token, tableId, message);
    index_DeleteAll(message);
    index_ButtonGroup(tableId);
}

function index_EditAdvanceCampaign(tableId, message)
{
    var tablerow = $(tableId).DataTable();
    $(tableId + ' tbody').on('click', '#edit', function () {
        var rowData = tablerow.row($(this).parents('tr')).data();
        $('#modalUpdateAdvanceCampaing').modal('toggle');
        $('#update-campaignname-advance').attr("data-eid", rowData.campaignId);
        $('#update-campaignname-advance').val(rowData.campaignName);
        $('#update-campaignactive-advance').prop('checked', rowData.active);

    });
}

function index_UpdateAdvanceCampaign(apiDomain,access_token, tableId,message)
{
    $('#save-update-campaign-advance').on('click', function () {
        if (index_CheckBeforeUpdate(message)) {
            $('#divLoading').show();
            var data = {
                CampaignId:parseInt( $('#update-campaignname-advance').attr('data-eid')),
                CampaignName: $('#update-campaignname-advance').val(),
                Description: $('#update-campaigndesciption-advance').val(),
                Active:$('#update-campaignactive-advance').prop('checked'),

            }
            $.ajax({
                url: apiDomain+ 'api/AutoCampaign/Update',
                type: 'post',
                data:JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                beforeSend: function (request) {
                   
                        request.setRequestHeader("Authorization", "Bearer " + access_token);
                    
                },
                success: function (r) {
                    $('#divLoading').hide();
                    switch(parseInt(r.ErrorCode))
                    {
                        case 0:
                            custom_shownotification("success", message.UpdateCampaignSuccess);
                            $('#modalUpdateAdvanceCampaing').modal('toggle');
                            $(tableId).dataTable().fnDraw();
                            break;
                        default:
                            custom_shownotification("success", message.UpdateCampaignError);
                            break;
                    }
                },
                error:function(x,s,e)
                {
                    $('#divLoading').hide();
                    custom_shownotification("error", message.HaveError);
                }
            })
        }
    });
}

function index_CheckBeforeUpdate(message)
{
    if ($('#update-campaignname-advance').val() == '' || $('#update-campaignname-advance').val() == null) {
        custom_shownotification("error", message.CampaignNameRequire);
        return false;
    }
    else
        return true;
}

function index_ButtonGroup(tableId)
{
    $(tableId + ' tbody').on('click', '#btn-group-advancecampaign', function () {
        if (mousePos.y >= $(window).height() / 2) {
            $(this).children('.dropdown-menu').addClass('bottom-up');
        }
    });
}

var mousePos;
document.onmousemove = handleMouseMove;
function handleMouseMove(event) {
    var dot, eventDoc, doc, body, pageX, pageY;

    event = event || window.event; // IE-ism

    // If pageX/Y aren't available and clientX/Y are,
    // calculate pageX/Y - logic taken from jQuery.
    // (This is to support old IE)
    if (event.pageX == null && event.clientX != null) {
        eventDoc = (event.target && event.target.ownerDocument) || document;
        doc = eventDoc.documentElement;
        body = eventDoc.body;

        event.pageX = event.clientX +
          (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
          (doc && doc.clientLeft || body && body.clientLeft || 0);
        event.pageY = event.clientY +
          (doc && doc.scrollTop || body && body.scrollTop || 0) -
          (doc && doc.clientTop || body && body.clientTop || 0);
    }

    mousePos = {
        x: event.pageX,
        y: event.pageY,
        m: $(window).width(),
        n: $(window).height(),
    };
}

function index_ManageAdvanceCampaign(tableId, message)
{
    var tablerow = $(tableId).DataTable();
    $(tableId + ' tbody').on('click', '#manage', function () {
        var rowData = tablerow.row($(this).parents('tr')).data();
        window.location.href = "/advancedcampaign/managecampaign?eId=" + rowData.campaignId;
    });
}

function index_ContactAdvanceCampaign(tableId,message)
{
    var tablerow = $(tableId).DataTable();
    $(tableId + ' tbody').on('click', '#contact', function () {
        var rowData = tablerow.row($(this).parents('tr')).data();
        var ContactgroupId = rowData.contactGroups[0].contactGroupId;
        var CampaignId = rowData.campaignId;
        window.location.href = "/contacts/contactlist?gId=" + encodeURI(ContactgroupId + '&eId=' + CampaignId + '&eId=' + CampaignId + '&gName=' + '(Thuộc chiến dịch nâng cao) ' + rowData.campaignName);
    });
}

function index_NewslettersAdvanceCampaign(tableId, message) {
    var tablerow = $(tableId).DataTable();
    $(tableId + ' tbody').on('click', '#newsletter', function () {
        var rowData = tablerow.row($(this).parents('tr')).data();
        window.location.href = "/advancedcampaign/managenewsletters?eId=" + rowData.campaignId;
    });
}

function index_DeleteAdvanceCampaign(tableId, message)
{
    var tablerow = $(tableId).DataTable();
    $(tableId + ' tbody').on('click', '#delete', function () {
        var rowData = tablerow.row($(this).parents('tr')).data();
        $('#modal-confirm-delete-campaign-advance').modal('toggle');
        $('#confirm-delete-advance').attr("data-type", 'single');
        $('#confirm-delete-advance').attr("data-eid", rowData.campaignId);
    });
}

function index_Delete(apiDomain,access_token,tableId,message)
{
    $('#confirm-delete-advance').on('click', function () {
        $('#modal-confirm-delete-campaign-advance').modal('toggle');
        var type = $(this).data('type');
        if (type == 'single') {
            var CampaignId = $('#confirm-delete-advance').attr("data-eid");
            index_DeleteSingleItem(apiDomain, access_token, tableId, message,CampaignId);
        }
        else
        {
            if (rows_selected.length == 1) {
                index_DeleteSingleItem(apiDomain, access_token, tableId, message,rows_selected[0]);
            }
            else {
                var length = 0;
                $.each(rows_selected, function (i, o) {
                    $.ajax({
                        url: apiDomain + 'api/AutoCampaign/Delete?CampaignId=' + o,
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function (request) {
                            request.setRequestHeader("Authorization", "Bearer " + access_token);
                        },
                        success: function (r) {
                            length++;
                            if (length == rows_selected.length);
                            {
                                $('#divLoading').hide();
                                $('#btn-delete-advance').addClass('hide');
                                rows_selected = [];

                                
                                custom_shownotification("success", message.DeleteCampaignSuccess);
                                $(tableId).dataTable().fnDraw();

                            }


                        }, error: function (x, s, e) {
                            length++;
                            if (length == rows_selected.length);
                            {
                                $('#btn-delete-advance').addClass('hide');
                                rows_selected = [];
                              
                                $('#divLoading').hide();
                                custom_shownotification("error", message.HaveError);
                            }
                        }
                    });
                })
            }
        }
    });
}


function index_DeleteSingleItem(apiDomain, access_token, tableId, message,CampaignId)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/AutoCampaign/Delete?CampaignId=' + CampaignId,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            $('#divLoading').hide();
            switch (parseInt(r.ErrorCode)) {
                case 0:
                  
                    custom_shownotification("success", message.DeleteCampaignSuccess);
                    $(tableId).dataTable().fnDraw();
                    break;
                default:
                    custom_shownotification("success", message.DeleteCampaignError);
                    break;
            }
        }, error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification("error", message.HaveError);
        }
    })
}

function index_DeleteAll(message)
{
    $('#btn-delete-advance').on('click', function () {
        if(rows_selected.length>0)
        {
            $('#modal-confirm-delete-campaign-advance').modal('toggle');
            $('#confirm-delete-advance').attr("data-type", 'multi');
        }
        else
        {
            custom_shownotification("error", message.NoSelectCampaign);
        }
    })
}

function indexadvance_CheckBox(apiDomain, access_token, tableId, message) {

    $(tableId).on('click', 'tbody input[type="checkbox"]', function (e) {
        if ($.fn.DataTable.isDataTable(tableId)) {
            var table = $(tableId).DataTable();
            var $row = $(this).closest('tr');

            // Get row data
            var data = table.row($row).data();

            // Get row ID
            var rowId = data.campaignId;// custom_GetFisrtProJson(data)

            // Determine whether row ID is in the list of selected row IDs 
            var index = $.inArray(rowId, rows_selected);

            // If checkbox is checked and row ID is not in list of selected row IDs
            if (this.checked && index === -1) {
                $(this).parent().addClass('checked')
                rows_selected.push(rowId);

                // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
            } else if (!this.checked && index !== -1) {
                $(this).parent().removeClass('checked')
                rows_selected.splice(index, 1);
            }

            if (this.checked) {
                $row.addClass('selected');
            } else {
                $row.removeClass('selected');
            }

            if (rows_selected.length > 0)
            {
                $('#btn-delete-advance').removeClass('hide');
            }
            else
            {
                $('#btn-delete-advance').addClass('hide');
            }

            // Update state of "Select all" control
            global_updateDataTableSelectAllCtrl(table);

            // Prevent click event from propagating to parent
            e.stopPropagation();
        }
    });

    // Handle click on "Select all" control
    $('input[name="select_all"]').on('click', function (e) {
        if (this.checked) {
            $(tableId + ' tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $(tableId + ' tbody input[type="checkbox"]:checked').trigger('click');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event

    $(tableId).on('draw', function () {
        var table = $(tableId).DataTable();
        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrl(table);
    });


}

