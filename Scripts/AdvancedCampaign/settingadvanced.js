﻿function settingadvanced_Action(apiDomain,access_token,message)
{

    //load danh sách group contact
    var ContactGroupId = -1; //lựa chọn ban đầu
    settingadvaced_LoadContactGroup(apiDomain, access_token, ContactGroupId, message);
    //tạo contact group contact
    settingadvanced_CreateContactGroup(apiDomain, access_token, message);

    //lưu campaign 
    settingadvanced_CreateAvancedCampaign(apiDomain, access_token, message);

    //cancel not create campaign auto 
    settingadvanced_CancelAdvanceCampaign();
}

//load danh sách liên hệ
function settingadvaced_LoadContactGroup(apiDomain, access_token, ContactGroupId, message)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain +'api/ContactGroup/GetListForAutoCampaign', //'api/ContactGroup/GetList?Active=true&LastItemId=null&CampaignAuto=false&RowCount=1000000',
        // contenType: 'application/x-www-form-urlencoded',
        contenType: 'application/json; charset=utf-8',
        type: 'GET',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            $('#contactgroup-to-advance').children().remove();
            var data = r.Data;
            if (data.length > 0) {
                var option="<option value=-1>"+message.SelectContactGroup+"</option>";
                $.each(data.reverse(), function (index, obj) {
                    var token=custom_bodauTiengViet(obj.ContactGroupName);
                    option += "<option data-tokens='" + token+" " + obj.ContactGroupName + "' value=" + obj.ContactGroupId + ">" + obj.ContactGroupName + "</option>";
                });
                $('<select id="select-contactgroup-to-advance" data-live-search="true"  class="bs-select form-control" data-show-subtext="true" data-style="#EEF1F5"></select>').append(option).appendTo('#contactgroup-to-advance');

            }
            else {
                var option = "<option value=-1>" + message.NotHaveContactGroup + "</option>";
                $('<select id="select-contactgroup-to-advance"  class="bs-select form-control" data-show-subtext="true" data-style="#EEF1F5"></select>').append(option).appendTo('#contactgroup-to-advance');

            }
            $('#select-contactgroup-to-advance').selectpicker('render');
        

            $('#select-contactgroup-to-advance').selectpicker('val', ContactGroupId);
            $('#divLoading').hide();

        },
        error: function (x, s, e) {
            custom_shownotification("error", message.HasErrorLoadContact);
            $('#divLoading').hide();
        }
    })
}


function settingadvanced_CreateAvancedCampaign(apiDomain, access_token, message) {
    $("#create-new-advance-campaign").click(function (e) {

        if (settingadvanced_CheckSaveAdvancedCampaign(message)) {
            var data = {
                ContactGroupId: parseInt($('#select-contactgroup-to-advance').val()),
                CampaignName: $('#txtCampaignName-advanced').val(),
                Description: $('#txtDesciption-advanced').val(),
                Active: true,
            };
            settingadvanced_SaveAdvancedCampaign(apiDomain, access_token, data, message);
        }
    });
}


function settingadvanced_SaveAdvancedCampaign(apiDomain, access_token, data, message)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/AutoCampaign/Create',
        type: 'Post',
        contentType: 'application/json; charset=utf-8',
        data:JSON.stringify(data),
        beforeSend:function(request)
        {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success:function(r)
        {
            switch(parseInt(r.ErrorCode))
            {
                case 0:
                    custom_shownotification("success", message.CreateCampaignSuccess);
                    var data = r.Data;
                    var CampaignId = data.CampaignId;
                    setTimeout(function () { window.location.href = '/AdvancedCampaign/ManageCampaign?eId='+CampaignId; }, 500);
                    break;
                default:
                    custom_shownotification("error", message.CreateCampaignError);
                    break;
            }
            $('#divLoading').hide();
           

        },error:function(x,s,e)
        {
            $('#divLoading').hide();
            custom_shownotification("error", message.CreateCampaignError);
        }
    })
}

function settingadvanced_CheckSaveAdvancedCampaign(message)
{
    if($('#txtCampaignName-advanced').val()==""||$('#txtCampaignName-advanced').val()==null)
    {
        custom_shownotification("error", message.CampaignNameRequire);
        return false;
    }
    if (parseInt($('#select-contactgroup-to-advance').val()) == -1) {
        custom_shownotification("error", message.ContactGroupRequire);
        return false;
    }
    else return true;
}

function settingadvanced_CancelAdvanceCampaign()
{
    $('#cancel-new-advance-campaign').on('click', function () {
        window.location.href = "/AdvancedCampaign";
    });
}

function settingadvanced_CreateContactGroup(apiDomain, access_token, message)
{
    $('#btn-create-contact-group').on('click', function () {
        settingadvanced_AddContactGroup(apiDomain, access_token, message);
    })
}

//them group
function settingadvanced_AddContactGroup(apiDomain, access_token, message) {
    var active = true;
    if ($('#checkGroupActive').prop('checked')) {
        active = true;
    } else {
        active = false;
    }
    var data = {
        Active: active,
        ContactGroupName: $('#contactgroupname').val(),
        Description: $('#contactgroupdesciption').val(),
    };
    if (settingadvanced_CheckNull(message)) {
        //đóng modal thêm group
        $('#modal-create-contactgroup').modal('toggle');
        $('#divLoading').show();
        $.ajax({
            url: apiDomain + 'api/ContactGroup/Create',
            type: 'POST',
            data: data,
            contentType: 'application/x-www-form-urlencoded',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            success: function (r) {
                var errorcode = parseInt(r.ErrorCode);
                $('#divLoading').hide();
                switch (errorcode) {
                    case 0:
                        var data = JSON.parse(JSON.stringify( r.Data));
                        custom_shownotification("success", message.CreateGroupSuccess);
                        settingadvaced_LoadContactGroup(apiDomain, access_token, data.ContactGroupId, message);
                        break;
                    default:
                        break;
                }
                
            },
            error: function (x, s, e) {
                $('#divLoading').hide();
                custom_shownotification("error", message.CreateGroupError);
            },
        });
    }
}

//kiểm tra text null thêm contactgroup
function settingadvanced_CheckNull(message) {
    if ($('#contactgroupname').val() == null || $('#contactgroupname').val() == "") {
        custom_shownotification("error", message.ContactGroupNameIsRequire);
        return false;
    }
    else {
        return true;
    }
};









