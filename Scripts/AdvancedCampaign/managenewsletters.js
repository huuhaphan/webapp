﻿function managenewsletter_Action(apiDomain,access_token,tableId,message)
{
    //checkbox on table
    managenewsletter_Checkbox(apiDomain, access_token, tableId, message);

    //modal create newsletter
    managenewsletter_OpenModalCreateNewsletter(apiDomain, access_token, tableId, message);

    //call summernote editor
    managenewletter_Editor();

    //delete newsletter
    managenewsletter_Delete(apiDomain, access_token, tableId, message);
}

function managenewsletter_Checkbox(apiDomain,access_token,tableId,message)
{
    var table = $(tableId).DataTable();
    //các sự kiện với checkbox ,select all
    $(tableId + ' tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');

        // Get row data
        var data = table.row($row).data();

        // Get row ID
        var rowId = custom_GetFisrtProJson(data)

        // Determine whether row ID is in the list of selected row IDs 
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            $(this).parent().addClass('checked')
            rows_selected.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            $(this).parent().removeClass('checked')
            rows_selected.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }

        //show delete all
        //if (rows_selected.length > 0) {
        //    $('#btn-deleteall-group').removeClass('hide');
        //}
        //else {
        //    $('#btn-deleteall-group').addClass('hide');
        //}

        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrl(table);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on "Select all" control
    $('input[name="select_all"]').on('click', function (e) {
        if (this.checked) {
            $(tableId + ' tbody input[type="checkbox"]:not(:checked)').trigger('click');
            //show deleteall
            //$('#btn-deleteall-group').removeClass('hide');
        } else {
            $(tableId + ' tbody input[type="checkbox"]:checked').trigger('click');
            //ẩn delete all
            //$('#btn-deleteall-group').addClass('hide');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    table.on('draw', function () {
        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrl(table);
    });
}

function managenewsletter_OpenModalCreateNewsletter(apiDomain, access_token, tableId, message)
{
    $('#create-new-advance-newsletter').on('click', function () {
        $('#modalCreateAdvanceNewsletter').modal('toggle');
    });
}

//summer editor

function managenewletter_Editor() {
    var height = parseInt($(window).height() - $('.page-bar').height() - $('.page-footer').height() - 120);
    $('#contentnewsletter').summernote({
        toolbar: [
          // [groupName, [list of button]]
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['style', 'ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['inset', ['link', 'table', 'hr']],
          ['misc', ['fullscreen', 'codeview', 'undo', 'redo']],
          ['insert', ['image']]
        ],

        height: height,

    });


};

function managenewsletter_Delete(apiDomain, access_token, tableId, message)
{
    var table = $(tableId).DataTable();
    $(tableId + ' tbody').on('click', '#delete', function () {
        rowData = table.row($(this).parents('tr')).data();
        $('#modal-confirm-delete-newsletter').modal('toggle');
     
    });
}


