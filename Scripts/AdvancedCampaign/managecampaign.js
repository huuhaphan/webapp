﻿function managecampaign_Action(apiDomain, access_token, CampaignId, message, permission)
{
    managecampaign_GetListEventGird(apiDomain, access_token, CampaignId, message,permission)
    managecampaign_Changeview(apiDomain, access_token, CampaignId, message, permission);

    //show tool khi hover 1 event
    managecampaign_HoverTool_Gird(CampaignId, message, permission);
    //show manage khi click manage 
    managecampaign_ClickManageButton();

    //show button add
    managecampaign_HoverAddDay();

    //bấm tạo mới trên day có event
    managecampaign_CreateOnGrid(CampaignId);
    managecampaign_Publish(apiDomain, access_token,message);
    managecampaign_ConfirmPublish(apiDomain, access_token, message);

    //cảnh báo khi có gate way
    managecampaign_ConfirmWarningPublish(apiDomain, access_token, message);

    managecampaign_ModalDelete();
    managecampaign_ConfirmDelete(apiDomain, access_token, CampaignId, message, permission);

    managecampaign_EditOnTable(CampaignId);

    managecampaign_Statistics();

    managecampaign_View();
    managecampaign_ModalSendTest(apiDomain, access_token, message);
    managecampaign_SendTestEmail(apiDomain, access_token, message);

    managecamapaign_LoadMoreOnDay();
    managecamapaign_HideMoreOnday();
    managecampaign_LoadMoreOndayHover();

    managecampaign_drop(apiDomain, access_token, CampaignId, message);

    managecampaign_resize();
    window.onresize = function () {
        managecampaign_resize();
    }
}

function managecampaign_resize()
{
    var width= window.innerWidth;
    if (width > 1280) {
        $('.advance-campaign-show-grid').css({ 'max-width': '1000px' });
    }
    if(width>1110 && width<=1280)
    {
        $('.advance-campaign-show-grid').css({ 'max-width': '825px' });
    }
    if (width>768 && width <= 1110) {
        $('.advance-campaign-show-grid').css({ 'max-width': '660px' });
    }
    
}

function managecampaign_ShowHide()
{
    $('#showless-newsletter').hide();
    $('#loadmore-newsletter').show();

    var size_li = $("#grid-newsletter .li-day").size();
    var x = 24;
    $('#grid-newsletter .li-day:lt(' + x + ')').show();
    $('#loadmore-newsletter').click(function () {
        x = (x + 12 <= size_li) ? x + 12 : size_li;
        $('#grid-newsletter .li-day:lt(' + x + ')').show();
        $('#showless-newsletter').show();
        if (x == size_li) {
            $('#loadmore-newsletter').hide();
        }
    });
    $('#showless-newsletter').click(function () {
        x = (x - 12 < 0) ? 12 : x - 12;
        $('#grid-newsletter .li-day').not(':lt(' + x + ')').hide();
        $('#loadmore-newsletter').show();
        $('#showless-newsletter').show();
        if (x == 24) {
            $('#showless-newsletter').hide();
        }
    });
}

function managecampaign_ShowGird(data, CampaignId, message) {
    $('#loadmore-newsletter').removeClass('hide');
   
    $('#grid-newsletter').children().remove();
    var html = "";
    var totalDay = 96;
    var obj = [];
    var arr = [];
    $.each(data, function (i, o) {
        if (o.BaseType == 1) { //timebase
            obj.push(o);
            arr.push(o.BaseDay);
        }
    });

    var grouparr = _.groupBy(obj, function (value) {
        return  'BaseDay'+ value.BaseDay;
    });
    
    
    for (var i = 0; i < totalDay; i++) {
        var li = '<li class=" col-lg-2 col-md-2 col-sm-3 nopading" style="width:164.66px;" data-date="'+i+'">';
        var Keys = 'BaseDay' + i;
        if (grouparr[Keys]) {
            var data = grouparr[Keys];
            if (data.length == 1) {
                
                html += li + managecampaign_CellSingle(data, i, CampaignId, message)+ '</li>';
            }
            else
            {

                html += li + managecampaign_CellMultiEvent(data, i, CampaignId, message)+'</li>';
            }
           

        }
        else {
            html += li + managecampaign_CellNoEvent(i, CampaignId, message) +'</li>';
        }
    }

    $('#grid-newsletter').append(html);


}

function managecampaign_CellNoEvent(i, CampaignId, message) {
    var html = '<div class="li-day btn"  ondragleave="leaveDrop(event)"  ondragover="allowDrop(event)" data-date="' + i + '">' +
               '<span  class="span-add-day" data-day="' + i + '" ><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span>' +
                                  ' <a class="number-day-create-event" href="/AdvancedCampaign/Create?eId=' + CampaignId + '&Day=' + i + '" title="' + message.Create + '">' +
                                       '<span >' + i + '</span>' +
       '</a>' +
       '</div>';

    return html;
}

function managecampaign_CellMultiEvent(data, i, CampaignId, message) {
    var base = "";
    $.each(data, function (j, o) {
        var image = "";

        if (data.SnapShotUrl != null && data.SnapShotUrl != "") {
            image = data.SnapShotUrl;
        }
        else {
            image = '/Content/Images/thumbnail-generator.png';
        }
        var Name = o.ResponderName;
        if (Name != "" && Name != 'null' && Name != null && Name.length > 15) {
            Name = Name.substring(0, 10) + ' ...';
        }

        var method = managecampaign_Gettype(o.SendMethod);

        base += ' <div  class="number-day-create-event-has multipe-baseday-event-has" style="background: rgb(195, 195, 195);"  data-type="' + o.BaseType + '"  data-name="' + o.ResponderName + '" data-img="' + image + '" data-date="' + i + '" data-baseId="' + o.BaseId + '" data-publish="' + o.Publish + '" style="text-align:left;">' +
                         
            '<a ondragstart="dragStart(event)"  ondrag="dragging(event)" draggable="true" id="dragtarget" style="color: white;font-weight: normal;"  href="/AdvancedCampaign/Create?eId=' + CampaignId + '&bId=' + o.BaseId + '&Day=' + i + '" data-date="' + i + '" data-baseId="' + o.BaseId + '">' + method +
               Name + '</a>' +
                           
            '</div>';


    });
    var more = "";
    var height = data.length * 22+ 20; 
    if (data.length > 4)
    {
        more = '<a class="load-more-base-on-day" id="load-more-base-on-day" >Xem thêm..</a>';
        height = 110;
    }
               

    var html = '<div class="li-day btn li-day-has-event" ondragleave="leaveDrop(event)" ondragover="allowDrop(event)" data-date="' + i + '">' +
                      '<span data-day="' + i + '"  class="span-add-day" ><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span>' +

                      '<span  class="span-number-day pull-left bold" >' + i + '</span><br/>' +
                      '<div id="container-event-day" style="height:' + height + 'px">' +
                           base +
                           
                         '</div>' +
                          more +
                        
                 '</div>';
    return html;
}

function managecampaign_CellSingle(data, i, CampaignId, message) {
    var style = '';

    var image = "";
    if (data[0].SnapShotUrl != null && data.SnapShotUrl != "") {
        image = data[0].SnapShotUrl;
    }
    else {
        image = '/Content/Images/thumbnail-generator.png';
    }

    var Name = data[0].ResponderName;
    if (Name != "" && Name != 'null' && Name != null && Name.length > 15) {
        Name = Name.substring(0, 10) + ' ...';
    }
    var method = managecampaign_Gettype(data[0].SendMethod);
    var href = '/AdvancedCampaign/Create?eId=' + CampaignId + '&bId=' + data[0].BaseId + '&Day=' + i + '';

    var html = '<div class="li-day btn li-day-has-event" ondragleave="leaveDrop(event)"  ondragover="allowDrop(event)" data-date="' + i + '">' +
                      '<span data-day="' + i + '" class="span-add-day" ><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></span>' +
                                   '<span class="span-number-day pull-left">' + i + '</span>' +
                                   '<div id="respondername-single-cell" class="name-single-cell">'+method + Name + '</div>' +
                                       '<a ondragstart="dragStart(event)" ondrag="dragging(event)" draggable="true" id="dragtarget" class="number-day-create-event-has" data-name="' + data[0].ResponderName + '"  data-img="' + image + '" data-type="' + data[0].BaseType + '" data-date="' + i + '" data-baseId="' + data[0].BaseId + '" data-publish="' + data[0].Publish + '" style="text-align:center;">' +
                                            '<div class="div-image-event">' +
                                               '<div class="div-hover-tool">' +
                                               '<img  onclick="managecampaign_Edit(\'' + href + '\');" data-href="/AdvancedCampaign/Create?eId=' + CampaignId + '&bId=' + data[0].BaseId + '&Day=' + i + '" id="snap-shot-autoresponder-event" class="thumnail-list-email"  src="' + image + '" style="width:70px; height:90px;" />' +
                                         '</div>' +
                                               '</div>' +
                                          '</a>' +
                                      '</div>';
    return html;


}

function managecampaign_ShowGridClick(data, message, permission) {
    var html = '';
    var date = new Date();
    var timezone = Math.abs(date.getTimezoneOffset() / 60);
    $('.table-manage-autoresponders-click').children().remove()
    html += '<table id="table-advance-event-click" class="view table table-striped">' +
              ' <tbody>';
    $.each(data, function (index, o) {
        
        var basetype = "";
        var numbercontact = "";
        var image = "";
        var currentTimeZoneOffsetInHours = timezone + parseInt(o.Timer);
        if (currentTimeZoneOffsetInHours >= 24) {
            var timer = currentTimeZoneOffsetInHours - 24;
        }
        else {
            var timer = currentTimeZoneOffsetInHours;
        }

        var timetype = "";
        if (o.SendTimeType == 1) {
            timetype = message.SameSignUp
        }
        if (o.SendTimeType == 2) {
            if(parseInt(o.Timer)>0)
                timetype = message.WithDelayOf + ' ' + message.Affter + ' ' + Math.abs(o.Timer) + ' ' + message.Hours;
            else
                timetype = message.WithDelayOf + ' ' + message.Before + ' ' + Math.abs( o.Timer) + ' ' + message.Hours;
        }
        if (o.SendTimeType == 3) {

            timetype = message.ExaclyAt + ' ' + timer + ' ' + message.Hours;
        }

        if (o.BaseType != 1) //timebase
        {
            if (o.BaseType == 3) //click
            {
                
                basetype += '<div><p>' + message.Clickbase + '</p>' +
                    '<div class="col-md-3 nopading text-left"><i style="font-size:50px;padding-top:20px;padding-left:4px;color:#cccccc;"  class="fa fa-mouse-pointer"></i></div>' +
                    '<div class="col-md-9">' + timetype + '</div>' +
                    '</div>';
                numbercontact += '<div><p>' + message.HasSend + '</p>' +
                  '<div class="col-md-3 nopading text-left" style="font-size:20px"><i style="font-size:20px;padding-left:4px;color:#cccccc;"  class="fa fa-user"></i>' + o.TotalSentNumber + '</div>' +

                  '</div>';
            }
            else if (o.BaseType == 2)//open
            {
                
                basetype += '<div><p>' + message.Openbase + '</p>' +
                    '<div class="col-md-3 nopading text-left"><i style="font-size:50px;padding-top:20px;padding-left:4px;color:#cccccc;"  class="icon-envelope-open"></i></div>' +
                    '<div class="col-md-9">' + timetype + '</div>' +
                    '</div>';
                numbercontact += '<div><p>' + message.HasSend + '</p>' +
                   '<div class="col-md-3 nopading text-left" style="font-size:20px"><i style="font-size:20px;padding-left:4px;color:#cccccc;"  class="fa fa-user"></i> ' + o.TotalSentNumber + '</div>' +

                   '</div>';
            }
            
            else if (o.BaseType == 4) //ngày đặt biệt
            {
                
                basetype += '<div><p>' + message.Specialday + '</p>' +
                        '<div class="col-md-3 nopading text-left"><i style="font-size:50px;padding-top:20px;padding-left:4px;color:#cccccc;"  class="fa fa-calendar"></i></div>' +
                        '<div class="col-md-9">' + timetype + '</div>' +
                        '</div>';
                    numbercontact += '<div><p>' + message.HasSend + '</p>' +
                        '<div class="col-md-3 nopading text-left" style="font-size:20px"><i style="font-size:20px;padding-left:4px;color:#cccccc;"  class="fa fa-user"></i> ' + o.TotalSentNumber + '</div>' +
                        '</div>';

               
            }

            //else if (o.BaseType == 5) //multiinput
            //{
                
            //    basetype += '<div><p>' + message.MultiInput + '</p>' +
            //            '<div class="col-md-3 nopading text-left"><i style="font-size:50px;padding-top:20px;padding-left:4px;color:#cccccc;"  class="fa fa-refresh"></i></div>' +
            //            '<div class="col-md-9">' + timetype + '</div>' +
            //            '</div>';
            //        numbercontact += '<div><p>' + message.HasSend + '</p>' +
            //            '<div class="col-md-3 nopading text-left" style="font-size:20px"><i style="font-size:20px;padding-left:4px;color:#cccccc;"  class="fa fa-user"></i> ' + o.TotalSentNumber + '</div>' +
            //            '</div>';


            //}
                
            if (o.Publish) {
                //var publish = ' <label class="switch-publish-event" style="margin-bottom: -4px;"><input type="checkbox" id="input-switch-publish" data-uid="' + o.BaseId + '" data-day="' + o.BaseDay + '" checked><div class="slider-event round-event"></div></label>';
                var status = '<span id="state-publish-event" data-uid="' + o.BaseId + '">' + message.PublishOn + '</span>';
                var publish = '<div class="onoffswitch">' +
                '<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="input-switch-publish" data-type="' + o.BaseType + '" data-uid="' + o.BaseId + '" data-day="' + o.BaseDay + '" checked>' +
                '<label class="onoffswitch-label" for="input-switch-publish">' +
                    '<span class="onoffswitch-inner"></span>' +
                    '<span class="onoffswitch-switch"></span>' +
                '</label>' +
              '</div>';
            }
            else {
                var status = '<span id="state-publish-event" data-uid="' + o.BaseId + '">' + message.PublishOff + '</span>';
                //var publish = ' <label class="switch-publish-event" style="margin-bottom: -4px;"><input type="checkbox" id="input-switch-publish" data-uid="' + o.BaseId + '" data-day="' + o.BaseDay + '" ><div class="slider-event round-event"></div></label>'
                var publish = '<div class="onoffswitch">' +
                '<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="input-switch-publish" data-uid="' + o.BaseId + '" data-day="' + o.BaseDay + '">' +
                '<label class="onoffswitch-label" for="input-switch-publish">' +
                    '<span class="onoffswitch-inner"></span>' +
                    '<span class="onoffswitch-switch"></span>' +
                '</label>' +
              '</div>';
            }


            if (o.SnapShotUrl != null && o.SnapShotUrl != "") {
                image = o.SnapShotUrl;
            }
            else image = '/Content/Images/thumbnail-generator.png';

            var method = managecampaign_Gettype(o.SendMethod);

            var toolpublish = ' <li> <a href="javascript:;"><i class="fa fa-globe"></i> Publish <span class="pull-right">' + publish + '</span></a></li>';
            var tooledit = ' <li> <a id="edit"  data-bId="' + o.BaseId + '" data-day="' + o.BaseDay + '" href="javascript:;"><i class="fa fa-edit"></i> ' + message.Edit + ' </a> </li>';
            var toolstatistic = ' <li> <a id="statistics" data-eid="' + o.CampaignId + '" data-baseid="' + o.BaseId + '" href="javascript:;"><i class="fa fa-pie-chart"></i> ' + message.Statistics + ' </a> </li>';
            var toolview = ' <li> <a id="viewevent" data-bid="' + o.BaseId + '" href="javascript:;"><i class="fa fa-eye"></i> ' + message.View + ' </a> </li>';
            var tooltest = '<li> <a id="testevent" data-bid="' + o.BaseId + '" href="javascript:;"><i class="fa fa-envelope"></i> ' + message.Test + ' </a> </li>';
            var tooldel = '<li>  <a id="delete" href="javascript:;" data-uid="' + o.BaseId + '"><i class="fa fa-remove"></i> ' + message.Delete + ' </a></li>';
            var totaltool = '';
            if (permission.IsParent) {
                totaltool = toolpublish + tooledit + toolstatistic + toolview + tooltest + tooldel;
            }
            else {
                if (permission.AutoCampaign == 2) {
                    if (permission.Statistic == 1) {
                        totaltool = toolpublish + tooledit + toolstatistic + toolview + tooltest + tooldel;
                    } else {
                        totaltool = toolpublish + tooledit + toolview + tooltest + tooldel;
                    }
                }
                if (permission.AutoCampaign == 1) {
                    if (permission.Statistic == 1) {
                        totaltool =  toolstatistic + toolview;
                    } else {
                        totaltool =  toolview;
                    }
                }
            }

            html +=
                   '<tr>' +
                     '<td class="col-md-5">' +
                 '<div class="div-parent-name-campaign">' +
                 '<div class="pull-left div-preview-snapshot div-preview-snapshot">' +
                   '<span class="helper-thumnail-list-email" style="height: 90px;">' +
                   '</span><img class="thumnail-list-email" src="' + image + '" style="cursor: zoom-in;width:100%;max-height: 85px;"></div>' +
                   '<div class="div-name-campaign" style="padding:5px;">' +
                   '<a id="editsetting"><p class="custom-p-table custom-name-on-table" id="tablecampaigname" data-bid="' + o.BaseId + '" data-day="' + o.BaseDay + '">' + o.ResponderName + '</p></a>' +
                   '<p class="custom-p-table" style="color:#9E9E9E">' + method + '<span>' + message.Status + ': ' + status + '</span></p></div></div>' +
                   //'<div class="pull-left" style="width:66px;height:102px;"><img class="thumnail-list-email" src="' + image + '"></div>' +
                   //'<div style="padding:15px;font-size:15px;" class="pull-left"><span>' + o.ResponderName + '</span><br/><span>' + message.Status + ': ' + status + '</span></div>' +
                '</td>' +
                   '<td class="col-md-5" style="vertical-align: top;">' +
                    basetype +
                 '</td>' +
                 //'<td class="col-md-2">' +
                 //  '<div>' + numbercontact + '</div>' +
                 //'</td>' +

                   ' <td style="vertical-align: top;" class="text-center" class="col-md-2">' +
                        '<div class="btn-group"><a id="btn-manage-togle" class="btn btn-xs green-jungle-stripe default dropdown-toggle" data-toggle="dropdown" href="javascript:;" aria-expanded="false"> ' + message.Tool + ' <i class="fa fa-angle-down"></i> </a> ' +
                        '<ul class="dropdown-menu pull-right" > ' +

                          totaltool+

                         '</ul> </div>' +
                    '</td>' +
                   ' </tr>';



        }
        });

    html += ' </tbody>' +
           ' </table>';
    $('.table-manage-autoresponders-click').append(html);
    
};


function managecampaign_ShowTable(data, message, permission) {
    $('.table-manage-autoresponders').children().remove()
    var html = '<table id="table-advance-event" class="view table table-striped">' +
              ' <tbody>';
    $.each(data, function (index, o) {

        var basetype = "";
        var numbercontact = "";
        var image = "";
        var timezone = new Date();
        var currentTimeZoneOffsetInHours = Math.abs(timezone.getTimezoneOffset() / 60) + parseInt(o.Timer);
        if (currentTimeZoneOffsetInHours >= 24) {
            var timerlocal = currentTimeZoneOffsetInHours - 24;
        }
        else {
            var timerlocal = currentTimeZoneOffsetInHours;
        }
        var timetype = "";
        if (o.SendTimeType == 1) {
            timetype = message.SameSignUp
        }
        if (o.SendTimeType == 2) {
            if(parseInt(o.Timer)>0)
                timetype = message.WithDelayOf + " " + message.Affter + " " + Math.abs(o.Timer) + "h";
            else
                timetype = message.WithDelayOf + " " + message.Before + " " + Math.abs(o.Timer) + "h";
        }
        if (o.SendTimeType == 3) {
            timetype = message.ExaclyAt + " " + timerlocal + "h";
        }
        if (o.BaseType == 1) //timebase
        {
            
            basetype += '<div><p>' + message.Timebase + '</p>' +
                '<div class="col-md-3" id="table-event-baseday"><input type="text" class="form-control" style="width:45px;text-align: center;font-weight:bold;padding:0" value=' + o.BaseDay + '></div>' +
                '<div class="col-md-9">' + timetype + '</div>' +
                '</div>';
            numbercontact += '<div><p>' + message.NumberOfContact+ '</p>' +
               '<div class="col-md-3 nopading text-left" style="font-size:20px"><i style="font-size:20px;padding-left:4px;color:#cccccc;"  class="fa fa-user"></i> ' + o.TotalSentNumber + '</div>' +
              
               '</div>';
        }
        if (o.BaseType == 2) { //open
            
            basetype += '<div><p>' + message.Openbase + '</p>' +
                '<div class="col-md-3 nopading text-left"><i style="font-size:50px;padding-top:20px;padding-left:4px;color:#cccccc;"  class="icon-envelope-open"></i></div>' +
                '<div class="col-md-9">' + timetype + '</div>' +
                '</div>';
            numbercontact += '<div><p>' + message.HasSend + '</p>' +
               '<div class="col-md-3 nopading text-left" style="font-size:20px"><i style="font-size:20px;padding-left:4px;color:#cccccc;"  class="fa fa-user"></i> ' + o.TotalSentNumber + '</div>' +
               
               '</div>';
        }
        if (o.BaseType == 3) //click
        {
            
            basetype += '<div><p>' + message.Clickbase + '</p>' +
                '<div class="col-md-3 nopading text-left"><i style="font-size:50px;padding-top:20px;padding-left:4px;color:#cccccc;"  class="fa fa-mouse-pointer"></i></div>' +
                '<div class="col-md-9">' + timetype + '</div>' +
                '</div>';
            numbercontact += '<div><p>' + message.HasSend + '</p>' +
                '<div class="col-md-3 nopading text-left" style="font-size:20px"><i style="font-size:20px;padding-left:4px;color:#cccccc;"  class="fa fa-user"></i> ' + o.TotalSentNumber + '</div>' +
                '</div>';
        }

      
        if (o.BaseType == 4) { //special day

            basetype += '<div><p>' + message.Specialday + '</p>' +
                        '<div class="col-md-3 nopading text-left"><i style="font-size:50px;padding-top:20px;padding-left:4px;color:#cccccc;"  class="fa fa-calendar"></i></div>' +
                        '<div class="col-md-9">' + timetype + '</div>' +
                        '</div>';
            numbercontact += '<div><p>' + message.HasSend + '</p>' +
                '<div class="col-md-3 nopading text-left" style="font-size:20px"><i style="font-size:20px;padding-left:4px;color:#cccccc;"  class="fa fa-user"></i> ' + o.TotalSentNumber + '</div>' +
                '</div>';
        }
        //else if (o.BaseType == 5) //multiinput
        //{

        //    basetype += '<div><p>' + message.MultiInput + '</p>' +
        //            '<div class="col-md-3 nopading text-left"><i style="font-size:50px;padding-top:20px;padding-left:4px;color:#cccccc;"  class="fa fa-refresh"></i></div>' +
        //            '<div class="col-md-9">' + timetype + '</div>' +
        //            '</div>';
        //    numbercontact += '<div><p>' + message.HasSend + '</p>' +
        //        '<div class="col-md-3 nopading text-left" style="font-size:20px"><i style="font-size:20px;padding-left:4px;color:#cccccc;"  class="fa fa-user"></i> ' + o.TotalSentNumber + '</div>' +
        //        '</div>';


        //}


        if (o.Publish)
        {
            var status = '<span id="state-publish-event" data-uid="'+o.BaseId+'">'+message.PublishOn+'</span>';
           // var publish = ' <label class="switch-publish-event" style="margin-bottom: -4px;"><input type="checkbox" id="input-switch-publish" data-uid="' + o.BaseId + '"  data-day="' + o.BaseDay + '" checked><div class="slider-event round-event"></div></label>';
            var publish = '<div class="onoffswitch">' +
                '<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="input-switch-publish" data-type="' + o.BaseType + '" data-uid="' + o.BaseId + '" data-day="' + o.BaseDay + '" checked>' +
                '<label class="onoffswitch-label" for="input-switch-publish">' +
                    '<span class="onoffswitch-inner"></span>' +
                    '<span class="onoffswitch-switch"></span>' +
                '</label>' +
              '</div>';
        }
        else
        {
            var status = '<span id="state-publish-event" data-uid="' + o.BaseId + '">'+message.PublishOff+'</span>';
            //var publish = ' <label class="switch-publish-event" style="margin-bottom: -4px;"><input type="checkbox" id="input-switch-publish" data-uid="' + o.BaseId + '" data-day="' + o.BaseDay + '"><div class="slider-event round-event"></div></label>'
            var publish = '<div class="onoffswitch">' +
                '<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="input-switch-publish" data-publish="'+o.Publish+'" data-uid="' + o.BaseId + '" data-day="' + o.BaseDay + '">' +
                '<label class="onoffswitch-label" for="input-switch-publish">' +
                    '<span class="onoffswitch-inner"></span>' +
                    '<span class="onoffswitch-switch"></span>' +
                '</label>' +
              '</div>';
        }

        if (o.SnapShotUrl != null && o.SnapShotUrl != "") {
            image = o.SnapShotUrl;
        }
        else image = '/Content/Images/thumbnail-generator.png';
        if (o.ResponderName == '' || o.ResponderName == 'null' || o.ResponderName == null)
        {
            o.ResponderName = 'Chưa cập nhật';
        }
        var method = managecampaign_Gettype(o.SendMethod);

        var toolpublish = ' <li> <a href="javascript:;"><i class="fa fa-globe"></i> Publish <span class="pull-right">' + publish + '</span></a></li>';
        var tooledit = ' <li> <a id="edit" data-bId="' + o.BaseId + '" data-day="' + o.BaseDay + '" href="javascript:;"><i class="fa fa-edit"></i> ' + message.Edit + ' </a> </li>';
        var toolstatistic = ' <li> <a id="statistics" data-eid="' + o.CampaignId + '" data-baseid="' + o.BaseId + '" href="javascript:;"><i class="fa fa-pie-chart"></i> ' + message.Statistics + ' </a> </li>';
        var toolview = ' <li> <a id="viewevent" data-bid="' + o.BaseId + '" href="javascript:;"><i class="fa fa-eye"></i> ' + message.View + ' </a> </li>';
        var tooltest = '<li> <a id="testevent" data-bid="' + o.BaseId + '" href="javascript:;"><i class="fa fa-envelope"></i> ' + message.Test + ' </a> </li>';
        var tooldel = '<li>  <a id="delete" href="javascript:;" data-uid="' + o.BaseId + '"><i class="fa fa-remove"></i> ' + message.Delete + ' </a></li>';
        var totaltool = '';
        if (permission.IsParent) {
            totaltool = toolpublish + tooledit + toolstatistic + toolview + tooltest + tooldel;
        }
        else {
            if (permission.AutoCampaign == 2) {
                if (permission.Statistic == 1) {
                    totaltool = toolpublish + tooledit + toolstatistic + toolview + tooltest + tooldel;
                } else {
                    totaltool = toolpublish + tooledit + toolview + tooltest + tooldel;
                }
            }
            if (permission.AutoCampaign == 1) {
                if (permission.Statistic == 1) {
                    totaltool =  toolstatistic + toolview;
                } else {
                    totaltool =  toolview;
                }
            }
        }

        html +=
               '<tr>' +
                 '<td class="col-md-5">' +
                 '<div class="div-parent-name-campaign">'+
                 '<div class="pull-left div-preview-snapshot div-preview-snapshot-draft">'+
                   '<span class="helper-thumnail-list-email" style="height: 90px;">'+
                   '</span><img class="thumnail-list-email" src="' + image + '" style="cursor: zoom-in;width:100%;max-height: 85px;"></div>' +
                   '<div class="div-name-campaign" style="padding:5px;">' +
                   '<a id="editsetting"><p class="custom-p-table custom-name-on-table" id="tablecampaigname" data-bid="' + o.BaseId + '" data-day="' + o.BaseDay + '">' + o.ResponderName + '</p></a>' +
                   '<p class="custom-p-table" style="color:#9E9E9E"><span>' + method + message.Status + ': ' + status + '</span></p></div></div>' +
                   //'<div class="pull-left" style="width:66px;height:102px;"><img class="thumnail-list-email" src="' + image + '"></div>' +
                   //'<div style="padding:15px;font-size:15px;" class="pull-left"><span>' + o.ResponderName + '</span><br/><span>' + message.Status + ': ' + status + '</span></div>' +
                '</td>' +
               '<td class="col-md-5" style="vertical-align: top;">' +
                basetype +
             '</td>' +
             //'<td class="col-md-2">' +
             //  '<div>' + numbercontact + '</div>' +
             //'</td>' +

               ' <td style="vertical-align: top;" class="text-center" class="col-md-2">' +
                    '<div class="btn-group"><a id="btn-manage-togle" class="btn btn-xs green-jungle-stripe default dropdown-toggle" data-toggle="dropdown" href="javascript:;" aria-expanded="false"> '+message.Tool+' <i class="fa fa-angle-down"></i> </a> ' +
                    '<ul class="dropdown-menu pull-right" > ' +
                       totaltool+
                     '</ul> </div>' +
                '</td>' +
               ' </tr>';


    });

  

    html += ' </tbody>' +
           ' </table>';
    $('.table-advance-event td').css('border', 'none !important');
    $('.table-manage-autoresponders').append(html);
    var height = $(window).height() - $('.page-header ').outerHeight() - $('.page-bar').outerHeight() - $('.portlet-title').outerHeight() - $('.page-footer').outerHeight()-30;
    $('#table-manage-autoresponders').slimScroll({
        height: height+'px',
       
    });

    $('.table-manage-autoresponders').parent('.slimScrollDiv').hide();
};


function managecampaign_GetListEventGird(apiDomain, access_token, CampaignId, message, permission) {
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/AutoCampaign/GetEventList?CampaignId=' + CampaignId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            $('#divLoading').hide();
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    managecampaign_ShowGird(data, CampaignId, message); //tạo gird
                    managecampaign_ShowGridClick(data, message, permission); //grid clickbase
                    managecampaign_ShowHide();//ẩn bớts
                   
                    break;
                default:
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification("error", message.HaveError);
        }
    })
}

function managecampaign_GetListEventTable(apiDomain, access_token, CampaignId, message, permission)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain+ 'api/AutoCampaign/GetEventList?CampaignId=' + CampaignId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success:function(r)
        {
            $('#divLoading').hide();
            switch(parseInt(r.ErrorCode))
            {
                case 0:
                    var data = r.Data;
                    managecampaign_ShowTable(data, message, permission) //tạo table
                    break;
                default:
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification("error", message.HaveError);
        }
    })
}


function managecampaign_Changeview(apiDomain, access_token, CampaignId, message, permission)
{
    $('#action-advance-grid').on('click', function () {
        if ($('.advance-campaign-show-grid').hasClass('hide')) {
            $('.advance-campaign-show-grid').removeClass('hide');
        }

        if ($('#advance-campaign-show-grid-click').hasClass('hide')) {
            $('#advance-campaign-show-grid-click').removeClass('hide');
        }

        if(!$('#advance-campaign-show-list').hasClass('hide'))
        {
            $('#advance-campaign-show-list').addClass('hide');
            $('#advance-campaign-show-list').parent('.slimScrollDiv').hide();
        }



        if(!$('#action-advance-grid-i').hasClass('font-blue'))
        {
            $('#action-advance-grid-i').addClass('font-blue');
            $('#action-advance-grid-i').removeClass('font-grey-gallery')
        }
        if ($('#action-advance-table-i').hasClass('font-blue')) {
            $('#action-advance-table-i').removeClass('font-blue');
            $('#action-advance-table-i').addClass('font-grey-gallery')
        }

        managecampaign_GetListEventGird(apiDomain, access_token, CampaignId, message, permission);
    });

    $('#action-advance-table').on('click', function () {
       
        if (!$('.advance-campaign-show-grid').hasClass('hide')) {
            $('.advance-campaign-show-grid').addClass('hide');
        }
        if (!$('#advance-campaign-show-grid-click').hasClass('hide')) {
            $('#advance-campaign-show-grid-click').addClass('hide');
        }
        if ($('#advance-campaign-show-list').hasClass('hide')) {
            $('#advance-campaign-show-list').removeClass('hide');
            $('#advance-campaign-show-list').parent('.slimScrollDiv').show();
        }

        if ($('#action-advance-grid-i').hasClass('font-blue')) {
            $('#action-advance-grid-i').removeClass('font-blue');
            $('#action-advance-grid-i').addClass('font-grey-gallery');
        }
        if (!$('#action-advance-table-i').hasClass('font-blue')) {
            $('#action-advance-table-i').addClass('font-blue');
            $('#action-advance-table-i').removeClass('font-grey-gallery');
        }

        managecampaign_GetListEventTable(apiDomain, access_token, CampaignId, message, permission)
    });


}


function managecampaign_HoverTool_Gird(CampaignId,message,permission)
{
    //singleday
    $('.advance-campaign-show-grid').on('mouseenter', '.div-hover-tool', function () {

        $(this).children('#snap-shot-autoresponder-event').css('border', '2px solid #26C281');
        var BaseId = $(this).parent().parent().data('baseid');
        var Date = $(this).parent().parent().data('date');
        var Name = $(this).parent().parent().data('name');
        var BaseType = $(this).parent().parent().data('type');
        if (Name != "" && Name != "null" && Name != null && Name.length > 20) {
            Name = Name.substring(0, 20) + ' ...';
        }

        var Publish = $(this).parent().parent().attr('data-publish');
        if (Publish == true || Publish == 'true') {
            //var publish = ' <label class="switch-publish-event" style="margin-bottom: -4px;"><input type="checkbox" id="input-switch-publish" data-uid="' + BaseId + '" data-day="' + Date + '" checked><div class="slider-event round-event"></div></label>';
            var publish = '<div class="onoffswitch">' +
                '<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="input-switch-publish" data-type="' + BaseType + '" data-uid="' + BaseId + '" data-day="' + Date + '" checked>' +
                '<label class="onoffswitch-label" for="input-switch-publish">' +
                    '<span class="onoffswitch-inner"></span>' +
                    '<span class="onoffswitch-switch"></span>' +
                '</label>' +
              '</div>';
        }
        else {
            // var publish = ' <label class="switch-publish-event" style="margin-bottom: -4px;"><input type="checkbox" id="input-switch-publish" data-uid="' + BaseId + '"  data-day="' + Date + '"><div class="slider-event round-event"></div></label>'
            var publish = '<div class="onoffswitch">' +
                '<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="input-switch-publish"  data-type="' + BaseType + '" data-uid="' + BaseId + '" data-day="' + Date + '" >' +
                '<label class="onoffswitch-label" for="input-switch-publish">' +
                    '<span class="onoffswitch-inner"></span>' +
                    '<span class="onoffswitch-switch"></span>' +
                '</label>' +
              '</div>';
        }


        var toolpublish = ' <li> <a href="javascript:;"><i class="fa fa-globe"></i> Publish <span class="pull-right">' + publish + '</span></a></li>';
        var tooledit = ' <li> <a id="edit" href="/AdvancedCampaign/Create?eId=' + CampaignId + '&bId=' + BaseId + '&Day=' + Date + '"><i class="fa fa-edit"></i> ' + message.Edit + ' </a> </li>';
        var toolstatistic = ' <li> <a id="statistics" data-eid="' + CampaignId + '" data-baseid="' + BaseId + '" href="javascript:;"><i class="fa fa-pie-chart"></i> ' + message.Statistics + ' </a> </li>';
        var toolview = ' <li> <a id="viewevent" data-bid="' + BaseId + '" href="javascript:;"><i class="fa fa-eye"></i> ' + message.View + ' </a> </li>';
        var tooltest = '<li> <a id="testevent" data-bid="' + BaseId + '" href="javascript:;"><i class="fa fa-envelope"></i> ' + message.Test + ' </a> </li>';
        var tooldel = '<li>  <a id="delete" href="javascript:;" data-uid="' + BaseId + '"><i class="fa fa-remove"></i> ' + message.Delete + ' </a></li>';
        var totaltool = '';
        if (permission.IsParent) {
            totaltool = toolpublish + tooledit + toolstatistic + toolview + tooltest + tooldel;
        }
        else {
            if (permission.AutoCampaign == 2) {
                if (permission.Statistic == 1) {
                    totaltool = toolpublish + tooledit + toolstatistic + toolview + tooltest + tooldel;
                } else {
                    totaltool = toolpublish + tooledit  + toolview + tooltest + tooldel;
                }
            }
            if (permission.AutoCampaign == 1) {
                if (permission.Statistic == 1) {
                    totaltool =  toolstatistic + toolview ;
                } else {
                    totaltool =   toolview ;
                }
            }
        }

        var html = '<div class="tools-preview-event">' +
            '<div class="item-in">' +
            '<div class="col-md-12 nopading">' +
            '<div class="dropdown inline clearfix">' +
             '<ul class="dropdown-menu" style="min-width:100%;min-height:238px;"> ' +
                  ' <li> <a href="javascript:;"><span class="preview-event-name" title="' + $(this).parent().parent().data('name') + '">' + Name + '</span> </a></li>' +
                     totaltool+
                     '</ul>' +
        '</div>' +
          '</div>' +
          '</div>';

        $(this).append(html);
        $('.tooltips').tooltip();
        //vị trí của các hover;
        //  setInterval(getMousePosition, 100);
        if (mousePos) {
            if (mousePos.x <= $(window).width() / 2 && mousePos.y <= $(window).height() / 2) {
                $('.tools-preview-event').addClass('tools-preview-event-leftbottom'); //1:1
            }
            if (mousePos.x >= $(window).width() / 2 && mousePos.y >= $(window).height() / 2) {
                $('.tools-preview-event').addClass('tools-preview-event-righttop');  //2:2
            }
            if (mousePos.x >= $(window).width() / 2 && mousePos.y <= $(window).height() / 2) {
                $('.tools-preview-event').addClass('tools-preview-event-rightbottom '); //2:1
            }
            if (mousePos.x <= $(window).width() / 2 && mousePos.y >= $(window).height() / 2) {
                $('.tools-preview-event').addClass('tools-preview-event-lefttop');   //1:2
            }
        }
        else {
            $('.tools-preview-event').addClass('tools-preview-event-rightbottom ');     //2:1
        }

        $(this).parent().parent().parent().children('.span-add-day').hide();


    }).on('mouseleave', '.div-hover-tool', function () {
        $(this).children('#snap-shot-autoresponder-event').css('border', 'none');
        $(this).children('.tools-preview-event').remove();

        $(this).parent().parent().parent().children('.span-add-day').show();

    });

    //multiday
    $('.advance-campaign-show-grid').on('mouseenter', '.number-day-create-event-has.multipe-baseday-event-has', function () {

        $(this).children('#snap-shot-autoresponder-event').css('border', '2px solid #26C281');
        var BaseId = $(this).data('baseid');
        var Date = $(this).data('date');
        var Name = $(this).data('name');
        var BaseType = $(this).data('type');

        if (Name != "" && Name != "null" && Name != null && Name.length > 20) {
            Name = Name.substring(0, 20) + ' ...';
        }

        var Publish = $(this).attr('data-publish');
        if (Publish == true || Publish == 'true') {
            //var publish = ' <label class="switch-publish-event" style="margin-bottom: -4px;"><input type="checkbox" id="input-switch-publish" data-uid="' + BaseId + '" data-day="' + Date + '" checked><div class="slider-event round-event"></div></label>';
            var publish = '<div class="onoffswitch">' +
                '<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="input-switch-publish"  data-type="' + BaseType + '" data-uid="' + BaseId + '" data-day="' + Date + '" checked>' +
                '<label class="onoffswitch-label" for="input-switch-publish">' +
                    '<span class="onoffswitch-inner"></span>' +
                    '<span class="onoffswitch-switch"></span>' +
                '</label>' +
              '</div>';
        }
        else {
            // var publish = ' <label class="switch-publish-event" style="margin-bottom: -4px;"><input type="checkbox" id="input-switch-publish" data-uid="' + BaseId + '"  data-day="' + Date + '"><div class="slider-event round-event"></div></label>'
            var publish = '<div class="onoffswitch">' +
                '<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="input-switch-publish"  data-type="' + BaseType + '" data-uid="' + BaseId + '" data-day="' + Date + '" >' +
                '<label class="onoffswitch-label" for="input-switch-publish">' +
                    '<span class="onoffswitch-inner"></span>' +
                    '<span class="onoffswitch-switch"></span>' +
                '</label>' +
              '</div>';
        }
        var toolpublish = ' <li> <a href="javascript:;"><i class="fa fa-globe"></i> Publish <span class="pull-right">' + publish + '</span></a></li>';
        var tooledit = ' <li> <a id="edit" href="/AdvancedCampaign/Create?eId=' + CampaignId + '&bId=' + BaseId + '&Day=' + Date + '"><i class="fa fa-edit"></i> ' + message.Edit + ' </a> </li>';
        var toolstatistic = ' <li> <a id="statistics" data-eid="' + CampaignId + '" data-baseid="' + BaseId + '" href="javascript:;"><i class="fa fa-pie-chart"></i> ' + message.Statistics + ' </a> </li>';
        var toolview = ' <li> <a id="viewevent" data-bid="' + BaseId + '" href="javascript:;"><i class="fa fa-eye"></i> ' + message.View + ' </a> </li>';
        var tooltest = '<li> <a id="testevent" data-bid="' + BaseId + '" href="javascript:;"><i class="fa fa-envelope"></i> ' + message.Test + ' </a> </li>';
        var tooldel = '<li>  <a id="delete" href="javascript:;" data-uid="' + BaseId + '"><i class="fa fa-remove"></i> ' + message.Delete + ' </a></li>';
        var totaltool = '';
        if (permission.IsParent) {
            totaltool = toolpublish + tooledit + toolstatistic + toolview + tooltest + tooldel;
        }
        else {
            if (permission.AutoCampaign == 2) {
                if (permission.Statistic == 1) {
                    totaltool = toolpublish + tooledit + toolstatistic + toolview + tooltest + tooldel;
                } else {
                    totaltool = toolpublish + tooledit + toolview + tooltest + tooldel;
                }
            }
            if (permission.AutoCampaign == 1) {
                if (permission.Statistic == 1) {
                    totaltool =  toolstatistic + toolview;
                } else {
                    totaltool =  toolview;
                }
            }
        }
        var html = '<div class="tools-preview-event">' +
            '<div class="item-in">' +
            '<div class="col-md-12 nopading">' +
            '<div class="dropdown inline clearfix">' +
             '<ul class="dropdown-menu" style="min-width:100%;min-height:238px;"> ' +
                  ' <li> <a href="javascript:;"><span class="preview-event-name" title="' + $(this).data('name') + '">' + Name + '</span> </a></li>' +
                  totaltool+
              '</ul>' +
        '</div>' +
          '</div>' +
          '</div>';

        $(this).append(html);
        $('.tooltips').tooltip();
        //vị trí của các hover;
        //  setInterval(getMousePosition, 100);
        if (mousePos) {
            if (mousePos.x <= $(window).width() / 2 && mousePos.y <= $(window).height() / 2) {
                $('.tools-preview-event').addClass('tools-preview-mulevent-leftbottom'); //1:1
            }
            if (mousePos.x >= $(window).width() / 2 && mousePos.y >= $(window).height() / 2) {
                $('.tools-preview-event').addClass('tools-preview-mulevent-righttop');  //2:2
            }
            if (mousePos.x >= $(window).width() / 2 && mousePos.y <= $(window).height() / 2) {
                $('.tools-preview-event').addClass('tools-preview-mulevent-rightbottom '); //2:1
            }
            if (mousePos.x <= $(window).width() / 2 && mousePos.y >= $(window).height() / 2) {
                $('.tools-preview-event').addClass('tools-preview-mulevent-lefttop');   //1:2
            }
        }
        else {
            $('.tools-preview-event').addClass('tools-preview-mulevent-rightbottom ');     //2:1
        }

        //ẩn nút tạo event
        $(this).parent().parent().children('.span-add-day').hide();

    }).on('mouseleave', '.number-day-create-event-has.multipe-baseday-event-has', function () {
        $(this).children('#snap-shot-autoresponder-event').css('border', 'none');
        $(this).children('.tools-preview-event').remove();
        $(this).parent().parent().children('.span-add-day').show();
    });
}

function managecampaign_HoverAddDay()
{
    $('.advance-campaign-show-grid').on('mouseenter', '.li-day', function () {
        $(this).children('.span-add-day').show();

    }).on('mouseleave', '.li-day', function () {
        $(this).children('.span-add-day').hide();
    });


    $('.advance-campaign-show-grid').on('mouseenter', '.multipe-baseday-event-has', function () {
        $(this).css('background', 'rgb(38, 194, 129)');

    }).on('mouseleave', '.multipe-baseday-event-has', function () {
        $(this).css('background', 'rgb(195, 195, 195)');
    });

}

function managecampaign_ClickManageButton()
{
    
   
    $('.table-manage-autoresponders').on('click', '#btn-manage-togle', function () {
       
        if (mousePos.y >= $(window).height() / 2) {
            $(this).parent().children('.dropdown-menu').addClass('bottom-up');
        }
    });
    $('.table-manage-autoresponders-click').on('click', '#btn-manage-togle', function () {
        if (mousePos.y >= $(window).height() / 2) {
            $(this).parent().children('.dropdown-menu').addClass('bottom-up');
        }
    });
}

function managecampaign_Edit(data) {
    window.location.href = data;
}

function managecampaign_EditOnTable(CampaignId)
{
    $('#advance-campaign-show-list, #advance-campaign-show-grid-click').on('click', '#edit', function () {
        var BaseId = $(this).data('bid');
        var Day = $(this).data('day');

        window.location.href = '/AdvancedCampaign/Create?eId=' + CampaignId + '&bId=' + BaseId + '&Day=' + Day;
    });

    $('.advance-campaign-show').on('click', '#tablecampaigname', function () {
        var BaseId = $(this).data('bid');
        var Day = $(this).data('day');

        window.location.href = '/AdvancedCampaign/Create?eId=' + CampaignId + '&bId=' + BaseId + '&Day=' + Day;
    });
}

function managecampaign_CreateOnGrid(CampaignId)
{
    $('.advance-campaign-show-grid').on('click', '.li-day-has-event', function (e) {
        if (e.target !== this)
            return;
        else {
            var Day = $(this).children('.span-number-day').text();
            window.location.href = '/AdvancedCampaign/Create?eId=' + CampaignId + '&Day=' + Day;
        }

    });

    $('.advance-campaign-show-grid').on('click', '.span-add-day', function (e) {
        
            var Day = $(this).data('day');
            window.location.href = '/AdvancedCampaign/Create?eId=' + CampaignId + '&Day=' + Day;
       

    });
}

function managecampaign_Publish(apiDomain, access_token,message)
{
    $('.advance-campaign-show').on('click', '.onoffswitch-label', function (e) {
        var Publish = $(this).parent().children('input').prop('checked');
        var day = $(this).parent().children('input').data('day');
        var BaseId = $(this).parent().children('input').data('uid');
        var BaseType = $(this).parent().children('input').data('type');
        
        var ResendType = 2;
        if (!Publish) {
            //nếu bật pubish mới kiểm tra 
            var checkgateway = managecampaign_DetectSMSGateWay(apiDomain, access_token, BaseId, message);
            var IsSMSGateWay = false;
            switch (checkgateway.ErrorCode) {
                case 0:
                    IsSMSGateWay = checkgateway.Data;
                    break;
                default:
                    IsSMSGateWay = false;
            }

            if (BaseType == 1) {
                if (parseInt(day) == 0) {
                    $('#confirm-publish-event').data('uid', BaseId);
                    $('#confirm-publish-event').data('publish', Publish);

                    $('#modal-warning-publish-event').modal('toggle');

                    if (IsSMSGateWay) {
                        $('#warning-publish-gateway').show();
                    } else {
                        $('#warning-publish-gateway').hide();
                    }

                }
                else {
                    if (IsSMSGateWay) {
                        $('#modelWarningSMSGateWay').modal('toggle');
                        $('#btn-waring-beforepublish').data('uid', BaseId);
                    } else {
                        managecampaign_ActionPublish(apiDomain, access_token, BaseId, Publish, ResendType, message);
                    }
                }
            }
            else
            {
                if (IsSMSGateWay) {
                    $('#modelWarningSMSGateWay').modal('toggle');
                    $('#btn-waring-beforepublish').data('uid', BaseId);
                } else {
                    managecampaign_ActionPublish(apiDomain, access_token, BaseId, Publish, ResendType, message);
                }
            }

        }
        else {
            
            managecampaign_ActionPublish(apiDomain, access_token, BaseId, Publish, ResendType, message)
        }
    })
}

function managecampaign_ConfirmWarningPublish(apiDomain,access_token,message)
{
    $('#btn-waring-beforepublish').on('click', function () {
        var BaseId= $('#btn-waring-beforepublish').data('uid');
        $('#modelWarningSMSGateWay').modal('toggle');
        var ResendType = 2; //mặc đinh cho ngày không phải là ngày 0
        var Publish=false //đang chưa publish
        managecampaign_ActionPublish(apiDomain, access_token, BaseId, Publish, ResendType, message);
    });
}

function managecampaign_ConfirmPublish(apiDomain,access_token,message)
{
    $('#confirm-publish-event-unsent').on('click', function () {

        var BaseId = $('#confirm-publish-event').data('uid');
        var Publish = $('#confirm-publish-event').data('publish');
        var ResendType = 2;
        managecampaign_TypePublish(apiDomain, access_token, BaseId, Publish, ResendType, message)
    });

    $('#confirm-publish-event-all').on('click', function () {

        var BaseId = $('#confirm-publish-event').data('uid');
        var Publish = $('#confirm-publish-event').data('publish');
        var ResendType = 1;
        managecampaign_TypePublish(apiDomain, access_token, BaseId, Publish, ResendType, message)
    })

    $('#confirm-publish-event-none').on('click', function () {

        var BaseId = $('#confirm-publish-event').data('uid');
        var Publish = $('#confirm-publish-event').data('publish');
        var ResendType = 0;
        managecampaign_TypePublish(apiDomain, access_token, BaseId, Publish, ResendType, message);
    })

}

function managecampaign_TypePublish(apiDomain, access_token, BaseId, Publish, ResendType, message) {
    $('#modal-warning-publish-event').modal('toggle');
    managecampaign_ActionPublish(apiDomain, access_token, BaseId, Publish, ResendType, message);
    $('#confirm-publish-event').data('uid', '');
    $('#confirm-publish-event').data('publish', '');
}

function managecampaign_ActionPublish(apiDomain, access_token, BaseId, Publish, ResendType, message)
{
    var data = {
        BaseId: BaseId,
        Publish: !Publish,
        ResendType: ResendType,
    }
    //$('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/AutoCampaign/UpdateEventPublish',
        type: 'post',
        contentType: 'application/json; charset=utf-8',
        data:JSON.stringify(data),
        beforeSend:function(request)
        {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch(parseInt(r.ErrorCode))
            {
                case 0:
                  
                    $('.advance-campaign-show').find('input:checkbox[data-uid="' + BaseId + '"]').attr('data-publish', !Publish);
                    $('.advance-campaign-show').find('[data-baseid="' + BaseId + '"]').attr('data-publish', !Publish);
             

                    if (!Publish == true) {
                        $('.advance-campaign-show').find('input:checkbox[data-uid="' + BaseId + '"]').prop('checked', true);
                        var text = message.PublishOn;
                        custom_shownotification('success', message.UpdateSuccess);
                    }
                    else {
                        $('.advance-campaign-show').find('input:checkbox[data-uid="' + BaseId + '"]').prop('checked', false);
                        var text = message.PublishOff;
                        custom_shownotification('success', message.UpdateSuccess);
                    }
                   
                    $('.advance-campaign-show').find('#state-publish-event[data-uid="' + BaseId + '"]').text(text);

                  //  $('#divLoading').hide();
                    break;
                default:
                  //  $('#divLoading').hide();
                    custom_shownotification('error', message.HaveError);
                    break;

            }
        },
        error:function(x,s,e)
        {
           // $('#divLoading').hide();
            custom_shownotification('error', message.HaveError);
        }
    })
}



function managecampaign_ModalDelete()
{
    //delete on grid
    $('.advance-campaign-show-grid').on('click', '.number-day-create-event-has #delete', function () {
        var BaseId = $(this).data('uid');
        $('#confirm-delete-event').data('uid', BaseId);
        $('#confirm-delete-event').data('type', 'grid');
        $('#modal-warning-delete-event').modal('toggle');
    });


    $('.advance-campaign-show-grid').on('click', '.table-manage-autoresponders-click #delete', function () {
        var BaseId = $(this).data('uid');
        $('#confirm-delete-event').data('uid', BaseId);
        $('#confirm-delete-event').data('type', 'grid');
        $('#modal-warning-delete-event').modal('toggle');
    });

    //delete on table
    $('#advance-campaign-show-list').on('click', '#delete', function () {
        var BaseId = $(this).data('uid');
        $('#confirm-delete-event').data('uid', BaseId);
        $('#confirm-delete-event').data('type', 'table');
        $('#modal-warning-delete-event').modal('toggle');
    })
}

function managecampaign_ConfirmDelete(apiDomain, access_token, CampaignId, message, permission)
{
    $('#confirm-delete-event').on('click', function () {
        var BaseId = $(this).data('uid');
        var TypeView = $(this).data('type');
        $(this).data('uid','');
        $('#modal-warning-delete-event').modal('toggle');
        managecampaign_Delete(apiDomain, access_token, CampaignId, BaseId, TypeView, message, permission)
    })

    
}

function managecampaign_Delete(apiDomain, access_token, CampaignId, BaseId, TypeView, message, permission) {
    $.ajax({
        url: apiDomain + 'api/AutoCampaign/DeleteEvent?BaseId=' + BaseId,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    if (TypeView == 'grid') {
                        managecampaign_GetListEventGird(apiDomain, access_token, CampaignId, message, permission);
                    }
                    if (TypeView == 'table') {
                        managecampaign_GetListEventTable(apiDomain, access_token, CampaignId, message, permission);
                    }
                    custom_shownotification('success', message.DeleteSuccess);
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }

        }, error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
        }

    });
}

function managecampaign_View()
{
    $('.advance-campaign-show').on('click', '#viewevent', function () {
        var BaseId = $(this).data('bid');
        window.open('/view/Event?bId=' + BaseId, '_blank');
    });
}

function managecampaign_ModalSendTest(apiDomain, access_token, message)
{
    $('.advance-campaign-show').on('click', '#testevent', function () {
        var BaseId = $(this).data('bid');

        managecampaign_SendTest(apiDomain, access_token, BaseId, message);

    });
}

function managecampaign_SendTest(apiDomain, access_token, BaseId, message) {

    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/AutoCampaign/GetEventInfo?BaseId=' + BaseId,
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    switch (data.SendMethod) {

                        case 1:
                            custom_shownotification('error', message.OnlyEmail);
                            break;
                        case 0:
                        case 2:
                            $('#modelSendTest').modal('toggle');
                            var NewsletterId = data.NewsletterId;
                        
                            $('#btnConfirm-Send-Test').data('bid', NewsletterId);
                         
                            break;

                    }
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
            $('#divLoading').hide();
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
            $('#divLoading').hide();
        }
    })

}

function managecampaign_SendTestEmail(apiDomain, access_token, message) {
    $('#btnConfirm-Send-Test').on('click', function () {
        var NewsletterId= $('#btnConfirm-Send-Test').data('bid' );
        var r = managecampaign_NewLetterInfo(apiDomain, access_token, NewsletterId, message);
        switch (r.ErrorCode) {
            case 0:
                var data = r.Data;
                if (data.HTML != '') {
                    var data = {
                        HTML: data.HTML,
                        ToEmail: $('#email-send-test-event').val(),
                        MessageName: 'Gửi thử - ' + data.MessageName,
                        Subject: data.Subject,
                        FromName: data.FromName,
                        FromEmail: data.FromEmail,
                        ReplyToEmail: data.ReplyToEmail,
                        EmbedMailUrl: data.EmbedMailUrl,
                        ShowPermissionReminder: data.ShowPermissionRemider,
                        ShowUnsubscribeButton: data.ShowUnsubscribeButton,
                        Facebook: data.Facebook,
                        Twitter: data.Twitter,
                    }


                    $.ajax({
                        url: apiDomain + 'api/EmailSend/Send',
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify(data),
                        beforeSend: function (request) {
                            request.setRequestHeader("Authorization", "Bearer " + access_token);
                        },
                        success: function (r) {
                            //loading
                            $('#divLoading').hide();
                            switch (parseInt(r.ErrorCode)) {
                                case 0:
                                    custom_shownotification('success', message.SentSuccess);
                                    $('#email-send-test-event').val('');
                                    $('#modelSendTest').modal('toggle');
                                    userstats_LoadUserInfo(apiDomain, access_token) //cập nhập lại số tiền còn sau đã gửi
                                    userstats_LoadUsed(apiDomain, access_token)//cập nhập lại số email đã gửi
                                    break;
                                default:
                                    custom_shownotification('error', message.HaveError);
                            }


                        },
                        error: function (x, e, s) {
                            //loading
                            $('#divLoading').hide();
                            custom_shownotification('error', message.HaveError);

                        }
                    });
                }
                else {
                    custom_shownotification('error', message.EmailNoContent);
                }

        }
    });
}

function managecampaign_NewLetterInfo(apiDomain, access_token, NewsletterId, message) {
    if (NewsletterId) {
        var html = $.ajax({
            url: apiDomain + 'api/newsletter/GetInfo?NewsletterId=' + NewsletterId,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            async: false,

        }).responseJSON;

        return html;
    }
    else {
        var html = [];
        html.ErrorCode = 104;
        return html;
    }

}


function managecampaign_Statistics()
{
    $('.advance-campaign-show').on('click', '#statistics', function () {
        var CampaignId = $(this).data('eid');
        var BaseId = $(this).data('baseid');
        window.location.href = '/statistics/timebased?eId=' + CampaignId + '&bId=' + BaseId;
    });
}


function managecamapaign_LoadMoreOnDay() {
    $('.advance-campaign-show-grid').on('click', '#load-more-base-on-day', function () {
        $(this).parent().children('#container-event-day').addClass('container-event-day');
        var height = $(this).parent().children('#container-event-day').children('.multipe-baseday-event-has').length * 22 + 110;
        $(this).hide();
        $(this).parent().children('#container-event-day').css('height', height);
    });
}

function managecampaign_LoadMoreOndayHover() {
    $('.advance-campaign-show-grid').on('mouseenter', '#load-more-base-on-day', function () {

        $(this).parent().children('.span-add-day').hide();
    }).on('mousout', '#load-more-base-on-day', function () {
        $(this).parent().children('.span-add-day').show();

    })
}

function managecamapaign_HideMoreOnday()
{
    $('body').on('click', function (e) {
        if ($(e.target).hasClass('load-more-base-on-day')) {

        }
        else
        {
            $('.li-day').find('.container-event-day').removeClass('container-event-day');
            $('.li-day').find('#load-more-base-on-day').show();
            $('.li-day #container-event-day').css('height', 110);
        }
    })
}

var mousePos;
document.onmousemove = handleMouseMove;
function handleMouseMove(event) {
    var dot, eventDoc, doc, body, pageX, pageY;

    event = event || window.event; // IE-ism

    // If pageX/Y aren't available and clientX/Y are,
    // calculate pageX/Y - logic taken from jQuery.
    // (This is to support old IE)
    if (event.pageX == null && event.clientX != null) {
        eventDoc = (event.target && event.target.ownerDocument) || document;
        doc = eventDoc.documentElement;
        body = eventDoc.body;

        event.pageX = event.clientX +
          (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
          (doc && doc.clientLeft || body && body.clientLeft || 0);
        event.pageY = event.clientY +
          (doc && doc.scrollTop || body && body.scrollTop || 0) -
          (doc && doc.clientTop || body && body.clientTop || 0);
    }

    mousePos = {
        x: event.pageX,
        y: event.pageY,
        m: $(window).width(),
        n: $(window).height(),
    };
}
//function getMousePosition() {
//    var pos = mousePos;
//    if (!pos) {
//        // We haven't seen any movement yet
//    }
//    else {
//        // Use pos.x and pos.y
//    }
//}

//test dragg

var current_baseday;
var current_baseid;
var new_baseday;
var template_baseday;
function dragStart(event) {
    //console.log('dragStart :' + JSON.stringify(event.target.id));
    current_baseday = event.currentTarget.getAttribute("data-date");
    current_baseid = event.currentTarget.getAttribute("data-baseid");
    event.dataTransfer.setData("Text", event.target.id);
}

function dragging(event) {
    if (event.target.parentNode.className=='number-day-create-event-has multipe-baseday-event-has')
    {
        if (event.target.parentNode.childNodes[1]) {
            event.target.parentNode.childNodes[1].style.visibility = 'hidden';
        }
    }
    else
    {
        if (event.target.parentNode.childNodes[1]) {
            event.target.parentNode.childNodes[1].style.visibility = 'hidden';
        }
    }
    //console.log('dragging :' + JSON.stringify(event.target.id));
}

function allowDrop(event) {
    template_baseday= event.currentTarget.getAttribute("data-date");
    //if (event.currentTarget.className != 'li-day btn li-day-has-event')
    //{
    if (current_baseid) {
        event.currentTarget.style.backgroundColor = "#26C281";
    }
        //console.log('allowDrop :' + JSON.stringify(event.target.id));
   // }
    if (current_baseday == template_baseday)
    {
        if (event.currentTarget.className == 'li-day btn li-day-has-event')
        {
            event.currentTarget.style.backgroundColor = "#eee";
        }
       
    }
    event.preventDefault();
}

function leaveDrop(event) {
    if (event.currentTarget.className == 'li-day btn li-day-has-event') {
        event.currentTarget.style.backgroundColor = "#eee";
    }
    if (event.currentTarget.className != 'li-day btn li-day-has-event') {
        event.currentTarget.style.background = "none";
        //console.log('allowDrop :' + JSON.stringify(event.target.id));
    }
    event.preventDefault();
}

function managecampaign_drop(apiDomain, access_token, CampaignId, message) {
    $('#advance-campaign-show-grid-open').on('drop', '.li-day', function (event) {

        new_baseday = event.currentTarget.getAttribute("data-date");
        event.preventDefault();
        if (parseInt(current_baseday) != parseInt(new_baseday)) {
            //var data = event.dataTransfer.getData("Text");
            //event.target.appendChild(document.getElementById(data));
            console.log(current_baseday + new_baseday + current_baseid);
            if (current_baseid) {
                managecampaign_UpdateBaseDay(apiDomain, access_token, CampaignId, message);
            }
        }
    });
    
}

function managecampaign_UpdateBaseDay(apiDomain, access_token, CampaignId, message)
{
    var  ResendType =2;
    if (parseInt(new_baseday) == 0)
    {
        ResendType = 2;
    }
    var data = {
        BaseId: current_baseid,
        BaseDay: new_baseday,
        ResendType: ResendType,
        ResendWhenReregister:false, //mặt định luôn từ ngày khác chuyển qua ngày o hay ngày o  chuyển qua ngày khác
    }
    $.ajax({
        url: apiDomain + 'api/AutoCampaign/UpdateEventBaseDay',
        type: 'Post',
        data:JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        beforeSend:function(request)
        {
            request.setRequestHeader('Authorization','Bearer '+access_token)
        },
        success:function(r)
        {
            custom_shownotification('success', message.UpdateSuccess);
            $.ajax({
                url: apiDomain + 'api/AutoCampaign/GetEventList?CampaignId=' + CampaignId,
                type: 'GET',
                contentType: 'application/json; charset=utf-8',
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + access_token);
                },
                success: function (r) {
                    $('#divLoading').hide();
                    switch (parseInt(r.ErrorCode)) {
                        case 0:
                            var data = r.Data;
                            var obj = [];
                            $.each(data, function (i, o) {
                                if (o.BaseType == 1) { //timebase
                                    obj.push(o);
                                }
                            });

                            var grouparr = _.groupBy(obj, function (value) {
                                return 'BaseDay' + value.BaseDay;
                            });

                            var loopday = { current_baseday, new_baseday };
                            $.each(loopday, function (i, o) {
                               
                                var Keys = 'BaseDay' + o;
                                var html = '';
                                if (grouparr[Keys]) {
                                    var data = grouparr[Keys];
                                    if (data.length == 1) {

                                        html = managecampaign_CellSingle(data, o, CampaignId, message);
                                    }
                                    else {

                                        html = managecampaign_CellMultiEvent(data, o, CampaignId, message);
                                    }


                                }
                                else {
                                    html = managecampaign_CellNoEvent(o, CampaignId, message);
                                }
                                console.log(html);
                               
                                var element = $('#advance-campaign-show-grid-open #grid-newsletter').find("li[data-date='" + o + "']");
                                element.children().remove();
                                element.append(html);
                                element.children().css('display', 'block');
                                current_baseid = undefined;
                            });

                            break;
                        default:
                            break;
                    }
                },
                error: function (x, s, e) {
                    $('#divLoading').hide();
                    custom_shownotification("error", message.HaveError);
                }
            })

        },error:function(x,s,e)
        {

        }
    })

}

function managecampaign_Gettype(SendMethod)
{
    var method = "";
    switch (SendMethod) {
        case 0: method = '<i class="fa fa-envelope"></i> ';
            break;
        case 1: method = '<i class="fa fa-commenting-o"></i> ';
            break;
        case 2: method = '<i class="fa fa-envelope" style="padding-right:2px;"></i><i class="fa fa-commenting-o"></i> ';
    }
    return method;
}


function managecampaign_DetectSMSGateWay(apiDomain, access_token,EventId, message) {
    var data=$.ajax({
        url: apiDomain + 'api/AutoCampaign/CheckEventHasSMSGateway?EventBaseId=' + EventId,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        async: false,
        success: function (r) {
            console.log(r);
        },
        error: function (r) {

        }
    }).responseJSON;
    return data;

};


