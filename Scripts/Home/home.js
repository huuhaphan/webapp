﻿function home_action(apiDomain, access_token, message) {
    //home_chart();
    home_DataDateTime();
    home_SelectDateTime(apiDomain, access_token, message);
    home_SendDashboard(apiDomain, access_token, message);
    home_ContactDashboard(apiDomain, access_token, message);
}

function home_SendDashboard(apiDomain,access_token,message) {
    var timezone = custom_GetTimeZone();
    var data = home_DataDateTime();
    $.ajax({
        url: apiDomain + 'api/report/Mobile/SendDashboard',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    //total
                    $('#totalcontactsms').text(formatMoney(data.TotalSMSContact,''));
                    $('#totalcontactsms').attr('data-value',formatMoney( data.TotalSMSContact,''));

                    $('#totalcontactbrandname').text(formatMoney( data.TotalSMSBrandContact,''));
                    $('#totalcontactbrandname').attr('data-value',formatMoney(  data.TotalSMSBrandContact,''));

                    $('#totalcontactrandom').text(formatMoney( data.TotalSMSOTPContact,''));
                    $('#totalcontactrandom').attr('data-value',formatMoney(  data.TotalSMSOTPContact,''));

                    $('#totalcontactgateway').text(formatMoney( data.TotalSMSGatewayContact,''));
                    $('#totalcontactgateway').attr('data-value',formatMoney(  data.TotalSMSGatewayContact,''));

                    //percent
                    $('#percentcontactbrandname').text(home_CalPercent(data.TotalSMSContact , data.TotalSMSBrandContact));
                    $('#percentcontactbrandname').attr('data-value',home_CalPercent( data.TotalSMSContact, data.TotalSMSBrandContact));

                    $('#percentcontactrandom').text(home_CalPercent( data.TotalSMSContact , data.TotalSMSOTPContact));
                    $('#percentcontactrandom').attr('data-value',home_CalPercent( data.TotalSMSContact, data.TotalSMSOTPContact));

                    $('#percentcontactgateway').text(home_CalPercent(data.TotalSMSContact , data.TotalSMSGatewayContact));
                    $('#percentcontactgateway').attr('data-value',home_CalPercent( data.TotalSMSContact , data.TotalSMSGatewayContact));

                    //total
                    $('#totalcontactemail').text(formatMoney( data.TotalMailContact,''));
                    $('#totalcontactemail').attr('data-value', data.TotalMailContact);

                    $('#totalcontactcampaign').text(formatMoney( data.TotalNormalMailContact,''));
                    $('#totalcontactcampaign').attr('data-value',formatMoney(data.TotalNormalMailContact,''));

                    $('#totalcontactautoresponder').text(formatMoney(data.TotalAutoMailContact,''));
                    $('#totalcontactautoresponder').attr('data-value',formatMoney(data.TotalAutoMailContact,''));

                    //percent
                    $('#percentcontactcampaign').text(home_CalPercent(data.TotalMailContact,data.TotalNormalMailContact));
                    $('#percentcontactcampaign').attr('data-value',home_CalPercent(data.TotalMailContact,data.TotalNormalMailContact));

                    $('#percentcontactautoresponder').text(home_CalPercent(data.TotalMailContact, data.TotalAutoMailContact));
                    $('#percentcontactautoresponder').attr('data-value', home_CalPercent(data.TotalMailContact , data.TotalAutoMailContact));

                     $('span[sdata-counter="scounterup"]').counterUp();

                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    
                    break;
            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
        }
    });
}

function home_CalPercent(total, per) {
    if (parseInt(per) == 0) {
        return 0;
    } else {
        var a = per*100/total;
        return a.toFixed(2);
    }
}

function home_DataDateTime() {
    var data = [];
    var type = $("input:radio[name='options-date']:checked").val();
    switch (parseInt(type)) {
        case 1:
            data = {
                FromDate: moment().format("YYYY-MM-DD"),
                ToDate: moment().format("YYYY-MM-DD"),
            };
            break;
        case 2:
            data = {
                FromDate: moment().subtract('days', 7).format("YYYY-MM-DD"),
                ToDate: moment().format("YYYY-MM-DD"),
            };
            break;
        case 3:
            data = {
                FromDate: moment().subtract('days', 30).format("YYYY-MM-DD"),
                ToDate: moment().format("YYYY-MM-DD"),
            };
            break;
        default:
            data = {
                FromDate: moment().format("YYYY-MM-DD"),
                ToDate: moment().format("YYYY-MM-DD"),
            };
            break;
    }
       
    console.log(data);
    return data;
}

function home_SelectDateTime(apiDomain, access_token, message)
{
    $("input:radio[name='options-date']").change(function () {
        home_SendDashboard(apiDomain, access_token, message);
        home_ContactDashboard(apiDomain, access_token, message);

    })
}

function home_ContactDashboard(apiDomain, access_token, message)
{
    var timezone = custom_GetTimeZone();
    var data = home_DataDateTime();
    $.ajax({
        url: apiDomain + 'api/report/Mobile/ContactDashboard',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;

                    $('#totalcontactsubscribe').text(formatMoney(data.Total,''));
                    $('#totalcontactsubscribe').attr('data-value',formatMoney(data.Total,''));

                    //$('span[sdata-counter="scounterup"]').counterUp();
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
        }
    });
}

//function home_chart() {

//    var chart = AmCharts.makeChart("chartdiv", {
//        "type": "serial",
//        "theme": "light",
//        "marginRight": 40,
//        "marginLeft": 40,
//        "autoMarginOffset": 20,
//        "mouseWheelZoomEnabled": false,
//        "dataDateFormat": "YYYY-MM-DD",
        
//        "valueAxes": [{
//            "id": "v1",
//            "axisAlpha": 0,
//            "position": "left",
//            "ignoreAxisWidth": true
//        }],
//        "balloon": {
//            "borderThickness": 1,
//            "shadowAlpha": 0
//        },
//        "graphs": [{
//            "id": "g1",
             
//            "balloon": {
//                "drop": true,
//                "adjustBorderColor": false,
//                "color": "#eee"
//            },
//            "lineColor":"#2ab4c0",
//            "bullet": "round",
//            "bulletBorderAlpha": 1,
//            "bulletColor": "#FFFFFF",
//            "bulletSize": 5,
//            "hideBulletsCount": 50,
//            "lineThickness": 2,
//            "title": "red line",
//            "useLineColorForBulletBorder": true,
//            "valueField": "value",
//            "balloonText": "<div style='font-size:15px; height:400px;width:200px;'>" +
//                                "<div >[[date]]<br></div><br/>" +
//                                "<div style='padding-left:15px;text-align:left'>" +
//                                "<span class='padding-tb-10'>REQUEST<br/><span class='font-green-sharp bold'>[[value]]</span><br></span><br/>" +
//                                 "<span class='padding-tb-10'>DELIVERED<br/><span class='font-yellow-soft bold'>[[value2]]</span><br></span><br/>" +
//                                "<span class='padding-tb-10'>OPENED<br/><span class='font-blue-sharp bold'>[[value3]]</span><br></span><br/>" +
//                                "<span class='padding-tb-10'>CLICKED<br/><span class='font-green bold'>[[value4]]</span><br></span><br/>" +
//                                "<span class='padding-tb-10'>UNIQUE CLICK<br/><span class='font-purple bold'>[[value5]]</span><br></span><br/>" +
//                                "<span class='padding-tb-10'>UNIQUE OPEN<br/><span class='font-red-mint bold'>[[value6]]</span></span>" +
//                        "</div></div>"
//        },
//                  {
//                      "id": "g2",
//                      "balloon": {
//                          "drop": true,
//                          "adjustBorderColor": false,
//                          "color": "#ffffff"
//                      },
//                      "lineColor": "#c8d046",
//                      "bullet": "round",
//                      "bulletBorderAlpha": 1,
//                      "bulletColor": "#FFFFFF",
//                      "bulletSize": 5,
//                      "hideBulletsCount": 50,
//                      "lineThickness": 2,
//                      "title": "red line",
//                      "useLineColorForBulletBorder": true,
//                      "valueField": "value2",
//                      "showBalloon": false,
//                  },
//        {
//            "id": "g3",
//            "balloon": {
//                "drop": true,
//                "adjustBorderColor": false,
//                "color": "#ffffff"
//            },
//            "lineColor": "#5C9BD1",
//            "bullet": "round",
//            "bulletBorderAlpha": 1,
//            "bulletColor": "#FFFFFF",
//            "bulletSize": 5,
//            "hideBulletsCount": 50,
//            "lineThickness": 2,
//            "title": "red line",
//            "useLineColorForBulletBorder": true,
//            "valueField": "value3",
//            "showBalloon": false,
//        },
//        {
//            "id": "g4",
//            "balloon": {
//                "drop": true,
//                "adjustBorderColor": false,
//                "color": "#ffffff"
//            },
//            "lineColor": "#32c5d2",
//            "bullet": "round",
//            "bulletBorderAlpha": 1,
//            "bulletColor": "#FFFFFF",
//            "bulletSize": 5,
//            "hideBulletsCount": 50,
//            "lineThickness": 2,
//            "title": "red line",
//            "useLineColorForBulletBorder": true,
//            "valueField": "value4",
//            "showBalloon": false,
//        },
//        {
//            "id": "g5",
//            "balloon": {
//                "drop": true,
//                "adjustBorderColor": false,
//                "color": "#ffffff"
//            },
//            "lineColor": "#8E44AD",
//            "bullet": "round",
//            "bulletBorderAlpha": 1,
//            "bulletColor": "#FFFFFF",
//            "bulletSize": 5,
//            "hideBulletsCount": 50,
//            "lineThickness": 2,
//            "title": "red line",
//            "useLineColorForBulletBorder": true,
//            "valueField": "value5",
//            "showBalloon": false,
//        },
//        {
//            "id": "g6",
//            "balloon": {
//                "drop": true,
//                "adjustBorderColor": false,
//                "color": "#ffffff"
//            },
//            "lineColor": "#e43a45",
//            "bullet": "round",
//            "bulletBorderAlpha": 1,
//            "bulletColor": "#FFFFFF",
//            "bulletSize": 5,
//            "hideBulletsCount": 50,
//            "lineThickness": 2,
//            "title": "red line",
//            "useLineColorForBulletBorder": true,
//            "valueField": "value6",
//            "showBalloon": false,
//        }],
//        //"chartScrollbar": {
//        //    "graph": "g1",
//        //    "oppositeAxis": false,
//        //    "offset": 30,
//        //    "scrollbarHeight": 20,
//        //    "backgroundAlpha": 0,
//        //    "selectedBackgroundAlpha": 0.1,
//        //    "selectedBackgroundColor": "#888888",
//        //    "graphFillAlpha": 0,
//        //    "graphLineAlpha": 0.5,
//        //    "selectedGraphFillAlpha": 0,
//        //    "selectedGraphLineAlpha": 1,
//        //    "autoGridCount": true,
//        //    "color": "#AAAAAA"
//        //},
//        "chartCursor": {
//            "pan": true,
//            "valueLineEnabled": true,
//            "valueLineBalloonEnabled": true,
//            "cursorAlpha": 1,
//            "cursorColor": "#258cbb",
//            "limitToGraph": "g1",
//            "valueLineAlpha": 0.2,
//            "valueZoomable": true
//        },
//        "valueScrollbar": {
//            "oppositeAxis": false,
//            "offset": 50,
//            "scrollbarHeight": 10
//        },
//        "categoryField": "date",
//        "categoryAxis": {
//            "parseDates": true,
//            "dashLength": 1,
//            "minorGridEnabled": true
//        },
//        "export": {
//            "enabled": true
//        },
//        "dataProvider": [{
//            "date": "2012-07-27",
//            "value": 13,
//            "value2": 11,
//            "value3": 13,
//            "value4": 14,
//            "value5": 15,
//            "value6": 19

//        }, {
//            "date": "2012-07-28",
//            "value": 13,
//            "value2": 13,
//            "value3": 16,
//            "value4": 11,
//            "value5": 12,
//            "value6": 14
//        }, {
//            "date": "2012-07-29",
//            "value": 15,
//            "value2": 13,
//            "value3": 12,
//            "value4": 13,
//            "value5": 14,
//            "value6": 19
//        }, {
//            "date": "2012-07-30",
//            "value": 16,
//            "value2": 15,
//            "value3": 19,
//            "value4": 17,
//            "value5": 13,
//            "value6": 14
//        }, {
//            "date": "2012-07-31",
//            "value": 18,
//            "value2": 13,
//            "value3": 11,
//            "value4": 12,
//            "value5": 14,
//            "value6": 18
//        }, {
//            "date": "2012-08-01",
//            "value": 13,
//            "value2": 11,
//            "value3": 18,
//            "value4": 12,
//            "value5": 16,
//            "value6": 14
//        }]
//    });

//    chart.addListener("rendered", zoomChart);

//    zoomChart();

//    function zoomChart() {
//        chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
//    }

//}