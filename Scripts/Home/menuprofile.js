﻿function menuprofile_SignOut(apiDomain, access_token) {
    $('#signout').on('click', function () {
        $.ajax({
            url: apiDomain + 'api/User/SignOut',
            type: 'POST',
            contentType: 'application/x-www-form-urlencoded',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            success: function (r) {
                $.ajax({
                    url: '/Login/SignOut',
                    type: 'POST',
                    success: function (r) {
                        window.location.href = '/Login'
                    },
                    error: function (x, s, e) {

                    }
                })
            },
            error: function (x, s, e) {
                $.ajax({
                    url: '/Login/SignOut',
                    type: 'POST',
                    success: function (r) {
                        window.location.href = '/Login'
                    },
                    error: function (x, s, e) {

                    }
                })
            }
        })
    });
}