﻿/// <reference path="../custom.js" />
function userstats_Action(apiDomain,access_token)
{

    if (parseInt($(window).outerWidth()) < 992) {
        $('#use-process').hide();
    }

    window.onresize = function () {
        if (parseInt($('.page-sidebar').outerWidth()) < 200) {
            $('#use-process').hide();
        }
        else {
            $('#use-process').show();
        }
    }

    userstats_LoadUsed(apiDomain, access_token);
    userstats_LoadUserInfo(apiDomain, access_token);
    userstats_GotoRecharge();
}

function userstats_LoadUsed(apiDomain, access_token)
{
    var date = custom_local2utc(new Date(), new Date());
    var timezone = custom_GetTimeZone();
    $.ajax({
        url: apiDomain + 'api/User/GetUserTotalSend',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: date,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = JSON.parse(JSON.stringify(r.Data));
                    $('#sendmailtoday').text(data.TotalEmail);
                    $('#sendsmstoday').text(data.TotalSMS)
                    break;
            }
        }, error: function (xhr,s,e) {
            if (xhr.status == '401') {
                custom_shownotification('error', 'Phiên đăng nhập hết hạn! Vui lòng đăng nhập lại');
                setTimeout(function () { window.location.href = '/login' }, 1000);
            }
            else {
                custom_shownotification('error', xhr.statusText);
            }
        }
    });
}

function userstats_LoadUserInfo(apiDomain, access_token) {
   
    $.ajax({
        url: apiDomain + 'api/User/Info?IsDirty=true',
        type: 'Get',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (result) {
          
            switch (parseInt(result.ErrorCode)) {
                case 0:
                    var redirect = "none";
                    var user = result.Data;
                    custom_UpdateSesion(user, access_token, redirect);
                    $('#process-userbalance').text(formatMoney(parseInt(user.CurrentBalance),'VNĐ'));
                    break;
                default:
                    break;
            }
        },
        error: function (x, s, e) {
            //loading 
          
            console.log(JSON.stringify(x.reponseText));
        }

    })
}

function userstats_GotoRecharge()
{
    $('#usestats-recharge').on('click', function () {
       window.location.href = '/Account/UserProfile#Recharge';
        //location.reload();
        Recharge();
    })
}