﻿function keeplive_GetInfo(apiDomain, access_token) {
    $.ajax({
        url: apiDomain + 'api/User/Info',
        type: 'Get',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (result) {
        },
        error: function (xhr, s, e) {
            if (xhr.status == '401') {
                custom_shownotification('error', 'Phiên đăng nhập hết hạn! Vui lòng đăng nhập lại');
                setTimeout(function () { window.location.href = '/login' }, 1000);
            }
            else {
                custom_shownotification('error', xhr.statusText);
            }
        }

    })
};