﻿(function (factory) {
    /* global define */
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(window.jQuery);
    }
}(function ($) {

    // Extends plugins for adding image.
    //  - plugin is external module for customizing.
    $.extend($.summernote.plugins, {
        /**
         * @param {Object} context - context object has status of editor.
         */
        'image': function (context) {
            var self = this;

            // ui has renders to build ui elements.
            //  - you can create a button with `ui.button`
            var ui = $.summernote.ui;

            // add hello button
            context.memo('button.image', function () {
                // create button
                var button = ui.button({
                    contents: '<i class="fa fa-image"/>',
                    tooltip: 'Upload image',
                    click: function () {
                        //self.$panel.show();
                        //self.$panel.hide(500);
                        //// invoke insertText method with 'image' on editor module.
                        //context.invoke('editor.insertText', 'image');
                        $("#insertImage").trigger('click');
                    }
                });
               
                // create jQuery object from button instance.
               
                var $image = button.render();
                return $image;
            });

            // This events will be attached when editor is initialized.
            this.events = {
                // This will be called after modules are initialized.
                'summernote.init': function (we, e) {
                   
                },
                // This will be called when user releases a key on editable.
                'summernote.keyup': function (we, e) {
                   
                }
            };

            // This method will be called when editor is initialized by $('..').summernote();
            // You can create elements for plugin
            this.initialize = function () {
                this.$panel = $('<div class="image-panel"/>').css({
                    position: 'absolute',
                    width: 100,
                    height: 100,
                    left: '50%',
                    top: '50%',
                    background: 'red'
                }).hide();

                this.$panel.appendTo('body');
            };

            // This methods will be called when editor is destroyed by $('..').summernote('destroy');
            // You should remove elements on `initialize`.
            this.destroy = function () {
                this.$panel.remove();
                this.$panel = null;
            };
        },
        //'contactname': function (context) {
        //    var self = this;

        //    // ui has renders to build ui elements.
        //    //  - you can create a button with `ui.button`
        //    var ui = $.summernote.ui;

        //    // add hello button
        //    context.memo('button.contactname', function () {
        //        // create button
        //        var button = ui.button({
        //            contents: '<i class="fa fa-user"/>',
        //            tooltip: 'Insert name of contact',
        //            click: function () {
        //                //self.$panel.show();
        //                //self.$panel.hide(500);
        //                //// invoke insertText method with 'image' on editor module.
        //                //context.invoke('editor.insertText', 'image');
        //                // $("#insertImage").trigger('click');
        //                $('#event_contentInput').summernote('focus');
        //                $('#event_contentInput').summernote('insertText', '#ContactName#');
        //            },
                    
        //        });

        //        // create jQuery object from button instance.

        //        var $persolinalize = button.render();
        //        return $persolinalize;
        //    });

        //    // This events will be attached when editor is initialized.
        //    this.events = {
        //        // This will be called after modules are initialized.
        //        'summernote.init': function (we, e) {

        //        },
        //        // This will be called when user releases a key on editable.
        //        'summernote.keyup': function (we, e) {

        //        }
        //    };

        //    // This method will be called when editor is initialized by $('..').summernote();
        //    // You can create elements for plugin
        //    this.initialize = function () {
        //        this.$panel = $('<div class="image-panel"/>').css({
        //            position: 'absolute',
        //            width: 100,
        //            height: 100,
        //            left: '50%',
        //            top: '50%',
        //            background: 'red'
        //        }).hide();

        //        this.$panel.appendTo('body');
        //    };

        //    // This methods will be called when editor is destroyed by $('..').summernote('destroy');
        //    // You should remove elements on `initialize`.
        //    this.destroy = function () {
        //        this.$panel.remove();
        //        this.$panel = null;
        //    };
        //},
        //'email': function (context) {
        //    var self = this;

        //    // ui has renders to build ui elements.
        //    //  - you can create a button with `ui.button`
        //    var ui = $.summernote.ui;

        //    // add hello button
        //    context.memo('button.email', function () {
        //        // create button
        //        var button = ui.button({
        //            contents: '<i class="fa fa-envelope"/>',
        //            tooltip: 'Insert email of contact',
        //            click: function () {
        //                //self.$panel.show();
        //                //self.$panel.hide(500);
        //                //// invoke insertText method with 'image' on editor module.
        //                //context.invoke('editor.insertText', 'image');
        //                // $("#insertImage").trigger('click');
        //                $('#event_contentInput').summernote('focus');
        //                $('#event_contentInput').summernote('insertText', '#Email#');
        //            },

        //        });

        //        // create jQuery object from button instance.

        //        var $persolinalize = button.render();
        //        return $persolinalize;
        //    });

        //    // This events will be attached when editor is initialized.
        //    this.events = {
        //        // This will be called after modules are initialized.
        //        'summernote.init': function (we, e) {

        //        },
        //        // This will be called when user releases a key on editable.
        //        'summernote.keyup': function (we, e) {

        //        }
        //    };

        //    // This method will be called when editor is initialized by $('..').summernote();
        //    // You can create elements for plugin
        //    this.initialize = function () {
        //        this.$panel = $('<div class="image-panel"/>').css({
        //            position: 'absolute',
        //            width: 100,
        //            height: 100,
        //            left: '50%',
        //            top: '50%',
        //            background: 'red'
        //        }).hide();

        //        this.$panel.appendTo('body');
        //    };

        //    // This methods will be called when editor is destroyed by $('..').summernote('destroy');
        //    // You should remove elements on `initialize`.
        //    this.destroy = function () {
        //        this.$panel.remove();
        //        this.$panel = null;
        //    };
        //},
    });

}));

(function (factory) {
        /* global define */
        if (typeof define === 'function' && define.amd) {
            // AMD. Register as an anonymous module.
            define(['jquery'], factory);
        } else if (typeof module === 'object' && module.exports) {
            // Node/CommonJS
            module.exports = factory(require('jquery'));
        } else {
            // Browser globals
            factory(window.jQuery);
        }
    }(function ($) {
        $.extend($.summernote.options, {
            template: {},
            usertemplate:{},
        });



        // Extend plugins for adding templates
        $.extend($.summernote.plugins, {
            /**
             * @param {Object} context - context object has status of editor.
             */
            'template': function (context) {
                var ui = $.summernote.ui;
                var options = context.options.template;
                var defaultOptions = {
                    label: 'Personalize',
                    tooltip: 'Insert Personalize',
                    path: '',
                    list: []
                };

                // Assign default values if not supplied
                for (var propertyName in defaultOptions) {
                    if (options.hasOwnProperty(propertyName) === false) {
                        options[propertyName] = defaultOptions[propertyName];
                    }
                }

                // add template button
                context.memo('button.template', function () {
                    // initialize list
                    var htmlDropdownList = '';
                    for (var htmlTemplate in options.list) {
                        if (options.list.hasOwnProperty(htmlTemplate)) {
                            htmlDropdownList += '<li><a href="#" data-value="' + htmlTemplate + '">' + options.list[htmlTemplate] + '</a></li>';
                        }
                    }

                    // create button
                    var button = ui.buttonGroup([
                        ui.button({
                            className: 'dropdown-toggle',
                            contents: '<span class="template"/> ' + options.label + ' <span class="caret"></span>',
                            tooltip: options.tooltip,
                            data: {
                                toggle: 'dropdown'
                            }
                        }),
                        ui.dropdown({
                            className: 'dropdown-template',
                            items: htmlDropdownList,
                            click: function (event) {
                                event.preventDefault();
                                var $button = $(event.target);
                                var value = $button.data('value');
                                $('#event_contentInput').summernote('focus');
                                $('#event_contentInput').summernote('insertText',' '+ value+' ');


                            }
                        })
                    ]);

                    // create jQuery object from button instance.
                    return button.render();
                });
            },
            'usertemplate': function (context) {
            var ui = $.summernote.ui;
            var options = context.options.usertemplate;
            var defaultOptions = {
                label: 'Personalize',
                tooltip: 'Insert Personalize',
                path: '',
                list: []
            };

                // Assign default values if not supplied
            for (var propertyName in defaultOptions) {
                if (options.hasOwnProperty(propertyName) === false) {
                    options[propertyName] = defaultOptions[propertyName];
                }
            }

                // add template button
            context.memo('button.usertemplate', function () {
                // initialize list
                var htmlDropdownList = '';
                for (var htmlTemplate in options.list) {
                    if (options.list.hasOwnProperty(htmlTemplate)) {
                        htmlDropdownList += '<li><a href="#" data-value="' + htmlTemplate + '">' + options.list[htmlTemplate] + '</a></li>';
                    }
                }

                // create button
                var button = ui.buttonGroup([
                    ui.button({
                        className: 'dropdown-toggle',
                        contents: '<span class="template"/> ' + options.label + ' <span class="caret"></span>',
                        tooltip: options.tooltip,
                        data: {
                            toggle: 'dropdown'
                        }
                    }),
                    ui.dropdown({
                        className: 'dropdown-usertemplate',
                        items: htmlDropdownList,
                        click: function (event) {
                            event.preventDefault();
                            var $button = $(event.target);
                            var value = $button.data('value');
                            $('#event_contentInput').summernote('focus');
                            $('#event_contentInput').summernote('insertText',' '+ value+' ');


                        }
                    })
                ]);

                // create jQuery object from button instance.
                return button.render();
            });
        }
        });
}));