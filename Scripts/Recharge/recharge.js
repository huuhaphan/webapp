﻿function recharge_Background() {

    $.backstretch([
    "../Content/Images/login-background.jpg",
    ], {
        fade: 1000,
        duration: 8000
    }
    );
}

function recharge_Action() {
    recharge_Back();
}

function recharge_Back() {
    $('#redirec-to-userprofile').on('click', function () {
        window.location.href = 'http://app.toomarketer.com/Account/UserProfile#Recharge';
    });
}