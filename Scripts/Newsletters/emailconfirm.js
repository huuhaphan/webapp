﻿
function emailconfirm_Action(apiDomain, access_token, CampaignIdUrl, message)
{
    var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
    if (basiccampaign != '0' && basiccampaign != null) {
        if( basiccampaign.CampaignId==CampaignIdUrl)
        {
            var CampaignId = basiccampaign.CampaignId;
            var SendId = basiccampaign.EmailSend.SentId;
        
            emailconfirm_GenTimeSelect();

            //load các thông tin (theo từng step mục đã hoàn thành chưa)
            emailconfirm_LoadInfo(message);
            //button hieu chinh
            emailconfirm_ActionEdit(CampaignId, message);

            //back về trang recipient
            emailcofirm_BackAction(CampaignId);

            //khai báo datetim
            emailconfirm_HandlerDateTime();

            //load số tiền dự kiến phải trả.
            emailcontent_EstimateCost(apiDomain, access_token, CampaignId,message)

            //click trên step 
            custom_ActionOfColStep(CampaignId, message);

            //gửi ngay
            emailcontent_SendCampaign(apiDomain, access_token, CampaignId, message);

            //các thao tác mở, đóng đặt lịch
            emailconfirm_Sendchedule(apiDomain, access_token, CampaignId, message);

            //hide menu
            // custom_HideMenu();

            eventclicksend(apiDomain, access_token, CampaignId, message);


            //xem trước 
            emailconfirm_preview(CampaignId);

            //mở modal gửi thử 
            emailconfirm_ShowModalSendTest(message);

            //gửi thử 
            emailconfirm_SendTest(apiDomain, access_token, CampaignId, SendId, message);

        }
        else {
            window.location.href = "/BasicCampaign/NotHavePermission";
        }
    }
    else
    {

        custom_shownotification('error', message.HaveError);
        $('#divLoading').hide();
        setTimeout(function () { window.location.href = "/BasicCampaign"; }, 1000)
    }

   
}

function emailconfirm_ActionEdit(CampaignId,message)
{
    $('#confirmeditcontent').on('click', function () {

        if (CampaignId != undefined) {
            //loading
            $('#divLoading').show();
            window.location.href = '/BasicCampaign/Content?eId=' + CampaignId;
        }
        else {
            custom_shownotification('error', message.HaveError);
        }

    });
    $('#confirmeditsetting').on('click', function () {
       
        if (CampaignId != undefined) {
            //loading
            $('#divLoading').show();
            window.location.href = '/BasicCampaign/Setting?eId=' + CampaignId;
        }
        else {
            custom_shownotification('error', message.HaveError);
        }

    });
    $('#confirmeditrecipient').on('click', function () {

        if (CampaignId != undefined) {
            //loading
            $('#divLoading').show();
            window.location.href = '/BasicCampaign/Recipient?eId=' + CampaignId;
        }
        else {
            custom_shownotification('error', message.HaveError);
        }

    });
}
function emailconfirm_LoadInfo(message) {
    //loading
    $('#divLoading').show();

    if (localStorage.getItem('BasicCampaign')) {
        var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
        console.log(basiccampaign);
        if (basiccampaign != '0' || basiccampaign != null) {
            var menthod = parseInt(basiccampaign.SendMethod);
            var emailsend = JSON.parse(JSON.stringify(basiccampaign.EmailSend));
            var smssend = JSON.parse(JSON.stringify(basiccampaign.SMSSend));
            switch (menthod) {
                case 1:
                    //kiểm tra thông tin
                    $('#confirmmenthod').text('SMS');
                    $('#statementhod').attr('src', '/Content/Images/tick-64.png');
                   
                    ////kiểm tra nội dung
                        $('#confirmsmslength').text('SMS :');
                        $('#smslength').text(smssend.MessageLength + ' message');
                        if (smssend.Message == null) {
                            $('#statemessage').attr('src', '/Content/Images/exclamation-64.png');
                            $('.info-content-beforesend').removeClass('note-info').addClass('note-warning');
                        }
                        else {
                            $('#statemessage').attr('src', '/Content/Images/tick-64.png')
                        }
                        $('#smsmenthod').removeClass('hide');

                    //kiểm tra người nhận tin
                        if (basiccampaign.ContactGroups.length == 0 || parseInt(smssend.TotalContact)==0) {
                            $('#staterecipient').attr('src', '/Content/Images/exclamation-64.png');
                            $('.info-recipient-beforesend').removeClass('note-info').addClass('note-warning');
                        }
                        else {
                            $('#staterecipient').attr('src', '/Content/Images/tick-64.png');
                        }

                    //kiểm tra mức phí 
                        $('#sendmoment #costsms').removeClass('hide');
                        $('#sendschedule #costsms').removeClass('hide');

                    //mặc định hiện phần gửi ngay
                        $('#sendmoment').removeClass('hide');
                        $('#sendmoment #totalmoney').removeClass('hide');
                        $('#sendschedule #totalmoney').removeClass('hide');

                        $('#img-loading-money-beforesend').show(); //loading tiền.
                    //ban trạng thái tiền là không cho 
                        $('#statemoney').attr('src', '/Content/Images/exclamation-64.png');

                    break;
                case 2:
                    //kiểm tra thông tin
                    $('#confirmmenthod').text('Email, SMS');
                    $('#confirmsender').text(emailsend.FromName);
                    $('#confirmemailsend').text( '<'+ emailsend.FromEmail+'>');
                    $('#confirmreply').text(emailsend.ReplyToEmail);
                   // var showunsubcribe; var facebook; var twitter;
                   // emailconfirm_LoadFooter(emailsend, facebook, showunsubcribe, twitter,message)
                    
                    if (!emailsend.FromName || !emailsend.FromEmail) {
                        $('#statementhod').attr('src', '/Content/Images/exclamation-64.png');
                    }
                    else {
                        $('#statementhod').attr('src', '/Content/Images/tick-64.png');
                    }
                    $('#emailmenthod').show();

                    //kiểm tra nội dung
                    $('#confirmsmslength').text('SMS :');
                    $('#smslength').text(message.MessageLength+ ': ' + smssend.MessageLength);
                    if (emailsend.IsUseTemplate == true) {
                        $('#confirmistemplate').text('Email: ' + message.EmailFormat);
                    }
                    else {
                        $('#confirmistemplate').text('Email: ' + message.EmailFormat);

                    }
                    if (smssend.Message == null || emailsend.HTML == null || emailsend.HTML == '' || smssend.Message == '') {
                        $('#confirmemailnull')
                        $('#statemessage').attr('src', '/Content/Images/exclamation-64.png');
                        $('.info-content-beforesend').removeClass('note-info').addClass('note-warning');
                    }
                    else {
                        $('#statemessage').attr('src', '/Content/Images/tick-64.png');
                    }
                    $('#emailmenthodcontent').removeClass('hide');
                    $('#smsmenthod').removeClass('hide');

                    //kiểm tra người nhận tin
                    if (basiccampaign.ContactGroups.length == 0) {
                        $('#staterecipient').attr('src', '/Content/Images/exclamation-64.png');
                        $('.info-recipient-beforesend').removeClass('note-info').addClass('note-warning');
                    }
                    else {
                        $('#staterecipient').attr('src', '/Content/Images/tick-64.png');
                    }

                    //kiểm tra mức phí 
                    $('#sendmoment #costsms').removeClass('hide');
                    $('#sendschedule #costsms').removeClass('hide');
                    $('#sendmoment #costemail').removeClass('hide');

                    //show xem trước
                    $("#btnpreviewcampaign1").show();
                    $("#btnpreviewcampaign2").show();
                    $("#btnpreviewcampaign3").show();
                    $("#lblpreviewcampaign").show();

                    //mặc định hiện phần gửi ngay
                    $('#sendmoment').removeClass('hide');

                    $('#sendschedule #costemail').removeClass('hide');
                    $('#sendschedule #noquota-schedule').removeClass('hide');
                    $('#noquota-moment ').removeClass('hide');

                    $('#noquota-schedule').removeClass('hide');
                    $('#sendmoment #totalmoney').removeClass('hide');
                    $('#sendschedule #totalmoney').removeClass('hide');

                    $('#img-loading-money-beforesend').show(); //loading tiền.

                    //ban trạng thái tiền là không cho 
                    $('#statemoney').attr('src', '/Content/Images/exclamation-64.png');

                    break;
                case 0:

                    //kiểm tra thông tin
                    $('#confirmmenthod').text('Email');
                    $('#confirmsender').text('From: ' + emailsend.FromName);
                    $('#confirmemailsend').text('<' + emailsend.FromEmail + '>');
                    $('#confirmreply').text('Reply:' + emailsend.ReplyToEmail);
                    if (!emailsend.FromName || !emailsend.FromEmail) {
                        $('#statementhod').attr('src', '/Content/Images/exclamation-64.png');
                    }
                    else {
                        $('#statementhod').attr('src', '/Content/Images/tick-64.png');
                    }

                    // var showunsubcribe; var facebook; var twitter;
                   // emailconfirm_LoadFooter(emailsend, facebook, showunsubcribe, twitter,message);
                    $('#emailmenthod').show();


                    //kiểm tra nội dung
                    $('#confirmistemplate').text('Email: ' + message.EmailFormat);
                    if (emailsend.HTML == null || emailsend.HTML == '') {
                        $('#statemessage').attr('src', '/Content/Images/exclamation-64.png');
                        $('.info-content-beforesend').removeClass('note-info').addClass('note-warning');
                    }
                    else {
                        $('#statemessage').attr('src', '/Content/Images/tick-64.png');
                    }

                    $('#emailmenthodcontent').removeClass('hide');

                    //kiểm tra người nhận tin
                    if (basiccampaign.ContactGroups.length == 0 || parseInt(emailsend.TotalContact)==0) {
                        $('#staterecipient').attr('src', '/Content/Images/exclamation-64.png');
                        $('.info-recipient-beforesend').removeClass('note-info').addClass('note-warning');
                    }
                    else {
                        $('#staterecipient').attr('src', '/Content/Images/tick-64.png');
                    }

                    //kiểm tra mức phí 
                    $('#sendmoment #costemail').removeClass('hide');
                    $('#sendschedule #costemail').removeClass('hide');

                    //show xem trước
                    $("#btnpreviewcampaign1").show();
                    $("#btnpreviewcampaign2").show();
                    $("#btnpreviewcampaign3").show();
                    $("#lblpreviewcampaign").show();

                    //mặc định hiện phần gửi ngay
                    $('#sendmoment').removeClass('hide');
                    $('#sendmoment #totalmoney').removeClass('hide');
                    $('#sendschedule #totalmoney').removeClass('hide');

                    $('#noquota-moment ').removeClass('hide');
                    $('#noquota-schedule').removeClass('hide');

                    $('#img-loading-money-beforesend').show(); //loading tiền.

                    //ban trạng thái tiền là không cho 
                    $('#statemoney').attr('src', '/Content/Images/exclamation-64.png');

            }
        }
    }
    //loading
    $('#divLoading').hide();
}

function emailconfirm_LoadFooter(emailsend, facebook, showunsubcribe, twitter, message) {

    //loading
    $('#divLoading').show();
    if (emailsend.Facebook == "") {
        facebook = message.NotSetting
    }
    else {
        facebook = emailsend.Facebook;
    }
    if (emailsend.ShowUnsubscribeButton == true) {
        showunsubcribe = message.Yes;
    }
    else {
        showunsubcribe = message.No;
    }
    if (emailsend.Twitter == "") {
        twitter = message.NotSetting;
    }
    else {
        twitter = emailsend.Twitter;
    }

    $('#confirmfooter').append(
                        '<span>- ' + message.ShowUnsubcribe + ': ' + showunsubcribe + '</span><br/>' +
                        '<span>- Facebook: ' + facebook + '</span> <br/>' +
                        '<span>- Twitter: ' + twitter + '</span> ');

    //loading
    $('#divLoading').hide();
};

function emailcofirm_BackAction(CampaignId)
{
    $('#btnConfirmBack').on('click', function () {

        //loading
        $('#divLoading').show();

        window.location.href = '/BasicCampaign/Recipient?eId=' + CampaignId;
    });
    
}

//nút gửi không cần đặt lịch
//var t_apiDomain;var t_access_token;var t_CampaignId;var t_message;

function emailcontent_SendCampaign(apiDomain, access_token, CampaignId, message) {
    $('#btnsendcampaign').on('click', function () {
        // t_apiDomain = apiDomain;
       //  t_CampaignId = CampaignId;
      //  t_access_token = access_token;
     //  t_message = message;

        $('#modelConfirmSend').modal('toggle');
        
    });
}

function eventclicksend(apiDomain, access_token, CampaignId, message)
{
    $('#btnConfirmSave').on('click', function () {
        //loading
        $('#divLoading').show();

        $('#modelConfirmSend').modal('toggle');
        if (emailconfirm_CheckMoneyBeforeSend(message)) {
            var data = {
                CampaignId: CampaignId,
                IsSchedule: false,
                ScheduleDate: moment().utc().format(),
            };
            sendcampaign(apiDomain, access_token, CampaignId, message, data);
        }
    });
}

// send 

function sendcampaign(apiDomain,access_token,CampaignId,message,data)
{
   
        $.ajax({
            url: apiDomain + 'api/BasicCampaign/Send',
            type: 'POST',
            contentType: 'application/x-www-form-urlencoded',
            data: data,
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            success: function (r) {
                //loading
              
                console.log(r);
                switch (parseInt(r.ErrorCode)) {
                    case 0:
                        custom_shownotification('success', message.SentCampaignSuccess);
                        var redirect = 'none';
                        //custom_GetInfo(apiDomain, access_token, redirect); //cập nhập lại session số tiền
                        setTimeout(function () {
                            window.location.href = '/BasicCampaign';
                        }, 1000);
                        $('#divLoading').hide();
                        break;
                    case 106:
                        custom_shownotification('error', r.ErrorMessage);
                        break;
                    case 102:
                        custom_shownotification('warning', 'Chiến dịch đã được gửi');
                        setTimeout(function () {
                            window.location.href = '/BasicCampaign';
                        }, 1000);
                        $('#divLoading').hide();
                        break;
                    default:
                        custom_shownotification('error', message.SentCampaignError);
                        $('#divLoading').hide();
                        break;
                }


            },
            error: function (x, e, s) {
                //loading
                $('#divLoading').hide();
                custom_shownotification('error', x.statusText);

            }
        });
}


//gửi đặt lịch
function emailcontent_SendCampaignSchedule(apiDomain, access_token, CampaignId, datatime, message) {
   
        if (emailconfirm_CheckMoneyBeforeSendSchedule(message)) {
            var data = {
                CampaignId: CampaignId,
                IsSchedule: true,
                ScheduleDate: datatime,
            };
            $.ajax({
                url: apiDomain + 'api/BasicCampaign/Send',
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                data: data,
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + access_token);
                },
                success: function (r) {
                    //loading
                    $('#divLoading').hide();
                    switch (parseInt(r.ErrorCode)) {
                        case 0:
                            custom_shownotification('success', message.ScheduleCampaignSuccess);
                            var redirect = 'none';
                            custom_GetInfo(apiDomain, access_token, redirect); //cập nhập lại session số tiền

                            setTimeout(function () {
                                window.location.href = '/BasicCampaign';
                            }, 500);
                            break;
                        case 106:
                            custom_shownotification('error', r.ErrorMessage);

                            break;
                        default:
                            custom_shownotification('error', message.ScheduleCampaignError);
                    }


                },
                error: function (x, e, s) {
                    //loading
                    $('#divLoading').hide();
                    custom_shownotification('error', 'Có lỗi xảy ra');

                }
            });
        }

}

function emailconfirm_HandlerDateTime()
{
    $(".date-picker").datepicker({
        minDate: moment(), // Current day
        maxDate: moment().add(3, 'days'), // 3 days from the current day
    });
}



//các thao tác đặt lịch, chọn, hủy , gửi luôn
function emailconfirm_Sendchedule(apiDomain, access_token, CampaignId, message) {
    //loading
    $('#divLoading').show();
    //nút chọn đặt lịch
    $('#btncampaignschedule').on('click', function () {

        var dateformat = moment().format('MM-DD-YYYY');
        $('#date-schedule-send-email').datepicker('update', dateformat);

        var date = new Date();
        //setdate = dateAdd(date, 5);
        //var hour = setdate.getHours();
        //var minus = setdate.getMinutes();
        //if (parseInt(hour) < 10) {
        //    if (minus < 10) {
        //        var time = '0' + hour + ":" + '0' + minus;
        //    }
        //    else {
        //        var time = '0' + hour + ":" + minus;
        //    }
        //}
        //else {
        //    if (minus < 10) {
        //        var time =  hour + ":" + '0' + minus;
        //    }
        //    else {
        //        var time =  hour + ":" + minus;
        //    }
        //}
        
        //$('#time-schedule-send-email-basic').val(time);
        //$('#time-schedule-send-email-basic').attr('min', time),

        $('#btncancelcampaignschedule').show();
        $('#btnsendcampaignschedule').show();
        $('#confirmscheduleaction').show();

        $('#btncampaignschedule').hide();
        $('#btnsendcampaign').hide();
        emailcontent_LoadStateMoneySchedule(); //cập nhập lại trạng thái của hình

        //cập nhập giá tiền
        //$('#sendmoment').addClass('hide');
        //$('#sendschedule').removeClass('hide');

        //loading
        $('#divLoading').hide();
    });

    //nut hủy đặt lịch
    $('#btncancelcampaignschedule').on('click', function () {
        //loading
        $('#divLoading').hide();

        $('#btncancelcampaignschedule').hide();
        $('#btnsendcampaignschedule').hide();
        $('#confirmscheduleaction').hide();
        $('#btncampaignschedule').show();
        $('#btnsendcampaign').show();
        emailcontent_LoadStateMoney();//cập nhập trạng thái của hình

        //cập nhập giá tiền 
        //$('#sendmoment').removeClass('hide');
        //$('#sendschedule').addClass('hide');

        //loading
        $('#divLoading').hide();

    });

    $('#btnsendcampaignschedule').on('click', function () {
        //loading
        $('#divLoading').show();

        if ($('#date-schedule-send-sms').datepicker('getDate') == null || $('#date-schedule-send-sms').datepicker('getDate') == "" || $('#select-hour-schuedule option:selected').val() == "" || $('#select-minus-schuedule option:selected').val() == "") {
            //loading
            $('#divLoading').hide();

            custom_shownotification('error', message.SelectDateScheduleError);
            return false;
        }
        else {

            var date = moment($('#date-schedule-send-email').datepicker('getDate')).format('YYYY-MM-DD');
           
            var now = new Date();
            var datenow = moment().format('YYYY-MM-DD');
            var time = [$('#select-hour-schuedule option:selected').val(), $('#select-minus-schuedule option:selected').val()] //$('#time-schedule-send-email-basic').val().split(/:/);

            if (new Date(datenow).getTime() > new Date(date).getTime()) {
                $('#divLoading').hide();
                custom_shownotification('error', message.SelectDayError);
                return false;
            }
            if (date == datenow && parseInt(time[0]) == now.getHours() && parseInt(time[1]) < parseInt(now.getMinutes()) || date == datenow && parseInt(time[0]) < now.getHours()) {
                $('#divLoading').hide();
                custom_shownotification('error', message.SelectTimeError);
                return false;
            }
            //if (moment().add('days', 3).format('YYYY-MM-DD') < date)
            //{
            //    custom_shownotification('error', 'Đặt lịch không quá 3 ngày');
            //}

            else {
                var datetime = date + 'T' + $('#select-hour-schuedule option:selected').val() + ':' + $('#select-minus-schuedule option:selected').val() + ':00';//$('#time-schedule-send-email-basic').val();

                var datimeutc= moment(datetime).utc().format('YYYY-MM-DD HH:mm');
                emailcontent_SendCampaignSchedule(apiDomain, access_token, CampaignId, datimeutc, message)
            }

        }
    })


};

//add minus
function dateAdd(date,units) {
    var ret = new Date(date); 
    ret.setTime(ret.getTime() + units * 60000);
      
    return ret;
}

//lấy thông tin số tiền sẽ trả khi gửi chiến dịch
var acceppsend = false;
var acceppsendschedule = false;
function emailcontent_EstimateCost(apiDomain,access_token,CampaignId,message)
{
    //loading
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/BasicCampaign/EstimateCost?CampaignId='+CampaignId,
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success:function(r)
        {

            switch (parseInt(r.ErrorCode))
            {
                case 0:
                    
                    emailcontent_LoadMoney(r.Data);
                    acceppsend = JSON.parse(JSON.stringify(r.Data)).CanSend;
                    acceppsendschedule = JSON.parse(JSON.stringify(r.Data)).CanSendSchedule;
                    emailcontent_LoadStateMoney(); //cập nhập hình cho trạng thái 
                    
                    //loading
                    $('#divLoading').hide();
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    $('.info-money-beforesend').removeClass('note-info').addClass('note-warning');
                    $('#img-loading-money-beforesend').hide(); //loading tiền.
                    break;
            }

        },
        error:function(x,s,e)
        {
            //loading
            $('#divLoading').hide();
            custom_shownotification('error', message.HaveError);
            $('.info-money-beforesend').removeClass('note-info').addClass('note-warning');
            $('#img-loading-money-beforesend').hide(); //loading tiền.
         
         
        },
    })
}


//lúc bình thường
function emailcontent_LoadStateMoney() {
    if (acceppsend == true) {
        $('#statemoney').attr('src', '/Content/Images/tick-64.png');
        $('#not-enough-money-send').hide();

    }
    else {
        $('#statemoney').attr('src', '/Content/Images/exclamation-64.png');
        $('.info-money-beforesend').removeClass('note-info').addClass('note-warning');
        $('#not-enough-money-send').show();
        $('#btncampaignschedule').prop('disabled', true);
        $('#btnsendcampaign').prop('disabled', true);

        
    }
}

//lúc chọn theo lịch
function emailcontent_LoadStateMoneySchedule() {
    if (acceppsendschedule == true) {
        $('#statemoney').attr('src', '/Content/Images/tick-64.png');
        $('#not-enough-money-send').hide();
    }
    else {
        $('#statemoney').attr('src', '/Content/Images/exclamation-64.png');
        $('.info-money-beforesend').removeClass('note-info').addClass('note-warning');
        $('#not-enough-money-send').show();
        $('#btncampaignschedule').prop('disabled', true);
        $('#btnsendcampaign').prop('disabled', true);
    }
}


//load tính tiền lên view 
function emailcontent_LoadMoney(data)
{
    //loading
    $('#divLoading').show();

    var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
    if (basiccampaign != '0' || basiccampaign != null) {
        var menthod = parseInt(basiccampaign.SendMethod);
    }
    
    var smssend = JSON.parse(JSON.stringify(basiccampaign.SMSSend));
    switch (menthod) {
        case 0: //email

            //thường
            $('#sendmoment #totalemailsend').text(formatMoney(data.EmailRecipientCount,""));
            $('#sendmoment #totalquotabefore').text(data.EmailQuotaRemainBeforeSend);
            $('#sendmoment #totalcostemail').text(formatMoney(data.EmailTotalCost,"") );
            $('#sendmoment #currentbalance').text(formatMoney(data.CurrentBalance,""));
            $('#sendmoment #costperemail').text(data.CostPerMail );
            if (parseInt(data.EmailQuotaRemainBeforeSend) - parseInt(data.EmailRecipientCount) > 0)
                $('#sendmoment #total-quota-after').text(parseInt(data.EmailQuotaRemainBeforeSend) - parseInt(data.EmailRecipientCount) + '/' + data.EmailTotalQuota);
            else
                $('#sendmoment #total-quota-after').text('0');

            //lịch
            //$('#sendschedule #totalemailsend').text(formatMoney(data.EmailRecipientCount,""));
            //$('#sendschedule #totalquotabefore').text(data.EmailQuotaRemainBeforeSend);

            //$('#sendschedule #totalcostemail').text(formatMoney(data.SMSTotalCost, ""));
            //$('#sendschedule #currentbalance').text(formatMoney(data.CurrentBalance,""));
            //$('#sendschedule #costperemail').text(data.CostPerMailSchedule );
            //if (parseInt(data.EmailQuotaRemainBeforeSend) - parseInt(data.EmailRecipientCount) > 0)
            //    $('#sendschedule #total-quota-after').text(parseInt(data.EmailQuotaRemainBeforeSend) - parseInt(data.EmailRecipientCount) + '/' + data.EmailTotalQuota);
            //else
            //    $('#sendschedule #total-quota-after').text('0/20');
          
           // $('#sendschedule #totalcost').text(formatMoney(data.EmailTotalCost, ""));
            $('#sendmoment #totalcost').text(formatMoney(parseInt(data.SMSTotalCost) + parseInt(data.EmailTotalCost), ""));
          
            $('#img-loading-money-beforesend').hide(); //loading tiền.
            break;
        case 1: //riêng sms không dùng  (tạm dùng xem sao )

            //sms
            $('#sendmoment #messagelength').text(smssend.MessageLength);
            $('#sendmoment #CostPerSMSViettel').text(data.CostPerSMSViettel);
            $('#sendmoment #SMSViettelCount').text(data.SMSViettelCount);
            $('#sendmoment #SMSViettelTotal').text(formatMoney(smssend.MessageLength * data.CostPerSMSViettel * data.SMSViettelCount, ''));


            $('#sendmoment #CostPerSMSMobi').text(data.CostPerSMSMobi);
            $('#sendmoment #SMSMobiCount').text(data.SMSMobiCount);
            $('#sendmoment #SMSMobiTotal').text(formatMoney(smssend.MessageLength * data.CostPerSMSMobi * data.SMSMobiCount, ''));

            $('#sendmoment #CostPerSMSVina').text(data.CostPerSMSVina);
            $('#sendmoment #SMSVinaCount').text(data.SMSVinaCount);
            $('#sendmoment #SMSVinaTotal').text(formatMoney(smssend.MessageLength * data.CostPerSMSVina * data.SMSVinaCount, ''));

            $('#sendmoment #CostPerSMS').text(data.CostPerSMS);
            var SMSAnotherCount = data.SMSRecipientCount - data.SMSViettelCount - data.SMSMobiCount - data.SMSVinaCount;
            $('#sendmoment #SMSAnotherCount').text(SMSAnotherCount);
            $('#sendmoment #SMSAnotherTotal').text(formatMoney(smssend.MessageLength * data.CostPerSMS * SMSAnotherCount, ''));

            $('#sendmoment #SMSTotalCost').text(formatMoney(data.SMSTotalCost, ''));


            $('#sendmoment #currentbalance').text(formatMoney(data.CurrentBalance, ""));
            $('#sendmoment #totalcost').text(formatMoney(parseInt(data.SMSTotalCost) + parseInt(data.EmailTotalCost), ""));

            $('#img-loading-money-beforesend').hide(); //loading tiền.
            break;
        case 2:

            $('#sendmoment #messagelength').text(smssend.MessageLength);
            $('#sendschedule #messagelength').text(smssend.MessageLength);

            $('#sendmoment #totalemailsend').text(formatMoney(data.EmailRecipientCount,""));
            $('#sendmoment #totalquotabefore').text(data.EmailQuotaRemainBeforeSend);
            $('#sendmoment #totalcostemail').text(formatMoney(data.EmailTotalCost, "") );
            $('#sendmoment #currentbalance').text(formatMoney(data.CurrentBalance,""));
            $('#sendmoment #costperemail').text(data.CostPerMail);
            $('#sendmoment #totalsmssend').text(formatMoney(data.SMSRecipientCount,""));
            if (parseInt(data.EmailQuotaRemainBeforeSend) - parseInt(data.EmailRecipientCount) > 0)
                $('#sendmoment #total-quota-after').text(parseInt(data.EmailQuotaRemainBeforeSend) - parseInt(data.EmailRecipientCount) + '/' + data.EmailTotalQuota);
            else
                $('#sendmoment #total-quota-after').text('0');
            
                      
            //sms
            $('#sendmoment #messagelength').text(smssend.MessageLength);
            $('#sendmoment #CostPerSMSViettel').text(data.CostPerSMSViettel);
            $('#sendmoment #SMSViettelCount').text(data.SMSViettelCount);
            $('#sendmoment #SMSViettelTotal').text(formatMoney(smssend.MessageLength * data.CostPerSMSViettel * data.SMSViettelCount, ''));


            $('#sendmoment #CostPerSMSMobi').text(data.CostPerSMSMobi);
            $('#sendmoment #SMSMobiCount').text(data.SMSMobiCount);
            $('#sendmoment #SMSMobiTotal').text(formatMoney(smssend.MessageLength * data.CostPerSMSMobi * data.SMSMobiCount, ''));

            $('#sendmoment #CostPerSMSVina').text(data.CostPerSMSVina);
            $('#sendmoment #SMSVinaCount').text(data.SMSVinaCount);
            $('#sendmoment #SMSVinaTotal').text(formatMoney(smssend.MessageLength * data.CostPerSMSVina * data.SMSVinaCount, ''));

            $('#sendmoment #CostPerSMS').text(data.CostPerSMS);
            var SMSAnotherCount = data.SMSRecipientCount - data.SMSViettelCount - data.SMSMobiCount - data.SMSVinaCount;
            $('#sendmoment #SMSAnotherCount').text(SMSAnotherCount);
            $('#sendmoment #SMSAnotherTotal').text(formatMoney(smssend.MessageLength * data.CostPerSMS * SMSAnotherCount, ''));

            $('#sendmoment #SMSTotalCost').text(formatMoney(data.SMSTotalCost,''));


            $('#sendmoment #currentbalance').text(formatMoney(data.CurrentBalance, ""));
            $('#sendmoment #totalcost').text(formatMoney(parseInt(data.SMSTotalCost) + parseInt(data.EmailTotalCost), ""));


            $('#img-loading-money-beforesend').hide(); //loading tiền.

            break;
    }

   
    //loading
    $('#divLoading').hide();
    

}

//ham kiểm tra tiền có đủ trả trước khi gửi 
function emailconfirm_CheckMoneyBeforeSend(message)
{
    //loading
    $('#divLoading').show();

    var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
    if (basiccampaign != '0' || basiccampaign != null) {
        var menthod = parseInt(basiccampaign.SendMethod);
        var emailsend = JSON.parse(JSON.stringify(basiccampaign.EmailSend));
        var smssend = JSON.parse(JSON.stringify(basiccampaign.SMSSend));
       
    }
    else {
        custom_shownotification('error', message.HaveError);
            return true;
           
    }
    if (menthod == 0)
    {
        if (emailsend.HTML == null || emailsend.HTML=='') {
            custom_shownotification('error', message.EmailContentError);
            $('#divLoading').hide();
            return false;
        }
        if(emailsend.ContactGroups.length==0)
        {
            custom_shownotification('error', message.SelectRecipientError);
            $('#divLoading').hide();
            return false;
        }
        if (parseInt(emailsend.TotalContact) == 0) {
            custom_shownotification('error', message.NoEmailContact);
            $('#divLoading').hide();
            return false;
        }
        if (acceppsend == false) {
            custom_shownotification('error', message.MoneyError);
            $('#divLoading').hide();
            return false;
        }

        else {
            return true;

        }
    }
    if (menthod ==1) {
        if (smssend.Message == '' || smssend.Message==null) {
            custom_shownotification('error', message.SMSContentError);
            $('#divLoading').hide();
            return false;
        }
        if (smssend.ContactGroups.length == 0) {
            custom_shownotification('error', message.SelectRecipientError);
            $('#divLoading').hide();
            return false;
        }
        if (parseInt(smssend.TotalContact) == 0) {
            custom_shownotification('error', message.NoSMSContact);
            $('#divLoading').hide();
            return false;
        }
        if (acceppsend == false) {
            custom_shownotification('error', message.MoneyError);
            $('#divLoading').hide();
            return false;
        }
        else {
            return true;

        }
    }
    if(menthod==2)
    {
        if (smssend.Message == '' || smssend.Message == null) {
            custom_shownotification('error', message.SMSContentError);
            $('#divLoading').hide();
            return false;
        }
        if (emailsend.HTML == ""||emailsend.HTML ==null) {
            custom_shownotification('error', message.EmailContentError);
            $('#divLoading').hide();
            return false;
        }
        if (emailsend.ContactGroups.length == 0) {
            custom_shownotification('error', message.SelectRecipientError);
            $('#divLoading').hide();
            return false;
        }
        if (emailsend.ContactGroups.length == 0) {
            custom_shownotification('error', message.SelectRecipientError);
            $('#divLoading').hide();
            return false;
        }
        if (acceppsend == false) {
            custom_shownotification('error', message.MoneyError);
            $('#divLoading').hide();
            return false;
        }
        else {
            return true;

        }
    }

    //loading
    $('#divLoading').hide();

    
    
}

//ham kiểm tra tiền có đủ trả trước khi gửi theo lich
function emailconfirm_CheckMoneyBeforeSendSchedule(message) {

    //loading
    $('#divLoading').show();

    var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
    if (basiccampaign != '0' || basiccampaign != null) {
        var menthod = parseInt(basiccampaign.SendMethod);
        var emailsend = JSON.parse(JSON.stringify(basiccampaign.EmailSend));
        var smssend = JSON.parse(JSON.stringify(basiccampaign.SMSSend));
    }
    else {
        custom_shownotification('error', message.HaveError);
        return true;

    }
    if (menthod == 0) {
        if (emailsend.HTML == null || emailsend.HTML == '') {
            custom_shownotification('error', message.EmailContentError);
            $('#divLoading').hide();
            return false;
        }
        if (emailsend.ContactGroups.length == 0) {
            custom_shownotification('error', message.SelectRecipientError);
            $('#divLoading').hide();
            return false;
        }
        if (acceppsend == false) {
            custom_shownotification('error', message.MoneyError);
            $('#divLoading').hide();
            return false;
        }
        else {
            return true;

        }
    }
    if (menthod == 1) {
        if (smssend.Message == '' || smssend.Message == null) {
            custom_shownotification('error', message.SMSContentError);
            $('#divLoading').hide();
            return false;
        }
        if (smssend.ContactGroups.length == 0) {
            custom_shownotification('error', message.SelectRecipientError);
            $('#divLoading').hide();
            return false;
        }

        if (acceppsend == false) {
            custom_shownotification('error', message.MoneyError);
            $('#divLoading').hide();
            return false;
        }
        else {
            return true;

        }
    }
    if (menthod == 2) {
        if (smssend.Message == '' || smssend.Message == null) {
            custom_shownotification('error', message.SMSContentError);
            $('#divLoading').hide();
            return false;
        }
        if (emailsend.HTML == "" || emailsend.HTML == null) {
            custom_shownotification('error', message.EmailContentError);
            $('#divLoading').hide();
            return false;
        }
        if (emailsend.ContactGroups.length == 0) {
            custom_shownotification('error', message.SelectRecipientError);
            $('#divLoading').hide();
            return false;
        }
        if (emailsend.ContactGroups.length == 0) {
            custom_shownotification('error', message.SelectRecipientError);
            $('#divLoading').hide();
            return false;
        }
        if (acceppsendschedule == false) {
            custom_shownotification('error', message.MoneyError);
            $('#divLoading').hide();
            return false;
        }
        else {
            return true;

        }
    }
   
}



function emailconfirm_preview(CampaignId)
{

    $('#iframepreviewmobile').attr('src', "/BasicCampaign/PreViewEmailContent?eId=" + CampaignId);
    

    $('#btnpreviewcampaign3').on('click', function () {
        $('#modalPreviewEmailBeforeSend').show();
       

        var height = $(window).outerHeight() - $('#custom-preview-header').outerHeight()-100;
        $('.bg-success-desktop').css('height', height);
        $('.bg-success-desktop .slimScrollDiv').css('height', height);
        $('.bg-success-desktop #content-desktop-preview-email').css('height', height);
        
        $('#content-desktop-preview-email').load("/BasicCampaign/PreViewEmailContentDesktop?eId="+CampaignId);
       
       
       // $('#content-mobile-preview').load("/BasicCampaign/PreViewEmailContent?eId="+CampaignId);
    })
    
    $('#item-preview-email-desktop').on('click', function () {
        $('.bg-success-desktop').show();
        $('.bg-success-mobile').hide();
    })
    $('#item-preview-email-mobile').on('click', function () {
        $('.bg-success-desktop').hide();
        $('.bg-success-mobile').show();
    });
    $('#btClosePreviewEmailContent').on('click', function () {
        //$('#content-desktop-preview').children().remove();
        //$('#content-mobile-preview').children().remove();
        $('#modalPreviewEmailBeforeSend').hide();
    })
}


function emailconfirm_ShowModalSendTest(message)
{
    $('#btnsendtestcampaign').on('click', function () {
      
            $('#modelSendTest').modal('toggle');
            var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
   
    });
}

function emailconfirm_SendTest(apiDomain,access_token,CampaignId,SendId,message)
{
    $('#btnConfirm-Send-Test').on('click', function () {
        if (emailconfirm_CheckEmailTest(message)) {
            var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
            if (basiccampaign != '0' && basiccampaign != null) {
                $('#divLoading').show();

                var html = emailconfirm_GetHtml(apiDomain, access_token, CampaignId, SendId, message);
                if (html != '') {
                    var data = {
                        HTML: html,
                        ToEmail: $('#email-send-test-campaign').val(),
                        MessageName: 'Send test campaign: +' + basiccampaign.CampaignName,
                        Subject: basiccampaign.EmailSend.Subject,
                        FromName: basiccampaign.EmailSend.FromName,
                        FromEmail: basiccampaign.EmailSend.FromEmail,
                        ReplyToEmail: basiccampaign.EmailSend.ReplyToEmail,
                        EmbedMailUrl: basiccampaign.EmailSend.EmbedMailUrl,
                        ShowPermissionReminder: basiccampaign.EmailSend.ShowPermissionRemider,
                        ShowUnsubscribeButton: basiccampaign.EmailSend.ShowUnsubscribeButton,
                        Facebook: basiccampaign.EmailSend.Facebook,
                        Twitter: basiccampaign.EmailSend.Twitter,
                    }


                    $.ajax({
                        url: apiDomain + 'api/EmailSend/Send',
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify(data),
                        beforeSend: function (request) {
                            request.setRequestHeader("Authorization", "Bearer " + access_token);
                        },
                        success: function (r) {
                            //loading
                            $('#divLoading').hide();
                            switch (parseInt(r.ErrorCode)) {
                                case 0:
                                    custom_shownotification('success', message.SendTestSuccess);
                                    $('#email-send-test-campaign').val('');
                                    $('#modelSendTest').modal('toggle');
                                    emailcontent_EstimateCost(apiDomain, access_token, CampaignId, message); //cập nhập lại số tiền dự tính
                                    userstats_LoadUserInfo(apiDomain, access_token) //cập nhập lại số tiền còn sau đã gửi
                                    userstats_LoadUsed(apiDomain, access_token)//cập nhập lại số email đã gửi
                                    break;
                                default:
                                    custom_shownotification('error', message.SendTestError);
                            }


                        },
                        error: function (x, e, s) {
                            //loading
                            $('#divLoading').hide();
                            custom_shownotification('error', message.HaveError);

                        }
                    });
                }
                else {
                    $('#divLoading').hide();
                    custom_shownotification('error', message.SendTestError);
                }
            }
           
                
            }

    })
}

function emailconfirm_GetHtml(apiDomain, access_token, CampaignId, SendId, message) {
    var html = $.ajax({
        url: apiDomain + 'api/EmailSend/GetInfo?SentId=' + SendId, type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        async: false,

    }).responseJSON;
    return html.Data.HTML;
}

function emailconfirm_CheckEmailTest(message)
{
    if($('#email-send-test-campaign').val() == "" || $('#email-send-test-campaign').val() == null)
    {
        custom_shownotification('error', message.EmailIsRequire);
        $('#divLoading').hide();
        return false;
    }
    if (!IsEmail($('#email-send-test-campaign').val())) {
        custom_shownotification('error', message.EmailFormatError);
        $('#divLoading').hide();
        return false;
    }
    if (acceppsend == false) {
        custom_shownotification('error', message.MoneyError);
        $('#divLoading').hide();
        return false;
    }
    else {
        $('#divLoading').hide();
        return true;
    }
}


function emailconfirm_GenTimeSelect() {
    $('#select-hour-schuedule').children().remove();
    var option = '<option value="">Giờ</option>';
    for (var i = 0; i < 24; i++) {
        if (i < 10) {
            option += '<option value="0' + i + '">0' + i + '</option>';
        } else {
            option += '<option value="' + i + '">' + i + '</option>';
        }
    }
    $('#select-hour-schuedule').append(option);

    $('#select-minus-schuedule').children().remove();
    var opt = '<option value="">Phút</option>';
    for (var i = 0; i < 60; i++) {
        if (i < 10) {
            opt += '<option value="0' + i + '">0' + i + '</option>';
        } else {
            opt += '<option value="' + i + '">' + i + '</option>';
        }

    }
    $('#select-minus-schuedule').append(opt);
}
