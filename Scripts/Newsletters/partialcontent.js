﻿/// <reference path="../appshare.js" />
function emailcontent_SysTemplateCube() {

    //systemplate
    $('#js-grid-sys-template').cubeportfolio({
        filters: '#js-filters-juicy-projects',
        loadMore: '#js-loadMore-sys-template',
        loadMoreAction: 'click',
        layoutMode: 'grid',
        defaultFilter: '*',
        animationType: 'quicksand',
        gapHorizontal: 35,
        gapVertical: 30,
        gridAdjustment: 'responsive',
        mediaQueries: [{
            width: 1500,
            cols: 6
        }, {
            width: 1100,
            cols: 6
        }, {
            width: 800,
            cols: 5
        }, {
            width: 480,
            cols: 3
        }, {
            width: 320,
            cols: 2
        }],
        caption: 'overlayBottomReveal',
        displayType: 'sequentially',
        displayTypeSpeed: 80,

        // lightbox
        lightboxDelegate: '.cbp-lightbox',
        lightboxGallery: true,
        lightboxTitleSrc: 'data-title',
        lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

    });

}
function emailcontent_TemplateCube() {
    // init cubeportfolio
    $('#js-grid-juicy-projects').cubeportfolio({
        filters: '#js-filters-juicy-projects',
        loadMore: '#js-loadMore-juicy-projects',
        loadMoreAction: 'click',
        layoutMode: 'grid',
        defaultFilter: '*',
        animationType: 'quicksand',
        gapHorizontal: 35,
        gapVertical: 30,
        gridAdjustment: 'responsive',
        mediaQueries: [{
            width: 1500,
            cols: 6
        }, {
            width: 1100,
            cols: 6
        }, {
            width: 800,
            cols: 5
        }, {
            width: 480,
            cols: 3
        }, {
            width: 320,
            cols: 2
        }],
        caption: 'overlayBottomReveal',
        displayType: 'sequentially',
        displayTypeSpeed: 80,

        // lightbox
        lightboxDelegate: '.cbp-lightbox',
        lightboxGallery: true,
        lightboxTitleSrc: 'data-title',
        lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

    });


}


function emailcontent_LoadMore(apiDomain, access_token, message) {
    $('#js-loadMore-juicy-projects').on('click', function () {

        emailcontent_LoadMyTemplate(apiDomain, access_token, message)
    })

    $('#js-loadMore-sys-template').on('click', function () {
        emailcontent_LoadSysTemplate(apiDomain, access_token, message)
    })


}


function emailcontent_FixShowCube() {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(window).trigger('resize');
    });
}



function emailcontent_GetTemplateInfo(apiDomain, access_token, TemplateId, message) {
    var html = $.ajax({
        url: apiDomain + 'api/template/GetInfo?TemplateId=' + TemplateId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        async: false,

    }).responseJSON;

    return html;
}

var LastSysTemId = null;
var IsScrollSysTemplate = true;
function emailcontent_LoadSysTemplate(apiDomain, access_token, message) {
    var Count = 20;
    $.ajax({
        url: apiDomain + 'api/template/GetSystemList?Count=' + Count + '&LastTemplateId=' + LastSysTemId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    if (r.Data.length > 0) {
                        var htmlContent = '';
                        $.each(r.Data, function (i, o) {
                            if (i == r.Data.length - 1) {
                                LastSysTemId = o.TemplateId;
                            }
                            var SnapShotUrl = '/Content/Images/thumbnail-generator.png';
                            if (o.SnapShotUrl != null && o.SnapShotUrl != '') {
                                SnapShotUrl = o.SnapShotUrl;
                            }
                            htmlContent += '<div class="cbp-item" data-tid=' + o.TemplateId + '>' +
                                       '<div class="cbp-caption">' +
                                       '<div class="cbp-caption-defaultWrap">' +
                                            '<img src="' + SnapShotUrl + '" alt="">' +
                                       '</div>' +
                                      ' <div class="cbp-caption-activeWrap">' +
                                      ' <div class="cbp-l-caption-alignCenter">' +
                                       '<div class="cbp-l-caption-body">' +
                                      ' <a id="select-use-template" data-id="' + o.TemplateId + '" class="cbp-l-caption-buttonLeft btn btn-sm green-jungle"><i class="fa fa-hand-pointer-o"></i>'+message.Select+'</a>' +
                                       '<a href="' + SnapShotUrl + '" class="cbp-lightbox cbp-l-caption-buttonRight btn btn-sm blue" data-title="System template<br>' + o.TemplateName + '"><i class="fa fa-eye"></i>'+message.View+'</a>' +
                                      ' </div>' +
                                       '</div>' +
                                      ' </div>' +
                                      ' </div>' +
                                       '<div class="cbp-l-grid-projects-title text-center" title="' + o.TemplateName + '">' + o.TemplateName + '</div>' +
                                     '  </div>';



                        });
                        $('#js-grid-sys-template').cubeportfolio('appendItems', htmlContent);
                        $('#js-grid-sys-template #loadMore-noMoreLoading').hide();
                        $('#js-loadMore-sys-template').show();
                        $('#warning-not-systemplate').hide();
                        if (r.Data.length < Count) {
                            IsScrollSysTemplate = false;
                            $('#js-loadMore-sys-template').hide();
                        }
                    }
                    else {
                        IsScrollSysTemplate = false;
                        if (LastSysTemId == null) {
                            $('#warning-not-systemplate').show();
                            $('#js-loadMore-sys-template .cbp-l-loadMore-link').hide();
                        }
                    }


                    $('#divLoading').hide();
                    break;
                default:
                    custom_shownotification('error', 'Lỗi lấy danh sách template hệ thống');
                    break;

            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', 'Lỗi lấy danh sách template hệ thống');
        }
    })
}

var LastMyTempId = null;
var IsScrollMytemplate = true;
function emailcontent_LoadMyTemplate(apiDomain, access_token, message) {
    var Count = 20;
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/template/GetList?Publish=-1&Count=' + Count + '&LastTemplateId=' + LastMyTempId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {

            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var htmlContent = '';
                    if (r.Data.length > 0) {
                        $.each(r.Data, function (i, o) {
                            if (i == r.Data.length - 1) {
                                LastMyTempId = o.TemplateId;
                            }
                            var SnapShotUrl = '/Content/Images/thumbnail-generator.png';
                            if (o.SnapShotUrl != null && o.SnapShotUrl != '') {
                                SnapShotUrl = o.SnapShotUrl;
                            }
                            htmlContent += '<div class="cbp-item" data-tid=' + o.TemplateId + '>' +
                                       '<div class="cbp-caption">' +
                                       '<div class="cbp-caption-defaultWrap">' +

                                            '<img id="img' + o.TemplateId + '" style="cursor: pointer;"  href="' + SnapShotUrl + '" class="cbp-lightbox" src="' + SnapShotUrl + '" alt="">' +

                                       '</div>' +
                                      ' <div class="cbp-caption-activeWrap">' +
                                      ' <div class="cbp-l-caption-alignCenter">' +
                                       '<div class="cbp-l-caption-body">' +
                                      //'<a id="select-delete-template" data-id="' + o.TemplateId + '" class="cbp-l-caption-buttonRight btn red uppercase"><i class="fa fa-eye"></i>Xóa <a>' +
                                      //  '<a id="select-view-template" data-id="' + o.TemplateId + '" class="cbp-l-caption-buttonRight btn yellow-gold uppercase"><i class="fa fa-eye"></i>Sửa <a>' +
                                      '<a id="select-use-template" data-id="' + o.TemplateId + '" class="btn btn-sm green-jungle"><i class="fa fa-hand-pointer-o"></i>'+message.Select+'</a>' +
                                //       '<a href="' + o.SnapShotUrl + '" class="cbp-lightbox cbp-l-caption-buttonRight btn blue uppercase" data-title="My template<br>' + o.TemplateName + '"><i class="fa fa-eye"></i>Xem</a>' +
                                      ' </div>' +
                                       '</div>' +
                                      ' </div>' +
                                      ' </div>' +
                                       '<div class="cbp-l-grid-projects-title text-center" title="' + o.TemplateName + '">' + o.TemplateName + '</div>' +
                                     '  </div>';



                        });
                        $('#js-grid-juicy-projects').cubeportfolio('appendItems', htmlContent);
                        $('#js-grid-juicy-projects #loadMore-noMoreLoading').hide();
                        $('#js-loadMore-juicy-projects').show();
                        $('#warning-not-mytemplate').hide();
                        if (r.Data.length < Count) {
                            IsScrollMytemplate = false;
                            $('#js-loadMore-juicy-projects').hide();
                        }
                    }
                    else {
                        IsScrollMytemplate = false;
                        if (LastMyTempId == null) {
                            $('#warning-not-mytemplate').show();
                            $('#js-loadMore-juicy-projects .cbp-l-loadMore-link').hide();
                        }
                    }

                    $('#divLoading').hide();
                    break;
                default:
                    custom_shownotification('error', 'Lỗi lấy danh sách template cá nhân');
                    $('#divLoading').hide();
                    break;

            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', 'Có lỗi xảy ra');
        }
    })
}

function partialcontent_AutoScroll(apiDomain, access_token, message) {
    $('#content-scroll-sys-template').bind('slimscroll', function (e, pos) {
        if (pos == 'bottom') {
            if (IsScrollSysTemplate) {
                emailcontent_LoadSysTemplate(apiDomain, access_token, message)
            }

        }
    });

    $('#content-scroll-template').bind('slimscroll', function (e, pos) {
        if (pos == 'bottom') {
            if (IsScrollMytemplate) {
                emailcontent_LoadMyTemplate(apiDomain, access_token, message);
            }

        }
    });

}


//ham lay thong tin device
function emailcontent_GetDeviceId(apiDomain, access_token,message)
{

    var data = appshare_GetGatewayConfigList(apiDomain, access_token, message);
    if (data.Data.length > 0) {
        //$('#select-devicenumber').removeClass('hide');
        var html = '';
        var obj = data.Data;
        console.log(obj);
        $.each(obj, function (i, o) {
            html += '<option value="' + o.ItemId + '" data-default="' + o.DefaultDeviceToken.ItemId + '" data-mobile="' + o.MobifoneDeviceToken.ItemId + '" data-vina="' + o.VinaphoneDeviceToken.ItemId + '"  data-viettel="' + o.ViettelDeviceToken.ItemId + '">' + o.ConfigName + '</option>';
        });

        $('#select-gatewayid').append(html);

    } else {
        var html = '<option value="-1">' + message.NoConfig + '</option>'
        $('#select-gatewayid').append(html);

        //$('#select-gateway-type').hide();
    }
}

//hàm lấy thông tin brandname sms
function emailcontent_GetSMSBrandName(apiDomain, access_token, message) {
    $.ajax({
        url: apiDomain + 'api/User/GetSMSBrandNames',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            var code = parseInt(r.ErrorCode);
            switch (code) {
                case 0:
                    if (r.Data.length > 0) {
                        var html = "";
                        $.each(r.Data, function (i, o) {
                            html += '<option value=' + o.BrandNameId + '>' + o.BrandName + '</option>';
                        });
                        $('#select-brandname').append(html);
                    }
                    else {
                      var  html = '<option value="-1">'+message.NoBrandName+'</option>';
                      $('#select-brandname').append(html);
                      $('#register-brandname').show();
                        //$('#select-brandname-type').addClass('hide');
                    }
                    break;
                default:
                    custom_shownotification('error', message.GetBrandNameError);
                    break;

            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.GetBrandNameError);
        }

    })
}



function emailcontent_CloseTemplate() {
    $('#btCloseTemplatContent').on('click', function () {
        $('#modalLoadTemplateContent').hide();
    });

    $('#btCloseEmailBuilderContent').on('click', function () {
        $('#modalLoadEmailBuilderContent').hide();
    });


}



function emailcontent_IframeBuilder(apidBuilder,Id, Type) {
    $('#divLoading').show();
    var height = parseInt($(window).height());
    $('#iframe-emailbuilder').css('height', height);
    $('#iframe-emailbuilder').attr('src', apidBuilder+'editor.html?t=' + Type + '&id=' + Id);
    $('#iframe-emailbuilder').load(function () {
        $('#divLoading').hide();
    });
}

function emailcontent_ShowBrandName(message) {
    $('#Performance-sendsms').text('40-70%');
    $('#divsmsservice').show();
    $("input:radio[name='smstype']").change(function () {
        if (parseInt($("input:radio[name='smstype']:checked").val()) == 2) {
            $('#Performance-sendsms').text('85-95%');
            $('#sms-brand-name').removeClass('hide');
            if (!$('#sms-gateway-name').hasClass('hide')) {
                $('#sms-gateway-name').addClass('hide');
            }
            $('input#smskhongdau').trigger('click'); //bật nhập không dấu
            $('#select-sms-input-codau').hide() //ản nhập có nhập dấu
            $('#divsmsservice').show();
            $('#note-send-webapp').show();
            $('#note-send-getway').hide();
        }
        if (parseInt($("input:radio[name='smstype']:checked").val()) == 1) {
            if (!$('#sms-brand-name').hasClass('hide')) {
                $('#sms-brand-name').addClass('hide');
                
            }
            if (!$('#sms-gateway-name').hasClass('hide')) {
                $('#sms-gateway-name').addClass('hide');
            }
            $('#divsmsservice').show();
            $('input#smskhongdau').trigger('click'); //bật nhập không dấu
            $('#select-sms-input-codau').hide() //ản nhập dấu

            $('#note-send-webapp').show();
            $('#note-send-getway').hide();
        }

        if (parseInt($("input:radio[name='smstype']:checked").val()) == 3) {
            $('#Performance-sendsms').text('40-70%');
            if (!$('#sms-brand-name').hasClass('hide')) {
                $('#sms-brand-name').addClass('hide');
            }
            if ($('#sms-gateway-name').hasClass('hide')) {
                $('#sms-gateway-name').removeClass('hide');
            }
            $('#note-send-webapp').hide();
            $('#note-send-getway').show();
            $('#divsmsservice').hide();
           $('#select-sms-input-codau').show() //cho nhập có dấu;
            
        }

        UpdateMessageCount(message);
    });
}

function emailcontent_CheckMessage(translate) {
    var str = $('#Message').val();
    console.log(AllowMsgCount);
    if (!AllowMsgCount) {
        custom_shownotification('error', 'Quá số lượng kí tự cho phép');
        return false;
    }
    if ($('#Message').val()) {
        if (parseInt($("input:radio[name='smstype']:checked").val()) == 2) {

            if (parseInt($("#select-brandname").val())==-1) {
                custom_shownotification('error', translate.NoBrandName);
                $('#divLoading').hide();
                return false;
            }
        }
        if (parseInt($("input:radio[name='smstype']:checked").val()) == 3 && parseInt($('#select-gatewayid').val())==-1) {
            custom_shownotification('error', translate.NoConfig);
                $('#divLoading').hide();
                return false;
            
        }

        if (parseInt($("input:radio[name='smstype']:checked").val()) == 3 && isNaN(parseInt($("#select-gatewayid").val()))) {

            custom_shownotification('error', message.NoConfig);
            $('#divLoading').hide();
            return false;
        }

        //if (parseInt($("input:radio[name='smstype']:checked").val())) {

        //    for (var i = 0, len = str.length; i < len; i++) {
        //        if (parseInt(str[i].charCodeAt(0)) > 127) {
        //            custom_shownotification('error', translate.Error);
        //            $('#divLoading').hide();
        //            return false;
        //        }
        //    }
        //    return true;
        //}
        else {
            return true;
        }

    }
    else {
        custom_shownotification('error', translate.NoContentSMS);
        $('#divLoading').hide();
        return false;
    }
}
var MsgCount;
var AllowMsgCount=true; 
function UpdateMessageCount(message) {

    var $this = $("#Message");
    var smsinputtype = parseInt($('input[name=smsinputtype]:checked').val());
    charLength = $this.val().length + appshare_SMSSymbolLength($this.val())
    //$.each(LISTPERSOLINATE, function (i, o) {
    //    if ($this.val().indexOf(o) != -1) {
    //        var re = new RegExp(o, 'g');
    //        var number = $this.val().match(re).length;
    //        charLength = charLength - number * o.length + number * LENGTH_PERSOLINATE_SMS;
    //    }
    //});


    if (parseInt($("input:radio[name='smstype']:checked").val()) == 1) {
        MsgCount = appshare_OTPNumber(charLength);
        if (charLength > LENGTHMAXOTP) {
            custom_shownotification('error', 'Tin nhắn tối đa 422 kí tự');
            AllowMsgCount = false;
        } else {
            AllowMsgCount = true;
        }
        var html = appshare_MsgLenghtCount(1, message);
        $("#MsgLengthCount").html("<b style='color:#8EBC00'>" + charLength + "</b> " + message.Character + " = <b style='color:#E84444'>" + MsgCount + "</b> " + message.SMS + html);

    }
    else if (parseInt($("input:radio[name='smstype']:checked").val()) == 2) {
        MsgCount = appshare_BrandNumber(charLength);
        if (charLength > LENGTHMAXBRAND) {
            custom_shownotification('error', 'Tin nhắn tối đa 611 kí tự');
            AllowMsgCount = false;
        } else {
            AllowMsgCount = true;
        }
        var html = appshare_MsgLenghtCount(2, message);
        $("#MsgLengthCount").html("<b style='color:#8EBC00'>" + charLength + "</b> " + message.Character + " = <b style='color:#E84444'>" + MsgCount + "</b> " + message.SMS + html);

    }
    else {
        var html = appshare_MsgLenghtCount(3, message);
        var smsinputtype = parseInt($("input:radio[name='smsinputtype']:checked").val());
        var obj = appshare_GateWayNumberAccented(smsinputtype, $this.val());
        MsgCount = obj.messages;
        $("#MsgLengthCount").html("<b style='color:#8EBC00'>" + charLength + " / " + obj.per_message + "</b> " + message.Character + " = <b style='color:#E84444'>" + MsgCount + " </b>" + message.SMS + html);
    }

    $('.popovers').popover();
};


function editcontent_EditSMS(message) {
    //$("#Message").bind("keydown", function (e) {
    //    console.log(e.key);
    //    if (parseInt(e.which) > 127 || parseInt(e.keyCode) > 127) //kiểm tra charcode có lớn hơn 127 thì báo lỗi
    //    {
    //        var kd = custom_bodauTiengViet(e.key);
    //        custom_InsertTextAtCaret('Message', kd);
    //        e.preventDefault();
    //    }
    //});

    $("#Message").bind("keypress, keyup", function (e) {
        editcontent_ChangeAccent();
        UpdateMessageCount(message);
    });
}

function editcontent_ChangeTypeInput(message)
{
    $("input:radio[name='smsinputtype']").change(function () {
        editcontent_ChangeAccent();
        UpdateMessageCount(message);
    });
}

function editcontent_ChangeAccent()
{
    var value = $("#Message").val();
    $("#Message").empty();
    var smsinputtype = parseInt($('input[name=smsinputtype]:checked').val());
    if (smsinputtype == 1) {
        $("#Message").val(custom_bodauTiengViet(value))
    }
    else {
        $("#Message").val(value);
    }
}

function editcontent_AddPersonalize(IdAdd, message) {
    $('#div-personalize-subject-sms').on('click', '.data-personalize', function () {
        var personalize = $(this).text();
        custom_InsertTextAtCaret(IdAdd, personalize);

        //nếu là sms thì update sms
        UpdateMessageCount(message);
    });

}


