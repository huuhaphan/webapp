﻿/// <reference path="partialcontent.js" />

//load thông tin nếu là update
function emailcontent_LoadInfo(apiDomain, access_token, message, CampaignId) {
    
    var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
    if (basiccampaign != '0' && basiccampaign != null) {
        emailcontent_Data(basiccampaign, message);
    }
};


function emailcontent_CampaignInfo(apiDomain, access_token, CampaignId, imgUrl, message) {
    $('.img-loading-content').show();
    
    $.ajax({
        url: apiDomain + 'api/BasicCampaign/GetInfo?CampaignId=' + CampaignId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {

            switch (parseInt(r.ErrorCode)) {
                case 0:
                    $('#divLoading').hide();
                    localStorage.setItem('BasicCampaign', JSON.stringify(r.Data));
                    var basiccampaign = r.Data;
                    localStorage.setItem('BasicCampaign', JSON.stringify(basiccampaign));
                    var emailsend = JSON.parse(JSON.stringify(basiccampaign.EmailSend));
                    //reload view 
                    emailcontent_ReloadAffterSave(basiccampaign);
                    LastMyTempId = null;
                    LastSysTemId = null;

                   
                    //reload my template
                    if (IsEditTemplate != null)
                    {
                        emailcontent_DestroyCube();
                        LastMyTempId = null;
                        emailcontent_TemplateCube();
                        emailcontent_LoadMyTemplate(apiDomain, access_token, message);
                        IsEditTemplate = null;
                    }
                    break;
                default:

                    break;
            }
        }, error: function (x,s,e) {
            $('.img-loading-content').hide();
        }
    });
}

function emailcontent_Data(basiccampaign, message) {
    $('#divLoading').show();
    var sendmendthod = parseInt(basiccampaign.SendMethod);
   
  
    var emailsend = JSON.parse(JSON.stringify(basiccampaign.EmailSend));
    var smssend = JSON.parse(JSON.stringify(basiccampaign.SMSSend));
    var IsMailBuilder = emailsend.IsMailBuilder;
    emailcontent_ShowType(sendmendthod, IsMailBuilder);
    var height = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.mt-element-step').outerHeight() - $('#divRedirect').outerHeight() - $('.page-footer').outerHeight() -$('.nav-tabs').outerHeight() - 50;

    $('#content-scroll-template').slimScroll({
        height: height - 70,
        alwaysVisible: true
    });
    $('#content-scroll-sys-template').slimScroll({
        height: height - 70,
        alwaysVisible: true
    })

    $('#porlet-edit-email-content').css('height', height + 'px');
   

    if (emailsend.HTML != "" && emailsend.HTML != "<br>") {
        $('#porlet-edit-email-content').show();
        $('#portlet-create-email-content').hide();
        $('#content-email-to-div img').attr('src', emailsend.SnapShotUrl);
        $('#img-snapshot-content-email').bind('error', function () {
            $(this).attr("src", "/Content/Images/thumbnail-generator.png");
        });
        $('#content-email-to-div img').css('height', (height - 50) + 'px');
        //$('#porlet-edit-email-content #content-email-to-div').slimScroll({
        //    height: height - $('#div-edit-email').outerHeight() - 50,
        //    alwaysVisible: true
        //});
       
    }


    else
    {
        $('#porlet-edit-email-content').hide();
        $('#portlet-create-email-content').show();
        $('#tab-select-type-edit-email').css('height', height-50);
    }



    if (IsMailBuilder === true || IsMailBuilder === 'true') {
        $('#editbydragg').css('display','block');
        $('#tryeditbyinput').css('display', 'block');
        $('#createtemplate').css('display', 'block');
        $('#resetallcontent').css('display', 'block');
    }
    else {
        $('#editbyinput').css('display', 'block');
        $('#resetallcontent').css('display', 'block');
    }

    $('#Message').val(smssend.Message);

    UpdateMessageCount(message);

    var SMSTypeId = smssend.SMSTypeId;
    var BrandNameId = smssend.BrandNameId;
    var DeviceTokenItemId=smssend.DeviceTokenItemId
    $('input:radio[name="smstype"][value="' + SMSTypeId + '"]').trigger('click');
    if (parseInt(SMSTypeId) == 2) {
        $('#sms-brand-name').removeClass('hide');
          $('#select-brandname').val(BrandNameId);
    }
   if (parseInt(SMSTypeId) == 3) {
       $('#sms-gateway-name').removeClass('hide');
        $('#select-gatewayid').val(smssend.GatewayConfigId);

        $('#select-sms-input-codau').show();
        if (custom_matchTiengViet(smssend.Message) != null) { //có dấu
            $('input#smscodau').trigger('click');
        }
    }
  



    $('#divLoading').hide();
}


function emailcontent_ReloadAffterSave(basiccampaign)
{

    var sendmendthod = parseInt(basiccampaign.SendMethod);
    var emailsend = JSON.parse(JSON.stringify(basiccampaign.EmailSend));
    var smssend = JSON.parse(JSON.stringify(basiccampaign.SMSSend));
    var IsMailBuilder = emailsend.IsMailBuilder;
    emailcontent_ShowType(sendmendthod, IsMailBuilder);
    var height = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.mt-element-step').outerHeight() - $('#divRedirect').outerHeight() - $('.page-footer').outerHeight()  - $('.nav-tabs').outerHeight() - 50;
    console.log('height:'+height);
    $('#content-scroll-template').slimScroll({
        height: height - 50,
        alwaysVisible: true
    });
    $('#content-scroll-sys-template').slimScroll({
        height: height - 50,
        alwaysVisible: true
    })

    $('#porlet-edit-email-content').css('height', height + 'px');
    
    if (emailsend.HTML != "" && emailsend.HTML != "<br>") {
        $('#porlet-edit-email-content').show();
        $('#portlet-create-email-content').hide();

        $('#content-email-to-div img').css('height', (height - 50) + 'px');

        setTimeout(function () {
            $('.img-loading-content').hide();
            $('#porlet-edit-email-content #content-email-to-div img').attr('src', emailsend.SnapShotUrl);
            $('#img-snapshot-content-email').bind('error', function () {
                $(this).attr("src", "/Content/Images/thumbnail-generator.png");
                //$('#porlet-edit-email-content #content-email-to-div').slimScroll({ destroy: true });
                //$('#porlet-edit-email-content #content-email-to-div').slimScroll({
                //    height: height - $('#div-edit-email').outerHeight(),
                //    alwaysVisible: true
                //});
            });
           
            // $('#img-snapshot-content-email').bind('success', function () {
            //     $('#porlet-edit-email-content #content-email-to-div').slimScroll({ destroy: true });
            //     $('#porlet-edit-email-content #content-email-to-div').slimScroll({
            //         height: height - $('#div-edit-email').outerHeight(),
            //         alwaysVisible: true
            //     });
            //});
            
        }, 3000);
                  
    }
    else {
        $('#porlet-edit-email-content').hide();
        $('#portlet-create-email-content').show();
        $('#tab-select-type-edit-email').css('height', height - 50);
    }

    $('#editbydragg').hide();
    $('#tryeditbyinput').hide();
    $('#createtemplate').hide();
    $('#editbyinput').hide();
    $('#resetallcontent').hide();

    if (IsMailBuilder === true || IsMailBuilder === 'true') {
        $('#editbydragg').css('display','block');
        $('#tryeditbyinput').css('display', 'block');
        $('#createtemplate').css('display', 'block');
        $('#resetallcontent').css('display', 'block');
    }
    else {
        $('#editbyinput').css('display', 'block');
        $('#resetallcontent').css('display', 'block');


    }

   
   
    
}


$(window).resize(function () {
    var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
    if (basiccampaign != '0' && basiccampaign != null) {
        emailcontent_ReloadAffterSave(basiccampaign);
    }
   
    
});

//cac sự kiện
function emailcontent_ActionButton(apiDomain, apidBuilder, access_token, CamId, userId, IdAdd, imgUrl, message) {


    //load sms brandname 
    emailcontent_GetSMSBrandName(apiDomain, access_token, message);
    //load device
    emailcontent_GetDeviceId(apiDomain, access_token, message);


    emailcontent_LoadInfo(apiDomain, access_token, message, CamId);

    if (localStorage.getItem('BasicCampaign')) {
        var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
        if (basiccampaign != '0' && basiccampaign != null) {
            var CampaignId = basiccampaign.CampaignId;
            var SendEmail = JSON.parse(JSON.stringify(basiccampaign.EmailSend))
            var EmailSendId = SendEmail.SentId;
            var SMSSendId = custom_GetSentSMSId();
            var SendSMS = JSON.parse(JSON.stringify(basiccampaign.SMSSend))
            var SentSMSId = SendSMS.SentId;
            var UserId = basiccampaign.UserId;

            emailcontent_TemplateCube();
            
            emailcontent_SysTemplateCube();
           
           
            emailcontent_FixShowCube();
            //sư kiên click trên col step by step
            custom_ActionOfColStep(CampaignId, message.NoExistNewsLetterId);

            //reload nội dung lên editor
            emailcontent_EditEmail();

            //check sms nào hoạt động
            appshare_GetSMSServiceEnables(apiDomain, access_token, message);

            //hiển thị brandname
            emailcontent_ShowBrandName(message);

            //action nut back 
            emailcontent_ActionBack(CampaignId);


            //page title click
            custom_PageTitleClick();

            // keyup blur
            editcontent_EditSMS(message);

            //changetype input
            editcontent_ChangeTypeInput(message);

            //addpersolinate to sms
            editcontent_AddPersonalize(IdAdd, message);

            //xử lý next và lưu nội dung sms nếu có
            emailcontent_ActionNext(apiDomain, access_token, SMSSendId, CampaignId, message);

            //xử lý lưu
            emailcontent_ActionSave(apiDomain, access_token, SMSSendId, CampaignId,  message);

            //open templete
            emailcontent_OpenTemplate(apiDomain, apidBuilder, access_token, EmailSendId, CampaignId)

            //close templete
            emailcontent_CloseTemplate();

            emailcontent_LoadEmailBuilder(apiDomain, apidBuilder, access_token, EmailSendId, CampaignId);

            emailcontent_LoadEmailEditor(EmailSendId);

            emailcontent_OpenModalConfirmReset();
            emailcontent_ResetGoToBuilder(apiDomain, access_token, EmailSendId, CampaignId);

            emailcontent_OpenModalConfirmTryHtml();

            emailcontent_ChangeHtmlEmail(apiDomain, access_token, EmailSendId, CampaignId);

            emailcontent_OpenModalCreateTemplate();

            emailcontent_CreateTemplate(apiDomain, access_token, EmailSendId, CampaignId, UserId, imgUrl,message);

            emailcontent_EditTemplate(apiDomain, access_token, EmailSendId, message);

            emailcontent_LoadMyTemplate(apiDomain, access_token, message);

            emailcontent_LoadSysTemplate(apiDomain, access_token, message);

            emailcontent_LoadMore(apiDomain, access_token, message);

            emalcontent_UseTemplate(apiDomain, apidBuilder, access_token, EmailSendId, CampaignId, message);

            emailcontent_OpenModalDeleteTemplate(apiDomain, access_token, EmailSendId, message);

            emailcontent_DelelteTemplate(apiDomain, access_token, EmailSendId, message);

            partialcontent_AutoScroll(apiDomain, access_token, message);


        }
    }
}




function emailcontent_LoadEmailBuilder(apiDomain, apidBuilder, access_token, SentId, CampaignId) {
    $('#editbydragg').on('click', function () {
        $('#modalLoadEmailBuilderContent').show();
        var Type = 'c'; //campaign;
        emailcontent_IframeBuilder(apidBuilder,SentId, Type)
    });
}

//sự kiện click edit email
function emailcontent_EditEmail()
{
        $('#divLoading').show();
    $('#imgecontent').on('click', function () {
        if (localStorage.getItem('BasicCampaign')) {
            var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
            if (basiccampaign != '0' && basiccampaign != null) {
                var EmailSend=JSON.parse(JSON.stringify(basiccampaign.EmailSend))
                $('#contentInput').summernote('code', EmailSend.HTML)
            }

        };
        $('#divLoading').hide();
    })
}


//quay lại
function emailcontent_ActionBack(CampaignId)
{
    $("#btnContentBack").on("click", function () {
        if (CampaignId) {
            $('#divLoading').show();
            window.location.href = '/BasicCampaign/Setting?eId=' + CampaignId;
        }
        else {
            $('#divLoading').show();
            window.location.href = '/BasicCampaign';
        }

    });
}



function emailcontent_LoadEmailEditor(EmailSendId) {
    $('.type-input-content.type-input-text , #editbyinput').on('click', function () {

        $('#modalLoadEmailEditorContent').show();
        var Type = 'c';
        emailcontent_IframeEditor(EmailSendId,Type);
    });
}

function emailcontent_IframeEditor(Id, Type)
{
    var height = parseInt($(window).height() - $('#header-email-builder').outerHeight());

    $('#iframe-emaileditor').css('height', height);
    $('#iframe-emaileditor').attr('src', '/EmailEditor?Type=' + Type + '&Id=' + Id);
}


function emailcontent_ActionSave(apiDomain, access_token, SMSSentId, CampaignId, message) {
    $("#btnContentSave").on('click', function () {

        var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
        var sendmendthod;
        if (basiccampaign != '0' && basiccampaign != null) {
            sendmendthod = parseInt(basiccampaign.SendMethod);

        }
        //nếu có dữ liệu sms lưu sms
        if (parseInt(sendmendthod) == 2 || parseInt(sendmendthod)==1) {
            if (emailcontent_CheckMessage(message)) {
                var data = emailsend_DataUpdateSMS(SMSSentId);

                var IsNext = false;
                emailsend_UpdateSMS(apiDomain, access_token, data, IsNext, message)
            }
        }
        else {
            $('#divLoading').hide();
            custom_shownotification('success', message.SaveSuccess);
        }

    });
}

function emailcontent_ActionNext(apiDomain, access_token, SMSSentId, CampaignId, message) {

    $("#btnContentNext").on('click', function () {
       
        //nếu có dữ liệu lưu sms trước khi tiếp tục (neu có nội dung)
        var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
        var sendmendthod;
        if (basiccampaign != '0' && basiccampaign != null) {
            sendmendthod = parseInt(basiccampaign.SendMethod);
        }
        if (parseInt(sendmendthod) == 0) {
            $('#divLoading').show();
            window.location.href = '/BasicCampaign/Recipient?eId=' + CampaignId
        }
        else {
            if (emailcontent_CheckMessage(message)) {
                var data = emailsend_DataUpdateSMS(SMSSentId);
                $('#divLoading').show();
                var IsNext = true;
                emailsend_UpdateSMS(apiDomain, access_token, data, IsNext, message)
            }
        }

    });
}

function emailsend_DataUpdateSMS(SMSSentId)
{
    var data = {};
    if (parseInt($("input:radio[name='smstype']:checked").val()) == 1) {
        data = {
            SMSSentId: SMSSentId,
            Message: custom_bodauTiengViet($('#Message').val()),
            MessageLength: MsgCount,
            SMSTypeId: parseInt($("input:radio[name='smstype']:checked").val()),

            //  IsTest: true,
        };
    }
    if (parseInt($("input:radio[name='smstype']:checked").val()) == 2) {
        data = {
            SMSSentId: SMSSentId,
            Message: $('#Message').val(),
            MessageLength: MsgCount,
            SMSTypeId: parseInt($("input:radio[name='smstype']:checked").val()),
            //  IsTest: true,
            BrandNameId: parseInt($("#select-brandname option:selected").val()),
            BrandName: $("#select-brandname option:selected").text(),
        };
    }
    else {
        data = {
            SMSSentId: SMSSentId,
            Message: $('#Message').val(),
            MessageLength: MsgCount,
            SMSTypeId: parseInt($("input:radio[name='smstype']:checked").val()),
            //  IsTest: true,
            BrandNameId: parseInt($("#select-brandname option:selected").val()),
            BrandName: $("#select-brandname option:selected").text(),
            DeviceTokenItemId: parseInt($('#select-gatewayid option:selected').attr('data-default')),
            DeviceTokenItemIdMobifone: parseInt($('#select-gatewayid option:selected').attr('data-mobile')),
            DeviceTokenItemIdVinaphone: parseInt($('#select-gatewayid option:selected').attr('data-vina')),
            DeviceTokenItemIdViettel: parseInt($('#select-gatewayid option:selected').attr('data-viettel')),
            GatewayConfigId:parseInt($('#select-gatewayid option:selected').val()),
        };
    }
    return data;
}

function emailsend_UpdateSMS(apiDomain,access_token,data, IsNext,message)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/SmsSend/UpdateInfo',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: data,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    custom_shownotification('success', message.SaveSuccess);
                    var data = JSON.parse(JSON.stringify(r.Data));
                    //lấy dữ liệu lên(từ local)
                    var datalocal = JSON.parse(localStorage.getItem('BasicCampaign'));
                    //cập nhập lại phần dữ liệu emailsend
                    datalocal.SMSSend = r.Data;
                    localStorage.setItem('BasicCampaign', JSON.stringify(datalocal));
                    if (IsNext) {
                        var CampaignId = data.CampaignId;
                        window.location.href = '/BasicCampaign/Recipient?eId=' + CampaignId
                    }
                    break;

                case 115:

                    custom_shownotification('error', message.SaveError);
                    break;
                case 101:

                    custom_shownotification('error', message.SaveError);
                    break;

                   
            }
            $('#divLoading').hide();

        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification('error', message.SaveError);
        }
    })
}

//hiện khung nhập nội dung theo lựa chọn của người dùng.
function emailcontent_ShowType(sendmendthod, IsMailBuilder) {
    var height = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.mt-element-step').outerHeight() - $('#divRedirect').outerHeight() - $('.page-footer').outerHeight() - $('.nav-tabs ').outerHeight() - 50 - $('#sms-note-content').height();
    $('body .btn.imgecontentemail').css({ 'width': 'auto', 'height': height - 100 + 'px' });
   // $('#basicsmscontent').children('.portlet-body').css('height', parseInt(height));
    $('#basicemailcontent').children('.portlet-body').css('height', parseInt(height));

    $('#img-snapshot-content-email').parent().css('width', $('#basicemailcontent').outerWidth() - 300);

    switch (sendmendthod) {
        case 0: //email
        
            $('#li-tab-content-email').show();
            $('#li-tab-content-email a').trigger('click');
            $('#li-tab-content-sms').hide();
            break;
        case 1:
            $('#li-tab-content-email').hide();
            $('#li-tab-content-sms').show();
            $('#li-tab-content-sms a').trigger('click');
       
            break;
        case 2: //cả 2
            $('#li-tab-content-sms').show();
            $('#li-tab-content-email').show();
            
           
            
           
            break;
    }
    


}

function emailcontent_OpenTemplate(apiDomain,apidBuilder, access_token, SentId, CampaignId) {
    $('.type-input-content.type-input-dragg').on('click', function () {

        $('#modalLoadEmailBuilderContent').show();
        var Type = 'c';
        emailcontent_IframeBuilder(apidBuilder,SentId, Type);
    })
}

function emailcontent_OpenModalConfirmReset()
{
    $('#resetallcontent').on('click', function () {
        $('#modal-warning-before-reset-email').modal('toggle');
    })
}

function emailcontent_ResetGoToBuilder(apiDomain, access_token, EmailSendId, CampaignId)
{
    $('#confirm-reset-emailcontent').on('click', function () {
        $('#modal-warning-before-reset-email').modal('toggle');
        var isave = 0; //click save 1 lần thôi

        if (isave == 0) {
            $('#divLoading').show();
            var data = {
                SentId: EmailSendId,
                HTML: "",
                JSONData: "",
                IsEmailBuilder: false,
                SnapShotUrl:"",

            }
            $.ajax({
                url: apiDomain + 'api/EmailSend/UpdateContent',
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                data: data,
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + access_token);
                },
                success: function (r) {
                    $('#divLoading').hide();
                    switch (parseInt(r.ErrorCode)) {
                        case 0:
                            isave++;
                            
                            var data = JSON.parse(JSON.stringify(r.Data));
                            //lấy dữ liệu lên(từ local)
                            var datalocal = JSON.parse(localStorage.getItem('BasicCampaign'));
                            //cập nhập lại phần dữ liệu emailsend
                            datalocal.EmailSend = r.Data
                            localStorage.setItem('BasicCampaign', JSON.stringify(datalocal));

                            emailcontent_ReloadAffterSave(datalocal);

                            //sau khi reload thì mở tab builder
                            $('#open-tab-email-builder').trigger( "click" );

                            break;
                        
                        default:
                            custom_shownotification('error', message.HaveError);
                            break;
                    }
                },
                error: function (x, s, e) {
                    $('#divLoading').hide();
                    custom_shownotification('error', message.HaveError);
                }
            });
        }
    })
}




function emailcontent_OpenModalConfirmTryHtml() {
    $('#tryeditbyinput').on('click', function () {
        $('#modal-warning-before-change-html').modal('toggle');
    })
}



function emailcontent_OpenModalCreateTemplate()
{
    $('#createtemplate').on('click', function () {
        $('#modal-create-template-htm').modal('toggle');
    })
}

function emailcontent_CreateTemplate(apiDomain, access_token, EmailSendId, CampaignId,UserId, imgUrl,message)
{
    $('#confirm-create-template').on('click', function () {
        if(emailcontent_CheckTemplate())
        {
            $('#divLoading').show();
            $('#modal-create-template-htm').modal('toggle');
            var data = {
                TemplateName: $('#template-name').val(),
                Subject: $('template-subject').val(),
                EmailSentId: EmailSendId,
                Publish:true,
            };
            $.ajax({
                url: apiDomain + 'api/template/CreateFromEmailSend',
                type: 'post',
                contentType: 'application/json; charset=utf-8',
                data:JSON.stringify(data),
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + access_token);
                },
                success:function(r)
                {
                    switch(parseInt(r.ErrorCode))
                    {
                        case 0:

                            //var data = r.Data;
                            //var TemplateId = data.TemplateId;
                            //if (parseInt(UserId) == 50)
                            //{
                            //    //system template publish false
                            //    emailcontent_UpdateTemplate(apiDomain, access_token, EmailSendId, CampaignId, data, imgUrl)
                            //}
                            //emailcontent_UpdateContentTemplate(apiDomain, access_token, EmailSendId, CampaignId, data, imgUrl,message);
                            custom_shownotification('success', message.CreateSuccess);
                            $('#divLoading').hide();
                            break;
                        default:
                            custom_shownotification('error', message.HaveError);
                            $('#divLoading').hide();
                            break;
                    }
                },
                error:function(x,s,e)
                {
                    $('#divLoading').hide();
                    custom_shownotification('error', message.HaveError);
                }
            })
        }
    });
}

function emailcontent_UpdateTemplate(apiDomain, access_token, EmailSendId, CampaignId, template, imgUrl,message) {

    var data = {
        TemplateId: template.TemplateId,
        TemplateName:template.TemplateName,
        Subject:template.Subject,
        Publish: false,
    }

    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/template/Update',
        type: 'post',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        data: JSON.stringify(data),
        success: function (r) {
            $('#divLoading').hide();
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    custom_shownotification('success', message.CreateSuccess);
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification('error', message.HaveError);
        }

    });
    
    
}

function emailcontent_UpdateContentTemplate(apiDomain, access_token, EmailSendId, CampaignId, data, imgUrl,message) {
    var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
    if (basiccampaign != '0' && basiccampaign != null) {
        var emailsend = JSON.parse(JSON.stringify(basiccampaign.EmailSend));
        var html = emailsend.HTML;
        var json = emailsend.JSONData;
        var snapshot = emailsend.SnapShotUrl;
        if (snapshot != "") {
            snapshot = snapshot.replace(imgUrl, '');
        }
        var data = {
            TemplateId: data.TemplateId,
            HTML: html,
            JSONData: json,
            //SnapShotUrl: snapshot,
        }

        $('#divLoading').show();
        $.ajax({
            url: apiDomain + 'api/template/UpdateContent',
            type: 'post',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader('Authorization', 'Bearer ' + access_token);
            },
            data:JSON.stringify(data),
            success: function (r) {
                $('#divLoading').hide();
                switch (parseInt(r.ErrorCode)) {
                    case 0:
                        custom_shownotification('success', message.CreateSuccess);
                        break;
                    default:
                        custom_shownotification('error', message.HaveError);
                        break;
                }
            },
            error: function (x, s, e) {
                $('#divLoading').hide();
                custom_shownotification('error',message.HaveError);
            }

        })
    }
    else {
        custom_shownotification('error', message.HaveError);
    }
}

function emailcontent_CheckTemplate()
{
    if ($('#template-name').val() == "" || $('#template-name').val() == null) {
        custom_shownotification('error', 'Tên mẫu email là bắt buộc');
        return false;
    }
    else return true;
}



function emailcontent_ChangeHtmlEmail(apiDomain, access_token, EmailSendId, CampaignId) {
    $('#confirm-change-html-emailcontent').on('click', function () {
        $('#modal-warning-before-change-html').modal('toggle');
        $('#modalLoadEmailEditorContent').show();
        var Type = 'c';
        emailcontent_IframeEditor(EmailSendId, Type);
    })
}


function emailcontent_UpdateContent(apiDomain, apidBuilder, access_token, SentId, IsMailBuilder, datatemplate, message) {
    var obj = datatemplate;
    var IsMailBuilder = false;
    if (obj.JSONData != null && obj.JSONData != '') {
        IsMailBuilder = true;
    }

    var data = {
        SentId: SentId,
        HTML: obj.HTML,
        JSONData: obj.JSONData,
        SnapShotUrl: obj.SnapShotUrl,
        IsMailBuilder: IsMailBuilder,
    }
    $.ajax({
        url: apiDomain + 'api/EmailSend/UpdateContent',
        type: 'Post',
        contentType: 'application/json; chartset=utf-8',
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            $('#divLoading').hide();
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    //lấy dữ liệu lên(từ local)
                    var datalocal = JSON.parse(localStorage.getItem('BasicCampaign'));
                    //cập nhập lại phần dữ liệu emailsend
                    datalocal.EmailSend = r.Data
                    localStorage.setItem('BasicCampaign', JSON.stringify(datalocal));


                    var Type = 'c';
                    if (IsMailBuilder) {
                        $('#modalLoadEmailBuilderContent').show();
                        emailcontent_IframeBuilder(apidBuilder,SentId, Type);
                    }
                    else {
                        $('#modalLoadEmailEditorContent').show();
                       
                        emailcontent_IframeEditor(SentId,Type);
                    }
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification('error', message.HaveError);
        }

    })
}



function emalcontent_UseTemplate(apiDomain,apidBuilder, access_token, EmailSendId, CampaignId, message)
{
    $('#js-grid-juicy-projects').on('click', '#select-use-template', function () {
      
        $('#divLoading').show();
        var TemplateId = $(this).attr('data-id');
        var data = emailcontent_GetTemplateInfo(apiDomain, access_token, TemplateId, message);
        if (parseInt(data.ErrorCode) == 0) {
            var obj = data.Data;
            if (obj.JSONData != null && obj.JSONData != '') {
                IsMailBuilder = true;

            }
            else IsMailBuilder = false;
            emailcontent_UpdateContent(apiDomain, apidBuilder, access_token, EmailSendId, IsMailBuilder, data.Data, message);
        }
        else {
            $('#divLoading').show();
            custom_shownotification('error', 'Lỗi chọn template');
        }
    });

    $('#js-grid-sys-template').on('click', '#select-use-template', function () {
     
        $('#divLoading').show();
        var TemplateId = $(this).attr('data-id');
        var data = emailcontent_GetTemplateInfo(apiDomain, access_token, TemplateId, message);
        if (parseInt(data.ErrorCode) == 0) {
            var obj = data.Data;
            if (obj.JSONData != null && obj.JSONData != '') {
                IsMailBuilder = true;

            }
            else IsMailBuilder = false;
            emailcontent_UpdateContent(apiDomain, apidBuilder, access_token, EmailSendId, IsMailBuilder, data.Data, message);
        }
        else {
            $('#divLoading').show();
            custom_shownotification('error', message.HaveError);
        }
    })
}

var IsEditTemplate = null;
function emailcontent_EditTemplate(apiDomain, access_token, EmailSendId, message) {

    $('#js-grid-juicy-projects').on('click', '#select-view-template', function () {
        $('#divLoading').show();
        var TemplateId = $(this).attr('data-id');
        IsEditTemplate = TemplateId;
        $('#modalLoadEmailBuilderContent').show();
        var Type = 't';
        emailcontent_IframeBuilder(TemplateId, Type);
    })
}

function emailcontent_OpenModalDeleteTemplate(apiDomain, access_token, EmailSendId, message) {

    $('#js-grid-juicy-projects').on('click', '#select-delete-template', function () {
       
        var TemplateId = $(this).attr('data-id');
        $('#confirm-delete-email-template').attr('data-id', TemplateId);
        $('#modal-warning-delete-template').modal('toggle');
    });
}

function emailcontent_DelelteTemplate(apiDomain, access_token, EmailSendId, message)
{
    $('#confirm-delete-email-template').on('click', function () {
        var TemplateId = $(this).attr('data-id');
        $('#divLoading').show();
        $('#modal-warning-delete-template').modal('toggle');
       
        $.ajax({
            url: apiDomain + 'api/template/Delete?TemplateId=' + TemplateId,
            type: 'post',
            contentType: 'application/json; charset=utf-8',
            beforeSend:function(request)
            {
                request.setRequestHeader('Authorization', 'Bearer ' + access_token);
            },
            success:function(r)
            {
                $('#divLoading').hide();
                switch(parseInt(r.ErrorCode))
                {
                    case 0:

                        $('#img' + TemplateId).closest('.cbp-item').remove();
                        emailcontent_DestroyCube();
                        LastMyTempId = null;
                        emailcontent_TemplateCube();
                        emailcontent_LoadMyTemplate(apiDomain, access_token, message);
                        custom_shownotification('success', message.DeleteSuccess);
                        break;
                    default:
                        custom_shownotification('error', message.HaveError);
                        break;

                }
            },
            error:function(x,s,e)
            {
                $('#divLoading').hide();
                custom_shownotification('error', message.HaveError);
            }
        })
    });
  
}




