﻿//hàm chung các sự kiện tương tác
var Embed = true;
var ShowPermission = true;
var ShowUnsub = true;
var Facebook;
var Twitter;




function emailsetting_ActionButton(apiDomain, access_token, CampaignId,IdAdd, info, message) {

    emailsetting_LoadFromEmail(apiDomain, access_token, message);

    emailsetting_LoadInfo(apiDomain, access_token, CampaignId, info);

    emailsetting_AddPersonalize(IdAdd, message);

    //var CampaignId = custom_GetCampaignId();
    //điều hướng của step
    custom_ActionOfColStep(CampaignId, message.NoExistNewsLetterId);

    //chon kieu gui email
    emailsetting_SelectType();

    //page title click
    custom_PageTitleClick();




    //sự kiện next 
    $("#btnSettingEmailNext").on('click', function () {
        //lưu trước khi next.
        $('#divLoading').show();
        var IsNext = true;
        emailsetting_SaveNext(apiDomain, access_token, IsNext, message);
       
    });

    $("#btnSettingEmailSave").on('click', function () {
        //lưu trước khi next.
        $('#divLoading').show();
        var IsNext = false;
        emailsetting_SaveNext(apiDomain, access_token, IsNext, message);

    });

    $("#btnSettingEmailBack").on('click', function () {
        //load lại thông tin đã lưu trontg local nếu có
        $('#divLoading').show();
      //  emailsetting_LoadInfo(piDomain, access_token, CampaignId,info);
        window.location.href = '/BasicCampaign'
    });

    //event switch bootrap embed url
    $('#txtSettingEmailEmbed').on('switchChange.bootstrapSwitch', function (event, state) {
        Embed = $('#txtSettingEmailEmbed').bootstrapSwitch('state');
    });

   
    $('#txtSettingEmailUnsub').on('switchChange.bootstrapSwitch', function (event, state) {
        ShowUnsub = $('#txtSettingEmailUnsub').bootstrapSwitch('state');
    });

    //event switch bootrap show pemission 
    $('#txtSettingEmailpemission').on('switchChange.bootstrapSwitch', function (event, state) {
        ShowPermission = $('#txtSettingEmailpemission').bootstrapSwitch('state');
    });

    //btm add facebook
    $('#btnSettingEmailFacebook').on('click', function () {
        $(this).attr("data-toggle", "modal");
        $(this).attr("href", "#modalUpdateFacebook");
    });

    //btn add Twitter
    $('#btnSettingEmailTwitter').on('click', function () {
        $(this).attr("data-toggle", "modal");
        $(this).attr("href", "#modalUpdateTwitter");
    });

    //btn xác nhận facebook
    $('#btnSelectFacebook').on('click', function () {
        Facebook = $('#txtSelectFacebook').val();
        $("#modalUpdateFacebook").modal('toggle');
        $('#btnSettingEmailFacebook').addClass('red');
    });
    $('#btnSelectTwitter').on('click', function () {
        Twitter = $('#txtSelectTwitter').val();
        $("#modalUpdateTwitter").modal('toggle');
        $('#btnSettingEmailTwitter').addClass('red');
    })


}

//lấy thông tin từ local đưa vào textbox 
function emailsetting_LoadInfo(apiDomain, access_token, CampaignId, info) {
    $('#divLoading').show();
    if (localStorage.getItem('BasicCampaign')) {
        var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
        if (basiccampaign != '0' && basiccampaign != null) {
            //if (CampaignId == basiccampaign.CampaignId) {

                emailsetting_Data(basiccampaign);
           // }
            //else {
            //    window.location.href = "/BasicCampaign/NotHavePermission";
            //    // emailsetting_CampaignInfo(apiDomain, access_token, CampaignId);
            //}
        }
        else {
            //emailsetting_ReadOnlyInput();
            var date = moment().format("YYYY-MM-DD HH:MM:SS");
            $('#txtSettingName').val("Basic campaign " + date);
            $('#txtSelectFacebook').val(info.Facebook);
            $('#txtSelectTwitter').val(info.Twitter);

        }
        $('#divLoading').hide();
    }
    else {
        var date = moment().format("YYYY-MM-DD HH:MM:SS");
        $('#txtSettingName').val("Basic campaign " + date);
        //emailsetting_ReadOnlyInput();
        $('#txtSelectFacebook').val(info.Facebook);
        $('#txtSelectTwitter').val(info.Twitter);

        $('#divLoading').hide();
    }

};

function emailsetting_Data(basiccampaign) {
    $('#txtSettingName').val(basiccampaign.CampaignName);
    var sendmendthod = parseInt(basiccampaign.SendMethod);
    emailsetting_SetTypeCampaing(sendmendthod);  //load check box mendthod
    if (sendmendthod == 0 || sendmendthod == 2) {
        var emailsend = JSON.parse(JSON.stringify(basiccampaign.EmailSend))
        $('#txtSettingSubject').val(emailsend.Subject),
        $('#txtSettingEmailEmbed').bootstrapSwitch('state', Boolean(emailsend.EmbedMailUrl));
        $('#txtSettingEmailpemission').bootstrapSwitch('state', Boolean(emailsend.ShowPermissionReminder));
        $('#txtSettingEmailEmbed').bootstrapSwitch('state', Boolean(emailsend.ShowUnsubscribeButton));
        if (emailsend.Facebook != "") {
            $('#txtSelectFacebook').val(emailsend.Facebook);
            $('#btnSettingEmailFacebook').addClass('red');
        }
        if (emailsend.Twitter != "") {
            $('#txtSelectTwitter').val(emailsend.Twitter);

            $('#btnSettingEmailTwitter').addClass('red');
        }
        $('#distributton').attr('href', '#distributtonsetting');
    }
    else {
        emailsetting_ReadOnlyInput();

    }
}
//set read only khi chua chon kieu gui email
function emailsetting_ReadOnlyInput() {
    $('#txtSettingSubject').attr('readonly', true),
    $('#txtSettingSender').attr('readonly', true),
    $('#txtSettingEmailSend').attr('readonly', true),
    $('#txtSettingEmailReply').attr('readonly', true),
    $('#distributton').attr('href', '#')
}

//xoa readonly khi chon kieu gui email
function emailSetting_RemoveReadOnlyInput() {
    $('#distributton').attr('href', '#distributtonsetting');
    $('#txtSettingSubject').attr('readonly', false),
    $('#txtSettingSender').attr('readonly', false),
    $('#txtSettingEmailSend').attr('readonly', false),
    $('#txtSettingEmailReply').attr('readonly', false);

}

//sự kiện check chọn loai hinh gui
function emailsetting_SelectType() {
    $('#checkboxemail').change(function () {
        if ($('#checkboxemail').prop('checked') == true) {
            emailSetting_RemoveReadOnlyInput();
        } else {
            emailsetting_ReadOnlyInput();
        }
    });
}

//set giá trị phương thức gửi của campaign lúc load trang
function emailsetting_SetTypeCampaing(menthod) {
    switch (menthod) {
        case 0:
            $('#checkboxemail').prop('checked', true);
            $('#checkboxsms').prop('checked', false);
            break;
        case 1:
            $('#checkboxsms').prop('checked', true);
            $('#checkboxemail').prop('checked', false);
            break;
        case 2:
            $('#checkboxemail').prop('checked', true);
            $('#checkboxsms').prop('checked', true);
            break;
    }
}

//lấy kết quả của việc chọn phương thức gửi của campaign
function emailsetting_GetTypeCampaign() {
    var menthod;
    if ($('#checkboxemail').prop('checked') == true && $('#checkboxsms').prop('checked') == false) {
        menthod = 0;
    }
    if ($('#checkboxemail').prop('checked') == false && $('#checkboxsms').prop('checked') == true) {
        menthod = 1;
    }
    if ($('#checkboxemail').prop('checked') == true && $('#checkboxsms').prop('checked') == true) {
        menthod = 2;
    }
    return menthod;
}

//kiểm tra trước khi lưu
function emailsetting_CheckBeforeSave(message) {

    if ($('#checkboxemail').prop('checked') == false && $('#checkboxsms').prop('checked') == false) {
        custom_shownotification('error', message.TypeSendCampaign);
        $('#divLoading').hide();
        return false;
        
    }
    if ($('#checkboxemail').prop('checked') == true) {

        if (!$('#txtSettingSubject').val()) {
            custom_shownotification('error', message.SubjectRequired);
            $('#divLoading').hide();
            return false;
           
        }

        if (!$('#txtSettingEmailSend').val()) {
            custom_shownotification('error', message.EmailSendRequired)
            
            $('#divLoading').hide();
            return false;
        }

        else {
            return true;
        }

    }
    else {
        return true;
    }

}



//lưu thông tin

function emailsetting_SaveNext(apiDomain, access_token,IsNext, message) {

    //kiểm tra null các trường bắt buộc
    if (emailsetting_CheckBeforeSave(message)) {
        if (localStorage.getItem('BasicCampaign')) {
            var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));

            //nếu trong local đã có =>cần update
            if (basiccampaign != '0' && basiccampaign != null) {
                var campaignId = basiccampaign.CampaignId;
                emailsetting_SaveUpdateCampaign(apiDomain, access_token, campaignId, IsNext, message);
            }

            //được tạo mới
            if (basiccampaign == '0' || basiccampaign == null) {

                emailsetting_SaveNewCampaign(apiDomain, access_token, IsNext, message);
            }
        }
        //tạo mới
        if (!localStorage.getItem('BasicCampaign')) {
            emailsetting_SaveNewCampaign(apiDomain, access_token, IsNext, message);
        }
    }
}

//function emailsetting_CampaignInfo(apiDomain, access_token, CampaignId) {
//    $('#divLoading').show();
//    $.ajax({
//        url: apiDomain + 'api/BasicCampaign/GetInfo?CampaignId='+CampaignId,
//        type: 'GET',
//        contentType: 'application/json; charset=utf-8',
//        beforeSend: function (request) {
//            request.setRequestHeader("Authorization", "Bearer " + access_token);
//        },
//        success: function (r) {

//            switch (parseInt(r.ErrorCode)) {
//                case 0:
//                       $('#divLoading').hide();
//                        localStorage.setItem('BasicCampaign', JSON.stringify(r.Data));
//                        var basiccampaign = JSON.parse(JSON.stringify(r.Data));
//                        emailsetting_Data(basiccampaign);
                        
//                        break;
//                case 104:
//                    $('#divLoading').hide();
//                    custom_shownotification('error', 'Không có quyền với chiến dịch này');
//                    setTimeout(function () {
//                        window.location.href = '/BasicCampaign';
//                    }, 1000);
//                    break;
//                case 101:
//                    $('#divLoading').hide();
//                    custom_shownotification('error', 'Không có quyền với chiến dịch này');
//                    setTimeout(function () {
//                        window.location.href = '/BasicCampaign';
//                    }, 1000);
//                    break;
//            }
//        }, error: function () {

//            $('#divLoading').hide();
//            custom_shownotification('error',  'Có lỗi xảy ra');
//        }
//    });
//}


//tạo mới campaign
function emailsetting_SaveNewCampaign(apiDomain, access_token,IsNext, message)
{
    var data = {
        CampaignName: $('#txtSettingName').val(),
        Active:true,
        SendMethod: emailsetting_GetTypeCampaign()
    };

    $.ajax({
        url: apiDomain + 'api/BasicCampaign/Create',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: data,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
          
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    localStorage.setItem('BasicCampaign', JSON.stringify(r.Data));
                    var data = JSON.parse(JSON.stringify(r.Data));
                    var menthod = parseInt(data.SendMethod);
                    if (menthod == 0 || menthod == 2) {
                        var sendId = JSON.parse(JSON.stringify(data.EmailSend)).SentId;
                        emailsetting_SaveUpdate(apiDomain, access_token, sendId,IsNext, message)
                    }
                    if(menthod==1)
                    {
                        if (IsNext) {
                            var CampaignId = data.CampaignId;
                            window.location.href = '/BasicCampaign/Content?eId=' + data.CampaignId;
                        }
                        custom_shownotification('success', 'Lưu thành công');
                    }
                    $('#divLoading').hide();
                    break;
                   
                default:
                    $('#divLoading').hide();
                    custom_shownotification('error', message.EmailSettingError);
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification('error', message.EmailSettingError);
        }
    })
}

//cập nhập campaign
function emailsetting_SaveUpdateCampaign(apiDomain, access_token, campaignId, IsNext, message)
{

    var data = {
        CampaignId: campaignId,
        Active: true,
        SendMethod: emailsetting_GetTypeCampaign(),
        CampaignName: $('#txtSettingName').val(),
    };

    $.ajax({
        url: apiDomain + 'api/BasicCampaign/UpdateCampaignInfo',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: data,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            console.log(JSON.parse(JSON.stringify(r.Data)));
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    localStorage.setItem('BasicCampaign', JSON.stringify(r.Data));
                    var data = JSON.parse(JSON.stringify(r.Data))
                    var menthod = parseInt(data.SendMethod)
                    if (menthod == 0 || menthod == 2) {
                        var emailsent = JSON.parse(JSON.stringify(data.EmailSend));
                        emailsetting_SaveUpdate(apiDomain, access_token, emailsent.SentId,IsNext, message)
                    }
                    else if (menthod == 1) {
                        if (IsNext) {
                            window.location.href = '/BasicCampaign/Content?eId=' + data.CampaignId;
                        }
                    }
                    $('#divLoading').hide();
                    break;
                default:
                    $('#divLoading').hide();
                    custom_shownotification('error', message.EmailSettingError);
                    break;

            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification('error', message.EmailSettingError);
        }
    });
}

//cập nhập các thông tin thêm.
function emailsetting_SaveUpdate(apiDomain, access_token, SentId ,IsNext, message)
{
    var frommail = $("#txtSettingEmailSend").select2('data')[0];
    var replymail = $("#txtSettingEmailReply").select2('data')[0];

    var data = {
            SentId: SentId,
            MessageName: $('#txtSettingName').val(),
            Subject: $('#txtSettingSubject').val(),
            FromName: frommail.fromname,
            FromEmail: frommail.fromemail,
            ReplyToEmail: replymail.text,
            EmbedMailUrl: Embed,
            ShowPermissionReminder: ShowPermission,
            ShowUnsubscribeButton: ShowUnsub,
            Facebook: Facebook,
            Twitter: Twitter
        };

        $.ajax({
            url: apiDomain + 'api/EmailSend/UpdateSendInfo',
            type: 'POST',
            contentType: 'application/x-www-form-urlencoded',
            data: data,
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            success: function (r) {
                switch (parseInt(r.ErrorCode))
                {
                    case 0:
                       // console.log(JSON.parse(JSON.stringify(r.Data)));
                        var data = JSON.parse(JSON.stringify(r.Data));
                        //lấy dữ liệu lên(từ local)
                        var datalocal = JSON.parse(localStorage.getItem('BasicCampaign'));
                        //cập nhập lại phần dữ liệu emailsend
                        datalocal.EmailSend = r.Data
                        localStorage.setItem('BasicCampaign', JSON.stringify(datalocal));

                        var CampaignId = data.CampaignId;
                        if (IsNext) {
                            window.location.href = '/BasicCampaign/Content?eId=' + CampaignId;
                        }

                        custom_shownotification('success', 'Lưu thành công');

                        $('#divLoading').hide();
                        break;
                    default:
                        $('#divLoading').hide();
                        custom_shownotification('error', message.EmailSettingError);
                        break;
                }
               
            },
            error: function (x, s, e) {
                $('#divLoading').hide();
                custom_shownotification('error', message.EmailSettingError);
            }
        })

}

function emailsetting_LoadFromEmail(apiDomain, access_token, message) {
   
    $.ajax({
        url: apiDomain + 'api/User/GetFromMailList',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    emailsetting_LoadSelectEmailFrom(data, message);

                    if (data.length == 0) {
                        $('#no-have-fromemail').show();
                    }
                    break;
                default:
                    custom_shownotification('error', 'Lỗi lấy danh sách email from');
                    break;
            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', 'Có lỗi xảy ra');
        }
    });
}

function emailsetting_LoadSelectEmailFrom(data, message) {

    var option = [];
    var optionreply = [];
    var ItemIdDefault;
    $.each(data, function (index, obj) {
        if (obj.IsDefault) {
            ItemIdDefault = obj.ItemId;
        }
        if (obj.IsVerify) {

            option.push({
                id: obj.ItemId,
                text: obj.FromName + ' <' + obj.FromMail + '>',
                fromname: obj.FromName,
                fromemail: obj.FromMail,
            });

            optionreply.push({
                id: obj.ItemId,
                text: obj.FromMail,
                fromname: obj.FromName,
                fromemail: obj.FromMail,
            });
        }

    });

    if (data.length == 0) {
        $('#txtSettingEmailSend').select2({
            data: option,

            "language": {
                "noResults": function () {
                    return "<a href='/Account/UserProfile#FromEmail' target='_blank' class='list-group-item bg-green bg-font-green' style=padding:5px;>" + message.AddNewFromEmail + "</a>";
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
        $('#txtSettingEmailReply').select2({
            data: optionreply,
            "language": {
                "noResults": function () {
                    return "<a href='/Account/UserProfile#FromEmail' target='_blank' class='list-group-item bg-green bg-font-green' style=padding:5px;>" + message.AddNewFromEmail + "</a>";
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
    }
    else {

        $('#txtSettingEmailSend').select2({
            data: option,
         
        });
        $('#txtSettingEmailReply').select2({
            data: optionreply,
           
        });


    }



    if (localStorage.getItem('BasicCampaign')) {
        var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
        if (basiccampaign != '0' && basiccampaign != null) {
            var emailsend = JSON.parse(JSON.stringify(basiccampaign.EmailSend));
            var text = emailsend.FromName + ' <' + emailsend.FromEmail + '>';
            var send = $("#txtSettingEmailSend option:contains('" + text + "')").val();

            $('#txtSettingEmailSend').select2('val', send);

            var reply = ($("#txtSettingEmailReply option:contains('" + emailsend.ReplyToEmail + "')").val());
            $('#txtSettingEmailReply').select2('val', reply);
        }
        else {

            $('#txtSettingEmailSend').select2('val', ItemIdDefault);
            $('#txtSettingEmailReply').select2('val', ItemIdDefault);

        }
    }
    else {

        $('#txtSettingEmailSend').select2('val', ItemIdDefault);
        $('#txtSettingEmailReply').select2('val', ItemIdDefault);
    }

    //placeholder search
    $("#txtSettingEmailSend").on("select2:open", function () {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", "Search...");
    });
    $("#txtSettingEmailSend").on("select2:close", function () {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", null);
    });

    $("#txtSettingEmailReply").on("select2:open", function () {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", "Search...");
    });
    $("#txtSettingEmailReply").on("select2:close", function () {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", null);
    });

}

function emailsetting_AddPersonalize(IdAdd, message) {
    $('#div-personalize-subject').on('click', '.data-personalize', function () {
        var personalize = $(this).text();
        custom_InsertTextAtCaret(IdAdd, personalize);

        //nếu là sms thì update sms
        //UpdateMessageCount(message);
    });

}










