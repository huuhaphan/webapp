﻿function emailsend_Action(tableId) {
    //click trên check box
    emailsend_ClickCheckBox(tableId);
    //page title click
    custom_PageTitleClick();
}

//set height cho table(scroll)
function emailsend_resizeHeight(tableId) {

    var height = parseInt($(window).height() - $('.page-header').height() - $('.page-bar').height() - $('.portlet-title').height() - $('#divAction').height() - $('.page-footer').height() - 100)
    return height;


};

//sư kiện click check box tren table 
function emailsend_ClickCheckBox(tableId) {
    var table = $(tableId).DataTable();
    //các sự kiện với checkbox ,select all
    $(tableId + ' tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');

        // Get row data
        var data = table.row($row).data();

        // Get row ID
        var rowId = custom_GetFisrtProJson(data)

        // Determine whether row ID is in the list of selected row IDs 
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            $(this).parent().addClass('checked')
            rows_selected.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            $(this).parent().removeClass('checked')
            rows_selected.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }

        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrl(table);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on table cells with checkboxes
    //$('#tableContactGroup').on('click', 'tbody td, thead th:first-child', function (e) {
    //    $(this).parent().find('input[type="checkbox"]').trigger('click');
    //});

    // Handle click on "Select all" control
    $('input[name="select_all"]').on('click', function (e) {
        if (this.checked) {
            $(tableId + ' tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $(tableId + ' tbody input[type="checkbox"]:checked').trigger('click');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    table.on('draw', function () {
        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrl(table);
    });
};
