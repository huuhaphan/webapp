﻿var SelectEmailRecipient = function () {
    var handleMultiSelect = function () {
        $('#emailRecipient').multiSelect();

    }
    return {
        //main function to initiate the module
        init: function () {
            handleMultiSelect();
            resizeSelectRecipient();
        }
    };

}();


function emailrecipient_LoadGroup(apiDomain, access_token, CampaignId, message) {
    if (localStorage.getItem('BasicCampaign')) {
        var basiccampagin = JSON.parse(localStorage.getItem('BasicCampaign'));
        if (basiccampagin != '0' && basiccampagin != null) {
            if (basiccampagin.CampaignId == CampaignId) {
                emailrecipient_ActionLoadGroup(apiDomain, access_token, CampaignId, message)
            }
            else {
                window.location.href = "/BasicCampaign/NotHavePermission";
            }
        }
    }

}

function emailrecipient_ActionLoadGroup(apiDomain, access_token, CampaignId, message)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/ContactGroup/GetList?Active=true&LastItemId=0&RowCount=1000',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
           
            var data = r.Data;
            var options = new Array();
            for (var i = 0; i < data.length; i++) {
                options.push('<option value="' + data[i].ContactGroupId + '">' + data[i].ContactGroupName + '</option>');
            }
            $('#emailRecipient').append(options.join(''));
            SelectEmailRecipient.init();
            if (localStorage.getItem('BasicCampaign')) {
                var basiccampagin = JSON.parse(localStorage.getItem('BasicCampaign'));
                if (basiccampagin != '0' && basiccampagin != null) {
                    if (basiccampagin.CampaignId == CampaignId) {
                        var select = new Array();
                        $.each(basiccampagin.ContactGroups, function (idx, obj) {
                            select.push(String(obj.ContactGroupId));
                        })
                        //console.log(select);
                        $('#emailRecipient').multiSelect('select', select);
                       
                    }
                    else {
                        window.location.href = "/BasicCampaign/NotHavePermission";
                    }
                }
                
            }
            $('#divLoading').hide();
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification('error', message.LoadContactListError);
        }
    });
}

function emailrecipient_ActionButton(apiDomain, access_token, CampaignId, message) {
    var CampaignId = custom_GetCampaignId();
    //sư kien click của col step by step
    custom_ActionOfColStep(CampaignId, message.NoExistNewsLetterId);
    
    //page title click
    custom_PageTitleClick();

    emailrecipient_EventChangeGroup(apiDomain, access_token, CampaignId, message);

    emailrecipient_SaveNext(apiDomain, access_token, CampaignId, message);

    emailrecipient_Save(apiDomain, access_token, CampaignId, message);

    //emailrecipient_ManuaSelect(message);

    emailrecipient_Back(CampaignId);
    
 
}

function emailrecipient_Back(CampaignId)
{

    $("#btnBack").on('click', function () {
        window.location.href = '/BasicCampaign/Content?eId=' + CampaignId
    })
}

//function emailrecipient_ManuaSelect(message)
//{
//    $("#manualSelectContact").on("click", function () {
//        //code tốt rồi
//        update_recipient = true;
//        $(this).removeAttr("data-target");
//        $(this).removeAttr("data-toggle");
//        $(this).removeAttr("href");

//        if ($('select#emailRecipient').val()) {

//            var listgroupId = String($('select#emailRecipient').val());
//            $(this).attr("data-target", "#ajaxselectrecipient");
//            $(this).attr("data-toggle", "modal");
//            $(this).attr("href", "/BasicCampaign/SelectContact?lgId=" + listgroupId);

//        }
//        else {
//            custom_shownotification('error', message.NoSelectContactGroup);
//        }

//    });
//}

function emailrecipient_SaveNext(apiDomain, access_token, CampaignId, message)
{
    $("#btnRecipientNext").on('click', function () {
        var update_recipient = false;
        if ($('select#emailRecipient').val()) {
            //loading
            $('#divLoading').show();
            var manual = false;
            if (rows_selected.length > 0) {
                manual = true;
            }
           
            update_recipient = !emailrecipient_CheckChangContactGroup();
            

            if (update_recipient == true) {
                var data = {
                    CampaignId: CampaignId,
                    IsManualSelectContact: manual,
                    Contacts: rows_data,
                    ContactGroupIds: $('select#emailRecipient').val()

                };
                $.ajax({
                    url: apiDomain + 'api/BasicCampaign/UpdateCampaignContactGroup',
                    type: 'POST',
                    contentType: 'application/x-www-form-urlencoded',
                    data: data,
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", "Bearer " + access_token);
                    },
                    success: function (r) {
                        switch (parseInt(r.ErrorCode)) {
                            case 0:
                                localStorage.setItem('BasicCampaign', JSON.stringify(r.Data));
                                window.location.href = '/BasicCampaign/Confirm?eId=' + JSON.parse(JSON.stringify(r.Data)).CampaignId
                                break;
                            case 105:
                                custom_shownotification('error', message.NoSelectCampaign);
                                $('#divLoading').hide();
                            case 115:
                                custom_shownotification('error', message.HaveError);
                                $('#divLoading').hide();

                        }

                    },
                    error: function (x, s, e) {
                        $('#divLoading').hide();
                        custom_shownotification('error', message.HaveError );
                    }
                })
            }
            else {
                $('#divLoading').show();
                window.location.href = '/BasicCampaign/Confirm?eId=' + CampaignId;
            }

        }
        else {
            custom_shownotification('error', message.NoSelectContactGroup);
        }
    });
}

function emailrecipient_Save(apiDomain, access_token, CampaignId, message) {
    $("#btnRecipientSave").on('click', function () {
        var update_recipient = false;
        if ($('select#emailRecipient').val()) {
            //loading
            $('#divLoading').show();
            var manual = false;
            if (rows_selected.length > 0) {
                manual = true;
            }

            update_recipient = !emailrecipient_CheckChangContactGroup();


            if (update_recipient == true) {
                var data = {
                    CampaignId: CampaignId,
                    IsManualSelectContact: manual,
                    Contacts: rows_data,
                    ContactGroupIds: $('select#emailRecipient').val()

                };
                $.ajax({
                    url: apiDomain + 'api/BasicCampaign/UpdateCampaignContactGroup',
                    type: 'POST',
                    contentType: 'application/x-www-form-urlencoded',
                    data: data,
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", "Bearer " + access_token);
                    },
                    success: function (r) {
                        switch (parseInt(r.ErrorCode)) {
                            case 0:
                                localStorage.setItem('BasicCampaign', JSON.stringify(r.Data));
                                custom_shownotification('success', message.SaveSuccess);
                                $('#divLoading').hide();
                                break;
                            case 105:
                                custom_shownotification('error', message.NoSelectCampaign);
                                $('#divLoading').hide();
                            case 115:
                                custom_shownotification('error', message.HaveError);
                                $('#divLoading').hide();

                        }

                    },
                    error: function (x, s, e) {
                        $('#divLoading').hide();
                        custom_shownotification('error', message.HaveError );
                    }
                })
            }
            else {
                $('#divLoading').hide();
                custom_shownotification('success', message.SaveSuccess);
            }

        }
        else {
            custom_shownotification('error', message.NoSelectContactGroup);
        }
    });
}


function emailrecipient_EventChangeGroup(apiDomain, access_token, CampaignId, message)
{
    var count = 1;
   
    $('#emailRecipient').change(function () {

        //khi click thay đổi danh sách
        //loading
        $('#divLoading').show();
        var countRecipient = 0;
        var arr = $('select#emailRecipient').val();
        $('#countRecipientLoad').css({ 'display': '' });
        $('#countRecipient').css({ 'display': 'none' });
        if (arr == null) {
            $('#countRecipient').text('0');
            $('#countRecipient').css({ 'display': '' });
            $('#countRecipientLoad').css({ 'display': 'none' });
            $('#divLoading').hide();
        }

        else if (arr != null) {
            $.ajax({
                url: apiDomain + 'api/Contact/Web/GetListByGroups?draw=' + count + '&SortColumn=CreateDate&SortDirection=Descending&ContactGroupIdList=' + arr,
                type: 'GET',
                contentType: 'application/x-www-form-urlencoded',
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + access_token);
                },
                success: function (r) {
                    countRecipient += r.recordsTotal;
                    localStorage.setItem('countRecipient', countRecipient);
                    $('#countRecipientLoad').css({ 'display': 'none' });
                    $('#countRecipient').css({ 'display': '' });
                    $('#countRecipient').text(formatMoney(countRecipient,""));
                    $('#divLoading').hide();

                },
                error: function (x, s, e) {
                    $('#divLoading').hide();
                    custom_shownotification('error', message.HaveError);
                }
            });
        }

        count++;
    });
}

function resizeFormRecipient() {
    $('#portletFormRecipient').css({ 'height': parseInt($('.page-content').height() - $('.page-bar').height() - $('.mt-element-step').height() - $('#divRedirect').height() - $('#info-select-recipient').height() - $('#divSelectManuaRecipient').height() - $('.page-footer').height()) });
    

}

function resizeSelectRecipient()
{
    $('.ms-list').css({ 'height': $(window).height() - $('.page-header').height() - $('.page-bar').height() - $('.mt-element-step').height() - $('#divRedirect').height() - $('#info-select-recipient').height() - $('.page-footer').height() - 50 });
    windownResizeRecipient();
}
function windownResizeRecipient()
{
    $(window).resize(function () {
        $('.ms-list').css({ 'height': $(window).height() - $('.page-header').height() - $('.page-bar').height() - $('.mt-element-step').height() - $('#divRedirect').height()- $('#info-select-recipient').height() - $('.page-footer').height() - 50 });

    })
}

function emailrecipient_CheckChangContactGroup() {
    if (localStorage.getItem('BasicCampaign')) {
        var basiccampagin = JSON.parse(localStorage.getItem('BasicCampaign'));
        if (basiccampagin != '0' && basiccampagin != null) {
            var contactgroup = [];
            $.each(basiccampagin.ContactGroups, function (i, o) {
                contactgroup.push(o.ContactGroupId);
            });
            var selectcontactgroup = [];
            $.each($('select#emailRecipient').val(), function (i, o) {
                selectcontactgroup.push(parseInt(o));
            });
            if (contactgroup.length == selectcontactgroup.length) {
                var change = true;
                $.each(selectcontactgroup, function (i, o) {
                    if(!contactgroup.contains(o))
                    {
                        change= false;
                    }
                });
                return change;

            }
            else 
            {
                return false;
            }
            
      
        }
        else  return true;
    }
    else return true;
}

