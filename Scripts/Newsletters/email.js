﻿/// <reference path="../ReadExcel/jszip.js" />
function email_Action(apiDomain, access_token, tableId,ServerImage, message) {

    email_CreateCampaign();
    email_Preview(apiDomain, access_token, tableId, message);
    email_Report(apiDomain, access_token, tableId, message);
    email_Copy(apiDomain, access_token, tableId, message);
    email_CancelSchedule(apiDomain, access_token, tableId, message);
    email_CheckBox(apiDomain, access_token, tableId, message);
    email_ShowSnapshot(apiDomain, access_token, tableId, ServerImage, message);


   

   
};

function email_Action_Draft(apiDomain, access_token, tableIddraft, ServerImage, message) {

    email_CreateCampaign();
    deleteCampaignDraftAll(apiDomain, access_token, tableIddraft, message);
    email_EditSettingDraft(apiDomain, access_token, tableIddraft, message);
    email_EditContentDraft(apiDomain, access_token, tableIddraft, message);
    email_EditRecipientDraft(apiDomain, access_token, tableIddraft, message);
    email_EditConfirmDraft(apiDomain, access_token, tableIddraft, message);
    email_DeleteDraft(apiDomain, access_token, tableIddraft, message);
    deleteCampaingDraftAllAction(apiDomain, access_token, tableIddraft, message);
    email_ConfirmDeleteDraft(apiDomain, access_token, tableIddraft, message)
    email_CheckBoxDraft(apiDomain, access_token, tableIddraft, message);
    email_SelectTool(tableIddraft);
    email_ShowSnapshotDraft(apiDomain, access_token, tableIddraft, ServerImage, message)


};

function email_Preview(apiDomain, access_token, tableId, message) {
    $(tableId).on('click', 'tbody #preview', function () {
        if ($.fn.DataTable.isDataTable(tableId)) {
            var tablerow = $(tableId).DataTable();
            var rowData = tablerow.row($(this).parents('tr')).data();
            window.location.href = "/BasicCampaign/ReViewCampaign?cId=" + rowData.campaignId;
            //var w = window.open("/BasicCampaign/ReViewCampaign?cId=" + rowData.campaignId, "_blank");
            //if (w) {
            //    //Browser has allowed it to be opened
            //    w.focus();
            //} else {
            //    //Browser has blocked it
            //    alert('Please allow popups for this website');
            //}
        }
    });
}

function email_ShowSnapshot(apiDomain, access_token, tableId, ServerImage, message) {
    $(tableId).on('click', 'tbody .thumnail-list-email', function () {
            var tablerow = $(tableId).DataTable();
            var rowData = tablerow.row($(this).parents('tr')).data();

            image = rowData.snapShotUrl;
            $('#img-preview-snapshot-email').attr('src', image);
            $('#img-preview-snapshot-email').bind('error', function () {
                $(this).attr("src", "/Content/Images/thumbnail-generator.png");
            });
            $('#modal-preview-snapshot-email').modal('toggle');
             
           
       
    });

    //$('#img-preview-snapshot-email').on('click', function () {
    //    $('#modal-preview-snapshot-email').modal('toggle');
    //})
}

function email_ShowSnapshotDraft(apiDomain, access_token, tableIddraft, ServerImage, message) {
    $(tableIddraft).on('click', 'tbody .thumnail-list-email', function () {
        var tablerow = $(tableIddraft).DataTable();
        var rowData = tablerow.row($(this).parents('tr')).data();

        image = rowData.snapShotUrl;
        $('#img-preview-snapshot-email').attr('src', image);
        $('#img-preview-snapshot-email').bind('error', function () {
            $(this).attr("src", "/Content/Images/thumbnail-generator.png");
        });
        $('#modal-preview-snapshot-email').modal('toggle');



    });

    //$('#img-preview-snapshot-email').on('click', function () {
    //    $('#modal-preview-snapshot-email').modal('toggle');
    //})
}

function email_Report(apiDomain, access_token, tableId, message) {
    $(tableId).on('click', 'tbody #report', function () {
        if ($.fn.DataTable.isDataTable(tableId)) {
            var tablerow = $(tableId).DataTable();
            var rowData = tablerow.row($(this).parents('tr')).data();
            localStorage.setItem('CampaignIdReport', rowData.campaignId);
            window.location.href = "/Statistics/GeneralStatistics?eId=" + rowData.campaignId;
        }
    });
}
function email_Copy(apiDomain, access_token, tableId, message) {

    $(tableId).on('click', 'tbody #re-used', function () {
        $('#divLoading').show();
        if ($.fn.DataTable.isDataTable(tableId)) {
            var tablerow = $(tableId).DataTable();
            var rowData = tablerow.row($(this).parents('tr')).data();
            var CampaignId = rowData.campaignId;
            
            $.ajax({
                url: apiDomain + 'api/BasicCampaign/Copy?CampaignId=' + CampaignId,
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                beforeSend: function (request) {
                    request.setRequestHeader('Authorization', 'Bearer ' + access_token);
                },
                success: function (r) {
                    $('#divLoading').hide();
                    switch (parseInt(r.ErrorCode)) {
                        case 0:

                            custom_shownotification('success', message.CopySuccess);
                            setTimeout(function () {
                                var urlRedirect = '/BasicCampaign/Setting?eId=' + r.Data.CampaignId;
                                email_GetInfo(apiDomain, access_token, r.Data.CampaignId, urlRedirect, message);
                            }, 300);

                            break;

                        default:
                            custom_shownotification('error', message.CopyError);
                            break;
                    }
                },
                error: function (x, s, e) {
                    $('#divLoading').hide();
                    custom_shownotification('error', message.CopyError);
                }
            })
        }
        else {
            $('#divLoading').hide();
        }
    });
}



//hủy đặt lịch
function email_CancelSchedule(apiDomain, access_token, tableId, message) {

    $(tableId).on('click', 'tbody #cancelschedule', function () {
        if ($.fn.DataTable.isDataTable(tableId)) {
            var tablerow = $(tableId).DataTable();
            var rowData = tablerow.row($(this).parents('tr')).data();
            email_ActionCancelSchedule(apiDomain, access_token, tableId, rowData.campaignId, rowData.scheduleDate, message);
        }
    });
}

function email_ActionCancelSchedule(apiDomain, access_token, tableId, CampaignId, scheduleDate, message) {
    var nowdate = Date.parse(moment().format());
    var scheduledate =Date.parse( moment(moment.utc(scheduleDate).toDate()).format());
    if (nowdate> scheduledate) {
        custom_shownotification('error', message.CancelScheduleWarning);
    }
    else {

        var data = {
            CampaignId: CampaignId
        };
        $('#divLoading').show();
        $.ajax({
            url: apiDomain + 'api/BasicCampaign/CancelSchedule',
            type: 'POST',
            data: data,
            contentType: 'application/x-www-form-urlencoded',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            success: function (r) {
                $('#divLoading').hide();
                switch (parseInt(r.ErrorCode)) {
                    case 0:
                        custom_shownotification('success', message.CancelScheduleSuccess);
                        $(tableId).dataTable().fnDraw();
                        break;
                    default:
                        custom_shownotification('success', message.CancelCheduleError);
                        break;
                }

            },
            error: function (x, s, e) {
                $('#divLoading').hide();
                custom_shownotification('error', message.HaveError);
            }
        })
    }
}

function email_CheckBox(apiDomain, access_token, tableId, message) {

    $(tableId).on('click', 'tbody input[type="checkbox"]', function (e) {
        if ($.fn.DataTable.isDataTable(tableId)) {
        var table = $(tableId).DataTable();
        var $row = $(this).closest('tr');

        // Get row data
        var data = table.row($row).data();

        // Get row ID
        var rowId = data.campaignId;// custom_GetFisrtProJson(data)

        // Determine whether row ID is in the list of selected row IDs 
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            $(this).parent().addClass('checked')
            rows_selected.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            $(this).parent().removeClass('checked')
            rows_selected.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }

        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrl(table);

        // Prevent click event from propagating to parent
        e.stopPropagation();
        }
    });

    // Handle click on "Select all" control
    $('input[name="select_all"]').on('click', function (e) {
        if (this.checked) {
            $(tableId + ' tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $(tableId + ' tbody input[type="checkbox"]:checked').trigger('click');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event

        $(tableId).on('draw', function () {
            var table = $(tableId).DataTable();
            // Update state of "Select all" control
            global_updateDataTableSelectAllCtrl(table);
        });
    
    
}

function email_CreateCampaign()
{

    $('#addNewsletter').on('click', function () {
        window.location.href = '/BasicCampaign/Setting?eId=0';
        //xóa newsletter lưu trong local khi tạo mới
        localStorage.setItem('BasicCampaign', 0);
    });
}




function email_EditSettingDraft(apiDomain, access_token, tableIddraft, message) {
    //button edit setting;
    $(tableIddraft).on('click', 'tbody #editsetting', function () {
        if ($.fn.DataTable.isDataTable(tableIddraft)) {
            var tablerowdraft = $(tableIddraft).DataTable();
            var rowData = tablerowdraft.row($(this).parents('tr')).data();

            var urlRedirect = '/BasicCampaign/Setting?eId=' + rowData.campaignId;
            email_GetInfo(apiDomain, access_token, rowData.campaignId, urlRedirect, message);
        }
    });
}
//button edit content;
function email_EditContentDraft(apiDomain, access_token, tableIddraft, message) {
    $(tableIddraft).on('click', 'tbody #editcontent', function () {
        if ($.fn.DataTable.isDataTable(tableIddraft)) {
            var tablerowdraft = $(tableIddraft).DataTable();
            var rowData = tablerowdraft.row($(this).parents('tr')).data();
            var urlRedirect = '/BasicCampaign/Content?eId=' + rowData.campaignId;
            email_GetInfo(apiDomain, access_token, rowData.campaignId, urlRedirect, message);
        }
    });
};

//button edit recipitent;
function email_EditRecipientDraft(apiDomain, access_token, tableIddraft, message) {
    $(tableIddraft).on('click', 'tbody #editrecipient', function () {
        if ($.fn.DataTable.isDataTable(tableIddraft)) {
            var tablerowdraft = $(tableIddraft).DataTable();
            var rowData = tablerowdraft.row($(this).parents('tr')).data();
            var urlRedirect = '/BasicCampaign/Recipient?eId=' + rowData.campaignId;
            email_GetInfo(apiDomain, access_token, rowData.campaignId, urlRedirect, message);
        }

    });
}

function email_EditConfirmDraft(apiDomain, access_token, tableIddraft, message) {
    //button confirm campaign before send
    $(tableIddraft).on('click', 'tbody #editconfirm', function () {
        if ($.fn.DataTable.isDataTable(tableIddraft)) {
            var tablerowdraft = $(tableIddraft).DataTable();
            var rowData = tablerowdraft.row($(this).parents('tr')).data();
            var urlRedirect = '/BasicCampaign/Confirm?eId=' + rowData.campaignId;
            email_GetInfo(apiDomain, access_token, rowData.campaignId, urlRedirect, message);
        }
    });
}

function email_DeleteDraft(apiDomain, access_token, tableIddraft, message) {
    //butoton confirm campaign before send
    $(tableIddraft).on('click', 'tbody #editdelete', function () {
        if ($.fn.DataTable.isDataTable(tableIddraft)) {
            var tablerowdraft = $(tableIddraft).DataTable();
            var rowData = tablerowdraft.row($(this).parents('tr')).data();
            // email_ConfirmDeleteDraft(apiDomain, access_token, tableIddraft, rowData.campaignId, message);
            $('#modal-confirm-delete-draft').modal('toggle');
            $('#confirm-delete-draft').attr('data-uid', rowData.campaignId)

        }
    });
}


function email_CheckBoxDraft(apiDomain, access_token, tableIddraft, message) {
    var tabledraft;
    //các sự kiện với checkbox ,select all
    if ($.fn.DataTable.isDataTable(tableIddraft)) {
         tabledraft = $(tableIddraft).DataTable();
    

    $(tableIddraft).on('click', 'tbody input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');

        // Get row data
        var data = tabledraft.row($row).data();

        // Get row ID
        var rowId = data.campaignId;//custom_GetFisrtProJson(data)

        // Determine whether row ID is in the list of selected row IDs 
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            $(this).parent().addClass('checked')
            rows_selected.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            $(this).parent().removeClass('checked')
            rows_selected.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }

        if (rows_selected.length > 0) {
            $('#btn-delete-draftcampaign').removeClass('hide');
        }
        else {
            $('#btn-delete-draftcampaign').addClass('hide');
        }

        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrlDraft(tabledraft);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on "Select all" control
    $('input[name="select_all_draft"]').on('click', function (e) {
        if (this.checked) {
            $(tableIddraft + ' tbody input[type="checkbox"]:not(:checked)').trigger('click');
            $('#btn-delete-draftcampaign').removeClass('hide');
        } else {
            $(tableIddraft + ' tbody input[type="checkbox"]:checked').trigger('click');
            $('#btn-delete-draftcampaign').addClass('hide');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    tabledraft.on('draw', function () {
        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrlDraft(tabledraft);
    });
    }
}

function email_GetInfo(apiDomain, access_token, CampaignId, urlRedirect, message)
{
   // window.location.href = urlRedirect; //test xong rồi xóa 
   // tạm thời không lấy thông tin từ sever xuống
    $.ajax({
        url: apiDomain + 'api/BasicCampaign/GetInfo?CampaignId=' + CampaignId,
        type: 'GET',
        contentType: 'application/x-www-form-urlencoded',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            switch(r.ErrorCode)
            {
                case 0:
                    localStorage.setItem('BasicCampaign', JSON.stringify(r.Data));
                    window.location.href = urlRedirect;
                    break;
                default:
                    message.GetInfoError;
                    break;
            }
            
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.GetInfoError);
        }
    })
}

function email_ConfirmDeleteDraft(apiDomain, access_token, tableIddraft, message)
{
  
    $('#confirm-delete-draft').on('click', function () {
        $('#modal-confirm-delete-draft').modal('toggle');
        var CampaignId = $('#confirm-delete-draft').attr('data-uid');
        $('#divLoading').show();
        $.ajax({
            url: apiDomain + 'api/BasicCampaign/Delete?CampaignId=' + CampaignId,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);

            },
            success: function (r) {
                switch (parseInt(r.ErrorCode)) {
                    case 0:
                        $(tableIddraft).dataTable().fnDraw();
                        custom_shownotification('success', message.DeleteSuccess);
                        $('#divLoading').hide();
                       
                        break;
                    default:
                        custom_shownotification('error', message.DeleteError);
                        $('#divLoading').hide();
                        break;
                }

            }, error: function (x,s,r) {
                custom_shownotification('error', message.DeleteError);
                $('#divLoading').hide();
            }
        })
    })
 
}



function deleteCampaignDraftAll(apiDomain, access_token, tableIddraft, message)
{
    $('#btn-delete-draftcampaign').on('click', function () {
        if (rows_selected.length > 0) {
            $('#modal-confirm-delete-all-draft').modal('toggle');    
        }

    });
}

function deleteCampaingDraftAllAction(apiDomain, access_token, tableIddraft, message)
{
    $('#confirm-delete-all-draft').on('click', function () {
     
        $('#modal-confirm-delete-all-draft').modal('toggle');

        if (rows_selected.length == 1) {
            $.ajax({
                url: apiDomain + 'api/BasicCampaign/Delete?CampaignId=' + rows_selected[0],
                type: 'GET',
                contentType: 'application/json; charset=utf-8',
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + access_token);

                },
                success: function (r) {
                    switch (parseInt(r.ErrorCode)) {
                        case 0:
                            $(tableIddraft).dataTable().fnDraw();
                            custom_shownotification('success', message.DeleteSuccess);
                            rows_selected = [];
                            break;
                        case 115:
                            custom_shownotification('error', message.DeleteError);
                            rows_selected = [];
                            $(tableIddraft).dataTable().fnDraw();
                            break;
                        default:
                            custom_shownotification('error', message.DeleteError);
                            rows_selected = [];
                            $(tableIddraft).dataTable().fnDraw();
                            break;
                    }

                },
                error: function () {
                    custom_shownotification('error', message.DeleteError);
                }
            });
        }
        else {
            var IdError = [];
            $.each(rows_selected, function (i, o) {
                $('#divLoading').show();
                $.ajax({
                    url: apiDomain + 'api/BasicCampaign/Delete?CampaignId=' + o,
                    type: 'GET',
                    contentType: 'application/json; charset=utf-8',
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", "Bearer " + access_token);

                    },
                    success: function (r) {
                        switch (parseInt(r.ErrorCode)) {
                            case 0:
                                {
                                    var len = parseInt(rows_selected.length - 1);
                                    if (i == len) {
                                        $(tableIddraft).dataTable().fnDraw();
                                        $('#btn-delete-draftcampaign').addClass('hide');
                                        $('#divLoading').hide();
                                        if (IdError.length > 0) {
                                            custom_shownotification('error', 'Đã xóa xong. Lỗi xóa chiến dịch: ' + IdError.length + '/' + rows_selected.length);
                                        }
                                        else {
                                            custom_shownotification('success', message.DeleteSuccess);
                                        }
                                        rows_selected = [];
                                        IdError = [];
                                    }
                                    break;
                                }
                              
                            default:
                                {
                                    IdError.push(o);
                                    console.log(IdError);
                                    var len = parseInt(rows_selected.length - 1);
                                   
                                    if (i == len) {
                                        console.log(IdError);
                                        $(tableIddraft).dataTable().fnDraw();
                                        $('#btn-delete-draftcampaign').addClass('hide');
                                        $('#divLoading').hide();
                                       
                                        if (IdError.length > 0) {
                                            custom_shownotification('error', 'Đã xóa xong. Lỗi xóa chiến dịch: ' + IdError.length + '/' + rows_selected.length);
                                        }
                                        else {
                                            custom_shownotification('success', message.DeleteSuccess);
                                        }
                                        rows_selected = [];
                                        IdError = [];
                                    }
                                    break;
                                }
                                

                        };


                    }, error: function () {
                        IdError.push(o);
                        if (i == rows_selected.length - 1) {
                            $(tableIddraft).dataTable().fnDraw();
                            $('#btn-delete-draftcampaign').addClass('hide');
                           
                            $('#divLoading').hide();
                            if (IdError.length > 0) {
                                custom_shownotification('error', 'Đã xóa xong. Lỗi xóa chiến dịch: ' + IdError.length + '/' + rows_selected.length);
                            }
                            else {
                                custom_shownotification('success', message.DeleteSuccess);
                            }
                            rows_selected = [];
                            IdError = [];
                        }
                    }
                });
            })
        }
    });
}


function email_ReloadTable(tableId, tableIddraft)
{
    $('#btn-reload-tableCampaign').on('click', function () {
        var Status
        if (localStorage.getItem('StatusBasicCampaign')) {
            Status = localStorage.getItem('StatusBasicCampaign');
            if (Status == 'IsSent') {
                $(tableId).dataTable().fnDraw();
            }
            else if (Status == 'Draft') {
                $(tableIddraft).dataTable().fnDraw();
            }
        }
       
    })
}

function email_SelectTool(tableIddraft)
{
    document.onmousemove = handleMouseMove;
    $(tableIddraft).on('click', 'tbody #btn-manage-togle', function () {
        if (mousePos.y > $(window).height() / 2) {
            $(this).parent().children('.dropdown-menu').addClass('bottom-up');
        }
        else
        {
            $(this).parent().children('.dropdown-menu').removeClass('bottom-up');
        }
    });
    

}

