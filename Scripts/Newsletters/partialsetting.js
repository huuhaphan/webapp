﻿/// <reference path="../appshare.js" />
/// <reference path="../custom.js" />
function partialsetting_Action(apiDomain, access_token, message) {
    partialsetting_Personalize(apiDomain, access_token, message);
    partialsetting_Personalize_SMS(apiDomain, access_token, message);
}

function partialsetting_Personalize(apiDomain, access_token, message) {
    var data = appshare_LoadSystemField(apiDomain, access_token, message);
    var user = appshare_LoadField(apiDomain, access_token, message);
    $('#div-personalize-subject').on('mouseenter', function () {
        var li = '';
        switch (data.ErrorCode)
        {
            case 0:
               
                $.each(data.Data, function (i, o) {
                    if (o.FieldName == 'Name') {
                        li += '  <li><a class="data-personalize"> #ContactName# </a></li>';
                    } else {
                        li += '  <li><a class="data-personalize"> #' + o.FieldName + '# </a></li>';
                    }
                });
                break;
        }
        switch(user.ErrorCode)
        {
            case 0:
                $.each(user.Data, function (i, o) {
                    li += '<li><a class="data-personalize"> #' + o.FieldName + '# </a></li>';
                });
                break;
        }
        
        var html = '<div id="list-personalize-subject" class="white-message white-tip">' +
            '<div class="dropdown inline clearfix">'+
            '<ul class="dropdown-menu" role="menu" style="height:200px; overflow-y: auto;">' + li +
              '   </ul>' +
             '<div>'+
            '<div>';
        $(this).append(html);
    }).on('mouseleave', function () {
        $(this).children('#list-personalize-subject').remove();
    });
    

    //$('#div-personalize-subject').on('mouseenter', 'a', function () {
    //    $(this).css({ 'color': "orange" });
    //})
    //$('#div-personalize-subject').on('mouseleave', 'a', function () {
    //    $(this).css({ 'color': "#337ab7" });
    //})
};




function partialsetting_Personalize_SMS(apiDomain, access_token, message) {
    //$('#div-personalize-subject-sms').on('mouseenter', function () {
    //    var html = '<div id="list-personalize-subject" class="white-message white-tip">' +
    //        '<div class="dropdown inline clearfix">' +
    //        '<ul class="dropdown-menu" role="menu">' +
    //                                                              '<li>' +
    //                                                                '<a class="data-personalize">#ContactName#</a>' +
    //                                                            '</li>' +
    //                                                            '<li>' +
    //                                                                '<a class="data-personalize">#FirstName#</a>' +
    //                                                            '</li>' +
    //                                                            '<li>' +
    //                                                                '<a class="data-personalize">#LastName#</a>' +
    //                                                            '</li>' +
    //                                                         '</ul>' +
    //                                                            '<div>' +
    //        '<div>';
    //    $(this).append(html);
    //}).on('mouseleave', function () {
    //    $(this).children('#list-personalize-subject').remove();
    //});

    var data = appshare_LoadFullField(apiDomain, access_token, message);
    $('#div-personalize-subject-sms').on('mouseenter', function () {
        var li = '';
        switch (data.ErrorCode) {
            case 0:
                localStorage.setItem('FullCustomField', JSON.stringify(data.Data));
                $.each(data.Data, function (i, o) {
                    if (o.FieldName == 'Name') {
                        li += '  <li><a class="data-personalize" data-fieldname="' + o.FieldName + '" data-maxlength="' + o.MaxLength + '">#ContactName#</a></li>';
                    } else {
                        li += '  <li><a class="data-personalize" data-fieldname="' + o.FieldName + '" data-maxlength="' + o.MaxLength + '">#' + o.FieldName + '#</a></li>';
                    }
                });
                break;
        }
        var html = '<div id="list-personalize-subject" class="white-message white-tip">'+
            '<div class="dropdown inline clearfix">' +
            '<ul class="dropdown-menu" role="menu" style="height:300px; overflow-y: auto;">' + li +
              '   </ul>' +
             '<div>' +
            '<div>';
        $(this).append(html);
    }).on('mouseleave', function () {
        $(this).children('#list-personalize-subject').remove();
    });
};




