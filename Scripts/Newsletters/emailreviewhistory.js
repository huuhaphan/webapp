﻿function emailreview_Action(apiDomain,access_token, campaignId, message)
{
    emailreview_GetCampaign(apiDomain, access_token, campaignId , message)
}

function emailreview_GetCampaign(apiDomain, access_token, campaignId, message) {
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + '/api/BasicCampaign/GetInfo?CampaignId=' + campaignId,
        type: 'Get',
        contentType: 'application/json; charset=utf-8',
        beforeSend:function(request){
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            console.log(r);
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    
                    switch (parseInt(r.Data.SendMethod)) {
                        case 0:
                            $('#panel-settingemail').show();
                            $('#panel-content-email').show();
                            emailreview_LoadGroupContact(apiDomain, access_token, data.CampaignId, message, data.EmailSend.ContactGroups)
                            break;
                        case 1:
                            $('#panel-settingsms').show();
                            $('#panel-contentsms').show();
                            emailreview_LoadGroupContact(apiDomain, access_token, data.CampaignId, message, data.SMSSend.ContactGroups)
                            break
                        case 2:
                            $('#panel-settingemail').show();
                            $('#panel-settingsms').show();

                            $('#panel-contentsms').show();
                            $('#panel-content-email').show();
                            emailreview_LoadGroupContact(apiDomain, access_token, data.CampaignId, message, data.EmailSend.ContactGroups)
                            break
                    }
                    $('#divcampaignname').text(data.CampaignName);
                    $('#infofromname').text(data.EmailSend.FromName);
                    $('#infofromemail').text(data.EmailSend.FromEmail);
                    $('#inforeplyemail').text(data.EmailSend.ReplyToEmail);
                    $('#infosubject').text(data.EmailSend.Subject);
                    $('#infosmscontent').text(data.SMSSend.Message);
                    $('#infoemailcontent').append(data.EmailSend.HTML);
                    $('#infosmslength').text(data.SMSSend.MessageLength);

                    var infosmstype = "";
                    var gatewayname = "";
                    var brandnamename = "";
                    switch (parseInt(data.SMSSend.SMSTypeId)) {
                        case 1:
                            infosmstype = "Gửi tin nhắn ngẫu nhiên (OTP)";
                            break;
                        case 2:
                            infosmstype = "Gửi tin nhắn thương hiệu (Brandname)";
                            brandnamename = data.SMSSend.BrandName;
                            $('#divinfobrandname').show();
                            break;
                        case 3:
                            infosmstype = "Gửi tin nhắn từ số cá nhân";
                            var gateway = appshare_GetGatewayConfigList(apiDomain, access_token, message);
                            if (gateway != undefined) {
                                var gatewaylist = gateway.Data;
                                $.each(gatewaylist, function (i, o) {
                                    if (o.ItemId == data.SMSSend.GatewayConfigId) {
                                        gatewayname = o.ConfigName;
                                    }
                                });
                            }
                            $('#divinfogatewayname').show();
                            break;
                    }
                    $('#divinfosmstype').show();
                    $('#infosmstype').text(infosmstype);
                    $('#infogatewayname').text(gatewayname);
                    $('#infobrandname').text(brandnamename);
                    $('#divLoading').hide();
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    $('#divLoading').hide();
                    break;
            }
        }, error: function (r) {
            custom_shownotification('error', message.HaveError);
            $('#divLoading').hide();
        }
    })
}

function emailreview_LoadGroupContact(apiDomain, access_token, CampaignId, message, Contactgroup) {
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/ContactGroup/GetList?Active=true&LastItemId=0&RowCount=0',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {

            var data = r.Data;
            console.log(data);
            var options = "";
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < Contactgroup.length; j++) {
                    if (parseInt(Contactgroup[j].ContactGroupId) == parseInt(data[i].ContactGroupId)) {
                        console.log(data[i]);
                        options += '<label class="label label-default">' + data[i].ContactGroupName + '</label>';
                    }
                }
            }
            if (options == "") {
                options = '<label class="label label-default">' + message.ContactListHasDelete + '</label>';
            }
            $('#divinfocontactgroup').append(options);

            $('#divLoading').hide();
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification('error', message.LoadContactListError);
        }
    });
}
