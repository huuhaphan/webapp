﻿
//load thông tin nếu là update
function editemailcontent_LoadInfo(message, translate) {
    var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
    if (basiccampaign != '0' && basiccampaign != null) {
        var sendmendthod = parseInt(basiccampaign.SendMethod);
        var emailsend = JSON.parse(JSON.stringify(basiccampaign.EmailSend));
        $('#editemail_contentInput').summernote('code', emailsend.HTML);


    }

};

//cac sự kiện
function editemailcontent_ActionButton(apiDomain, access_token, imgUrl, translate, message) {
    var CampaignId;
    var EmailSendId;

    if (localStorage.getItem('BasicCampaign')) {
        var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
        if (basiccampaign != '0' && basiccampaign != null) {
            CampaignId = basiccampaign.CampaignId;
            EmailSend = JSON.parse(JSON.stringify(basiccampaign.EmailSend))
            EmailSendId = EmailSend.SentId;
            UserId = basiccampaign.UserId;
            SnapShotUrl = EmailSend.SnapShotUrl;
            if (SnapShotUrl != '') {
                SnapShotUrl = SnapShotUrl.replace(imgUrl, '');
            }
          
        }
    }
    
  
    //reload nội dung lên editor
    editemailcontent_EditEmail();

    //action nut back 
    editemailcontent_ActionBack(CampaignId);

    //lưu nội dung email
    editemailcontent_SaveEmailContent(apiDomain, access_token, EmailSendId, SnapShotUrl);

    //sự kiện inset image 
    editemailcontent_ActionUploadImage(apiDomain, access_token, UserId, imgUrl)

    //personalize
    editemailcontent_CustomPersonalize();

    //add personalize
    editemailcontent_AddCustomPersonalize();

    editemailcontent_SaveAndExit(apiDomain, access_token, EmailSendId);

}

//summer editor
var EmailEditor = function () {
    var handleSummernote = function () {
        var height = parseInt($(window).height() -100);
        $('#editemail_contentInput').summernote({
            toolbar: [
              // [groupName, [list of button]]
              ['style', ['bold', 'italic', 'underline', 'clear']],
              ['font', ['strikethrough', 'superscript', 'subscript']],
              ['fontsize', ['fontsize']],
              ['color', ['color']],
              ['para', ['style', 'ul', 'ol', 'paragraph']],
              ['height', ['height']],
              ['inset', ['link', 'table', 'hr']],
              ['misc', ['codeview', 'undo', 'redo', 'help']],
              ['insert', ['image']],
              ['personalize', ['contactname','email']],
             
            ],

            minHeight: height,
        

        });

        //API:
        //var sHTML = $('#summernote_1').code(); // get code
        //$('#summernote_1').destroy(); // destroy
    }

    return {
        //main function to initiate the module
        init: function () {
            handleSummernote();
        }
    };

}();

function editemailcontent_EditEmail() {
        if (localStorage.getItem('BasicCampaign')) {
            var basiccampaign = JSON.parse(localStorage.getItem('BasicCampaign'));
            if (basiccampaign != '0' && basiccampaign != null) {
                var EmailSend = JSON.parse(JSON.stringify(basiccampaign.EmailSend))
                $('#editemail_contentInput').summernote('code', EmailSend.HTML)
            }

        };
}

//quay lại
function editemailcontent_ActionBack(CampaignId) {
    $("#close-edit-email-content").on("click", function () {
        window.parent.postMessage("ClosePopup", "*");
    });
}




var file;
var blobName;
function editemailcontent_ActionUploadImage(apiDomain, access_token, userId, imgUrl) {
    //button trong editor click trigger input file bên ngoài
    $('#insertImage').on('change', function (e) {
        //sự kiện chọn hình, upload hình
        var files = e.target.files;
        var name = files[0].name;
        file = files[0];
        var extend = name.split('.').pop();
        console.log(extend);
        blobName = userId + moment().format('YYYYMMDDHHmmss') + '.' + extend;
        $.ajax({
            url: apiDomain + 'api/EmailSend/GetSASImage?BlobName=' + blobName,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            success: function (r) {
                uri = r.Data;
                editemailcontent_uploadToAzureEmailContent(apiDomain, access_token, file, uri, blobName, imgUrl)
            }
        });
    });
}

//upload lên azure và server
function editemailcontent_uploadToAzureEmailContent(apiDomain, access_token, file, uri, blobName, imgUrl) {

    var maxBlockSize = 2 * 1024 * 1024;
    var fileSize = file.size;
    if (fileSize < maxBlockSize) {
        maxBlockSize = fileSize;
    }
    var blockIds = new Array();
    var blockIdPrefix = "block-";
    var currentFilePointer = 0;
    var totalBytesRemaining = fileSize;
    var numberOfBlocks = 0;
    var bytesUploaded = 0;

    if (fileSize % maxBlockSize == 0) {
        numberOfBlocks = fileSize / maxBlockSize;
    } else {
        numberOfBlocks = parseInt(fileSize / maxBlockSize, 10) + 1;
    }

    var reader = new FileReader();
    reader.onloadend = function (evt) {
        if (evt.target.readyState == FileReader.DONE) { // DONE == 2
            var r = uri + '&comp=block&blockid=' + blockIds[blockIds.length - 1];
            var requestData = new Uint8Array(evt.target.result);
            $.ajax({
                url: r,
                type: "PUT",
                data: requestData,
                processData: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('x-ms-blob-type', 'BlockBlob');
                    //    xhr.setRequestHeader('Content-Length', requestData.length);
                },
                success: function (data, status) {
                    editemailcontent_uploadFileInBlocks();
                },
                error: function (xhr, desc, err) {
                }
            });
        }
    };


    function editemailcontent_uploadFileInBlocks() {

        if (totalBytesRemaining > 0) {
            var fileContent = file.slice(currentFilePointer, currentFilePointer + maxBlockSize);
            var blockId = blockIdPrefix + pad(blockIds.length, 6);
            console.log("block id = " + blockId);
            blockIds.push(btoa(blockId));
            reader.readAsArrayBuffer(fileContent);
            currentFilePointer += maxBlockSize;
            totalBytesRemaining -= maxBlockSize;
            if (totalBytesRemaining < maxBlockSize) {
                maxBlockSize = totalBytesRemaining;
            }
        } else {
            editemailcontent_commitBlockList();
        }
    }

    editemailcontent_uploadFileInBlocks();

    function editemailcontent_commitBlockList() {

        var submituri = uri + '&comp=blocklist';
        var requestBody = '<?xml version="1.0" encoding="utf-8"?><BlockList>';
        for (var i = 0; i < blockIds.length; i++) {
            requestBody += '<Latest>' + blockIds[i] + '</Latest>';
        }
        requestBody += '</BlockList>';
        $.ajax({
            url: submituri,
            type: "PUT",
            data: requestBody,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('x-ms-blob-content-type', file.type);
                xhr.setRequestHeader('Content-Length', requestBody.length);
            },
            success: function (data, status) {
              
                //$('#editemail_contentInput').summernote('insertImage', imgUrl + blobName, '');
                $('#editemail_contentInput').summernote('insertImage', imgUrl + blobName, function ($image) {
                    $image.css('max-width', $image.width());
                    $image.css('width', '100%');
                    $image.attr('data-filename', '');
                });
            },
            error: function (xhr, desc, err) {
                custom_shownotification('error', 'Lỗi upload hình');
            }
        });

    }
}




//lưu nội dung của email trong campaign
function editemailcontent_SaveEmailContent(apiDomain, access_token, SentId, SnapShotUrl) {
    var isave = 0; //click save 1 lần thôi
    $('#btnsaveemailcontent').on('click', function () {
      
        if (isave == 0) {
          
            var html = $('#editemail_contentInput').summernote('code');
            if (html) {
                var data = {
                    SentId: SentId,
                    HTML: html,
                    JSONData: "",
                    IsMailBuilder: false,
                  
                    
                }
                $('#divLoading').show();
                $.ajax({
                    url: apiDomain + 'api/EmailSend/UpdateContent',
                    type: 'POST',
                    contentType: 'application/x-www-form-urlencoded',
                    data: data,
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", "Bearer " + access_token);
                    },
                    success: function (r) {
                        $('#divLoading').hide();
                        switch (parseInt(r.ErrorCode)) {
                            case 0:
                                isave++;
                                // console.log(JSON.parse(JSON.stringify(r.Data)));
                                var data = JSON.parse(JSON.stringify(r.Data));
                                //lấy dữ liệu lên(từ local)
                                var datalocal = JSON.parse(localStorage.getItem('BasicCampaign'));
                                //cập nhập lại phần dữ liệu emailsend
                                datalocal.EmailSend = r.Data
                                localStorage.setItem('BasicCampaign', JSON.stringify(datalocal));
                                custom_shownotification('success', 'Lưu nội dung email thành công');
                                break;
                            case 101:
                                custom_shownotification('error', 'Chiến dịch bị lỗi, không thể cập nhập nội dung');
                            default:
                                custom_shownotification('error', 'Có lỗi xảy ra,  không thể cập nhập nội dung');
                                break;
                        }
                    },
                    error: function (x, s, e) {
                        $('#divLoading').hide();
                        custom_shownotification('error', 'Lưu nội dung thất bại, không thể tiếp tục. ');
                    }
                });
            }
            else {
                custom_shownotification('error', 'Chưa nhập nội dung email');
               
            }
        }
        

    });

};

function editemailcontent_SaveAndExit(apiDomain, access_token, SentId)
{
    var isave = 0; //click save 1 lần thôi
    $('#btnsaveandexitemailcontent').on('click', function () {
        if (isave == 0) {
            $('#divLoading').show();
            var html = $('#editemail_contentInput').summernote('code');
            console.log(html);
            if (html) {
                var data = {
                    SentId: SentId,
                    HTML: html,
                    JSONData: "",
                    IsMailBuilder: false,
                   

                }
                $.ajax({
                    url: apiDomain + 'api/EmailSend/UpdateContent',
                    type: 'POST',
                    contentType: 'application/x-www-form-urlencoded',
                    data: data,
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", "Bearer " + access_token);
                    },
                    success: function (r) {
                        $('#divLoading').hide();
                        switch (parseInt(r.ErrorCode)) {
                            case 0:
                                isave++;
                                // console.log(JSON.parse(JSON.stringify(r.Data)));
                                var data = JSON.parse(JSON.stringify(r.Data));
                                //lấy dữ liệu lên(từ local)
                                var datalocal = JSON.parse(localStorage.getItem('BasicCampaign'));
                                //cập nhập lại phần dữ liệu emailsend
                                datalocal.EmailSend = r.Data
                                localStorage.setItem('BasicCampaign', JSON.stringify(datalocal));
                                custom_shownotification('success', 'Lưu nội dung email thành công');

                                // window.location.href = '/BasicCampaign/Content?eId=' + datalocal.CampaignId;
                                window.parent.postMessage("ClosePopup", "*");
                                break;
                            case 101:
                                custom_shownotification('error', 'Chiến dịch bị lỗi, không thể cập nhập nội dung');
                            default:
                                custom_shownotification('error', 'Có lỗi xảy ra,  không thể cập nhập nội dung');
                                break;
                        }
                    },
                    error: function (x, s, e) {
                        $('#divLoading').hide();
                        custom_shownotification('error', 'Lưu nội dung thất bại, không thể tiếp tục. ');
                    }
                });
            }
            else {
                custom_shownotification('error', 'Chưa nhập nội dung email');
                $('#divLoading').hide();
            }
        }
    })
}


var editemailcontent_selection;
var editemailcontent_cursorPos;

function editemailcontent_CustomPersonalize()
{
    //$('.note-btn-group.btn-group.note-personalize').on('mouseenter', function () {
    //    var html = '<div id="list-personalize-subject" class="error-message white-tip">' +
    //        '<span style="color:#888;">Các trường dữ liệu liên lạc</span><br/><br/>' +
    //        '<a class="data-personalize">#ContactName#</a><br/><br/>' +
    //        '<a class="data-personalize">#Email#</a><br/><br/>' +
    //        '<div>'
    //    $('.note-btn-group.btn-group.note-personalize').append(html);

       

    //}).on('mouseleave', function () {
    //    $(this).children('#list-personalize-subject').remove();
    //});


    //$('.note-btn-group.btn-group.note-personalize').on('mouseenter', 'a', function () {
    //    $(this).css({ 'color': "orange" });
    //})
    //$('.note-btn-group.btn-group.note-personalize').on('mouseleave', 'a', function () {
    //    $(this).css({ 'color': "#337ab7" });
    //})
}

function editemailcontent_AddCustomPersonalize()
{
    //$('.note-btn-group.btn-group.note-personalize').on('click', '.data-personalize', function () {
    //    var personalize = $(this).text();

    //    $('#editemail_contentInput').summernote('focus');
    //    $('#editemail_contentInput').summernote('insertText', personalize);
    //});

    
  
}



