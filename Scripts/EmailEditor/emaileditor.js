﻿function emaileditor_ActionButton(apiDomain, access_token, UserId, Id, Type, imgUrl, message) {

    emaileditor_LoadInfo(apiDomain, access_token, UserId, Id, Type,message);

    emaileditor_ActionBack();

    //lưu nội dung 
    emaileditor_SaveContent(apiDomain, access_token, Id, Type, message);

    //sự kiện inset image 
    emaileditor_ActionUploadImage(apiDomain, access_token, UserId, imgUrl,message)

    emaileditor_SaveAndExit(apiDomain, access_token, Id, Type, message);

    emaileditor_LoadUserCustomField(apiDomain, access_token, message);

    emaileditor_LoadSystemCustomField(apiDomain, access_token, message);


}


//summer editor
var emaileditor_EmailEditor = function () {
    var handleSummernote = function (lang) {
        var height = parseInt($(window).height() - 100);
        $('#event_contentInput').summernote({
            //lang:'vi-VN',
            popover: {
                image: [
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['custom', ['imageAttributes', 'imageShape']],
                    ['remove', ['removeMedia']]
                ],
            },
            lang: lang,
            imageAttributes:{
                imageDialogLayout:'default', // default|horizontal
                icon:'<i class="note-icon-pencil"/>',
                removeEmpty:false // true = remove attributes | false = leave empty if present
            },
            displayFields:{
                imageBasic:true,  // show/hide Title, Source, Alt fields
                imageExtra:false, // show/hide Alt, Class, Style, Role fields
                linkBasic:true,   // show/hide URL and Target fields for link
                linkExtra:false   // show/hide Class, Rel, Role fields for link
            },
            toolbar: [
              // [groupName, [list of button]]
              ['style', ['bold', 'italic', 'underline', 'clear']],
              ['font', ['strikethrough', 'superscript', 'subscript']],
              ['fontsize', ['fontsize']],
              ['color', ['color']],
              ['para', ['style', 'ul', 'ol', 'paragraph']],
              ['height', ['height']],
              ['insert', ['link', 'table', 'hr']],
              ['insert', ['image', 'picture', 'template']],
             
              ['misc', ['codeview', 'undo', 'redo', 'fullscreen']],
            
            ],


            minHeight: height,


        });

        //API:
        //var sHTML = $('#summernote_1').code(); // get code
        //$('#summernote_1').destroy(); // destroy
    }

    return {
        //main function to initiate the module
        init: function (lang) {
            handleSummernote(lang);
        }
    };

}();


function emaileditor_LoadInfo(apiDomain, access_token, UserId, Id, Type,message) {
    if (Id) {
        emaileditor_Info(apiDomain, access_token, UserId, Id, Type,message);
    }
}

function emaileditor_Info(apiDomain, access_token, UserId, Id, Type, message) {
    var apiUrl = '';
    if (Type == 'n') {
        apiUrl = 'api/newsletter/GetInfo?NewsletterId=' + +Id;
    }
    if (Type == 't') {
        apiUrl = 'api/template/GetInfo?TemplateId=' + +Id;
    }
    if (Type == 'c') {
        apiUrl = 'api/EmailSend/GetInfo?SentId=' + +Id;
    }
    $.ajax({
        url: apiDomain + apiUrl,
        type: 'GET',
        contentType: 'application/json; charset-utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    $('#event_contentInput').summernote('code', data.HTML);
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
        },

    });
}

//quay lại
function emaileditor_ActionBack(CampaignId) {
    $("#close-edit-email-content").on("click", function () {
        window.parent.postMessage("ClosePopup", "*");
    });
}

//lưu nội dung
function emaileditor_SaveContent(apiDomain, access_token, Id, Type, message) {
    $('#btnsaveemailcontent').on('click', function () {

      
            var html = '<html>'+ $('#event_contentInput').summernote('code') + '</html>';
            if (html) {
                var data = {
                    Id: Id,
                    HTML: html,
                    JSONData: "",
                    IsMailBuilder: false,
                    IsEditor:true,


                };
                var IsExit = false;
                if (Type == 'n') {
                    emaileditor_SaveNewLetter(apiDomain, access_token, Id, Type,data, message, IsExit);
                }
                if (Type == 't') {
                    emaileditor_SaveTemplate(apiDomain, access_token, Id, Type,data, message, IsExit);
                }
                if (Type == 'c') {
                    emaileditor_SaveSent(apiDomain, access_token, Id, Type, data, message, IsExit);
                }

            }
            else {
                custom_shownotification('error', message.NoContent);

            }
    });

}



function emaileditor_SaveAndExit(apiDomain, access_token, Id, Type, message) {
    $('#btnsaveandexitemailcontent').on('click', function () {
            $('#divLoading').show();
            var html = $('#event_contentInput').summernote('code');
            console.log(html);
            if (html) {
                var data = {
                    Id: Id,
                    HTML: html,
                    JSONData: "",
                    IsMailBuilder: false,
                    IsEditor: true,

                }
                var IsExit = true;
                if (Type == 'n') {
                    emaileditor_SaveNewLetter(apiDomain, access_token, Id, Type,data, message, IsExit);
                }
                if (Type == 't') {
                    emaileditor_SaveTemplate(apiDomain, access_token, Id, Type,data, message, IsExit);
                }
                if (Type == 'c') {
                    emaileditor_SaveSent(apiDomain, access_token, Id, Type, data, message, IsExit);
                }
            }
            else {
                custom_shownotification('error', message.NoContent);
                $('#divLoading').hide();
            }
    })
}

function emaileditor_SaveNewLetter(apiDomain, access_token, Id, Type, data, message, IsExit) {
    $('#divLoading').show();
    var para = {
        NewsletterId: data.Id,
        HTML: data.HTML,
        JSONData: data.JSONData,
        SnapshotUrl:'',
        IsEditor: data.IsEditor,
    }
    $.ajax({
        url: apiDomain + 'api/newsletter/UpdateMailContent',
        type: 'POST',
        //contentType: 'application/json; charset=utf-8',
        //data: JSON.stringify(para),
        contentType: 'application/x-www-form-urlencoded',
        data: para,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            $('#divLoading').hide();
            switch (parseInt(r.ErrorCode)) {
                case 0:
                  
                    custom_shownotification('success', message.SaveSuccess);
                    if (IsExit) {
                        window.parent.postMessage("ClosePopup", "*");
                    }
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification('error', message.HaveError);
        }
    });
}

function emaileditor_SaveSent(apiDomain, access_token, Id, Type, data, message, IsExit) {
    $('#divLoading').show();
    var para = {
        SentId: data.Id,
        HTML: data.HTML,
        JSONData: data.JSONData,
        IsMailBuilder: data.IsMailBuilder,
        SnapshotUrl: '',
    }
    $.ajax({
        //url: apiDomain + 'api/EmailSend/UpdateContent',
        //type: 'POST',
        //contentType: 'application/json; charset=utf-8',
        //data: JSON.stringify(para),

        url: apiDomain + 'api/EmailSend/UpdateContent',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        data: para,
       
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            $('#divLoading').hide();
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = JSON.parse(JSON.stringify(r.Data));
                    //lấy dữ liệu lên(từ local)
                    var datalocal = JSON.parse(localStorage.getItem('BasicCampaign'));
                    //cập nhập lại phần dữ liệu emailsend
                    datalocal.EmailSend = r.Data
                    localStorage.setItem('BasicCampaign', JSON.stringify(datalocal));
                    custom_shownotification('success', message.SaveSuccess);
                    if (IsExit) {
                        window.parent.postMessage("ClosePopup", "*");
                    }
                    break;
               
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification('error', message.HaveError);
        }
    });
}

function emaileditor_SaveTemplate(apiDomain, access_token, Id, Type, data, message, IsExit)
{
    $('#divLoading').show();
    var para = {
        TemplateId: data.Id,
        HTML: data.HTML,
        JSONData: data.JSONData,
        IsEditor: data.IsEditor,
        SnapShotUrl:'',
    }
    $.ajax({
        url: apiDomain + 'api/template/UpdateContent',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(para),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            $('#divLoading').hide();
            switch (parseInt(r.ErrorCode)) {
                case 0:
                
                    custom_shownotification('success', message.SaveSuccess);
                    if (IsExit) {
                        window.parent.postMessage("ClosePopup", "*");
                    }
                    break;
                
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification('error', message.HaveError);
        }
    });
}


var file;
var blobName;
function emaileditor_ActionUploadImage(apiDomain, access_token, userId, imgUrl,message) {
    //button trong editor click trigger input file bên ngoài
    $('#insertImage').on('change', function (e) {
        //sự kiện chọn hình, upload hình
        $('#divLoading').show();
        var files = e.target.files;
        var name = files[0].name;
        file = files[0];
        var extend = name.split('.').pop();
        console.log(extend);
        blobName = userId + moment().format('YYYYMMDDHHmmss') + '.' + extend;
        $.ajax({
            url: apiDomain + 'api/EmailSend/GetSASImage?BlobName=' + blobName,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            success: function (r) {
                uri = r.Data;
                emaileditor_uploadToAzureEmailContent(apiDomain, access_token, file, uri, blobName, imgUrl,message)
            },error:function(r)
            {
                custom_shownotification('error', 'Lỗi upload hình');

                $('#divLoading').hide();
            }
        });
    });
}

//upload lên azure và server
function emaileditor_uploadToAzureEmailContent(apiDomain, access_token, file, uri, blobName, imgUrl,message) {

    var maxBlockSize = 2 * 1024 * 1024;
    var fileSize = file.size;
    if (fileSize < maxBlockSize) {
        maxBlockSize = fileSize;
    }
    var blockIds = new Array();
    var blockIdPrefix = "block-";
    var currentFilePointer = 0;
    var totalBytesRemaining = fileSize;
    var numberOfBlocks = 0;
    var bytesUploaded = 0;

    if (fileSize % maxBlockSize == 0) {
        numberOfBlocks = fileSize / maxBlockSize;
    } else {
        numberOfBlocks = parseInt(fileSize / maxBlockSize, 10) + 1;
    }

    var reader = new FileReader();
    reader.onloadend = function (evt) {
        if (evt.target.readyState == FileReader.DONE) { // DONE == 2
            var r = uri + '&comp=block&blockid=' + blockIds[blockIds.length - 1];
            var requestData = new Uint8Array(evt.target.result);
            $.ajax({
                url: r,
                type: "PUT",
                data: requestData,
                processData: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('x-ms-blob-type', 'BlockBlob');
                    //    xhr.setRequestHeader('Content-Length', requestData.length);
                },
                success: function (data, status) {
                    emaileditor_uploadFileInBlocks(message);
                },
                error: function (xhr, desc, err) {
                    custom_shownotification('error', 'Lỗi upload hình');
                    $('#divLoading').hide();
                }
            });
        }
    };


    function emaileditor_uploadFileInBlocks(message) {

        if (totalBytesRemaining > 0) {
            var fileContent = file.slice(currentFilePointer, currentFilePointer + maxBlockSize);
            var blockId = blockIdPrefix + pad(blockIds.length, 6);
            console.log("block id = " + blockId);
            blockIds.push(btoa(blockId));
            reader.readAsArrayBuffer(fileContent);
            currentFilePointer += maxBlockSize;
            totalBytesRemaining -= maxBlockSize;
            if (totalBytesRemaining < maxBlockSize) {
                maxBlockSize = totalBytesRemaining;
            }
        } else {
            emaileditor_commitBlockList(message);
        }
    }

    emaileditor_uploadFileInBlocks(message);

    function emaileditor_commitBlockList(message) {

        var submituri = uri + '&comp=blocklist';
        var requestBody = '<?xml version="1.0" encoding="utf-8"?><BlockList>';
        for (var i = 0; i < blockIds.length; i++) {
            requestBody += '<Latest>' + blockIds[i] + '</Latest>';
        }
        requestBody += '</BlockList>';
        $.ajax({
            url: submituri,
            type: "PUT",
            data: requestBody,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('x-ms-blob-content-type', file.type);
                xhr.setRequestHeader('Content-Length', requestBody.length);
            },
            success: function (data, status) {

                $('#divLoading').hide();
                $('#event_contentInput').summernote('insertImage', imgUrl + blobName, function ($image) {
                    $image.css({ 'max-width': $image.width(), 'width': '100%' });
                    $image.attr('data-filename', '');
                });
            },
            error: function (xhr, desc, err) {
                $('#divLoading').hide();
                custom_shownotification('error', 'Lỗi upload hình');
            }
        });

    }
}

function emaileditor_LoadUserCustomField(apiDomain, access_token, message){
    var data= appshare_LoadField(apiDomain, access_token, message);
    switch(parseInt(data.ErrorCode))
    {
        case 0:
            var html = '';
            if (data.Data.length > 0)
            {
                $.each(data.Data, function (i, o) {
                    html += '<li><a href="#" data-value="#' + o.FieldName + '#">' + o.FieldName + '</a></li>'
                });
            }
            $('.dropdown-menu.dropdown-usertemplate').append(html);
            break;
    }
}

function emaileditor_LoadSystemCustomField(apiDomain, access_token, message) {
    var data = appshare_LoadSystemField(apiDomain, access_token, message);
    var user = appshare_LoadField(apiDomain, access_token, message);
   
    switch (parseInt(data.ErrorCode)) {
        case 0:
            var html1 = '';
            if (data.Data.length > 0) {
                $.each(data.Data, function (i, o) {
                    if (o.FieldName == 'Name')
                    {
                        html1 += '<li><a href="#" data-value="#ContactName#">ContactName</a></li>';
                    }
                    else{
                        html1 += '<li><a href="#" data-value="#' + o.FieldName + '#">' + o.FieldName + '</a></li>'
                    }
                });
            }
            $('.dropdown-menu.dropdown-template').append(html1);
            break;
    }
    switch (parseInt(user.ErrorCode)) {
        case 0:
            var html2 = '';
            if (user.Data.length > 0) {
                $.each(user.Data, function (i, o) {
                    html2 += '<li><a href="#" data-value="#' + o.FieldName + '#">' + o.FieldName + '</a></li>'
                });
            }
            $('.dropdown-menu.dropdown-template').append(html2);
            break;
    }
    $('.dropdown-menu.dropdown-template').css({ 'height': '400px', 'overflow-y': 'scroll', 'width': '100%' });
}
