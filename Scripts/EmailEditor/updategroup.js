﻿function updategroup_EventUpdateGroup(apiDomain, access_token, GroupId, message) {
    $('#updatecontactgroup').on('click', function () {
        if (updategroup_CheckNull() == true) {
            updategroup_Update(apiDomain, access_token,GroupId, message)
        }
    });

};

function updategroup_LoadData(data)
{
    if (data.Active == 'True') {
        $('#ucheckGroupActive').prop('checked',true);
    }
    $('#ucontactgroupname').val(data.ContactGroupName);
    if (data.Desciption == null || data.Desciption == 'null')
    {
        data.Desciption = '';
    }
    $('#ucontactgroupdesciption').val(data.Desciption);
}


//update contactgroup
function updategroup_Update(apiDomain, access_token, GroupId, message) {
    var active = true;
    if ($('#ucheckGroupActive').prop('checked')) {
        active = true;
    } else {
        active = false;
    }
    var data = {
        ContactGroupId:GroupId,
        Active: active,
        ContactGroupName: $('#ucontactgroupname').val(),
        Description: $('#ucontactgroupdesciption').val(),
    };
    $.ajax({
        url: apiDomain + 'api/ContactGroup/Update',
        type: 'POST',
        data: data,
        contentType: 'application/x-www-form-urlencoded',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            var errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
            switch (errorcode) {
                case 0:
                    custom_shownotification("success", message.Success);
                    $('#modalCreateGroup').modal('toggle');
                    //sau khi them thanh cong refesh table.
                    $("#tableContactGroup").dataTable().fnDraw();
                    break;
            }
        },
        error: function (x, s, e) {
            custom_shownotification("success", message.Error);
        },
    })
}

//kiểm tra text null
function updategroup_CheckNull() {
    if ($('#ucontactgroupname').val() == null | $('#ucontactgroupname').val() == "") {
        return false;
    }
    else {
        return true;
    }
};
