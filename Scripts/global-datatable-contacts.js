﻿

//Hàm chung cho tất cả các table./-------------------------------------------------

var rows_contact_selected = []; //tới đây
var rows_contact_data_selected = [];
function global_Contact_DataTable(apiDomain, access_token, tableId, dataUrl, columnDefine, AdditionalParameters, height, row, message, permissiontable) {
    var heightScroll = height;
    var columns = columnDefine.columns;
    var columnDefs = new Array();

    //check box
    if (columnDefine.columnCheckbox) {
        var obj = {
            'targets': [0],
            'render': function (data, type, full, meta) {
                return '<div class="checker"><span><input type="checkbox" class="checkboxes" value="' + data + '"></span></div>';

            },
            'width': "1%",
            className: "dt-body-left",
        };
        columnDefs.push(obj);
    }
   

    //define table contact list
    if (columnDefine.customContactDefs) {
        var obj = {
            'targets': [1],
            'render': function (data, type, full, meta) {
                if (data != null && data != '') {
                    var char = data.charAt(0);

                } else {
                    var char = 'E';
                    data = '(Email chưa có dữ liệu)';
                }
                var randomColor = Math.floor(Math.random() * 16777215).toString(16);
                var isOk = /^#[0-9A-F]{6}$/i.test('#' + randomColor);
                if (isOk == true) {
                    return '<div class="email-width-bkg"><span  id="info" class="imgletter" style="cursor: pointer; text-transform: uppercase ;background-color:#' + randomColor + ';margin-right:10px;">' + char + '</span><span style="font-weight:500;">' + data + '</span></div>';
                }
                else {
                    return '<div class="email-width-bkg"><span id="info" class="imgletter" style="cursor: pointer; text-transform: uppercase ;background-color:#5099BD;margin-right:10px;">' + char + '</span><span style="font-weight:500;">' + data + '</span></div>';

                }
            }
        };
        var objdate = {
            'targets': [4],
            'render': function (data, type, full, meta) {
                return moment(moment.utc(data).toDate()).format('YYYY-MM-DD HH:mm');
            },
            className: "dt-body-right"
        }
        var tool = {
            'targets': [columns.length - 1],
            'render': function (data, type, full, meta) {
                var edit = '<a id="edit" class="btn btn-xs green-jungle-stripe default tooltips" data-toggle="tooltip" data-placement="top" title="' + message.Edit + '" style="margin:3px;"><i class="icon-note"></i> ' + message.Edit + '</a>';
                var del = '<a id="delete" class="btn btn-xs red-stripe default tooltips" data-toggle="tooltip" data-placement="top" title="' + message.Delete + '" style="margin:3px;"><i class="fa fa-remove"></i> ' + message.Delete + '</a>';
                var tool = '';
                if (permissiontable.IsParent) {
                    tool = edit + del;
                } else {
                    if (parseInt(permissiontable.ManageContact) == 2) {
                        tool = edit + del;
                    }
                }
                return tool;

                ;
            },
            className: "dt-body-right"
        };
        columnDefs.push(obj);
        columnDefs.push(objdate);
        columnDefs.push(tool);
    }

   
    //define page leng
    var pageLength = 5;
    switch (row) {
        case 5: pageLength = [[5, 20, 50, 100], [5, 20, 50, 100]];
            break;
        case 10:
            pageLength = [[10, 20, 50, 100], [10, 20, 50, 100]];
            break;
        case 20:
            pageLength = [[20, 50, 100], [20, 50, 100]];
            break;
    }

    //khai báo tabable
    var CustomColumn = [];
    var table = $(tableId).DataTable({
        "dom": "<'row'<'col-sm-12'tr>>" +
              "<'row'<'col-sm-4'l><'col-sm-3'i><'col-sm-5'p>>",

        "serverSide": true,

        "ajax": {
            "url": apiDomain + dataUrl,
            "type": 'GET',
            "contentType": 'application/x-www-form-urlencoded',
            "beforeSend": function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            "data": AdditionalParameters,
            "error": handleAjaxError, //định nghĩa lại thông báo lỗi
            "dataSrc": function (json) {
                var return_data = new Array();
                var data = json.data;
                for (var i = 0; i < data.length; i++) {
                    var obj_data = {
                        "contactId": data[i].contactId,
                        "email": data[i].email,
                        "contactName": data[i].contactName,
                        "mobile": data[i].mobile,
                        "createDate": data[i].createDate,

                    };
                    CustomColumn = [
                      { "data": data.contactId, 'title': 'contactId', },
                      { "data": data.email, 'title': 'email', },
                      { "data": data.contactName, 'title': 'contactName', },
                      { "data": data.mobile, 'title': 'mobile', },
                      { "data": data.createDate, 'title': 'createDate', },
                    ];
                    var customfield = data[i].customFields;
                    if (customfield.length > 0) {
                        for (var j = 0; j < customfield.length; j++) {
                            obj_data[customfield[j].CustomFieldName] = customfield[j].CustomFieldValue;
                            CustomColumn.push({ "data": customfield[j].CustomFieldName, 'title': customfield[j].CustomFieldName, });
                        }
                    }
                    obj_data.zzzzId = data[i].contactId;
                    CustomColumn.push({ "data": data[i].contactId, 'title': 'contactId', });
                    return_data.push(obj_data);
                }
                return return_data;
            }
        },
        "ordering": false,
        "bSort": false,
        "pageLength": row,
        "bFilter": true, //disable search
        'searchDelay': 2000, //delay thời gian search
        "lengthMenu": pageLength,
        "bAutoWidth": false,
        "columns": CustomColumn,
        "columnDefs": columnDefs,

        "scrollY": heightScroll + 'px',

        "scrollCollapse": true,
        "fnInitComplete": function (oSettings, json) {
            App.unblockUI('body');


        },
        "fnDrawCallback": function (row, data, dataIndex) {
            App.unblockUI('body');
            //nếu snapshot email lỗi thì lấy hình mặt định
            $('img.thumnail-list-email').bind('error', function () {
                $(this).attr("src", "/Content/Images/thumbnail-generator.png");
            });
            //set chiều cao để snapshot middle
            $('.helper-thumnail-list-email').css({ 'height': $(this).parent().find('.div-parent-name-campaign').height() });

        },
        'rowCallback': function (row, data, dataIndex) {

            $(row).find('#switchpublish').bootstrapSwitch();

            $(row).find('.tooltips').tooltip();


            // Get row ID
            var rowId = custom_GetFisrtProJson(data);

            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rows_selected) !== -1) {
                $(row).find('input[type="checkbox"]').prop('checked', true);
                $(row).find('input[type="checkbox"]').parent().addClass('checked');
                $(row).addClass('selected');
            }
            //chiều cao tối thiểu cho table =tối đa scroll
            $('.dataTables_scrollBody').css('min-height', heightScroll);

        }
    });

    //xử lí thông báo lỗi khi table gửi ajax load dữ liệu không thành công
    function handleAjaxError(xhr, textStatus, error) {
        App.unblockUI('body');
        if (xhr.status == '401') {
            custom_shownotification('error', 'Phiên đăng nhập hết hạn! Vui lòng đăng nhập lại');
            setTimeout(function () { window.location.href = '/login' }, 1000);
        }
        else {
            custom_shownotification('error', 'An error occurred on the server. Please try again latter.');
            $(tableId).dataTable().fnProcessingIndicator(false);
        }
    }

    //dừng xử lí trên table
    jQuery.fn.dataTableExt.oApi.fnProcessingIndicator = function (oSettings, onoff) {
        if (typeof (onoff) == 'undefined') {
            onoff = true;
        }
        this.oApi._fnProcessingDisplay(oSettings, onoff);
    };

    //khai báo tooltip
    $('[data-toggle="tooltip"]').tooltip();



}
