﻿function resetpassword_Action(apiDomain, Email, message)
{
    resetpassword_UpdatePass(apiDomain, Email, message);
}
function resetpassword_UpdatePass(apiDomain, Email, message) {
    $('#btn-request-forget-pass').on('click', function () { 
        if (checkData(message)) {
            $('divLoading').show();
            $.ajax({
                url: apiDomain + 'api/User/ResetPassword?Email=' + Email + '&Password=' + $('#new-password').val(),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (r) {
                    $('divLoading').hide();
                    var error = parseInt(r.ErrorCode)
                    switch (error) {
                        case 0:
                            $('.alert-danger', $('.resetpass-form')).show();
                            $('.alert-danger span', $('.resetpass-form')).text(message.ResetPasswordSuccess);
                            $('#new-password').val("");
                            $('#renew-password').val("");
                            setTimeout(function () { window.location.href='/login' }, 2000);
                            break;
                        case 101:
                            $('.alert-danger', $('.resetpass-form')).show();
                            $('.alert-danger span', $('.resetpass-form')).text(message.ResetPasswordError);
                            break;
                    }
                },
                error: function (x, s, e) {
                    $('divLoading').hide();
                    $('.alert-danger', $('.resetpass-form')).show();
                    $('.alert-danger span', $('.resetpass-form')).text(message.ResetPasswordError);
                }

            })
        }
    });
};

function checkData(message)
{
    if(!$('#new-password').val())
    {
        $('.alert-danger', $('.resetpass-form')).show();
        $('.alert-danger span', $('.resetpass-form')).text(message.PasswordRequire);
        return false;
    }
    if (!$('#renew-password').val())
    {
        $('.alert-danger', $('.resetpass-form')).show();
        $('.alert-danger span', $('.resetpass-form')).text(message.RePasswordRequire);
        return false;
    }
    if ($('#renew-password').val().localeCompare($('#new-password').val()) != 0) {
        $('.alert-danger', $('.resetpass-form')).show();
        $('.alert-danger span', $('.resetpass-form')).text(message.RePasswordNotCompare);
        return false;
    }
    else return true;

}