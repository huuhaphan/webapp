﻿function global_updateDataTableSelectAllCtrl(table) {
    var $table = table.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('#select_all');

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        //chkbox_select_all.checked = false;
        $('#select_all').parent().removeClass('checked');
        chkbox_select_all.prop('checked', false);
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        //chkbox_select_all.checked = true;
        chkbox_select_all.prop('checked', true);
        $('#select_all').parent().addClass('checked');
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {
        //chkbox_select_all.checked = true;
        chkbox_select_all.prop('checked', true);
        $('#select_all').parent().removeClass('checked');
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }

}
//draft campaign
function global_updateDataTableSelectAllCtrlDraft(table) {
    var $table = table.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    //var chkbox_select_all = $('#select_all', $table).get(0);
    var chkbox_select_all = $('#select_all_draft');

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        chkbox_select_all.checked = false;
        $('#select_all_draft').parent().removeClass('checked');
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        chkbox_select_all.checked = true;
        $('#select_all_draft').parent().addClass('checked');
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {
        chkbox_select_all.checked = true;
        $('#select_all_draft').parent().removeClass('checked');
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }

}



//Hàm chung cho tất cả các table./-------------------------------------------------

var rows_selected = [];
var rows_data_selected = [];
function global_DataTable(apiDomain, access_token, tableId, dataUrl, columnDefine, AdditionalParameters, height, row, message, permissiontable) {
    var heightScroll = height;
    var columns = columnDefine.columns;
    var columnDefs = new Array();

    //check box
    if (columnDefine.columnCheckbox) {
        var obj = {
            'targets': [0],
            'render': function (data, type, full, meta) {
                return '<div class="checker"><span><input type="checkbox" class="checkboxes" value="' + data + '"></span></div>';

            },
            'width': "1%",
            className: "dt-body-left",
        };
        columnDefs.push(obj);
    }
    //active
    if (columnDefine.columnActive) {
        var obj = {
            'targets': [columns.length - 2],
            'render': function (data, type, full, meta) {
                if (data === 'true' | data === true) {
                    return '<span class="label label-success">Active</span>';
                }
                if (data === 'false' | data === false) {
                    return '<span class="label label-danger">Inactive</span>';
                }

            }
        };
        columnDefs.push(obj);
    }
    //subcribe
    if (columnDefine.columnUnsubcribe) {
        var obj = {
            'targets': [columns.length - 2],
            'render': function (data, type, full, meta) {
                if (data === 'false' | data === false) {
                    return '<span class="label label-success">subcribe</span>';
                }
                if (data === 'true' | data === true) {
                    return '<span class="label label-danger">unsubcribe</span>';
                }

            }
        };
        columnDefs.push(obj);


    }
    //login log
    if (columnDefine.loginlogDefs) {
        var obj = {
            'targets': [columns.length - 2],
            'render': function (data, type, full, meta) {
                if (data === 'false' | data === false) {
                    return '<span class="label label-danger">' + columnDefine.ColumnSuccessFalseLabel + '</span>';

                }
                if (data === 'true' | data === true) {
                    return '<span class="label label-success">' + columnDefine.ColumnSuccessTrueLabel + '</span>';

                }

            }
        };
        columnDefs.push(obj);

        var obj1 = {
            'targets': [0],
            'render': function (data, type, full, meta) {
                return moment(data).local().format("DD-MM-YYYY HH:mm");

            }
        };
        columnDefs.push(obj1);
    }

    //monthly plan log
    if (columnDefine.monthlyplanlogDefs)
    {
        var obj = {
            'targets': [1],
            'render': function (data, type, full, meta) {
                return moment(data).local().format("DD-MM-YYYY HH:mm");

            },
            
        };

        var obj1 = {
            'targets': [2],
            'render': function (data, type, full, meta) {
                return moment(data).local().format("DD-MM-YYYY HH:mm");

            },
        }
        columnDefs.push(obj);
        columnDefs.push(obj1);
    }


    //define table contact group
    if (columnDefine.contactGroupDefs) {
        var obj = {
            'targets': [1],
            'render': function (data, type, full, meta) {
                var active = "";
                var campaign = "";
                if (full.active == true) {
                    active = '' + message.Status + ': <span class="badge badge-success badge-roundless">Active</span>';
                }
                else{
                    active = '' + message.Status + ': <span class="badge badge-danger badge-roundless">Inactive</span>';
                }
                if (full.campaignId) {
                    if (parseInt(permissiontable.AutoCampaign) != 0) {
                        var campaign = '<a title="Danh sách thuộc chiến dịch autoresponder" href="/advancedcampaign/managecampaign?eId=' + full.campaignId + '" class="badge badge-warning badge-roundless" style="margin-left:5px;"><span>auto</span></a> ';
                    }
                }
                
                return '<a id="manage"><span class="custom-p-table">' + full.contactGroupName + '</span></a> </br>' +
                    //'<span>' + message.DateCreated + ': ' + moment(moment.utc(full.createDate).toDate()).format('YYYY-MM-DD HH:mm') + '</span></br>' +
                    '<span >' + message.TotalContact + ': <b>' + formatMoney(full.contactCount, "") + '</b> <i class="icon-user" style="color:gray"></i></span></br>' + active + campaign;
                    
            }
        };

        var date = {
            'targets': [2],
            'render': function (data, type, full, meta) {
                return moment(moment.utc(full.createDate).toDate()).format('YYYY-MM-DD HH:mm');

            },
            className: "dt-body-right"
        };

        var tool = {
            'targets': [columns.length - 1],
            'render': function (data, type, full, meta) {
                var manage = '<a id="manage" class="btn btn-xs green-jungle-stripe default popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Manager + '" style="margin:3px;"><i class="icon-notebook"></i> ' + message.Manager + '</a>';
                var edit = '<a id="edit" class="btn btn-xs green-jungle-stripe default popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Edit + '" style="margin:3px;"><i class="icon-note"></i> ' + message.Edit + '</a>';
                var del = '<a id="delete" class="btn btn-xs red-stripe default popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Delete + '" style="margin:3px;"><i class="fa fa-remove"></i> ' + message.Delete + '</a>';
                var tool = '';
                if (permissiontable.IsParent) {
                    tool = manage + edit + del;
                } else {
                    if (parseInt(permissiontable.ManageGroup) == 2) {

                        tool = manage + edit + del;

                    }
                    if (parseInt(permissiontable.ManageGroup) == 1) {

                        tool = manage;


                    }
                }
                return tool;

            },
            className: "dt-body-right"
        };
        columnDefs.push(obj);
        columnDefs.push(tool);
        columnDefs.push(date);
    }

    //define table contact list
    if (columnDefine.contactDefs) {

        var obj = {
            'targets': [1],
            'render': function (data, type, full, meta) {
                //return '<a id="info"><span style= "color:#00AFEC;font-size:18px;">' + full.contactName + '</span></a></br><span>'+message.DateCreated+': ' + moment(new Date(full.createDate)).local().format("DD-MM-YYYY HH:mm")+'</span><br/>'+
                //    '<span>'+full.note+'</span>';
                if (data != null && data != '') {
                    var char = data.charAt(0);

                } else {
                    var char = 'E';
                    data = '(Email chưa có dữ liệu)';
                }
                var randomColor = Math.floor(Math.random() * 16777215).toString(16);
                var isOk = /^#[0-9A-F]{6}$/i.test('#' + randomColor);

                var edit = '<a id="edit" class="btn btn-xs green-jungle-stripe default popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Edit + '" style="margin:3px;"><i class="icon-note"></i> ' + message.Edit + '</a>';
                var del = '<a id="delete" class="btn btn-xs red-stripe default popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Delete + '" style="margin:3px;"><i class="fa fa-remove"></i> ' + message.Delete + '</a>';
                var tool = '';
                if (permissiontable.IsParent) {
                    tool = edit + del;
                } else {
                    if (parseInt(permissiontable.ManageContact) == 2) {
                        tool = edit + del;
                    }
                }

                if (isOk == true) {
                    return '<div class="email-width-bkg"><span  id="info" class="imgletter" style="cursor: pointer; text-transform: uppercase ;background-color:#' + randomColor + ';margin-right:10px;">' + char + '</span><span style="font-weight:500;">' + data + '</span><span class="tool-contactlist hide">' + tool + '</span></div>';
                }
                else {
                    return '<div class="email-width-bkg"><span id="info" class="imgletter" style="cursor: pointer; text-transform: uppercase ;background-color:#5099BD;margin-right:10px;">' + char + '</span><span style="font-weight:500;">' + data + '</span><span class="tool-contactlist hide">' + tool + '</span></div>';

                }
            },
         
        };

        var objdate = {
            'targets': [4],
            'render': function (data, type, full, meta) {
                return '<span title="Create date">'+ moment(moment.utc(data).toDate()).format('YYYY-MM-DD HH:mm')+'</span>';
            },
            className: "dt-body-left"
        }
        var obj5 = {
            'targets': [5],
            'render': function (data, type, full, meta) {
                //var cf = full.customFields;
                //if (cf.length > 0) {
                //    var dataobje = getObjects(cf, 'customFieldName', 'FirstName')[0];
                //    if (dataobje)
                //        return '<span title="' + dataobje.customFieldName + '">' + dataobje.customFieldValue + '</span>';
                //    else return '';
                //} else return '';
                if (full.firstName == null || full.firstName == 'null') {
                    full.firstName = '';
                }
                return '<span title="FirstName">' + full.firstName + '</span>';
               

            },
            className: "dt-body-left",
            "visible": false,
           
        }
        var obj6 = {
            'targets': [6],
            'render': function (data, type, full, meta) {
                //var cf = full.customFields;
                //if (cf.length > 0) {

                //    var dataobje = getObjects(cf, 'customFieldName', 'LastName')[0];
                //    if (dataobje)
                //        return '<span title="' + dataobje.customFieldName + '">' + dataobje.customFieldValue + '</span>';
                //    else return '';
                //} else return '';
                if (full.lastName == null || full.lastName == 'null') {
                    full.lastName = '';
                }
                return '<span title="LastName">' + full.lastName + '</span>';

            },
            className: "dt-body-left",
            "visible": false,
        }
        var obj7 = {
            'targets': [7],
            'render': function (data, type, full, meta) {
                if (full.birthDate != null) {
                    return '<span title="BirthDate">' + moment(full.birthDate).format('DD-MM-YYYY') + '</span>';
                }
                else {
                    return '';
                }
                 //var cf = full.customFields;
                //if (cf.length > 0) {

                //    var dataobje = getObjects(cf, 'customFieldName', 'BirthDate')[0];
                //    if (dataobje)
                //        return '<span title="' + dataobje.customFieldName + '">' + dataobje.customFieldValue + '</span>';
                //    else return '';
                //} else return '';

            },
            className: "dt-body-left",
            "visible": false,
        }
        var obj8 = {
            'targets': [8],
            'render': function (data, type, full, meta) {
                var cf = full.customFields;
                if (cf.length > 0) {

                    var dataobje = getObjects(cf, 'customFieldName', 'Age')[0];
                    if (dataobje)
                        return '<span title="' + dataobje.customFieldName + '">' + dataobje.customFieldValue + '</span>';
                    else return '';
                } else return '';

            },
            className: "dt-body-left",
            width: "300px",
            "visible": false,
        }
        var obj9 = {
            'targets': [9],
            'render': function (data, type, full, meta) {
                //var cf = full.customFields;
                //if (cf.length > 0) {

                //    var dataobje = getObjects(cf, 'customFieldName', 'Gender')[0];
                //    if (dataobje)
                //        return '<span title="' + dataobje.customFieldName + '">' + dataobje.customFieldValue + '</span>';
                //    else return '';
                //} else return '';
                
                    return '<span title="Gender">' + full.gender + '</span>';

            },
            className: "dt-body-left",
            "visible": false,
        }
        var obj10 = {
            'targets': [10],
            'render': function (data, type, full, meta) {
                var cf = full.customFields;
                if (cf.length > 0) {
                    var dataobje = getObjects(cf, 'customFieldName', 'Fax')[0];
                    if (dataobje)
                        return '<span title="' + dataobje.customFieldName + '">' + dataobje.customFieldValue + '</span>';

                    else return '';
                } else return '';

            },
            className: "dt-body-left",
            "visible": false,
        }
        var obj11 = {
            'targets': [11],
            'render': function (data, type, full, meta) {
                var cf = full.customFields;
                if (cf.length > 0) {
                    var dataobje = getObjects(cf, 'customFieldName', 'Company')[0];
                    if (dataobje)
                        return '<span title="' + dataobje.customFieldName + '">' + dataobje.customFieldValue + '</span>';

                    else return '';
                } else return '';

            },
            className: "dt-body-left",
            "visible": false,
        }
        var obj12 = {
            'targets': [12],
            'render': function (data, type, full, meta) {
                var note = full.note;
                if (note) {
                    if (note.length > 100) {
                        note = note.substring(0, 50) + '...';
                    }
                    return '<span title="Comment">' + note + '</span>';
                } else return '';

            },
            className: "dt-body-left",
            "visible": false,
        }
        var obj13 = {
            'targets': [13],
            'render': function (data, type, full, meta) {
                var cf = full.customFields;
                if (cf.length > 0) {

                    var dataobje = getObjects(cf, 'customFieldName', 'URL')[0];
                    if (dataobje)
                        return '<span title="' + dataobje.customFieldName + '">' + dataobje.customFieldValue + '</span>';
                    else return '';
                } else return '';

            },
            className: "dt-body-left",
            "visible": false,
        }
        var obj14 = {
            'targets': [14],
            'render': function (data, type, full, meta) {
                var cf = full.customFields;
                if (cf.length > 0) {
                    var dataobje = getObjects(cf, 'customFieldName', 'City')[0];
                    if (dataobje)
                        return '<span title="' + dataobje.customFieldName + '">' + dataobje.customFieldValue + '</span>';
                    else return '';
                } else return '';

            },
            className: "dt-body-left",
            "visible": false,
        }
        var obj15 = {
            'targets': [15],
            'render': function (data, type, full, meta) {
                var cf = full.customFields;
                if (cf.length > 0) {
                    var dataobje = getObjects(cf, 'customFieldName', 'Street')[0];
                    if (dataobje)
                        return '<span title="' + dataobje.customFieldName + '">' + dataobje.customFieldValue + '</span>';
                    else return '';
                } else return '';

            },
            className: "dt-body-left",
            "visible": false,
        }
        var obj16 = {
            'targets': [16],
            'render': function (data, type, full, meta) {
                var cf = full.customFields;
                if (cf.length > 0) {
                    var dataobje = getObjects(cf, 'customFieldName', 'Address')[0];
                    if (dataobje)
                        return '<span title="' + dataobje.customFieldName + '">' + dataobje.customFieldValue + '</span>';
                    else return '';
                } else return '';

            },
            className: "dt-body-left",
           "visible": false,
        }
        var obj17 = {
            'targets': [17],
            'render': function (data, type, full, meta) {
                var note = full.note;
                if (note) {
                    if (note.length > 100) {
                        note = note.substring(0, 50) + '...';
                    }
                    return '<span title="Comment">' + note + '</span>';
                } else return '';

            },
            className: "dt-body-left",
             "visible": false,
        }
        var obj18 = {
            'targets': [18],
            'render': function (data, type, full, meta) {
                var cf = full.customFields;
                if (cf.length > 0) {
                    var dataobje = getObjects(cf, 'customFieldName', 'Comment1')[0];
                    if (dataobje)
                        return '<span title="' + dataobje.customFieldName + '">' + dataobje.customFieldValue + '</span>';
                    else return '';
                } else return '';

            },
            className: "dt-body-left",
            "visible": false,
        }
        var obj19 = {
            'targets': [19],
            'render': function (data, type, full, meta) {
                var cf = full.customFields;
                if (cf.length > 0) {
                    var dataobje = getObjects(cf, 'customFieldName', 'Comment2')[0];
                    if (dataobje)
                        return '<span title="' + dataobje.customFieldName + '">' + dataobje.customFieldValue + '</span>';
                    else return '';
                } else return '';

            },
            className: "dt-body-left",
            "visible": false,
        }
        var obj20 = {
            'targets': [20],
            'render': function (data, type, full, meta) {
                var cf = full.customFields;
                if (cf.length > 0) {
                    var dataobje = getObjects(cf, 'customFieldName', 'Comment3')[0];
                    if (dataobje)
                        return '<span title="' + dataobje.customFieldName + '">' + dataobje.customFieldValue + '</span>';
                    else return '';
                } else return '';

            },
            className: "dt-body-left",
            "visible": false,
        }
        var tool = {
            'targets': [columns.length - 1],
            'render': function (data, type, full, meta) {
                var edit = '<a id="edit" class="btn btn-xs green-jungle-stripe default popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Edit + '" style="margin:3px;"><i class="icon-note"></i> ' + message.Edit + '</a>';
                var del = '<a id="delete" class="btn btn-xs red-stripe default popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Delete + '" style="margin:3px;"><i class="fa fa-remove"></i> ' + message.Delete + '</a>';
                var tool = '';
                if (permissiontable.IsParent) {
                    tool = edit + del;
                } else {
                    if (parseInt(permissiontable.ManageContact) == 2) {
                        tool = edit + del;
                    }
                }
                return tool;

                ;
            },
            className: "dt-body-right",
          
        };
        columnDefs.push(obj);
        columnDefs.push(objdate);
        columnDefs.push(obj5);
        columnDefs.push(obj6);
        columnDefs.push(obj7);
        columnDefs.push(obj8);
        columnDefs.push(obj9);
        columnDefs.push(obj10);
        columnDefs.push(obj11);
        columnDefs.push(obj12);
        columnDefs.push(obj13);
        columnDefs.push(obj14);
        columnDefs.push(obj15);
        columnDefs.push(obj16);
        columnDefs.push(obj17);
        columnDefs.push(obj18);
        columnDefs.push(obj19);
        columnDefs.push(obj20);
        columnDefs.push(tool);
    }

    // define table basiccampaing colum of basiccampaign(newsletter)
    if (columnDefine.newsLettetDefs) {
        var obj = {
            'targets': [1],
            'render': function (data, type, full, meta) {
                //đặt lịch
                var image = '/Content/Images/thumbnail-generator.png';
                if (full.snapShotUrl != null)
                {
                    image = full.snapShotUrl
                }
                    
                if (full.isScheduled == true && full.isSent == false) {
                    return '<div class="div-parent-name-campaign">' +
                        '<div class="pull-left div-preview-snapshot">' +
                         '<span class="helper-thumnail-list-email"></span>'+
                        '<img class="thumnail-list-email" src="' + image + '" style="cursor: zoom-in;width:100%;max-height: 85px;"></div>' +
                        '<div class="div-name-campaign" style="padding:5px;" class="pull-left"><a><p class="custom-p-table custom-name-on-table campaign-name-sent" id="tablecampaigname">' + full.campaignName + '</p></a><p style="margin: 6px 0;">' + message.Modify + ': ' + moment(moment.utc(full.lastModifyDate).toDate()).format('YYYY-MM-DD HH:mm') + '</p><p style="margin: 6px 0;"><span>' + '<span id="custom-label-table" class="label label-sm label-warning" >' + message.HasSchedule + '</span>' + ' ' + moment(moment.utc(full.scheduleDate).toDate()).format('YYYY-MM-DD HH:mm') + '</span></p></div></div>';
                }

                //đã gửi
                if (full.isSent == true  ){ // & full.isScheduled == false) {
                    return '<div class="div-parent-name-campaign"><div class="pull-left div-preview-snapshot">' +
                          '<span class="helper-thumnail-list-email"></span>'+
                        '<img class="thumnail-list-email" src="' + image + '" style="cursor: zoom-in;width:100%;max-height: 85px;"></div>' +
                    '<div class="div-name-campaign" style="padding:5px;" class="pull-left"><a><p class="custom-p-table custom-name-on-table campaign-name-sent" id="tablecampaigname">' + full.campaignName + '</p></a><p style="margin: 6px 0;">' + message.Modify + ': ' + moment(moment.utc(full.lastModifyDate).toDate()).format('YYYY-MM-DD HH:mm') + '</p><p style="margin: 6px 0;"><span>' + '<span id="custom-label-table"  class="label label-sm label-success">' + message.HasSent + '</span>' + ' ' + moment(moment.utc(full.sentDate).toDate()).format("DD-MM-YYYY HH:mm") + '</span></p></div></div>';

                }

                //nháp
                //if (full.isSent == false && full.isScheduled == false) {
                //    return '<a id="editsetting"><p class="custom-p-table custom-name-on-table" id="tablecampaigname">' + full.campaignName + '</p></a><p class="custom-p-table">' + message.Modify + ': ' + moment(full.lastModifyDate).local().format("DD-MM-YYYY HH:mm") + '</p>';

                //}
            }
        };

        var tools = {
            'targets': [columns.length - 1],
            'render': function (data, type, full, meta) {
                //đặt lịch
                if (full.isScheduled == true && full.isSent == false) {
                    var reuse = '<a id="re-used"  data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Reuse + '" class="btn btn-xs green-jungle-stripe default popovers " style="margin-right:5px;"><i class="fa fa-clone"></i>' + message.BtnCopy + '</a>';
                    var preview = '<a id="preview"  data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Preview + '" class="btn btn-xs green-jungle-stripe default popovers " style="margin:3px;"><i class="fa fa-desktop"></i>' + message.BtnPreview + '</a>';
                    var cancelschel = '<a id="cancelschedule" data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Cancelschedule + '" class="btn btn-xs yellow-gold-stripe default popovers" style="margin:3px;"><i class="fa fa-ban"></i>' + message.Cancelschedule + '</a>';
                    var tool = '';
                    if (permissiontable.IsParent) {
                        tool = reuse + preview + cancelschel;
                    } else {
                        if (permissiontable.RocketCampaign == 2) {
                            tool = reuse + preview + cancelschel;
                        }
                        if (permissiontable.RocketCampaign == 1) {
                            tool =  preview ;
                        }

                    }
                    return tool;
                                   
                }
                if (full.isSent == true) {
                    var reuse = '<a id="re-used"  data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Reuse + '" class="btn btn-xs green-jungle-stripe default popovers " style="margin-right:5px;"><i class="fa fa-clone"></i>' + message.BtnCopy + '</a>';
                    var preview = '<a id="preview"  data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Preview + '" class="btn btn-xs green-jungle-stripe default popovers " style="margin:3px;"><i class="fa fa-desktop"></i>' + message.BtnPreview + '</a>';
                    var report = '<a id="report"  data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Viewreport + '" class="btn btn-xs green-jungle-stripe default popovers " style="margin:3px;"><i class="fa fa-area-chart"></i>' + message.BtnReport + '</a>';
                    var tool = '';
                    if (permissiontable.IsParent) {
                        tool = reuse + preview + report;
                    } else {
                        if (permissiontable.Statistic == 1) {
                            if (permissiontable.RocketCampaign == 2) {
                                tool = reuse + preview + report;
                            }
                            if (permissiontable.RocketCampaign == 1) {
                                tool = preview + report;
                            }
                        } else {
                            if (permissiontable.RocketCampaign == 2) {
                                tool = reuse + preview;
                            }
                            if (permissiontable.RocketCampaign == 1) {
                                tool = preview;
                            }
                        }


                    }
                    return tool;
   
                }
                else {
                    return '<div></div>'
                }

            },
            className: "dt-body-right",
        };
        var infosent = {
            'targets': [2],
            'render':
                // ==>chờ dữ liệu tạm che không xóa
                function (data, type, full, meta) {   
                    // return '<span></span>';
                    if (full.totalContact == 0) {
                        full.totalContact = 'N/A';
                    }
                    
                    if (full.totalSMSContact == 0) {
                        full.totalSMSContact = 'N/A';
                    }
                   
                    if (parseInt(full.sendMethod) == 2) {
                        
                        return '<div id="custom-sent-table">' +
                                '<p>' +
                                '<h6 id="custom-totalsend-table" >' + full.totalContact + ' <span  style="color:#c8c8c8;">Email</h6></span><br>' +
                                '<h6 id="custom-totalsend-table" >' + full.totalSMSContact + ' <span style="color:#c8c8c8;">SMS</span></h6>' +
                                '</p>' +
                              '</div>'

                    }
                    if (parseInt(full.sendMethod) == 1) {

                        return '<div id="custom-sent-table">' +
                                '<p>' +
                                '<h6 id="custom-totalsend-table" >' + full.totalSMSContact + ' <span style="color:#c8c8c8;">SMS</span></h6>' +
                                '</p>' +
                              '</div>'

                    }
                    else {

                        return '<div id="custom-sent-table">' +
                                '<p>' +
                                   '<h6 id="custom-totalsend-table" >' + full.totalContact + ' <span  style="color:#c8c8c8;">Email</h6></span><br>' +
                                '</p>' +
                              '</div>'


                    }

               
            },
            className: "dt-body-left",
        }
        columnDefs.push(obj);
        columnDefs.push(tools);
        columnDefs.push(infosent);
    }


    // define table basiccampaing colum of basiccampaigndraft(newsletter)
    if (columnDefine.newsLettetDefsdraft) {
        var obj = {
            'targets': [1],
            'render': function (data, type, full, meta) {
                var image = '/Content/Images/thumbnail-generator.png';
                if (full.snapShotUrl != null) {
                    image = full.snapShotUrl
                }
                return '<div class="div-parent-name-campaign"><div class="pull-left div-preview-snapshot div-preview-snapshot-draft">' +
                     '<span class="helper-thumnail-list-email"></span>' +
                    '<img class="thumnail-list-email" src="' + image + '" style="cursor: zoom-in;width:100%;max-height: 85px;"></div>' +
                    '<div class="div-name-campaign"  style="padding:5px;" class="pull-left"><a id="editsetting"><p class="custom-p-table custom-name-on-table" id="tablecampaigname">' + full.campaignName + '</p></a><p style="margin: 6px 0;"><span>' + message.Modify + ': ' + moment(moment.utc(full.lastModifyDate).toDate()).format('YYYY-MM-DD HH:mm') + '</span></p><span style="margin:6px 0 !important" class="label label-sm label-warning">Nháp</span></div></div>';
            }
        };

        var infosent = {
            'targets': [2],
            'render': function (data, type, full, meta) {
                return '<div class="btn-group"><a id="btn-manage-togle" class="btn btn-xs green-jungle-stripe default dropdown-toggle" data-toggle="dropdown" href="javascript:;" aria-expanded="false"> ' + message.Tools + ' <i class="fa fa-angle-down"></i> </a> ' +
                        '<ul class="dropdown-menu pull-right" style="min-width:150px;"> ' +

                '<li><a id="editsetting" class="popovers"  data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Editsetting + '" " style="margin:3px;"><i class="fa fa-cog"></i>' + message.BtnSetting + '</a></li>' +
                    '<li><a id="editcontent" class="popovers"  data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Editcontent + '"  style="margin:3px;"><i class="fa fa-sticky-note-o"></i>' + message.BtnContent + '</a></li>' +
                    '<li><a id="editrecipient" class="popovers"   data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Editrecipient + '"style="margin:3px;"><i class="fa fa-user"></i>' + message.BtnRecipient + '</a></li>' +
                    '<li><a id="editconfirm" class="popovers"   data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Editconfirm + '"  style="margin:3px;"><i class="fa fa-check-square-o"></i>' + message.BtnConfirm + '</a></li>' +
                    '<li><a id="editdelete" class="popovers"   data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.BtnDelete + '"  style="margin:3px;"><i class="fa fa-remove"></i>' + message.BtnDelete + '</a></li>' +
               '</ul> </div>';
                // return '<div></div>'
            },
            className: "dt-body-right",
        }
        columnDefs.push(obj);
        columnDefs.push(infosent);
    }


      // define table advancecampaign
    if (columnDefine.advanceDefs) {
        var obj = {
            'targets': [1],
            'render': function (data, type, full, meta) {

                //return '<div class="pull-left"><img class="thumnail-list-email" src="/Content/Images/thumbnail-generator.png"></div>';
                return '<div class="pull-left" ><a id="manage"><span class="custom-p-table" id="tablecampaigname">' + full.campaignName + '</span></a></br><span>' + message.Modify + ': ' + moment(moment.utc(full.lastModifyDate).toDate()).format('YYYY-MM-DD HH:mm') + '</span><br/></div>';
            }
        };

        var tools = {
            'targets': [columns.length - 1],
            'render': function (data, type, full, meta) {
                var edit = '  <li>' +
                                                                           ' <a id="edit" href="javascript:;"><i class="fa fa-edit"></i> ' + message.Edit + ' </a>' +
                                                                       ' </li>';
                var manage = '  <li>' +
                                                                           ' <a  id="manage"  href="javascript:;"><i class="fa fa-paper-plane-o"></i> ' + message.Autoresponder + ' </a>' +
                                                                       ' </li>';
                var contact = '<li>' +
                                                                          '  <a id="contact" href="javascript:;"><i class="fa fa-user"></i> ' + message.Contacts +
                                                                           ' </a>' +
                                                                        '</li>';
                var del = '  <li>' +
                                                                           ' <a id="delete"  href="javascript:;"><i class="fa fa-remove"></i> ' + message.Delete + ' </a>' +
                                                                       ' </li>';
                var tool = '';
                if (permissiontable.IsParent) {
                    tool = edit + manage + contact + del;
                }
                else {
                    if (parseInt(permissiontable.AutoCampaign) == 2) {
                        if (parseInt(permissiontable.ManageContact) == 0) {
                            tool = edit + manage + del;
                        }else
                        {
                            tool = edit + manage + contact + del;
                        }
                    }
                    else {
                        if (parseInt(permissiontable.AutoCampaign) == 1) {
                            tool = manage + contact;
                        }
                    }
                }
                return '<div class="btn-group" id="btn-group-advancecampaign">' +
                                              '<a class="btn btn-xs green-jungle-stripe default dropdown-toggle" data-toggle="dropdown" href="javascript:; "> ' + message.Manage +
                                                                       ' <i class="fa fa-angle-down"></i>' +
                                                                   ' </a>' +
                                                                   ' <ul class="dropdown-menu pull-right" style="min-width:150px;">' +
                                                                     tool +
                                                                    '</ul>' +
                                                               ' </div>';

            },
            className: "dt-body-right td-vertical-align",
        };

        columnDefs.push(obj);
        columnDefs.push(tools);


    }

    //define manage advance campaign
    //if (columnDefine.manageadvanceDefs)
    //{
    //    var obj = {
    //        'targets': [1],
    //        'render': function (data, type, full, meta) {

    //            return '<div class="pull-left"><img class="thumnail-list-email" src="/Content/Images/thumbnail-generator.png"></div>' +
    //            '<div id="info-manage-advance" class="pull-left" style="padding: 5px 0 0 20px;">' +
    //                '<a><span id="tablecampaigname">' + full.campaignName + '</span></a></br>' +
    //                '<span>' + message.createDate + ': ' + moment(moment.utc(full.lastModifyDate).toDate()).format('YYYY-MM-DD HH:mm') + '</span><br/>' +
    //                '<span>Trạng thái: <input id="swicth-stutus-manage-advance" type="checkbox" checked class="make-switch" data-size="mini" data-off-text="OFF" data-on-text="ON"  data-on-color="success" data-off-color="warning"></span>' +
    //                '</div>';
    //        }
    //    };

    //    var tools = {
    //        'targets': [columns.length - 1],
    //        'render': function (data, type, full, meta) {
    //            return '<div class="btn-group">' +
    //                                          '<a class="btn btn-xs green-jungle-stripe default dropdown-toggle" data-toggle="dropdown" href="javascript:; "> Quản lý' +
    //                                                                   ' <i class="fa fa-angle-down"></i>' +
    //                                                               ' </a>' +
    //                                                               ' <ul class="dropdown-menu" style="min-width:150px;">' +
    //                                                                 '  <li>' +
    //                                                                       ' <a id="edit" href="javascript:;"><i class="fa fa-edit"></i> Hiệu chỉnh </a>' +
    //                                                                   ' </li>' +
    //                                                                 '  <li>' +
    //                                                                       ' <a  id="manage"  href="javascript:;"><i class="fa fa-paper-plane-o"></i> Sao chép </a>' +
    //                                                                   ' </li>' +
    //                                                                    '<li>' +
    //                                                                      '  <a href="javascript:;"><i class="fa fa-user"></i> Xem trước' +

    //                                                                       ' </a>' +
    //                                                                    '</li>' +
    //                                                                    '<li>' +
    //                                                                      '  <a href="javascript:;"><i class="fa fa-bar-chart"></i> Thống kê' +

    //                                                                       ' </a>' +
    //                                                                    '</li>' +
    //                                                                '</ul>' +
    //                                                           ' </div>' +
    //            '<a id="delete"  class="btn btn-xs red-stripe default" style="margin:3px;"><i class="fa fa-remove"></i>Xóa</a>';
    //        },
    //        className: "dt-body-right td-vertical-align",
    //    };
    //    var type = {
    //        'targets': [2],
    //        'render': function (data, type, full, meta) {
    //            if (full.TimeBase) {
    //                return '<p style="font-size:12px;margin-bottom: 0px !important;">TIME-BASED</p>' +
    //                       '<div class="background-basetime-manage">' +
    //                         '<input class="background-basetime-input" type="text" value="0">' +

    //                        '</div>' +
    //                       '<div style="padding-top:10px;"> Send immediately, any day of the week.</div>'
    //            }
    //            else
    //            {
    //                return '<p style="font-size:12px;margin-bottom: 0px !important;">CLICK</p>' +
    //                       '<div class="background-baseclick-manage">' +
    //                        '<i class="fa fa-mouse-pointer"></i>'+
    //                        '</div>' +
    //                       '<div style="padding-top:10px;"> Send immediately, any day of the week.</div>'
    //            }

    //        },
    //        className: "dt-body-left",
    //    };
    //    var contact = {

    //        'targets': [3],
    //        'render': function (data, type, full, meta) {
    //            if(full.TimeBase)
    //            {
    //                return '<p style="font-size:12px;margin-bottom: 0px !important;">NUMBER OF CONTACTS</p>' +
    //                       '<p><i class="icon-user custom-icon-user"></i><span style="padding-left:10px;font-size:35px;">0</span></p>'

    //            }
    //            else
    //            {
    //                return '<p style="font-size:12px;margin-bottom: 0px !important;">ALREADY SENT</p>' +
    //                       '<p><i class="icon-user custom-icon-user"></i><span style="padding-left:10px;font-size:35px;">0</span></p>'

    //            }
    //        },
                
    //        className: "dt-body-left",
    //    };

    //    columnDefs.push(obj);
    //    columnDefs.push(tools);

    //    columnDefs.push(type);
    //    columnDefs.push(contact);
    //}

    //define table newletter of advancecampaign
    if (columnDefine.newslettersDefs)
    {
        var obj = {
            'targets': [1],
            'render': function (data, type, full, meta) {

                return '<div class="pull-left"><img class="thumnail-list-email" src="/Content/Images/thumbnail-generator.png"></div>' +
                '<div class="pull-left" style="padding: 20px 0 0 20px;"><a><span class="custom-p-table" id="tablecampaigname">' + full.messageName + '</span></a></br><span>' + message.ModifyDate + ': ' + moment(full.lastModifyDate).local().format("DD-MM-YYYY HH:mm") + '</span><br/><span></div>';
            }
        };

        var tools = {
            'targets': [columns.length - 1],
            'render': function (data, type, full, meta) {
                return '<div class="btn-group">' +
                                              '<a class="btn btn-xs green-jungle-stripe default dropdown-toggle" data-toggle="dropdown" href="javascript:; ">'+ message.Tools +
                                                                       ' <i class="fa fa-angle-down"></i>' +
                                                                   ' </a>' +
                                                                   ' <ul class="dropdown-menu" style="min-width:150px;">' +
                                                                     '  <li>' +
                                                                           ' <a id="reused" href="javascript:;"><i class="fa fa-refresh"></i> '+message.ReUsed+' </a>' +
                                                                       ' </li>' +
                                                                     '  <li>' +
                                                                           ' <a  id="preview"  href="javascript:;"><i class="fa fa-desktop"></i> ' + message.PreView + ' </a>' +
                                                                       ' </li>' +
                                                                    '</ul>' +
                                                               ' </div>' +
                '<a id="delete"  class="btn btn-xs red-stripe default" style="margin:3px;"><i class="fa fa-remove"></i>' + message.Delete + '</a>';
            },
            className: "dt-body-right td-vertical-align",
        };

        columnDefs.push(obj);
        columnDefs.push(tools);
    }

    //define tabe manageform
    if (columnDefine.manageformDefs) {
        var obj = {
            'targets': [1],
            'render': function (data, type, full, meta) {
                var active = "";
                if (permissiontable.IsParent) {
                    if (full.active == true) {
                        active = '<input type="checkbox" id="switchpublish" checked class="make-switch" data-size="mini">';//'<input id="switchpublish" type="checkbox" checked class="make-switch" data-size="mini" data-off-text="OFF" data-on-text="ON"  data-on-color="success" data-off-color="warning">';
                    }
                    else {
                        active = '<input id="switchpublish" type="checkbox" class="make-switch" data-size="mini" data-off-text="OFF" data-on-text="ON"  data-on-color="success" data-off-color="warning">';
                    }
                }
                else {
                    if (parseInt(permissiontable.ManageForm) == 2) {
                        if (full.active == true) {
                            active = '<input type="checkbox" id="switchpublish" checked class="make-switch" data-size="mini">';//'<input id="switchpublish" type="checkbox" checked class="make-switch" data-size="mini" data-off-text="OFF" data-on-text="ON"  data-on-color="success" data-off-color="warning">';
                        }
                        else {
                            active = '<input id="switchpublish" type="checkbox" class="make-switch" data-size="mini" data-off-text="OFF" data-on-text="ON"  data-on-color="success" data-off-color="warning">';
                        }
                    }
                }
                return '<a id="edit"><span class="custom-p-table">' + full.formName + '</span>&nbsp <i id="icon-edit-form" class="icon-pencil hide"></i></a></a></br>' +
                       '<span>' + message.DateCreated + ': ' + moment(full.createDate).format("DD-MM-YYYY HH:mm") + '</span></br>' +active;
            }
        };
        var obj2 = {
            'targets': [2],
            'render': function (data, type, full, meta) {
                var dangki = 0;
                if (parseInt(full.subscribers) > 0)
                {
                    dangki = Math.round(parseFloat(full.subscribers / full.visitors) * 10000) / 100
                }
                return '<span>NGƯỜI ĐĂNG KÍ</span></br>' +
                       '<span class="font-lg bold">' + full.subscribers + '</span><br/>' +
                       '<span style="font-size: 10px;">TỈ LỆ ĐĂNG KÍ: ' + dangki + '%</span>'
            },
            className: "dt-body-left"
        };
        var obj3 = {
            'targets': [3],
            'render': function (data, type, full, meta) {
                return '<span>KHÁCH TRUY CẬP</span></br>' +
                       '<span class="font-lg bold">' + full.visitors + '</span></br>' +
                       '<span  style="font-size: 10px;">TRUY CẬP MỘT LẦN: ' + full.uniqueVisitors + '</span></br>'
            },
            className: "dt-body-left"
        };
        var tool = {
            'targets': [columns.length - 1],
            'render': function (data, type, full, meta) {
                var preview = '<a id="preview" class="btn btn-xs grey yellow-lemon-stripe popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.GetLink + '" style="margin:3px;"><i class="fa fa-globe"></i> ' + message.GetLink + '</a>';
                var edit = '<a id="editcustomfield" class="btn btn-xs grey green-stripe popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="'+message.TitleDesignForm+'" style="margin:3px;"><i class="fa fa-bars"></i> ' + message.Design + '</a>';
                var del = '<a id="delete" class="btn btn-xs grey red-stripe popovers"  data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.Delete + '" style="margin:3px;"><i class="fa fa-remove"></i> ' + message.Delete + '</a>';
                var setting = '<a id="setting" class="btn btn-xs grey green-stripe popovers"  data-container="body" data-trigger="hover" data-placement="top" data-content="' + message.TitleSettingForm + '" style="margin:3px;"><i class="fa fa-cog"></i> ' + message.Setting + '</a>';
                var tool = '';
                if (permissiontable.IsParent) {
                    tool = preview + setting + edit + del;
                }
                else {
                    if (parseInt(permissiontable.ManageForm) == 2) {
                        tool = preview +setting+ edit + del;
                    }
                    if (parseInt(permissiontable.ManageForm) == 1) {
                        tool = preview + edit;
                    }
                }
                return tool;
                      
                
            },
            className: "dt-body-right"
        };

        columnDefs.push(obj);
        columnDefs.push(obj2);
        columnDefs.push(obj3);
        columnDefs.push(tool);
    }

    //define ngày trong blancelog table
    if (columnDefine.banlancelogDefs) {
        var obj = {
            'targets': [columns.length - 1],
            'render': function (data, type, full, meta) {
                return moment(moment.utc(data).toDate()).format('YYYY-MM-DD HH:mm:ss');
            }
        };
        columnDefs.push(obj);
    }

    //define ngày trong paymentlog
    if (columnDefine.paymentlogDefs) {
        var obj1 = {
            'targets': [0],
            'render': function (data, type, full, meta) {
                return moment(new Date(data)).format("DD-MM-YYYY HH:mm");
            }
        };
        columnDefs.push(obj1);
        var obj2 = {
            'targets': [columns.length - 2],
            'render': function (data, type, full, meta) {
                if (data === 'false' | data === false) {
                    return '<span class="label label-danger">' + columnDefine.ColumnFalseLabel + '</span>';

                }
                if (data === 'true' | data === true) {
                    return '<span class="label label-success">' + columnDefine.ColumnTrueLabel + '</span>';

                }

            }
        };
        columnDefs.push(obj2);
    }

    //define sms campaign

    if (columnDefine.smscampaignDefs) {
        var obj0 = {
            'targets': [0],
            'render': function (data, type, full, meta) {
                var smstype = "";
                var status = "";
                if (parseInt(full.smsTypeId) == 2) {
                    smstype = "SMS Brandname"
                }
                else if (parseInt(full.smsTypeId) == 1) {
                    smstype = "SMS thường"
                }
                else {
                    smstype = "SMS Cá nhân"
                }
                if (full.isSent == true) {
                    status = '<span class="label label-success">' + message.HasSent + '</span>';
                }
                else if (full.isSent == false && full.isScheduled == true) {
                    status = '<span class="label label-warning">' + message.Schedule + '</span>';
                }
                else {
                    status = '<span class="label label-danger">' + message.NotSend + '</span>';
                }
                var messagename = "";
                if (full.messageName) {
                    messagename = full.messageName;
                }
                var date;
                if (full.isSent == true) {
                    date = moment(moment.utc(full.sendDate).toDate()).format('YYYY-MM-DD HH:mm');
                }
                else if (full.isSent == false && full.isScheduled == true) {

                    date = moment(moment.utc(full.scheduleDate).toDate()).format('YYYY-MM-DD HH:mm')
                }
                else {
                    date = moment(moment.utc(full.lastModifyDate).toDate()).format('YYYY-MM-DD HH:mm:ss');

                }

                return '<div><span class="custom-p-table" id="tablecampaigname">' + messagename + '</span><br/><span>Loại:  <span style="font-weight:600;">' + smstype + '</span></span></br>' +
                    '<span>Liên hệ: <span style="font-weight:600;">' + full.totalContact + '</span> <i class="fa fa-user" style="color:#c2cad8"></i></span>' +
                    '<span style="padding-left:10px;">Độ dài tin: <span style="font-weight:600;">' + full.messageLength + '</span> <i class="fa fa-envelope"  style="color:#c2cad8"></i></span></br>' +
                    date + '<br/>'
                status +
                '</div>'
            },
            className: "dt-body-left"

        };
        //var obj1 = {
        //    'targets': [3],
        //    'render': function (data, type, full, meta) {
        //        var date;
        //        if (full.isSent == true) {
        //            date = moment(full.sendDate).format("DD-MM-YYYY HH:mm");
        //        }
        //        else if (full.isSent == false && full.isScheduled == true) {
        //            date = moment(full.scheduleDate).format("DD-MM-YYYY HH:mm");
        //        }
        //        else {
        //            date = moment(full.lastModifyDate).format("DD-MM-YYYY HH:mm");
        //        }
        //        return date
        //    },
        //    className: "dt-body-center"

        //};
        var obj2 = {
            'targets': [2],
            'render': function (data, type, full, meta) {
                if (parseInt(full.smsTypeId) == 2) {
                    return full.brandName
                }
                else if (parseInt(full.smsTypeId) == 1) {
                    return '';
                }
                else {
                    return '';
                }
            },
          
        }
        var obj3 = {
            'targets': [3],
            'render': function (data, type, full, meta) {
                var button = "";
                var detail = "<a id='smsinfo' class='btn btn-xs default green-stripe'>" + message.Detail + "</a>";
                var schedule = "<a id='smscancelschedule' class='btn btn-xs default yellow-gold-stripe'>" + message.CancelSchedule + "</a>";
                var report = "<a id='smsreport' class='btn btn-xs default yellow-gold-stripe'>" + message.Report + "</a>";
                var update = "<a id='smsedit' class='btn btn-xs default red-stripe'>" + message.Update + "</a>";
                if (full.isSent == true) {
                    button = detail+ report;

                }
                else if (full.isSent == false && full.isScheduled == true) {
                    button = detail + schedule;
                }
                else {
                    button = update;
                }
                return button;
            },
            className: "dt-body-right"

        };
        columnDefs.push(obj0);
        columnDefs.push(obj2);
        columnDefs.push(obj3);
    }

    //define spam report in mail suppression
    if (columnDefine.spamreportDefs)
    {
        var obj0 = {
            'targets': [1],
            'render': function (data, type, full, meta) {
                var char = data.charAt(0);
                var randomColor = Math.floor(Math.random() * 16777215).toString(16);
                var isOk = /^#[0-9A-F]{6}$/i.test('#' + randomColor);
                if (isOk == true) {
                    return '<div class="email-width-bkg"><span class="imgletter" style="text-transform: uppercase;background-color:#' + randomColor + ';margin-right:10px;">' + char + '</span><span style="font-weight:500;">' + data + '</span></div>';
                }
                else
                {
                    return '<div class="email-width-bkg"><span class="imgletter" style="text-transform: uppercase;background-color:#818cda;margin-right:10px;">' + char + '</span><span style="font-weight:500;">' + data + '</span></div>';
                }
            }

        };
        columnDefs.push(obj0);
        var obj1 = {
            'targets': [2],
            'render': function (data, type, full, meta) {
                return moment(moment.utc(data).toDate()).format('YYYY-MM-DD HH:mm');
            }
        };
        columnDefs.push(obj1);

        var obj2 = {
            'targets': [4],
            'render': function (data, type, full, meta) {
                var del = '<a id="delete" class="btn btn-xs red btn-outline">Xóa</a>';
                var tool = '';
                if (permissiontable.ParentId) {
                    tool = del;
                } else {
                    if(parseInt(permissiontable.Suppression)==2)
                    {
                        tool = del;
                    }
                }
                return tool;
                
            },
            className: "dt-body-right"
        };
        columnDefs.push(obj2);
    }

    //icon toolbox
    var strAction = '';
    if (columnDefine.columnAction) {
        $.each(columnDefine.columnAction, function (i, val) {
            strAction += '<a id="' + val.id + '" class="btn btn-xs tooltips' + val.color + '" data-toggle="tooltip" data-placement="top" title="' + val.label + '" style="margin:3px;"><i class="' + val.icon + '"></i> ' + val.label + '</a>';
        });

        if (strAction.length > 0) {
            var obj = {
                'targets': [columns.length - 1],
                'render': function (data, type, full, meta) {
                    return strAction;
                },
                className: "dt-body-right"
            };
            columnDefs.push(obj);
        }
    }

    //define page leng
    var pageLength = 5;
    switch(row)
    {
        case 5: pageLength = [[5, 20, 50, 100], [5, 20, 50, 100]];
            break;
        case 10:
            pageLength = [[10, 20, 50, 100], [10, 20, 50, 100]];
            break;
        case 20:
            pageLength =[ [20, 50, 100], [20, 50, 100]];
            break;
    }

    //khai báo tabable
    var table = $(tableId).DataTable({
        "dom": "<'row'<'col-sm-12'tr>>" +
              "<'row'<'col-sm-4'l><'col-sm-3'i><'col-sm-5'p>>",

        "serverSide": true,
        "ajax": {
            url: apiDomain + dataUrl,
            type: 'GET',
            contentType: 'application/x-www-form-urlencoded',
            beforeSend: function (request) {
               request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            data: AdditionalParameters,
            error: handleAjaxError //định nghĩa lại thông báo lỗi
        },
        "ordering": false,
        "pageLength": row,
        "bFilter": true, //disable search
        'searchDelay': 2000, //delay thời gian search
        "lengthMenu": pageLength,
        "bAutoWidth": false,
        "columns": columns,
        "columnDefs": columnDefs,
        "scrollY": heightScroll + 'px',
        "scrollX": true,
        "scrollCollapse": true,
        "fnInitComplete": function (oSettings, json) {
            App.unblockUI('body');
           
                     
        },
        "fnDrawCallback":function(row, data, dataIndex)
        {
            App.unblockUI('body');
            //nếu snapshot email lỗi thì lấy hình mặt định
            $('img.thumnail-list-email').bind('error', function () {
                $(this).attr("src", "/Content/Images/thumbnail-generator.png");
            });
            //set chiều cao để snapshot middle
            $('.helper-thumnail-list-email').css({ 'height': $(this).parent().find('.div-parent-name-campaign').height() });
            
        },
        'rowCallback': function (row, data, dataIndex) {

            $(row).find('#switchpublish').bootstrapSwitch();

            $(row).find('.tooltips').tooltip();
            $(row).find('.popovers').popover();

            // Get row ID
            var rowId = custom_GetFisrtProJson(data);
            
            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rows_selected) !== -1) {
                $(row).find('input[type="checkbox"]').prop('checked', true);
                $(row).find('input[type="checkbox"]').parent().addClass('checked');
                $(row).addClass('selected');
            }
            //chiều cao tối thiểu cho table =tối đa scroll
            $('.dataTables_scrollBody').css('min-height', heightScroll);  
            
        }
    });

    //xử lí thông báo lỗi khi table gửi ajax load dữ liệu không thành công
    function handleAjaxError(xhr, textStatus, error) {
        App.unblockUI('body');
        if (xhr.status == '401') {
            custom_shownotification('error', 'Phiên đăng nhập hết hạn! Vui lòng đăng nhập lại');
            setTimeout(function () { window.location.href = '/login' }, 1000);
        }
        else {
            custom_shownotification('error', 'An error occurred on the server. Please try again latter.');
            $(tableId).dataTable().fnProcessingIndicator(false);
        }
    }
    
    //dừng xử lí trên table
    jQuery.fn.dataTableExt.oApi.fnProcessingIndicator = function (oSettings, onoff) {
        if (typeof (onoff) == 'undefined') {
            onoff = true;
        }
        this.oApi._fnProcessingDisplay(oSettings, onoff);
    };

   //khai báo tooltip
    $('[data-toggle="tooltip"]').tooltip();
    $('.popovers').popover();
    

}
