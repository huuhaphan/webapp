﻿function verify_Action(apiDomain, access_token,ItemId, message)
{
    verify_UpdateFromEmailVerify(apiDomain, access_token, ItemId, message);
}

function verify_UpdateFromEmailVerify(apiDomain, access_token, ItemId, message)
{
    if(ItemId)
    {
        $.ajax({
            url: apiDomain + 'api/User/UpdateFromMailVerify?ItemId=' + ItemId,
            type: 'post',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader('Authorization', 'Bearer ' + access_token);
            },
            success:function(r)
            {
                var data = r.Data;
                switch(parseInt(r.ErrorCode))
                {
                    case 0:
                        $('#status-verify-emailfrom').append(message.VeriFySuccess + ' <span style="color:#FFC107">' + data.FromMail + '</span> ' + message.Successfull);
                        break;
                    default:
                        $('#status-verify-emailfrom').text(message.VeriFyError);
                        $('#status-verify-emailfrom').css({ 'color': '#FFC107' });
                        break;
                }
            },
            error:function(x,s,e)
            {
                $('#status-verify-emailfrom').text(message.VeriFyError);
                $('#status-verify-emailfrom').css({ 'color': '#FFC107' });
            }
            
        })
    }
    else {
        $('#status-verify-emailfrom').text(message.DifferenceAcount);
        $('#status-verify-emailfrom').css({ 'color': '#FFC107' })
    }
}

var tm = 10;
function verify_Redirect() {
        tm--;
        $('#CodeHint').html('Trang tự động chuyển đến trang đăng nhập sau <span class="font-yellow-gold" id="lockTimer"></span>, hoặc bấm <a href="/login" class="font-yellow-gold">vào đây</a> để tiếp tục');
        $('#lockTimer').text(tm + 's');
        if (tm == 0) {
            window.location.href = "/Login";
        }
    
}

setInterval(function () {
    verify_Redirect();
}, 1000);