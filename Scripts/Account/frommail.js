﻿function frommail_Proccess(apiDomain, access_token, userId, message, permissiontable)
{
    //get list from email
    frommail_GetFromEmail(apiDomain, access_token, message, permissiontable);

    //show modal confirm delete
    frommail_ModalDeleteFromEmail();

    //delete 1 from email
    frommail_DeleteFromEmail(apiDomain, access_token, message, permissiontable);

    //show from add from email
    frommail_ShowAdd();

    //add 1 from email
    frommail_AddFromEmail(apiDomain, access_token, message, permissiontable);

    //set default 1 from email
    frommail_DefaultFromEmail(apiDomain, access_token, message, permissiontable);

    frommail_ResendFromEmail(apiDomain, access_token, message);
}



function frommail_GetFromEmail(apiDomain, access_token, message, permissiontable) {
    var data;
    $.ajax({
        url: apiDomain + 'api/User/GetFromMailList',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    frommail_TableFromEmail(data, message, permissiontable);
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
        }
    })

}

function frommail_TableFromEmail(data, message, permissiontable) {
    $('#table-config-from-email').children().remove();
    var html = "";
    if (data.length > 0) {
        $.each(data, function (i, o) {
            var tool = '';
            var status = '';
            if (!o.IsDefault) {
                if (!o.IsVerify) {
                    status = '<span style="font-size:13px;">' + message.VerificationPending + '</span><br/><span style="font-size:12px;">' + message.NotifyVerify + '  </span>';
                    if (permissiontable.IsParent) {
                        tool = '<a data-fromid="' + o.ItemId + '" data-frommail="' + o.FromMail + '" class="btn btn-xs default yellow-gold-stripe" id="resend">' + message.Reverify + '</a><a data-fromid="' + o.ItemId + '" class="btn btn-xs default red-stripe" id="delete"> ' + message.Delete + '</a></td>';

                    } else {
                        if (parseInt(permissiontable.FromMail) == 2) {
                            tool = '<a data-fromid="' + o.ItemId + '" data-frommail="' + o.FromMail + '" class="btn btn-xs default yellow-gold-stripe" id="resend">' + message.Reverify + '</a><a data-fromid="' + o.ItemId + '" class="btn btn-xs default red-stripe" id="delete"> ' + message.Delete + '</a></td>';

                        }
                    }
                }
                else {
                    status = '<span style="font-size:13px;">' + message.Verified + ' </span>';
                    if (permissiontable.IsParent) {
                        tool = '<a id="default" data-fromid="' + o.ItemId + '" class="btn btn-xs default green-jungle-stripe">' + message.Setdefault + ' </a>' +
                          '<a data-fromid="' + o.ItemId + '" class="btn btn-xs default red-stripe" id="delete"> ' + message.Delete + '</a>';

                    } else {
                        if (parseInt(permissiontable.FromMail) == 2) {
                            tool = '<a id="default" data-fromid="' + o.ItemId + '" class="btn btn-xs default green-jungle-stripe">' + message.Setdefault + ' </a>' +
                          '<a data-fromid="' + o.ItemId + '" class="btn btn-xs default red-stripe" id="delete"> ' + message.Delete + '</a>';

                        }
                    }
                }
            }
            else {
                if (!o.IsVerify) {
                    status = '<span><b style="font-size:13px;"> ' + message.Default + '</b></span><br/><span style="font-size:13px;">' + message.VerificationPending + '</span><br/><span style="font-size:12px;">' + message.NotifyVerify + '  </span>';
                    if (permissiontable.IsParent) {
                        tool = '<a data-fromid="' + o.ItemId + '" data-frommail="' + o.FromMail + '" class="btn btn-xs default green-jungle-stripe" id="resend">' + message.Reverify + '</a><a data-fromid="' + o.ItemId + '" class="btn btn-xs default red-stripe" id="delete"> ' + message.Delete + '</a></td>';
                    } else {
                        if (parseInt(permissiontable.FromMail) == 2) {
                            tool = '<a data-fromid="' + o.ItemId + '" data-frommail="' + o.FromMail + '" class="btn btn-xs default green-jungle-stripe" id="resend">' + message.Reverify + '</a><a data-fromid="' + o.ItemId + '" class="btn btn-xs default red-stripe" id="delete"> ' + message.Delete + '</a></td>';
                        }
                    }
                }
                else {
                    status = '<b style="font-size:13px;"> ' + message.Default + '</b>';
                }

            }



            html +=
                   '<tr>' +
                   '<td style="width:40%;"><p style="font-size:14px;margin-bottom:0px;"> ' + o.FromName + '</p> <span style="font-size:14px">' + o.FromMail + '</span></td>' +
                   '<td style="width:30%;">' + status + '</td>' +
                   '<td style="width:30%;"class="text-right"> ' +
                    tool +
                      '</td>'
            '</tr>';

        });
    } else {
        html += '<tr>' +
                   '<td colspan="3" class="text-center"><p style="font-size:14px;">'+message.NoFromEmail+' </span></td>' +

         '</tr>';
    }

    var height = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.portlet-title').outerHeight() - $('.page-footer').outerHeight() - 200;
    $('.inner_table').css({
        'height': height,
    })
    $('#table-config-from-email').append(html);
}

function frommail_ModalDeleteFromEmail() {
    $('#table-config-from-email').on('click', '#delete', function () {
        var ItemId = $(this).data('fromid');
        $('#confirm-delete-from-mail').data('fromid', ItemId);
        $('#modal-delete-from-email').modal('toggle');
    })

}

function frommail_DeleteFromEmail(apiDomain, access_token, message, permissiontable) {
    $('#confirm-delete-from-mail').on('click', function () {
        var ItemId = $(this).data('fromid');
        frommail_ActionDeleteFromEmail(apiDomain, access_token, ItemId, message, permissiontable);
    })
}

function frommail_ActionDeleteFromEmail(apiDomain, access_token, ItemId, message, permissiontable) {
    $.ajax({
        url: apiDomain + 'api/User/DeleteFromMail?ItemId=' + ItemId,
        type: 'Post',
        contentType: 'application/json; chartset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    custom_shownotification('success', message.DeleteSuccess);
                    $('#modal-delete-from-email').modal('toggle');
                    frommail_GetFromEmail(apiDomain, access_token, message, permissiontable);

                    break;
                default:
                    custom_shownotification('error', HaveError);

                    break;
            }
        }
        , error: function (x, s, e) {
            custom_shownotification('error', HaveError);
        }
    });
}

function frommail_ShowAdd() {
    $('#btn-show-add-from-email').on('click', function () {
        $('#div-add-from-email').show();
        $(this).hide();
    })
}

function frommail_AddFromEmail(apiDomain, access_token, message, permissiontable) {
    $('#btn-add-from-email').on('click', function () {
        if (frommail_CheckAddFromEmail(message)) {
            frommail_AcctionAddFromEmail(apiDomain, access_token, message, permissiontable);
        }
    });
}

function frommail_CheckAddFromEmail(message) {
    if (!$('#fromemail-name').val()) {
        custom_shownotification('error', message.FromNameRequire);
        return false;
    }
    if (!$('#fromemail-email').val()) {
        custom_shownotification('error', message.FromEmailRequire);
        return false;
    }
    if (!IsEmail($('#fromemail-email').val())) {
        custom_shownotification('error', message.EmailErrorFormat);
        return false;
    }
    else return true;
}

function frommail_AcctionAddFromEmail(apiDomain, access_token, message, permissiontable) {
    var data = {
        FromMail: $('#fromemail-email').val(),
        FromName: $('#fromemail-name').val(),
    }
    $.ajax({
        url: apiDomain + 'api/User/CreateFromMail',
        type: 'post',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (r.ErrorCode) {
                case 0:
                    custom_shownotification('success', message.CreateSuccess);
                    frommail_GetFromEmail(apiDomain, access_token, message, permissiontable);
                    $('#fromemail-email').val('');
                    $('#fromemail-name').val('');
                    break;
                case 102:
                    custom_shownotification('error', message.FromEmailExist);
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
        }
    })
}

function frommail_DefaultFromEmail(apiDomain, access_token, message, permissiontable) {
    $('#table-config-from-email').on('click', '#default', function () {
        var ItemId = $(this).data('fromid');
        frommail_SetDefaultFromEmail(apiDomain, access_token, ItemId, message, permissiontable);
    });
}


function frommail_ResendFromEmail(apiDomain, access_token, message) {
    $('#table-config-from-email').on('click', '#resend', function () {
        var ItemId = $(this).data('fromid');
        var FromMail=$(this).data('frommail');
        var data = {
            ItemId: ItemId,
            FromMail: FromMail,
        }
        
        if ($.cookie('numrefmail') != null && $.cookie('numrefmail') != undefined && $.cookie('numrefmail') != '') {
            var numresend = JSON.parse($.cookie('numrefmail'));
            console.log(numresend);
            if (numresend.ItemId == ItemId && parseInt(numresend.Num) == 5)
            {
                custom_shownotification('warning', message.ResentOverTime);
            }
            else
            {
                frommail_Resend(apiDomain, access_token, data, message);
                //gửi email 
            }
        }
        else {
            var numrefmail = {
                ItemId: ItemId,
                Num:0,
            }
            $.cookie('numrefmail', JSON.stringify(numrefmail), { expires: 1 });
            frommail_Resend(apiDomain, access_token, data, message);
        }
    });
}

function frommail_Resend(apiDomain,access_token,data,message)
{
    $.ajax({
        url: apiDomain + 'api/User/ResendVerifyFromMail',
        data:JSON.stringify(data),
        type: 'post',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);

        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    custom_shownotification('success', message.ResentEmailVerify);
                    var numresend = JSON.parse($.cookie('numrefmail'));
                    numresend.Num = parseInt(numresend.Num) + 1;
                    $.cookie('numrefmail', JSON.stringify(numresend), { expires: 1 });
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        }, error: function () {
            custom_shownotification('error', message.HaveError);
        }
    })
}

function frommail_SetDefaultFromEmail(apiDomain, access_token, ItemId, message, permissiontable) {
    $.ajax({
        url: apiDomain + '/api/User/UpdateFromMailDefault?ItemId=' + ItemId,
        type: 'post',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);

        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    custom_shownotification('success', message.UpdateSuccess);
                    frommail_GetFromEmail(apiDomain, access_token, message, permissiontable);
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        }, error: function () {
            custom_shownotification('error', message.HaveError);
        }
    })
}




