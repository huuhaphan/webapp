﻿function money_tablesToExcel(table, name, filename) {
    var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets>'
    , templateend = '</x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head>'
    , body = '<body>'
    , tablevar = '<table>{table'
    , tablevarend = '}</table>'
    , bodyend = '</body></html>'
    , worksheet = '<x:ExcelWorksheet><x:Name>'
    , worksheetend = '</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet>'
    , worksheetvar = '{worksheet'
    , worksheetvarend = '}'
    , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    , wstemplate = ''
    , tabletemplate = '';

    var tables = '';
    var ctx = {};
    var exceltable;

    tables = table;

    for (var i = 0; i < tables.length; ++i) {
        wstemplate += worksheet + worksheetvar + i + worksheetvarend + worksheetend;
        tabletemplate += tablevar + i + tablevarend;
    }

    var allTemplate = template + wstemplate + templateend;
    var allWorksheet = body + tabletemplate + bodyend;
    var allOfIt = allTemplate + allWorksheet;


    for (var j = 0; j < tables.length; ++j) {
        ctx['worksheet' + j] = name[j];
    }

    if (!tables[0].nodeType) exceltable = document.getElementById(tables[0]);
    ctx['table' + 0] = exceltable.innerHTML;

    window.location.href = uri + base64(format(allOfIt, ctx));

};

var Payment_Menthod; //phương thức nạp tiền
var Payment_Amount; //số tiền nạp
var Payment_Note; //ghi chú nạp
var Payment_ForPlanId = ""; //nạp tiền nên biến này để trống
var Payment_Price; //số tiền nạp
//var Payment_CancalUrl = 'http://app.toomarketer.com/Account/UserProfile#Recharge';
//var Payment_SuccessUrl = 'http://app.toomarketer.com/Recharge';
//var Payment_SuccessUrlPayPal = 'http://app.toomarketer.com/RechargePaypal';

function money_Action(apiDomain, access_token, UserInfo, message, MoneyFirst, BonusFirst, MoneyLast)
{


    money_LoadUserInfo(apiDomain, access_token, MoneyFirst, BonusFirst, MoneyLast,UserInfo);
    LoadSysPrice(apiDomain, access_token);

    money_ViaATM(apiDomain, access_token, UserInfo, message);//nap qua ngân hàng
    money_ViaVisa(apiDomain, access_token, UserInfo, message);//nạp qua visa
    money_ViaSendMoney(message);//nạp qua chuyển khoản
    money_ViaPaypal(message);//qua paypal
    //chọn ngân hàng và thực hiện nạp tiền 
    money_SelectBankPayMent(apiDomain, access_token, UserInfo, message);

    //money_HistoryPayment(apiDomain, access_token, message);
    //money_HistoryBlance(apiDomain, access_token, message);

    money_FormatCurency();

    ChangeDateReport(apiDomain, access_token, message);
    money_ExportExcel();

    money_ActionCode(apiDomain, access_token, message);
    checkCodeInput();
}

function money_ExportExcel()
{
    $('#money-export-excel').on('click', function () {
        var table = ['total-export-excel'];
        var name = ['Sheet1'];
        var filename = 'info-billing';
        money_tablesToExcel(table, name, filename);
    });
}

function money_LoadUserInfo(apiDomain, access_token, MoneyFirst, BonusFirst, MoneyLast,UserInfo)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/User/Info?IsDirty=true',
        type: 'Get',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (result) {
            $('#divLoading').hide();
            switch(parseInt(result.ErrorCode))
            {
                case 0:
                    var user =result.Data;
                    $('#user-balance').text(formatMoney(parseInt(user.CurrentBalance), ''));
                    $('#money-current-balance').text(' ' + formatMoney(parseInt(user.CurrentBalance), ''));
                    $('#process-userbalance').text(' ' + formatMoney(parseInt(user.CurrentBalance), 'VNĐ'));
                    $('.note-success .money-first-recharge').text(formatMoney(parseInt(MoneyFirst), 'VNĐ'));
                    $('.note-success .money-first-recharge-2').text(formatMoney(parseInt(MoneyFirst), 'VNĐ'));
                    $('.note-success .money-last-recharge').text(formatMoney(parseInt(MoneyLast), 'VNĐ'));
                    $('.note-success .money-bonus-first-recharge').text(formatMoney(parseInt(BonusFirst), 'VNĐ'));
                    $('#pricefee').text(formatMoney(parseInt(MoneyFirst), ''));

                    break;
                default:
                    break;
            }
        },
        error: function (x, s, e) {
            //loading 
            $('#divLoading').hide();
            if (x.status == '401') {
                custom_shownotification('error', 'Phiên đăng nhập hết hạn! Vui lòng đăng nhập lại');
                setTimeout(function () { window.location.href = '/login' }, 1000);
            }
            else {
                custom_shownotification('error','Có lỗi xảy ra');
            }
        }

    })
}


function LoadSysPrice(apiDomain,access_token) {
    $.ajax({
        url: apiDomain + 'api/User/GetPriceInfo',
        type: 'Get',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (result) {
            $('#divLoading').hide();
            switch (parseInt(result.ErrorCode)) {
                case 0:
                    var data =result.Data;
                    $('#pricesms').text(formatMoney(parseInt(data.SMS), 'VNĐ/SMS'));
                    $('#pricesmsbrand').text(formatMoney(parseInt(data.SMSBrandName), 'VNĐ/SMS'))
                    $('#priceemail').text(' ' + formatMoney(parseInt(data.Email), 'VNĐ/Email'));
                    break;
                default:
                    break;
            }
        },
        error: function (x, s, e) {
            //loading 
            $('#divLoading').hide();
            if (x.status == '401') {
                custom_shownotification('error', 'Phiên đăng nhập hết hạn! Vui lòng đăng nhập lại');
                setTimeout(function () { window.location.href = '/login' }, 1000);
            }
            else {
                custom_shownotification('error', 'Có lỗi xảy ra');
            }
        }

    })
}


function money_HistoryPayment(apiDomain, access_token, message)
{
    var startdate = $('#select-date-from').val();
    var enddate = $('#select-date-to').val();
    $('#time-export-excel').text('('+startdate + ' - ' + enddate+')');
    $.ajax({
        url: apiDomain + 'api/User/Web/GetPaymentLog?draw=1&start=0&length=0&IsPaid=true&StartDate=' + startdate + '&EndDate=' + enddate,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',

        beforeSend: function (resquest) {
            resquest.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            var dt = r.data;
            var total = 0;
            $('#table-payment-report-total').children().remove();
            if (r.data.length > 0) {
                var html = '<table class="divTable" style="width: 100%;">' +
                    '<thead class="divTableBody">' +
                    '<tr class="divTableRow divHeadTableRow">' +
                    '<th class="divTableCell text-center font-green-jungle"><span><b>'+message.Date+'</b></span> </th>' +
                    '<th class="divTableCell text-center"> <span>'+message.Method+'</span></th>' +
                     '<th class="divTableCell text-center" > <span>'+message.Cost+'</span></th>' +
                    '</tr>'+
                  '</thead>' 

                $.each(dt, function (i, o) {
                    total += o.amount;
                    html += '<tr class="divTableRow">' +
                                '<td class="divTableCell text-left" style="text-align:left;"><span>' + moment(o.createDate).format('DD-MM-YYYY HH:mm') + '</span></td>' +
                                '<td class="divTableCell text-center"><span>' + JSON.parse(JSON.stringify(o.paymentMethod)).paymentMethodName + ' </span></td>' +
                                '<td class="divTableCell text-right" ><span>' + formatMoney(o.amount, '') + '</span> </td>' +
                            '</tr>'

                });
                html += '<tr class="divTableRow">' +
                                '<td class="divTableCell text-right"><span>&nbsp</span></td>' +
                                '<td class="divTableCell text-right" style="text-align:right;"><span><b>' + message.Total + ':</b></span></td>' +
                                '<td class="divTableCell text-right" style="text-align:right;"><span><b>' + formatMoney(total, '') + '</b></span></td>' +
                            '</tr>' +
                      //  '</tbody>' +
                        '</table>';

                $('#table-payment-report-total').append(html);

            }
        }

    });
}

function money_HistoryBlance(apiDomain, access_token,message) {

    var startdate = $('#select-date-from').val();
    var enddate = $('#select-date-to').val();
    $('')
    $.ajax({
        url: apiDomain + 'api/User/Web/GetBalanceLog?draw=1&start=0&length=0&StartDate=' + startdate + '&EndDate=' + enddate,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',

        beforeSend: function (resquest) {
            resquest.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            var dt = r.data;
            var total = 0;
            var totalsms = 0;
            var totaltest = 0;
            var sumtotal = 0;
            if (r.data.length > 0) {

                $('#table-blance-report-total').children().remove();
                $('#table-blance-report-total-sms').children().remove();
                $('#table-blance-report-total-emailtest').children().remove();


                var html = '<table class="divTable" style="width: 100%;">' +
                    '<thread class="divTableBody">' +
                    '<tr class="divTableRow divHeadTableRow">' +
                    '<th class="divTableCell text-center font-green-jungle"><span><b>' + message.Date + '</b></span> </th>' +
                    '<th class="divTableCell text-center"> <span>' + message.Campaign + '</span></th>' +
                     '<th class="divTableCell text-center"> <span>' + message.Cost + '</span></th>' +
                    '</tr>' +
                    '</thread>';

                var htmlsms = '<table class="divTable" style="width: 100%;">' +
                    '<thread class="divTableBody">' +
                    '<tr class="divTableRow divHeadTableRow">' +
                    '<th class="divTableCell text-center font-green-jungle"><span><b>' + message.Date + '</b></span> </th>' +
                    //'<div class="divTableCell text-center"> <span>' + message.Campaign + '</span></div>' +
                     '<th class="divTableCell text-center"> <span>' + message.Cost + '</span></th>' +
                    '</tr>'+
                    '</thread>';
                var htmltest = '<table class="divTable" style="width: 100%;">' +
                    '<thead class="divTableBody">' +
                    '<tr class="divTableRow divHeadTableRow">' +
                    '<th class="divTableCell text-center font-green-jungle"><span><b>' + message.Date + '</b></span> </th>' +
                     '<th class="divTableCell text-center"> <span>' + message.Cost + '</span></th>' +
                    '</tr>' +
                    '</thread>';


                var groups = _.groupBy(r.data, function (value) {
                    return value.campaignId;
                });

                $.each(groups, function (i, o) {
                    var date;
                    var money = 0;
                    var campaign = "";
                    if (i !== null && i !== "null") {
                        $.each(o, function (n, m) {
                            if (n == 0) {

                                date = m.createDate;
                                campaign = m.campaignName;
                            }

                            money += m.amount;



                        });

                        total += Math.abs(money);
                        if (Math.abs(money) > 0) {
                            html += '<tr class="divTableRow">' +
                                        '<td class="divTableCell text-left" style="text-align:left;"><span>' + moment(date).format('DD-MM-YYYY hh:mm') + '</span></td>' +
                                        '<td class="divTableCell text-center"><span>' + campaign + ' </span></td>' +
                                        '<td class="divTableCell text-right"><span>' + formatMoney(Math.abs(money), '') + '</span> </td>' +
                                    '</tr>'
                        }
                    }
                    else
                    {
                        var groupsms = _.groupBy(o, function (value) {
                            return moment(new Date( value.createDate)).format('DD-MM-YYYY');
                        });
                       
                        $.each(groupsms, function (g, k) {
                            var datesms = g;
                            var datetest = g;
                            var moneysms = 0;
                            var moneytest = 0;

                            $.each(k, function (y, z) {
                                if (z.spendTypeId == 3 ||z.spendTypeId==7) { //gửi sms
                                    moneysms += z.amount;
                                }
                                if (z.spendTypeId == 5||z.spendTypeId==6) { //mail test
                                    moneytest += z.amount;
                                }
                            });

                            totalsms += Math.abs(moneysms);
                            totaltest += Math.abs(moneytest);
                            if (Math.abs(moneysms) > 0) {
                                htmlsms += '<tr class="divTableRow">' +
                                            '<td class="divTableCell text-left" style="text-align:left;"><span>' + datesms + '</span></td>' +
                                            //'<div class="divTableCell text-center"><span> Chiến dịch gửi sms</span></div>' +
                                            '<td class="divTableCell text-right"><span>' + formatMoney(Math.abs(moneysms), '') + '</span> </td>' +
                                        '</tr>';
                            }
                            if (Math.abs(moneytest) > 0) {
                              
                                htmltest += '<tr class="divTableRow">' +
                                            '<td class="divTableCell text-left" style="text-align:left;"><span>' + datetest + '</span></td>' +
                                            //'<div class="divTableCell text-center"><span> Chiến dịch gửi sms</span></div>' +
                                            '<td class="divTableCell text-right"><span>' + formatMoney(Math.abs(moneytest), '') + '</span> </td>' +
                                        '</tr>';
                            }

                        })


                    }

                    });
                


                html += '<tr class="divTableRow">' +
                                '<td class="divTableCell text-right"><span>&nbsp</span></td>' +
                                '<td class="divTableCell text-right" style="text-align:right;"><span><b>' + message.Total + ':</b></span></td>' +
                                '<td class="divTableCell text-right" style="text-align:right;"><span><b>' + formatMoney(total, '') + '</b></span></td>' +
                            '</tr>' +
                        '</table>';
                    htmlsms += '<tr class="divTableRow">' +
                                        //'<div class="divTableCell text-right"><span>&nbsp</span></div>' +
                                        '<td class="divTableCell text-right" style="text-align:right;"><span><b>' + message.Total + ':</b></span></td>' +
                                        '<td class="divTableCell text-right" style="text-align:right;"><span><b>' + formatMoney(totalsms, '') + '</b></span></td>' +
                                    '</tr>' +
                                 '</table>';
                    htmltest += '<tr class="divTableRow">' +
                                            //'<div class="divTableCell text-right"><span>&nbsp</span></div>' +
                                            '<td class="divTableCell text-right" style="text-align:right;"><span><b>' + message.Total + ':</b></span></td>' +
                                            '<td class="divTableCell text-right" style="text-align:right;"><span><b>' + formatMoney(totaltest, '') + '</b></span></td>' +
                                        '</tr>' +
                                    '</table>';

                    $('#table-blance-report-total').append(html);
                    $('#table-blance-report-total-sms').append(htmlsms);
                    $('#table-blance-report-total-emailtest').append(htmltest);
            }

            sumtotal = parseInt(total) + parseInt(totalsms) + parseInt(totaltest);
            $('#user-paid').text(formatMoney(sumtotal,' '));
                
                
        }
        

    });
}


function money_GetReport(apiDomain, access_token, message)
{
    var getdate = custom_local2utc($('#select-date-from').val(), $('#select-date-to').val());
    var timezone = custom_GetTimeZone();
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/User/GetAccountMoneyReport',
        type: 'POST',
        contentType: 'application/json; charset=utf-8', //'application/x-www-form-urlencoded',
        data: JSON.stringify(getdate),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
            request.setRequestHeader("timezone", timezone);
        },
        success:function(r)
        {
            $('#divLoading').hide();
            switch(parseInt(r.ErrorCode))
            {
                case 0:
                    console.log(r.Data);
                    var data = r.Data;
                    $('#time-export-excel').val($('#select-date-from').val() + ' / ' + $('#select-date-to').val())
                    $('#user-balance-total').text(formatMoney(data.EndBalance, 'VNĐ'));
                    //if (Math.abs(parseInt(data.TotalSpendAmount)) == Math.abs(parseInt(data.EndBalance))) {
                    //    $('#user-paid-total').text('0 VNĐ');
                    //}
                    //else {
                        $('#user-paid-total').text(formatMoney(Math.abs(data.TotalSpendAmount), 'VNĐ'));
                    //}
                    if (parseInt(data.TotalRechargeAmount) < 0) {
                        $('#user-timecharge-total').text('N/A VNĐ');
                    }
                    else {
                        $('#user-timecharge-total').text(formatMoney(data.TotalRechargeAmount, 'VNĐ'));
                    }
                    if (parseInt(data.TotalMailAmount) < 0) {
                        $('#user-paid-campaign-total').text('N/A VNĐ');
                    }
                    else {
                        $('#user-paid-campaign-total').text(formatMoney(data.TotalMailAmount, 'VNĐ'));
                    }

                    if (parseInt(data.TotalMailContact) < 0) {
                        $('#user-contact-campaign-total').text('N/A Email');
                    }
                    else {
                        $('#user-contact-campaign-total').text(formatMoney(data.TotalMailContact, 'Email'));
                    }


                    if (parseInt(data.TotalSMSAmount) < 0) {
                        $('#user-paid-sms-total').text('N/A VNĐ');
                    }
                    else {
                        $('#user-paid-sms-total').text(formatMoney(data.TotalSMSAmount, 'VNĐ'));
                    }
                    if (parseInt(data.TotalSMSContact) < 0) {
                        $('#user-contact-sms-total').text('N/A SMS')
                    }
                    else {
                        $('#user-contact-sms-total').text(formatMoney(data.TotalSMSContact, 'SMS'));
                    }
                    if (parseInt(data.TotalSMSNormalAmount) < 0) {
                        $('#user-paid-smsnomal-total').text('N/A VNĐ');
                    }
                    else
                    {
                        $('#user-paid-smsnomal-total').text(formatMoney(data.TotalSMSNormalAmount, 'VNĐ'));
                    }
                    if (parseInt(data.TotalSMSNormalContact) < 0) {
                        $('#user-contact-smsnomal-total').text('N/A SMS');
                    }
                    else {
                        $('#user-contact-smsnomal-total').text(formatMoney(data.TotalSMSNormalContact, 'SMS'));
                    }
                    if (parseInt(data.TotalSMSBrandAmount) < 0) {
                        $('#user-paid-brandname-total').text('N/A VNĐ');
                    }
                    else{
                        $('#user-paid-brandname-total').text(formatMoney(data.TotalSMSBrandAmount, 'VNĐ'));
                    }
                    if(parseInt(data.TotalSMSBrandContact)<0)
                    {
                        $('#user-contact-brandname-total').text('N/A SMS');
                    }
                    else {
                        $('#user-contact-brandname-total').text(formatMoney(data.TotalSMSBrandContact, 'SMS'));
                    }

                    //random auto
                    if (parseInt(data.TotalAutoSMSContact) < 0) {
                        $('#user-contact-random-auto-total').text('N/A SMS');
                    }
                    else if (data.TotalAutoSMSContact == null) {
                        $('#user-contact-random-auto-total').text('0 SMS');
                    }
                    else {
                        $('#user-contact-random-auto-total').text(formatMoney(data.TotalAutoSMSContact, 'SMS'));
                    }
                    if (parseInt(data.TotalAutoSMSAmount) < 0) {
                        $('#user-paid-ranđom-auto-total').text('N/A VNĐ');
                    }
                    else if (data.TotalAutoSMSAmount == null) {
                        $('#user-paid-ranđom-auto-total').text('0 VNĐ');
                    }
                    else {
                        $('#user-paid-ranđom-auto-total').text(formatMoney(data.TotalAutoSMSAmount, 'VNĐ'));
                    }

                    //brandname auto
                    if (parseInt(data.TotalAutoSMSBrandContact) < 0) {
                        $('#user-contact-brandname-auto-total').text('N/A SMS');
                    }
                    else if (data.TotalAutoSMSBrandContact == null) {
                        $('#user-contact-brandname-auto-total').text('0 SMS');
                    }
                    else {
                        $('#user-contact-brandname-auto-total').text(formatMoney(data.TotalAutoSMSBrandContact, 'SMS'));
                    }
                    if (parseInt(data.TotalAutoSMSBrandAmount) < 0) {
                        $('#user-paid-brandname-auto-total').text('N/A VNĐ');
                    }
                    else if (data.TotalAutoSMSBrandAmount == null) {
                        $('#user-paid-brandname-auto-total').text('0 VNĐ');
                    }
                    else {
                        $('#user-paid-brandname-auto-total').text(formatMoney(data.TotalAutoSMSBrandAmount, 'VNĐ'));
                    }

                    //email auto 
                    if (parseInt(data.TotalAutoMailContact) < 0) {
                        $('#user-contact-email-auto-total').text('N/A Email');
                    }
                    else if (data.TotalAutoMailContact == null) {
                        $('#user-contact-email-auto-total').text('0 Email');
                    }
                    else {
                        $('#user-contact-email-auto-total').text(formatMoney(data.TotalAutoMailContact, 'Email'));
                    }

                    if (parseInt(data.TotalAutoMailAmount) < 0) {
                        $('#user-paid-email-auto-total').text('N/A VNĐ');
                    }
                    else if (data.TotalAutoMailAmount == null) {
                        $('#user-paid-email-auto-total').text('0 VNĐ');
                    }
                    else {
                        $('#user-paid-email-auto-total').text(formatMoney(data.TotalAutoMailAmount, 'VNĐ'));
                    }

                    //sms gateway
                    if (parseInt(data.TotalSMSGatewayAmount) < 0) {
                        $('#user-paid-sms-via-person-total').text('N/A VNĐ');
                    }
                    else if (data.TotalSMSGatewayAmount == null) {
                        $('#user-paid-sms-via-person-total').text('0 VNĐ');
                    }
                    else {
                        $('#user-paid-sms-via-person-total').text(formatMoney(data.TotalSMSGatewayAmount, 'VNĐ'));
                    }
                    if (parseInt(data.TotalSMSGatewayContact) < 0) {
                        $('#user-contact-sms-via-person-total').text('N/A SMS');
                    }
                    else if (data.TotalSMSGatewayContact == null) {
                        $('#user-contact-sms-via-person-total').text('0 SMS');
                    }
                    else {
                        $('#user-contact-sms-via-person-total').text(formatMoney(data.TotalSMSGatewayContact, 'SMS'));
                    }

                    if (parseInt(data.TotalAutoSMSGatewayAmount) < 0) {
                        $('#user-paid-sms-via-person-total-auto').text('N/A VNĐ');
                    }
                    else if (data.TotalAutoSMSGatewayAmount == null) {
                        $('#user-paid-sms-via-person-total-auto').text('0 VNĐ');
                    }
                    else {
                        $('#user-paid-sms-via-person-total-auto').text(formatMoney(data.TotalAutoSMSGatewayAmount, 'VNĐ'));
                    }

                    if (parseInt(data.TotalAutoSMSGatewayContact) < 0) {
                        $('#user-contact-sms-via-person-total-auto').text('N/A SMS');
                    }
                    else if (data.TotalAutoSMSGatewayContact == null) {
                        $('#user-contact-sms-via-person-total-auto').text('0 SMS');
                    }
                    else {
                        $('#user-contact-sms-via-person-total-auto').text(formatMoney(data.TotalAutoSMSGatewayContact, 'SMS'));
                    }


                    break;
            }
           
        
        },
        error:function(x,s,e)
        {
            $('#divLoading').hide();
            custom_shownotification('error', 'Có lỗi xảy ra');
        }
    })
}


function ChangeDateReport(apiDomain, access_token, message)
{
    $('#select-date-report').on('click', function () {
        //money_HistoryPayment(apiDomain, access_token, message);
        //money_HistoryBlance(apiDomain, access_token, message);
        money_GetReport(apiDomain, access_token, message);
    });
}

//unique theo campagnid
Array.prototype.uniqueobj = function uniqueobj() {
    var flags = [], output = [], l = this.length, i;
    for (i = 0; i < l; i++) {
        if (flags[this[i].campaignId]) continue;
        flags[this[i].ContactId] = true;
        output.push(this[i]);
    }
   
    return output;
}



function money_FormatCurency() {
    $('#mask_currency_recharge').keyup(function (event) {

        // skip for arrow keys
        //if (event.which >= 37 && event.which <= 40) return;

        // format number
        $(this).val(function (index, value) {
            return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            ;
        });
    });
}

//mở modal nạp qua atm
function money_ViaATM(apiDomain, access_token, UserInfo, message)
{
    $('#via-atm-money').on('click', function () {
        if (money_CheckInfo(message)) {
            $(this).attr('data-target', '#modal-atm-money');
            $(this).attr('data-toggle', 'modal');
        }

    })
}

//click chọn ngân hàng để nạp qua atm
function money_SelectBankPayMent(apiDomain, access_token, UserInfo,message) {
    $('#money-box-bank').on('click', '#seclect-bank-atm', function () {

        if (money_CheckInfo(message)) {

            Payment_MenthodId = 2; //mặt định
            Payment_Note = "Nạp tiền vào tài khoản";
            Payment_Price = Number($('#mask_currency_recharge').val().replace(/[&\/\VNĐ\_#,+.'"]/g, ''));
            var Object = money_GenCode(apiDomain, access_token, Payment_MenthodId, Payment_Price, Payment_ForPlanId, Payment_Note);
            var errorcode = parseInt(Object.ErrorCode)
            switch (errorcode) {
                case 0:
                    var data = JSON.parse(JSON.stringify(Object.Data));
                    var bank = $(this).children().children('i').attr('class'); //lấy mã ngân hàng
                    var Payment = {
                        Bank: bank,
                        PayCode: data.PayCode,
                        Menthod: 'ATM_ONLINE', //mã mặt định
                        Amount: Payment_Price,
                        //CancelUrl: Payment_CancalUrl,
                        //SuccessUrl: Payment_SuccessUrl,
                        PlanId: Payment_ForPlanId
                    }
                    //hàm xử lí 
                    money_GeneralPayment(Payment, message);

                    break;
                default:
                    custom_shownotification("error", message.GetCodeError);
                    break;
            }
        }
    });

}
    //chọn kiểu nạp visa 
    function money_ViaVisa(apiDomain, access_token, UserInfo,message) {
        $('#via-visa-money').on('click', function () {
            if (money_CheckInfo(message)) {
                Payment_MenthodId = 3;
                Payment_Note = "Nạp tiền vào tài khoản";
                Payment_Price = Number($('#mask_currency_recharge').val().replace(/[&\/\VNĐ\_#,+.'"]/g, ''));
                
                var Object = money_GenCode(apiDomain, access_token, Payment_MenthodId, Payment_Price, Payment_ForPlanId, Payment_Note);
                var errorcode = parseInt(Object.ErrorCode)
                switch (errorcode) {
                    case 0:
                        var data = Object.Data;
                        var Payment = {
                            PayCode: data.PayCode,
                            UserName: UserInfo.Name,
                            Amount: Payment_Price,
                            PlanId: Payment_ForPlanId,
                            Menthod: 'Visa',
                        }
                        //hàm xử lí 
                        money_GeneralPaymentPaypal(Payment, message);

                        break;
                    default:
                        custom_shownotification("error", message.GetCodeError);
                        break;
                }
            }
        })

    }

    //nạp qua chuyển khoản 
    function money_ViaSendMoney(message) {
        $('#via-sendcount').on('click', function () {
            if (money_CheckInfo(message)) {

                //thì modal phuong thuc nạp tiền đóng
                Payment_MenthodId = 1;
                Payment_Note = "Nạp tiền vào tài khoản";
                Payment_Price = $('#mask_currency_recharge').val().replace(/[&\/\VNĐ\_#,+.'"]/g, '');
                
                //tạo pay code
                var Object = money_GenCode(apiDomain, access_token, Payment_MenthodId, Payment_Price, Payment_ForPlanId, Payment_Note);
                var errorcode = parseInt(Object.ErrorCode)
                switch (errorcode) {
                    case 0:
                        $(this).attr('data-target', '#modal-payment-send-money');
                        $(this).attr('data-toggle', 'modal');
                        var data = JSON.parse(JSON.stringify(Object.Data))

                        $('#payment-code').text(data.PayCode);
                        $('#payment-code-2').text(data.PayCode);
                        $('#payment-plan-name').text(PlanName);
                        $('#payment-price').text(Price);
                        $('#payment-price-2').text(Price);
                        $('#payment-create-date').text(moment((data.CreateDate)).local().format("DD-MM-YYYY HH:mm"))
                        break;

                    default: custom_shownotification("error", message.GetCodeError);
                        break;
                }


            }
        })
    }

    // nạp qua paypal
    function money_ViaPaypal(message) {
        $('#via-paypal').on('click', function () {
            if (money_CheckInfo(message)) {
                Payment_MenthodId = 3; //paypal
                Payment_Note = "Nạp tiền vào tài khoản";
                Payment_Price = Number($('#mask_currency_recharge').val().replace(/[&\/\VNĐ\_#,+.'"]/g, ''));
                
                var Object = money_GenCode(apiDomain, access_token, Payment_MenthodId, Payment_Price, Payment_ForPlanId, Payment_Note);
                var errorcode = parseInt(Object.ErrorCode)
                switch (errorcode) {
                    case 0:
                        var data = Object.Data;
                        var Payment = {
                            PayCode: data.PayCode,
                            UserName: UserInfo.Name,
                            Amount: Payment_Price,
                            //CancelUrl: Payment_CancalUrl,
                            //SuccessUrl: Payment_SuccessUrlPayPal,
                            PlanId: Payment_ForPlanId,
                            Menthod: 'Paypal',
                        }
                        //hàm xử lí 
                        money_GeneralPaymentPaypal(Payment, message);

                        break;
                    default:
                        custom_shownotification("error", message.GetCodeError);
                        break;
                }
            }
            
        });
    }

    //kiểm tra thông tin
    function money_CheckInfo(message) {
        var Money = parseInt(Number($('#mask_currency_recharge').val().replace(/[&\/\VNĐ\_#,+.'"]/g, '')));
        if (Number.isInteger(Money / 1000)) {
            if (Money > 100000000) {
                custom_shownotification("error", "Số tiền nhập tối đa là 100.000.000 VNĐ");
                Money.Accept = false;
            } else {
                var Money = $.ajax({
                    url: '/Account/CheckMoneyRecharge?m=' + Money,
                    contentType: 'application/json; charset=utf-8',
                    type: 'GET',
                    async: false,
                }).responseJSON;
                if (Money.Accept == false) {
                    custom_shownotification("error", message.Money100 + ' ' + formatMoney(parseInt(Money.LeastMoney), 'VNĐ'));
                }
            }
           
        } else {
            custom_shownotification("error", "Số tiền nhập vào là bội số của 1000");
            Money.Accept = false;
          
        }
        return Money.Accept;


        //if (parseInt(Number($('#mask_currency_recharge').val().replace(/[&\/\VNĐ\_#,+.'"]/g, ''))) < 100000) {
        //    custom_shownotification("error", message.Money100);
        //    return false;
        //}


        //else {
        //    return true;
        //}
    };

   

    //    //hàm nạp tiền chung
    //    function money_GeneralPayment(data) {
    //        $.ajax({
    //            url: '/Account/UserPayment',
    //            type: 'POST',
    //            data: JSON.stringify(data),
    //            contentType: 'application/json; charset=utf-8',

    //            success: function (r) {
    //                switch (r.Success) {
    //                    case true:
    //                        window.location.href = r.Result;
    //                        break;
    //                    case false:
    //                        alert(r.Result);
    //                }

    //            },
    //            error: function (x, s, e) {
    //                alert('Có lỗi xảy ra , thử lại sau')
    //            },

    //        })

    //    }

    //    //tạo payment code
    //    function money_GenCode(apiDomain, access_token, PaymentMethodId, Amount, ForPlanId, Note) {
    //        var Paycode = $.ajax({
    //            url: apiDomain + 'api/User/CreatePayment?PaymentMethodId=' + PaymentMethodId + '&Amount=' + Amount + '&ForPlanId=' + ForPlanId + '&Note=' + Note + '',
    //            type: 'POST',
    //            contentType: 'application/x-www-form-urlencoded',
    //            beforeSend: function (request) {
    //                request.setRequestHeader("Authorization", "Bearer " + access_token);
    //            },
    //            async: false,
    //            success: function (r) {
    //                var errorcode = parseInt(r.ErrorCode);
    //                switch (errorcode) {
    //                    case 0:
    //                        break;
    //                    default: alert('Xảy ra lỗi tạo mã thanh toán');
    //                        break;
    //                }

    //            },
    //            error: function (x, s, e) {
    //                alert('Xảy ra lỗi tạo mã thanh toán');
    //            }
    //        }).responseJSON;
    //        return Paycode;
    //    }
    //}

    //resize table
    function resizeTablePayment() {
        var height = parseInt($(window).height() - $('.page-header').height() - $('.page-bar').height() - $('.portlet-title').height() - $('#divAction').height() - $('.page-footer').height() - 100)

        return height;
    }

    function resizeTableBlance() {
        var height = parseInt($(window).height() - $('.page-header').height() - $('.page-bar').height() - $('.portlet-title').height() - $('#divAction').height() - $('.page-footer').height() - 100)
        return height;
    };


    function money_GeneralPayment(data, message) {
        $('#divLoading').show();
        $.ajax({
            url: '/Account/UserRecharge',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',

            success: function (r) {
                $('#divLoading').hide();
                switch (r.Success) {
                    case true:
                        window.location.href = r.Result;
                        break;
                    case false:
                        custom_shownotification('error', r.Result);
                        break;
                      
                }

            },
            error: function (x, s, e) {
                custom_shownotification('error', message.Error)
                $('#divLoading').hide();
            },

        })

    }


    function money_GeneralPaymentPaypal(data, message) {
        $('#divLoading').show();
        $.ajax({
            url: '/Account/UserRechargePaypal',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',

            success: function (r) {
                $('#divLoading').hide();
                switch (r.Success) {
                    case true:
                       
                         window.location.href = r.Result;
                        break;
                    case false:
                        custom_shownotification('error', r.Result);
                        break;

                }

            },
            error: function (x, s, e) {
                custom_shownotification('error', message.Error)
                $('#divLoading').hide();
            },

        })

    }

    function money_GenCode(apiDomain, access_token, PaymentMethodId, Amount, ForPlanId, Note) {
        var Paycode = $.ajax({
            url: apiDomain + 'api/User/CreatePayment?PaymentMethodId=' + PaymentMethodId + '&Amount=' + Amount + '&ForPlanId=' + ForPlanId + '&Note=' + Note + '',
            type: 'POST',
            contentType: 'application/x-www-form-urlencoded',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            async: false,
         
        }).responseJSON;
        return Paycode
    }

    

    function money_ActionCode(apiDomain, access_token, message)
    {
        $('#btn-recharge-bycode').click(function () {
            if ($('#code-to-recharge').val()) {
                $.ajax({
                    url: apiDomain + 'api/User/CreatePaymentFromCode?Code=' + $('#code-to-recharge').val(),
                    type: 'Post',
                    contentType: 'application/json; charset=utf-8',
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', 'Bearer ' + access_token)
                    },
                    success: function (r) {
                        switch (parseInt(r.ErrorCode)) {
                            case 0:
                                //money_LoadUserInfo(apiDomain, access_token);
                                custom_shownotification('success', 'Nạp tiền thành công');
                                $.when(
                                    money_AddToPaid()).then(function () {
                                    setTimeout(function () {
                                        $('#btnrecharge').trigger('click');

                                    }, 500)
                                });
                                
                                $('#code-to-recharge').val('');
                                break;
                            case 101:
                                $('#code-to-recharge').val('');
                                setRetryCookie();
                                break;
                            default:
                                $('#code-to-recharge').val('');
                                custom_shownotification('error', 'Có lỗi xảy ra');
                             //   setRetryCookie();
                                break;
                        }
                    },
                    error: function () {
                        $('#code-to-recharge').val('');
                        custom_shownotification('error', 'Có lỗi xảy ra');
                       // setRetryCookie();
                    }
                })


            }
            else {
                custom_shownotification('error', 'Chưa nhập mã nạp tiền');
            }
        });
    }


var tm = 5;
function checkCodeInput() {
    if ($.cookie('tuc') != null && $.cookie('tuc') != undefined) {
        var t = $.cookie('tuc');
        t = JSON.parse(t);
        if (t.time >= tm) {
            $('#CodeHint').html('Bạn đã thử quá ' + tm + ' lần! Vui lòng đợi <span id="lockTimer"></span> để nhập lại!');
            //setTimerToUnLock(t.expire);
            var expire = t.expire;
            $('#lockTimer').html(moment(moment(expire).diff(moment())).format('mm:ss'));
            $('#code-to-recharge').prop('disabled', true);
        }
    }
    else {
        $('#code-to-recharge').prop('disabled', false);
        $('#CodeHint').html('');
    }
}

setInterval(function () {
                checkCodeInput();
}, 1000);

function setRetryCookie() {
    var date = new Date();
    var minutes = 5;
    date.setTime(date.getTime() + (minutes * 60 * 1000));

    var t = $.cookie('tuc');
    if (t == undefined) {
        t = { time: 1, expire: date };
    }
    else {
        t = JSON.parse(t);
        t.time += 1;
        t.expire = date;
    }

    $.cookie('tuc', JSON.stringify(t), { expires: date });

    $('#code-to-recharge').val('');
    if (t.time >= 5) {
        checkCodeInput();
        custom_shownotification('error', 'Nhập mã quá 5 lần');
    }
    else {

        custom_shownotification('warning', 'Mã không tồn tại hoặc đã được sử dụng! Bạn còn ' + (tm - t.time) + ' lần thử!');
    }
};

function money_AddToPaid() {
    $.ajax({
        url: '/Recharge/UpdateRechargeByCode',
        type: 'GET',
        async:true,
        contentType: 'application/json; charset=utf-8',
        success: function () {

        }, error: function () {

        }
    })

}

      
           

           
        

