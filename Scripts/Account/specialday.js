﻿function specialday_Proccess(apiDomain, access_token, userId, message, permissiontable) {
    //get list from email
    specialday_GetSpecialDay(apiDomain, access_token, message, permissiontable);

    //show modal confirm delete
    specialday_ModelDeleteSpecialDay();

    //delete 1 from email
    specialday_DeleteSpecialDay(apiDomain, access_token, message, permissiontable);

    //show from add from email
    specialday_ShowAdd();
    specialday_ConfirmAdd(apiDomain, access_token, message, permissiontable);

   

    //set default 1 from email
    specialday_UpdateSpecialDay(apiDomain, access_token, message, permissiontable);

    //specialday_ResendFromEmail(apiDomain, access_token, message);
}



function specialday_GetSpecialDay(apiDomain, access_token, message, permissiontable) {
    var data = {
       // Active: true,
    }
    $.ajax({
        url: apiDomain + 'api/AutoCampaign/GetSpecialDays',
        type: 'Post',
        data:JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    specialday_TableSpecialDay(data, message, permissiontable);
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
        }
    })

}

function specialday_TableSpecialDay(data, message, permissiontable) {
    $('#table-specialday').children().remove();
    var html = "";
    if (data.length > 0) {
        $.each(data, function (i, o) {
            var tool = '';
            status = '<span style="font-size:13px;">' + message.Verified + ' </span>';
            if (permissiontable.IsParent) {
                console.log(data.IsPredefine);
                if (!o.IsPredefine) {
                    tool = '<a id="update" data-fromid="' + o.ItemId + '" data-fromname="' + o.ItemName + '" data-fromdate="' + o.ItemDate + '" class="btn btn-xs default green-stripe">' + message.Update + ' </a>' +
                      '<a data-fromid="' + o.ItemId + '"  class="btn btn-xs default red-stripe" id="delete"> ' + message.Delete + '</a>';
                }
            } else {
                if (parseInt(permissiontable.SpecialDay) == 2) {
                    if (!o.IsPredefine) {
                        tool = '<a id="update" data-fromid="' + o.ItemId + '"  class="btn btn-xs default green-stripe">' + message.Update + ' </a>' +
                      '<a data-fromid="' + o.ItemId + '" data-fromname="' + o.ItemName + '" data-fromdate="' + o.ItemDate + '" class="btn btn-xs default red-stripe" id="delete"> ' + message.Delete + '</a>';

                    }
                }
            }




            html +=
                   '<tr>' +
                   '<td style="width:40%;"><p style="font-size:14px;margin-bottom:0px;"> ' + o.ItemName + '</p></td>' +
                   '<td style="width:30%;">' + moment(new Date(o.ItemDate)).format('DD-MM') + '</td>' +
                   '<td style="width:30%;"class="text-right"> ' +
                    tool +
                      '</td>'
            '</tr>';

        });
    } else {
        html += '<tr>' +
                   '<td colspan="3" class="text-center"><p style="font-size:14px;">' + message.NoSpecialDay + ' </span></td>' +

         '</tr>';
    }

    var height = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.portlet-title').outerHeight() - $('.page-footer').outerHeight() - 200;
    $('.inner_table').css({
        'height': height,
    })
    $('#table-specialday').append(html);
};

function specialday_ModelDeleteSpecialDay() {
    $('#table-specialday').on('click', '#delete', function () {
        var ItemId = $(this).data('fromid');
        $('#confirm-delete-specialday').data('fromid', ItemId);
        $('#modal-delete-specialday').modal('toggle');
    })

}

function specialday_DeleteSpecialDay(apiDomain, access_token, message, permissiontable) {
    $('#confirm-delete-specialday').on('click', function () {
        var ItemId = $(this).data('fromid');
        specialday_ActionDeleteSpecialDay(apiDomain, access_token, ItemId, message, permissiontable);
    })
}

function specialday_ActionDeleteSpecialDay(apiDomain, access_token, ItemId, message, permissiontable) {
   var  data = {
       ItemId: ItemId,
       ForceDelete:false,
   }
   $('#modal-delete-specialday').modal('toggle');
    $.ajax({
        url: apiDomain + 'api/AutoCampaign/DeleteSpecialDay',
        type: 'Post',
        data:JSON.stringify(data),
        contentType: 'application/json; chartset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    custom_shownotification('success', message.DeleteSuccess);
                    specialday_GetSpecialDay(apiDomain, access_token, message, permissiontable);
      
                    break;
                case 102:
                    custom_shownotification('error', message.UsingInEvent);
                    break;
                default:
                    custom_shownotification('error', HaveError);
                    break;
            }
        }
        , error: function (x, s, e) {
            custom_shownotification('error', HaveError);
        }
    });
}

function specialday_ShowAdd() {
    $('#btn-show-add-from-email').on('click', function () {
        $('#modal-add-special').modal('toggle');
    })
}


function specialday_CheckSpecialDay(message) {
    if (!$('#txtSpecialDayName').val()) {
        custom_shownotification('error', message.SepcialNameRequire);
        return false;
    }
    if (!$('#txtSpecialDay').val()) {
        custom_shownotification('error', message.SepcialDayRequire);
        return false;
    }
   
    else return true;
}

function specialday_AcctionAddFromEmail(apiDomain, access_token, message, permissiontable) {
    var data = {
        FromMail: $('#fromemail-email').val(),
        FromName: $('#fromemail-name').val(),
    }
    $.ajax({
        url: apiDomain + 'api/User/CreateFromMail',
        type: 'post',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (r.ErrorCode) {
                case 0:
                    custom_shownotification('success', message.CreateSuccess);
                    specialday_GetSpecialDay(apiDomain, access_token, message, permissiontable);
                    $('#fromemail-email').val('');
                    $('#fromemail-name').val('');
                    break;
                case 102:
                    custom_shownotification('error', message.FromEmailExist);
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
        }
    })
}

function specialday_UpdateSpecialDay(apiDomain, access_token, message, permissiontable) {
    $('#table-specialday').on('click', '#update', function () {
        var ItemId = $(this).data('fromid');
        var name = $(this).data('fromname');
        $('#txtSpecialDayName').val(name);
        var date = $(this).data('fromdate');
        console.log(date);
        $('#txtSpecialDay').datepicker("setDate", new Date(date));
        $('#modal-add-special').modal('toggle');
        $('#confirm-add-specialday').attr('fromid', ItemId);
        //specialday_SetUpdateSpecialDay(apiDomain, access_token, ItemId, message, permissiontable);
    });
}


//function specialday_ResendFromEmail(apiDomain, access_token, message) {
//    $('#table-specialday').on('click', '#resend', function () {
//        var ItemId = $(this).data('fromid');
//        var FromMail = $(this).data('frommail');
//        var data = {
//            ItemId: ItemId,
//            FromMail: FromMail,
//        }

//        if ($.cookie('numrefmail') != null && $.cookie('numrefmail') != undefined && $.cookie('numrefmail') != '') {
//            var numresend = JSON.parse($.cookie('numrefmail'));
//            console.log(numresend);
//            if (numresend.ItemId == ItemId && parseInt(numresend.Num) == 5) {
//                custom_shownotification('warning', message.ResentOverTime);
//            }
//            else {
//                specialday_Resend(apiDomain, access_token, data, message);
//                //gửi email 
//            }
//        }
//        else {
//            var numrefmail = {
//                ItemId: ItemId,
//                Num: 0,
//            }
//            $.cookie('numrefmail', JSON.stringify(numrefmail), { expires: 1 });
//            specialday_Resend(apiDomain, access_token, data, message);
//        }
//    });
//}

//function specialday_Resend(apiDomain, access_token, data, message) {
//    $.ajax({
//        url: apiDomain + 'api/User/ResendVerifyFromMail',
//        data: JSON.stringify(data),
//        type: 'post',
//        contentType: 'application/json; charset=utf-8',
//        beforeSend: function (request) {
//            request.setRequestHeader('Authorization', 'Bearer ' + access_token);

//        },
//        success: function (r) {
//            switch (parseInt(r.ErrorCode)) {
//                case 0:
//                    custom_shownotification('success', message.ResentEmailVerify);
//                    var numresend = JSON.parse($.cookie('numrefmail'));
//                    numresend.Num = parseInt(numresend.Num) + 1;
//                    $.cookie('numrefmail', JSON.stringify(numresend), { expires: 1 });
//                    break;
//                default:
//                    custom_shownotification('error', message.HaveError);
//                    break;
//            }
//        }, error: function () {
//            custom_shownotification('error', message.HaveError);
//        }
//    })
//}



function specialday_ConfirmAdd(apiDomain, access_token, message, permissiontable)
{
    $('#confirm-add-specialday').on('click', function () {

        var itemid = $(this).attr('fromid');
       
        if (itemid != null && itemid != '') { //cập nhập
            if (specialday_CheckSpecialDay(message)) {

                var data = {
                    ItemId:itemid,
                    ItemName: $('#txtSpecialDayName').val(),
                    ItemDate: moment($('#txtSpecialDay').datepicker('getDate')).format('YYYY-MM-DD'),
                    Active: true,
                };
                $('#divLoading').show();
                $('#modal-add-special').modal('toggle');
                $.ajax({
                    url: apiDomain + 'api/AutoCampaign/UpdateSpecialDay',
                    type: 'post',
                    data: JSON.stringify(data),
                    contentType:'application/json; charset=utf-8',
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', 'Bearer ' + access_token);
                    },
                    success: function (r) {

                        switch (parseInt(r.ErrorCode)) {
                            case 0:
                                specialday_GetSpecialDay(apiDomain, access_token, message, permissiontable);
                                custom_shownotification('success', message.UpdateSuccess);
                                break;
                            default:
                                custom_shownotification('error', message.HaveError);
                                break;
                        }
                        $('#divLoading').hide();
                    },
                    error: function (x, s, e) {
                        custom_shownotification('error', message.HaveError);
                        $('#divLoading').hide();
                    }

                })
            }

        }

        else {


            if (specialday_CheckSpecialDay(message)) {
                
                var data = {
                    ItemName: $('#txtSpecialDayName').val(),
                    ItemDate: moment($('#txtSpecialDay').datepicker('getDate')).format('YYYY-MM-DD'),
                    Active: true,
                };
                $('#divLoading').show();
                $('#modal-add-special').modal('toggle');
                $.ajax({
                    url: apiDomain + 'api/AutoCampaign/CreateSpecialDay',
                    type: 'post',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', 'Bearer ' + access_token);
                    },
                    success: function (r) {

                        switch (parseInt(r.ErrorCode)) {
                            case 0:
                                specialday_GetSpecialDay(apiDomain, access_token, message, permissiontable);
                                custom_shownotification('success', message.UpdateSuccess);
                                break;
                            default:
                                custom_shownotification('error', message.HaveError);
                                break;
                        }
                        $('#divLoading').hide();
                    },
                    error: function (x, s, e) {
                        custom_shownotification('error', message.HaveError);
                        $('#divLoading').hide();
                    }

                })
            }
        }
    })
}




