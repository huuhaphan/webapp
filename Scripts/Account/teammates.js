﻿function teammates_Action(apiDomain, access_token, message, permissiontable)
{
   
    //chọn theo nhóm
    teammates_ChoicePermisstion(apiDomain, access_token, message);

    //chọn theo preset
    teamates_ClickListBtnPermission(apiDomain, access_token, message);

    teamates_ValueRadioPermission();

    teammates_ChangeRadioPermission();
    //load danh sách phân quyền
    teammates_GetFunctionList(apiDomain, access_token, message);

    teammates_ModalCreate(message);

    //load danh sách subuser
    teammates_GetSubUserList(apiDomain, access_token, message, permissiontable);

    //add sub user
    teammates_UpdateSubUser(apiDomain, access_token, message, permissiontable);

    //click update subuser
    teammates_Update(apiDomain, access_token, message);

    //click delelte subuser
    teammates_Delete(apiDomain, access_token, message);

    //confirm delete
    temmates_DeleteSubUser(apiDomain, access_token, message, permissiontable);

    //rsent email mời subuser
    teammates_ResentEmail(apiDomain, access_token, message);

}

function teammates_GetSubUserList(apiDomain, access_token, message, permissiontable) {
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/User/GetSubUserList',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch(parseInt(r.ErrorCode))
            {
                case 0:
                    var data = r.Data;
                    teammates_GenTableSubUser(data, message, permissiontable)
                    $('#divLoading').hide();
                    break;
                default:
                    custom_shownotification('error', message.errorReadSubUser);
                    $('#divLoading').hide();
                    break;
            }

        }, error: function (x, s, e) {
            custom_shownotification('error', message.errorReadSubUser);
            $('#divLoading').hide();
        }
    })
}

function teammates_GenTableSubUser(data, message, permissiontable)
{
    $('#table-subuser').children().remove();
    if(data.length>0)
    {
        var html = "";
        $.each(data, function (i, o) {
            var resent = '';
            if (o.FirstName == null) {
                o.FirstName = "";
            } if (o.LastName == null) {
                o.LastName = "";
            } if (o.MobilePhone == null) {
                o.MobilePhone = "";
                resent = '<a id="resent-subuser" data-id="' + o.UserId + '" class="btn btn-xs btn btn-xs yellow-gold-stripe default">' + message.reInvite + '</a>'
            }
            
            var update='<a id="update-subuser" data-id="' + o.UserId + '" class="btn btn-xs btn btn-xs green-jungle-stripe default">' +message.update + '</a>';
            var del='<a data-id="' + o.UserId + '" id="delete-subuser" data-id="' + o.UserId + '" class="btn btn-xs red-stripe default">' + message.deleteSubUser + '</a>';
            var tool = '';
            if (permissiontable.IsParent) {
                tool = resent + update + del;
            } else {
                if (parseInt(permissiontable.SubAccount)==2) {
                    tool = resent + update + del;
                }
            }

            html += '<tr>' +
                '<td style="width:40%">' + o.Email + '</td>'+
                '<td style="width:20%">' + o.FirstName + '</td>' +
                 '<td style="width:30%">' + o.LastName + '</td>' +
                 '<td style="width:30%">' + o.MobilePhone + '</td>' +
                '<td style="width:40%" class="text-right">' + tool + '</td>' +
                '</tr>';

        });
    }
    else {
        html = '<tr ><td colspan="5" class="text-center">' + message.noHaveSubUser + '</td></tr>'
    }
    $('#table-subuser').append(html);
}

function teammates_IntializeSlider(id,values)
{
    $('#'+id).ionRangeSlider({
        type: "single",
        values: values,
        hide_from_to: true,
        hide_min_max: true,
        grid: true,
        grid_snap: true,
        step: 1,
      
    });
    $('.irs-grid-text.js-grid-text-0').text('');
}


function teammates_GetFunctionList(apiDomain, access_token,message)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/User/GetFunctionList',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success:function(r)
        {
          
            switch(parseInt(r.ErrorCode))
            {
                case 0:
                    var data = r.Data;
                    teammates_LoadData(data, message);
                    $('#divLoading').hide();
                    break;
                default:
                    custom_shownotification('error', errorGetPermission);
                    $('#divLoading').hide();
                    break;
            }
           

        },error:function(x,s,e)
        {
            $('#divLoading').hide();
        }
    })
}

function teammates_LoadData(data, message) {
    //$('#group-custom-permission').children().remove();
    //parent
    $.each(data, function (i, o) {
        if (o.ParentId == null || o.ParentId == 'null') {
            switch (parseInt(o.MaxAccessLevel)) {
                case 1:
                    var html = '<div class="col-md-12 box-detail-permission">' +
                                       '<div class="col-md-6 col-sm-6 title-teammates-permission">' + o.FunctionName + '</div>' +
                                       '<div class="col-md-6 col-sm-6 nopading" style="width:146px;"><input class="input-data-permission" data-parent="'+o.ParentId+'" data-id="' + o.FunctionId + '" id="range_' + i + '" type="text" /> </div>' +
                                   '</div>';
                    
                    $('#group-custom-permission').append(html);
                    var values = ["0", "1"];
                    var id = 'range_' + i;
                    teammates_IntializeSlider(id, values);
                    teammates_LoadDataChildrent(o.FunctionId, data);
                    break;
                    //case 5:
                    //    var html = '<div class="col-md-12 box-detail-permission">' +
                    //                       '<div class="col-md-6 col-sm-6 title-teammates-permission">' + o.Name + '</div>' +
                    //                       '<div class="col-md-6 col-sm-6 nopading" style="width:280px;"><input id="range_' + i + '" type="text" /> </div>' +
                    //                   '</div>';
                    //    $('#group-custom-permission').append(html);
                    //    var values = ["1", "3"];
                    //    var id = 'range_' + i;
                    //    teammates_IntializeSlider(id,values);

                    //    break;
                case 2:
                    var html = '<div class="col-md-12 box-detail-permission">' +
                                       '<div class="col-md-6 col-sm-6 title-teammates-permission">' + o.FunctionName + '</div>' +
                                       '<div class="col-md-6 col-sm-6 nopading" style="width:280px;"><input class="input-data-permission" data-parent="'+o.ParentId+'" data-id="' + o.FunctionId + '" id="range_' + i + '" type="text" /> </div>' +
                                   '</div>';
                    $('#group-custom-permission').append(html);
                    var values = ["0", "1", "2"];
                    var id = 'range_' + i;
                    teammates_IntializeSlider(id, values);
                    teammates_LoadDataChildrent(o.FunctionId, data);
                    break;

            }
        }
    });
}

function teammates_LoadDataChildrent(ParentId, data)
{
    $.each(data, function (i, o) {
        if (o.ParentId != null && o.ParentId != 'null' && o.ParentId == ParentId) {
            switch (parseInt(o.MaxAccessLevel)) {
                case 1:
                    var html = '<div class="col-md-12 box-detail-permission">' +
                                       '<div class="col-md-offset-1 col-md-5 col-sm-5 title-teammates-permission">' + o.FunctionName + '</div>' +
                                       '<div class="col-md-6 col-sm-6 nopading" style="width:146px;"><input class="input-data-permission" data-parent="'+o.ParentId+'" data-id="' + o.FunctionId + '" id="range_'+ParentId + i + '" type="text" /> </div>' +
                                   '</div>';

                    $('#group-custom-permission').append(html);
                    var values = ["0", "1"];
                    var id = 'range_'+ParentId + i;
                    teammates_IntializeSlider(id, values);

                    break;
                    //case 5:
                    //    var html = '<div class="col-md-12 box-detail-permission">' +
                    //                       '<div class="col-md-6 col-sm-6 title-teammates-permission">' + o.Name + '</div>' +
                    //                       '<div class="col-md-6 col-sm-6 nopading" style="width:280px;"><input id="range_' + i + '" type="text" /> </div>' +
                    //                   '</div>';
                    //    $('#group-custom-permission').append(html);
                    //    var values = ["1", "3"];
                    //    var id = 'range_' + i;
                    //    teammates_IntializeSlider(id,values);

                    //    break;
                case 2:
                    var html = '<div class="col-md-12 box-detail-permission">' +
                                       '<div class="col-md-offset-1 col-md-5 col-sm-5 title-teammates-permission">' + o.FunctionName + '</div>' +
                                       '<div class="col-md-6 col-sm-6 nopading" style="width:280px;"><input class="input-data-permission" data-parent="' + o.ParentId + '" data-id="' + o.FunctionId + '" id="range_' + ParentId + i + '" type="text" /> </div>' +
                                   '</div>';
                    $('#group-custom-permission').append(html);
                    var values = ["0", "1", "2"];
                    var id = 'range_' + ParentId + i;
                    teammates_IntializeSlider(id, values);
                    break;

            }
        }
    });
}


function teammates_ChoicePermisstion(apiDomain, access_token, message)
{
    var choices = $('.choice-permission');

    choices.on('click', function (event) {
        var choice = $(event.target);
        choice
            .find('[name="permission"]')
            .prop('checked', true)
            .trigger('change');
    });

    var inputs = $('.choice-permission input');
    inputs.on('change', function (event) {
        var input = $(event.target);
        var choice = $(this).closest('.choice-permission');

        $('.choice-permission.active').removeClass('active');
        choice.addClass('active');
    });
}

function teammates_ChangeRadioPermission()
{
    

    $('.md-radio-permision input[name=permission]').on('change', function () {
        teamates_ValueRadioPermission();
    });
}

function teamates_ValueRadioPermission()
{
    var value = $('.md-radio-permision input[name=permission]:checked').val();
    if (value == 0) {
        $('#content-set-permisstion').show();

    } else {
        $('#content-set-permisstion').hide();
    }
}

var btn_permission;
var IsParrent=true;
function teamates_ClickListBtnPermission(apiDomain, access_token, message) {

    $('#list-btn-permission a').on('click', function () {

        btn_permission = $(this).data('value');
        $('#list-btn-permission a').removeClass('green-jungle');
        $(this).addClass('green-jungle');
        if (btn_permission != 0) { // !=custom
            teamates_GetPresetById(apiDomain, access_token, btn_permission, message);
        }
    });

    $('#group-custom-permission').on('change', 'input', function () {
        if (!IsGetPresetById) {
            $('#list-btn-permission #custom-access').trigger('click');

            var value = $(this).val();
            var functionid = parseInt($(this).data('id'));
            var parentid = parseInt($(this).data('parent'));
            if ($(this).data('parent') == null || $(this).data('parent') == 'null') {

                var input = $('#group-custom-permission input[data-parent=' + functionid + ']');
                $.each(input, function (i, o) {
                    var slider = $(this).data("ionRangeSlider");
                    slider.update({
                        from: value
                    });
                })
            } else {
                parent = $('#group-custom-permission input[data-id=' + parentid + ']');
                var parentval = parent.val();
                if (parseInt(parentval) < parseInt(value))
                {
                    var slider =$(this).data("ionRangeSlider");
                    slider.update({
                        from: parseInt(parentval)
                    });
                }
            }
        } 
    });
}

var IsGetPresetById = false;
function teamates_GetPresetById(apiDomain, access_token, PresetId, message)
{
    $.ajax({
        url: apiDomain + 'api/User/GetPresetFunctionList?PresetId=' + PresetId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend:function(request){
            request.setRequestHeader('Authorization','Bearer '+access_token);
        },
        success:function(r)
        {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    $('#divLoading').hide();
                    var data = r.Data;
                    IsGetPresetById = true;
                    teammates_SetValueOfIonRanger(data.Functions);
                    IsGetPresetById = false;
                    break;
                default:
                    $('#divLoading').hide();
                    custom_shownotification('error', message.errorGetPreset);
                    break;
            }
        },error:function(x,s,e)
        {
            $('#divLoading').hide();
            custom_shownotification('error', message.errorGetPreset);
        }
    })
}

function teammates_ModalCreate(message)
{
    $('#create-teammates').on('click', function () {
        $('#btn-subuser-update').attr('data-id', '');
        $('#modal-update-teammates-label').text(message.createsubuser);
        $('#btn-subuser-update').text(message.create);
        $('#modal-update-teammates').modal('toggle');
       
        $('#subuser-email').removeAttr('readonly');

        teammates_ResetData();
    })
   
}

function teammates_UpdateSubUser(apiDomain, access_token, message, permissiontable)
{
    $('#btn-subuser-update').on('click', function () {
        if (teammates_checkDataSubUse(message)) {
            var data = teammates_getDataSubUser();
            var userid = $('#btn-subuser-update').attr('data-id');
            if (userid == '' || userid == undefined) {
                teammates_CreateSubUser(apiDomain, access_token, data, message, permissiontable);
            }
            else {
                teammates_UpdateSubUserFunction(apiDomain, access_token, data, message, permissiontable);
            }
        }
    });
}



function teammates_checkDataSubUse(message)
{
    if ($('#subuser-email').val() == '') {
        custom_shownotification('error', message.emailSubUserRequire);
        return false;
    }
    if ($('input[name=permission]:checked').val() == 0 && btn_permission == undefined) {
        custom_shownotification('error', message.presetRequire);
        return false;
    } else {
        return true
    };
}

function teammates_getDataSubUser()
{
    var FunctionList = [];
    if (btn_permission == 0) //custom
    {
        var listf = $('#group-custom-permission .input-data-permission');
        $.each(listf, function (i, o) {
            var obj = {
                FunctionId: $(this).attr('data-id'),
                AccessLevel: $(this).val(),
            };
            FunctionList.push(obj);
        });
    }

    var data = {
        Email: $('#subuser-email').val().trim(),
        RoleId: $('input[name=permission]:checked').val(),
        PresetId: btn_permission,
        FunctionList: FunctionList,
    }
    var userid = $('#btn-subuser-update').attr('data-id');
    if (userid != '' && userid != null && userid != undefined) {
        data.SubUserId = userid;
    }
    console.log(data);
    return data;
}

function teammates_CreateSubUser(apiDomain, access_token, data, message, permissiontable)
{
    $('#divLoading').show();

    $.ajax({
        url: apiDomain + 'api/User/CreateSubUser',
        type: 'POST',
        data:JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch(parseInt(r.ErrorCode))
            {
                case 0:
                    $('#divLoading').hide();
                    custom_shownotification('success', message.createSuccess);
                    //ẩn modal
                    $('#modal-update-teammates').modal('toggle');
                    //load lại table
                    teammates_GetSubUserList(apiDomain, access_token, message, permissiontable)
                    break;
                case 102:
                    custom_shownotification('error', message.emailExist);
                    $('#divLoading').hide();
                    break;
                default:
                    custom_shownotification('error', message.haveError);
                    $('#divLoading').hide();
                    break;
            }
        },error:function(x,s,e)
        {
            custom_shownotification('error', message.haveError);
            $('#divLoading').hide();
        }
    })
}

function teammates_UpdateSubUserFunction(apiDomain, access_token, data, message, permissiontable) {
    $('#divLoading').show();

    $.ajax({
        url: apiDomain + 'api/User/UpdateSubUserFunction',
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    $('#divLoading').hide();
                    custom_shownotification('success', message.updateSuccess);
                    //ẩn modal
                    $('#modal-update-teammates').modal('toggle');
                    //load lại table
                    teammates_GetSubUserList(apiDomain, access_token, message, permissiontable)
                    break;
                default:
                    custom_shownotification('error', message.haveError);
                    $('#divLoading').hide();
                    break;
            }
        }, error: function (x, s, e) {
            custom_shownotification('error', message.haveError);
            $('#divLoading').hide();
        }
    })
}

function teammates_Update(apiDomain,access_token, message)
{
   
    $('#table-subuser').on('click', '#update-subuser', function () {
        var userid = $(this).attr('data-id');
        $('#btn-subuser-update').attr('data-id', userid);
        teammates_ResetData();
        teammates_GetSubUserInfo(apiDomain, access_token, message, userid);
        $('#subuser-email').attr('readonly', true);
        $('#modal-update-teammates-label').text(message.editsubuser)
        $('#btn-subuser-update').text(message.upgrade);
    });
}

function teammates_Delete(apiDomain, access_token, message) {
    $('#table-subuser').on('click', '#delete-subuser', function () {
        var userid = $(this).attr('data-id');
        $('#modelConfirmDeleteSubUser').modal('toggle');
        $('#btnDeleteSubUser').attr('data-id', userid);

    });


   
}

function teammates_GetSubUserInfo(apiDomain, access_token, message, useid)
{
    $.ajax({
        url: apiDomain + 'api/User/GetSubUserInfo?SubUserId=' + useid,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend:function(request){
            request.setRequestHeader('Authorization','Bearer '+access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    $('#divLoading').hide();
                    var data = r.Data;
                    var email = data.Email;
                    teammates_SetValueModalPermission(email, data)
                    break;
                default:
                    $('#divLoading').hide();
                    custom_shownotification('error', message.haveError);
                    break;
            }
        }, error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification('error', message.haveError);
        }
    })
}

function teammates_SetValueModalPermission(email,data)
{
    $('#modal-update-teammates').modal('toggle');

    //set email
    $('#subuser-email').val(email);
    $('#subuser-email').attr('readonly');

    var role = data.RoleId;
    $('input[name=permission][value=' + role + ']').prop('checked', true);
    //show detail permisson
    teamates_ValueRadioPermission();

    var preset = data.PresetId;
    btn_permission = data.PresetId;
    $('#list-btn-permission a').removeClass('green-jungle');
    $('#list-btn-permission a[data-value=' + preset + ']').addClass('green-jungle');
  
    //list ionranger
    teammates_SetValueOfIonRanger(data.FunctionList);

}

function teammates_SetValueOfIonRanger(data)
{
    var listf = $('#group-custom-permission .input-data-permission');
    if (data.length > 0) {
        $.each(listf, function (i, o) {

            var FunctionId = $(this).attr('data-id');
            var functionobj = getObjects(data, 'FunctionId', FunctionId);
            if (functionobj.length > 0) {
                var from = parseInt(functionobj[0].AccessLevel);
            } else {
                var from = 0;
            }
            var slider = $(this).data("ionRangeSlider");
            slider.update({
                from: from
            });
        });
    }
    else {
        //set default là 0
        var slider = $(this).data("ionRangeSlider");
        slider.update({
            from: 0
        });
    }

}

function teammates_ResentEmail(apiDomain,access_token,message)
{
    $('#table-subuser').on('click', '#resent-subuser', function () {
        var userid = $(this).data('id');
        $('#modelConfirmResendSubUser').modal('toggle');

        $('#btnResentSubUser').on('click', function () {

            $('#modelConfirmResendSubUser').modal('toggle');
            teammates_ResendInviteSubUser(apiDomain, access_token, userid, message);
        })
    
    });
}

function teammates_ResendInviteSubUser(apiDomain, access_token, userid, message) {
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/User/ResendInviteSubUser?SubUserId=' + userid,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        beforeSend:function(request)
        {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success:function(r)
        {
            switch(parseInt(r.ErrorCode))
            {
                case 0:
                    $('#divLoading').hide();
                    custom_shownotification('success', message.success);
                    break;
                default:
                    $('#divLoading').hide();
                    custom_shownotification('error', message.haveError);
                    break;
            }
        },error:function(x,s,e)
        {
            $('#divLoading').hide();
            custom_shownotification('error', message.haveError);
        }
    })
}

function temmates_DeleteSubUser(apiDomain, access_token, message, permissiontable)
{
    $('#btnDeleteSubUser').on('click', function () {
        var userid = $(this).attr('data-id');
        $.ajax({
            url: apiDomain + 'api/User/DeleteSubUser?SubUserId=' + userid,
            type: 'post',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader('Authorization', 'Bearer ' + access_token);
            },
            success: function (r) {
                switch (parseInt(r.ErrorCode)) {
                    case 0:
                        $('#divLoading').hide();
                        custom_shownotification('success', message.deleteSuccess);
                        $('#modelConfirmDeleteSubUser').modal('toggle');
                        //load lại danh sach
                        teammates_GetSubUserList(apiDomain, access_token, message, permissiontable);
                        break;
                    default:
                        $('#divLoading').hide();
                        custom_shownotification('error', message.haveError);
                        break;
                }
            }, error: function (x, s, r) {
                $('#divLoading').hide();
                custom_shownotification('error', message.haveError);
            }
        })
    })
}

function teammates_ResetData()
{
    $('#modal-update-teammates').on('shown', function () {
        $('#modal-update-teammates .modal-dialog div').scrollTop(0);
    });
    //email
    $('#subuser-email').val('');

    //permission admin
    $('input[name=permission][value=1]').prop('checked', true);
    teamates_ValueRadioPermission();
    //preset custom
    btn_permission = 0;
    $('#list-btn-permission a').removeClass('green-jungle');
    $('#list-btn-permission a[data-value=0]').addClass('green-jungle');
}