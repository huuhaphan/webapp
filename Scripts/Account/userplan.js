﻿
var ForPlanId;  //plan id trong view ds plan lấy khi click chọn gói để mua
var MenthodId; //lấy khi lick vào chọn phương thức thanh toán
var Note; // lấy khi click chọn phương thức thanh toán 
var Price;//lấy khi click chọn gói để mua 
var PlanName;// lấy khi clic vào chọn gói 
var color = ["blue", "red", "green", "yellow-gold", "purple-seance"]; //color danh sách gói cước
var CancelUrl = "http://localhost:6114/Account/UserProfile#MonthlyPlan";
var SuccessUrl = "http://localhost:6114/Payment";

//var card = -1; //chưa chọn nhà mạng cung cấp thẻ cào 

function userplan_Action(apiDomain, access_token, UserInfo,message)
{
    //load danh sách gói cước 
    SelectMonthlyPlanTab(apiDomain, access_token, message);

    //click chọn gói cước mua
    userplan_OpenModelBuy(apiDomain, access_token,UserInfo,message);

    //chọn loại thẻ cào 
    //userplan_ClickChooseCard();

    //userplan_CheckPaymentCard();

    //nạp qua thẻ atm có internetbanking
    userplan_SelectBankATM(apiDomain, access_token,UserInfo,message);

    //chọn phương thức nạp qua tài khoản visa
    userplan_SelectVisa(apiDomain, access_token,UserInfo,message);

    //chọn phương thức nạp qua tài khoản ngân lượng
    //userplan_SelectNL(apiDomain, access_token,UserInfo);

    //chọn phương thức chuyển khoản
    userplan_SelectSendMoney(apiDomain, access_token,message);

    //chọn phương thức atm 
    userplan_SelectATM(apiDomain, access_token,message);

    //đóng phương thức atm
    userplan_CloseSelectATM();

    ////đóng phương thức nạp thẻ cào 
    //userplan_CloseSelectCard();

    ////chọn phương thức nạp thẻ cào 
    //userplan_SelectCard();

   
   
}

function SelectMonthlyPlanTab(apiDomain, access_token,message)
{
   
    userplan_LoadPlan(apiDomain, access_token,message)
}

function userplan_LoadPlan(apiDomain,access_token,message)
{
    $.ajax({
        url: apiDomain + 'api/list/Plan',
        contentType: 'aplication/json;charset=ut-8',
        type: 'GET',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            var errorcode = parseInt(r.ErrorCode)
            switch(errorcode)
            {
                case 0:
                    var data = JSON.parse(JSON.stringify(r.Data))
                    var column = 12 / data.length;

                    var html ='';
                    $.each(r.Data, function (i, data) {
                        var auto = data.IsAutoResponder;
                        var submit = "";
                        switch (parseInt(data.Price))
                        {
                            case 0:
                                submit = '<button id="click-use-plan" data-plan="' + data.PlanName + '" data-price="' + data.Price + '" data-planid="' + data.PlanId + '" data-id="' + i + '" type="button" class="btn-circle btn btn-circle btn ' + color[i] + ' sbold uppercase price-button"  data-loading-text="Loading..."  >' + message.ButtonFree + '</button>';
                                break;
                            default:
                                submit = '<button id="click-buy-plan" data-plan="' + data.PlanName + '" data-price="' + data.Price + '" data-planid="' + data.PlanId + '" data-id="' + i + '" type="button" class="btn-circle btn btn-circle btn ' + color[i] + ' sbold uppercase price-button"  data-loading-text="Loading..." >' + message.ButtonBuy + '</button>';
                                break;
                        }
                        switch (data.IsAutoResponder)
                        {
                            case true:
                                auto = 'Có';
                                break;
                            case false:
                                auto = 'Không';
                                break;
                        }

                        html += '<div class="col-md-' + column + '">' +
                                                       ' <div class="price-column-container border-active">' +
                                                           ' <div class="price-table-head bg-' + color[i] + '">' +
                                                              '  <h2 class="no-margin" id="plan-name">' + data.PlanName + '</h2>' +
                                                          '  </div>' +
                                                           ' <div class="arrow-down border-top-' + color[i] + '"></div>' +
                                                         '   <div class="price-table-pricing">' +
                                                              '  <h3>' +
                                                                  '  <span class="price-sign">đ</span>'+data.Price+'</h3>' +
                                                               ' <p>trên tháng</p>' +
                                                            '</div>' +
                                                           '<div class="price-table-content">' +
                                                                '<div class="row mobile-padding">' +
                                                                   ' <div class="col-xs-3 text-right mobile-padding">' +
                                                                        '<i class="icon-drawer"></i>' +
                                                                   ' </div>' +
                                                                   ' <div class="col-xs-9 text-left mobile-padding"> Hạn mức: ' + data.Quota + ' email</div>' +
                                                                '</div>' +
                                                               ' <div class="row mobile-padding">' +
                                                                    '<div class="col-xs-3 text-right mobile-padding">' +
                                                                      '  <i class="icon-refresh"></i>' +
                                                                  '  </div>' +
                                                                   ' <div class="col-xs-9 text-left mobile-padding">Nâng cao: ' + auto + '</div>' +
                                                               ' </div>' +
                                                                
                                                           ' </div>' +
                                                            '<div class="arrow-down arrow-grey"></div>' +
                                                           ' <div class="price-table-footer">' +
                                                              submit+
                                                            '</div>' +
                                                       ' </div>' +
                                                    '</div>'
                    })
                    $('#pricing-email').append(html);
                    break;
                default: custom_shownotification('error', message.GetPlanError);
                    break;
            }
            console.log(r);
             
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.GetPlanError);
        }
    });
   
}

//chọn mua gói sử dụng 
function userplan_OpenModelBuy(apiDomain, access_token, UserInfo, message) {
    $('#pricing-email').on('click', '#click-buy-plan', function () {
        //chạy trạng thái loadding chờ xử lí 
        var btn = $(this);
        btn.button('loading');
        MenthodId = 7;
        var MonthlyPlanName = $(this).data('plan');
        Note = "Nâng cấp gói"
        ForPlanId = $(this).data('planid'); //lấy plan id dùng khi tạo paycode
        Price = $(this).data('price')//lấy Price dùng khi tạo pay code
        //nếu đủ tiền
        var price = $(this).data('price');
        $.ajax({
            url: '/PayMent/ComparePrice?pId=' + ForPlanId,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',

            success: function (r) {
                switch (parseInt(r)) {
                    case 1: //đủ tiền upgrade
                        $.ajax({
                            url: '/Payment/UserUpgrade?PlanId=' + ForPlanId,
                            type: "POST",
                            success: function (result) {
                                switch (parseInt(result.ErrorCode)) {
                                    case 0:
                                        var redirect = "none";
                                        //cập nhập session
                                        $.ajax({
                                            url: apiDomain + 'api/User/Info?IsDirty=true',
                                            type: 'Get',
                                            contentType: 'application/json; charset=utf-8',
                                            beforeSend: function (request) {
                                                request.setRequestHeader("Authorization", "Bearer " + access_token);
                                            },
                                            success: function (resultinfo) {
                                                var user = (JSON.parse(JSON.stringify(resultinfo.Data)))
                                                var currentplan = JSON.parse(JSON.stringify(user.CurrentMonthlyPlan));
                                                $('#info-monthlyplan-name').text(use.CurrentMonthlyPlan);
                                                $('#info-quota').text(user.Quota);
                                                $('#plan-start-date').text("");
                                                $('#plan-start-date').text(user.StarDate);
                                                $('#plan-end-date').text("");
                                                $('#plan-end-date').text(user.EndDate);
                                                $('#current-blance-user').text("");
                                                $('#current-blance-user').text(user.CurrentBalance)
                                               

                                                //lưu session
                                                custom_UpdateSesion(user,access_token, redirect);

                                            },
                                            error: function (x, s, e) {
                                                console.log(JSON.stringify(x.reponseText));
                                            }

                                        });
                                        custom_shownotification('success', message.UpgradeSuccess);
                                        //reload table log trong tab lịch sử 
                                        $("#tableMonthlyPlanLog").dataTable().fnDraw();

                                        //load lại thông tin từ session khi khi chuyển qua tab currentplanlog
                                        //vì sau khi nâng cấp gói thông tin được lưu trên session phải cập nhập thông tin
                                        //var MonthlyPlan = JSON.parse(JSON.stringify(r.Data));
                                        //$('#info-monthlyplan-name').text(MonthlyPlanName);
                                        //$('#info-quota').text(MonthlyPlan.Quota);
                                        //$('#plan-start-date').text("");
                                        //$('#plan-start-date').text(parseJsonDate(MonthlyPlan.StarDate));
                                        //$('#plan-end-date').text("");
                                        //$('#plan-end-date').text(parseJsonDate(MonthlyPlan.EndDate));
                                        btn.button('reset');
                                        break;
                                    case 104:
                                        custom_shownotification('error', message.HadPaid);
                                        btn.button('reset');
                                        break;

                                    default:
                                        custom_shownotification('error', message.UpgradeError);
                                        btn.button('reset');
                                        break;
                                }
                            },
                            error: function (x, s, e) {
                                btn.button('reset');
                                custom_shownotification('error', message.UpgradeError)
                            }
                        });
                        break;
                    case 0: //không đủ tiền
                        btn.button('reset');
                        //btn.attr('data-target', '#modal-buy-plan');
                        //btn.attr('data-toggle', 'modal');
                        $('#modal-buy-plan').modal('toggle');

                        $('.selected-plan').text(btn.data('plan')); //đưa lên modal phương thức thanh toán 
                        $('.slected-price').text(btn.data('price')); //đưa lên modal phương thức thanh toán 

                        ForPlanId = btn.data('planid'); //lấy plan id dùng khi tạo paycode
                        Price = btn.data('price')//lấy Price dùng khi tạo pay code
                        PlanName = btn.data('plan')// lấy plan name dùng khi hiển thị trong chuyển khoản
                        
                        
                        break;
                    case -1: //lỗi lúc kiểm tra trên mvc
                        custom_shownotification('error', message.Error);
                        btn.button('reset');
                        break;
                }

            },
            error: function (x, s, e) {
                custom_shownotification('error', message.Error);
                btn.button('reset');
            },

        })

    });
}

// convert datatetime
function parseJsonDate(jsonDateString) {
    return moment(new Date(parseInt(jsonDateString.replace('/Date(', '')))).local().format("DD-MM-YYYY HH:mm");
}

////chọn phương thức nạp bằng thẻ cào 
//function userplan_SelectCard()
//{
//    $('#payment-choose-card').on('click', function () {
//        $('#modal-buy-plan').modal('toggle');
//    })
//}

////đóng modal phương thức thẻ cào 
//function userplan_CloseSelectCard()
//{
//    $('#close-modal-card').on('click', function () {
//        //reset 
//        card = -1;
//        userplan_ResetChooseCard();
//        $('#seri-card').val("");
//        $('#pin-card').val("");
//        var validator = $("#form-payment-card").validate();
//        validator.resetForm();
//        $('#modal-buy-plan').modal('toggle');
//    })
//}

//chọn phương thức chuyển khoản
function userplan_SelectSendMoney(apiDomain, access_token,message)
{
    $('#payment-choose-sendcount').on('click', function () {
        //thì modal phuuong thuc nạp tiền đóng
        MenthodId = 1;
        Note = 'Mua gói sử dụng';
        //tạo pay code
        var Object= userplan_GenCode(apiDomain, access_token, MenthodId, Price, ForPlanId, Note);
        var errorcode = parseInt(Object.ErrorCode)
        switch (errorcode) {
            case 0:
                $(this).attr('data-target', '#modal-send-money');
                $(this).attr('data-toggle', 'modal');
                var data = JSON.parse(JSON.stringify(Object.Data))

                $('#pay-code').text(data.PayCode);
                $('#pay-code-2').text(data.PayCode);
                $('#pay-plan-name').text(PlanName);
                $('#pay-price').text(Price);
                $('#pay-price-2').text(Price);
                $('#pay-create-date').text(data.CreateDate)
                $('#modal-buy-plan').modal('toggle');
                break;
            default: custom_shownotification('error', message.GenCodeError)
        }

        
    })
   
}

//chọn phương  thức atm qua api ngân lượng
function userplan_SelectATM(apiDomain, access_token,message) {
    $('#payment-choose-atm').on('click', function () {
        //thì modal phuuong thuc nạp tiền đóng
        $('#modal-buy-plan').modal('toggle');
    });
}
//đóng moal phương thức atm 
function userplan_CloseSelectATM() {
    $('#close-modal-atm').on('click', function () {
        //thì mở lại modal phương thức
        $('#modal-buy-plan').modal('toggle');
    });
}

//function userplan_ClickChooseCard()
//{
//    userplan_Viettel();
//    userplan_Mobile();
//    userplan_Vina();
//    userplan_Vcoin();
//    userplan_Gate();
//}

//function userplan_Viettel()
//{
//    $('#click-choose-card-viettel').on('click', function () {
//        userplan_ResetChooseCard();
//        $(this).children().css('background', 'url(../Content/Images/items-viettel.png) no-repeat 0 -41px');
//        card = 1;
//    })
//}
//function userplan_Mobile() {
//    $('#click-choose-card-mobi').on('click', function () {
//        userplan_ResetChooseCard();
//        $(this).children().css('background', 'url(../Content/Images/items-mobiphone.png) no-repeat 0 -41px');
//        card = 2;
//    })
//}
//function userplan_Vina() {
//    $('#click-choose-card-vina').on('click', function () {
//        userplan_ResetChooseCard();
//        $(this).children().css('background', 'url(../Content/Images/items-vinaphone.png) no-repeat 0 -41px');
//        card = 3;
//    })
//}
//function userplan_Vcoin() {
//    $('#click-choose-card-vcoin').on('click', function () {
//        userplan_ResetChooseCard();
//        $(this).children().css('background', 'url(../Content/Images/items-vcoin.png) no-repeat 0 -41px');
//        card = 4;
//    })
//}
//function userplan_Gate() {
//    $('#click-choose-card-gate').on('click', function () {
//        userplan_ResetChooseCard();
//        $(this).children().css('background', 'url(../Content/Images/items-gate.png) no-repeat 0 -41px');
//        card = 5;
//    })
//}

//function userplan_ResetChooseCard()
//{
    
//    $('.item-viettel').css('background', 'url(../Content/Images/items-viettel.png) no-repeat 0 0');
//    $('.item-mobi').css('background', 'url(../Content/Images/items-mobiphone.png) no-repeat 0 0');
//    $('.item-vina').css('background', 'url(../Content/Images/items-vinaphone.png) no-repeat 0 0');
//    $('.item-vcoin').css('background', 'url(../Content/Images/items-vcoin.png) no-repeat 0 0');
//    $('.item-gate').css('background', 'url(../Content/Images/items-gate.png) no-repeat 0 0');

//}



//function userplan_CheckPaymentCard() {
//    $('#form-payment-card').validate({
//        errorElement: 'span', //default input error message container
//        errorClass: 'help-block help-block-error', // default input error message class
//        focusInvalid: false, // do not focus the last invalid input
//        ignore: "", // validate all fields including form hidden input

//        rules: {

//            sericard: {
//                required: true,
//                number: true
//            },
//            pincard: {
//                required: true,
//                number: true
//            },
//        },
//        errorPlacement: function (error, element) {
//            error.insertAfter(element); // for other inputs, just perform default behavior

//        },

//        highlight: function (element) { // hightlight error inputs
//            $(element)
//                .closest('.form-group').addClass('has-error'); // set error class to the control group
//        },

//        unhighlight: function (element) { // revert the change done by hightlight
//            $(element)
//                .closest('.form-group').removeClass('has-error'); // set error class to the control group
//        },

//        success: function (label) {
//            label
//                .closest('.form-group').removeClass('has-error'); // set success class to the control group
//        },

//        submitHandler: function (form) {
//            if (card == -1) {
//                custom_shownotification("error", "Chọn nhà cung cấp");
//            }
//            else
//            {
//                MenthodId = 4;
//                var data = {
//                    Cardtype: card,
//                    Sericard: $('#seri-card').val(),
//                    Pincard: $('#pin-card').val(),
//                }
//                //gọi hàm nạp 
//                userplan_GeneralPaymentCard(data);
//            }



//        }
//    });
//};


//chọn ngân hàng thanh toán bằng atm
function userplan_SelectBankATM(apiDomain, access_token, UserInfo,message)
{
    $('#box-bank-atm').on('click', '#seclect-bank-atm', function () {
        MenthodId = 2; //mặt định
        Note = "Mua gói sử dụng";
        var Object= userplan_GenCode(apiDomain, access_token, MenthodId, Price, ForPlanId, Note);
        var errorcode = parseInt(Object.ErrorCode)
        switch (errorcode) {
            case 0:
                var data = JSON.parse(JSON.stringify(Object.Data));
                var bank = $(this).children().children('i').attr('class'); //lấy mã ngân hàng
                var Payment = {
                    Bank: bank, 
                    PayCode: data.PayCode,
                    UserName: UserInfo.Name,
                    Email: UserInfo.Email,
                    Mobile: UserInfo.Mobile,
                    Menthod: 'ATM_ONLINE', //mã mặt định
                    Amount: Price,
                    CancelUrl: CancelUrl,
                    SuccessUrl: SuccessUrl,
                    PlanId:ForPlanId,
                }
                //hàm xử lí 
                userplan_GeneralPayment(Payment, message);

                break;
            default:
                custom_shownotification('error', message.GenCodeError)
                break;
        }
        
        
        

        
    })
}

//thanh toán bằng visa
function userplan_SelectVisa(apiDomain, access_token,UserInfo,message)
{
    $('#payment-choose-visa').on('click', function () {
        MenthodId = 5;
        Note = "Mua gói sử dụng";
        var Object = userplan_GenCode(apiDomain, access_token, MenthodId, Price, ForPlanId, Note);
        var errorcode = parseInt(Object.ErrorCode)
        switch (errorcode) {
            case 0:
                var data = JSON.parse(JSON.stringify(Object.Data));
                var Payment = {
                    PayCode:data.PayCode,
                    UserName: UserInfo.Name,
                    Email: UserInfo.Email,
                    Mobile: UserInfo.Mobile,
                    Menthod: 'VISA',
                    Amount: Price,
                    CancelUrl: CancelUrl,
                    SuccessUrl: SuccessUrl,
                    PlanId: ForPlanId,
                }
                //hàm xử lí 
                userplan_GeneralPayment(Payment, message);

                break;
            default:
                custom_shownotification('error', message.GenCodeError)
                break;
        }
    });
}

//thanh toán bằng tài khoản ngân lượng
//function userplan_SelectNL(apiDomain, access_token,UserInfo) {
//    $('#payment-choose-NL').on('click', function () {
//        MenthodId = 6;
//        Note = "Mua gói sử dụng";
//        var Object = userplan_GenCode(apiDomain, access_token, MenthodId, Price, ForPlanId, Note);
//        var errorcode = parseInt(Object.ErrorCode)
//        switch (errorcode) {
//            case 0:
//                var data = JSON.parse(JSON.stringify(Object.Data));
//                var Payment = {
//                    UserName: UserInfo.Name,
//                    Email: UserInfo.Email,
//                    Mobile: UserInfo.Mobile,
//                    Menthod: 'NL',
//                    Amount: Price,
//                    PayCode:data.PayCode,
//                }
//                //hàm xử lí 
//                userplan_GeneralPayment(Payment);

//                break;
//            default:
//                alert('Có lỗi khi lấy mã thanh toán từ server');
//                break;
//        }
//    });
//}

//hàm nạp tiền chung
function userplan_GeneralPayment(data,message)
{
    $.ajax({
        url: '/Account/UserPayment',
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',

        success:function(r)
        {
            switch(r.Success)
            {
                case true:
                    window.location.href = r.Result;
                    break;
                case false:
                    alert(r.Result);
            }
          
        },
        error:function(x,s,e)
        {
            custom_shownotification('error', message.Error)
        },

    })

}

////hàm nạp card điện thoại
//function userplan_GeneralPaymentCard(data) {
//    $.ajax({
//        url: '/Account/UserPaymentCard',
//        type: 'POST',
//        data: JSON.stringify(data),
//        contentType: 'application/json; charset=utf-8',

//        success: function (r) {
//            alert(r);
//        },
//        error: function (x, s, e) {
//            alert('Có lỗi xảy ra , thử lại sau')
//        },

//    })

//}


function userplan_GenCode(apiDomain, access_token, PaymentMethodId, Amount, ForPlanId, Note)
{
    var Paycode = $.ajax({
        url: apiDomain + 'api/User/CreatePayment?PaymentMethodId=' + PaymentMethodId + '&Amount=' + Amount + '&ForPlanId=' + ForPlanId + '&Note=' + Note + '',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        async: false,
        //success: function (r) {
        //    var errorcode = parseInt(r.ErrorCode);
        //    switch (errorcode) {
        //        case 0:
        //            break;
        //        default: alert('Xảy ra lỗi tạo mã thanh toán');
        //            break;
        //    }

        //},
        //error: function (x, s, e) {
        //    alert('Xảy ra lỗi tạo mã thanh toán');
        //}
    }).responseJSON;
    return Paycode
}
