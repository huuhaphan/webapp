﻿function frommobile_Proccess(apiDomain, access_token, userId, message, permissiontable) {
    //get list from email
    frommobile_GetDeviceList(apiDomain, access_token, message, permissiontable);

    //get list config
    frommobile_GetGatewayConfigList(apiDomain, access_token, message, permissiontable);

    frommobile_ResendFromMobile(apiDomain, access_token, message);

    frommobile_OpenModalAddSetting();
    frommobile_UpDateSetting();
    frommobile_OpenModalDeleteSetting();
    frommobile_DeleteSetting(apiDomain, access_token, message, permissiontable);
    frommobile_AddSetting(apiDomain, access_token, message, permissiontable);

    frommobile_OpenModalDeleteDevice();
    frommobile_DeleteDevice(apiDomain, access_token, message, permissiontable);
}



function frommobile_GetDeviceList(apiDomain, access_token, message, permissiontable) {
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/User/GetDeviceList?IsConfirm=null',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    frommobile_TableFromMobile(data, message, permissiontable);
                    frommobile_SelectMobileSetting(data, message);
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
            $('#divLoading').hide();
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
            $('#divLoading').hide();
        }
    })

}

function frommobile_GetGatewayConfigList(apiDomain, access_token, message, permissiontable)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/User/GetGatewayConfigList',
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    frommobile_TableSettingFromMobile(data, message, permissiontable);
                  
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
            $('#divLoading').hide();
        },
        error: function (x, s, e) {
            custom_shownotification('error', message.HaveError);
            $('#divLoading').hide();
        }
    })

}

function frommobile_AddSetting(apiDomain, access_token, message, permissiontable) {


    $('#btn-add-setting-from-mobile').on('click', function () {
        if (frommobile_CheckDataSetting(message)) {
            var data = {
                ConfigName: $('#frommobile-setting-name').val(),
                DefaultDeviceTokenItemId: parseInt($('#select-sendsms-default-device').val()),
                MobifoneDeviceTokenItemId: parseInt($('#select-sendsms-device').val()),
                VinaphoneDeviceTokenItemId: parseInt($('#select-sendsms-device-vina').val()),
                ViettelDeviceTokenItemId: parseInt($('#select-sendsms-device-viettel').val()),
            };
            var ulrapi = 'api/User/CreateGatewayConfig';
            var item = $(this).attr('data-itemid');
            if (item !== null && item != undefined) {
                data.ItemId = item;
                ulrapi = 'api/User/UpdateGatewayConfig';
            }
            $('#modal-add-setting-from-mobile').modal('toggle');
            $.ajax({
                url: apiDomain + ulrapi,
                type: 'post',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                beforeSend: function (request) {
                    request.setRequestHeader('Authorization', 'Bearer ' + access_token);
                },
                success: function (r) {
                    switch (parseInt(r.ErrorCode)) {
                        case 0:
                            var data = r.Data;
                            custom_shownotification('success', message.UpdateSuccess);

                            //load lại dan sách
                            frommobile_GetGatewayConfigList(apiDomain, access_token, message, permissiontable);

                            break;
                        default:
                            custom_shownotification('error', message.HaveError);
                            break;
                    }
                },
                error: function (x, s, e) {
                    custom_shownotification('error', message.HaveError);
                }
            });
        }
    })

}


function frommobile_CheckDataSetting(message)
{
    if ($('#frommobile-setting-name').val() == "") {
        custom_shownotification('error', 'Tên cấu hình là bắt buộc');
        return false;
    }
    if (parseInt($('#select-sendsms-device').val()) == -1) {
        custom_shownotification('error', 'Chưa đăng kí thiết bị gửi');
    }
    else {
        return true;
    }
}

function frommobile_OpenModalAddSetting()
{
    $('#btn-add-setting').on('click', function () {
        $('#frommobile-setting-name').val('');
        $('#modal-add-setting-from-mobile').modal('toggle');
    });
}

function frommobile_UpDateSetting() {
    $('#inner_table_setting').on('click', '#update-setting', function () {
        var name = $(this).attr('data-name');
        var item = $(this).attr('data-setting-id');
        var itemdefault = $(this).attr('data-default');
        var itemmobie = $(this).attr('data-mobi');
        var itemvina = $(this).attr('data-vina');
        var itemviettel = $(this).attr('data-viettel');
        $('#frommobile-setting-name').val(name);
        $('#select-sendsms-default-device').val(itemdefault);
        $('#select-sendsms-device').val(itemmobie);
        $('#select-sendsms-device-vina').val(itemvina);
        $('#select-sendsms-device-viettel').val(itemviettel);
        $('#btn-add-setting-from-mobile').attr('data-itemid', item);
        $('#modal-add-setting-from-mobile').modal('toggle');


    });
}

function frommobile_OpenModalDeleteSetting() {
    $('#inner_table_setting').on('click', '#delete-setting', function () {
        var id = $(this).attr('data-id');
        $('#modelConfirmDeleteSetting').modal('toggle');
        $('#btnDeleteSetting').attr('data-id', id);
    });
}

function frommobile_DeleteSetting(apiDomain, access_token, message, permissiontable) {
    $('#btnDeleteSetting').on('click', function () {
        $('#modelConfirmDeleteSetting').modal('toggle');
        $('#divLoading').show();
        var id = $(this).attr('data-id');
        $.ajax({
            url: apiDomain+'api/User/DeleteGatewayConfig?ItemId=' + id,
            type: 'Post',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader('Authorization', 'Bearer ' + access_token);
            },
            success: function (r) {
                switch (parseInt(r.ErrorCode)) {
                    case 0:
                        custom_shownotification('success', message.DeleteSuccess);
                        frommobile_GetGatewayConfigList(apiDomain, access_token, message, permissiontable);
                        break;
                    default:
                        custom_shownotification('error', message.HaveError);
                        break;
                }
                $('#divLoading').hide();

            },
            error: function (x, s, e) {
                custom_shownotification('error', message.HaveError);
                $('#divLoading').hide();
            }
        })
    })
}

function frommobile_SelectMobileSetting(data, message) {
    var html = '';
    if (data.length > 0) {
        var obj = data;
        $.each(obj, function (i, o) {
            html += '<option value="' + o.ItemId + '">' + o.DeviceModel + '<' + o.MobileNumber + '>' + '</option>';
        });

       
    } else {
        var html = '<option value="-1">Chưa đăng kí số điện thoại</option>';
    }
    $('#select-sendsms-device').append(html);
    $('#select-sendsms-device-vina').append(html);
    $('#select-sendsms-device-viettel').append(html);
    $('#select-sendsms-default-device').append(html);
}


function frommobile_OpenModalDeleteDevice() {
    $('#inner-table-config').on('click', '#delete-device', function () {
        var id = $(this).attr('data-id');
        $('#modelConfirmDeleteDevice').modal('toggle');
        $('#btnDeleteDevice').attr('data-id', id);
    });
}

function frommobile_DeleteDevice(apiDomain, access_token, message, permissiontable) {
    $('#btnDeleteDevice').on('click', function () {
        $('#modelConfirmDeleteDevice').modal('toggle');
        $('#divLoading').show();
        var id = $(this).attr('data-id');
        var data = {
            DeviceId: id,
        };
        $.ajax({
            url: apiDomain + 'api/User/DeleteUserDevice',
            type: 'Post',
            data:JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader('Authorization', 'Bearer ' + access_token);
            },
            success: function (r) {
                switch (parseInt(r.ErrorCode)) {
                    case 0:
                        custom_shownotification('success', message.DeleteSuccess);
                        frommobile_GetDeviceList(apiDomain, access_token, message, permissiontable);
                        break;
                    case 115:
                        custom_shownotification('error', message.BeforeDeleteCofing);
                        break;
                    default:
                        custom_shownotification('error', message.HaveError);
                        break;
                }
                $('#divLoading').hide();

            },
            error: function (x, s, e) {
                custom_shownotification('error', message.HaveError);
                $('#divLoading').hide();
            }
        })
    })
}



function frommobile_TableSettingFromMobile(data, message, permissiontable)
{
    $('#table-setting-from-mobile').children().remove();
    var html = "";
    var tool = '';
    if (data.length > 0) {
        $.each(data, function (i, o) {
            var def = o.DefaultDeviceToken;
            var defdevice = typeof def.DeviceModel !== 'undefined' && def.DeviceModel !== null ? def.DeviceModel : '';
            var defmobile = typeof def.MobileNumber !== 'undefined' && def.MobileNumber !== null ? def.MobileNumber : '';
            var mobi = o.MobifoneDeviceToken;
            var mobidevice = typeof mobi.DeviceModel !== 'undefined' && mobi.DeviceModel !== null ? mobi.DeviceModel : '';
            var mobimobile = typeof mobi.MobileNumber !== 'undefined' && mobi.MobileNumber !== null ? mobi.MobileNumber : '';
            var vina = o.VinaphoneDeviceToken;
            var vinadevice = typeof vina.DeviceModel !== 'undefined' && vina.DeviceModel !== null ? vina.DeviceModel : '';
            var vinamobile = typeof vina.MobileNumber !== 'undefined' && vina.MobileNumber !== null ? vina.MobileNumber : '';
            var viettel = o.ViettelDeviceToken;
            var vietteldevice = typeof viettel.DeviceModel !== 'undefined' && viettel.DeviceModel !== null ? viettel.DeviceModel : '';
            var viettelmobile = typeof viettel.MobileNumber !== 'undefined' && viettel.MobileNumber !== null ? viettel.MobileNumber : '';
            
         
            if (def != null && viettel != null && mobi != null && vina != null)
                if (permissiontable.IsParent) {
                    tool = '<a class="btn btn-xs default green-jungle-stripe" id="update-setting" data-name="' + o.ConfigName + '" data-setting-id="' + o.ItemId + '" data-viettel="' + viettel.ItemId + '" data-vina="' + vina.ItemId + '" data-mobi="' + mobi.ItemId + '" data-default="' + def.ItemId + '">' + message.Update + '</a>' +
                           '<a class="btn btn-xs default red-stripe" id="delete-setting" data-id="' + o.ItemId + '">' + message.Delete + '</a>';
                } else {
                    if (parseInt(permissiontable.FromMobile) == 2) {
                        tool = '<a class="btn btn-xs default green-jungle-stripe" id="update-setting" data-name="' + o.ConfigName + '" data-setting-id="' + o.ItemId + '" data-viettel="' + viettel.ItemId + '" data-vina="' + vina.ItemId + '" data-mobi="' + mobi.ItemId + '" data-default="' + def.ItemId + '">' + message.Update + '</a>' +
                           '<a class="btn btn-xs default red-stripe" id="delete-setting" data-id="' + o.ItemId + '">' + message.Delete + '</a>';
                    }
                }
            html +=
                   '<tr>' +
                   '<td style="width:20%;"><span style="font-size:13px">' + o.ConfigName + '</span></td>' +
                    '<td style="width:80%;" class="text-left">' +

                     '<span style="font-size:13px;margin-bottom:0px;">' + message.Senttoanother + ':  ' + def.DeviceModel + '<' + def.MobileNumber + '>' + '</span>' +
                    '<span style="font-size:13px;margin-bottom:0px;padding-left:20px;">' + message.Senttoviettel + ':  ' + viettel.DeviceModel + '<' + viettel.MobileNumber + '>' + '</span><br/>' +
                      '<span style="font-size:13px;margin-bottom:0px;">' + message.Senttomobi + ':  ' + mobi.DeviceModel + '<' + mobi.MobileNumber + '>' + '</span>' +
                       '<span style="font-size:13px;margin-bottom:0px;padding-left:20px;">' + message.Senttovina + ':  ' + vina.DeviceModel + '<' + vina.MobileNumber + '>' + '</span><br/>' +

                    ' </td>' +
                 
                   '<td style="width:20%;"class="text-right"> ' +
                    tool +
                      '</td>'
            '</tr>';

        });
    } else {
        html += '<tr>' +
                   '<td colspan="3" class="text-center"><p style="font-size:13px;"> ' + message.NohaveConfig + ' </span></td>' +
         '</tr>';
    }

    var height = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.portlet-title').outerHeight() - $('.page-footer').outerHeight() - 100;
    $('#inner_table_setting').css({
        'height': height/2-60,
    });
    $('#table-setting-from-mobile').append(html);
}

function frommobile_TableFromMobile(data, message, permissiontable) {
    $('#table-config-from-mobile').children().remove();
    var html = "";
    if (data.length > 0) {
        $.each(data, function (i, o) {
            var tool = '';
            var status = '';

            if (!o.IsConfirmed) {
                status = '<span style="font-size:13px;">' + message.VerificationPending + '</span><br/><span style="font-size:13px;"> ' + message.VerifyMobilePending + ' </span>';
                if (permissiontable.IsParent) {
                    tool = //'<a data-deviceid="' + o.DeviceId + '" data-frommobile="' + o.MobileNumber + '" class="btn btn-xs default green-jungle-stripe" id="resend">' + message.Reverify + '</a>' +
                           '<a class="btn btn-xs default red-stripe" id="delete-device" data-id="' + o.DeviceId + '">' + message.Delete + '</a>';

                } else {
                    if (parseInt(permissiontable.FromMobile) == 2) {
                        tool = //'<a data-deviceid="' + o.DeviceId + '" data-frommobile="' + o.MobileNumber + '" class="btn btn-xs default green-jungle-stripe" id="resend">' + message.Reverify + '</a>'+
                               '<a class="btn btn-xs default red-stripe" id="delete-device" data-id="' + o.DeviceId + '">' + message.Delete + '</a>';
                    }
                }
            }
            else {
                status = '<span style="font-size:13px;">' + message.Verified + ' </span>';
                if (permissiontable.IsParent) {
                    tool ='<a class="btn btn-xs default red-stripe" id="delete-device" data-id="' + o.DeviceId + '">' + message.Delete + '</a>';

                } else {
                    if (parseInt(permissiontable.FromMobile) == 2) {
                        tool ='<a class="btn btn-xs default red-stripe" id="delete-device" data-id="' + o.DeviceId + '">' + message.Delete + '</a>';
                    }
                }

            }

            html +=
                   '<tr>' +
                   '<td style="width:20%;" class="text-left"><span style="font-size:14px;margin-bottom:0px;font-weight:bold;"> ' + o.DeviceModel + '</span> </td>' +
                   '<td style="width:20%;"><span style="font-size:13px">' +o.MobileNumber+ '</span></td>' +
                   '<td style="width:40%;" class="text-left">' + status + '</td>' +
                   '<td style="width:20%;"class="text-right"> ' +
                    tool +
                      '</td>'
            '</tr>';

        });
    } else {
        html += '<tr>' +
                   '<td colspan="3" class="text-center"><p style="font-size:13px;"> ' + message.NohaveMobile+ ' </span></td>' +
         '</tr>';
    }

    var height = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.portlet-title').outerHeight() - $('.page-footer').outerHeight() - 100;
    $('#inner-table-config').css({
        'height': height/2-30,
    });
    $('#table-config-from-mobile').append(html);
}


function frommobile_ResendFromMobile(apiDomain, access_token, message) {
    $('#table-config-from-mobile').on('click', '#resend', function () {
        var DeviceId = $(this).attr('data-deviceid');
        var MobileNumber = $(this).attr('data-frommobile');
        var data = {
            MobileNumber: MobileNumber,
            DeviceId: DeviceId,
        }

        if ($.cookie('numrefmobile') != null && $.cookie('numrefmobile') != undefined && $.cookie('numrefmobile') != '') {
            var numresend = JSON.parse($.cookie('numrefmobile'));
            console.log(numresend);
            if (numresend.DeviceId == DeviceId && parseInt(numresend.Num) == 5) {
                custom_shownotification('warning', message.ResentOverTime);
            }
            else {
                frommobile_Resend(apiDomain, access_token, data, message);
                //gửi email 
            }
        }
        else {
            var numrefmail = {
                DeviceId: DeviceId,
                Num: 0,
            }
            $.cookie('numrefmobile', JSON.stringify(numrefmail), { expires: 1 });
            frommobile_Resend(apiDomain, access_token, data, message);
        }
    });
}

function frommobile_Resend(apiDomain, access_token, data, message) {
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/User/VerifyMobileNumberRequest',
        data: JSON.stringify(data),
        type: 'post',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);

        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    custom_shownotification('success', message.VerifyMobilePending);
                    var numresend = JSON.parse($.cookie('numrefmobile'));
                    numresend.Num = parseInt(numresend.Num) + 1;
                    $.cookie('numrefmobile', JSON.stringify(numresend), { expires: 1 });
                    break;
                case 102:
                    custom_shownotification('error', message.Verified);
                    break;
                case 106:
                    custom_shownotification('error', message.WatingSendCodeVerifyMobile);
                    break;
                default:
                    custom_shownotification('error', message.HaveError);
                    break;
            }
            $('#divLoading').hide();
        }, error: function () {
            custom_shownotification('error', message.HaveError);
            $('#divLoading').hide();
        }
    })
}


