﻿function usersprofile_Action() {
    //LoadFirstHash();
    ClickOnMenuProfile(); //on menubar
    ClickOnProfile(); //on page

};

function ClickOnProfile()
{
    $('body').on('click', '#btnoverview', function () {
        AccountSetting();
    });
    $('body').on('click', '#btnaccountsetting', function () {
        AccountSetting();
    });

    $('body').on('click', '#btnfrommail', function () {
        FromMail();
    });

     $('body').on('click', '#btnfrommobile', function () {
        FromMobile();
    });


    $('body ').on('click', '#btnmoney', function () {
        Money();
    });

    $('body ').on('click', '#btnrecharge', function () {
        Recharge();
    });

    $('body ').on('click', '#btnteammates', function () {
        Teammates();
    });

    $('body ').on('click', '#btnspecialday', function () {
        SpecialDay();
    });

    $('body ').on('click', '#btnintegrated', function () {
        Integrated();
    });
}

function ClickOnMenuProfile()
{
    $('.btnmenuprofile').on('click', function () {
        var id=$(this).attr('id');
        DetectFirstAcountClick(id);
    })

    //$('.dropdown-user ul li a').on('click', function () {
    //    var id=$(this).attr('id');
    //    DetectFirstAcountClick(id);
    //})
}

function LoadFirstHash()
{
    if (window.location.hash) {
        var hash = (window.location.hash);
        switch (hash) {
            case '#AcountSetting':
                AccountSetting();
                break;
            case '#Teammates':
                Teammates();
                break;
            case '#FromEmail':
                FromMail();
            case '#FromMobile':
                FromMobile();
                break;
            case '#ManageMoney':
                Money();
                break;
            case '#Recharge':
                Recharge();
                break;
            case '#Integrated':
                Integrated();
                break;

        }
    }
}

function DetectFirstAcountClick(id)
{
        switch (id) {
            case 'btnoverview':
                AccountSetting();
                break;
            case 'btnaccountsetting':
                AccountSetting();
                break;
            case 'btnfrommail':
                FromMail();
                break;
            case 'btnmoney':
                Money();
                break;
            case 'btnrecharge':
                Recharge();
                break;
            case 'btnteammates':
                Teammates();
                break;
            case 'btnmenuaccountsetting':
                AccountSetting();
                break;
            case 'btnmenufrommail':
                FromMail();
                break;
            case 'btnmenufrommobile':
                FromMobile();
                break;
            case 'btnmenumoney':
                Money();
                break;
            case 'btnmenurecharge':
                Recharge();
                break;
            case 'btnmenuteammates':
                Teammates();
                break;
            case 'btnspecialday':
                SpecialDay();
                break;
            case 'btnmenuintegrated':
                Integrated();
                break;
        }
    
}


function Integrated() {

    $.ajax({
        type: "POST",
        url: '/Account/Integrated',
        success: function (result) {
            $('.profile-content').html(result);
            $('.profile-usermenu ul').find('li').removeClass('active');
            $('.dropdown-menu').find('li').removeClass('active');
            $('.dropdown-menu #integrated').addClass('active');
            $('.profile-usermenu .nav #integrated').addClass('active');

        }
    });
};

function Teammates() {

    $.ajax({
        type: "POST",
        url: '/Account/Teammates',
        success: function (result) {
            $('.profile-content').html(result);
            $('.profile-usermenu ul').find('li').removeClass('active');
            $('.dropdown-menu').find('li').removeClass('active');
            $('.dropdown-menu #teammates').addClass('active');
            $('.profile-usermenu .nav #teammates').addClass('active');
        }
    });
};

function SpecialDay()
{
    $.ajax({
        type: "POST",
        url: '/Account/SpecialDay',
        success: function (result) {
            $('.profile-content').html(result);

            $('.profile-usermenu ul').find('li').removeClass('active');
            $('.dropdown-menu').find('li').removeClass('active');
            $('.dropdown-menu #specialday').addClass('active');
            $('.profile-usermenu .nav #specialday').addClass('active');
        }
    });
}

function Money() {

    $.ajax({
        type: "POST",
        url: '/Account/Money',
        success: function (result) {
            $('.profile-content').html(result);

            $('.profile-usermenu ul').find('li').removeClass('active');
            $('.dropdown-menu').find('li').removeClass('active');
            $('.dropdown-menu #money').addClass('active');
            $('.profile-usermenu .nav #money').addClass('active');
        }
    });
};
//function UserPlan() {

//    $.ajax({
//        type: "POST",
//        url: '/Account/UserPlan',
//        success: function (result) {
//            $('.profile-content').html(result);
//            $('.nav #accountsetting').removeClass('active');
//            $('.nav #overview').removeClass('active');
//            $('.nav #userplan').addClass('active');
//            $('.nav #money').removeClass('active');
//            $('.nav #recharge').removeClass('active');
           

//        }
//    });

//};
//function OverView() {

//    $.ajax({
//        type: "POST",
//        url: '/Account/OverView',
//        success: function (result) {
//            $('.profile-content').html(result);
//            $('.nav #overview').addClass('active');
//            $('.nav #accountsetting').removeClass('active');
//            $('.nav #userplan').removeClass('active');
//            $('.nav #recharge').removeClass('active');
          
//        }
//    });

//};
function AccountSetting() {

    $.ajax({
        type: "POST",
        url: '/Account/AccountSetting',
        success: function (result) {
            $('.profile-content').html(result);

            $('.profile-usermenu ul').find('li').removeClass('active');
            $('.dropdown-menu').find('li').removeClass('active');
            $('.dropdown-menu #accountsetting').addClass('active');
            $('.profile-usermenu .nav #accountsetting').addClass('active');
        }
    });
};

function FromMail()
{
    $.ajax({
        type: "POST",
        url: '/Account/FromMail',
        success: function (result) {
            $('.profile-content').html(result);

            $('.profile-usermenu ul').find('li').removeClass('active');
            $('.dropdown-menu').find('li').removeClass('active');
            $('.dropdown-menu #frommail').addClass('active');
            $('.profile-usermenu .nav #frommail').addClass('active');
        }
    });
}

function FromMobile() {
    $.ajax({
        type: "POST",
        url: '/Account/FromMobile',
        success: function (result) {
            $('.profile-content').html(result);

            $('.profile-usermenu ul').find('li').removeClass('active');
            $('.dropdown-menu').find('li').removeClass('active');
            $('.dropdown-menu #frommobile').addClass('active');
            $('.profile-usermenu .nav #frommobile').addClass('active');
        }
    });
}


function Recharge()
{
    $.ajax({
        type: "POST",
        url: '/Account/Recharge',
        success: function (result) {
            $('.profile-content').html(result);

            $('.profile-usermenu ul').find('li').removeClass('active');
            $('.dropdown-menu').find('li').removeClass('active');
            $('.dropdown-menu #recharge').addClass('active');
            $('.profile-usermenu .nav #recharge').addClass('active');

        }
    });
}