﻿/// <reference path="../custom.js" />
function accountsetting_Proccess(apiDomain, access_token, userId, message) {
    accounsetting_UdatePass(apiDomain, access_token, message);
    accountsetting_UpdateInfo(apiDomain, access_token, message);
    accountsetting_Avatar(apiDomain, access_token, userId, message);
    accountsetting_Email(apiDomain,access_token,message);
};

function accountsetting_Email(apiDomain, access_token, message) {
    $('#update-email').on('click', function () {
        if (account_Checkemail()) {
            $('#divLoading').show();
            var data = {
                Email: $('#newemail-update').val(),
            }
            $.ajax({
                url: apiDomain + 'api/User/UpdateEmail',
                type: 'post',
                data:JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                beforeSend: function (request) {
                    request.setRequestHeader('Authorization', 'Bearer ' + access_token);
                },
                success: function (r) {
                    switch (parseInt(r.ErrorCode)) {
                        case 0:
                            $('#divLoading').hide();
                            $('#info-email-login').text($('#newemail-update').val());
                            $('#newemail-update').val('');
                            custom_shownotification("success", message.Success);
                            break;
                        case 102:
                            $('#divLoading').hide();
                            $('#newemail-update').val('');
                            custom_shownotification("error","Email đã tồn tại");
                            break;
                        default:
                            $('#divLoading').hide();
                            $('#newemail-update').val('');
                            custom_shownotification("error", message.Error);
                            break;
                    }
                   

                },
                error: function (x, s, e) {
                    $('#divLoading').hide();
                    custom_shownotification("error", message.Error);
                }
            })
        }
    });
}
function account_Checkemail()
{
    if ($('#newemail-update').val() == "" || $('#newemail-update').val() == null) {
        custom_shownotification("error", 'Vui lòng nhập email cần thay đổi');
        return false;
    }
    if (!IsEmail($('#newemail-update').val())) {
        custom_shownotification("error", 'Email không đúng định dạng');
        return false;
    }
    else return true;
}

function UpdatePass(apiDomain, access_token, message) {
    var data = {
        OldPassword: $('#old_password').val(),
        NewPassword: $('#new_password').val(),
        ConfirmPassword: $('#rnew_password').val()
    }
   
  
    $.ajax({
        url: apiDomain + 'api/User/UpdatePassword',
        type: 'POST',
        data: data,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        contentType: 'application/x-www-form-urlencoded',
        success: function (r) {
           
            var error = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)))
            switch (error) {
                case 0: custom_shownotification("success", message.Success);
                    $('#old_password').val('');
                    $('#new_password').val('');
                    $('#rnew_password').val('');
                    break;
                case 101: custom_shownotification("error", message.OldPassError);
                    break;
            }
        },
        error: function (x, s, e) {
            if (x.status == '401') {
                custom_shownotification('error', 'Phiên đăng nhập hết hạn! Vui lòng đăng nhập lại');
                setTimeout(function () { window.location.href = '/login' }, 1000);
            }
            else {
                custom_shownotification("error", message.Error);
            }
        }

    })
};
function accounsetting_UdatePass(apiDomain, access_token, message) {
    $('.accountsetting-form').validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            oldpassword: {
                required: true,
                minlength: 6,
            },
            newpassword: {
                required: true,
                minlength: 6,
            },
            rnewpassword: {
                equalTo: "#new_password"
            },


        },
        messages: {
            oldpassword:{
                required: message.Entercurrentpass,
                minlength: message.PassworldLeastCharacter,
               
            },
            newpassword: {
                required: message.Enternewpass,
                minlength: message.PassworldLeastCharacter,
            },
            rnewpassword: {

                equalTo: message.Reenternewpass,
                
            }
        },
        
        invalidHandler: function (event, validator) { //display error alert on form submit   

        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },

        errorPlacement: function (error, element) {
            if (element.closest('.input-icon').size() === 1) {
                error.insertAfter(element.closest('.input-icon'));
            } else {
                error.insertAfter(element);
            }
        },

        submitHandler: function (form) {
            UpdatePass(apiDomain, access_token, message);
        }
    });
    $('.accountsetting-form input').keypress(function (e) {
        if (e.which == 13) {
            if ($('.accountsetting-form').validate().form()) {
                UpdatePass(apiDomain, access_token, message);
            }
            return false;
        }
    });

};

function accountsetting_LoadInfo(apiDomain, access_token, userinfo) {

    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/User/Info?IsDirty=true',
        type: 'Get',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (result) {
            $('#divLoading').hide();
            switch (parseInt(result.ErrorCode)) {
                case 0:
                    var user = (JSON.parse(JSON.stringify(result.Data)));
                    $('#acount-firstname').val(user.FirstName);
                    $('#acount-lastname').val(user.LastName);
                    $('#acount-mobile').val(user.MobilePhone);
                    $('#acount-company').val(user.Company);
                    $('#acount-street').val(user.Street);
                    $('#acount-facebook').val(user.Facebook);
                    $('#acount-twitter').val(user.Twitter);
                    if (user.City == null) {
                        $('#accountcity').val('');
                    }
                    else {
                        $('#accountcity').val(user.City);
                    }

                    accountsetting_GetCountryOnload(apiDomain, access_token, user.Country);
                    break;
                default:
                    break;
            }
        },
        error: function (x, s, e) {
            //loading 
            $('#divLoading').hide();
            if (x.status == '401') {
                custom_shownotification('error', 'Phiên đăng nhập hết hạn! Vui lòng đăng nhập lại');
                setTimeout(function () { window.location.href = '/login' }, 1000);
            }
            else {
                console.log(JSON.stringify(x.reponseText));
            }
        }

    })

};

function accountsetting_UpdateInfo(apiDomain, access_token, message) {
    $('#submit_updateinfo').on('click', function () {
        var data = {
            firstName: $('#acount-firstname').val(),
            lastName: $('#acount-lastname').val(),
            mobilePhone: $('#acount-mobile').val().trim(),
            company: $('#acount-company').val(),
            street: $('#acount-street').val(),
            facebook: $('#acount-facebook').val(),
            twitter: $('#acount-twitter').val(),
            city: $('#accountcity').val(),
            country: $('#accountcountry option:selected').val(),

        };
        $('#divLoading').show();
        var redirect = "none";
        if (check_InfoBeforeUpdate(message)) {
            $.ajax({
                url: apiDomain + 'api/User/Update',
                type: 'POST',
                data: data,
                contentType: 'application/x-www-form-urlencoded',
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + access_token);
                },
                success: function (r) {
                    $('.profile-usertitle-name').text(data.firstName + ' ' + data.lastName)
                    $('.username.username-hide-on-mobile').text(data.lastName);
                    custom_GetInfo(apiDomain, access_token, redirect);
                    custom_shownotification("success", message.Success);
                    $('#divLoading').hide();
                },
                error: function (x, s, e) {
                    if (x.status == '401') {
                        custom_shownotification('error', 'Phiên đăng nhập hết hạn! Vui lòng đăng nhập lại');
                        setTimeout(function () { window.location.href = '/login' }, 1000);
                    }
                    else
                    {
                        custom_shownotification('error', x.statusText);
                    }
                    $('#divLoading').hide();
                },
            });
        }
    });
};


function check_InfoBeforeUpdate(message)
{
    if ($('#acount-firstname').val() == "" || $('#acount-firstname').val() == null)
    {
        custom_shownotification("error", message.FirstName);
        $('#divLoading').hide();
        return false;
    }
    if ($('#acount-lastname').val() == "" || $('#acount-lastname').val() == null)
    {
        custom_shownotification("error", message.LastName);
        $('#divLoading').hide();
        return false;
    }
    if ($('#acount-mobile').val() == "" || $('#acount-mobile').val() == null)
    {
        custom_shownotification("error", message.Mobile);
        $('#divLoading').hide();
        return false;
    }
    if (!IsMobile($('#acount-mobile').val().trim()))
    {
        custom_shownotification("error", message.MobileFormat);
        $('#divLoading').hide();
        return false;
    }

    else
    {
        return true;
    }
}

//load quốc gia khi load trang (thông tin user)
function accountsetting_GetCountryOnload(apiDomain, access_token, Country) {
    $.ajax({
        url: apiDomain + 'api/list/Country',
        type: 'GET',
        contentType: 'application/x-www-form-urlencoded',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            var data = JSON.parse(JSON.stringify(r.Data));
            var options = new Array();
            for (var i = 0; i < data.length; i++) {
                options.push('<option value="' + data[i].CountryId + '">' + data[i].CountryName + '</option>');
            }
            $('#accountcountry').append(options.join(''));
            $('#accountcountry').val(parseInt(Country));
         

        },error:function(x,s,r)
        {
           
           custom_shownotification('error', 'Có lỗi lấy thông tin quốc gia');
           
        }

    });
    
};


//load quốc gia cho sự kiên change
//function accountsetting_GetCountry(apiDomain, access_token) {
//    $.ajax({
//        url: apiDomain + 'api/list/Country',
//        type: 'GET',
//        contentType: 'application/x-www-form-urlencoded',
//        beforeSend: function (request) {
//            request.setRequestHeader("Authorization", "Bearer " + access_token);
//        },
//        success: function (r) {
//            var data = JSON.parse(JSON.stringify(r.Data));
//            var options = new Array();
//            for (var i = 0; i < data.length; i++) {
//                options.push('<option value="' + data[i].CountryId + '">' + data[i].CountryName + '</option>');
//            }
//            $('#accountcountry').append(options.join(''));
//            var countryid = data[0].CountryId;
//           // accountsetting_GetCity(apiDomain, access_token, countryid);
//        }

//    })
//};



//sự kiện chọn hình, upload hình
function accountsetting_Avatar(apiDomain, access_token, userId, message)
{
    
    var file;
    var blobName;
    $('#accountavatar').on('change', function (e) {
        var files = e.target.files;
        var name = files[0].name;
        file = files[0];
        var extend = name.split('.').pop();
        console.log(extend);
        blobName = userId + moment().format('YYYYMMDDHHmmss') + '.' + extend;
    });
    $('#submitavatar').on('click', function (e) {
        if (file) {
            $('#divLoading').show();
            $.ajax({
                url: apiDomain + 'api/User/GetAvatarSAS?BlobName=' + blobName,
                type: 'GET',
                contentType: 'application/json; charset=utf-8',
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + access_token);
                },
                success: function (r) {
                    uri = r.Data;
                    uploadToAzure(apiDomain, access_token, file, uri, blobName)
                }
            });
        }
        });

}


//upload lên azure và server
function uploadToAzure(apiDomain, access_token, file, uri, blobName) {

    var maxBlockSize = 2 * 1024 * 1024;
    var fileSize = file.size;
    if (fileSize < maxBlockSize) {
        maxBlockSize = fileSize;
    }
    var blockIds = new Array();
    var blockIdPrefix = "block-";
    var currentFilePointer = 0;
    var totalBytesRemaining = fileSize;
    var numberOfBlocks = 0;
    var bytesUploaded = 0;

    if (fileSize % maxBlockSize == 0) {
        numberOfBlocks = fileSize / maxBlockSize;
    } else {
        numberOfBlocks = parseInt(fileSize / maxBlockSize, 10) + 1;
    }

    var reader = new FileReader();
    reader.onloadend = function (evt) {
        if (evt.target.readyState == FileReader.DONE) { // DONE == 2
            var r = uri + '&comp=block&blockid=' + blockIds[blockIds.length - 1];
            var requestData = new Uint8Array(evt.target.result);
            $.ajax({
                url: r,
                type: "PUT",
                data: requestData,
                processData: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('x-ms-blob-type', 'BlockBlob');
                    //    xhr.setRequestHeader('Content-Length', requestData.length);
                },
                success: function (data, status) {
                    uploadFileInBlocks();
                },
                error: function (xhr, desc, err) {
                    $('#divLoading').hide();
                }
            });
        }
    };


    function uploadFileInBlocks() {

        if (totalBytesRemaining > 0) {
            var fileContent = file.slice(currentFilePointer, currentFilePointer + maxBlockSize);
            var blockId = blockIdPrefix + pad(blockIds.length, 6);
            console.log("block id = " + blockId);
            blockIds.push(btoa(blockId));
            reader.readAsArrayBuffer(fileContent);
            currentFilePointer += maxBlockSize;
            totalBytesRemaining -= maxBlockSize;
            if (totalBytesRemaining < maxBlockSize) {
                maxBlockSize = totalBytesRemaining;
            }
        } else {
            commitBlockList();
        }
    }

    uploadFileInBlocks();

    function commitBlockList() {

        var submituri = uri + '&comp=blocklist';
        var requestBody = '<?xml version="1.0" encoding="utf-8"?><BlockList>';
        for (var i = 0; i < blockIds.length; i++) {
            requestBody += '<Latest>' + blockIds[i] + '</Latest>';
        }
        requestBody += '</BlockList>';
        $.ajax({
            url: submituri,
            type: "PUT",
            data: requestBody,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('x-ms-blob-content-type', file.type);
                xhr.setRequestHeader('Content-Length', requestBody.length);
            },
            success: function (data, status) {
                uploadAvata();
            },
            error: function (xhr, desc, err) {
                $('#divLoading').hide();
            }
        });

        function uploadAvata() {
            var data = {
                BlobName: blobName
            };
            $.ajax({
                url: apiDomain + 'api/User/UpdateAvatar',
                type: 'POST',
                data: data,
                contentType: 'application/x-www-form-urlencoded',
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + access_token);
                },
                success: function (r) {
                    var redirect = "none";
                    var user = JSON.parse(JSON.stringify(r.Data));
                   
                    //cập nhập lại session userinfo
                    custom_UpdateSesion(user, access_token, redirect);
                    //cập nhập lại vavatar
                    reloadAvatar(user.Avatar);
                },
                error:function(x,s,e)
                {
                    Console.log(x.responseText);
                    $('#divLoading').hide();
                }
            });
        }
    }
}

function reloadAvatar(Avatar) {
    setTimeout(function () {
        document.getElementById("avatarprofile").src = Avatar;
        document.getElementById("avatarheader").src = Avatar;
        $('#divLoading').hide();
    }, 3000);
   
 
}


//function countsetting_GetFromEmail(apiDomain,access_token,message)
//{
//    var data;
//    $.ajax({
//        url: apiDomain + 'api/User/GetFromMailList',
//        type: 'GET',
//        contentType: 'application/json; charset=utf-8',
//        beforeSend:function(request)
//        {
//            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
//        },
//        success:function(r)
//        {
//            switch(parseInt(r.ErrorCode))
//            {
//                case 0:
//                    var data = r.Data;
//                    countsetting_TableFromEmail(data);
//                    break;
//                default:
//                    custom_shownotification('error', 'Lỗi lấy danh sách email from');
//                    break;
//            }
//        },
//        error:function(x,s,e)
//        {
//            custom_shownotification('error', 'Lỗi lấy danh sách email from');
//        }
//    })
    
//}

//function countsetting_TableFromEmail(data)
//{
//    $('#table-config-from-email').children().remove();
//    var html = "";
//    if (data.length > 0) {
//        $.each(data, function (i, o) {
//            var tool = '';
//            var status = '';
//            if (!o.IsDefault)
//            {
//                if(!o.IsVerify)
//                {
//                    status = 'Đang chờ xác thực';
//                    tool = '<a data-fromid="' + o.ItemId + '" class="btn btn-xs default red-stripe" id="delete"> Xóa</a></td>';
//                }
//                else
//                {
//                  status = 'Đã xác thực';
//                  tool=  '<a id="default" data-fromid="' + o.ItemId + '" class="btn btn-xs default green-stripe"> Set default</a>' +
//                    '<a data-fromid="' + o.ItemId + '" class="btn btn-xs default red-stripe" id="delete"> Xóa</a>';
//                }
//            }
//            else
//            {
//                status = '<b> Mặc định</b>';
//            }
           
            
           
//            html +=
//                   '<tr>' +
//                   '<td style="width:40%;"><p style="font-size:16px;margin-bottom:0px;"> ' + o.FromName + '</p> <span style="font-size:16px">' + o.FromMail + '</span></td>' +
//                   '<td style="width:30%;">'+status+'</td>' +
//                   '<td style="width:30%;"class="text-right"> ' +
//                    tool+
//                      '</td>'
//                   '</tr>';

//        });
//    } else {
//       html+= '<tr>' +
//                  '<td colspan="3" class="text-center"><p style="font-size:16px;">Chưa có email nào đăng kí</span></td>' +
                 
//        '</tr>';
//    }
  
//    var height = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.portlet-title').outerHeight() - $('.page-footer').outerHeight() -200;
//    $('.inner_table').css({
//        'height': height,
//    })
//    $('#table-config-from-email').append(html);
//}

//function acountsetting_ModalDeleteFromEmail()
//{
//    $('#table-config-from-email').on('click', '#delete', function () {
//        var ItemId = $(this).data('fromid');
//        $('#confirm-delete-from-mail').data('fromid', ItemId);
//        $('#modal-delete-from-email').modal('toggle');
//    })
   
//}

//function acountsetting_DeleteFromEmail(apiDomain,access_token,message)
//{
//    $('#confirm-delete-from-mail').on('click', function () {
//        var ItemId = $(this).data('fromid');
//        accountsetting_ActionDeleteFromEmail(apiDomain, access_token, ItemId, message);
//    })
//}

//function accountsetting_ActionDeleteFromEmail(apiDomain, access_token, ItemId, message) {
//    $.ajax({
//        url: apiDomain + 'api/User/DeleteFromMail?ItemId=' + ItemId,
//        type: 'Post',
//        contentType: 'application/json; chartset=utf-8',
//        beforeSend:function(request)
//        {
//            request.setRequestHeader('Authorization','Bearer '+access_token);
//        },
//        success: function (r) {
//            switch (parseInt(r.ErrorCode)) {
//                case 0:
//                    custom_shownotification('success', 'Xóa thành công');
//                    $('#modal-delete-from-email').modal('toggle');
//                    countsetting_GetFromEmail(apiDomain, access_token, message);

//                    break;
//                default:
//                    custom_shownotification('error', 'Có lỗi xảy ra');
                    
//                    break;
//            }
//        }
//        , error: function (x, s, e) {
//            custom_shownotification('error', 'Có lỗi xảy ra');
//        }
//    });
//}

//function acountsetting_ShowAdd()
//{
//    $('#btn-show-add-from-email').on('click',function(){
//        $('#div-add-from-email').show();
//        $(this).hide();
//    })
//}

//function acountsetting_AddFromEmail(apiDomain,access_token,message)
//{
//    $('#btn-add-from-email').on('click', function () {
//        if(acountsetting_CheckAddFromEmail(message))
//        {
//            acountsetting_AcctionAddFromEmail(apiDomain, access_token, message);
//        }
//    });
//}

//function acountsetting_CheckAddFromEmail(message)
//{
//    if(!$('#fromemail-name').val())
//    {
//        custom_shownotification('error', 'Tên là bắt buộc');
//        return false;
//    }
//    if (!$('#fromemail-email').val()) {
//        custom_shownotification('error', 'Email là bắt buộc');
//        return false;
//    }
//    else return true;
//}

//function acountsetting_AcctionAddFromEmail(apiDomain,access_token,message)
//{
//    var data = {
//        FromMail: $('#fromemail-email').val(),
//        FromName: $('#fromemail-name').val(),
//    }
//    $.ajax({
//        url: apiDomain + 'api/User/CreateFromMail',
//        type: 'post',
//        contentType:'application/json; charset=utf-8',
//        data: JSON.stringify(data),
//        beforeSend:function(request)
//        {
//            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
//        },
//        success:function(r)
//        {
//            switch(r.ErrorCode)
//            {
//                case 0:
//                    custom_shownotification('success', 'Tạo from email thành công');
//                    countsetting_GetFromEmail(apiDomain, access_token, message);
//                    break;
//                default:
//                    custom_shownotification('error', 'Lỗi tạo from email');
//                    break;
//            }
//        },
//        error:function(x,s,e)
//        {
//            custom_shownotification('error', 'Lỗi tạo from email');
//        }
//    })
//}

//function accountsetting_DefaultFromEmail(apiDomain, access_token, message)
//{
//    $('#table-config-from-email').on('click', '#default', function () {
//        var ItemId = $(this).data('fromid');
//        accountsetting_SetDefaultFromEmail(apiDomain, access_token, ItemId, message);
//    });
//}

//function accountsetting_SetDefaultFromEmail(apiDomain, access_token, ItemId, message)
//{
//    $.ajax({
//        url: apiDomain + '/api/User/UpdateFromMailDefault?ItemId=' + ItemId,
//        type: 'post',
//        contentType: 'application/json; charset=utf-8',
//        beforeSend:function(request)
//        {
//            request.setRequestHeader('Authorization', 'Bearer ' + access_token);

//        },
//        success:function(r)
//        {
//            switch(parseInt(r.ErrorCode))
//            {
//                case 0:
//                    custom_shownotification('success', 'Cập nhập thành công');
//                    countsetting_GetFromEmail(apiDomain, access_token, message);
//                    break;
//                default:
//                    custom_shownotification('error', 'Có lỗi xảy ra');
//                    break;
//            }
//        }, error: function () {
//            custom_shownotification('error', 'Có lỗi xảy ra');
//        }
//    })
//}




