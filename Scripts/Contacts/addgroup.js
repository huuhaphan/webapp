﻿/// <reference path="formsetting.js" />
function addgroup_EventAdd(apiDomain, access_token, message) {
    $('#savecontactgroup').on('click', function () {
        if (addgroup_CheckNull(message) == true) {
            $('#divLoading').show();
            addgroup_AddContactGroup(apiDomain, access_token, message)
        }
    });

};
function creategroup_EventAdd(apiDomain, access_token, message) {
    $('#saveCreateContactGroup').on('click', function () {
        if (addgroup_CheckNull(message) == true) {
            $('#divLoading').show();
            creategroup_AddContactGroup(apiDomain, access_token, message)
        }
    });

};

//them contactgroup
function addgroup_AddContactGroup(apiDomain, access_token, message)
{
    var active = true;
    if ($('#checkGroupActive').prop('checked')) {
        active = true;
    } else {
        active = false;
    }
    var data = {
        Active:active,
        ContactGroupName: $('#contactgroupname').val(),
        Description: $('#contactgroupdesciption').val(),
    };
    $.ajax({
        url: apiDomain + 'api/ContactGroup/Create',
        type: 'POST',
        data:data,
        contentType: 'application/x-www-form-urlencoded',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success:function(r)
        {
            var errorcode = parseInt(r.ErrorCode);
            $('#divLoading').hide();
            switch (errorcode)
            {
                case 0:
                    custom_shownotification("success", message.Success);
                    $('#modalCreateGroup').modal('toggle');
                    //sau khi them thanh cong refesh table.
                    $("#tableContactGroup").dataTable().fnDraw();
                    //var data = r.Data;

                    ////tạo form đầu tiên cho group
                    //var formdata = {
                    //    ContactGroupId: data.ContactGroupId,
                    //    FormName: data.ContactGroupName,
                    //    AfterSubscriptionAction: '2',  //mặt định
                    //    CustomThankyouPageUrl: '',
                    //    AlreadySubscribeAction: '2',//mặt định // $('input[name=subcribepage]:checked', '#formSetting').val(),
                    //    CustomAlreadySubscribePageUrl: '',//$('#subcribe-url-page').val(),
                    //    Active: true,
                    //};
                    //var IsRedirect = false;
                    //formsetting_ActionCreate(apiDomain, access_token, formdata,IsRedirect, message)
                       
                    
                    break;
                default:
                    break;
            }
        },
        error:function(x, s, e)
        {
            $('#divLoading').hide();
            custom_shownotification("error", message.Error);
        },
    })
}

function creategroup_AddContactGroup(apiDomain, access_token, message) {
    var active = true;
    if ($('#checkGroupActive').prop('checked')) {
        active = true;
    } else {
        active = false;
    }
    var data = {
        Active: active,
        ContactGroupName: $('#contactgroupname').val(),
        Description: $('#contactgroupdesciption').val(),
    };
    $.ajax({
        url: apiDomain + 'api/ContactGroup/Create',
        type: 'POST',
        data: data,
        contentType: 'application/x-www-form-urlencoded',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            var errorcode = parseInt(r.ErrorCode);
            $('#divLoading').hide();
            switch (errorcode) {
                case 0:
                    custom_shownotification("success", message.Success);
                    //sau khi them thanh cong chuyển đến quản lý danh sách.
                    var data = r.Data;
                    var gId = data.ContactGroupId;
                    setTimeout(function () {
                        window.location.href = '/Contacts/ContactList?gId=' + encodeURI(data.ContactGroupId + '&eId=' + data.CampaignId + '&gName=' + data.ContactGroupName, "UTF-8");
                    }, 300);
                   
                   
                    break;
                default:
                    custom_shownotification("error", message.Error);
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification("error", message.Error );
        },
    })
}

//kiểm tra text null
function addgroup_CheckNull(message)
{
    if ($('#contactgroupname').val() == null | $('#contactgroupname').val() == "")
    {
        custom_shownotification("error", message.GroupNameRequire);
        return false;
    }
    else
    {
        return true;
    }
};
