﻿//thêm sự kiện thêm contact bằng tay.
function addcontact_EventAdd(apiDomain, access_token, groupId,CampaignId, message) {
    $('#savecontact').on('click', function () {
        if (addcontact_CheckNull(message) == true) {
            //$('#divLoading').show();
            addcontact_AddContact(apiDomain, access_token, groupId, CampaignId, message)
        }
    })
}
//thêm bằng tay từng contact
function addcontact_AddContact(apiDomain, access_token, groupId, CampaignId, message) {
    var accept = true;
    var sytemcustomfield=localStorage.getItem('SytemCustomField');
    var fullcustomfield = JSON.parse(sytemcustomfield); //đã lưu tại contactlist.js
    console.log(fullcustomfield);

    var active = true;
    if ($('#createContacts #checkContactActive').prop('checked')) {
        active = true;
    } else {
        active = false;
    }
    var mobile = $('#createContacts #txtPhone').val().trim();
    if (mobile) {
        if (parseInt(mobile.substr(0, 2)) == 84) {
            mobile = '0' + mobile.replace(mobile.substr(0, 2), '');
        }
        if (mobile.substr(0, 3) == '+84') {
            mobile = '0' + mobile.replace(mobile.substr(0, 3), '');
        }
    }
    var BirthDate = "";
    if ($('#createContacts #txtBirthday').val() != null && $('#createContacts #txtBirthday').val() != '') {
        BirthDate = moment($('#createContacts #txtBirthday').datepicker('getDate')).format('YYYY-MM-DD');
        
    }

    


    var CustomFields = [];
    if ($('#createContacts #txtAge').val() != null && $('#createContacts #txtAge').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 7);
        var maxLength = getValues(object, 'MaxLength');
        
        if ($('#createContacts #txtAge').val().length > maxLength[0]) {

            custom_shownotification("error", 'Age vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }
        CustomFields.push({ CustomFieldId: 7, CustomFieldValue: $('#createContacts #txtAge').val(), CustomFieldName: 'Age' });
    }
    
    if ($('#createContacts #txtFax').val() != null && $('#createContacts #txtFax').val() != '') {
        var object = getObjects(fullcustomfield, 'FieldId', 13);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#createContacts #txtFax').val().length > maxLength[0]) {

            custom_shownotification("error", 'Fax vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }
        CustomFields.push({ CustomFieldId: 13, CustomFieldValue: $('#createContacts #txtFax').val(), CustomFieldName: 'Fax' });
    }
    if ($('#createContacts #txtCompany').val() != null && $('#createContacts #txtCompany').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 14);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#createContacts #txtCompany').val().length > maxLength[0]) {

            custom_shownotification("error", 'Company vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }
        
        CustomFields.push({ CustomFieldId: 14, CustomFieldValue: $('#createContacts #txtCompany').val(), CustomFieldName: 'Company' });
    }
    if ($('#createContacts #txtUrl').val() != null && $('#createContacts #txtUrl').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 16);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#createContacts #txtUrl').val().length > maxLength[0]) {

            custom_shownotification("error", 'URL vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }
        CustomFields.push({ CustomFieldId: 16, CustomFieldValue: $('#createContacts #txtUrl').val(), CustomFieldName: 'URL' });
    }
    if ($('#createContacts #txtCity').val() != null && $('#createContacts #txtCity').val() != '') {
        var object = getObjects(fullcustomfield, 'FieldId', 19);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#createContacts #txtCity').val().length > maxLength[0]) {

            custom_shownotification("error", 'City vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }
        CustomFields.push({ CustomFieldId: 19, CustomFieldValue: $('#createContacts #txtCity').val(), CustomFieldName: 'City' });
    }
    if ($('#createContacts #txtStreet').val() != null && $('#createContacts #txtStreet').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 20);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#createContacts #txtStreet').val().length > maxLength[0]) {

            custom_shownotification("error", 'Street vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }
        CustomFields.push({ CustomFieldId: 20, CustomFieldValue: $('#createContacts #txtStreet').val(), CustomFieldName: 'Street' });
    }
    if ($('#createContacts #txtAddress').val() != null && $('#createContacts #txtAddress').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 21);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#createContacts #txtAddress').val().length > maxLength[0]) {

            custom_shownotification("error", 'Address vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }
        CustomFields.push({ CustomFieldId: 21, CustomFieldValue: $('#createContacts #txtAddress').val(), CustomFieldName: 'Address' });
    }

    if ($('#createContacts #txtNote').val() != null && $('#createContacts #txtNote').val() != '') {
        var object = getObjects(fullcustomfield, 'FieldId', 15);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#createContacts #txtNote').val().length > maxLength[0]) {

            custom_shownotification("error", 'Comment vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }

    }

    if ($('#createContacts #txtNote1').val() != null && $('#createContacts #txtNote1').val() != '') {
        var object = getObjects(fullcustomfield, 'FieldId', 22);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#createContacts #txtNote1').val().length > maxLength[0]) {

            custom_shownotification("error", 'Comment1 vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }

        CustomFields.push({ CustomFieldId: 22, CustomFieldValue: $('#createContacts #txtNote1').val(), CustomFieldName: 'Comment1' });
    }
    if ($('#createContacts #txtNote2').val() != null && $('#createContacts #txtNote2').val() != '') {
        var object = getObjects(fullcustomfield, 'FieldId', 23);
        var maxLength = getValues(object, 'MaxLength');
        console.log(maxLength[0]);
        console.log()
        if ($('#createContacts #txtNote2').val().length > maxLength[0]) {

            custom_shownotification("error", 'Comment2 vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }
        CustomFields.push({ CustomFieldId: 23, CustomFieldValue: $('#createContacts #txtNote2').val(), CustomFieldName: 'Comment2' });
    }
    if ($('#createContacts #txtNote3').val() != null && $('#createContacts #txtNote3').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 24);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#createContacts #txtNote3').val().length > maxLength[0]) {

            custom_shownotification("error", 'Comment3 vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }
        CustomFields.push({ CustomFieldId: 24, CustomFieldValue: $('#createContacts #txtNote3').val(), CustomFieldName: 'Comment3' });
    }


    var data = {
        ContactGroupId: groupId,
        CampaignId: CampaignId,
        ContactName: $('#createContacts #txtContactName').val(),
        Email: $('#createContacts #txtEmail').val(),
        Mobile: mobile,
        Note: $('#createContacts #txtNote').val(),
        Active: active,
        FirstName: $('#createContacts #txtFirstName').val(),
        LastName: $('#createContacts #txtLastName').val(),
        BirthDate: BirthDate,
        Gender:$('#createContacts #txtGender').val(),
        CustomFields: CustomFields,


    };
    console.log(accept);
    if (accept) {
        addcontact_ActionAdd(apiDomain, access_token, data, message);
    } 

}

function addcontact_ActionAdd(apiDomain, access_token,data, message)
{
    
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/Contact/Create',
        type: 'POST',
        data: data,
        contentType: 'application/x-www-form-urlencoded',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            var errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
            $('#divLoading').hide();
            switch (errorcode) {
                case 0:
                    custom_shownotification("success", message.Success);
                    $('#modalCreateContact').modal('toggle');
                    $('#tableContact').dataTable().fnDraw();
                    break;
                case 110:
                    $('#divLoading').hide();
                    custom_shownotification("error", r.ErrorMessage);
                    break
                case 102:
                    $('#divLoading').hide();
                    custom_shownotification("error", message.EmailExistSuppression);
                    break;
                case 107:
                    $('#divLoading').hide();
                    custom_shownotification("error", message.EmailExitsSentError);
                    break;
                case 999:
                    $('#divLoading').hide();
                    custom_shownotification("error", r.ErrorMessage);
                    break;
                default:
                    custom_shownotification("error", message.Error);
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification("error", message.Error);
        },
    });
}

//kiểm tra contact trước khi thêm/update
function addcontact_CheckNull(message) {
    if ($('#txtContactName').val() == null || $('#txtContactName').val() == '') {
        $('.alert-danger span', $('#createContacts')).text(message.NameNull);
        $('.alert-danger', $('#createContacts')).show();
        topFunction();
        return false;
    }
    if (($('#modalCreateContact #txtEmail').val() == null || $('#modalCreateContact #txtEmail').val() == '') && ($('#txtPhone').val() == '' || $('#txtPhone').val() == null)) {
        $('.alert-danger span', $('#createContacts')).text(message.MobileOrEmailNull);
        $('.alert-danger', $('#createContacts')).show();
        topFunction();
        return false;
    }
    if($('#modalCreateContact #txtEmail').val() && !IsEmail($('#modalCreateContact #txtEmail').val()))
    {
        $('.alert-danger span', $('#createContacts')).text(message.EmailErrorFormat);
        $('.alert-danger', $('#createContacts')).show();
        topFunction();
        return false;
    }
    if ($('#txtPhone').val() && !IsMobile($('#txtPhone').val().trim())) {
        $('.alert-danger span', $('#createContacts')).text(message.MobileErrorFormat);
        $('.alert-danger', $('#createContacts')).show();
         topFunction();
        return false;
    }
    else {
        return true;
    }

}

function topFunction() {
    var myDiv = document.getElementById('content-field');
    myDiv.scrollTop = 0;
}


