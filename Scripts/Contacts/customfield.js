﻿function customfield_Action(apiDomain, access_token, message) {
    customfield_LoadCustomField(apiDomain, access_token, message);
    //customfield_OpenModelCreate();
    customfield_ShowDelete();
    customfield_ActionDeleteCustomField(apiDomain, access_token, message);
}

function customfield_LoadCustomField(apiDomain, access_token, message) {
    $.ajax({
        url: apiDomain + 'api/CustomField/GetFullList',
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            $('#custom-field-custom').children().remove();
            switch (parseInt(r.ErrorCode)) {
                case 0:

                    var html = '';
                    html += '<div  class="col-md-2 create-custom" style="padding:5px"><a id="create-custom-field" data-toggle="modal" href="#modal-create-customfield" class="btn btn-sm green-jungle" style="margin: 2px 0;width:100%;padding: 7px 7px;"><i class="fa fa-plus"></i>Tạo mới</a></div>';

                    $.each(r.Data, function (idx, data) {
                       var FieldName = data.FieldName;
                            if (FieldName.length > 15) {
                                FieldName = FieldName.substring(0, 12) + '...';
                            }
                            if (data.FieldId != 10078) //contetn
                            {
                                if (!data.IsPredefine) {

                                    html += '<div data-id="' + data.FieldId + '" class="col-md-2" style="padding:5px"><a id="' + data.FieldId + '"  class="btn btn-sm white tooltips" data-container="body" data-placement="top" title="' + data.FieldName + '" data-original-title="' + data.FieldName + '" style="margin: 2px 0;width:100%;padding: 7px 7px;text-align:left;">' +
                                        '<i class="' + data.IconUrl + '"></i>' +
                                        FieldName +
                                        '</a></div>';
                                } else {
                                    html += '<div class="col-md-2 col-symtem" style="padding:5px"><a id="' + data.FieldId + '"  class="btn btn-sm default tooltips" data-container="body" data-placement="top" title="' + data.FieldName + '" data-original-title="' + data.FieldName + '" style="margin: 2px 0;width:100%;padding: 7px 7px;text-align:left;">' +
                                        '<i class="' + data.IconUrl + '"></i>' +
                                        FieldName +
                                        '</a></div>';
                                }
                            }

                    });
                    
                    $('#custom-field-custom').append(html);
                    $('#content-custom-field').css({ 'max-height': $(window).innerHeight() - 200});
                    break;
            }
            
        },
        error: function (r) {
        }
    });
}
    

    function customfield_ShowDelete()
    {
        var del='<a class="del-customfield" style="position: absolute;top: 0;right: 10px;top: 10px;"><i class="fa fa-remove"></i></a>'
        $('#custom-field-custom').on('mouseenter', '.col-md-2:not(.create-custom):not(.col-symtem)', function () {
            console.log('o');
            $(this).append(del);
        }).on('mouseleave', '.col-md-2', function () {
            console.log('a');
            $(this).children('.del-customfield').remove();
        });
    }

    function customfield_ActionDeleteCustomField(apiDomain, access_token, message) {
        $('#custom-field-custom').on('click', '.del-customfield', function () {
            var id = $(this).parent().attr('data-id');
            var r = confirm(message.ConfirmDelete);
            if (r == true) {


                $.ajax({
                    url: apiDomain + 'api/CustomField/Delete?CustomFieldId=' + id,
                    type: 'GET',
                    contentType: 'application/json; charset=utf-8',
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", "Bearer " + access_token);
                    },
                    success: function (r) {
                        switch (parseInt(r.ErrorCode)) {
                            case 0:
                                custom_shownotification('success', message.Success);
                                customfield_LoadCustomField(apiDomain, access_token, message);
                        }
                    },
                    error: function () {
                        custom_shownotification('error', message.Error);
                    }
                });
            }
        });
}

