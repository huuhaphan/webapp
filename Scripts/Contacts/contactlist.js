﻿//các sự kiện
function contactlist_Action(apiDomain, access_token, tableId, ContactGroupId,CampaignId, message) {

    

    //click form manage
    //contactlist_ManageForm(ContactGroupId, CampaignId);

    //sytem customfield 
    contactlist_SymtemCustomField(apiDomain, access_token, tableId, ContactGroupId, message);

    //export
    contactlist_ExportExcel(apiDomain, access_token, tableId, ContactGroupId, message);

    //click form payment 
    contactlist_PaymentForm(ContactGroupId);
    contactlist_Delete(apiDomain, access_token, tableId, message);

    //sau khi import xong đóng modal thì refesh lại table.
    refeshtableAfterImport(tableId);

    //move contact
    contactlist_MoveAll(apiDomain, access_token, message, tableId, ContactGroupId, CampaignId);

    //copy contact
    contactlist_CopyAll(apiDomain, access_token, message, tableId, ContactGroupId);

    //lưu 
    contactlist_Save_MoveCopy(apiDomain, access_token, message, tableId, ContactGroupId, CampaignId);

    //reload
    contactlist_ReloadTable(tableId);

    contactlist_SubmitExport(apiDomain, access_token, ContactGroupId, message);

    var tablerow = $(tableId).DataTable();
    //click tạo mới
    $('#createContact').on('click', function () {
        $(this).attr("href", "/Contacts/AddContact?ContactGroupId=" + ContactGroupId + '&CampaignId=' + CampaignId);
    });

    //lick info trên lưới
    $(tableId + ' tbody').on('click', '#info', function () {
        var rowData = tablerow.row($(this).parents('tr')).data();
        $(this).attr("data-toggle", "modal");
        $(this).attr("data-target", "#modalInfoContact");
        $(this).attr("href", "/Contacts/ContactInfo?cId=" + rowData.contactId);
    });

    //edit click trên lưới
    $(tableId + ' tbody').on('click', '#edit', function () {
        var rowData = tablerow.row($(this).parents('tr')).data();
        console.log(rowData);
        $(this).attr("data-toggle", "modal");
        $(this).attr("data-target", "#modalCreateContact");
        $(this).attr('href', '/Contacts/UpdateContact?ContactGroupId='+ContactGroupId+ '&ContactId=' + encodeURI(rowData.contactId + '&ContactName=' + rowData.contactName + '&Email=' + rowData.email + '&Mobile=' + rowData.mobile + '&Note=' + rowData.note + '&Active=' + rowData.active, "UTF-8"));
    });

    //delete
    var datadelele;
    $(tableId + ' tbody').on('click', '#delete', function () {
        var rowdata = tablerow.row($(this).parents('tr')).data();
        datadelele = rowdata;
        $(this).attr("data-toggle", "modal");
        $(this).attr("href", "#modelConfirmDeleteContact");
    });

    $('#modelConfirmDeleteContact #btnDeleteContact').on('click', function () {
        App.blockUI({
            target: 'body',
            animate: true
        });
        $('#modelConfirmDeleteContact').modal('toggle');

        $.ajax({
            url: apiDomain + 'api/Contact/Delete?ContactId=' + datadelele.contactId,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            success: function (r) {
                var errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
                switch (errorcode) {
                    case 0:
                        App.unblockUI('body');
                        custom_shownotification("success", message.DeleteSuccess);
                        $('#tableContact').dataTable().fnDraw();
                        break;
                }
            },
            error: function (x, s, e) {
                App.unblockUI('body');
                custom_shownotification("error", message.DeleteError);
            },
        });

    });

    var table = $(tableId).DataTable();
    //các sự kiện với checkbox ,select all
    $(tableId + ' tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');
        
        // Get row data
        var data = table.row($row).data();
        
        //tạo list data để move copy;
        var cusfield = data.customFields;
        var CustomFields = [];
        if (cusfield.length > 0)
        {
            $.each(cusfield, function (i, o) {
                var cus = {
                    CustomFieldId: o.customFieldId,
                    CustomFieldValue: o.customFieldValue,
                    CustomFieldName:o.customFieldName,
                };
                CustomFields.push(cus);
            })
        }
        var objdata = {
            ContactId: data.contactId,
            Email: data.email,
            ContactName: data.contactName,
            Mobile: data.mobile,
            CustomFields: CustomFields,
        };

        // Get row ID
        var rowId = custom_GetFisrtProJson(data)

        // Determine whether row ID is in the list of selected row IDs 
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            $(this).parent().addClass('checked')
            rows_selected.push(rowId);
            rows_data_selected.push(objdata);
            console.log(objdata);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            $(this).parent().removeClass('checked')
            rows_selected.splice(index, 1);
            rows_data_selected.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }

        if (rows_selected.length > 0) {
            $('#btn-deleteall-contactlist').removeClass('hide');
            $('#number-select-all').text(rows_selected.length + ' ('+message.Row+')');
            $('#number-select-all').show();
            $('#btn-moveall-contactlist').removeClass('hide');
            $('#btn-copyall-contactlist').removeClass('hide');
        }
        else {
            $('#btn-deleteall-contactlist').addClass('hide');
            $('#btn-moveall-contactlist').addClass('hide');
            $('#btn-copyall-contactlist').addClass('hide');
            
            $('#number-select-all').text('');
            $('#number-select-all').hide();
        }
        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrl(table);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on table cells with checkboxes
    //$('#tableContactGroup').on('click', 'tbody td, thead th:first-child', function (e) {
    //    $(this).parent().find('input[type="checkbox"]').trigger('click');
    //});

    // Handle click on "Select all" control
    $('input[name="select_all"]').on('click', function (e) {
        if (this.checked) {
            $(tableId + ' tbody input[type="checkbox"]:not(:checked)').trigger('click');
            $('#btn-deleteall-contactlist').removeClass('hide');
            $('#btn-moveall-contactlist').removeClass('hide');
            $('#btn-copyall-contactlist').removeClass('hide');
            $('#number-select-all').text(rows_selected.length + ' (dòng)');
            $('#number-select-all').show();
        } else {
            $(tableId + ' tbody input[type="checkbox"]:checked').trigger('click');
            $('#btn-deleteall-contactlist').addClass('hide');
            $('#btn-moveall-contactlist').addClass('hide');
            $('#btn-copyall-contactlist').addClass('hide');

            $('#number-select-all').text('');
            $('#number-select-all').hide();
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    table.on('draw', function () {
        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrl(table);
    });
}


//dùng so sánh maxleng khi tạo , update contact.
function contactlist_SymtemCustomField(apiDomain, access_token, tableId, ContactGroupId, message) {
    $.ajax({
        url: apiDomain + 'api/CustomField/GetSystemList',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            var errorcode = parseInt(r.ErrorCode);
            switch (errorcode) {
                case 0:
                    //lưu xuống localstorage 
                    localStorage.setItem('SytemCustomField', JSON.stringify(r.Data));
                    break;
            }
        }, error: function () {

        }
    });
}

//xóa nhiều contact
function contactlist_Delete(apiDomain, access_token, tableId, message) {
    $('#btn-deleteall-contactlist').on('click', function () {

        if (rows_selected.length > 0) {
            //mở model xác nhận
            $('#modelConfirmDeleteAllContact').modal('toggle');
        }
    });

    $('#btnDeleteAllContact').on('click', function () {
        //App.blockUI({
        //    target: 'body',
        //    animate: true
        //});
        $('#divLoading').show();

        $('#modelConfirmDeleteAllContact').modal('toggle');
        $.ajax({
            url: apiDomain + 'api/Contact/DeleteMany',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            data: JSON.stringify(rows_selected),
            success: function (r) {
                
                //App.unblockUI('body');
                $('#divLoading').hide();
                errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
                switch (errorcode) {
                    case 0:
                        rows_selected = [];
                        rows_data_selected = [];
                        $('#select_all').prop('checked', false);
                        $('#btn-deleteall-contactlist').addClass('hide');
                        $('#btn-moveall-contactlist').addClass('hide');
                        $('#btn-copyall-contactlist').addClass('hide');
                        $(tableId).dataTable().fnDraw();
                        custom_shownotification("success", message.DeleteSuccess);

                        break;
                    default: custom_shownotification("error", message.DeleteError);
                        rows_selected = [];
                        rows_data_selected = [];
                        $('#select_all').prop('checked', false);
                        $('#btn-deleteall-contactlist').addClass('hide');
                        $('#btn-moveall-contactlist').addClass('hide');
                        $('#btn-copyall-contactlist').addClass('hide');
                      
                        $(tableId).dataTable().fnDraw();
                        break;
                }
            },
            error: function (x, s, e) {
                //App.unblockUI('body');
                $('#divLoading').hide();
                custom_shownotification("error", message.DeleteError);
                rows_selected = [];
                rows_data_selected = [];
                $('#select_all').prop('checked', false);
                $('#btn-deleteall-contactlist').addClass('hide');
                $('#btn-moveall-contactlist').addClass('hide');
                $('#btn-copyall-contactlist').addClass('hide');
                $(tableId).dataTable().fnDraw();
            }
        })
    });
}


//load thông tin info lên modal
function contactlist_LoadInfo(apiDomain, access_token, tableId, ContactId, message)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/Contact/GetInfo?ContactId=' + ContactId,
        type: 'GET',
        contentType: 'application/x-www-form-urlencoded',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (result) {
            var data = JSON.parse(JSON.stringify(result.Data))
            $('#InfoContacts #txtContactName').val(data.ContactName);
            $('#InfoContacts #txtPhone').val(data.Mobile);
            $('#InfoContacts #txtEmail').val(data.Email);
            $('#InfoContacts #txtNote').val(data.Note);
            $('#InfoContacts #txtFirstName').val(data.FirstName);
            $('#InfoContacts #txtLastName').val(data.LastName);
            $('#InfoContacts #txtBirthday').val(moment(moment.utc(data.BirthDate).toDate()).format('DD-MM-YYYY'));
            $('#InfoContacts #txtGender').val(data.Gender);
            //$('#InfoContacts #txtIp').val(data.IP);
            //$('#InfoContacts #txtLocation').val(data.Location);
            $('#InfoContacts #txtCreateDate').val(moment(moment.utc(data.CreateDate).toDate()).format('DD-MM-YYYY HH:mm'));
            $('#InfoContacts #txtCreateFrom').val(data.CreateFromSource);

            if (data.CustomFields.length > 0) {
                var html = ''
                $('#info-contact-customfield').children().remove();
                $.each(data.CustomFields, function (i, o) {
                    html += '<div class="form-group form-md-line-input">' +
               '<label class="col-md-3 control-label" for="form_control_1">' +
                  o.CustomFieldName +
               '</label>' +
               '<div class="col-md-9">' +
                   '<input type="text" class="form-control" value="' + o.CustomFieldValue + '"/>' +
               '</div>' +
          ' </div>';
                });
                $('#info-contact-customfield').append(html);
            }
           

            $('#divLoading').hide();
        },
        error: function () {

            $('#divLoading').hide();
            custom_shownotification("error", message.GetInfoError);
        }
    });
}

function refeshtableAfterImport(tableId)
{
    $('#closeTableAfterContact').on('click', function () {
        $(tableId).dataTable().fnDraw();
    })
}

//sự kiện click vào manageform
//function contactlist_ManageForm(ContactGroupId, CampaignId) {
//    $("#btnManageForm").on("click", function () {
//        window.location.href = '/Contacts/ManageForm?gId=' + ContactGroupId;
//    });
//}

//sự kienj click vào thanh toán
function contactlist_PaymentForm(ContactGroupId) {
    $("#btnPayForm").on("click", function () {
        window.location.href = '/Contacts/FormPayment?gId=' + ContactGroupId;
    });
}


//function resizeTableContactList() {
//        var height = parseInt($('.page-content').outerHeight() - $('.page-bar').outerHeight());
//        var heightscroll = height - 150;
//        return heightscroll;
//}

function contactlist_ExportExcel(apiDomain, access_token, tableId, ContactGroupId, message)
{
    $('#btn-export-contact').on('click', function () {
        $('#file-export-name').prop('disabled', false);
        $('#submit-export-group-contact').prop('disabled', false);
        $('#table-file-export').children().remove();
        var name = 'Export ' + moment().format('YYYY-MM-DD HH:mm');
        $('#file-export-name').val(name);
        $('#modal-export-group-contact').modal('toggle');

        //$('#divLoading').show();
        //$.ajax({
        //    url: apiDomain + 'api/Contact/Web/GetList?draw=1&start=0&length=0&SuppressionMail=false&Unsubscribe=false&Active=true&ContactGroupId=' + ContactGroupId,
        //    type: 'GET',
        //    contentType: 'application/json; charset=utf-8',
        //    beforeSend: function (request) {
        //        request.setRequestHeader("Authorization", "Bearer " + access_token);
        //    },
        //    success:function(r)
        //    {
               
        //        if (r.data.length > 0) {
        //            custom_shownotification("success", "Vui lòng chờ, đang export dữ liệu");
        //            var list = [];
        //            var head = ["ContactName", "Email", "Mobile"];
        //            list.push(head);
        //            $.each(r.data, function (i, o) {
        //                var obj = [];
        //                obj.push(o.contactName);
        //                obj.push(o.email);
        //                obj.push(o.mobile);
        //                list.push(obj);
        //            });
                   
        //            write_exportContact(list);
        //        }
        //        else
        //        {
        //            $('#divLoading').hide();
        //            custom_shownotification("error", "Không có dữ liệu");
        //        }
        //    },
        //    error:function()
        //    {
        //        $('#divLoading').hide();
        //        custom_shownotification("error", "Xảy ra lỗi");
        //    }
        //})
    })
}

function contactlist_SubmitExport(apiDomain, access_token, ContactGroupId, message) {
    $('#submit-export-group-contact').on('click', function () {
       
        $('#table-file-export').children().remove();
        if ($('#file-export-name').val()) {

            var html = '<table style="width:100%;" class="table table-striped">' +
                                   ' <thead>' +
                                       ' <tr>' +
                                       '   <td>' +
                                           message.FileName +
                                           ' </td>' +
                                          '  <td>' +
                                           message.Status +
                                                    '</td>' +
                                                    '<td>' +
                                                     message.CreateDate +
                                                  '  </td>' +
                                             '   </tr>' +
                                           '  </thead>' +
                                          '  <tbody>' +
                                              '  <tr>' +
                                                  '  <td>' +
                                                          $('#file-export-name').val() +
                                                  '  </td>' +
                                                 '   <td id="status-export">' +
                                                      '<div><i class="fa fa-spinner fa-pulse fa-fw"></i> ' + message.Processing + ' </div>' +
                                                   ' </td>' +
                                                 '   <td>' +
                                                     moment().format('YYYY-MM-DD HH:mm') +
                                                   ' </td>' +
                                              '  </tr>' +
                                           ' </tbody>' +
                                      '  </table>';
            $('#table-file-export').append(html);
            $('#file-export-name').prop('disabled', true);
            $('#submit-export-group-contact').prop('disabled', true);
            contactlist_CreateExport(apiDomain, access_token, ContactGroupId, message);


        }
        else {
            custom_shownotification("error", message.FileNameRequire);
        }
    })
}

var checkfileexport;
function contactlist_CreateExport(apiDomain, access_token, ContactGroupId, message) {
   
    var ContactGroupIds=[];
    ContactGroupIds.push(ContactGroupId);
    var FileName = $('#file-export-name').val();
    var data = {
        FileType: 'xlsx',
        FileName: FileName.replace(/\s/g, "-"),
        ContactGroupIds: ContactGroupIds,
    }
    $.ajax({
        url: apiDomain + 'api/report/CreateContactExport',
        type: 'post',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);

        },
        success: function (r) {

            switch (parseInt(r.ErrorCode)) {

                case 0:
                    {
                        var data = r.Data;
                        var ExportId = data.ExportId;
                        switch (parseInt(data.Status)) {
                            case 0: //pendding
                                checkfileexport = setInterval(function () { contactlist_CheckExportComplete(apiDomain, access_token, ExportId, message); }, 3000);
                                break;
                            case 1:
                                var html = '<div><a href=' + data.FilePath + '><i class="fa fa-cloud-download font-green-jungle"></i> '+message.DownLoad+'</div>';
                                setTimeout(function () {
                                    $('#status-export').children().remove();
                                    $('#status-export').append(html)},3000);
                                break;
                            case 2:
                                var html = '<div><i class="fa fa-remove font-red"></i> '+message.ExportError+' </div>';
                                $('#status-export').children().remove();
                                $('#status-export').append(html);
                                break;

                        }
                    }
                    break;
                default:
                    custom_shownotification("error", message.ExportError);
                    break;


            }
        },
        error: function (x, s, e) {
            custom_shownotification("error", message.ExportError);
        }
    })
}

function contactlist_CheckExportComplete(apiDomain, access_token, ExportId, message) {
    $.ajax({
        url: apiDomain + 'api/report/GetContactExport?ExportId=' + ExportId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);

        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    console.log(data);
                    switch (parseInt(data.Status)) {
                        case 1:
                            var html = '</div><a href=' + data.FilePath + '><i class="fa fa-cloud-download font-green-jungle"></i>'+message.DownLoad+' </a></div>';
                            setTimeout(function () {
                                $('#status-export').children().remove();
                                $('#status-export').append(html);
                            }, 3000);
                            clearInterval(checkfileexport);
                            break;
                        case 2:
                            var html = '<div><i class="fa fa-remove font-red"></i>'+message.ExportError+'</div>';
                            $('#status-export').children().remove()
                            $('#status-export').append(html);
                            clearInterval(checkfileexport);
                            break;

                    }
                    break;

            }
        },
        error: function (x, s, e) {
            custom_shownotification("error",'Có lỗi xảy ra');
            clearInterval(checkfileexport);
        }
    })
}



var CopyMove = -1; //1 copy //0 move

function contactlist_MoveAll(apiDomain, access_token, message, tableId, ContactGroupId, CampaignId)
{
    $('#btn-moveall-contactlist').on('click', function () {
        CopyMove = 0;
        $('#total-copy-move').text(rows_data_selected.length);
        $('#modal-copy-move-contact .modal-title').text(message.MoveContact);
        $('#modal-copy-move-contact #copy-move-to').text(message.MoveTo);
        contactlist_LoadContactGroup(apiDomain, access_token, message, tableId, ContactGroupId, CampaignId);
        $('#modal-copy-move-contact').modal('toggle');
  
    });
}

function contactlist_CopyAll(apiDomain, access_token, message, tableId, ContactGroupId, CampaignId)
{
    $('#btn-copyall-contactlist').on('click', function () {
        CopyMove = 1;
        $('#total-copy-move').text(rows_data_selected.length);
        $('#modal-copy-move-contact .modal-title').text(message.CopyContact);
        $('#modal-copy-move-contact #copy-move-to').text(message.CopyTo);
        contactlist_LoadContactGroup(apiDomain, access_token, message, tableId, ContactGroupId, CampaignId);
        $('#modal-copy-move-contact').modal('toggle');
        
    });
}

function contactlist_Save_MoveCopy(apiDomain, access_token, message, tableId, ContactGroupId, CampaignId)
{
    $('#save-copy-move-contact').on('click', function () {
        if(CopyMove==1)
        {
            contactlist_Copy(apiDomain, access_token, CampaignId,ContactGroupId, message, tableId);
        }
        else if(CopyMove==0)
        {
            contactlist_Move(apiDomain, access_token, message, tableId, CampaignId, ContactGroupId);
        }

    })
}

function contactlist_Copy(apiDomain, access_token, CampaignId, ContactGroupId, message, tableId)
{
    var cId = $('#select-group-move-copy').select2('data')[0].campaignId;
        if (cId == '' || cId == null || cId == 'null') {
            cId = '';
        }
        var data = {
            FromContactGroupId: ContactGroupId,
            CopyToContactGroupId: $('#select-group-move-copy').val(),
            CampaignId: cId,
            ContactList: rows_data_selected,
        }
        console.log(data);
        console.log($('#select-group-move-copy').select2('data')[0])
        $('#divLoading').show();
        $.ajax({
            url: apiDomain + 'api/Contact/Copy',
            type: 'Post',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data),
            beforeSend: function (request) {
                request.setRequestHeader('Authorization', "Bearer " + access_token);

            },
            success: function (r) {
                switch (parseInt(r.ErrorCode)) {
                    case 0:
                        custom_shownotification("success", message.CopySuccess);
                        contactlist_ResetSelectAll(tableId);
                       
                        break;
                    default:
                        custom_shownotification("error", message.CopyError);
                        contactlist_ResetSelectAll(tableId);
                }
                $('#divLoading').hide();
            },
            error: function (x, s, e) {
                custom_shownotification("error", message.CopyError);
                contactlist_ResetSelectAll(tableId);
                $('#divLoading').hide();
            }
        });



    //var iderror=[];
    //$.each(rows_data_selected, function (i, o) {
    //    console.log('i: ' + i);
    //    if (o.ContactName == "" || o.ContactName == null) {
    //        o.ContactName = o.Email.substring(0, o.Email.indexOf('@'));

    //    }

    //    var cId = $('#select-group-move-copy').select2('data')[0].campaignId;
    //    console.log(cId);
    //    if (cId == '' || cId == null || cId == 'null') {
    //        cId = '';
    //    }
    //    var data = {
    //        ContactGroupId: $('#select-group-move-copy').val(),
    //        CampaignId: cId,
    //        ContactName: o.ContactName,
    //        Email: o.Email,
    //        Mobile: o.Mobile,
    //        Note: '',
    //        Active: true,

    //    };
    //    $('#divLoading').show();
    //    $.ajax({
    //        url: apiDomain + 'api/Contact/Create',
    //        type: 'POST',
    //        data: data,
    //        contentType: 'application/x-www-form-urlencoded',
    //        beforeSend: function (request) {
    //            request.setRequestHeader("Authorization", "Bearer " + access_token);
    //        },
           
    //        success: function (r) {
    //            var errorcode = parseInt(r.ErrorCode);
    //            $('#divLoading').hide();
    //            switch (errorcode) {
    //                case 0:
    //                    {
    //                        var len = rows_data_selected.length - 1;
    //                        if (i == len) {
    //                            setTimeout(function () {

    //                                console.log('s: ' + iderror.length);
    //                                if (iderror.length > 0) {
    //                                    custom_shownotification("success", 'Đã copy xong. Lỗi copy : ' + iderror.length + '/' + rows_data_selected.length + 'liên hệ');
    //                                    contactlist_ResetSelectAll(tableId);
    //                                }
    //                                else {
    //                                    custom_shownotification("success", message.CopySuccess);
    //                                    contactlist_ResetSelectAll(tableId);
    //                                }
    //                                $('#divLoading').hide();

    //                            }, 1000);
    //                        }
    //                    }
    //                    break;
    //                default:
    //                    {
    //                        iderror.push(o.ContactId);
    //                        console.log(iderror);
    //                        var leng = rows_data_selected.length - 1;
    //                        if (i == leng) {
    //                            setTimeout(function () {

    //                                if (iderror.length > 0) {

    //                                    custom_shownotification("error", 'Đã copy xong. Lỗi copy : ' + iderror.length + '/' + rows_data_selected.length + ' liên hệ');
    //                                    contactlist_ResetSelectAll(tableId);
    //                                    console.log('end');
    //                                }
    //                                else {
    //                                    custom_shownotification("success", message.CopySuccess);
    //                                    contactlist_ResetSelectAll(tableId);
    //                                }
    //                                $('#divLoading').hide();
    //                            }, 1000);
    //                        }
    //                    }
    //                    break;
    //            }
    //        },
    //        error: function (x, s, e) {
    //            iderror.push(o.ContactId);
    //            console.log('e: ' + iderror.length);
    //            var lenght = rows_data_selected.length - 1;
                
    //            if (i == lenght) {
    //                setTimeout(function () {
    //                    if (iderror.length > 0) {
    //                        custom_shownotification("error", 'Đã copy xong. Lỗi copy : ' + iderror.length + '/' + rows_data_selected.length + ' liên hệ');
    //                        contactlist_ResetSelectAll(tableId);
    //                    }
    //                    else {
    //                        custom_shownotification("success", message.CopySuccess);
    //                        contactlist_ResetSelectAll(tableId);
    //                    }
    //                    $('#divLoading').hide();
    //                }
    //            , 1000);
    //            }
    //        },
    //    });
    //});


   
}


function contactlist_Move(apiDomain, access_token, message, tableId, CampaignId, ContactGroupId) {

    var cId = $('#select-group-move-copy').select2('data')[0].campaignId;
    console.log(cId);
    if (cId == '' || cId == null || cId == 'null') {
        cId = '';
    }
    var data = {
        FromContactGroupId: ContactGroupId,
        CopyToContactGroupId: $('#select-group-move-copy').val(),
        CampaignId: cId,
        ContactList: rows_data_selected,
    }
    console.log(data);
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/Contact/Move',
        type: 'Post',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', "Bearer " + access_token);

        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    custom_shownotification("success", message.MoveSuccess);
                    contactlist_ResetSelectAll(tableId);
                    break;
                default:
                    custom_shownotification("error", message.MoveError);
                    contactlist_ResetSelectAll(tableId);
            }
            $('#divLoading').hide();
        },
        error: function (x, s, e) {
            custom_shownotification("error", message.MoveError);
            contactlist_ResetSelectAll(tableId);
            $('#divLoading').hide();
        }
    });

    //var iderror = [];
    //$.each(rows_data_selected, function (i, o) {
    //    if (o.ContactName == "" || o.ContactName == null) {
    //        o.ContactName = o.Email.substring(0, o.Email.indexOf('@'));

    //    }
    //    var cId = $('#select-group-move-copy').select2('data')[0].campaignId;
    //    console.log(cId);
    //    if (cId == '' || cId == null || cId == 'null') {
    //        cId = '';
    //    }
    //    var data = {
    //        ContactGroupId: $('#select-group-move-copy').val(),
    //        CampaignId: cId,
    //        ContactName: o.ContactName,
    //        Email: o.Email,
    //        Mobile: o.Mobile,
    //        Note: '',
    //        Active: true,

    //    };
    //    $('#divLoading').show();
    //    $.ajax({
    //        url: apiDomain + 'api/Contact/Create',
    //        type: 'POST',
    //        data: data,
    //        contentType: 'application/x-www-form-urlencoded',
    //        beforeSend: function (request) {
    //            request.setRequestHeader("Authorization", "Bearer " + access_token);
    //        },
    //        success: function (r) {
    //            var errorcode = parseInt(r.ErrorCode);
    //            $('#divLoading').hide();
    //            switch (errorcode) {
    //                case 0:
    //                case 102:
    //                    {
    //                        var len = rows_data_selected.length - 1;

    //                        $.ajax({
    //                            url: apiDomain + 'api/Contact/Delete?ContactId=' + o.ContactId,
    //                            type: 'GET',
    //                            contentType: 'application/json; charset=utf-8',
    //                            beforeSend: function (request) {
    //                                request.setRequestHeader("Authorization", "Bearer " + access_token);
    //                            },
    //                            success: function (r) {
    //                                $('#divLoading').hide();
    //                                errorcode = parseInt(r.ErrorCode);
    //                                switch (errorcode) {
    //                                    case 0:
    //                                        if (i == len) {
    //                                            if (iderror.length > 0) {
    //                                                custom_shownotification("error", 'Di chuyển xong. Lỗi di chuyển ' + iderror.length + '/' + rows_data_selected.length + ' liên hệ');
    //                                                contactlist_ResetSelectAll(tableId);
    //                                            }
    //                                            else {
    //                                                custom_shownotification("success", message.MoveSuccess);
    //                                                contactlist_ResetSelectAll(tableId);
    //                                            }
    //                                            $('#divLoading').hide();
    //                                        }
    //                                        break;
    //                                    default:
    //                                        iderror.push(o.ContactId);
    //                                        if (i == len) {
    //                                            if (iderror.length > 0) {
    //                                                custom_shownotification("error", 'Di chuyển xong. Lỗi di chuyển ' + iderror.length + '/' + rows_data_selected.length + ' liên hệ');
    //                                                contactlist_ResetSelectAll(tableId);
    //                                            }
    //                                            else {
    //                                                custom_shownotification("success", message.MoveSuccess);
    //                                                contactlist_ResetSelectAll(tableId);
    //                                            }
    //                                            $('#divLoading').hide();
    //                                        }
    //                                        break;
    //                                }
    //                            },
    //                            error: function (x, s, e) {

    //                                iderror.push(o.ContactId);
    //                                if (i == len) {
    //                                    if (iderror.length > 0) {
    //                                        custom_shownotification("error", 'Di chuyển xong. Lỗi di chuyển ' + iderror.length + '/' + rows_data_selected.length + ' liên hệ');
    //                                        contactlist_ResetSelectAll(tableId);
    //                                    }
    //                                    else {
    //                                        custom_shownotification("success", message.MoveSuccess);
    //                                        contactlist_ResetSelectAll(tableId);
    //                                    }
    //                                    $('#divLoading').hide();
    //                                }
    //                            }
    //                        })

    //                    }
    //                    break;


    //                default:
    //                    {
    //                        iderror.push(o.ContactId);
    //                        var len = rows_data_selected.length - 1;
    //                        if (i == len) {
    //                            if (iderror.length > 0) {
    //                                custom_shownotification("error", 'Di chuyển xong. Lỗi di chuyển ' + iderror.length + '/' + rows_data_selected.length + ' liên hệ');
    //                                contactlist_ResetSelectAll(tableId);
    //                            }
    //                            else {
    //                                custom_shownotification("success", message.MoveSuccess);
    //                                contactlist_ResetSelectAll(tableId);
    //                            }
    //                            $('#divLoading').hide();

    //                        }
    //                    }
    //                    break;

    //            }

    //        },
    //        error: function (x, s, e) {
    //            iderror.push(o.ContactId);
    //            var len = rows_data_selected.length - 1;
    //            if (i == len) {
    //                $('#divLoading').hide();
    //                if (iderror.length > 0) {
    //                    custom_shownotification("error", 'Di chuyển xong. Lỗi di chuyển ' + iderror.length + '/' + rows_data_selected.length + ' liên hệ');
    //                    contactlist_ResetSelectAll(tableId);
    //                }
    //                else {
    //                    custom_shownotification("success", message.MoveSuccess);
    //                    contactlist_ResetSelectAll(tableId);
    //                }
    //            }
    //        },
    //    });
    //})


}



function contactlist_LoadContactGroup(apiDomain, access_token, message, tableId, ContactGroupId, CampaignId)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/ContactGroup/Web/GetList?start=0&draw=1&length=1000000',
        contenType: 'application/x-www-form-urlencoded',
        type: 'GET',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
           // $('#div-select-group-move-copy').children().remove();
            var data = r.data;
            var sdata = [];
            if (r.recordsTotal > 1) {
                //var option = "<option value=-1>" + message.SelectContactGroup + "</option>";
                $.each(data, function (index, obj) {
                    if (obj.contactGroupId != ContactGroupId) {
                        var odata = { id: obj.contactGroupId, text: obj.contactGroupName, campaignId: obj.campaignId }
                        sdata.push(odata);
                        //  option += "<option value=" + obj.contactGroupId + ">" + obj.contactGroupName + "</option>";

                    }
                });
                console.log(sdata);
                // $('<select id="select-group-move-copy" class="bs-select form-control" data-show-subtext="true" data-style="#EEF1F5"></select>').append(option).appendTo('#div-select-group-move-copy');

            };
            $('#select-group-move-copy').children().remove();
            $('#select-group-move-copy').select2({ data: sdata });
            $('.select2.select2-container.select2-container--bootstrap').css({ 'width': '100% !important' });
            
            //else {
            //    var option = "<option value=-1>" + message.NotHaveContactGroup + "</option>";
            //   // $('<select id="select-group-move-copy" class="bs-select form-control" data-show-subtext="true" data-style="#EEF1F5"></select>').append(option).appendTo('#div-select-group-move-copy');
            //    $('#select-group-move-copy').append(option);
            //}
            //$('#select-group-move-copy').selectpicker('render');
            //$('#select-group-move-copy').select2({ dropdownAutoWidth: 'true' });
            $('#divLoading').hide();

        },
        error: function (x, s, e) {
            custom_shownotification("success", message.HasErrorLoadContact);
            $('#divLoading').hide();
        }
    })
}

function contactlist_ResetSelectAll(tableId)
{
    rows_selected = [];
    rows_data_selected = [];
    $('#select_all').prop('checked', false);
    $('#btn-deleteall-contactlist').addClass('hide');
    $('#btn-moveall-contactlist').addClass('hide');
    $('#btn-copyall-contactlist').addClass('hide');
    $('#modal-copy-move-contact').modal('toggle');
    $(tableId).dataTable().fnDraw();
}



function contactlist_ReloadTable(tableId)
{
    $('#btn-reload-table-contactlist').on('click', function () {
        $(tableId).dataTable().fnDraw();
    })
}
