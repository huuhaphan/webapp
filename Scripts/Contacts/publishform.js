﻿
function publishform_Action(formId, form64, width, height, key)
{
    publishform_ChangeType(formId, form64, width, height, key);
    CopyCode();
    //CopyLink();
}

//change radio button chọn kiểu publish
function publishform_ChangeType(formId, form64, width, height, key)
{
 
    InsertHtmlCode(formId, form64, width, height);

   // InsertHtmlCodeIframe(formId, form64, width, height);
    
    InsertWebLink(formId, width, height, key);

    InsertWebPopup(form64, width, height);
}

function publishform_HideHtmlCode()
{
    //if(!$('#html-code').hasClass('display-hide'))
    //{
    //    $('#html-code').addClass('display-hide')
    //}
}

function publishform_HideWebLink() {
    //if (!$('#web-form-link').hasClass('display-hide')) {
    //    $('#web-form-link').addClass('display-hide')
    //}
}

function InsertHtmlCode(formId, form64, width, height) {

    var htmlcode = '<script class="script-webform-toomarketer" id="script-webform-toomarketer" src="https://forms.toomarketer.com/scripts/toomarketer-web-form-nonejs.v2.js?fId=' + form64 + '"></script>';
   
    $('#htlmcode').val(htmlcode);
}




function InsertHtmlCodeIframe(formId, form64, iframe_width, iframe_height) {
   
    var htmlcode = '<div style="margin:0 auto;max-width:460px;width:100%;"><iframe src="https://forms.toomarketer.com/Form/ViewForm?fId=' + formId + '" scrolling="no" style="max-width: 460px;width: 100%;margin:0 auto;height: ' + iframe_height + 'px;"></iframe></div>';

    $('#htlmcodeiframe').val(htmlcode);
}

function InsertWebLink(formId, width, height, key)
{
    var link = 'https://forms.toomarketer.com/Form/ViewForm?fId=' + formId;
    $('#webformlink').val(link);
    var data = {
        longUrl: link,
    }
    $.ajax({
        url: "https://www.googleapis.com/urlshortener/v1/url?key="+key,
        type: 'post',
        data:JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        success: function (r) {
            $('#webformlinkshort').val(r.id);
        }
    });

}

function InsertWebPopup(form64, width, height)
{
    var htmlcode = '<script class="script-webpopup-toomarketer" id="script-webpopup-toomarketer" src="http://forms.toomarketer.com/scripts/Webform-Popup-nonejs.js?fId=' + form64 + '"></script>';
    $('#webformpopup').val(htmlcode);
}


function CopyCode()
{
    document.getElementById("copy-code").addEventListener("click", function () {
        copyToClipboard(document.getElementById("htlmcode"));
        document.getElementById("htlmcode").select();
    });

    document.getElementById("copy-link").addEventListener("click", function () {
        copyToClipboard(document.getElementById("webformlink"));
        document.getElementById("webformlink").select();
    });

    document.getElementById("copy-shortlink").addEventListener("click", function () {
        copyToClipboard(document.getElementById("webformlinkshort"));
        document.getElementById("webformlinkshort").select();
    });

    document.getElementById("copy-popup").addEventListener("click", function () {
        copyToClipboard(document.getElementById("webformpopup"));
        document.getElementById("webformpopup").select();
    });
}



function copyToClipboard(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch (e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }

    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    custom_shownotification('success', 'Copied');
    return succeed;
}