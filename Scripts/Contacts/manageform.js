﻿function manageform_Action(apiDomain, access_token, ContactGroupId, tableId, message,eId, gName)
{
    //tạo form mới
    //manageform_CreateForm(ContactGroupId);
    //change publish
    manageform_ChangeStatepublish(apiDomain, access_token, tableId, message)

    //select checkbox
    manageform_SelectCheckbox(tableId);
    
    //sự kiện edit form
    manageform_UpdateForm(apiDomain, access_token, tableId, eId, gName);

    //sự kiện edit custom field form
    manageform_UpdateCustomFieldForm(apiDomain, access_token, tableId, eId, gName);

    //sự kiện preview form
    manageform_Preview(apiDomain, access_token, tableId, ContactGroupId, eId, gName);

    //sự kiện quay lại 
    //manageform_Previous(ContactGroupId);

    //resize porlet
    resizeTableManageForm();

    //formhover(tableId)

    //delete form
    form_Delete(apiDomain, access_token, tableId, message)

    //xóa trên table
    manageform_DeleteForm(apiDomain, access_token, tableId, message)
}

//sự kiện click tạo form mới
//function manageform_CreateForm(ContactGroupId)
//{
//    //chuyển đến trang tạo mới form
//    $('#btnCreateForm').on('click', function () {
//        $('#divLoading').show();
//        window.location.href = '/Contacts/FormSetting?gId=' + ContactGroupId +'&fId=-1';
//    });
   
//}

//sự kiện click quay lại 
//function manageform_Previous(ContactGroupId)
//{
//    $('#btn-manageform-previous').on('click', function () {
//        window.location.href = '/Contacts/ContactList?gId=' + ContactGroupId;
//    });
//}



function manageform_ChangeStatepublish(apiDomain, access_token, tableId, message)
{
    var tablerow = $(tableId).DataTable();

    //edit click
    $(tableId + ' tbody').on('switchChange.bootstrapSwitch','#switchpublish', function(event, state) {
        var rowData = tablerow.row($(this).parents('tr')).data();
        var data = {
            FormId: rowData.formId,
            FormName:rowData.formName,
            AfterSubscriptionAction: rowData.afterSubscriptionAction,
            AlreadySubscribeAction: rowData.alreadySubscribeAction,
            Active: state
        }
        $.ajax({
            url: apiDomain + 'api/Form/Update',
            type: 'Post',
            contentType: 'application/x-www-form-urlencoded',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            data:data,
            success: function (r) {
                var errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
                switch (errorcode) {
                    case 0:
                        custom_shownotification("success", message.UpdateSuccess);
                       
                        break;
                    default:
                        custom_shownotification("error", message.UpdateError);
                        break;

                }
            },
            error:function(x,s,e)
            {
                custom_shownotification("error", message.UpdateError);
            }
        });
        
    });
}



//sự kiện edit form
function manageform_UpdateForm(apiDomain, access_token, tableId, eId, gName) {
    var tablerow = $(tableId).DataTable();
    //edit click
    $(tableId + ' tbody').on('click', '#edit', function () {
        var rowData = tablerow.row($(this).parents('tr')).data();
      //  window.location.href = "/Contacts/FormSetting?gId=" + encodeURI(rowData.contactGroupId + '&eId=' + eId + '&gName=' + gName + '&fId=' + rowData.formId, "UTF-8");
        window.location.href = "/Contacts/Form?gId=" + encodeURI(rowData.contactGroupId + '&eId=' + eId + '&gName=' + gName + '&fId=' + rowData.formId, "UTF-8");

    });
}


function manageform_Preview(apiDomain, access_token, tableId, ContactGroupId, eId, gName) {
    var tablerow = $(tableId).DataTable();
    //preview click
    $(tableId + ' tbody').on('click', '#preview', function () {
        var rowData = tablerow.row($(this).parents('tr')).data();
        window.location.href = '/Contacts/PublishForm?gId=' + encodeURI(ContactGroupId + '&eId=' + eId + '&gName=' + gName + '&fId=' + rowData.formId, "UTF-8");
        w.focus();
    });
}

//sự kiện eidit custom field
function manageform_UpdateCustomFieldForm(apiDomain, access_token, tableId,eId,gName) {
    var tablerow = $(tableId).DataTable();
    //edit click
    $(tableId + ' tbody').on('click', '#editcustomfield', function () {
        var rowData = tablerow.row($(this).parents('tr')).data();
        window.location.href = "/Contacts/Form?gId=" + encodeURI(rowData.contactGroupId + '&eId=' + eId + '&gName=' + gName + '&fId=' + rowData.formId, "UTF-8");

    });
}

//sự kiện delete
function manageform_DeleteForm(apiDomain, access_token, tableId, message) {
    var tablerow = $(tableId).DataTable();
    //edit click
    $(tableId + ' tbody').on('click', '#delete', function () {
        var rowData = tablerow.row($(this).parents('tr')).data();
        var data = new Array()
          data.push(rowData.formId);
        $('#confirmdelete-form').modal('toggle');

        $('#confirmDelete-form').on('click', function () {

            $('#confirmdelete-form').modal('toggle');
            form_ActionDelete(apiDomain, access_token, tableId, data, message);
        })

    });
}

//xử lí với  check box trên lưới
function manageform_SelectCheckbox(tableId)
{
    var table = $(tableId).DataTable();
    //các sự kiện với checkbox ,select all
    $(tableId + ' tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');

        // Get row data
        var data = table.row($row).data();

        // Get row ID
        var rowId = custom_GetFisrtProJson(data)

        // Determine whether row ID is in the list of selected row IDs 
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            $(this).parent().addClass('checked')
            rows_selected.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            $(this).parent().removeClass('checked')
            rows_selected.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }

        //show delete all
        if (rows_selected.length > 0) {
            $('#btn-deleteall-form').removeClass('hide');
        }
        else {
            $('#btn-deleteall-form').addClass('hide');
        }

        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrl(table);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on "Select all" control
    $('input[name="select_all"]').on('click', function (e) {
        if (this.checked) {
            $(tableId + ' tbody input[type="checkbox"]:not(:checked):not(#switchpublish)').trigger('click');
            //show deleteall
            $('#btn-deleteall-form').removeClass('hide');
        } else {
            $(tableId + ' tbody input[type="checkbox"]:checked:not(#switchpublish)').trigger('click');
            //show deleteall
            $('#btn-deleteall-form').addClass('hide');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    table.on('draw', function () {
        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrl(table);
    });
}

function resizeTableManageForm()
{
    //var height = parseInt($('.page-content').outerHeight() - $('.page-bar').outerHeight());
    //$('#porlet-manage-form').css('height', height-10);
   
}

function manageform_HeightScorllManageForm()
{
    var height = parseInt($('.page-content').outerHeight() - $('.page-bar').outerHeight());
    var heightscroll = height -150;
    return heightscroll;
}

function formhover(tableId) {
    $(tableId+' tbody').on('mouseover', 'tr', function () {
        var tablerow = $(tableId).DataTable();
        var rowData = tablerow.row($(this)).data();
        $(this).children().find('#icon-edit-form').removeClass('hide');


    }).on('mouseout', 'tr', function () {
        $(this).children().find('#icon-edit-form').addClass('hide');

    });
}

//xóa nhiều contactgroup
function form_Delete(apiDomain, access_token, tableId, message) {
    $('#btn-deleteall-form').on('click', function () {
        //mở model xác nhận
        $('#confirmdeleteall-form').modal('toggle');
    });

    $('#confirmDeleteAll-form').on('click', function () {
        $('#divLoading').show();
        //App.blockUI({
        //    target: 'body',
        //    animate: true
        //});
        $('#confirmdeleteall-form').modal('toggle');
        var data = rows_selected;
        form_ActionDelete(apiDomain, access_token,tableId, data, message);
    });
}

function form_ActionDelete(apiDomain, access_token, tableId, data, message)
{
    $.ajax({
        url: apiDomain + '/api/Form/DeleteMany',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        data: JSON.stringify(data),
        success: function (r) {
            App.unblockUI('body');
            $('#divLoading').hide();
            errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
            switch (errorcode) {
                case 0:
                    custom_shownotification("success", message.DeleteSuccess);
                    rows_selected = [];
                    $('#btn-deleteall-form').addClass('hide');
                    $(tableId).dataTable().fnDraw();

                    break;
                default: custom_shownotification("error", message.DeleteError);
                    break;
            }
        },
        error: function (x, s, e) {
            // App.unblockUI('body');
            $('#divLoading').hide();

            custom_shownotification("error", message.DeleteError);
        }
    });
}
