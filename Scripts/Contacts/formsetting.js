﻿
//ham các sự kiện
function formsetting_Action(apiDomain, access_token, ContactGroupId, FormId, eId, gName, message, userinfo, permission)
{

    formsetting_LoadForm(apiDomain, access_token, FormId, message, userinfo, permission);

    //hàm xử lí ẩn hiện text box chọn custom thankpage
    GetValueThank();

    //hàm xử lý ẩn hiện text box chọn ready page
    GetValueReady();

    //hàm xử lý ẩn hiên check box chọn gửi 
    GetValueNotify(userinfo);
    

    //hàm xử lý ẩn hiện chọn email sms 
    //GetEmailMobileNotify();

    //hàm xử lí tạo một form mới.
    formsetting_CreateForm(apiDomain, access_token, ContactGroupId, FormId, eId, gName, message);

    //hàm xử lí quay lại
    //formsetting_PreviousAction(ContactGroupId);

    //xác nhận tiếp tục cập nhập customfield
    formsetting_RedirectUpdateCustomField(ContactGroupId);

    formsetting_ModalUpdateForm(apiDomain, access_token, ContactGroupId, FormId, eId, gName, message);

    formsetting_SettingForm(apiDomain, access_token, ContactGroupId, FormId, eId, gName, message, userinfo, permission)

    formsetting_LoadModalUpdateForm(apiDomain, access_token, ContactGroupId, FormId, eId, gName, message, userinfo, permission);

}


//ẩn hiện textbox nhập địa chỉ thankpage
function GetValueThank() {

    $('#formSetting input[name=thankpage]').on('change', function () {
        var value = $('input[name=thankpage]:checked', '#formSetting').val();
        console.log(value);
        if(value==3)
        {
            $('#urlThankpage').css({'display':''})
        }
        else {
            $('#urlThankpage').css({ 'display': 'none' })
        }
    });
}

//ẩn hiện textbox nhập địa chỉ readypage
function GetValueReady() {

    $('#formSetting input[name=readypage]').on('change', function () {
        var value = $('input[name=readypage]:checked', '#formSetting').val();
        console.log(value);
        if (value == 3) {
            $('#urlReadypage').css({ 'display': '' })
        }
        else {
            $('#urlReadypage').css({ 'display': 'none' })
        }
    });
}



function GetValueNotify(userinfo) {
    //tạm set cứng người nhận là user đang dùng 
    $('#user-receive-notify').attr('src', userinfo.Avartar);
    $('#user-receive-notify').attr('data-userid',userinfo.UserId)
    $('#user-receive-notify').attr('title', userinfo.Email+' '+userinfo.Mobile);




    $('#formSetting input[name=notify-register]').on('change', function () {
        formsetting_SetNotify();
    });

    $('#formSetting input[name=typesendnotify]').on('change', function () {
        formsetting_SetTypeNotify();
    });


}

function formsetting_SetNotify()
{
    var value = $('input[name=notify-register]:checked', '#formSetting').val();
    console.log(value);
    if (value == 2) {
        $('#info-notify-send').show();
        formsetting_SetTypeNotify();

    }
    else {
        $('#info-notify-send').hide();
    }
}

function formsetting_SetTypeNotify()
{
    if ($('input#sendemail').prop('checked') == true || $('input#sendsms').prop('checked') == true) {
        $('#div-list-user').show();
    }
    else {
        $('#div-list-user').hide();
    }
    if ($('input#send-notify').prop('checked') == true) {
        $('#div-list-device').show();
    }
    else {
        $('#div-list-device').hide();
    }
}


//function GetEmailMobileNotify() {
//    formsetting_EmailMobileNotify();
//    $('#formSetting input[name=typesendnotify]').on('change', function () {
//        formsetting_EmailMobileNotify();
//    });
//}

//function formsetting_EmailMobileNotify()
//{
//    var value = parseInt($('input[name=typesendnotify]:checked', '#formSetting').val());

//    if (value == 0) {
//        $('#selectemailnotify').show();
//        $('#selectmobilenotify').hide();
//    }
//    if (value == 1) {
//        $('#selectmobilenotify').show();
//        $('#selectemailnotify').hide();
//    }
//    else if (value == 2){
//        $('#selectemailnotify').show();
//        $('#selectmobilenotify').show();
//    }
//}

//test
var list_user_notify = [];
function formsetting_GetListRecipient(apiDomain, access_token, userinfo, FormId, ListUser, message, permission)
{


    $('divLoading').show();
    $.ajax({
        url: apiDomain + 'api/User/GetSubUserList',
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    $('divLoading').hide();
                    var listusersub = [];
                    if (permission.IsParent ||permission.IsParent=='True') {
                        listusersub.push(userinfo);
                    }
                    if (r.Data.length > 0) {
                        $.each(r.Data, function (i, o) {
                            if (o.MobilePhone != '') {
                                var user = {
                                    Email: o.Email,
                                    Mobile: o.MobilePhone,
                                    UserId: o.UserId,
                                }
                                listusersub.push(user);
                            }
                        });


                    }

                    formsetting_GenListSelect(listusersub, ListUser, message)
                    break;
                default:
                    $('divLoading').hide();
                    custom_shownotification("error", 'Có lỗi xảy ra');
                    break;
            }

        }, error: function (x, s, e) {
            $('divLoading').hide();
            custom_shownotification("error", 'Có lỗi xảy ra');
        }
    })

  
}

list_device_notify = [];
function formsetting_GetListDevice(apiDomain, access_token,ListDevice, message) {
    $('divLoading').show();
    $.ajax({
        url: apiDomain + 'api/User/GetDeviceList?IsConfirm=true',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    $('divLoading').hide();
                    var listdevice = [];
                    if (r.Data.length > 0) {
                        $.each(r.Data, function (i, o) {
                            var device = {
                                ItemId: o.ItemId,
                                DeviceModel: o.DeviceModel,
                                Mobile: o.MobileNumber,
                            };
                            listdevice.push(device);
                        });


                    }

                    formsetting_GenDeviceSelect(listdevice, ListDevice, message)
                    break;
                default:
                    $('divLoading').hide();
                    custom_shownotification("error", 'Có lỗi xảy ra');
                    break;
            }

        }, error: function (x, s, e) {
            $('divLoading').hide();
            custom_shownotification("error", 'Có lỗi xảy ra');
        }
    });
}


function formsetting_GenDeviceSelect(listdevice, ListDevice, message) {
    var option;
    $.each(listdevice, function (i, o) {
        option += '<option value="' + o.ItemId + '" selected>' + o.DeviceModel + '<' + o.Mobile + '>' + '</option>';

    })


    $('#list-device-sent-notify').children().remove();
    $('<select id="select-list-device-sent-notify" multiple="multiple" data-width="100%" ></select>').append(option).appendTo('#list-device-sent-notify');
    $('#select-list-device-sent-notify').multiselect({
        includeSelectAllOption: true,
        buttonWidth: '100%',

        selectAllValue: 'select-all-value',
        selectAllText: 'Chọn tất cả',
        allSelectedText: 'Đã chọn tất cả',
        nonSelectedText: 'Chưa chọn thiết bị nhận nào',
        enableFiltering: false,
        onInitialized: function (select, container) {
            var value = $('#select-list-device-sent-notify option:selected');
            list_device_notify = [];
            $.each(value, function (i, o) {
                list_device_notify.push($(this).val());
            });
            console.log('fa2' + list_device_notify);
        },
        
        onDropdownHidden: function (event) {
            var value = $('#select-list-device-sent-notify option:selected');
            list_device_notify = [];
            $.each(value, function (i, o) {
                list_device_notify.push($(this).val());
            });

        },
        onChange: function (option, checked, select) {
            var value = $('#select-list-device-sent-notify option:selected');
            list_device_notify = [];
            $.each(value, function (i, o) {
                list_device_notify.push($(this).val());
            });
        },
        onSelectAll: function () {
            var value = $('#select-list-device-sent-notify option:selected');
            list_device_notify = [];
            $.each(value, function (i, o) {
                list_device_notify.push($(this).val());
            });
        },

    });
    $('#select-list-device-sent-notify').multiselect('selectAll', false);
    $('#select-list-device-sent-notify').multiselect('updateButtonText');
    $('#select-list-device-sent-notify .multiselect').addClass('white');
    $('.multiselect-container').css('width', '100%');
    if (ListDevice.length>0) {
        $('#select-list-device-sent-notify').multiselect('deselectAll', false);
        list_device_notify = [];
        list_device_notify = ListDevice;

        $.each(ListDevice, function (i, o) {
            $('#select-list-device-sent-notify').multiselect('select', o);
        })


    }
}

function formsetting_GenListSelect(listusersub, ListUser, message)
{
    var option;
    $.each(listusersub, function (i,o) {
        option += '<option value="' + o.UserId + '" selected>' + o.Email + '<' + o.Mobile + '>' + '</option>';

    })
  
   
    $('#list-sent-notify').children().remove();
    $('<select id="select-list-sent-notify" multiple="multiple" data-width="100%" ></select>').append(option).appendTo('#list-sent-notify');
    $('#select-list-sent-notify').multiselect({
        includeSelectAllOption: true,
        buttonWidth: '100%',

        selectAllValue: 'select-all-value',
        selectAllText: 'Chọn tất cả',
        allSelectedText: 'Đã chọn tất cả',
        nonSelectedText: 'Chưa chọn người nhận nào',
        enableFiltering: false,
        onInitialized: function (select, container) {
            var value = $('#select-list-sent-notify option:selected');
            list_user_notify = [];
            //dùng để chọn tất cả mặt định ban đầu 
            $.each(value, function (i, o) {
                list_user_notify.push($(this).val());
            });
            console.log('fa'+list_user_notify);
        },
        onDropdownHidden: function (event) {
            var value = $('#select-list-sent-notify option:selected');
            list_user_notify = [];
            $.each(value, function (i, o) {
                list_user_notify.push($(this).val());
            });

        },
        onChange: function(option, checked, select) {
            var value = $('#select-list-sent-notify option:selected');
            list_user_notify = [];
            $.each(value, function (i, o) {
                list_user_notify.push($(this).val());
            });
        },
        onSelectAll: function () {
            var value = $('#select-list-sent-notify option:selected');
            list_user_notify = [];
            $.each(value, function (i, o) {
                list_user_notify.push($(this).val());
            });
        },

    });
    $('#select-list-sent-notify').multiselect('selectAll', false);
    $('#select-list-sent-notify').multiselect('updateButtonText');
    $('#select-list-sent-notify .multiselect').addClass('white');
    $('.multiselect-container').css('width', '100%');
    if (ListUser.length>0) {
        $('#select-list-sent-notify').multiselect('deselectAll', false);
        list_user_notify = [];
        list_user_notify = ListUser;

        $.each(ListUser, function (i, o) {
            $('#select-list-sent-notify').multiselect('select', o);
        })


    }
}

function formsetting_LoadSelectEmailFrom(data, message) {

    var option = [];
    var optionreply = [];
    var ItemIdDefault;
    $.each(data, function (index, obj) {
        if (obj.IsDefault) {
            ItemIdDefault = obj.ItemId;
        }
        if (obj.IsVerify) {

            option.push({
                id: obj.ItemId,
                text: obj.FromName + ' <' + obj.FromMail + '>',
                fromname: obj.FromName,
                fromemail: obj.FromMail,
            });

            optionreply.push({
                id: obj.ItemId,
                text: obj.FromMail,
                fromname: obj.FromName,
                fromemail: obj.FromMail,
            });
        }

    });

    if (data.length == 0) {
        $('#txtSettingEmailSend').select2({
            data: option,

            "language": {
                "noResults": function () {
                    return "<a href='/Account/UserProfile#FromEmail' target='_blank' class='list-group-item bg-green bg-font-green' style=padding:5px;>" + message.AddNewFromEmail + "</a>";
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
     
    }
    else {

        $('#txtSettingEmailSend').select2({
            data: option,

        });



    }


        $('#txtSettingEmailSend').select2('val', ItemIdDefault);


    //placeholder search
    $("#txtSettingEmailSend").on("select2:open", function () {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", "Search...");
    });
    $("#txtSettingEmailSend").on("select2:close", function () {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", null);
    });

   
}



//ẩn hiện textbox nhập địa chỉ trang chuyển đến
function GetValueSubscribeAction() {
    
    //$('#formSetting input[name=subcribepage]').on('change', function () {
    //    var value = $('input[name=subcribepage]:checked', '#formSetting').val();
    //    console.log(value);
    //    if(value==3)
    //    {
    //        $('#urlSubscribePage').css({ 'display': '' })
    //    }
    //    else {
    //        $('#urlSubscribePage').css({ 'display': 'none' })
    //    }
    //});
}


//hàm load form nếu là edit update
function formsetting_LoadForm(apiDomain, access_token, FormId, message, userinfo, permission)
{
    if (FormId != -1) {
        $('#divLoading').show();
        $.ajax({
            url: apiDomain + 'api/Form/Info?FormId=' + FormId,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            success: function (r) {
                $('#divLoading').hide();
                var errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
                switch (errorcode) {
                    case 0:
                        var data = JSON.parse(JSON.stringify(r.Data));
                       // console.log(data);
                        //set giá trị 
                        $('#formname').val(data.FormName);
                        $('#thank-url-page').val(data.CustomThankyouPageUrl);
                        $('#ready-url-page').val(data.CustomAlreadySubscribePageUrl);
                        $('#action-ready-subcribe input[value="' + data.AfterSubscriptionAction + '"]').prop('checked', true);
                        $('#action-ready-subcribe-page input[value="' + data.AlreadySubscribeAction + '"]').prop('checked', true);
                        
                        $('#comfirmoption').prop('checked', data.ConfirmedOption);
                        $('#comfirmoption').bootstrapSwitch('state', data.ConfirmedOption);
                        if (parseInt(data.NotifyType) == 0)
                        {
                            $('#unsend-notify-register').prop('checked', true);
                        }
                        else {
                            $('#send-notify-register').prop('checked', true);
                            //$('#info-notify-send input[value=' + data.NotifyType + ']', '#formSetting').prop('checked', true);
                            var NotifyType = custom_dec2bin(data.NotifyType);
                            $('.md-checkbox-inline input[name="typesendnotify"]').prop('checked', false);
                            for (var i = NotifyType.length - 1; i >= 0; i--) {
                                console.log(Math.pow(2, NotifyType.length - 1 - i));
                                if (parseInt(NotifyType[i]) == 1) {

                                    $('input[name="typesendnotify"][value="' + Math.pow(2, NotifyType.length - 1 - i) + '"]').prop('checked', true);
                                }
                                else {
                                    $('input[name="typesendnotify"][value="' + Math.pow(2, NotifyType.length - 1 - i) + '"]').prop('checked', false);
                                }
                            }
                        }
                        formsetting_SetNotify();
                        

                        if (data.AfterSubscriptionAction == 3) {
                            $('#urlThankpage').css({ 'display': '' })
                        }
                        else {
                            $('#urlThankpage').css({ 'display': 'none' })
                        }
                        
                        var ListUser = data.NotifyToUsers;
                        var ListDevice = data.NotifyToDevices;
                        formsetting_GetListRecipient(apiDomain, access_token, userinfo, FormId, ListUser, message, permission)
                        formsetting_GetListDevice(apiDomain, access_token,ListDevice, message);

                        break;
                    default: custom_shownotification("error", message.GetInfoError);
                        break;
                }
            },
            error: function (x, s, e) {
                $('#divLoading').hide();
                custom_shownotification("error", message.GetInfoError);
            },
        });
    } else {
        var ListUser = [];
        var ListDevice = [];
        formsetting_GetListRecipient(apiDomain, access_token, userinfo, FormId, ListUser, message, permission);
        formsetting_GetListDevice(apiDomain, access_token,ListDevice, message);
    }
}


//click btn on form
function formsetting_LoadModalUpdateForm(apiDomain, access_token, ContactGroupId, FormId, eId, gName, message, userinfo, permission)
{
    $('#btnSettingForm').on('click', function () {
        formsetting_LoadForm(apiDomain, access_token, FormId, message, userinfo, permission);
        $('#btnFormSettingSaveModal').attr('data-id', FormId);
        $('#modalSettingForm').modal('toggle');
    });



}

//click on row
function formsetting_SettingForm(apiDomain, access_token, ContactGroupId, FormId, eId, gName, message, userinfo, permission) {
    var tablerow = $('#tableForm').DataTable();
    $('#tableForm tbody').on('click', '#setting', function () {
        var rowData = tablerow.row($(this).parents('tr')).data();
        FormId = rowData.formId;
        $('#btnFormSettingSaveModal').attr('data-id', FormId);
        formsetting_LoadForm(apiDomain, access_token, FormId, message, userinfo, permission);
        $('#modalSettingForm').modal('toggle');
    })
}


//sự kiện tạo một form mới //gọi api tạo form
function formsetting_CreateForm(apiDomain, access_token, ContactGroupId, FormId ,eId,gName, message)
{
    $('#btnFormSettingNext').on('click', function () {
        //nếu là tạo mới
        if (parseInt(FormId) == -1) {
            if (formsetting_CheckData()) {
                var data = formsetting_GetDataSave(ContactGroupId, FormId);
                var IsRedirect = true;
                formsetting_ActionCreate(apiDomain, access_token, eId, gName, data,IsRedirect, message);
            }
        }
        //nếu là cập nhập
        else {
            if (formsetting_CheckData()) {
                var data = formsetting_GetDataSave(ContactGroupId, FormId);
                formsetting_ActionUpdate(apiDomain, access_token, ContactGroupId,eId,gName, data, message);
            }
        }
    })
}

function formsetting_ModalUpdateForm(apiDomain, access_token, ContactGroupId, FormId, eId, gName, message)
{
    $('#btnFormSettingSaveModal').on('click', function () {
        if (formsetting_CheckData()) {
            FormId= parseInt($('#btnFormSettingSaveModal').attr('data-id'));
            var data = formsetting_GetDataSave(ContactGroupId, FormId);
            formsetting_ActionModalUpdate(apiDomain, access_token, ContactGroupId, eId, gName, data, message);
        }
    })
}

function formsetting_GetDataSave(ContactGroupId, FormId)
{
    

    var isnotify = $('input[name=notify-register]:checked', '#formSetting').val();
    var NotifyType=0;
    if (parseInt(isnotify) == 1)
    {
           NotifyType = 0;
    } else {
        //NotifyType = parseInt($('input[name=typesendnotify]:checked', '#formSetting').val());
        $.each($("input[name='typesendnotify']:checked", "#formSetting"), function () {
            console.log(parseInt($(this).val()));
            NotifyType += parseInt($(this).val());

        });
        
    }

    var data = {
        ContactGroupId: ContactGroupId,
        FormName: $('#formname').val(),
        AfterSubscriptionAction: $('input[name=thankpage]:checked', '#formSetting').val(),
        CustomThankyouPageUrl: $('#thank-url-page').val(),
        AlreadySubscribeAction: $('input[name=readypage]:checked', '#formSetting').val(), //'2',//$('input[name=subcribepage]:checked', '#formSetting').val(),
        CustomAlreadySubscribePageUrl: $('#ready-url-page').val(),
        Active: true, //khi có hàm load thông tin thì lấy từ thông tin
        NotifyToUsers: list_user_notify,
        NotifyToDevices: list_device_notify,
        NotifyType: NotifyType,
        ConfirmedOption: $('#comfirmoption').prop('checked'),
    };
    if (FormId != -1)
    {
        data.FormId = FormId;
    }

    return data;
}

function formsetting_CheckData() {
    if (!$('#formname').val()) {
        custom_shownotification("error", 'Tên form là bắt buộc');
        return false;
    }

    var isnotify = $('input[name=notify-register]:checked', '#formSetting').val();

    if (parseInt(isnotify) == 2 && $('input#sendemail').prop('checked') == false && $('input#sendsms').prop('checked') == false && $('input#send-notify').prop('checked') == false) {
        custom_shownotification("error", 'Chưa chọn kiểu gửi thông báo');
        return false;
    }

    if (parseInt(isnotify) == 2 && list_user_notify.length == 0 && ($('input#sendemail').prop('checked') == true || $('input#sendsms').prop('checked') == true)) {
        custom_shownotification("error", 'Chưa chọn người nhận thông báo');
        return false;
    }

    if (parseInt(isnotify) == 2 && $('input#send-notify').prop('checked') == true && list_device_notify.length == 0) {
        custom_shownotification("error", 'Chưa chọn thiết bị nhận thông báo');
        return false;
    }

    else return true;
}

function formsetting_ActionCreate(apiDomain, access_token, eId, gName, data,IsRedirect , message)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/Form/Create',
        type: 'POST',
        data: data,
        contentType: 'application/x-www-form-urlencoded',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            $('#divLoading').hide();
            var errorcode = parseInt(r.ErrorCode);
            if (IsRedirect) {
            switch (errorcode) {
                case 0:
                    
                        var data = JSON.parse(JSON.stringify(r.Data));
                        fId = data.FormId;
                        gId = data.ContactGroupId;
                        window.location.href = '/Contacts/Form?gId=' + encodeURI(gId + '&eId=' + eId + '&gName=' + gName + '&fId=' + fId, "UTF-8");
                
                    break;

                case 999:
                    $('#divLoading').hide();
                    custom_shownotification("error", message.RangeError);
                    break;
                default:
                    $('#divLoading').hide();
                    custom_shownotification("error", message.Error);

                    break;
            }
            }
        },
        error: function (x, s, e) {
            if (IsRedirect) {
                $('#divLoading').hide();
                custom_shownotification("error", message.Error);
            }
        },
    });
}
function formsetting_ActionUpdate(apiDomain, access_token,ContactGroupId,eId,gName, data, message)
{
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/Form/Update',
        type: 'Post',
        contentType: 'application/x-www-form-urlencoded',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        data: data,
        success: function (r) {
            $('#divLoading').hide();
            var errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
            switch (errorcode) {
                case 0:
                    var FormId = JSON.parse(JSON.stringify(r.Data)).FormId;
                    $('#update_form_id').text(FormId);
                    custom_shownotification("success", message.Success);
                    setTimeout(function () {
                        window.location.href = '/Contacts/ManageForm?gId=' + encodeURI(ContactGroupId + '&eId=' + eId + '&gName=' + gName + '&fId=' + FormId, "UTF-8");
                    }, 1000);
                    //$('#confirm-update-form').modal('toggle');
                    break;
                default:
                    custom_shownotification("error", message.Error);
                    break;

            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification("error", message.Error);
        }
    });
}

function formsetting_ActionModalUpdate(apiDomain, access_token, ContactGroupId, eId, gName, data, message) {
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/Form/Update',
        type: 'Post',
        contentType: 'application/x-www-form-urlencoded',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        data: data,
        success: function (r) {
            $('#divLoading').hide();
            var errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
            switch (errorcode) {
                case 0:
                    var FormId = JSON.parse(JSON.stringify(r.Data)).FormId;
                    $('#update_form_id').text(FormId);
                    custom_shownotification("success", message.Success);
                    $('#modalSettingForm').modal('toggle');
                    //$('#confirm-update-form').modal('toggle');
                    break;
                default:
                    custom_shownotification("error", message.Error);
                    break;

            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification("error", message.Error);
        }
    });
}

function formsetting_RedirectUpdateCustomField(ContactGroupId)
{
    $('#confirm-continue-update-form').on('click', function () {
        var fId= $('#update_form_id').text();
        window.location.href = '/Contacts/Form?gId=' + ContactGroupId + '&fId=' + fId;
    })
}


//function formsetting_PreviousAction(ContactGroupId)
//{
//    $('#btnFormSettingBack').on('click', function () {

//        window.location.href = '/Contacts/ManageForm?gId=' + ContactGroupId ;
//    });
//}