﻿/// <reference path="customfield.js" />
function createcustomfield_Action(apiDomain, access_token, message, Title, PlaceHolder, idGroupValue, permission, FormId) {
    cre_LoadTypeCustomField(apiDomain, access_token, Title, PlaceHolder, idGroupValue);
    cre_openModalAddCustom(apiDomain, access_token, Title, PlaceHolder, idGroupValue);
    cre_ActionAddValueField(Title, PlaceHolder, idGroupValue);
    cre_ActionChangeTypeCustomField(apiDomain, access_token, Title, PlaceHolder, idGroupValue);
    cre_ActionChangeFormatCustomField(apiDomain, access_token, Title, PlaceHolder, idGroupValue);
    cre_CreateUserCustomField(apiDomain, access_token, message, permission, FormId);
    cre_CheckValueMaxLength(message);
}


//load danh sách customfield type
function cre_LoadTypeCustomField(apiDomain, access_token, Title, PlaceHolder, idGroupValue) {
    $.ajax({
        url: apiDomain + '/api/CustomField/GetTypeList',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:

                    var options = new Array();
                    $.each(JSON.parse(JSON.stringify(r.Data)), function (i, obj) {
                        options.push('<option value="' + obj.FieldTypeId + '" data-icon="' + obj.IconUrl + '">' + obj.FieldTypeName + '</option>');

                    });
                    $('<select id="CustomFieldType" class="bs-select form-control" data-show-subtext="true">').append(options.join('')).appendTo('#div-custom-field-type');
                    //reder code style select
                    $('#div-custom-field-type #CustomFieldType').selectpicker();

                    //load format after load type 
                    var FieldTypeId = parseInt($('select[id=CustomFieldType]').val());
                    cre_LoadFormatCustomField(apiDomain, access_token, FieldTypeId, Title, PlaceHolder, idGroupValue);
                    break;
            }
        },
        error: function (x, s, e) {

        },
    });
}

//load danh sách format theo type.
function cre_LoadFormatCustomField(apiDomain, access_token, FieldTypeId, Title, PlaceHolder, idGroupValue) {
    //check select model exist before load->if exist remove
    if ($('#div-custom-field-format #CustomFieldFormat').length) {
        $('#div-custom-field-format').children().remove();
    }
    $.ajax({
        url: apiDomain + 'api/CustomField/GetFormatList?FieldTypeId=' + FieldTypeId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            var options = new Array();
            $.each(JSON.parse(JSON.stringify(r.Data)), function (i, obj) {
                options.push('<option value="' + obj.FieldFormatId + '" data-icon="glyphicon-tag">' + obj.FieldFormatName + '</option>');

            });
            $('<select id="CustomFieldFormat" class="bs-select form-control" data-show-subtext="true">').append(options.join('')).appendTo('#div-custom-field-format');
            //reder code style select
            $('#div-custom-field-format #CustomFieldFormat').selectpicker();

            //load field input value 
            cre_ChangeAnotherValue(apiDomain, access_token, Title, PlaceHolder, idGroupValue);
        },
        error: function (x, s, e) {

        },
    })
}

//sự kiện thay đổi type customfield
function cre_ActionChangeTypeCustomField(apiDomain, access_token, Title, PlaceHolder, idGroupValue) {
    $('#div-custom-field-type').on('change', '#CustomFieldType', function (e) {
        //xóa tất cả các value field hiện tại
        cre_DeleteAllValue(idGroupValue);
        //load format field
        var FieldTypeId = parseInt($('select[id=CustomFieldType]').val());
        cre_LoadFormatCustomField(apiDomain, access_token, FieldTypeId, Title, PlaceHolder, idGroupValue);
        
    });
}
//sự kiện thay đổi fromat customfield
function cre_ActionChangeFormatCustomField(apiDomain, access_token, Title, PlaceHolder, idGroupValue) {
    $('#div-custom-field-format').on('change', '#CustomFieldFormat', function (e) {
        //xóa tất cả các value field hiện tại
        cre_DeleteAllValue(idGroupValue);
        cre_ChangeAnotherValue(apiDomain, access_token, Title, PlaceHolder, idGroupValue);
    });
}


//sự kiện thay đổi fortmat type khi tao custom field --> thay đổi các field nhập giá trị.
function cre_ChangeAnotherValue(apiDomain, access_token, title, placeholder, id) {
    var type = parseInt($('#div-custom-field-type #CustomFieldType').val());
    var format = parseInt($('#div-custom-field-format #CustomFieldFormat').val());
    var maxLength = $('#input-customfield-maxlength').val();
    console.log(type);
    console.log(format);
    switch (type) {
        case 1:

            switch (format) {
                case 1:
                    cre_addAnotherValue_Text(title, placeholder, id,maxLength);
                    cre_HideDel(id);
                    cre_HideAdd();
                    break;
                case 2:
                    cre_addAnotherValue_Text(title, placeholder, id, maxLength);
                    cre_HideDel(id);
                    cre_HideAdd();
                    break;
                default:
                    cre_addAnotherValue_Text(title, placeholder, id, maxLength);
                    cre_addAnotherValue_Text(title, placeholder, id, maxLength);
                    cre_ShowAdd();
                    cre_HideDel(id);
                    break;
            }
            break;
        case 2:
            switch (format) {
                case 1:
                    cre_addAnotherValue_Number(title, placeholder, id, maxLength);
                    cre_HideDel(id);
                    cre_HideAdd();
                    break;
                case 2:
                    cre_addAnotherValue_Number(title, placeholder, id, maxLength);
                    cre_HideDel(id);
                    cre_HideAdd();
                    break;
                default:
                    cre_addAnotherValue_Number(title, placeholder, id, maxLength);
                    cre_addAnotherValue_Number(title, placeholder, id, maxLength);
                    cre_ShowAdd();
                    cre_HideDel(id);
                    break;
            }
            break;
        case 3:
            cre_addAnotherValue_Text(title, placeholder, id, maxLength);
            cre_HideDel(id);
            cre_HideAdd();
            break;
        case 4:
            cre_addAnotherValue_Text(title, placeholder, id, maxLength);
            cre_HideDel(id);
            cre_HideAdd()
            break;
        case 6:
            $('#input-customfield-maxlength').val('11');
            cre_addAnotherValue_Number(title, placeholder, id, maxLength);
            cre_HideDel(id);
            cre_HideAdd();
            break;
        case 7:
            $('#input-customfield-maxlength').val('11');
            switch (format) {
                case 1:
                    cre_addAnotherValue_Date(title, placeholder, id, maxLength);
                    cre_HideDel(id);
                    cre_HideAdd();
                    break;
                default:
                    cre_addAnotherValue_Date(title, placeholder, id, maxLength);
                    cre_addAnotherValue_Date(title, placeholder, id, maxLength);
                    cre_ShowAdd();
                    cre_HideDel(id);
                    break;
            }
            break;
        case 8:
            $('#input-customfield-maxlength').val('16');
            switch (format) {
                case 1:
                    cre_addAnotherValue_DateTime(title, placeholder, id, maxLength);
                    cre_HideDel(id);
                    cre_HideAdd();
                    break;
                default:
                    cre_addAnotherValue_DateTime(title, placeholder, id, maxLength);
                    cre_addAnotherValue_DateTime(title, placeholder, id, maxLength);
                    cre_ShowAdd();
                    cre_HideDel(id);
                    break;
            }
            break;
        case 10:
            cre_addAnotherValue_Gender(title, placeholder, id, maxLength);
            cre_addAnotherValue_Gender(title, placeholder, id, maxLength);
            cre_ShowAdd();
            cre_HideDel(id);
            break;
    }

};


//append field để nhập thêm gia trị trong create custom field
//ẩn hiện nút delete , add value 

function cre_addAnotherValue(title, placeholder, id) {
    var type = parseInt($('select[id=CustomFieldType]').val());
    var format = parseInt($('select[id=CustomFieldFormat]').val());
    var maxLength = $('#input-customfield-maxlength').val();
    console.log(type);
    console.log(format);
    switch (type) {
        case 1:
            cre_addAnotherValue_Text(title, placeholder, id, maxLength);
            cre_ShowDel(id);
            break;
        case 2:
            cre_addAnotherValue_Number(title, placeholder, id, maxLength);
            cre_ShowDel(id);
            break;
        case 3:
            cre_addAnotherValue_Text(title, placeholder, id, maxLength);

            cre_ShowDel(id);
            break;
        case 4:
            cre_addAnotherValue_Text(title, placeholder, id, maxLength);

            cre_ShowDel(id);
            break;
        case 6:
            cre_addAnotherValue_Number(title, placeholder, id, maxLength);

            cre_ShowDel(id);
            break;
        case 7:
            cre_addAnotherValue_Date(title, placeholder, id, maxLength);

            cre_ShowDel(id);
            break;
        case 8:
            cre_addAnotherValue_DateTime(title, placeholder, id, maxLength);

            cre_ShowDel(id);
            break;
        case 10:
            cre_addAnotherValue_Gender(title, placeholder, id, maxLength);
            cre_ShowDel(id);
            break;
    }

};

// chuỗi control delete field value trong model tạo field
var cre_deleteValueField = '<a id="deleteAnotherValue" class="btn btn-circle btn-icon-only"><i class="icon-trash"></i></a>';

// add value in modal create custom field
function cre_addAnotherValue_Text(title, placeholder, id, maxLength) {
    var newItem = $(document.createElement('div'))
                       .attr("class", "form-group").attr("id", id);
    newItem.after().html('<label class="col-md-3 control-label">' + title + '</label>' +
                             '<div class="col-md-6">' +
                                 '<input type="text" id="input-customfield-value" name="FieldValue" maxlength="'+maxLength+'" class="form-control inputFieldValue" placeholder="' + placeholder + '">' +
                             '</div>' +
                             cre_deleteValueField);
    newItem.appendTo("#GroupValue");
};
function cre_addAnotherValue_Number(title, placeholder, id, maxLength) {
    var newItem = $(document.createElement('div'))
                        .attr("class", "form-group").attr("id", id);
    newItem.after().html('<label class="col-md-3 control-label">' + title + '</label>' +
                             '<div class="col-md-6">' +
                                 '<input type="text" id="input-customfield-value" name="FieldValue" maxlength="' + maxLength + '" class="form-control inputFieldValue" placeholder="' + placeholder + '" onkeypress="validate(event)">' +
                             '</div>' +
                             cre_deleteValueField);
    newItem.appendTo("#GroupValue");

};
function cre_addAnotherValue_Date(title, placeholder, id, maxLength) {
    var newItem = $(document.createElement('div'))
                       .attr("class", "form-group").attr("id", id);
    newItem.after().html('<label class="col-md-3 control-label">' + title + '</label>' +
                             '<div class="col-md-6">' +
                                    '<input name="FieldValue" id="input-customfield-value" class="form-control date-picker inputFieldValue" />' +
                             '</div>' +
                             cre_deleteValueField);
    newItem.appendTo("#GroupValue");
    form_datesingle();
};

function cre_addAnotherValue_DateTime(title, placeholder, id, maxLength) {
    var newItem = $(document.createElement('div'))
                       .attr("class", "form-group").attr("id", id);
    newItem.after().html('<label class="col-md-3 control-label">' + title + '</label>' +
                             '<div class="col-md-6">' +
                                    '<div class="form_datetime"> ' +
                                         '<input name="FieldValue" id="input-customfield-value" class="form-control inputFieldValue" />' +
                                         //'<span class="input-group-btn">' +
                                         //     '<button class="btn default date-set" type="button">' +
                                         //         '<i class="fa fa-calendar"></i>' +
                                         //      '</button>' +
                                         // '</span>' +
                                       '</div>' +
                             '</div>' +
                             cre_deleteValueField);
    newItem.appendTo("#GroupValue");
    from_datetimesingle();
};
function cre_addAnotherValue_Gender(title, placeholder, id, maxLength) {
    var newItem = $(document.createElement('div'))
                       .attr("class", "form-group").attr("id", id);
    newItem.after().html('<label class="col-md-3 control-label">' + title + '</label>' +
                             '<div class="col-md-6">' +
                                 '<input type="text" name="FieldValue" id="input-customfield-value" maxlength="' + maxLength + '" class="form-control inputFieldValue" placeholder="' + placeholder + '">' +
                             '</div>' +
                             cre_deleteValueField);
    newItem.appendTo("#GroupValue");
};

//thêm customfield của người dùng
function cre_CreateUserCustomField(apiDomain, access_token, message, permission, FormId) {
    $('#save-user-custom-field').on('click', function () {
        if (form_CheckBeforeCreateCustomField(message)) {
            var ListValue = new Array();
            if ($('#GroupValue').length) {

                var count = 0;
                var items = $('#GroupValue #GroupFieldValue #input-customfield-value');
                $.each(items, function (index, item) {
                    var obj = { FieldSelected: false, FieldValue: '', SortId: '' };
                    // console.log($(item))
                    // alert($(item).val());
                    obj.FieldValue = $(item).val(); // "this" is the current element in the loop

                    
                    if ($('#GroupValue').children().length > 1) {
                        obj.FieldSelected = 'false';
                        obj.SortId = count;
                    }
                    ListValue.push(obj);
                    console.log(ListValue)
                    count++;
                });
            }
            var data = {
                FieldTypeId: parseInt($('select[id=CustomFieldType]').val()),
                FieldFormatId: parseInt($('select[id=CustomFieldFormat]').val()),
                FieldName: custom_bodauTiengViet($('#create-custom-field-name').val().replace(/\s/g, '')),
                MaxLength:$('#input-customfield-maxlength').val(),
                Values: ListValue

            };
            $.ajax({
                url: apiDomain + 'api/CustomField/Create',
                type: 'POST',
                data: data,
                contentType: 'application/x-www-form-urlencoded',
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + access_token);
                },
                success: function (r) {
                    var errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
                    switch (errorcode) {
                        case 0:
                            custom_shownotification("success", message.SaveCustomFieldSuccess);
                            $('.create-customfield-modal').modal('toggle');

                            customfield_LoadCustomField(apiDomain, access_token, message);
                            if (FormId) {
                                $("#predefine1").children().remove();
                                $("#predefine2").children().remove();
                                $("#usercustom1").children().remove();
                                $("#usercustom2").children().remove();
                                $("#delete-usercustom1").children().remove();
                                $("#delete-usercustom2").children().remove();
                                form_LoadCustomField(apiDomain, access_token, FormId, message, permission)
                            }
                            
                            break;
                        default: custom_shownotification("error", message.SaveCustomFieldError);
                    }
                },
                error: function (x, s, e) {
                    custom_shownotification("error", message.SaveCustomFieldError);
                }

            });
        }
    });
}

function from_datetimesingle() {
    $('.form_datetime input').datetimepicker(
        {
            autoclose: true,
            isRTL: App.isRTL(),
            format: "dd/mm/yyyy hh:ii",
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
        });
}

function form_datesingle() {
    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        orientation: "left",
        autoclose: true
    });
}

//kiểm tra tên rỗng 
function form_CheckBeforeCreateCustomField(message) {
    var items = $('#GroupValue #GroupFieldValue #input-customfield-value');

    if ($('#input-customfield-maxlength').val() == '') {
        custom_shownotification("error", message.RequireInputLengthValue);
        return false;
    }

    var error = 0;
    $.each(items, function (index, item) {
        var obj = { FieldSelected: false, FieldValue: '', SortId: '' };
       
        obj.FieldValue = $(item).val(); 

        var charLength = $(item).val().length + appshare_SMSSymbolLength($(item).val());
        if (charLength > parseInt($('#input-customfield-maxlength').val())) {
            custom_shownotification("error", message.ValueExceedLength);
            error+=1;
            return false;
        }
    });
    if (error>0) {
        return false;
    }


    if ($('#create-custom-field-name').val() == "") {
        custom_shownotification("error", message.CheckName);
        return false;
    }
    if ($('#create-custom-field-name').val()) {
        if (!IsMessage($('#create-custom-field-name').val())) {
            custom_shownotification('error', message.ODau);
            return false;
        }
        else return true;
    }

    else return true;
}

//ẩn nút xóa field giá trị
function cre_HideDel(id) {
    var deleteid = $('#' + id).children('a').attr('id');
    $('#' + id + '>#' + deleteid).each(function () {
        $(this).css({ 'display': 'none' });
    })
};

//hiện nút xóa giá trị
function cre_ShowDel(id) {
    var deleteid = $('#' + id).children('a').attr('id');
    $('#' + id + '>#' + deleteid).each(function () {
        $(this).css({ 'display': '' });
    })
};

//ẩn nút thêm field giá trị
function cre_HideAdd() {
    $('#GroupAddValue').css({ 'display': 'none' });
};

//hiện nút thêm filed giá trị
function cre_ShowAdd() {
    $('#GroupAddValue').css({ 'display': '' });
};

//xóa tất cả field giá trị
function cre_DeleteAllValue(id) {

    var parentid = $("#" + id).parent().attr('id');
    $('#' + parentid + '>' + '#' + id).each(function () {
        $(this).remove();
    })
};

//xóa field nhập value
//sự kiện xóa 1 append
$('#formAddCustomField').on('click', '#deleteAnotherValue', function () {
    $(this).parent().remove();
    var id = $(this).parent().attr('id');
    var value = $('div#' + id).length;
    if (value == 2) {
        cre_HideDel(id);
    }

});

function cre_openModalAddCustom(apiDomain, access_token, Title, PlaceHolder, idGroupValue) {
    //bên form
    $('#user-custom-field-panel').on('click', '#create-custom-field', function () {
        console.log('fafa');
        $('#GroupValue').children().remove();
        $('#create-custom-field-name').val('');
        $('#CustomFieldType').selectpicker('deselectAll');

        $('#CustomFieldFormat').selectpicker('deselectAll');

        $('#input-customfield-maxlength').val('');
        $("#CustomFieldType").val('default');
        $("#CustomFieldType").selectpicker("refresh");
        $("#CustomFieldFormat").val('default');
        $("#CustomFieldFormat").selectpicker("refresh");
        $('.create-customfield-modal').modal('toggle');
        cre_ChangeAnotherValue(apiDomain, access_token, Title, PlaceHolder, idGroupValue);

    });

    $('#modal-create-customfield').on('shown.bs.modal', function (e) {
        $('#GroupValue').children().remove();
        $('#create-custom-field-name').val('');
        $('#CustomFieldType').selectpicker('deselectAll');

        $('#CustomFieldFormat').selectpicker('deselectAll');

        $('#input-customfield-maxlength').val('');
        $("#CustomFieldType").val('default');
        $("#CustomFieldType").selectpicker("refresh");
        $("#CustomFieldFormat").val('default');
        $("#CustomFieldFormat").selectpicker("refresh");
        cre_ChangeAnotherValue(apiDomain, access_token, Title, PlaceHolder, idGroupValue);
    })
}

//nút thêm giá trị customfield
function cre_ActionAddValueField(Title, PlaceHolder, idGroupValue) {
    $('#addAnotherValue').on('click', function () {
        cre_addAnotherValue(Title, PlaceHolder, idGroupValue);
    });
}


function cre_CheckValueMaxLength(message) {
    $('#modal-create-customfield').on("keyup", ".inputFieldValue", function (e) {
        var charLength = $(this).val().length + appshare_SMSSymbolLength($(this).val());
        if (charLength > parseInt($('#input-customfield-maxlength').val())) {
            custom_shownotification("error", message.ValueExceedLength);
        }

    });


    $('#modal-create-customfield').on("blur", "#input-customfield-maxlength", function (e) {
        var items = $('#GroupValue #GroupFieldValue #input-customfield-value');
        $.each(items, function (index, item) {
            $(this).attr('maxlength', $('#input-customfield-maxlength').val());
        });

    });

}
