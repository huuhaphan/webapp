﻿//hàm xử lí chung
//biến toàn cục form đã được lưu chưa
window.savedform = true;
$(window).resize(function () {
    form_resizeContentForm();
});

//click vào các items trên content form
var fielditemId;

//click vào các items content trên form
var fielditemIdContent;

//var ItemNotDelete = 1;//email là mặt định không được xóa
var ItemNotDelete;
//load form 
var form_listId = [];//name ,email, mobile


function form_Action(apiDomain, access_token, Title, PlaceHolder, idGroupValue, messageWarning, btnSaveId, FormId, GroupId, nameButton, nameHeader, userId, imgUrl, eId, gName, permission) {

    //resize
    form_resizeContentForm();

    //intialize
    form_intialize();

    //upload image
    form_ActionUploadImage(apiDomain, access_token, userId, imgUrl, messageWarning);

    //upload background
    form_ActionUploadBackground(apiDomain, access_token, userId, imgUrl, messageWarning);

    //load form ----------------------------
    form_LoadForm(apiDomain, access_token, FormId, nameButton, nameHeader);

    //load customfield
    form_LoadCustomField(apiDomain, access_token, FormId, messageWarning, permission)

    

    //load customfield type dùng khi tạo người dùng tạo customfield
    //LoadTypeCustomField(apiDomain, access_token, Title, PlaceHolder, idGroupValue);

    //ẩn menu
   // custom_HideMenu();
    //kéo thay đổi vị trí của các field
    form_ChangePositionField();

    //dùng để gọi khi sự kiện load select option type , format xong 
   // form_ChangeAnotherValue(apiDomain, access_token, Title, PlaceHolder, idGroupValue);

    custom_WarningLoad(btnSaveId, messageWarning.WarningPageLoad);

    

    //sự kiện change type customfield
    //form_ActionChangeTypeCustomField(apiDomain, access_token, Title, PlaceHolder, idGroupValue);

    //sự kiện change format customfield
    //form_ActionChangeFormatCustomField(apiDomain, access_token, Title, PlaceHolder, idGroupValue);
    
    //sự kiện nút thêm giá trị cho customfield
    //form_ActionAddValueField(Title, PlaceHolder, idGroupValue);
    
    //lưu custom field của người dùng tạo
    //form_CreateUserCustomField(apiDomain, access_token, FormId, messageWarning, permission);

    //sự kiện click vào headline form
    form_ClickInHeadLine();

    //click vào items 
    form_ClickInItems();

    //click vào button form
    form_ClickInButton();

    //click vào image
    //form_ClickInImage();

    //click in control
    form_ClickInControl();

    //click vào content form ngoài trừ form thì xóa các css border , ẩn hình ẩn tab style
    form_ClickInContentForm();

    //sự kiện xóa 1 field item 
    form_DeleteItem();

    //sự kiện change setting style form item 
    form_ChangeSettingStyle();

    //update customfile form -----------------------------><----------------------------------------
    form_UpdateCustomFieldToForm(apiDomain, access_token, FormId, GroupId, messageWarning, eId, gName);

    //open modal dal addcustomfield
    //form_openModalAddCustom(apiDomain, access_token, Title, PlaceHolder, idGroupValue);
    //xóa 1 custom field
    form_DeleteCustomField(apiDomain, access_token, messageWarning);
    form_ActionDeleteCustomField(apiDomain, access_token, messageWarning);


}

//khởi tạo control
function form_intialize() {
    //color picker
    $('#color-headline-form,#color-content-form, #color-button-text-form,#color-button-text-hover-form,#color-button-form,#color-button-hover-form,#color-label-form,#color-text-form,#color-bacground-form, #color-border-form ').minicolors({
        control: $(this).attr('data-control') || 'hue',
        defaultValue: $(this).attr('data-defaultValue') || '',
        inline: $(this).attr('data-inline') === 'true',
        letterCase: $(this).attr('data-letterCase') || 'lowercase',
        opacity: $(this).attr('data-opacity'),
        position: $(this).attr('data-position') || 'bottom left',
        change: function (hex, opacity) {
            if (!hex) return;
            if (opacity) hex += ', ' + opacity;
            if (typeof console === 'object') {
                console.log(hex);
            }
        },
        theme: 'bootstrap'
    });

    //slider
    var slider1 = document.getElementById('slider-size-label');
    noUiSlider.create(slider1, {
        start: 10,
        animate: false,
        range: {
            min: 10,
            max: 40
        },
    });
    var slider2 = document.getElementById('slider-size-textbox');
    noUiSlider.create(slider2, {
        start: 20,
        animate: false,
        range: {
            min: 20,
            max: 60
        }
    });

    var slider3 = document.getElementById('slider-size-header');
    noUiSlider.create(slider3, {
        start: 10,
        animate: false,
        range: {
            min: 10,
            max: 60
        }
    });

    var slider4 = document.getElementById('slider-size-button');
    noUiSlider.create(slider4, {
        start: 10,
        animate: false,
        range: {
            min: 10,
            max: 100
        }
    });

    var slider5 = document.getElementById('slider-size-height-button');
    noUiSlider.create(slider5, {
        start: 10,
        animate: false,
        range: {
            min: 1,
            max: 5
        }
    });

    var slider5 = document.getElementById('slider-size-content');
    noUiSlider.create(slider5, {
        start: 10,
        animate: false,
        range: {
            min: 10,
            max: 40
        }
    });


}


//load custom field
function form_LoadCustomField(apiDomain, access_token, FormId, message, permission)
{
    //load customfield hệ thống
    customField_System(apiDomain, access_token, FormId, message, permission);

}



function form_LoadForm(apiDomain, access_token, FormId, nameButton, nameHeader)
{
    if (FormId) {
        $.ajax({
            url: apiDomain + 'api/Form/Info?FormId=' + FormId,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            success: function (r) {
                var errorcode = parseInt(r.ErrorCode);
                var data = r.Data;
                switch (errorcode) {
                    case 0:
                        if (r.Data) {
                            var CustomContent = JSON.parse(JSON.stringify(r.Data)).FormContent;
                            var FormCustomFields = data.FormCustomFields;

                            if (FormCustomFields.length > 0) {
                              
                                //nếu đã tạo form rồi giờ update

                                form_LoadFormCustomField(FormCustomFields);//load form custom field.

                                addSubmit(CustomContent.FormSubmitButtonLabel);

                                addHeadline(CustomContent.FormHeader);

                                form_SetValueTabStyleWithData(CustomContent);//load nội dung của tab style theo form
                               
                            }
                            else {
                                setTimeout(function () {
                                    //nếu chưa tạo form
                                    //tạo header button 
                                    addSubmit(nameButton); //tạo button
                                    addHeadline(nameHeader);  //tạo headline
                                    //tạo field mặt định
                                    form_listId = ["2", "1", "10"];
                                    $.each(form_listId, function (index, id) {
                                        form_CustomField(id);
                                    });

                                    form_DisableButtonCustomField();
                                    form_SetValueTabStyleNoData(CustomContent);
                                }, 1500);
                            }
                            //xử lý load form chỗ này
                        }

                        break;
                    case 101:
                        window.location.href = "/Contacts" //chờ làm trang không tồn tại
                        break;
                    default:
                        custom_shownotification("error", 'Có lỗi xảy ra');
                        break;
                }
            },
            error: function (x, s, e) {
                custom_shownotification("error", 'Có lỗi xảy ra');
            }
        });
    }
}


//load customfield

function form_LoadFormCustomField(FormCustomFields) {
    $.each(FormCustomFields, function (index, obj) {
        var fieldId = obj.FieldId;
        if (parseInt(fieldId) != 10078 && parseInt(fieldId) != 10079) { // ngoại trừ content&line
            form_listId.push(fieldId); //dùng disable button không cho add thêm khi có trong này
        }
        var fieldName = obj.FieldLabel;
        var fieldTypeId = obj.FieldTypeId;
        var fieldTypeName = JSON.parse(JSON.stringify(obj.FieldType)).FieldTypeName;
        var fieldFormatId = obj.FieldFormatId;
        var fieldValue = new Array();
        var isRequired = obj.IsRequired;
        var placeholder = obj.PlaceHolder;
        var fieldLabelPosition = obj.FieldLabelPosition;
        var fieldWidth = obj.Width;
        var maxlength = obj.MaxChar;

        $.each(JSON.parse(JSON.stringify(obj.Values)), function (index, value) {
            fieldValue.push(value.FieldValue);
        });

        var data = {
            fieldId: fieldId,
            fieldName: fieldName,
            fieldTypeId: fieldTypeId,
            fieldTypeName: fieldTypeName,
            fieldFormatId: fieldFormatId,
            fieldValue: fieldValue,
            isRequired: isRequired,
            placeholder: placeholder,
            fieldLabelPosition: fieldLabelPosition,
            fieldWidth: fieldWidth,
            maxlength: maxlength,

        };

        from_ActionCreateCustomField(data);
    });
};


//load value tabstyle đúng với form customfile khi load lên
function form_SetValueTabStyleWithData(CustomContent)
{
    //header
    $('#header-alignment input[value="' + CustomContent.FormHeaderAlign + '"]').parent().addClass('active');
    $('#color-headline-form').minicolors('value', CustomContent.FormHeaderColor);
    $('.controlInput #_headlineFormSubmit').css('color', CustomContent.FormHeaderColor);
  
    var slider_size_lable = document.getElementById('slider-size-header');
    slider_size_lable.noUiSlider.set(CustomContent.FormHeaderSize);
    $('.controlInput #_headlineFormSubmit').css('font-size', CustomContent.FormHeaderSize);

    $('.controlInput .headline').css('text-align', CustomContent.FormHeaderAlign);

    //buton
    var slider_size_height_button = document.getElementById('slider-size-height-button');
    slider_size_height_button.noUiSlider.set(CustomContent.FormButtonSize);
    form_SizeButtonHeight(parseInt(CustomContent.FormButtonSize));

    $('.controlInput .submit').css('text-align', CustomContent.FormButtonAlign);
    
    var slider_size_button = document.getElementById('slider-size-button');
    slider_size_button.noUiSlider.set(CustomContent.FormButtonWidth);
    $('.controlInput #_btnformSubmit').css('width',parseInt(CustomContent.FormButtonWidth)+'%');

    $('#color-button-text-form').minicolors('value', CustomContent.FormButtonColor);
    $('#color-button-text-hover-form').minicolors('value', CustomContent.FormButtonColorHover);
    $('#color-button-form').minicolors('value', CustomContent.FormButtonBgColor);
    $('#color-button-hover-form').minicolors('value', CustomContent.FormButtonBgColorHover);

    addLabelStyle(CustomContent);

    addTextStyle(CustomContent);

    addImage(CustomContent);


}

function form_SetValueTabStyleNoData()
{
    //header
    $('#header-alignment input[value="left"]').parent().addClass('active');
    $('#color-headline-form').minicolors('value', '#34495e');
    //$('#header-size input[value="sz-nm"]').parent().addClass('active');
    $('.controlInput .headline').css('text-align', 'left');
    var slider_size_lable = document.getElementById('slider-size-header');
    slider_size_lable.noUiSlider.set(20);
    $('.controlInput #_headlineFormSubmit').css('font-size', '20px');
    
    //buton
   
    form_SizeButtonHeight(3);
    var slider_size_height_button = document.getElementById('slider-size-height-button');
    slider_size_height_button.noUiSlider.set(3);

    var slider_size_button = document.getElementById('slider-size-button');
    slider_size_button.noUiSlider.set(100);
    $('.controlInput #_btnformSubmit').css('width', '100%');

    $('.controlInput .submit').css('text-align', 'left');

    $('#color-button-text-form').minicolors('value', '#fff');
    $('#color-button-text-hover-form').minicolors('value', '#fff');
    $('#color-button-form').minicolors('value', '#217ebd');
    $('#color-button-hover-form').minicolors('value', '');

    //background
    $('#color-bacground-form').minicolors('value', '#fff');

    //lable
    $('#color-label-form').minicolors('value', '#424242');
    var slider_size_lable = document.getElementById('slider-size-label');
    slider_size_lable.noUiSlider.set(18);
    $('.controlInput #captionname').css('font-size', '18px');

    //no banner
    $('.controlInput #image').addClass('hide');
    $('.controlInput').css('max-width', '460px');

    //textbox control
    form_SetHeightControl(30);
    var slider_size_textbox = document.getElementById('slider-size-textbox');
    slider_size_textbox.noUiSlider.set(30);
}


function addImage(data) {
    $('#custom-image-form').attr('src', data.FormImageLink);
    $('#thubnail-image-form img').attr('src', data.FormImageLink);
    if (data.FormImageVisible) {
        $('#show-image-form').prop('checked', true);
        $('.controlInput #image').removeClass('hide');
        $('.controlInput #image').removeClass('hide');
        var pst=0;
        form_SetPositionImage(data.FormImagePosition);
        switch (data.FormImagePosition) {
            case 0:
                pst= 'top';
                break;
            case 1:
                pst ='bottom';
                break;
            case 2:
                pst ='left';
                break;
            case 3:
                pst ='right';
                break;
            default:
                pst = 'top';
                break;
        }
       
        $('#image-position input[value="' + pst + '"]').parent().trigger('click');


    }
    else {
        $('.controlInput #image').addClass('hide');
        $('.controlInput').css('max-width', '460px');
    }

    if (data.FormBackgroundImageUrl != '') {
        $('#thubnail-background-form img').attr('src', data.FormBackgroundImageUrl);
        $('.controlInput').css('background-image', 'url(' + data.FormBackgroundImageUrl + ')');
    }
    else {
         $('#thubnail-background-form img').attr('src','');

    }
    if (data.FormBackgroundImageSize != '') {

        if (data.FormBackgroundImageSize.match(/^[0-9]+$/) != null) {
            $('#bacground-size').val(data.FormBackgroundImageSize);
            $('.controlInput').css('background-size', data.FormBackgroundImageSize+'%');
        }
        else {
            $('#background-size-auto').val(data.FormBackgroundImageSize);
            $('.controlInput').css('background-size', data.FormBackgroundImageSize);
        }


    }
    

    if (data.FormBackgroundColor!='') {
        $('.controlInput').css('#background-color', data.FormBackgroundColor);
        $('#color-bacground-form').minicolors('value', data.FormBackgroundColor);
    }
    else {
        
        $('.controlInput').css('#background-color', 'transparent');
        $('#color-bacground-form').minicolors('value', '');
    }

    if (data.FormBackgroundImagePosition != '') {
        var position = data.FormBackgroundImagePosition.split(",");

        $('.controlInput').css('background-position-x', position[0] + '%');
        $('.controlInput').css('background-position-y', position[1] + '%');

        $('#bacground-position-x').val(position[0]);
        $('#bacground-position-y').val(position[1]);

    }

    if (data.FormBackgroundImageRepeat == 'true') {
        $('.control-web-form').css('background-repeat', 'repeat');
        $('#repeat-background-form').prop('checked', true);

    }
    else {
        $('.control-web-form').css('background-repeat', 'no-repeat');
        $('#repeat-background-form').prop('checked', false);
    }


}

function addLabelStyle(data) {
    if (data.FormLabelColor != '') {

        $('.controlInput #captionname').css('color', data.FormLabelColor);
        $('#color-label-form').minicolors('value', data.FormLabelColor);
    }
    else {
        $('.controlInput #captionname').css('color', '#424242');
        $('#color-label-form').minicolors('value', '#424242');
    }

    if (data.FormLabelSize) {
        $('.controlInput #captionname').css('font-size', data.FormLabelSize + 'px');
        var slider_size_lable = document.getElementById('slider-size-label');
        slider_size_lable.noUiSlider.set(data.FormLabelSize);
    }
    else {
        $('.controlInput #captionname').css('font-size', '18px');
        var slider_size_lable = document.getElementById('slider-size-label');
        slider_size_lable.noUiSlider.set(18);
    }
    if(data.FormLabelAlign)
    {
        form_SetLabelPositon(data.FormLabelAlign);
        $('#label-position').val(data.FormLabelAlign);
    }
}

function addLabelStyleNewField() {
    //color
    $('.controlInput #captionname').css('color', $('#color-label-form').val());

    //size
    var slider_size_lable = document.getElementById('slider-size-label');
    $('.controlInput #captionname').css('font-size', slider_size_lable.noUiSlider.get() + 'px');

    //position
    var position = $('#label-position').val();
    form_SetLabelPositon(position);

}

function addTextStyleNewField() {
    //boder
    form_SetControlBorder($('#color-border-form').val());
  
    //color
    form_SetColorControl($('#color-text-form').val());

    //height
    var slider_size_textbox = document.getElementById('slider-size-textbox'); 
    form_SetHeightControl(parseInt(slider_size_textbox.noUiSlider.get()));

    //radius
    form_SetRadiusFromInput();

}


function addTextStyle(data) {
    if (data.FormControlTextColor != '') {

        form_SetColorControl(data.FormControlTextColor);
        $('#color-text-form').minicolors('value', data.FormControlTextColor);
    }

    if (data.FormControlBorderColor!='')
    {
        form_SetControlBorder(data.FormControlBorderColor);
        $('#color-border-form').minicolors('value', data.FormControlBorderColor);
    }

    if (data.FormControlSize != '') {
        form_SetHeightControl(parseInt(data.FormControlSize));
     
        var slider_size_textbox = document.getElementById('slider-size-textbox');
        slider_size_textbox.noUiSlider.set(data.FormControlSize);
    }
    else {
        form_SetHeightControl(42);
        var slider_size_textbox = document.getElementById('slider-size-textbox');
        slider_size_textbox.noUiSlider.set(42);
    }
    
    if (data.FormControlBorderRadius) {
        var radius = data.FormControlBorderRadius; //.split(',');
        var radiuslt = radius; //radius[0];
        var radiusrt = radius; ///radius[1];
        var radiusrb = radius; //radius[2];
        var radiuslb = radius; //radius[3];

        $('#color-radius-lefttop-form').val(radiuslt);
        $('#color-radius-righttop-form').val(radiusrt);
        $('#color-radius-leftbottom-form').val(radiuslb);
        $('#color-radius-rightbottom-form').val(radiusrb);
        
        $('.controlInput input').css({ 'border-top-left-radius': radiuslt + 'px', 'border-top-right-radius': radiusrt + 'px', 'border-bottom-right-radius': radiusrb + 'px', 'border-bottom-left-radius': radiuslb + 'px' });
        $('.controlInput textarea').css({ 'border-top-left-radius': radiuslt + 'px', 'border-top-right-radius': radiusrt + 'px', 'border-bottom-right-radius': radiusrb + 'px', 'border-bottom-left-radius': radiuslb + 'px' });
        $('.controlInput select').css({ 'border-top-left-radius': radiuslt + 'px', 'border-top-right-radius': radiusrt + 'px', 'border-bottom-right-radius': radiusrb + 'px', 'border-bottom-left-radius': radiuslb + 'px' });
        $('.controlInput #_btnformSubmit').css({ 'border-top-left-radius': radiuslt + 'px', 'border-top-right-radius': radiusrt + 'px', 'border-bottom-right-radius': radiusrb + 'px', 'border-bottom-left-radius': radiuslb + 'px' });

    }
}



//custom field hệ thống
function customField_System(apiDomain, access_token, FormId, message, permission)
{
    $.ajax({
        url: apiDomain + 'api/CustomField/GetSystemList',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            var errorcode = parseInt(r.ErrorCode);
            switch (errorcode) {
                case 0:
                    var html1 = "";
                    var html2 = "";
                    $.each(r.Data, function (idx, data) {
                        if (idx % 2 == 0) {
                            html1 += '<a id="'+data.FieldId+'"  class="btn btn-sm white tooltips" data-container="body" data-placement="top" data-original-title="'+data.FieldName+'" style="margin: 2px 0;width:100%;padding: 7px 7px;">' +
                                                '<i class="'+data.IconUrl+'"></i>' +
                                                data.FieldName +
                                            '</a>'
                        }
                        else
                        {
                         
                            html2 += '<a id="' + data.FieldId + '"  class="btn btn-sm white tooltips" data-container="body" data-placement="top" data-original-title="' + data.FieldName + '" style="margin: 2px 0;width:100%;padding: 7px 7px;">' +
                                                '<i class="' + data.IconUrl + '"></i>' +
                                                data.FieldName +
                                            '</a>'
                        }
                    });
                    
                    $("#predefine1").append(html1);
                    $("#predefine2").append(html2);

                    //ẩn button custom field nếu đã có trong form khi mở form ra update
                    form_DisableButtonCustomField();

                    customField_Users(apiDomain, access_token, r.Data, FormId, message, permission);
                    break;
                default: custom_shownotification("error", message.ErrorGetInfo);
                    break;
            }
        },
        error: function (x,s,e) {
            console.log(x.responeText);
        },

    })
}

//custom field user
function customField_Users(apiDomain, access_token,predefine,FormId,message, permission) {
    $.ajax({
        url: apiDomain + 'api/CustomField/GetList?Active=true',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (r) {
            var errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
            switch (errorcode) {
                case 0:
                    //var predefine = JSON.parse(localStorage.getItem("CustomField"));
                    var listcustomfield = predefine.concat(r.Data);
                    localStorage.setItem('CustomField', JSON.stringify(listcustomfield));
                    var html1 = "";
                    var html2 = "";
                    var html3 = "";
                    var html4 = "";
                    $.each(r.Data, function (idx, data) {
                        if (idx % 2 == 0) {
                            html1 += '<a id="' + data.FieldId + '" class="btn btn-sm white tooltips" data-container="body" data-placement="top" data-original-title="' + data.FieldName + '" style="margin: 2px 0;width:100%;padding: 7px 7px;">' +
                                                '<i class="' + data.IconUrl + '"></i>' +
                                                data.FieldName +
                                            '</a>';
                            html3 += '<a id="' + data.FieldId + '" class="btn btn-sm white tooltips" data-container="body" data-placement="top" data-original-title="' + data.FieldName + '" style="margin: 2px 0;width:100%;padding: 7px 7px;">' +
                                               '<i class="fa fa-remove font-dark"></i>' +
                                               data.FieldName +
                                           '</a>';
                        }
                        else {
                            html2 += '<a id="' + data.FieldId + '"  class="btn btn-sm white tooltips" data-container="body" data-placement="top" data-original-title="' + data.FieldName + '" style="margin: 2px 0;width:100%;padding: 7px 7px;">' +
                                                '<i class="' + data.IconUrl + '"></i>' +
                                                data.FieldName +
                                            '</a>';
                            html4 += '<a id="' + data.FieldId + '"  class="btn btn-sm white tooltips" data-container="body" data-placement="top" data-original-title="' + data.FieldName + '" style="margin: 2px 0;width:100%;padding: 7px 7px;">' +
                                                '<i class="fa fa-remove font-dark"></i>' +
                                                data.FieldName +
                                            '</a>';
                        }
                    });

                    $("#usercustom1").append(html1);
                    $("#usercustom2").append(html2);
                    $("#delete-usercustom1").append(html3);
                    $("#delete-usercustom2").append(html4);

                    if (permission.IsParent) {
                        if (r.Data.length % 2) {
                            $('#usercustom2').append('<a id="create-custom-field" class="btn btn-sm green-jungle" style="margin: 2px 0;width:100%;padding: 7px 7px;"><i class="fa fa-plus"></i>' + message.Create + '</a>');
                        }
                        else {
                            $('#usercustom1').append('<a id="create-custom-field" class="btn btn-sm green-jungle" style="margin: 2px 0;width:100%;padding: 7px 7px;"><i class="fa fa-plus"></i>' + message.Create + '</a>');

                        }
                    } else {
                        if(parseInt(permission.ManageCustomField)==2){
                            if (r.Data.length % 2) {
                                $('#usercustom2').append('<a id="create-custom-field" class="btn btn-sm green-jungle" style="margin: 2px 0;width:100%;padding: 7px 7px;"><i class="fa fa-plus"></i>' + message.Create + '</a>');
                            }
                            else {
                                $('#usercustom1').append('<a id="create-custom-field" class="btn btn-sm green-jungle" style="margin: 2px 0;width:100%;padding: 7px 7px;"><i class="fa fa-plus"></i>' + message.Create + '</a>');

                            }
                        }
                    }

                    //khi field đã có trong form rồi thì disable nó 
                    form_DisableButtonCustomField();

                    break;
                default: custom_shownotification("error", "Lỗi khi lấy danh sách custom field");
                    break;
            }
        },
        error: function (x, s, e) {
            console.log(x.responeText);
        },

    })
}

//khi field đã có trong form rồi thì disable nó 
function form_DisableButtonCustomField()
{
    $.each(form_listId, function (index, obj) {
       
        $('#pre-custom-field-panel' + ' #' + obj).prop("disabled", true);
        $('#pre-custom-field-panel' + ' #' + obj).attr("disabled", true);

        $('#user-custom-field-panel' + ' #' + obj).prop("disabled", true);
        $('#user-custom-field-panel' + ' #' + obj).attr("disabled", true);
    });
}


//sự kiện click tạo customfile vào form

var headsize = 'sz-nm';
var submitsize = 'bt-sz-nm'
function addField(nameButtonSubmit, nameHeadline) {

    ////đối với pre-custom-field
    //addFieldPre(nameButtonSubmit, nameHeadline)
    ////đối với user-custom-field
    //addFieldUser(nameButtonSubmit, nameHeadline)
    addFieldPreUser(nameButtonSubmit, nameHeadline)
}

function addFieldPreUser(nameButtonSubmit, nameHeadline)
{
    $('#pre-custom-field-panel , #user-custom-field-panel').on('click', 'div div a:not("#create-custom-field")', function () {

        //set biến toàn cục là form đã thay đổi 
        window.savedform = false;

        var id = $(this).attr('id');
        if (parseInt(id) != 10078 && parseInt(id) != 10079) {
            $(this).prop("disabled", true);//khi nhấn tạo field thì disable luôn ngoại trừ 2 ông trên
            $(this).attr("disabled", true);
        }
        form_CustomField(id); //add field vào form theo từng kiểu


        var data = [];
        addLabelStyleNewField();
        addTextStyleNewField();

        if ($('.items li').length == 1 && $('#divheadlineForm').length <=0 && $('#divbtnformSubmit').length<=0) {
            addSubmit(nameButtonSubmit); //tạo button
            addHeadline(nameHeadline); //tạo head line
        }
    });
}

//function addFieldUser(nameButtonSubmit, nameHeadline)
//{
//    $('#user-custom-field-panel').on('click', 'div div a:not("#create-custom-field")', function () {

//        //set biến toàn cục là form đã thay đổi 
//        window.savedform = false;

//        var id = $(this).attr('id');
//        $(this).prop("disabled", true);//khi nhấn tạo field thì disable luôn
//        $(this).attr("disabled", true);
//        form_CustomField(id); //add field vào form theo từng kiểu

//        var data=[];
//        addLabelStyle(data);
//        addTextStyle(data);

//        if ($('.items li').length == 1 && $('#divheadlineForm').length <= 0 && $('#divbtnformSubmit').length <= 0) {
//            addSubmit(nameButtonSubmit); //tạo button
//            addHeadline(nameHeadline);  //tạo headline
//        }
//    });
//}

//sự kiện xóa 1 customfield của user
function form_DeleteCustomField(apiDomain, access_token,message)
{
    $('#select-field-delete').on('click', 'a', function () {
        $('#confirm-delete-customfield-modal').modal('toggle');
        //set biến toàn cục là form đã thay đổi 
        window.savedform = false;
        var id = $(this).attr('id');
        $('#id-custom-field').val(id);

    })
}

function form_ActionDeleteCustomField(apiDomain,access_token, message)
{
    $('#delete-user-custom-field').on('click', function () {
        var id = $('#id-custom-field').val();
        $('#confirm-delete-customfield-modal').modal('toggle');
        $.ajax({
            url: apiDomain + 'api/CustomField/Delete?CustomFieldId=' + id,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            success: function (r) {
                switch (parseInt(r.ErrorCode)) {
                    case 0:
                        $('#select-field-delete #' + id).remove();
                        $('#user-custom-field-panel' + ' #' + id).remove();
                        custom_shownotification('success', messageWarning.Success);
                    
                }
            },
            error: function () {
                custom_shownotification('error', messageWarning.Error);
            }
        })
    })
}

////sự kiện xóa 1 append item

function form_DeleteItem() {
    $('.items').on('click', 'li ._element_action .imgdelete', function () {

        //set biến toàn cục là form đã thay đổi 
        window.savedform = false;
        console.log(fielditemId);
        $(this).parent().parent().parent().remove();

        //kích hoạt lại nút tạo field tương ứng
        $('#user-custom-field-panel' +' #' + fielditemId).prop("disabled", false);
        $('#user-custom-field-panel' + ' #' + fielditemId).attr("disabled", false);

        $('#pre-custom-field-panel' + ' #' + fielditemId).prop("disabled", false);
        $('#pre-custom-field-panel' + ' #' + fielditemId).attr("disabled", false);

        var length = $('.items li').length;

        ////xoa luon header va button neu khong con customfiel nào
        //if (length == 0) {
        //    $('#divbtnformSubmit').remove();
        //    $('#divheadlineForm').remove();

        //}
    })
}

//add field vào form theo từng kiểu

function form_CustomField(id) {
    console.log('id'+id)
    var myJsonString = JSON.parse(localStorage.getItem('CustomField'));
    var object = getObjects(myJsonString, 'FieldId', id);
    var SortId;
    var value = JSON.parse(JSON.stringify(object))[object.length - 1];

    var fieldTypeId = getValues(object, 'FieldTypeId')[0];
    var fieldTypeName = getValues(object, 'FieldTypeName')[0];
    var fieldFormatId = getValues(object, 'FieldFormatId')[0];
    var fieldId = getValues(object, 'FieldId')[0];
    var fieldName = getValues(object, 'FieldName')[0];
    var fieldValue = getValues(value, 'FieldValue');
    var fieldLabel = getValues(object, 'FieldLabel')[0];
    var maxlength = getValues(object, 'MaxLength')[0];
    var fieldLabelPosition = 'left';
    var fieldWidth = '13';
    var isRequired = false;
    if (object.PlaceHolder)
    {
        var placeholder = object.PlaceHolder;
    }
    else {
        var placeholder = "Enter your " + fieldName;
    }

    //nếu là email thì mặt định khi tạo là required
    if (parseInt(id)==1 || parseInt(id)==10)
    {
        isRequired = true;
    }

    var data = {
        fieldId: fieldId,
        fieldName: fieldName,
        fieldTypeId: fieldTypeId,
        fieldTypeName: fieldTypeName,
        fieldFormatId: fieldFormatId,
        fieldValue: fieldValue,
        isRequired: isRequired,
        placeholder: placeholder,
        fieldLabelPosition: fieldLabelPosition,
        fieldWidth: fieldWidth,
        maxlength: maxlength,
    }

    from_ActionCreateCustomField(data);

};

function from_ActionCreateCustomField(data) {
    
    switch (data.fieldTypeId) {
        //text
        case 1:
            switch (data.fieldFormatId) {
                case 1:
                    fieldTextSingle(data);
                    break;
                case 2:
                    fieldTextMulti(data);
                    break;
                case 3:
                    fieldDropDown(data);
                    break;
                case 4:
                    fieldCheckBox(data);
                    break;
                case 5:
                    fieldRadio(data);
                    break


            }
            break;
            //number
        case 2:
            switch (data.fieldFormatId) {
                case 1:
                    fieldNumberSingle(data);
                    break;
                case 2:
                    fieldNumberMulti(data);
                    break;
                case 3:
                    fieldDropDown(data);
                    break;
                case 4:
                    fieldCheckBox(data)
                    break;
                case 5:
                    fieldRadio(data)
                    break;

            }
            break;
            //email
        case 3:
            switch (data.fieldFormatId) {
                case 1:
                    fieldTextSingle(data);
                    break;
            }
            break;
        case 4:
            switch (data.fieldFormatId) {
                case 1:
                    fieldTextSingle(data);
                    break;
            }
            break;
        case 6:
            switch (data.fieldFormatId) {
                case 1:
                    fieldNumberSingle(data);
                    break;
            }
            break;
        case 7:
            switch (data.fieldFormatId) {
                case 1:
                    fieldDateSingle(data);
                    break;
                case 3:
                    fieldDropDown(data);
                    break;
                case 4:
                    fieldCheckBox(data);
                    break;
                case 5:
                    fieldRadio(data);
                    break;
            }
            break;
        case 8:
            switch (data.fieldFormatId) {
                case 1:
                    fieldDateTimeSingle(data);
                    break;
                case 3:
                    fieldDropDown(data);
                    break;
                case 4:
                    fieldCheckBox(data);
                    break;
                case 5:
                    fieldRadio(data);
                    break;
            }
            break;
        case 10:
            switch (data.fieldFormatId) {
                case 3:
                    fieldDropDown(data);
                    break;

                case 5:
                    fieldRadio(data);
                    break;
            }
            break;
        case 11:
            switch (data.fieldFormatId) {
                case 3:
                    fieldDropDown(data);
                    break;
            }
            break;
        case 12:
            switch (data.fieldFormatId) {
                case 3:
                    fieldDropDown(data);
                    break;
            }
            break;
        case 13:
            switch (data.fieldFormatId) {
                case 6:
                    fieldContent(data)
                    break;
            }
        case 14:
            switch (data.fieldFormatId) {
                case 7:
                    fieldLine(data)
                    break;
            }
        

    }
    $("._element_action").css({ 'display': 'none' });
}


function fieldContent(data)
{
    var postion = form_PostionOfContent(data.fieldLabelPosition);
    var random=Math.floor(Math.random() * 90 + 10);
    form_CheckValue(data.fieldValue);
    var newItem = $(document.createElement('li')).attr("id", data.fieldId);
    newItem.after().html(
        '<div id="div' + data.fieldId + random + '" class="custom-padding-item" data-maxlength="' + data.maxlength+'" data-formatId="' + data.fieldFormatId + '" data-typeId="' + data.fieldTypeId + '" data-component="text-box">' +
          controlDelete +
        '<div style="text-align:'+postion+'">' +
              '<span style="color:' + data.placeholder + ';font-size:'+ data.fieldWidth + 'px;" id="Content" class=""  name="' + data.fieldTypeName + '">' + data.fieldName + '</span>' +
        '</div>' +
        '</div>'
        );

    $("#form .items").append(newItem);
    ChangeStyle('div' + data.fieldId);
}

function fieldLine(data)
{
    var random = Math.floor(Math.random() * 90 + 10);
    form_CheckValue(data.fieldValue);
    var newItem = $(document.createElement('li')).attr("id", data.fieldId);
    newItem.after().html(
        '<div id="div' + data.fieldId + random + '" class="custom-padding-item" data-maxlength="' + data.maxlength +'" data-formatId="' + data.fieldFormatId + '" data-typeId="' + data.fieldTypeId + '" data-component="text-box">' +
          controlDelete +
        '<div>' +
              '<span id="' + data.fieldName + '" class="custom-straightline"  name="' + data.fieldTypeName + '">' +
        '</div>' +
        '</div>'
        );

    $("#form .items").append(newItem);
    ChangeStyle('div' + data.fieldId)+ random;
}

function fieldTextSingle(data) {

    form_CheckValue(data.fieldValue);
    var classreq = '';
    if (data.isRequired == true) {
        classreq = 'checkrequired';
    }
    var newItem = $(document.createElement('li')).attr("id", data.fieldId);
        newItem.after().html(
            '<div id="div' + data.fieldId + '" class="custom-padding-item" data-maxlength="' + data.maxlength +'" data-formatId="' + data.fieldFormatId + '" data-typeId="' + data.fieldTypeId + '" data-component="text-box">'
            + controlDelete +
            '<label id="captionname" class="' + classreq + '">' + data.fieldName + '</label>' +
            '<div>' +
                  '<input type="text" id="' + data.fieldName + '" class="form-control input-sm" value="' + data.fieldValue + '" name="' + data.fieldTypeName + '" placeholder="' + data.placeholder +'">' +
                  '<div class="form-control-focus"></div>' +
            '</div>' +
            '</div>'
            );
    
       $("#form .items").append(newItem);
    ChangeStyle('div' + data.fieldId);
}

function fieldDateSingle(data) {
    
    form_CheckValue(data.fieldValue);
    var classreq = '';
    if (data.isRequired == true) {
        classreq = 'checkrequired';
    }
    var newItem = $(document.createElement('li'))
                                .attr("id", data.fieldId);
        newItem.after().html(
            '<div id="div' + data.fieldId + '" class="custom-padding-item" data-maxlength="' + data.maxlength +'" data-formatid="' + data.fieldFormatId + '" data-typeid="' + data.fieldTypeId + '" data-component="text-box">'
            + controlDelete +
            '<label id="captionname" class="' + classreq + '">' + data.fieldName + '</label>' +
            '<div>' +
                 '<input id="' + data.fieldName + '" class="form-control form-control-inline input-sm date-picker" value="' + data.fieldValue + '" name="' + data.fieldTypeName + '" placeholder="' + data.placeholder + '"/>' +
                 '<div class="form-control-focus"></div>' +
            '</div>' +
            '</div>'
     );
        newItem.appendTo("#form .items");
    ChangeStyle('div' + data.fieldId);
    form_datesingle();
}

function fieldDateTimeSingle(data) {
   
    form_CheckValue(data.fieldValue);
    var classreq = '';
    if (data.isRequired == true) {
        classreq = 'checkrequired';
    }
       var newItem = $(document.createElement('li'))
                            .attr("id", data.fieldId);
        newItem.after().html(
            '<div id="div' + data.fieldId + '" class="custom-padding-item form-group" data-maxlength="' + data.maxlength +'" data-formatid="' + data.fieldFormatId + '" data-typeid="' + data.fieldTypeId + '" data-component="text-box">' + controlDelete +
                '<label id="captionname" class="' + classreq + '">' + data.fieldName + '</label>' +
             
                '<div class="form_datetime"> ' +
                    '<input id="' + data.fieldName + '" class="form-control" value="' + data.fieldValue + '" name="' + data.fieldTypeName + '" placeholder="' + data.placeholder + '" />' +
                    //'<span class="input-group-btn">' +
                    //    '<button class="btn default date-set" type="button">' +
                    //        '<i class="fa fa-calendar"></i>' +
                    //    '</button>' +
                    //'</span>' +
                  '</div>' +
              '</div>'

              );
        newItem.appendTo("#form .items");
    ChangeStyle('div' + data.fieldId)
    from_datetimesingle();
}


function fieldNumberSingle(data) {
    
    form_CheckValue(data.fieldValue);
    var classreq = '';
    if (data.isRequired == true) {
        classreq = 'checkrequired';
    }
      var newItem = $(document.createElement('li'))
                                .attr("id", data.fieldId);
      newItem.after().html('<div id="div' + data.fieldId + '" class="custom-padding-item" data-maxlength="' + data.maxlength +'" data-formatid="' + data.fieldFormatId + '" data-typeid="' + data.fieldTypeId + '" data-component="text-box">' +
            controlDelete + '<label id="captionname" class="' + classreq + '">' + data.fieldName + '</label>' +
     '<div><input type="text" id="' + data.fieldName + '" class="form-control input-sm" onkeypress="validate(event)" value="' + data.fieldValue + '" name="' + data.fieldTypeName + '" placeholder="' + data.placeholder + '"></div></div>'
     );
        newItem.appendTo("#form .items");
    ChangeStyle('div' + data.fieldId);
}

function fieldTextMulti(data)
{
    form_CheckValue(data.fieldValue);
    var classreq = '';
    if (data.isRequired == true) {
        classreq = 'checkrequired';
    }
     var newItem = $(document.createElement('li'))
                                .attr("id", data.fieldId);
        newItem.after().html(
            '<div id="div' + data.fieldId + '" class="custom-padding-item" data-maxlength="' + data.maxlength +'" data-formatid="' + data.fieldFormatId + '" data-typeid="' + data.fieldTypeId + '" data-component="text-multi">'
              + controlDelete +
           '<label id="captionname" class="' + classreq + '">' + data.fieldName + '</label>' +
                 '<div>' +
                    '<textarea id="' + data.fieldName + '" class="form-control" row="3" value="' + data.fieldValue + '" name="' + data.fieldTypeName + '" placeholder="' + data.placeholder + '"></textarea>' +
                    '<div class="form-control-focus"></div>' +
                 '</div>' +
            '</div>'
                 );
        newItem.appendTo("#form .items");
    ChangeStyle('div' + data.fieldId);
}

function fieldNumberMulti(data) {
 
    form_CheckValue(data.fieldValue);
    var classreq = '';
    if (data.isRequired == true) {
        classreq = 'checkrequired';
    }
    var newItem = $(document.createElement('li'))
                                .attr("id", data.fieldId);
        newItem.after().html(
            '<div id="div' + data.fieldId + '" class="custom-padding-item" data-maxlength="' + data.maxlength +'" data-formatid="' + data.fieldFormatId + '" data-typeid="' + data.fieldTypeId + '" data-component="text-box">'
            + controlDelete +
           '<label id="captionname" class="' + classreq + '">' + data.fieldName + '</label>' +
               
            '<div>' +
                '<textarea id="' + data.fieldName + '" class="form-control" row="3" value="' + data.fieldValue + '" onkeypress="validate(event)" name="' + data.fieldTypeName + '" placeholder="' + data.placeholder + '"></textarea>' +
                '<div class="form-control-focus"></div>' +
            '</div>' +
            '</div>'
                 );
        newItem.appendTo("#form .items");
    ChangeStyle('div' + data.fieldId);
   
}


function fieldDropDown(data)
{
    form_CheckValue(data.fieldValue);
    if (data.isRequired == true) {
        var newItem = $(document.createElement('li'))
                            .attr("id", data.fieldId);
        newItem.after().html('<div id="div' + data.fieldId + '" class="custom-padding-item" data-maxlength="' + data.maxlength +'" data-formatid="' + data.fieldFormatId + '" data-typeid="' + data.fieldTypeId + '"data-component="dropdown-list">' +
            controlDelete + '<label id="captionname" class="checkrequired">' + data.fieldName + '</label>' +
                 '<div id="' + data.fieldId + '"></div></div>'
                 );
    }
    else
    {
        var newItem = $(document.createElement('li'))
                           .attr("id", data.fieldId);
        newItem.after().html('<div id="div' + data.fieldId + '" class="custom-padding-item" data-maxlength="' + data.maxlength +'" data-formatid="' + data.fieldFormatId + '" data-typeid="' + data.fieldTypeId + '"data-component="dropdown-list">' +
            controlDelete + '<label id="captionname">' + data.fieldName + '</label>' +
                 '<div id="' + data.fieldId + '"></div></div>'
                 );
    }
    newItem.appendTo("#form .items");
    
    var arrayLength = data.fieldValue.length;
    var options = new Array();
    for (var i = 0; i < arrayLength; i++) {
        options.push('<option value="' + data.fieldValue[i] + '">' + data.fieldValue[i] + '</option>');
        //Do something
    }
    $('<select id="' + data.fieldId + '" type="text" id="' + data.fieldName + '" class="form-control" name="' + data.fieldTypeName + '"/>').append(options.join('')).appendTo('#div' + data.fieldId);
    ChangeStyle('div' + data.fieldId);
}

function fieldCheckBox(data)
{
    
    form_CheckValue(data.fieldValue);
    if (data.isRequired == true) {
        var newItem = $(document.createElement('li'))
                        .attr("id", data.fieldId);
        newItem.after().html('<div id="div' + data.fieldId + '" class="custom-padding-item" data-maxlength="' + data.maxlength +'" data-formatid="' + data.fieldFormatId + '" data-typeid="' + data.fieldTypeId + '" data-component="check-box">' + controlDelete +
                                  '<label id="captionname" class="checkrequired">' + data.fieldName + '</label>' +
               
                                      '<div class="md-checkbox-list">' +
                                        '</div>' +
                                        '</div>'
                                        );
    }
    else {
        var newItem = $(document.createElement('li'))
                            .attr("id", data.fieldId);
        newItem.after().html('<div id="div' + data.fieldId + '" class="custom-padding-item" data-maxlength="' + data.maxlength +'" data-formatid="' + data.fieldFormatId + '" data-typeid="' + data.fieldTypeId + '" data-component="check-box">' + controlDelete +
                                    '<label id="captionname">' + data.fieldName + '</label>' +
                                      '<div class="md-checkbox-list">' +
                                        '</div>' +
                                        '</div>'
                                        );
    }
    newItem.appendTo("#form .items");
    var arrayLength = data.fieldValue.length;
   
    var options = new Array();
    for (var i = 0; i < arrayLength; i++) {
        options.push('<div class="md-checkbox" id="md-checkbox-'+i+'">' +
        '<input type="checkbox" id="' + data.fieldId + i + '" value="' + data.fieldValue[i] + '" id="' + data.fieldName + '" class="md-check" name="' + data.fieldTypeName + '">' +
        '<label  for="' + data.fieldId + i + '">' +
            '<span class="inc"></span>' +
            '<span class="check"></span>' +
            '<span class="box"></span> ' + data.fieldValue[i] + ' </label>' +
    '</div>');
 
    }
    $('#div' + data.fieldId + ' .md-checkbox-list').append(options.join(''));
    ChangeStyle('div' + data.fieldId)
}

function fieldRadio(data) {
    form_CheckValue(data.fieldValue);
  
    if (data.isRequired == true) {

        var newItem = $(document.createElement('li')).attr("id", data.fieldId);
        newItem.after().html('<div id="div' + data.fieldId + '" class="custom-padding-item" data-maxlength="' + data.maxlength +'" data-formatid="' + data.fieldFormatId + '" data-typeid="' + data.fieldTypeId + '" data-component="radio-button"> ' +
                            controlDelete + '<label id="captionname" class="checkrequired">' + data.fieldName + '</label>' +
              
                                        '<div class="md-radio-list">' +
                                        '</div>' +
                                        '</div>'
                                       );
    }
    else {
        var newItem = $(document.createElement('li')).attr("id", data.fieldId);
        newItem.after().html('<div id="div' + data.fieldId + '" class="custom-padding-item" data-maxlength="' + data.maxlength +'" data-formatid="' + data.fieldFormatId + '" data-typeid="' + data.fieldTypeId + '" data-component="radio-button"> ' +
                            controlDelete + '<label id="captionname">' + data.fieldName + '</label>' +
                                        '<div class="md-radio-list">' +
                                        '</div>' +
                                        '</div>'
                                       );
    }
    newItem.appendTo("#form .items");
    var arrayLength = data.fieldValue.length;
    var options = new Array();
    for (var i = 0; i < arrayLength; i++) {
        options.push('<div class="md-radio" id="md-radio-'+i+'">' +
        '<input type="radio" id="' + data.fieldId + i + '" name="' + data.fieldName + '" value="' + data.fieldValue[i] + '" class="md-radiobtn" name="' + data.fieldTypeName + '">' +
        '<label for="' + data.fieldId + i + '">' +
            '<span class="inc"></span>' +
            '<span class="check"></span>' +
            '<span class="box"></span> ' + data.fieldValue[i] + ' </label>' +
    '</div>');
    }
    $('#div' + data.fieldId + ' .md-radio-list').append(options.join(''));

    ChangeStyle('div' + data.fieldId);
   
   
}




function form_CheckValue(fieldValue) {
    if (!fieldValue) {
        fieldValue = '';
    }
}

//chuỗi div control delete field
var controlDelete = '<div class="_element_action"><i class="imgdelete" alt="Delete." title="Delete"></i></div>';

// chuỗi control delete field value trong model tạo field
var deleteValueField = '<a id="deleteAnotherValue" class="btn btn-circle btn-icon-only"><i class="icon-trash"></i></a>';


//số field được tạo
//var count = 0;
//change style input khi thêm 
var md = false;
function ChangeStyle(id) {
    var style = 'form-group';
    if (md == true) {
        style = 'form-group form-md-line-input form-md-floating-label has-info';
        $("#" + id).addClass(style);
    }
    else {
        $("#" + id).addClass(style);
    }

    $('.items li div').css({ 'border': '' });
    $('.items input').attr('disabled', false).css({cursor:'default'});
    $('.items select').attr('disabled', false).css({ cursor: 'default' });


}


//click vào content form ngoài trừ form thì xóa các css border , ẩn hình ẩn tab style
function form_ClickInContentForm() {
    $('#contentForm').on('click', function (evt) {
        if (evt.target.id == ".controlInput")
            return;
        if ($(evt.target).closest('.controlInput').length)
            return;

        $('.items li div').css({ 'border': ''});
        $("._element_action").css({ 'display': 'none' });

        $('#divheadlineForm').css({ 'border': '' });
        $('#divbtnformSubmit').css({ 'border': '' });
        $('.controlInput').css({ 'border': '' });
        //đồng thời ẩn tab setting 
        form_HideTabStyle();
    });
}


function form_ClickInItems() {
    //  hiển thị nút xóa khi click field
    $('.items').on('click', 'li', function () {

        var id = $(this).attr('id');
        var childrenid = $(this).children().attr('id')

        fielditemId = id; //lấy id để kiểm tra nếu xóa field thì enable nut tạo field tương ứng
        fielditemIdContent=childrenid;
        //ẩn boder, nút xóa của tất cả các item.
        formHideBorderItems();

        //hiện border và nút xóa của item được click

        $('#' + childrenid).css({ 'border': '1px solid #2196F3' });
        if (id != ItemNotDelete) {    //nếu là item mặt định thì không hiện nút xóa
            $('#' + childrenid).children('._element_action').css({ 'display': '' });
            $('#' + childrenid).children().children('i').addClass('icon-trash')
            $('#' + childrenid).children().children('i').css({ 'color': '#2196F3' })
        }
        //ẩn tất cả các tab pane trong tab style
        formHideAllTabPaneStyle();

        if (parseInt(id) != 10078 && parseInt(id) != 10079) {
            //show tab pane setting style field
            form_ShowTabPaneStyleField(id);
            //đồng thời hiển thị table setting style
           
        }
        else {
            form_ShowTabPaneStyleContent(childrenid)
        }
        form_ShowTabStyle();
    })
};



//click vào headline form
function form_ClickInHeadLine() {
    $('.controlInput').on('click', '#divheadlineForm', function () {

        
        //xóa style border , ẩn nút xóa của các items (nếu có item nào đã được click trước đó)
        formHideBorderItems();
        $('#divheadlineForm').css({ 'border': '1px solid #2196F3' });

        //ẩn tất cả các tab pane trong tab style
        formHideAllTabPaneStyle();

        //show tab pane setting style headerline
        form_ShowTabPaneStyleHeaderline();

        //mỏ tab style
        form_ShowTabStyle();
        
        
    })
}

function form_PostionOfContent(Postion)
{
    var pcontent = 1;
    switch (Postion) {
        case 'left':
            pcontent = 1;
            break;
        case 'center':
            pcontent = 2;
            break;
        case 'right':
            pcontent = 3;
            break;
        case 1:
            pcontent = 'left';
            break;
        case 2:
            pcontent = 'center';
            break;
        case 3:
            pcontent = 'right';
            break;
        default:
            pcontent = 1;
            break;
    }
    return pcontent;
}

//click vào button form
function form_ClickInButton() {
    $('.controlInput').on('click', '#divbtnformSubmit #_btnformSubmit', function () {

       
        //xóa style border , ẩn nút xóa của các items (nếu có item nào đã được click trước đó)
        formHideBorderItems();
        $('#divbtnformSubmit').css({ 'border': '1px solid #2196F3' });

        //ẩn tất cả các tab pane trong tab style
        formHideAllTabPaneStyle();

        //hiện tab pane setting style button
        form_ShowTabPaneStyleButton();

        //hiện tab style
        form_ShowTabStyle();
    })
}

function form_ClickInImage() {
    //$('#custom-image-form').on('click', function () {

    //    formHideBorderItems();
    //    $('.controlInput').css({ 'border': '1px solid #2196F3' });

    //    //xóa style border , ẩn nút xóa của các items (nếu có item nào đã được click trước đó)
       
       
    //    //ẩn tất cả các tab pane trong tab style
    //    formHideAllTabPaneStyle();

    //    //hiện tab style
    //    form_ShowTabStyle();
    //});
}

function form_ClickInControl()
{
    $('#showaddimage,#custom-image-form').on('click', function (event) {
        
            console.log('alo');
            //xóa style border , ẩn nút xóa của các items (nếu có item nào đã được click trước đó)
            formHideBorderItems();

            $('.controlInput').css({ 'border': '1px solid #2196F3' });

            //ẩn tất cả các tab pane trong tab style
            formHideAllTabPaneStyle();


            //hiện tab image
            form_ShowTabImage();

            //hiện tab style
            form_ShowTabStyle();
    });
}

function form_ShowTabImage()
{
    $('#tab-pane-setting-image-form').removeClass('display-hide');
    $('#tab-pane-setting-background-form').removeClass('display-hide');
}

//ẩn tất cả các tab pane trong tab style
function formHideAllTabPaneStyle()
{
    if( !$('#tab-pane-setting-style-field').hasClass('display-hide'))
    {
        $('#tab-pane-setting-style-field').addClass('display-hide');
    }
    if (!$('#tab-pane-setting-style-headerline').hasClass('display-hide')) {
        $('#tab-pane-setting-style-headerline').addClass('display-hide');
    }
    if (!$('#tab-pane-setting-style-content').hasClass('display-hide')) {
        $('#tab-pane-setting-style-content').addClass('display-hide');
    }
    if (!$('#tab-pane-setting-style-button').hasClass('display-hide')) {
        $('#tab-pane-setting-style-button').addClass('display-hide');
    }

    if (!$('#tab-pane-setting-image-form').hasClass('display-hide')) {
        $('#tab-pane-setting-image-form').addClass('display-hide');
    }
    if (!$('#tab-pane-setting-background-form').hasClass('display-hide')) {
        $('#tab-pane-setting-background-form').addClass('display-hide');
    }

    if (!$('#tab-pane-label-textbox-field').hasClass('display-hide')) {
        $('#tab-pane-label-textbox-field').addClass('display-hide');
    }

    if (!$('#tab-pane-setting-style-text-content').hasClass('display-hide')) {
        $('#tab-pane-setting-style-text-content').addClass('display-hide');
    }

   
}

//hiện tab pane style field
function form_ShowTabPaneStyleField(id)
{
    //load lable
    var label = $('#div' + id).children('#captionname').text();
    //alert(label);
    $('#input-lable-custom-field-form').val(label);

    //load placeholder
    var component = $('#div' + fielditemId).data('component');
    console.log(component);
    switch (component) {
        case 'text-box':
            var placeholder = $('#div' + fielditemId + ' input[type="text"]').attr('placeholder');
            break;
        case 'text-multi':
            var placeholder = $('#div' + fielditemId + ' textarea').attr('placeholder');
            break;
        default:
            var placeholder = $('#div' + fielditemId + ' input[type="text"]').attr('placeholder');
            break;
    }

    // alert(placeholder);
    $('#input-placeholder-custom-field-form').val(placeholder);

    
    //load check required
    if ($('#div' + fielditemId).children('#captionname').hasClass('checkrequired')) {
        $('#required-custom-field-form').prop('checked', true);
    }
    else {
        $('#required-custom-field-form').prop('checked', false);
    }

   
    $('#required-custom-field-form').prop('disabled', false);


    $('#tab-pane-setting-style-field').removeClass('display-hide');
    $('#tab-pane-label-textbox-field').removeClass('display-hide');
    

}



//hiện tab pane style headerline
function form_ShowTabPaneStyleHeaderline() {
    var label = $('#_headlineFormSubmit').text();
    $('#input-lable-headerline-form').val(label);

    $('#header-alignment input:radio:checked').val('right');

    $('#tab-pane-setting-style-headerline').removeClass('display-hide');
}


//hiện tab pane style content
function form_ShowTabPaneStyleContent(id) {
    //$('#input-lable-content-form').val('');
    //$('#color-content-form').minicolors('value', '#0f0f0f');
    //var slider_size_content = document.getElementById('slider-size-content');
    //slider_size_content.noUiSlider.set(13);

    var label = $('#' + id + ' #Content').text();
    $('#input-lable-content-form').val(label);

    $('#tab-pane-setting-style-text-content').removeClass('display-hide');
   
}

//hiện tab pane style button
function form_ShowTabPaneStyleButton() {

    var lable = $('#_btnformSubmit').text().trim();
    $('#input-lable-button-form').val(lable);
    $('#tab-pane-setting-style-button').removeClass('display-hide');
}

//ẩn tab style
function form_HideTabStyle() {
    $('#tab-setting-style').removeAttr('data-toggle');
    $('#tab-select-field').tab('show');
};

//hiển thị tab style
function form_ShowTabStyle() {
    $('#tab-setting-style').attr('data-toggle', 'tab').tab('show');

};

//sự kiện change input ->change label placeholder field, header,button
function form_ChangeSettingStyle() {

    //change lablel content
    form_ChangeLabelContent();

    form_ChangeColorContent();

    form_ChangeSizeContent();

    form_ChangeAlignContent();

    //change lable fiel
    form_ChangeLabelField();

    //change placeholder field
    form_ChangePlaceHolderField();


    //change lable button
    form_ChangeLableButton();

    //change lable header line 
    ChangeLabelHeaderline();

    //change check required field
     form_ChangeRequired();

    //change text header align
    form_ChangeAlignHeaderLine();

    //change size text header line 
    form_ChangeSizeHeaderLine();

    //change color headerline 
    form_ChangeColorHeaderLine();

    //change align button
    form_ChangeAlignButton();

    //change size button
    form_ChangeSizeButton();

    //change width button
    form_ChangeWidthButton();

    //change color text button
    form_ChangeColorTextButton();

    //change color hover text button 
    form_ChangeColorHoverTextButton();

    //change background button 
    form_ChangeColorButton();

    //change hover color button
    form_ChangeColorHoverButton();

    //change position image
    form_ChangePositionImage();

    //show hide image
    form_ShowImage();

    //change size background-size
    form_ChangeBackgroundSize();

    //change postion
    form_ChangeBackgroundPosition();

    //change backgroundcolor
    form_ChangeBackgroundFrom();

    //change repeat
    form_ChangeRepeat();

    //change color label
    form_ChangeLabelColor();

    //change color textbox
    form_ChangeControlColor();

    //change color border
    form_ChangeControlBorder();

    //change radius
    form_ChangeRadiusControl();

    //change size of label
    form_ChangeSizeLabel();

    //change position label
    form_ChangeLabelPosition();

    //change size of textbox
    form_ChangeSizeTextbox();

    //remove banner
    form_RemoveBanner();

    //remove background-image
    form_RemoveBackground();

}

function form_RemoveBanner()
{
    $('#remove-banner').on('click', function () {
        $('#thubnail-image-form img').attr('src', '');
        $('#tab-pane-setting-image-form .fileinput-preview.fileinput-exists.thumbnail img').attr('src', '');
        $('#fileinput-banner').fileinput('clear');
        $('#custom-image-form').attr('src', '');
        $('.controlInput #image').addClass('hide');
        $('#show-image-form').prop('checked', false);
    });
}

function form_RemoveBackground()
{
    $('#remove-background').on('click', function () {
        $('#thubnail-background-form img').attr('src', '');
        $('#tab-pane-setting-background-form .fileinput-preview.fileinput-exists.thumbnail img').attr('src', '');
        $('#fileinput-background').fileinput('clear');
        $('.controlInput').css('background-image', 'none');
    })
}

function form_ChangeSizeLabel() {
    var slider1 = document.getElementById('slider-size-label');
    slider1.noUiSlider.on('update', function (values, handle) {
        $('.controlInput #captionname').css('font-size', parseInt(values[handle]));
    });
}

function form_ChangeSizeTextbox() {
    var slide2 = document.getElementById('slider-size-textbox');
    slide2.noUiSlider.on('update', function (values, handle) {
        var size = parseInt(values[handle]);
        form_SetHeightControl(size)
    });
}

function form_SetHeightControl(size){
    $('.controlInput input[type=text]').css('height', size);
    $('.controlInput input[type=number]').css('height', size);
    $('.controlInput input[name=Date]').css('height', size);
    $('.controlInput input[name=DateTime]').css('height', size);
    $('.controlInput select').css('height', size);
    //$('.controlInput textarea').css('height', size); mặt đinh row

    $('.controlInput input[type=text]').css('font-size', size / 2);
    $('.controlInput input[type=number]').css('font-size', size / 2);
    $('.controlInput input[name=Date]').css('font-size', size / 2);
    $('.controlInput input[name=DateTime]').css('font-size', size / 2);
    $('.controlInput select').css('font-size', size / 2);
    $('.controlInput textarea').css('font-size', size / 2);
}

//change lable field
function form_ChangeLabelField() {
    $('#input-lable-custom-field-form').bind("keypress keyup blur", function () {

        //set biến toàn cục là form đã thay đổi 
        window.savedform = false;
        console.log(window.savedform);

        var lable = $('#input-lable-custom-field-form').val();
        $('#div' + fielditemId).children('#captionname').text(lable);
        //if (lable) {
        //    $('#div' + fielditemId).children('#captionname').removeClass('hide');
        //    $('#div' + fielditemId).children('#captionname').text(lable);
        //}
        //else {
        //    $('#div' + fielditemId).children('#captionname').addClass('hide');
        //    $('#div' + fielditemId).children('#captionname').text('');
        //}
        
    });
}

//change placeholder field
function form_ChangePlaceHolderField() {
    $('#input-placeholder-custom-field-form').bind("keypress keyup blur", function () {

        //set biến toàn cục là form đã thay đổi 
        window.savedform = false;
        console.log(window.savedform);

        var placeholder = $('#input-placeholder-custom-field-form').val();
        $('#div' + fielditemId + ' input').attr('placeholder', placeholder);
        $('#div' + fielditemId + ' textarea').attr('placeholder', placeholder);
    })
}

function form_ChangeLabelContent()
{
    $('#input-lable-content-form').bind("keypress keyup blur", function () {

        //set biến toàn cục là form đã thay đổi 
        window.savedform = false;
        console.log(window.savedform);
        console.log('có xảy ra');
        var label = $(this).val();
        if (label) {
            $('#' + fielditemIdContent + ' #Content').text(label);
        }
        else {
            $('#' + fielditemIdContent + ' #Content').text('Vui lòng nhập nội dung');
        }
    })

}

//change erorr meessage
//function form_ChangeErrorMessage()
//{
//    $('#input-errormessage-custom-field-form').bin('keypress keyup blur',function(){
//        var errormessage = $('#input-errormessage-custom-field-form').val();
//        $('#div' + fielditemId +' input[name="Text"]').attr('placeholder', placeholder);
//    })
//}

//change check required
function form_ChangeRequired()
{
    $('#required-custom-field-form').click(function () {

        //set biến toàn cục là form đã thay đổi 
        window.savedform = false;
        console.log(window.savedform);

        if ($(this).prop('checked')==true) {
            
            $('#div' + fielditemId).children('#captionname').addClass('checkrequired');
        }
        else {
            $('#div' + fielditemId).children('#captionname').removeClass('checkrequired');
        }
    });
}

//change lable button
function form_ChangeLableButton() {
    $('#input-lable-button-form').bind("keypress keyup blur", function () {

        //set biến toàn cục là form đã thay đổi 
        window.savedform = false;
        console.log(window.savedform);
        var lable = $('#input-lable-button-form').val();
        $('#_btnformSubmit').text(lable);
    });
}

//change text header
function ChangeLabelHeaderline() {
    $('#input-lable-headerline-form').bind("keypress keyup blur", function () {

        //set biến toàn cục là form đã thay đổi 
        window.savedform = false;
        console.log(window.savedform);
        var lable = $('#input-lable-headerline-form').val();
        if (lable) {
            $('#_headlineFormSubmit').text(lable);
        }
        else {
            $('#_headlineFormSubmit').text('Nhập tiêu đề ở đây');
        }
    });
}

//change text header align
function form_ChangeAlignHeaderLine() {
    $('#header-alignment').on('change', function () {

        //set biến toàn cục là form đã thay đổi 
        window.savedform = false;
        console.log(window.savedform);
        var align = $('#header-alignment input:radio:checked').val();
        //alert(align);
        $('.controlInput .headline').css({ 'text-align': align });
       
    })
}

//change size header 
function form_ChangeSizeHeaderLine()
{
    //$('#header-size').on('change', function () {

    //    //set biến toàn cục là form đã thay đổi 
    //    window.savedform = false;
    //    console.log(window.savedform);
    //    var size = $('#header-size input:radio:checked').val();
    //    form_RemoveClassSize('#_headlineFormSubmit');
    //    $('.controlInput #_headlineFormSubmit').addClass(size);
    //})
    var slider3 = document.getElementById('slider-size-header');
    slider3.noUiSlider.on('update', function (values, handle) {
        $('.controlInput #_headlineFormSubmit').css('font-size', parseInt(values[handle]));
    });
}

//change color headerline 
function form_ChangeColorHeaderLine()
{
    $('#color-headline-form').on('change', function () {

        var color = $('#color-headline-form').val();
       // alert(color);
        $('.controlInput #_headlineFormSubmit').css('color',color);
    })
}

function form_ChangeSizeContent()
{
    var slider = document.getElementById('slider-size-content');
    slider.noUiSlider.on('update', function (values, handle) {
        $('#' + fielditemIdContent + ' #Content').css('font-size', parseInt(values[handle]) + 'px');
        $('#' + fielditemIdContent + ' #Content').attr('font-size', parseInt(values[handle]));
    });
}

function form_ChangeAlignContent()
{
    $('#content-alignment').on('change', function () {

        //set biến toàn cục là form đã thay đổi 
        window.savedform = false;
        var align = $('#content-alignment input:radio:checked').val();
        $('#' + fielditemIdContent + ' #Content').parent().css({ 'text-align': align });
        $('#' + fielditemIdContent + ' #Content').attr('text-align', align);
    })
}

function form_ChangeColorContent()
{
    $('#color-content-form').on('change', function () {
        var color = $(this).val();
        $('#' + fielditemIdContent + ' #Content').css('color', color);
     
    })
}

function form_ChangeBackgroundFrom() {
    $('#color-bacground-form').on('change', function () {

        var color = $('#color-bacground-form').val();
        if (color) {
            $('.controlInput').css('background-color', color);
        }
        else {
            $('.controlInput').css('background-color', 'transparent');
        }
    })
}

//change align button
function form_ChangeAlignButton()
{
    $('#button-alignment').on('change', function () {

        //set biến toàn cục là form đã thay đổi 
        window.savedform = false;
        console.log(window.savedform);
        var align = $('#button-alignment input:radio:checked').val();
        $('.controlInput .submit').css({ 'text-align': align });
    })
}

//change size button
function form_ChangeSizeButton()
{

    var slider5 = document.getElementById('slider-size-height-button');
    slider5.noUiSlider.on('update', function (values, handle) {
        window.savedform = false;
        var size = parseInt(values[handle]);
        form_SizeButtonHeight(size);
    });
}

function form_SizeButtonHeight(size)
{
    var classsize = {};
    switch (size) {
        case 1:
            classsize = { 'font-size': '11px', 'padding': '3px 8px' };
            break;
        case 2:
            classsize = { 'font-size': '11px', 'padding': '7px 18px' };
            break;
        case 3:
           classsize = { 'font-size': '12px', 'padding': '9px 14px 8px' };
            break;
        case 4:
            classsize = { 'font-size': '16px', 'padding': '12px 26px 10px' };
            break;
        case 5:
           classsize = { 'font-size': '20px', 'padding': '15px 36px 12px' };
           break;
        default:
            classsize = { 'font-size': '12px', 'padding': '9px 14px 8px' };
            break;

    }
    $('.controlInput #_btnformSubmit').css(classsize);
}

function form_ChangeWidthButton() {
    var slider4 = document.getElementById('slider-size-button');
    slider4.noUiSlider.on('update', function (values, handle) {
        $('.controlInput #_btnformSubmit').css('width', parseInt(values[handle]) +'%');
    });
}






//change color text button
function form_ChangeColorTextButton()
{
    $('#color-button-text-form').on('change', function () {

        var color = $('#color-button-text-form').val();
        $('#_btnformSubmit').css('color', color);
    })
}

//change color hover text button
function form_ChangeColorHoverTextButton()
{
    $('#color-button-text-hover-form').on('change', function () {

        var color = $('#color-button-text-hover-form').val();
        $('#_btnformSubmit').mouseover(function(){
            $(this).css('color', color);
        });
        $('#_btnformSubmit').mouseout(function () {
            $(this).css('color', $('#color-button-text-form').val());
        });
    })
}

//change color button
function form_ChangeColorButton() {
    $('#color-button-form').on('change', function () {

        var color = $('#color-button-form').val();
       
        $('#_btnformSubmit').css({
            'background-color': color + ' !important',
            'border-color': color + ' !important',
        })
    })
}

//change hover color button
function form_ChangeColorHoverButton()
{
    $('#color-button-hover-form').on('change', function () {

        var color = $('#color-button-hover-form').val();
        $('#_btnformSubmit').mouseover(function () {
            $(this).css({
                'background-color': color + ' !important',
                'border-color': color + ' !important',
            })
        });
        $('#_btnformSubmit').mouseout(function () {
            $(this).css({
                'background-color': $('#color-button-form').val() + ' !important',
                'border-color': $('#color-button-form').val() + ' !important',
            })
        });
    })
}

function form_ChangePositionImage() {
    $('#image-position').on('change', function () {
        window.savedform = false;
        var position = $('#image-position input:radio:checked').val();
        if ($('#show-image-form').prop('checked')) {
            form_SetPositionImage(position);
        }


    });
}

function form_SetPositionImage(position) {
    switch (position) {
        case 'top':
            form_ResetPositionImage();
            $('.controlInput #image').insertBefore($('.controlInput #form'));
            $('.controlInput').css('max-width', '460px');
            break;
        case 'bottom':
            form_ResetPositionImage()
            $('.controlInput #image').insertAfter($('.controlInput #form'));
            $('.controlInput').css('max-width', '460px');
            break;
        case 'left':
            form_ResetPositionImage();
            $('.controlInput #image').addClass('pull-left');
            $('.controlInput #form').addClass('pull-right');
            $('.controlInput #form').css('width', '50%');
            $('.controlInput #image').css('width', '48%');
            $('.controlInput').css('max-width', '960px');

            break;
        case 'right':
            form_ResetPositionImage();
            $('.controlInput #image').addClass('pull-right');
            $('.controlInput #form').addClass('pull-left');
            $('.controlInput #form').css('width', '50%');
            $('.controlInput #image').css('width', '48%');
            $('.controlInput').css('max-width', '960px');
            break;
        case 0:
            form_ResetPositionImage();
            $('.controlInput #image').insertBefore($('.controlInput #form'));
            $('.controlInput').css('max-width', '460px');

            break;
        case 1:
            form_ResetPositionImage()
            $('.controlInput #image').insertAfter($('.controlInput #form'));
            $('.controlInput').css('max-width', '460px');
            break;
        case 2:
            form_ResetPositionImage();
            $('.controlInput #image').addClass('pull-left');
            $('.controlInput #form').addClass('pull-right');
            $('.controlInput #form').css('width', '50%');
            $('.controlInput #image').css('width', '48%');
            $('.controlInput').css('max-width', '960px');
            break;
        case 3:
            form_ResetPositionImage();
            $('.controlInput #image').addClass('pull-right');
            $('.controlInput #form').addClass('pull-left');
            $('.controlInput #form').css('width', '50%');
            $('.controlInput #image').css('width', '48%');
            $('.controlInput').css('max-width', '960px');
            break;
        default:
            $('.controlInput').css('max-width', '460px');
            break;
    }
}

function form_ShowImage() {
    $('#show-image-form').change(function () {
        if ($('#show-image-form').prop('checked')) {
            form_ResetPositionImage();
            $('.controlInput #image').removeClass('hide');
        }
        else {
            form_ResetPositionImage();
            $('.controlInput #image').addClass('hide');
        }
    });
}

function form_ResetPositionImage() {
    $('.controlInput #image').removeClass('pull-left');
    $('.controlInput #image').removeClass('pull-right');
    $('.controlInput #form').removeClass('pull-right');
    $('.controlInput #form').removeClass('pull-left');
    $('.controlInput #form').css('width', '100%');
    $('.controlInput #image').css('width', '100%');
}

//sự kiện chọn size
function form_ChangeBackgroundSize() {
    var size = $('#bacground-size').val();
    var autosize = $('#background-size-auto').val();

    $('#bacground-size').on('input propertychange paste', function () {
        size = $(this).val();
        form_BackgroundSize(size);
        if (autosize != 'noselect') {
            $('#background-size-auto').val('noselect');
        }
    });

    $('#background-size-auto').on('change', function () {
        autosize = $(this).val();
        if (autosize != 'noselect') {
            $('.controlInput').css('background-size', autosize);
            if (size != '') {
                $('#bacground-size').val('');
            }
        }
        else {
            form_BackgroundSize(size);
        }

    });
}

//set background size
function form_BackgroundSize(size) {

    if (parseInt(size) > 100) {
        $('#bacground-size').val(100)
    }
    if ($('#bacground-size').val()) {
        $('.controlInput').css('background-size', size + '%');
    }
    else {
        $('.controlInput').css('background-size', 'auto');
    }
}

function form_ChangeBackgroundPosition() {
    var positionx = $('#bacground-position-x').val();
    var positiony = $('#bacground-position-y').val();

    $('#bacground-position-x').on('input propertychange paste', function () {
        positionx = $(this).val();
        console.log(positionx);
        form_BackgroundPostion(positionx, positiony)

    });

    $('#bacground-position-y').on('input propertychange paste', function () {
        positiony = $(this).val();
        console.log(positiony);
        form_BackgroundPostion(positionx, positiony)

    });
}

//set position
function form_BackgroundPostion(positionx, positiony) {
    if (positionx) {
        if (parseInt(positionx) > 100) {
            $('#bacground-position-x').val(100)
        }

        $('.controlInput').css('background-position-x', positionx + '%');
    }
    else {
        $('.controlInput').css('background-position-x', '');
    }

    if (positiony) {
        if (parseInt(positiony) > 100) {
            $('#bacground-position-y').val(100)
        }

        $('.controlInput').css('background-position-y', positiony + '%');
    }
    else {
        $('.controlInput').css('background-position-y', '');
    }
}

function form_ChangeRepeat() {
    $('#repeat-background-form').on('change', function () {
        if ($(this).prop('checked')) {
            $('.controlInput').css('background-repeat', 'repeat');
        }
        else {
            $('.controlInput').css('background-repeat', 'no-repeat');
        }
    });
}

function form_ChangeLabelColor() {
    $('#color-label-form').on('change', function () {
        var color = $(this).val();
        $('.controlInput #captionname').css('color', color);

    })
}

function form_ChangeControlColor() {
    $('#color-text-form').on('change', function () {
        var color = $(this).val();
        form_SetColorControl(color);
    });
}

function form_SetColorControl(color) {
    $('.controlInput input').css('color', color);

    $('.controlInput textarea').css('color', color);

    $('.controlInput select').css('color', color);
    $('.controlInput .md-radio-list').css('color', color);

    $('.controlInput .md-radio-list .check').css('background', color);

    $('.controlInput .md-checkbox-list').css('color', color);
    $('.controlInput .md-checkbox-list .check').css('border', '1px solid ' + color);
    $('.controlInput .md-checkbox-list .check').css('border-top', 'none');
    $('.controlInput .md-checkbox-list .check').css('border-left', 'none');
    
}

function form_ChangeControlBorder() {
    $('#color-border-form').on('change', function () {
        var color = $(this).val();
        form_SetControlBorder(color);

    });
}

function form_SetControlBorder(color) {
    $('.controlInput input').css('border-color', color);
    $('.controlInput textarea').css('border-color', color);
    $('.controlInput select').css('border-color', color);

    $('.controlInput .md-radio-list .box').css('border', '1px solid ' + color);

    $('.controlInput .md-checkbox-list .box').css('border', '1px solid ' + color);
    
}

function form_ChangeRadiusControl() {
    $('input[name=change-radius-control]').on('input propertychange paste', function () {
        var radius = $(this).val();

        form_UpdateRadius(radius);
    });

}

function form_UpdateRadius(radius) {
    if ($('#change-all-radius-control').prop('checked')) {
        $('input[name=change-radius-control]').val(radius);
        $('.controlInput input').css('border-radius', radius + 'px');
        $('.controlInput textarea').css('border-radius', radius + 'px');
        $('.controlInput select').css('border-radius', radius + 'px');
        $('.controlInput #_btnformSubmit').css('border-radius', radius + 'px');
    }
    else {
        form_SetRadiusFromInput();
    }
}

function form_SetRadiusFromInput() {
    var radiuslt = $('#color-radius-lefttop-form').val();
    var radiusrt = $('#color-radius-righttop-form').val();
    var radiuslb = $('#color-radius-leftbottom-form').val();
    var radiusrb = $('#color-radius-rightbottom-form').val();


    $('.controlInput input').css({ 'border-top-left-radius': radiuslt + 'px', 'border-top-right-radius': radiusrt + 'px', 'border-bottom-right-radius': radiusrb + 'px', 'border-bottom-left-radius': radiuslb + 'px' });
    $('.controlInput textarea').css({ 'border-top-left-radius': radiuslt + 'px', 'border-top-right-radius': radiusrt + 'px', 'border-bottom-right-radius': radiusrb + 'px', 'border-bottom-left-radius': radiuslb + 'px' });
    $('.controlInput select').css({ 'border-top-left-radius': radiuslt + 'px', 'border-top-right-radius': radiusrt + 'px', 'border-bottom-right-radius': radiusrb + 'px', 'border-bottom-left-radius': radiuslb + 'px' });
    $('.controlInput #_btnformSubmit').css({ 'border-top-left-radius': radiuslt + 'px', 'border-top-right-radius': radiusrt + 'px', 'border-bottom-right-radius': radiusrb + 'px', 'border-bottom-left-radius': radiuslb + 'px' });

}

function form_ChangeLabelPosition() {
    $('#label-position').on('change', function () {
        var position = $(this).val();
        form_SetLabelPositon(position);
    });
}

function form_SetLabelPositon(position) {
    switch (position) {
        case 'top':
            $('.controlInput #captionname').css('display', 'block');
            $('.controlInput #captionname').css('text-align', 'left');
            break;
        case 'inside':
            $('.controlInput #captionname').css('display', 'none');
            break;
        case 'center':
            $('.controlInput #captionname').css('display', 'block');
            $('.controlInput #captionname').css('text-align', 'center');
            break;

    }
}


//ẩn border, nút xóa của tất cả items
function formHideBorderItems() {
    $('.items li div').css({ 'border': '' });
    $("._element_action").css({ 'display': 'none' });
    $('#divbtnformSubmit').css({ 'border': '' });
    $('#divheadlineForm').css({ 'border': '' });
    $('.controlInput').css('border', 'none');
   
}

//khai báo sự kiện kéo thay đổi vị trí của panel
    function form_ChangePositionField() {
        $(".items").sortable({
            placeholder: 'item-placeholder',
            axis: "y",
            revert: 150,
            start: function (e, ui) {

                placeholderHeight = ui.item.outerHeight();
                ui.placeholder.height(placeholderHeight + 15);
                $('<div class="item-placeholder-animator" data-height="' + placeholderHeight + '"></div>').insertAfter(ui.placeholder);

            },
            change: function (event, ui) {
                window.savedform = false;

                ui.placeholder.stop().height(0).animate({
                    height: ui.item.outerHeight() + 15
                }, 300);

                placeholderAnimatorHeight = parseInt($(".item-placeholder-animator").attr("data-height"));

                $(".item-placeholder-animator").stop().height(placeholderAnimatorHeight + 15).animate({
                    height: 0
                }, 300, function () {
                    $(this).remove();
                    placeholderHeight = ui.item.outerHeight();
                    $('<div class="item-placeholder-animator" data-height="' + placeholderHeight + '"></div>').insertAfter(ui.placeholder);
                });

            },
            stop: function (e, ui) {

                $(".item-placeholder-animator").remove();

            },
        });
    };


//    function form_openModalAddCustom(apiDomain, access_token, Title, PlaceHolder, idGroupValue)
//    {
//        $('#user-custom-field-panel').on('click', '#create-custom-field', function () {
//            $('#GroupValue').children().remove();
//            $('#create-custom-field-name').val('');
//            $('#CustomFieldType').selectpicker('deselectAll');

//            $('#CustomFieldFormat').selectpicker('deselectAll');
           
//            form_ChangeAnotherValue(apiDomain, access_token, Title, PlaceHolder, idGroupValue)
//            $('#modal-create-customfield').modal('toggle');
//        });
//    }

////append field để nhập thêm gia trị trong create custom field
////ẩn hiện nút delete , add value 
//function form_addAnotherValue(title, placeholder, id) {
//    var type = parseInt($('select[id=CustomFieldType]').val());
//    var format = parseInt($('select[id=CustomFieldFormat]').val());
//    console.log(type);
//    console.log(format);
//    switch(type)
//    {
//        case 1:
//            form_addAnotherValue_Text(title, placeholder, id);
//            showDel(id);
//            break;
//        case 2:
//            form_addAnotherValue_Number(title, placeholder, id);
//            showDel(id);
//            break;
//        case 3:
//            form_addAnotherValue_Text(title, placeholder, id);
          
//            showDel(id);
//            break;
//        case 4:
//            form_addAnotherValue_Text(title, placeholder, id);
            
//            showDel(id);
//            break;
//        case 6:
//            form_addAnotherValue_Number(title, placeholder, id);
            
//            showDel(id);
//            break;
//        case 7:
//            form_addAnotherValue_Date(title, placeholder, id);
            
//            showDel(id);
//            break;
//        case 8:
//            form_addAnotherValue_DateTime(title, placeholder, id);
            
//            showDel(id);
//            break;
//        case 10:
//            form_addAnotherValue_Gender(title, placeholder, id);
//            showDel(id);
//            break;
//    }

//};

////sự kiện thay đổi type customfield
//function form_ActionChangeTypeCustomField(apiDomain, access_token, Title, PlaceHolder, idGroupValue) {
//    $('#div-custom-field-type').on('change', '#CustomFieldType', function (e) {
//        //xóa tất cả các value field hiện tại
//        deleteAllValue(idGroupValue);
//        //load format field
//        var FieldTypeId = parseInt($('select[id=CustomFieldType]').val());
//        LoadFormatCustomField(apiDomain, access_token, FieldTypeId, Title, PlaceHolder, idGroupValue)
//    });
//}
////sự kiện thay đổi fromat customfield
//function form_ActionChangeFormatCustomField(apiDomain, access_token, Title, PlaceHolder, idGroupValue) {
//    $('#div-custom-field-format').on('change', '#CustomFieldFormat', function (e) {
//        //xóa tất cả các value field hiện tại
//        deleteAllValue(idGroupValue);
//        form_ChangeAnotherValue(apiDomain, access_token, Title, PlaceHolder, idGroupValue);
//    });
//}

////sự kiện thay đổi fortmat type khi tao custom field --> thay đổi các field nhập giá trị.
//function form_ChangeAnotherValue(apiDomain, access_token, title, placeholder, id) {
//    var type = parseInt($('#div-custom-field-type #CustomFieldType').val());
//    var format = parseInt($('#div-custom-field-format #CustomFieldFormat').val());
//    console.log(type);
//    console.log(format);
//    switch (type) {
//        case 1:
            
//            switch (format) {
//                case 1:
//                    form_addAnotherValue_Text(title, placeholder, id);
//                    hideDel(id);
//                    hideAdd();
//                    break;
//                case 2:
//                    form_addAnotherValue_Text(title, placeholder, id);
//                    hideDel(id);
//                    hideAdd();
//                    break;
//                default:
//                    form_addAnotherValue_Text(title, placeholder, id);
//                    form_addAnotherValue_Text(title, placeholder, id);
//                    showAdd();
//                    hideDel(id);
//                    break;
//            }
//            break;
//        case 2:
//            switch (format) {
//                case 1:
//                    form_addAnotherValue_Number(title, placeholder, id);
//                    hideDel(id);
//                    hideAdd();
//                    break;
//                case 2:
//                    form_addAnotherValue_Number(title, placeholder, id);
//                    hideDel(id);
//                    hideAdd();
//                    break;
//                default:
//                    form_addAnotherValue_Number(title, placeholder, id);
//                    form_addAnotherValue_Number(title, placeholder, id);
//                    showAdd();
//                    hideDel(id);
//                    break;
//            }
//            break;
//        case 3:
//            form_addAnotherValue_Text(title, placeholder, id);
//            hideDel(id);
//            hideAdd();
//            break;
//        case 4:
//            form_addAnotherValue_Text(title, placeholder, id);
//            hideDel(id);
//            hideAdd()
//            break;
//        case 6:
//            form_addAnotherValue_Number(title, placeholder, id);
//            hideDel(id);
//            hideAdd();
//            break;
//        case 7:
            
//            switch (format) {
//                case 1:
//                    form_addAnotherValue_Date(title, placeholder, id);
//                    hideDel(id);
//                    hideAdd();
//                    break;
//                default:
//                    form_addAnotherValue_Date(title, placeholder, id);
//                    form_addAnotherValue_Date(title, placeholder, id);
//                    showAdd();
//                    hideDel(id);
//                    break;
//            }
//            break;
//        case 8:
            
//            switch (format) {
//                case 1:
//                    form_addAnotherValue_DateTime(title, placeholder, id);
//                    hideDel(id);
//                    hideAdd();
//                    break;
//                default:
//                    form_addAnotherValue_DateTime(title, placeholder, id);
//                    form_addAnotherValue_DateTime(title, placeholder, id);
//                    showAdd();
//                    hideDel(id);
//                    break;
//            }
//            break;
//        case 10:
//            form_addAnotherValue_Gender(title, placeholder, id);
//            form_addAnotherValue_Gender(title, placeholder, id);
//            showAdd();
//            hideDel(id);
//            break;
//    }

//};

////load danh sách customfield type
//function LoadTypeCustomField(apiDomain, access_token, Title, PlaceHolder, idGroupValue)
//{
//        $.ajax({
//            url: apiDomain + '/api/CustomField/GetTypeList',
//            type: 'GET',
//            contentType: 'application/json; charset=utf-8',
//            beforeSend: function (request) {
//                request.setRequestHeader("Authorization", "Bearer " + access_token);
//            },
//            success: function (r) {
//                switch (parseInt(r.ErrorCode)) {
//                    case 0:

//                        var options = new Array();
//                        $.each(JSON.parse(JSON.stringify(r.Data)), function (i, obj) {
//                            options.push('<option value="' + obj.FieldTypeId + '" data-icon="'+obj.IconUrl+'">' + obj.FieldTypeName + '</option>');

//                        });
//                        $('<select id="CustomFieldType" class="bs-select form-control" data-show-subtext="true">').append(options.join('')).appendTo('#div-custom-field-type');
//                        //reder code style select
//                        $('#div-custom-field-type #CustomFieldType').selectpicker();

//                        //load format after load type 
//                        var FieldTypeId = parseInt($('select[id=CustomFieldType]').val());
//                        LoadFormatCustomField(apiDomain, access_token, FieldTypeId, Title, PlaceHolder, idGroupValue);
//                        break;
//                }
//            },
//            error: function (x, s, e) {

//            },
//        });
//}

////load danh sách format theo type.
//function LoadFormatCustomField(apiDomain, access_token, FieldTypeId, Title, PlaceHolder, idGroupValue)
//{
//    //check select model exist before load->if exist remove
//    if ($('#div-custom-field-format #CustomFieldFormat').length) {
//        $('#div-custom-field-format').children().remove();
//    }
//    $.ajax({
//        url: apiDomain +'api/CustomField/GetFormatList?FieldTypeId=' + FieldTypeId,
//        type: 'GET',
//        contentType: 'application/json; charset=utf-8',
//        beforeSend: function (request) {
//            request.setRequestHeader("Authorization", "Bearer " + access_token);
//        },
//        success:function(r)
//        {
//            var options = new Array();
//            $.each(JSON.parse(JSON.stringify(r.Data)), function (i, obj) {
//                options.push('<option value="' + obj.FieldFormatId + '" data-icon="glyphicon-tag">' + obj.FieldFormatName + '</option>');

//            });
//            $('<select id="CustomFieldFormat" class="bs-select form-control" data-show-subtext="true">').append(options.join('')).appendTo('#div-custom-field-format');
//            //reder code style select
//            $('#div-custom-field-format #CustomFieldFormat').selectpicker();

//            //load field input value 
//            form_ChangeAnotherValue(apiDomain, access_token, Title, PlaceHolder, idGroupValue);
//        },
//        error:function(x,s,e)
//        {

//        },
//    })
//}

////nút thêm giá trị customfield
//function form_ActionAddValueField(Title, PlaceHolder, idGroupValue) {
//    $('#addAnotherValue').on('click', function () {
//        form_addAnotherValue(Title, PlaceHolder, idGroupValue);
//    });
//}



//// add value in modal create custom field
//function form_addAnotherValue_Text(title, placeholder, id) {
//    var newItem = $(document.createElement('div'))
//                       .attr("class", "form-group").attr("id", id);
//    newItem.after().html('<label class="col-md-3 control-label">' + title + '</label>' +
//                             '<div class="col-md-6">' +
//                                 '<input type="text" id="input-customfield-value" name="FieldValue" class="form-control" placeholder="' + placeholder + '">' +
//                             '</div>' +
//                             deleteValueField);
//    newItem.appendTo("#GroupValue");
//};
//function form_addAnotherValue_Number(title, placeholder, id) {
//    var newItem = $(document.createElement('div'))
//                        .attr("class", "form-group").attr("id", id);
//    newItem.after().html('<label class="col-md-3 control-label">' + title + '</label>' +
//                             '<div class="col-md-6">' +
//                                 '<input type="text" id="input-customfield-value" name="FieldValue" class="form-control" placeholder="' + placeholder + '" onkeypress="validate(event)">' +
//                             '</div>' +
//                             deleteValueField);
//    newItem.appendTo("#GroupValue");

//};
//function form_addAnotherValue_Date(title, placeholder, id) {
//    var newItem = $(document.createElement('div'))
//                       .attr("class", "form-group").attr("id", id);
//    newItem.after().html('<label class="col-md-3 control-label">' + title + '</label>' +
//                             '<div class="col-md-6">' +
//                                    '<input name="FieldValue" id="input-customfield-value" class="form-control date-picker" />' +
//                             '</div>' +
//                             deleteValueField);
//    newItem.appendTo("#GroupValue");
//    form_datesingle();
//};

//function form_addAnotherValue_DateTime(title, placeholder, id) {
//    var newItem = $(document.createElement('div'))
//                       .attr("class", "form-group").attr("id", id);
//    newItem.after().html('<label class="col-md-3 control-label">' + title + '</label>' +
//                             '<div class="col-md-6">' +
//                                    '<div class="form_datetime"> ' +
//                                         '<input name="FieldValue" id="input-customfield-value" class="form-control" />' +
//                                         //'<span class="input-group-btn">' +
//                                         //     '<button class="btn default date-set" type="button">' +
//                                         //         '<i class="fa fa-calendar"></i>' +
//                                         //      '</button>' +
//                                         // '</span>' +
//                                       '</div>' +
//                             '</div>' +
//                             deleteValueField);
//    newItem.appendTo("#GroupValue");
//    from_datetimesingle();
//};
//function form_addAnotherValue_Gender(title, placeholder, id) {
//    var newItem = $(document.createElement('div'))
//                       .attr("class", "form-group").attr("id", id);
//    newItem.after().html('<label class="col-md-3 control-label">' + title + '</label>' +
//                             '<div class="col-md-6">' +
//                                 '<input type="text" name="FieldValue" id="input-customfield-value"  class="form-control" placeholder="' + placeholder + '">' +
//                             '</div>' +
//                             deleteValueField);
//    newItem.appendTo("#GroupValue");
//};

////thêm customfield của người dùng
//function form_CreateUserCustomField(apiDomain, access_token, formId, message, permission)
//{
//    $('#save-user-custom-field').on('click', function () {
//        if (form_CheckBeforeCreateCustomField(message)) {
//            var ListValue = new Array();
//            if ($('#GroupValue').length) {

//                var count = 0;
//                var items = $('#GroupValue #GroupFieldValue #input-customfield-value')
//                $.each(items, function (index, item) {
//                    var obj = { FieldSelected: false, FieldValue: '', SortId: '' };
//                    // console.log($(item))
//                   // alert($(item).val());
//                    obj.FieldValue = $(item).val(); // "this" is the current element in the loop
//                    if ($('#GroupValue').children().length > 1) {
//                        obj.FieldSelected = 'false';
//                        obj.SortId = count;
//                    }
//                    ListValue.push(obj);
//                    console.log(ListValue)
//                    count++;
//                });
//            }
//            var data = {
//                FieldTypeId: parseInt($('select[id=CustomFieldType]').val()),
//                FieldFormatId: parseInt($('select[id=CustomFieldFormat]').val()),
//                FieldName: custom_bodauTiengViet($('#create-custom-field-name').val().replace(/\s/g, '')),
//                Values: ListValue

//            };
//            $.ajax({
//                url: apiDomain + 'api/CustomField/Create',
//                type: 'POST',
//                data: data,
//                contentType: 'application/x-www-form-urlencoded',
//                beforeSend: function (request) {
//                    request.setRequestHeader("Authorization", "Bearer " + access_token);
//                },
//                success: function (r) {
//                    var errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
//                    switch (errorcode) {
//                        case 0:
//                            custom_shownotification("success", message.SaveCustomFieldSuccess);
//                            $('.create-customfield-modal').modal('toggle');

//                            //reload customfield
//                            $("#predefine1").children().remove();
//                            $("#predefine2").children().remove();
//                            $("#usercustom1").children().remove();
//                            $("#usercustom2").children().remove();
//                            $("#delete-usercustom1").children().remove();
//                            $("#delete-usercustom2").children().remove();
//                            customField_System(apiDomain, access_token, formId, message, permission);
//                            break;
//                        default: custom_shownotification("error", message.SaveCustomFieldError);
//                    }
//                },
//                error: function (x, s, e) {
//                    custom_shownotification("error", message.SaveCustomFieldError);
//                }

//            });
//        }
//        });
//}

function from_datetimesingle() {
    $('.form_datetime input').datetimepicker(
        {
            autoclose: true,
            isRTL: App.isRTL(),
            format: "dd MM yyyy - hh:ii",
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
        });
}

function form_datesingle() {
    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        orientation: "left",
        autoclose: true
    });
}

////kiểm tra tên rỗng 
//function form_CheckBeforeCreateCustomField(message)
//{
//    if ($('#create-custom-field-name').val() == "") {
//        custom_shownotification("error", message.CheckName);
//        return false;
//    }
//    if ($('#create-custom-field-name').val()) {
//        if (!IsMessage($('#create-custom-field-name').val())) {
//            custom_shownotification('error', message.ODau);
//            return false;
//        }
//        else return true;
//    }
//    else return true;
//}

////ẩn nút xóa field giá trị
//function hideDel(id) {
//    var deleteid = $('#' + id).children('a').attr('id');
//    $('#' + id + '>#' + deleteid).each(function () {
//        $(this).css({ 'display': 'none' });
//    })
//};

////hiện nút xóa giá trị
//function showDel(id) {
//    var deleteid = $('#' + id).children('a').attr('id');
//    $('#' + id + '>#' + deleteid).each(function () {
//        $(this).css({ 'display': '' });
//    })
//};

////ẩn nút thêm field giá trị
//function hideAdd() {
//    $('#GroupAddValue').css({ 'display': 'none' });
//};

////hiện nút thêm filed giá trị
//function showAdd() {
//    $('#GroupAddValue').css({ 'display': '' });
//};

////xóa tất cả field giá trị
//function deleteAllValue(id) {

//    var parentid = $("#" + id).parent().attr('id');
//    $('#' + parentid + '>' + '#' + id).each(function () {
//        $(this).remove();
//    })
//};

////xóa field nhập value
////sự kiện xóa 1 append
//$('#formAddCustomField').on('click', '#deleteAnotherValue', function () {
//    $(this).parent().remove();
//    var id = $(this).parent().attr('id');
//    var value = $('div#' + id).length;
//    if (value == 2) {
//        hideDel(id);
//    }

//});


var idb = 'btnformSubmit';
var idbp = 'btnformSubmitPosition';
var idbz = 'btnformSubmitSize'
var parentidb = '_btnformSubmit'
var idl = 'headlineForm';
var idlp = 'headlineFormPosition';
var idlz = 'headlineFormSize'
var parentidl = '_headlineFormSubmit'

//hàm tạo nút submit
function addSubmit(nameButtonSubmit) {
    
        var newItem = $(document.createElement('div')).attr('id', 'divbtnformSubmit');
        newItem.after().html(
                       '<a id="' + parentidb + '" class="btn blue" > ' + nameButtonSubmit + ' </a>'
          );
        newItem.appendTo(".controlInput .submit");
      
     
    }

//hàm tạo headline
function addHeadline(nameHeadline)
{
    var newItem = $(document.createElement('div')).attr('id','divheadlineForm');
    newItem.after().html(
       '<span  id="' + parentidl + '" style="text-align:left; border-bottom:0 !important">' + nameHeadline + '</span>'

      );
    newItem.appendTo(".controlInput .headline");


}

//nút lưu form
function form_UpdateCustomFieldToForm(apiDomain, access_token, FormId, GroupId, messageWarning, eId, gName) {
    $('#btnSaveForm').on('click', function () {
        var itemsexist = []; //yêu cầu customfield mobile 10 hoặc email 1 phải có

        if (form_CheckExistCustomField()) { //kiểm tra tồn tại custom field trong form trước khi lưu
            window.savedform = true;
            $('#divLoading').show();
            // //-------------------form content ok
            var position = $('#image-position input:radio:checked').val();
            var FormImagePosition = 0;
            switch (position) {
                case 'top':
                    FormImagePosition = 0;
                    break;
                case 'bottom':
                    FormImagePosition = 1;
                    break;
                case 'left':
                    FormImagePosition = 2;
                    break;
                case 'right':
                    FormImagePosition = 3;
                    break;
            };
            //background size
            var FormBackgroundImageSize = '';
            if ($('#background-size-auto').val() != 'noselect') {
                FormBackgroundImageSize = $('#background-size-auto').val()
            }
            if ($('#bacground-size').val() != '') {
                FormBackgroundImageSize = $('#bacground-size').val();
            }

            //background link 
            var FormBackgroundImageUrl = '';
            var background = $('.controlInput').css("backgroundImage");
            if (background && background != 'none') {

                FormBackgroundImageUrl = background.replace('url("', '').replace('")', '');
            }

            //background position
            var FormBackgroundImagePosition = "";
            var PositionX = '';
            var PositionY = '';
            if ($('#bacground-position-x').val()) {
                PositionX = $('#bacground-position-x').val();
            }
            if ($('#bacground-position-x').val()) {
                PositionY = $('#bacground-position-y').val();
            }
            FormBackgroundImagePosition = PositionX + ',' + PositionY;

            //size label
            var slide_label_size = document.getElementById('slider-size-label');
            var FormLabelSize = slide_label_size.noUiSlider.get();

            //size textbox
            var slide_textbox_size = document.getElementById('slider-size-textbox');
            var FormControlSize = slide_textbox_size.noUiSlider.get();

            //size header
            var slide_header_size = document.getElementById('slider-size-header');
            var FormHeaderSize = slide_header_size.noUiSlider.get();

            //button height
            var slide_size_height_button = document.getElementById('slider-size-height-button');
            var FormButtonSize = slide_size_height_button.noUiSlider.get();

            //button width
            var slide_size_button = document.getElementById('slider-size-button');
            var FormButtonWidth = slide_size_button.noUiSlider.get();

            var FormContent = {
                FormId: FormId,
                FormHeader: $('#_headlineFormSubmit').text(),
                FormHeaderSize: FormHeaderSize,
                FormHeaderAlign: $('.controlInput .headline').css('text-align'),
                FormHeaderColor: $('#color-headline-form').val(),
                FormSubmitButtonLabel: $('#_btnformSubmit').text().trim(),
                FormButtonSize: FormButtonSize,
                FormButtonWidth: FormButtonWidth,
                FormButtonAlign: $('.controlInput .submit').css('text-align'),
                FormButtonColor: $('#color-button-text-form').val(),
                FormButtonBgColor: $('#color-button-form').val(),
                FormButtonColorHover: $('#color-button-text-hover-form').val(),
                FormButtonBgColorHover: $('#color-button-hover-form').val(),
                FormImageVisible: $('#show-image-form').prop('checked'),
                FormImagePosition: FormImagePosition, //top
                FormImageLink: $('#custom-image-form').attr('src'),
                FormBackgroundImageUrl: FormBackgroundImageUrl,
                FormBackgroundImageSize: FormBackgroundImageSize,
                FormBackgroundColor: $('#color-bacground-form').val(),
                FormBackgroundImagePosition: FormBackgroundImagePosition,
                FormBackgroundImageRepeat: $('#repeat-background-form').prop('checked'),
                FormLabelColor: $('#color-label-form').val(),
                FormLabelAlign: $('#label-position').val(),
                FormControlTextColor: $('#color-text-form').val(),
                FormControlBorderColor: $('#color-border-form').val(),
                FormControlBorderRadius: $('#color-radius-lefttop-form').val(), //+ ',' + $('#color-radius-righttop-form').val() + ',' + $('#color-radius-rightbottom-form').val() + ',' + $('#color-radius-leftbottom-form').val(),
                FormLabelSize: FormLabelSize,
                FormControlSize: FormControlSize,
            }
            // //--------------------------------------end------------------

            var FormCustomFields = new Array();

            var items = $('.items li');

            $.each(items, function (index, item) {
                var Values = new Array();

                var component = $(this).children('#div' + $(this).attr('id')).data('component');
                //tìm giá trị của custom field theo từng loại
                switch (component) {
                    case 'text-box':
                        var object_textbox_values = {
                            "FieldValue": "",
                            "FieldSelected": "",
                            "SortId": ""
                        };
                        var value = $('#' + $(this).attr('id') + ' #div' + $(this).attr('id') + ' input[type="text"]').attr('value');
                        if (value) {
                            object_textbox_values.FieldValue = value;
                        }
                        else {
                            object_textbox_values.FieldValue = "";
                        }
                        object_textbox_values.FieldSelected = false;
                        object_textbox_values.SortId = 0;
                        Values.push(object_textbox_values);
                        break;

                    case 'text-multi':
                        var object_multi_values = {
                            "FieldValue": "",
                            "FieldSelected": "",
                            "SortId": ""
                        };
                        var value = $('#' + $(this).attr('id') + ' #div' + $(this).attr('id') + ' textarea').attr('value');
                        if (value) {
                            object_multi_values.FieldValue = value;
                        }
                        else {
                            object_multi_values.FieldValue = "";
                        }
                        object_multi_values.FieldSelected = false;
                        object_multi_values.SortId = 0;
                        Values.push(object_multi_values);
                        break;
                    case 'radio-button':
                        var item_radio_values = $(this).children().children().children('.md-radio');
                        $.each(item_radio_values, function (index, itemvalue) {
                            var object_radio_values = {
                                "FieldValue": "",
                                "FieldSelected": "",
                                "SortId": ""
                            }
                            object_radio_values.FieldValue = $('#' + $(this).attr('id') + ' input[type="radio"]').attr('value');
                            object_radio_values.SortId = index;
                            object_radio_values.FieldSelected = false;
                            Values.push(object_radio_values);

                        });
                        break;
                    case 'check-box':
                        var item_checkbox_values = $(this).children().children().children('.md-checkbox');
                        $.each(item_checkbox_values, function (index, itemvalue) {
                            var object_checkbox_values = {
                                "FieldValue": "",
                                "FieldSelected": "",
                                "SortId": ""
                            }
                            object_checkbox_values.FieldValue = $('#' + $(this).attr('id') + ' input[type="checkbox"]').attr('value');
                            object_checkbox_values.SortId = index;
                            object_checkbox_values.FieldSelected = false;
                            Values.push(object_checkbox_values);

                        });
                        break;
                    case 'dropdown-list':
                        var item_dropdown_values = $(this).children().children().children('option');
                        if (item_dropdown_values.length != 0) {
                            $.each(item_dropdown_values, function (index, itemvalue) {
                                var object_checkbox_values = {
                                    "FieldValue": "",
                                    "FieldSelected": "",
                                    "SortId": ""
                                }
                                object_checkbox_values.FieldValue = $(this).attr('value');
                                object_checkbox_values.SortId = index;
                                object_checkbox_values.FieldSelected = false;
                                Values.push(object_checkbox_values);

                            });
                        }
                        else {
                            var object_checkbox_values_null = {
                                "FieldValue": "",
                                "FieldSelected": false,
                                "SortId": 0
                            }
                            Values.push(object_checkbox_values_null);
                        }
                        break;
                }

                var IsRequired = false;
                if ($(this).children().children('#captionname').hasClass('checkrequired')) {
                    IsRequired = true;
                }
               

                switch (component) {
                    case 'text-box':
                        var StrPlaceHolder = $(this).children().children().children('input').attr('placeholder');
                        break;
                    case 'text-multi':
                        var StrPlaceHolder = $(this).children().children().children('textarea').attr('placeholder');
                        break;
                    default:
                        var StrPlaceHolder = $(this).children().children().children('input').attr('placeholder');
                        break;
                }


                if (parseInt($(this).attr('id')) == 10078 || parseInt($(this).attr('id')) == 10079) {
                    var childrenid = $(this).children().attr('id')
                    var Postion = form_PostionOfContent($('#' + childrenid + ' #Content').parent().css('text-align'));
                    var Width=$('#' + childrenid + ' #Content').css('font-size').replace('px','');
                    var objectcustomfields = {
                        "CustomFieldId": $(this).attr('id'),
                        "FieldTypeId": $('#' + childrenid).data('typeid'),
                        "FieldFormatId": $('#' + childrenid).data('formatid'),
                        "SortId": index,
                        "FieldLabel": $('#' + childrenid +' #Content').text(),
                        "FieldLabelPosition": Postion,
                        "PlaceHolder": $('#' + childrenid + ' #Content').css('color'), //color
                        "IsRequired": false,
                        "ErrorMessage": "",
                        "MinChar": 1,  //bắt buộc có 
                        "MaxChar": $('#' + childrenid).data('maxlength'), //bắt buộc có 
                        "Width": Width,  //bắt buộc có
                        "Values": [{ FieldValue: '', FieldSelected: false, SortId: 0}],
                    };
                }
                else
                {
                    var objectcustomfields = {
                        "CustomFieldId": $(this).attr('id'),
                        "FieldTypeId": $(this).children('#div' + $(this).attr('id')).data('typeid'),
                        "FieldFormatId": $(this).children('#div' + $(this).attr('id')).data('formatid'),
                        "SortId": index,
                        "FieldLabel": $(this).children().children('#captionname').text(),
                        "FieldLabelPosition": 1,
                        "PlaceHolder": StrPlaceHolder,
                        "IsRequired": IsRequired,
                        "ErrorMessage": "",
                        "MinChar": 1,  //bắt buộc có 
                        "MaxChar": $(this).children('#div' + $(this).attr('id')).data('maxlength'), //bắt buộc có 
                        "Width": 50,   //bắt buộc có
                        "Values": Values,
                    };
                }
                FormCustomFields.push(objectcustomfields);

                //tính xem có mobi hay email thì add vào 
                if (parseInt(objectcustomfields.CustomFieldId) == 1 || parseInt(objectcustomfields.CustomFieldId) == 10) {
                    itemsexist.push(objectcustomfields);
                }
            });

            console.log(itemsexist);
            if (itemsexist.length < 1) {
                custom_shownotification('error', 'Form bắt buộc có field Email hoặc Mobile');
                $('#divLoading').hide();
            }
            else {
                var itemrequire = 0;
                $.each(itemsexist, function (i, o) {
                    if (o.IsRequired) {
                        itemrequire += 1;
                    }
                });
                if (itemrequire == 0) {
                    if (itemsexist.length == 1) {
                        if (itemsexist[0].CustomFieldId == 1) {
                            custom_shownotification('error', itemsexist[0].FieldLabel+' bắt buộc có thuộc tính Field is required ✓');
                            $('#divLoading').hide();
                        }
                        if (itemsexist[0].CustomFieldId == 10) {
                            custom_shownotification('error', itemsexist[0].FieldLabel + ' bắt buộc có thuộc tính Field is required ✓');
                            $('#divLoading').hide();
                        }

                    } else {
                        custom_shownotification('error', itemsexist[0].FieldLabel + ' hoặc ' + itemsexist[1].FieldLabel + ' bắt buộc có thuộc tính Field is required ✓');
                        $('#divLoading').hide();

                    }

                }
                else {

                    var data = {
                        FormId: FormId, //lấy từ viewbag truyền vào
                        FormContent: FormContent,
                        FormCustomFields: FormCustomFields,
                    }


                    $.ajax({
                        url: apiDomain + 'api/Form/UpdateCustomField',
                        type: 'POST',
                        contentType: 'application/x-www-form-urlencoded',
                        beforeSend: function (request) {
                            request.setRequestHeader("Authorization", "Bearer " + access_token);
                        },
                        data: data,
                        success: function (r) {
                            var errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
                            switch (errorcode) {
                                case 0:
                                    custom_shownotification('success', messageWarning.UpdateFormSuccess);
                                    var fId = JSON.parse(JSON.stringify(r.Data)).FormId;
                                    var height = form_getHeightForm();
                                    var width = form_getWidthForm();
                                    setTimeout(function () {
                                        window.location.href = '/Contacts/PublishForm?gId=' + encodeURI(GroupId + '&eId=' + eId + '&gName=' + gName + '&fId=' +FormId, "UTF-8");
                                    }, 2000);
                                    break;
                                default: custom_shownotification('error', messageWarning.UpdateFormError);
                                    $('#divLoading').hide();
                                    break;
                            }

                        },
                        error: function (x, s, e) {
                            custom_shownotification('error', messageWarning.UpdateFormError);
                            $('#divLoading').hide();
                        }

                    });
                }
            }
        }
    });
}

    function form_getWidthForm() {
    var width = $('.controlInput').outerWidth();
    return width;
}
    function form_getHeightForm() {
    var height = $('.controlInput').outerHeight();
    return height;
}

    //kiểm tra có customfile chưa trước khi lưu
    function form_CheckExistCustomField() {
    if ($('.items li').length == 0) {
        custom_shownotification('error', messageWarning.CheckExistCustomField);
        return false;
    }
    if ($('#_btnformSubmit').text() =="") {
        custom_shownotification('error', 'Nhãn nút không được trống');
        return false;
    }
    else {
        return true;
    }
}

    //// chỉnh width của contentForm 
    //function form_resizeContentForm() {

    //    $('#scroll-field-form').css({
    //        'height': parseInt($('.page-content').outerHeight() - $('.page-bar').outerHeight() - $('#fieldForm .nav-tabs').outerHeight() - 30),
    //    }).addClass("scroller");
    //    $('#contentForm').css({
    //        'width': parseInt($('.row').width() - $('#fieldForm').width() - 5),

    //    });
    //    $('#scroll-form-content').css({
    //        'height': parseInt($('.page-content').outerHeight() - $('.page-bar').outerHeight() - $('#contentForm .portlet-title').outerHeight() - 30),
    //    }).addClass("scroller");


    //};


    //$(window).resize(function () {
    //    form_resizeContentForm();
    //});

var form_file;
var form_blobName;
var typeimage;
    function form_ActionUploadImage(apiDomain, access_token, userId, imgUrl, message) {
        //button trong editor click trigger input file bên ngoài
    $('#select-image-form').on('change', function (e) {
        //sự kiện chọn hình, upload hình
        var files = e.target.files;
        var name = files[0].name;
        form_file = files[0];
        var extend = name.split('.').pop();
        console.log(extend);
        form_blobName = userId + moment().format('YYYYMMDDHHmmss') + '.' +extend;
        typeimage = "Image";
        emaileditor_getSAS(apiDomain, access_token, form_file, form_blobName, imgUrl, message)
    });
}

    function form_ActionUploadBackground(apiDomain, access_token, userId, imgUrl, message) {
        //button trong editor click trigger input file bên ngoài
    $('#select-background-form').on('change', function (e) {
        //sự kiện chọn hình, upload hình
        var files = e.target.files;
        var name = files[0].name;
        form_file = files[0];
        var extend = name.split('.').pop();
        console.log(extend);
        form_blobName = userId + moment().format('YYYYMMDDHHmmss') + '.' +extend;
        typeimage = "Background";
        emaileditor_getSAS(apiDomain, access_token, form_file, form_blobName, imgUrl, message)
    });
}

    function emaileditor_getSAS(apiDomain, access_token, form_file, form_blobName, imgUrl, message) {
    $.ajax({
            url: apiDomain + 'api/EmailSend/GetSASImage?BlobName=' +form_blobName,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " +access_token);
    },
            success: function (r) {
            uri = r.Data;
            emaileditor_uploadToAzureEmailContent(apiDomain, access_token, form_file, uri, form_blobName, imgUrl, message)
    }
    });
}

    //upload lên azure và server
    function emaileditor_uploadToAzureEmailContent(apiDomain, access_token, file, uri, blobName, imgUrl, message) {

    var maxBlockSize = 2 * 1024 * 1024;
    var fileSize = file.size;
    if (fileSize < maxBlockSize) {
        maxBlockSize = fileSize;
    }
    var blockIds = new Array();
    var blockIdPrefix = "block-";
    var currentFilePointer = 0;
    var totalBytesRemaining = fileSize;
    var numberOfBlocks = 0;
    var bytesUploaded = 0;

    if (fileSize % maxBlockSize == 0) {
        numberOfBlocks = fileSize / maxBlockSize;
    } else {
        numberOfBlocks = parseInt(fileSize / maxBlockSize, 10) +1;
    }

    var reader = new FileReader();
    reader.onloadend = function (evt) {
        if (evt.target.readyState == FileReader.DONE) { // DONE == 2
            var r = uri + '&comp=block&blockid=' + blockIds[blockIds.length -1];
            var requestData = new Uint8Array(evt.target.result);
            $.ajax({
                    url: r,
                    type: "PUT",
                    data: requestData,
                    processData: false,
                    beforeSend: function (xhr) {
                    xhr.setRequestHeader('x-ms-blob-type', 'BlockBlob');
                        //    xhr.setRequestHeader('Content-Length', requestData.length);
            },
                    success: function (data, status) {
                    emaileditor_uploadFileInBlocks(message);
            },
                    error: function (xhr, desc, err) {
            }
            });
    }
    };


        function emaileditor_uploadFileInBlocks(message) {

        if (totalBytesRemaining > 0) {
            var fileContent = file.slice(currentFilePointer, currentFilePointer +maxBlockSize);
            var blockId = blockIdPrefix + pad(blockIds.length, 6);
            console.log("block id = " +blockId);
            blockIds.push(btoa(blockId));
            reader.readAsArrayBuffer(fileContent);
            currentFilePointer += maxBlockSize;
            totalBytesRemaining -= maxBlockSize;
            if (totalBytesRemaining < maxBlockSize) {
                maxBlockSize = totalBytesRemaining;
        }
        } else {
            emaileditor_commitBlockList(message);
        }
    }

    emaileditor_uploadFileInBlocks(message);

        function emaileditor_commitBlockList(message) {

        var submituri = uri + '&comp=blocklist';
        var requestBody = '<?xml version="1.0" encoding="utf-8"?><BlockList>';
        for (var i = 0; i < blockIds.length; i++) {
            requestBody += '<Latest>' + blockIds[i] + '</Latest>';
        }
        requestBody += '</BlockList>';
        $.ajax({
                url: submituri,
                type: "PUT",
                data: requestBody,
                beforeSend: function (xhr) {
                xhr.setRequestHeader('x-ms-blob-content-type', file.type);
                xhr.setRequestHeader('Content-Length', requestBody.length);
        },
                success: function (data, status) {
                var image = imgUrl +blobName;
                if (typeimage == 'Image') {

                    $('#custom-image-form').attr('src', image);
                    console.log(image);
                }
                else if (typeimage == 'Background') {
                    console.log(image);
                    $('.controlInput').css('background-image', 'url(' + image + ')');
                }
        },
                error: function (xhr, desc, err) {
                custom_shownotification('error', 'Lỗi upload hình');
        }
        });

    }
}

