﻿var countact_group_auto = []; 
function contactgroup_Action(apiDomain, access_token, tableId, message) {
    //contactgroup_OpenTools(tableId);
    contactgroup_Delete(apiDomain, access_token, tableId, message)
    contactgroup_WarningMergerGroup();
    contactgroup_MergerGroup(apiDomain, access_token, tableId, message);
    contactgroup_OpenExport();
    contactgroup_SubmitExport(apiDomain, access_token, message);

    var tablerow = $(tableId).DataTable();

    //edit click
    $(tableId).on('click', ' tbody #edit', function () {
        var rowData = tablerow.row($(this).parents('tr')).data();
        $(this).attr("data-toggle", "modal");
        $(this).attr("data-target", "#modalCreateGroup");
        $(this).attr('href', '/Contacts/UpdateContactGroup?contactGroupId=' + encodeURI(rowData.contactGroupId + '&' + 'ContactGroupName=' + rowData.contactGroupName + '&' + 'Description=' + rowData.description + '&' + 'Active=' + rowData.active, "UTF-8"));
    });

    //manage click
    $(tableId).on('click', ' tbody #manage', function () {
        var rowData = tablerow.row($(this).parents('tr')).data();
        // window.location.href = '/Contacts/ContactList?gId=' + rowData.contactGroupId;
        window.location.href = '/Contacts/ContactList?gId=' + encodeURI(rowData.contactGroupId + '&eId=' + rowData.campaignId + '&gName=' + rowData.contactGroupName, "UTF-8");
    });
    //delete click
    var rowData
    $(tableId).on('click', ' tbody #delete', function () {
        rowData = tablerow.row($(this).parents('tr')).data();
        if (rowData.campaignId!=null &&rowData.campaignId!='') {
            custom_shownotification("warning", message.WarningDeleteGroupAuto);
        } else {
            $(this).attr("data-toggle", "modal");
            $(this).attr("href", "#deleteontable");
        }
    });

    $("#confirmDelete").click(function () {

            $.ajax({
                url: apiDomain + '/api/ContactGroup/Delete?ContactGroupId=' + rowData.contactGroupId,
                type: 'GET',
                contentType: 'application/json; charset=utf-8',
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + access_token);
                },
                
                success: function (r) {
                    $('#deleteontable').modal('toggle');
                    errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
                    switch (errorcode) {
                        case 0:
                            custom_shownotification("success", message.DeleteSuccess);
                            rows_selected = [];
                            $('#btn-deleteall-group').addClass('hide');
                            
                            $(tableId).dataTable().fnDraw();

                            break;
                        default: custom_shownotification("error", message.DeleteError);
                            break;
                    }
                },
                error: function (x, s, e) {
                    $('#deleteontable').modal('toggle');
                    custom_shownotification("error", message.DeleteError);
                }
            })
    });

    $('#toolsreload').on('click', function () {
        $(tableId).dataTable().fnDraw();
    });

    var table = $(tableId).DataTable();
    //các sự kiện với checkbox ,select all
    $(tableId).on('click', ' tbody input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');

        // Get row data
        var data = table.row($row).data();
        // Get row ID
        var rowId = custom_GetFisrtProJson(data)

        // Determine whether row ID is in the list of selected row IDs 
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            $(this).parent().addClass('checked')
            rows_selected.push(rowId);
            countact_group_auto.push(data);
            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            $(this).parent().removeClass('checked')
            rows_selected.splice(index, 1);
            countact_group_auto.splice(index, 1);
        }
       
        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }

        //show delete all
        if (rows_selected.length > 0)
        {
            $('#btn-deleteall-group').removeClass('hide');
            $('#btn-export-contact-group').removeClass('hide');
            $('#number-contactgroup-select').show();
            $('#number-contactgroup-select').text(' ' + rows_selected.length + ' (' + message.Row + ')')

        }
        else {
            $('#btn-deleteall-group').addClass('hide');
            $('#btn-export-contact-group').addClass('hide');
            $('#number-contactgroup-select').hide();
            $('#number-contactgroup-select').text('');
        }
        if (rows_selected.length >= 2) {
          
            $('#btn-merge-contact-group').removeClass('hide');
        }
        else {
           
            $('#btn-merge-contact-group').addClass('hide');
        }

        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrl(table);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on table cells with checkboxes
    //$('#tableContactGroup').on('click', 'tbody td, thead th:first-child', function (e) {
    //    $(this).parent().find('input[type="checkbox"]').trigger('click');
    //});

    // Handle click on "Select all" control
    $('input[name="select_all"]').on('click', function (e) {
        if (this.checked) {
            $(tableId + ' tbody input[type="checkbox"]:not(:checked)').trigger('click');
           
            $('#btn-export-contact-group').removeClass('hide');
        } else {
            $(tableId + ' tbody input[type="checkbox"]:checked').trigger('click');
            
           
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    table.on('draw', function () {
        // Update state of "Select all" control
        global_updateDataTableSelectAllCtrl(table);
    });

};


//xóa nhiều contactgroup
function contactgroup_Delete(apiDomain, access_token, tableId, message) {
    $('#btn-deleteall-group').on('click', function () {
        var groupisauto = [];
        {
            $.each(countact_group_auto, function (i, o) {
                if (o.campaignId != null && o.campaignId != '')
                {
                    groupisauto.push(o.campaignId);
                }
            });
        }
        if (groupisauto.length == 0) { //không có cái nào là auto
            if (rows_selected.length > 0) {
                //mở model xác nhận
                $('#modalconfirmdeleteall').modal('toggle');
            }
        } else { //nếu có hiện thông báo 
            custom_shownotification("warning", message.WarningDeleteGroupAuto);
        }
    });

    $('#confirmDeleteAll').on('click', function () {
        $('#divLoading').show();

        $('#modalconfirmdeleteall').modal('toggle');
        $.ajax({
            url: apiDomain + '/api/ContactGroup/DeleteMany',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            data: JSON.stringify(rows_selected),
            success: function (r) {
                App.unblockUI('body');
                $('#divLoading').hide();

                errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
                switch (errorcode) {
                    case 0:
                        custom_shownotification("success", message.DeleteSuccess);
                        rows_selected = [];
                        $('#btn-deleteall-group').addClass('hide');
                        $('#btn-merge-contact-group').addClass('hide');
                        $('#btn-export-contact-group').addClass('hide');
                        rows_selected = [];
                        $(tableId).dataTable().fnDraw();

                        break;
                    default: custom_shownotification("error", message.DeleteError);
                        break;
                }
            },
            error: function (x, s, e) {

                $('#divLoading').hide();

                custom_shownotification("error", message.DeleteErro);
            }
        })
    });
}

function contactgroup_WarningMergerGroup()
{
    $('#btn-merge-contact-group').on('click', function () {
        $('#modal-warning-merger-group').modal('toggle');
    });

    $('#confirm-merge-group').on('click', function () {
        $('#modal-warning-merger-group').modal('toggle');
        $('#modal-merge-group').modal('toggle');
    });
}


function contactgroup_MergerGroup(apiDomain, access_token, tableId, message) {
    $('#action-merge-group').on('click', function () {
        if ($('#mergegroupname').val()) {

            data = {
                ContactGroupIds: rows_selected,
                ContactGroupName: $('#mergegroupname').val()
            };
            $('#divLoading').show();
            $.ajax({
                url:apiDomain+ 'api/ContactGroup/Merge',
                type: 'post',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                beforeSend: function (request) {
                    request.setRequestHeader('Authorization', 'Bearer ' + access_token);
                },
                success: function (r) {
                    $('#divLoading').hide();
                    switch (parseInt(r.ErrorCode)) {
                        case 0:
                            $('#modal-merge-group').modal('toggle');
                            rows_selected = [];
                            $(tableId).dataTable().fnDraw();
                            custom_shownotification("success", message.MergerSuccess);
                            break;
                        default:
                            custom_shownotification("success", message.MergerError);
                            break;

                    }
                },
                error: function (x, s, e) {
                    custom_shownotification("error", message.MergerError);
                    $('#divLoading').hide();
                }
            });
        } else {
            
            custom_shownotification("error", message.GroupNameRequire);
        }
    });
  
}


function contactgroup_OpenExport()
{
    $('#btn-export-contact-group').on('click', function () {
        var Type=1 //export gỏup;
        contactgroup_Modal(Type);

    });

    $('#btn-export-all-contact-group').on('click', function () {
        var Type=2 //export all;
        contactgroup_Modal(Type);
    });
}

function contactgroup_Modal(Type)
{
  
    $('#file-export-name').prop('disabled', false);
    $('#submit-export-group-contact').prop('disabled', false);
    $('#table-file-export').children().remove();
    $('#file-export-name').val('Export-contact-group'+ moment().format('YYYY-MM-DD-HH-ss'));
    $('#modal-export-group-contact').modal('toggle');
    $('#submit-export-group-contact').attr('data-type', Type);
}

function contactgroup_SubmitExport(apiDomain, access_token, message)
{
    $('#submit-export-group-contact').on('click', function () {
        var Type = $('#submit-export-group-contact').attr('data-type');
        $('#table-file-export').children().remove();
        if ($('#file-export-name').val()) {

            var html = '<table style="width:100%;" class="table table-striped">' +
                                   ' <thead>' +
                                       ' <tr>' +
                                       '   <td>' +
                                          message.FileName+
                                           ' </td>' +
                                          '  <td>' +
                                           message.Status +
                                                    '</td>' +
                                                    '<td>' +
                                                     message.CreateDate +
                                                  '  </td>' +
                                             '   </tr>' +
                                           '  </thead>' +
                                          '  <tbody>' +
                                              '  <tr>' +
                                                  '  <td>' +
                                                          locdauExport($('#file-export-name').val()) +
                                                  '  </td>' +
                                                 '   <td id="status-export">' +
                                                      '<div><i class="fa fa-spinner fa-pulse fa-fw"></i> ' + message.Processing + ' </div>' +
                                                   ' </td>' +
                                                 '   <td>' +
                                                     moment().format('YYYY-MM-DD HH:mm') +
                                                   ' </td>' +
                                              '  </tr>' +
                                           ' </tbody>' +
                                      '  </table>';
            $('#table-file-export').append(html);
            $('#file-export-name').prop('disabled', true);
            $('#submit-export-group-contact').prop('disabled', true);
            contactgroup_CreateExport(apiDomain, access_token, Type, message);


        }
        else {
            custom_shownotification("error", message.FileNameRequire);
        }
    })
}

var checkfileexport;

function contactgroup_CreateExport(apiDomain, access_token, Type, message)
{
    if (Type == 1)
    {
        var ContactGroupIds = rows_selected;
    }
    else if (Type == 2)
    {
       var  ContactGroupIds = [];
    }
    var data = {
        FileType: 'xlsx',
        FileName: locdauExport($('#file-export-name').val()).replace(/\s/g, "-"),
        ContactGroupIds: ContactGroupIds,
    }
    $.ajax({
        url: apiDomain + 'api/report/CreateContactExport',
        type: 'post',
        data:JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        beforeSend:function(request)
        {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);

        },
        success:function(r)
        {
            
            switch(parseInt(r.ErrorCode))
            {
                
                case 0:
                    {
                        var data = r.Data;
                        var ExportId = data.ExportId;
                        switch (parseInt(data.Status)) {
                            case 0: //pendding
                                checkfileexport= setInterval(function () { contactgroup_CheckExportComplete(apiDomain, access_token, ExportId, message); }, 3000);
                                break;
                            case 1:
                                var html = '<div><a href=' + data.FilePath + '><i class="fa fa-cloud-download font-green-jungle"></i> '+message.DownLoad+'</div>';
                                setTimeout(function () {
                                    $('#status-export').children().remove();
                                    $('#status-export').append(html);
                                }, 3000);
                                break;
                            case 2:
                                var html = '<div><i class="fa fa-remove font-red"></i> ' + message.ExportError + ' </div>';
                                $('#status-export').children().remove();
                                $('#status-export').append(html);
                                break;

                        }
                    }
                    break;
                default:
                    custom_shownotification("error", message.ExportError);
                    break;
                   
                    
            }
        },
        error: function (x, s, e) {
            custom_shownotification("error", message.ExportError);
        }
    })
}

function contactgroup_CheckExportComplete(apiDomain, access_token, ExportId, message)
{
    $.ajax({
        url: apiDomain + 'api/report/GetContactExport?ExportId='+ExportId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);

        },
        success: function (r) {
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    var data = r.Data;
                    switch (parseInt(data.Status)) {
                        case 1:
                            var html = '</div><a href=' + data.FilePath + '><i class="fa fa-cloud-download font-green-jungle"></i> '+message.DownLoad+' </a></div>';
                            setTimeout(function () {
                                $('#status-export').children().remove();
                                $('#status-export').append(html);
                            }, 3000);
                            clearInterval(checkfileexport);
                            break;
                        case 2:
                            var html = '<div><i class="fa fa-remove font-red"></i> ' + message.ExportError + '</div>';
                            $('#status-export').children().remove()
                            $('#status-export').append(html);
                            clearInterval(checkfileexport);
                            break;

                    }
                    break;

            }
        },
        error: function (x, s, e) {
            custom_shownotification("error", 'Có lỗi xảy ra');
            clearInterval(checkfileexport);
        }
    })
}


//function contactgroup_OpenTools(tableId)
//{
//    var html="";
//    $(tableId + ' tbody').on('mouseover', 'tr', function () {
//       // var tablerow = $(tableId).DataTable();
//       // var rowData = tablerow.row($(this)).data();
//        html = '<div id="divtootsgroup" class="btn-group">' +

//                                           '<button class="btn blue dropdown-toggle" data-toggle="dropdown">Open Me!' +
//       '  <i class="fa fa-angle-down"></i>' +
//       '</button>' +
//       ' <ul class="dropdown-menu" role="menu">' +
//       ' <li role="presentation">' +
//       '<a role="menuitem" tabindex="-1" href="javascript:;"> Action' +
//       '<span class="badge badge-success"> 2 </span>' +
//       '  </a>' +
//       '</li>' +

//       '</ul>' +
//                                       '</div>'
//        $(this).children().find('#toolsgroup').append(html);

//    });
//    //}).on('mouseout', 'tr', function () {
//    //    $(this).children().find('#divtootsgroup').remove();

//    //});

  
//}
