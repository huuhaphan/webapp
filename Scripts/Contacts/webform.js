﻿function Intialize(apiDomain, FormId, Width, Height) {
    $.ajax({
        url: apiDomain + '/api/Form/Info?FormId=' + FormId,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        //beforeSend: function (request) {
        //    request.setRequestHeader("Authorization", "Bearer " + access_token);
        //},
        success: function (r) {
            var errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
            switch (errorcode) {
                case 0:
                    var data = JSON.parse(JSON.stringify(r.Data))
                    var CustomContent = data.FormContent;
                    var FormCustomFields = data.FormCustomFields;
                    var nameButtonSubmit = CustomContent.FormSubmitButtonLabel;
                    var submitsize = CustomContent.FormButtonSize;
                    addSubmit(CustomContent);
                    addHeadline(CustomContent);
                    addStyle(CustomContent, Width);
                    webform_LoadFormCustomField(apiDomain, FormCustomFields);

                    break;
            }
        }


    });
}

//tạm login để truy cập
function webform_Action(apiDomain, FormId, Width, Height) {

    Intialize(apiDomain, FormId, Width, Height);
   

}

//hàm tạo nút submit
function addSubmit(CustomContent) {

    var newItem = $(document.createElement('div')).attr('id', 'divsubmit');
    newItem.after().html(
                   '<a id="webform-send-info" class="btn blue ' + CustomContent.FormButtonSize + '" > ' + CustomContent.FormSubmitButtonLabel + ' </a>'


      );
    newItem.appendTo(".control-web-form .submit");


}

//hàm tạo headline
function addHeadline(CustomContent) {
    var newItem = $(document.createElement('div')).attr('id', 'divheader');
    newItem.after().html(
       '<span class="' + CustomContent.FormHeaderSize + '" style="border-bottom:0 !important">' + CustomContent.FormHeader + '</span>'

      );
    newItem.appendTo(".control-web-form .headline");

}

//set style 
function addStyle(CustomContent, Width) {
    //parentdiv
    $('.control-web-form').css('width', Width)
    //head
    $('.control-web-form #divheader').css({
        'text-align': CustomContent.FormHeaderAlign,
        'color': CustomContent.FormHeaderColor,

    });

    //button
    $('.control-web-form #divsubmit').css('text-align', CustomContent.FormButtonAlign);
    $('.control-web-form #divsubmit a').css({
        'color': CustomContent.FormButtonColor,
        'background-color': CustomContent.FormButtonBgColor,
    });

    //hover
    $('.control-web-form #divsubmit a').mouseout(function () {
        $(this).css({
            'color': CustomContent.FormButtonColor,
            'background-color': CustomContent.FormButtonBgColor,
        });
    });
    $('.control-web-form #divsubmit a').mouseover(function () {
        $(this).css({
            'color': CustomContent.FormButtonColorHover,
            'background-color': CustomContent.FormButtonBgColorHover,
        });
    });



}



//hàm tạo custom field
function webform_LoadFormCustomField(apiDomain, FormCustomFields) {
    $.each(FormCustomFields, function (index, obj) {
        var fieldId = obj.FieldId;
        var fieldName = obj.FieldLabel;
        var fieldTypeId = obj.FieldTypeId;
        var fieldTypeName = JSON.parse(JSON.stringify(obj.FieldType)).FieldTypeName;
        var fieldFormatId = obj.FieldFormatId;
        var isRequired = obj.IsRequired;
        var placeholder = obj.PlaceHolder;
        if (!placeholder) {
            placeholder = "";
        }
        var fieldValue = new Array();
        $.each(JSON.parse(JSON.stringify(obj.Values)), function (index, value) {
            fieldValue.push(value.FieldValue);
        });
        webfrom_ActionCreateCustomField(apiDomain, fieldId, fieldName, fieldTypeId, fieldTypeName, fieldFormatId, fieldValue, placeholder, isRequired);
    });
};

function webfrom_ActionCreateCustomField(apiDomain, fieldId, fieldName, fieldTypeId, fieldTypeName, fieldFormatId, fieldValue, placeholder, isRequired) {
    switch (fieldTypeId) {
        //text
        case 1:
            switch (fieldFormatId) {
                case 1:
                    fieldTextSingle(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, placeholder, isRequired);
                    break;
                case 2:
                    fieldTextMulti(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, placeholder, isRequired);
                    break;
                case 3:
                    fieldDropDown(apiDomain, fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired);
                    break;
                case 4:
                    fieldCheckBox(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired);
                    break;
                case 5:
                    fieldRadio(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired);
                    break;


            }
            break;
            //number
        case 2:
            switch (fieldFormatId) {
                case 1:
                    fieldNumberSingle(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, placeholder, isRequired);
                    break;
                case 2:
                    fieldNumberMulti(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, placeholder, isRequired);
                    break;
                case 3:
                    fieldDropDown(apiDomain, fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired);
                    break;
                case 4:
                    fieldCheckBox(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired)
                    break;
                case 5:
                    fieldRadio(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired)
                    break;

            }
            break;
            //email
        case 3:
            switch (fieldFormatId) {
                case 1:
                    fieldTextSingle(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, placeholder, isRequired);
                    break;
            }
            break;
        case 4:
            switch (fieldFormatId) {
                case 1:
                    fieldTextSingle(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, placeholder, isRequired);
                    break;
            }
            break;
        case 6:
            switch (fieldFormatId) {
                case 1:
                    fieldNumberSingle(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, placeholder, isRequired);
                    break;
            }
            break;
        case 7:
            switch (fieldFormatId) {
                case 1:
                    fieldDateSingle(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired);
                    break;
                case 3:
                    fieldDropDown(apiDomain, fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired);
                    break;
                case 4:
                    fieldCheckBox(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired);
                    break;
                case 5:
                    fieldRadio(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired);
                    break;
            }
            break;
        case 8:
            switch (fieldFormatId) {
                case 1:
                    fieldDateTimeSingle(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired);
                    break;
                case 3:
                    fieldDropDown(apiDomain, fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired);
                    break;
                case 4:
                    fieldCheckBox(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired);
                    break;
                case 5:
                    fieldRadio(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired);
                    break;
            }
            break;
        case 10:
            switch (fieldFormatId) {
                case 3:
                    fieldDropDown(apiDomain, fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired);
                    break;

                case 5:
                    fieldRadio(fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired);
                    break;
            }
            break;
        case 11:
            switch (fieldFormatId) {
                case 3:
                    fieldDropDown(apiDomain, fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired);
                    break;
            }
            break;
        case 12:
            switch (fieldFormatId) {
                case 3:
                    fieldDropDown(apiDomain, fieldId, fieldName, fieldValue, fieldTypeId, fieldFormatId, fieldTypeName, isRequired);
                    break;
            }
            break;




    }
}

function fieldTextSingle(fieldId, fieldName, fieldValue, fieldType, fieldFormat, fieldTypeName, placeholder, isRequired) {

    webform_CheckValue(fieldValue);
    if (isRequired == true) {
        var newItem = $(document.createElement('li')).attr("id", fieldId);
        newItem.after().html(
            '<div id="div' + fieldId + '" class="" data-formatId="' + fieldFormat + '" data-typeId="' + fieldType + '" data-component="text-box">' +
            '<label id="captionname" class="checkrequired">' + fieldName + '</label>' +
            '<span class="required" style="color: rgb(255, 0, 0);">*</span>' +
            '<div>' +
                  '<input type="text" id="' + fieldName + '" class="form-control input-sm" value="' + fieldValue[0] + '" name="' + fieldTypeName + '" placeholder="' + placeholder + '">' +
                  '<div class="form-control-focus"></div>' +
            '</div>' +
            '</div>'
            );
    }
    else {
        var newItem = $(document.createElement('li')).attr("id", fieldId);
        newItem.after().html(
            '<div id="div' + fieldId + '" class="" data-formatId="' + fieldFormat + '" data-typeId="' + fieldType + '" data-component="text-box">' +
            '<label id="captionname">' + fieldName + '</label>' +
            '<span class="required"></span>' +
            '<div>' +
                  '<input type="text" id="' + fieldName + '" class="form-control input-sm" value="' + fieldValue[0] + '" name="' + fieldTypeName + '" placeholder="' + placeholder + '">' +
                  '<div class="form-control-focus"></div>' +
            '</div>' +
            '</div>'
    );
    }

    newItem.appendTo(".control-web-form .items");
    ChangeStyle('div' + fieldId);
}

function fieldDateSingle(fieldId, fieldName, fieldValue, fieldType, fieldFormat, fieldTypeName, isRequired) {

    webform_CheckValue(fieldValue);
    if (isRequired == true) {
        var newItem = $(document.createElement('li'))
                          .attr("id", fieldId);
        newItem.after().html(
            '<div id="div' + fieldId + '" class="" data-formatid="' + fieldFormat + '" data-typeid="' + fieldType + '" data-component="text-box">' +

           '<label id="captionname" class="checkrequired">' + fieldName + '</label>' +
            '<span class="required" style="color: rgb(255, 0, 0);">*</span>' +
            '<div>' +
                 '<input id="' + fieldName + '" class="form-control form-control-inline input-sm date-picker" value="' + fieldValue[0] + '" name="' + fieldTypeName + '"/>' +
                 '<div class="form-control-focus"></div>' +
            '</div>' +
            '</div>'
     );
    }
    else {
        var newItem = $(document.createElement('li'))
                          .attr("id", fieldId);
        newItem.after().html(
            '<div id="div' + fieldId + '" class="" data-formatid="' + fieldFormat + '" data-typeid="' + fieldType + '" data-component="text-box">' +

            '<label id="captionname">' + fieldName + '</label>' +
            '<span class="required"></span>' +
            '<div>' +
                 '<input id="' + fieldName + '" class="form-control form-control-inline input-sm date-picker" value="' + fieldValue[0] + '" name="' + fieldTypeName + '"/>' +
                 '<div class="form-control-focus"></div>' +
            '</div>' +
            '</div>'
     );
    }

    newItem.appendTo(".control-web-form .items");
    datesingle();
    ChangeStyle('div' + fieldId);
}

function fieldDateTimeSingle(fieldId, fieldName, fieldValue, fieldType, filedFormat, fieldTypeName, isRequired) {

    webform_CheckValue(fieldValue);
    if (isRequired == true) {
        var newItem = $(document.createElement('li'))
                           .attr("id", fieldId);
        newItem.after().html(
            '<div id="div' + fieldId + '" class="form-group" data-formatid="' + filedFormat + '" data-typeid="' + fieldType + '" data-component="text-box">' +
               '<label id="captionname" class="checkrequired">' + fieldName + '</label>' +
            '<span class="required" style="color: rgb(255, 0, 0);">*</span>' +
                '<div class="input-group date form_datetime"> ' +
                    '<input id="' + fieldName + '" class="form-control" value="' + fieldValue[0] + '" name="' + fieldTypeName + '" />' +
                    '<span class="input-group-btn">' +
                        '<button class="btn default date-set" type="button">' +
                            '<i class="fa fa-calendar"></i>' +
                        '</button>' +
                    '</span>' +
                  '</div>' +
              '</div>'

              );
    }
    else {
        var newItem = $(document.createElement('li'))
                           .attr("id", fieldId);
        newItem.after().html(
            '<div id="div' + fieldId + '" class="form-group" data-formatid="' + filedFormat + '" data-typeid="' + fieldType + '" data-component="text-box">' +
                '<label id="captionname">' + fieldName + '</label>' +
                '<div class="input-group date form_datetime"> ' +
                    '<input id="' + fieldName + '" class="form-control" value="' + fieldValue[0] + '" name="' + fieldTypeName + '" />' +
                    '<span class="input-group-btn">' +
                        '<button class="btn default date-set" type="button">' +
                            '<i class="fa fa-calendar"></i>' +
                        '</button>' +
                    '</span>' +
                  '</div>' +
              '</div>'

              );
    }

    newItem.appendTo(".control-web-form .items");
    datetimesingle();
    ChangeStyle('div' + fieldId);
}


function fieldNumberSingle(fieldId, fieldName, fieldValue, fieldType, fieldFormat, fieldTypeName, placeholder, isRequired) {

    webform_CheckValue(fieldValue);
    if (isRequired == true) {
        var newItem = $(document.createElement('li'))
                           .attr("id", fieldId);
        newItem.after().html('<div id="div' + fieldId + '" class="" data-formatid="' + fieldFormat + '" data-typeid="' + fieldType + '" data-component="text-box">' +
          '<label id="captionname" class="checkrequired">' + fieldName + '</label>' +
            '<span class="required" style="color: rgb(255, 0, 0);">*</span>' +
     '<div><input type="text" id="' + fieldName + '" class="form-control input-sm" onkeypress="return isNumber(event)" value="' + fieldValue[0] + '" name="' + fieldTypeName + '"placeholder="' + placeholder + '"></div></div>'
     );
    }
    else {
        var newItem = $(document.createElement('li'))
                           .attr("id", fieldId);
        newItem.after().html('<div id="div' + fieldId + '" class="" data-formatid="' + fieldFormat + '" data-typeid="' + fieldType + '" data-component="text-box">' +
           '<label id="captionname">' + fieldName + '</label>' +
     '<div><input type="text" id="' + fieldName + '" class="form-control input-sm" onkeypress="return isNumber(event)" value="' + fieldValue[0] + '" name="' + fieldTypeName + '"placeholder="' + placeholder + '"></div></div>'
     );
    }

    newItem.appendTo(".control-web-form .items");
    ChangeStyle('div' + fieldId);

}

function fieldTextMulti(fieldId, fieldName, fieldValue, fieldType, fieldFormat, fieldTypeName, placeholder, isRequired) {
    webform_CheckValue(fieldValue);
    if (isRequired == true) {
        var newItem = $(document.createElement('li'))
                      .attr("id", fieldId);
        newItem.after().html(
            '<div id="div' + fieldId + '" class="" data-formatid="' + fieldFormat + '" data-typeid="' + fieldType + '" data-component="text-box">' +
           '<label id="captionname" class="checkrequired">' + fieldName + '</label>' +
            '<span class="required" style="color: rgb(255, 0, 0);">*</span>' +
                 '<div>' +
                    '<textarea id="' + fieldName + '" class="form-control" row="3" value="' + fieldValue[0] + '" name="' + fieldTypeName + '" placeholder="' + placeholder + '"></textarea>' +
                    '<div class="form-control-focus"></div>' +
                 '</div>' +
            '</div>'
                 );
    }
    else {
        var newItem = $(document.createElement('li'))
                      .attr("id", fieldId);
        newItem.after().html(
            '<div id="div' + fieldId + '" class="" data-formatid="' + fieldFormat + '" data-typeid="' + fieldType + '" data-component="text-box">' +
            '<label id="captionname">' + fieldName + '</label>' +
            '<span class="required"></span>' +
                 '<div>' +
                    '<textarea id="' + fieldName + '" class="form-control" row="3" value="' + fieldValue[0] + '" name="' + fieldTypeName + '" placeholder="' + placeholder + '"></textarea>' +
                    '<div class="form-control-focus"></div>' +
                 '</div>' +
            '</div>'
                 );
    }

    newItem.appendTo(".control-web-form .items");
    ChangeStyle('div' + fieldId);
}

function fieldNumberMulti(fieldId, fieldName, fieldValue, fieldType, fieldFormat, fieldTypeName, isRequired) {

    webform_CheckValue(fieldValue);
    if (isRequired == true) {
        var newItem = $(document.createElement('li'))
                        .attr("id", fieldId);
        newItem.after().html(
            '<div id="div' + fieldId + '" class="" data-formatid="' + fieldFormat + '" data-typeid="' + fieldType + '" data-component="text-box">' +
           '<label id="captionname" class="checkrequired">' + fieldName + '</label>' +
            '<span class="required" style="color: rgb(255, 0, 0);">*</span>' +
            '<div>' +
                '<textarea id="' + fieldName + '" class="form-control" row="3" value="' + fieldValue[0] + '" onkeypress="return isNumber(event)" name="' + fieldTypeName + '"></textarea>' +
                '<div class="form-control-focus"></div>' +
            '</div>' +
            '</div>'
                 );
    }
    else {
        var newItem = $(document.createElement('li'))
                        .attr("id", fieldId);
        newItem.after().html(
            '<div id="div' + fieldId + '" class="" data-formatid="' + fieldFormat + '" data-typeid="' + fieldType + '" data-component="text-box">' +
            '<label id="captionname">' + fieldName + '</label>' +
            '<span class="required"></span>' +
            '<div>' +
                '<textarea id="' + fieldName + '" class="form-control" row="3" value="' + fieldValue[0] + '" onkeypress="return isNumber(event)" name="' + fieldTypeName + '"></textarea>' +
                '<div class="form-control-focus"></div>' +
            '</div>' +
            '</div>'
                 );
    }

    newItem.appendTo(".control-web-form .items");
    ChangeStyle('div' + fieldId);

}


function fieldDropDown(apiDomain, fieldId, fieldName, fieldValue, fieldType, fieldFormat, fieldTypeName, isRequired) {

    webform_CheckValue(fieldValue);
    if (isRequired == true) {
        var newItem = $(document.createElement('li'))
                        .attr("id", fieldId);
        newItem.after().html('<div id="div' + fieldId + '" class="" data-formatid="' + fieldFormat + '" data-typeid="' + fieldType + '"data-component="dropdown-list">' +
         '<label id="captionname" class="checkrequired">' + fieldName + '</label>' +
            '<span class="required" style="color: rgb(255, 0, 0);">*</span>' +
                 '<div id="' + fieldId + '"></div></div>'
                 );
    }
    else {
        var newItem = $(document.createElement('li'))
                        .attr("id", fieldId);
        newItem.after().html('<div id="div' + fieldId + '" class="" data-formatid="' + fieldFormat + '" data-typeid="' + fieldType + '"data-component="dropdown-list">' +
           '<label id="captionname">' + fieldName + '</label>' +
                 '<div id="' + fieldId + '"></div></div>'
                 );
    }

    newItem.appendTo(".control-web-form .items");
    ChangeStyle('div' + fieldId);

    if (fieldId == 17) { //load quốc gia
        webform_GetCountryOnload(apiDomain, fieldId, fieldName, fieldTypeName, isRequired);
    }
    else {
        var arrayLength = fieldValue.length;
        var options = new Array();
        for (var i = 0; i < arrayLength; i++) {
            options.push('<option value="' + fieldValue[i] + '">' + fieldValue[i] + '</option>');
            //Do something
        }
        $('<select id="' + fieldId + '" type="text" id="' + fieldName + '" class="form-control" name="' + fieldTypeName + '"/>').append(options.join('')).appendTo('#div' + fieldId);
    }
};

function fieldCheckBox(fieldId, fieldName, fieldValue, fieldType, fieldFormat, fieldTypeName, isRequired) {

    webform_CheckValue(fieldValue);
    if (isRequired == true) {
        var newItem = $(document.createElement('li'))
                        .attr("id", fieldId);
        newItem.after().html('<div id="div' + fieldId + '" data-formatid="' + fieldFormat + '" data-typeid="' + fieldType + '" data-component="check-box">' +
                                    '<label id="captionname" class="checkrequired">' + fieldName + '</label>' +
            '<span class="required" style="color: rgb(255, 0, 0);">*</span>' +
                                      '<div class="md-checkbox-list">' +
                                        '</div>' +
                                        '</div>'
                                        );
    }
    else {
        var newItem = $(document.createElement('li'))
                            .attr("id", fieldId);
        newItem.after().html('<div id="div' + fieldId + '" data-formatid="' + fieldFormat + '" data-typeid="' + fieldType + '" data-component="check-box">' +
                                    '<label id="captionname">' + fieldName + '</label>' +
                                      '<div class="md-checkbox-list">' +
                                        '</div>' +
                                        '</div>'
                                        );
    }

    newItem.appendTo(".control-web-form .items");
    var arrayLength = fieldValue.length;

    var options = new Array();
    for (var i = 0; i < arrayLength; i++) {
        options.push('<div class="md-checkbox" id="md-checkbox-' + i + '">' +
        '<input type="checkbox" id="' + fieldName + i + '" value="' + fieldValue[i] + '" id="' + fieldName + '" class="md-check" name="' + fieldTypeName + '">' +
        '<label  for="' + fieldName + i + '">' +
            '<span class="inc"></span>' +
            '<span class="check"></span>' +
            '<span class="box"></span> ' + fieldValue[i] + ' </label>' +
    '</div>');

    }
    $('#div' + fieldId + ' .md-checkbox-list').append(options.join(''));
    ChangeStyle('div' + fieldId);
}

function fieldRadio(fieldId, fieldName, fieldValue, fieldType, fieldFormat, fieldTypeName, isRequired) {
    webform_CheckValue(fieldValue)
    if (isRequired == true) {
        var newItem = $(document.createElement('li')).attr("id", fieldId);
        newItem.after().html('<div id="div' + fieldId + '" data-formatid="' + fieldFormat + '" data-typeid="' + fieldType + '" data-component="radio-button"> ' +
                            '<label id="captionname" class="checkrequired">' + fieldName + '</label>' +
            '<span class="required" style="color: rgb(255, 0, 0);">*</span>' +
                                        '<div class="md-radio-list">' +
                                        '</div>' +
                                        '</div>'
                                       );
    }
    else {
        var newItem = $(document.createElement('li')).attr("id", fieldId);
        newItem.after().html('<div id="div' + fieldId + '" data-formatid="' + fieldFormat + '" data-typeid="' + fieldType + '" data-component="radio-button"> ' +
                            '<label id="captionname">' + fieldName + '</label>' +
                                        '<div class="md-radio-list">' +
                                        '</div>' +
                                        '</div>'
                                       );
    }

    newItem.appendTo(".control-web-form .items");
    var arrayLength = fieldValue.length;
    var options = new Array();
    for (var i = 0; i < arrayLength; i++) {
        options.push('<div class="md-radio" id="md-radio-' + i + '">' +
        '<input type="radio" id="' + fieldName + i + '" name="' + fieldName + '" value="' + fieldValue[i] + '" class="md-radiobtn" name="' + fieldTypeName + '">' +
        '<label for="' + fieldName + i + '">' +
            '<span class="inc"></span>' +
            '<span class="check"></span>' +
            '<span class="box"></span> ' + fieldValue[i] + ' </label>' +
    '</div>');
    }
    $('#div' + fieldId + ' .md-radio-list').append(options.join(''));
    ChangeStyle('div' + fieldId);
}


function webform_CheckValue(fieldValue) {
    if (!fieldValue) {
        fieldValue = '';
    }
}

var md = false;
function ChangeStyle(id) {
    var style = 'form-group';
    if (md == true) {
        style = 'form-group form-md-line-input form-md-floating-label has-info';
        $("#" + id).addClass(style);
    }
    else {
        $("#" + id).addClass(style);
    }
}

//khai bao ngày
function datesingle() {
    $('.date-picker').datepicker({
        // rtl: App.isRTL(),
        orientation: "left",
        autoclose: true
    });
}
//khai báo ngày giờ
function datetimesingle() {
    $(".form_datetime").datetimepicker({
        autoclose: true,
        // isRTL: App.isRTL(),
        format: "dd MM yyyy - hh:ii",
        pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
    });

    $(".form_advance_datetime").datetimepicker({
        // isRTL: App.isRTL(),
        format: "dd MM yyyy - hh:ii",
        autoclose: true,
        todayBtn: true,
        startDate: "2013-02-14 10:00",
        pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
        minuteStep: 10
    });

    $(".form_meridian_datetime").datetimepicker({
        // isRTL: App.isRTL(),
        format: "dd MM yyyy - HH:ii P",
        showMeridian: true,
        autoclose: true,
        pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
        todayBtn: true
    });
}

//kiểm tra text là số 
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


function webform_GetCountryOnload(apiDomain, fieldId, fieldName, fieldTypeName) {
    $.ajax({
        url: apiDomain + 'api/list/Country',
        type: 'GET',
        contentType: 'application/x-www-form-urlencoded',
        success: function (r) {
            var data = JSON.parse(JSON.stringify(r.Data));
            var options = new Array();
            for (var i = 0; i < data.length; i++) {
                options.push('<option value="' + data[i].CountryId + '">' + data[i].CountryName + '</option>');

            }
            $('<select id="' + fieldId + '" type="text" id="' + fieldName + '" class="form-control" name="' + fieldTypeName + '"/>').append(options.join('')).appendTo('#div' + fieldId);

        },
        error: function (x, s, e) {
        }

    });

};
