﻿//sự kiện update contact
function updatecontact_EventUpdate(apiDomain, access_token, ContactGroupId, contactId, message) {

    updatecontact_LoadInfo(apiDomain, access_token, contactId, message) //chạy ok nhưng không dùng vì đang dùng model truyền từ url

    $('#modalupdatecontact').on('click',function () {
        if (updatecontact_CheckNull(message) == true) {
            updatecontact_UpdateContact(apiDomain, access_token, contactId, message);
            //var paid = true;
            //if ($('#UpdateContacts #ucheckPaid').prop('checked')) {
            //    paid = 'true';
            //    updatePaid(apiDomain, ContactGroupId, paid);
            //} else {
            //    paid = 'false';
            //    updatePaid(apiDomain, ContactGroupId, paid);
            //}
            
            
        }
    })
}

function updatecontact_LoadInfo(apiDomain, access_token, ContactId, message) {
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/Contact/GetInfo?ContactId=' + ContactId,
        type: 'GET',
        contentType: 'application/x-www-form-urlencoded',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + access_token);
        },
        success: function (result) {
            var data = JSON.parse(JSON.stringify(result.Data))
            $('#UpdateContacts #txtContactName').val(data.ContactName);
            $('#UpdateContacts #txtPhone').val(data.Mobile);
            $('#UpdateContacts #txtEmail').val(data.Email);
            $('#UpdateContacts #txtNote').val(data.Note);
            if (data.BirthDate != null) {
                $('#UpdateContacts #txtBirthday').datepicker("setDate", new Date(data.BirthDate));
            }
            if (data.Active === 'True' | data.Active === true) {
                $('#ucheckContactActive').prop('checked', true);
            }
            $('#UpdateContacts #txtFirstName').val(data.FirstName);
            $('#UpdateContacts #txtLastName').val(data.LastName);

            $('#UpdateContacts #txtGender').val(data.Gender);


            if (data.CustomFields.length > 0) {
                var cf = data.CustomFields;
                var dataage = getObjects(cf, 'CustomFieldName', 'Age')[0];
                if (dataage) {
                    $('#UpdateContacts #txtAge').val(dataage.CustomFieldValue)
                };
                
                var txtFax = getObjects(cf, 'CustomFieldName', 'Fax')[0];
                if (txtFax) {
                    $('#UpdateContacts #txtFax').val(txtFax.CustomFieldValue)
                };
                var txtCompany = getObjects(cf, 'CustomFieldName', 'Company')[0];
                if (txtCompany) {
                    $('#UpdateContacts #txtCompany').val(txtCompany.CustomFieldValue)
                };
                var txtUrl = getObjects(cf, 'CustomFieldName', 'URL')[0];
                if (txtUrl) {
                    $('#UpdateContacts #txtUrl').val(txtUrl.CustomFieldValue)
                };
                var txtCity = getObjects(cf, 'CustomFieldName', 'City')[0];
                if (txtCity) {
                    $('#UpdateContacts #txtCity').val(txtCity.CustomFieldValue)
                };
                var txtStreet = getObjects(cf, 'CustomFieldName', 'Street')[0];
                if (txtStreet) {
                    $('#UpdateContacts #txtStreet').val(txtStreet.CustomFieldValue)
                };
                var txtAddress = getObjects(cf, 'CustomFieldName', 'Address')[0];
                if (txtAddress) {
                    $('#UpdateContacts #txtAddress').val(txtAddress.CustomFieldValue)
                };
                var txtNote1 = getObjects(cf, 'CustomFieldName', 'Comment1')[0];
                if (txtNote1) {
                    $('#UpdateContacts #txtNote1').val(txtNote1.CustomFieldValue)
                };
                var txtNote2 = getObjects(cf, 'CustomFieldName', 'Comment2')[0];
                if (txtNote2) {
                    $('#UpdateContacts #txtNote2').val(txtNote2.CustomFieldValue)
                };
                var txtNote3 = getObjects(cf, 'CustomFieldName', 'Comment3')[0];
                if (txtNote3) {
                    $('#UpdateContacts #txtNote3').val(txtNote3.CustomFieldValue)
                };
            }
            $('#divLoading').hide();
        },
        error: function () {

            $('#divLoading').hide();
            custom_shownotification("error", message.Error);
        }
    });
}

//update contact
function updatecontact_UpdateContact(apiDomain, access_token, contactId, message) {
  
    var active = true;
    if ($('#UpdateContacts #ucheckContactActive').prop('checked')) {
        active = true;
    } else {
        active = false;
    }

    var mobile = $('#UpdateContacts #txtPhone').val();
    if (parseInt(mobile.substr(0, 2)) == 84) {
        mobile = '0' + mobile.replace(mobile.substr(0, 2), '');
    }
    if (mobile.substr(0, 3) == '+84') {
        mobile = '0' + mobile.replace(mobile.substr(0, 3), '');
    }

    var BirthDate = "";
    if ($('#UpdateContacts #txtBirthday').val() != null && $('#UpdateContacts #txtBirthday').val() != '') {
        BirthDate = moment($('#UpdateContacts #txtBirthday').datepicker('getDate')).format('YYYY-MM-DD');

    }

    var accept = true;
    var sytemcustomfield = localStorage.getItem('SytemCustomField');
    var fullcustomfield = JSON.parse(sytemcustomfield); //đã lưu tại contactlist.js


    var CustomFields = [];
    if ($('#UpdateContacts #txtAge').val() != null && $('#UpdateContacts #txtAge').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 7);
        var maxLength = getValues(object, 'MaxLength');

        if ($('#UpdateContacts #txtAge').val().length > maxLength[0]) {

            custom_shownotification("error", 'Age vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }

        CustomFields.push({ CustomFieldId: 7, CustomFieldValue: $('#UpdateContacts #txtAge').val(), CustomFieldName: 'Age' });
    }
    

    if ($('#UpdateContacts #txtFax').val() != null && $('#UpdateContacts #txtFax').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 13);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#UpdateContacts #txtFax').val().length > maxLength[0]) {

            custom_shownotification("error", 'Fax vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }

        CustomFields.push({ CustomFieldId: 13, CustomFieldValue: $('#UpdateContacts #txtFax').val(), CustomFieldName: 'Fax' });
    }
    if ($('#UpdateContacts #txtCompany').val() != null && $('#UpdateContacts #txtCompany').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 14);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#UpdateContacts #txtCompany').val().length > maxLength[0]) {

            custom_shownotification("error", 'Company vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }

        CustomFields.push({ CustomFieldId: 14, CustomFieldValue: $('#UpdateContacts #txtCompany').val(), CustomFieldName: 'Company' });
    }
    if ($('#UpdateContacts #txtUrl').val() != null && $('#UpdateContacts #txtUrl').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 16);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#UpdateContacts #txtUrl').val().length > maxLength[0]) {

            custom_shownotification("error", 'URL vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }

        CustomFields.push({ CustomFieldId: 16, CustomFieldValue: $('#UpdateContacts #txtUrl').val(), CustomFieldName: 'URL' });
    }
    if ($('#UpdateContacts #txtCity').val() != null && $('#UpdateContacts #txtCity').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 19);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#UpdateContacts #txtCity').val().length > maxLength[0]) {

            custom_shownotification("error", 'City vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }

        CustomFields.push({ CustomFieldId: 19, CustomFieldValue: $('#UpdateContacts #txtCity').val(), CustomFieldName: 'City' });
    }
    if ($('#UpdateContacts #txtStreet').val() != null && $('#UpdateContacts #txtStreet').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 20);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#UpdateContacts #txtStreet').val().length > maxLength[0]) {

            custom_shownotification("error", 'Street vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }

        CustomFields.push({ CustomFieldId: 20, CustomFieldValue: $('#UpdateContacts #txtStreet').val(), CustomFieldName: 'Street' });
    }
    if ($('#UpdateContacts #txtAddress').val() != null && $('#UpdateContacts #txtAddress').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 21);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#UpdateContacts #txtAddress').val().length > maxLength[0]) {

            custom_shownotification("error", 'Address vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }

        CustomFields.push({ CustomFieldId: 21, CustomFieldValue: $('#UpdateContacts #txtAddress').val(), CustomFieldName: 'Address' });
    }

    if ($('#UpdateContacts #txtNote').val() != null && $('#UpdateContacts #txtNote1').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 15);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#UpdateContacts #txtNote').val().length > maxLength[0]) {

            custom_shownotification("error", 'Comment vượt quá giới hạn kí tự cho phép. ' + maxLength[0]+' kí tự');
            accept = false;
            return;
        }

    }

    if ($('#UpdateContacts #txtNote1').val() != null && $('#UpdateContacts #txtNote1').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 22);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#UpdateContacts #txtNote1').val().length > maxLength[0]) {

            custom_shownotification("error", 'Comment1 vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }

        CustomFields.push({ CustomFieldId: 22, CustomFieldValue: $('#UpdateContacts #txtNote1').val(), CustomFieldName: 'Comment1' });
    }
    if ($('#UpdateContacts #txtNote2').val() != null && $('#UpdateContacts #txtNote2').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 23);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#UpdateContacts #txtNote2').val().length > maxLength[0]) {

            custom_shownotification("error", 'Comment2 vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }

        CustomFields.push({ CustomFieldId: 23, CustomFieldValue: $('#UpdateContacts #txtNote2').val(), CustomFieldName: 'Comment2' });
    }
    if ($('#UpdateContacts #txtNote3').val() != null && $('#UpdateContacts #txtNote3').val() != '') {

        var object = getObjects(fullcustomfield, 'FieldId', 24);
        var maxLength = getValues(object, 'MaxLength');
        if ($('#UpdateContacts #txtNote3').val().length > maxLength[0]) {

            custom_shownotification("error", 'Comment3 vượt quá giới hạn kí tự cho phép. ' + maxLength[0] + ' kí tự');
            accept = false;
            return;
        }

        CustomFields.push({ CustomFieldId: 24, CustomFieldValue: $('#UpdateContacts #txtNote3').val(), CustomFieldName: 'Comment3' });
    }

    var data = {
        ContactId: contactId,
        ContactName: $('#UpdateContacts #txtContactName').val(),
        Email: $('#UpdateContacts #txtEmail').val(),
        Mobile: mobile,
        Note: $('#UpdateContacts #txtNote').val(),
        Active: active,
        FirstName: $('#UpdateContacts #txtFirstName').val(),
        LastName: $('#UpdateContacts #txtLastName').val(),
        BirthDate: BirthDate,
        Gender: $('#UpdateContacts #txtGender').val(),
        CustomFields: CustomFields,


    };

    if (accept) {
        $('#divLoading').show();
        $.ajax({
            url: apiDomain + 'api/Contact/Update',
            type: 'POST',
            data: data,
            contentType: 'application/x-www-form-urlencoded',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Bearer " + access_token);
            },
            success: function (r) {
                var errorcode = parseInt(r.ErrorCode);
                switch (errorcode) {
                    case 0:
                        custom_shownotification("success", message.Success);
                        $('#modalCreateContact').modal('toggle');
                        $('#tableContact').dataTable().fnDraw();
                        break;
                    case 105:
                        $('.alert-danger span', $('#UpdateContacts')).text(message.EmailError);
                        $('.alert-danger', $('#UpdateContacts')).show();
                        break;
                }
                $('#divLoading').hide();
            },
            error: function (x, s, e) {
                $('#divLoading').hide();
                custom_shownotification("error", message.Error);
            },
        });
    }

}

//load datacontact lên modal update
function updatecontact_LoadUpdate(data) {
    $('#UpdateContacts #txtContactName').val(data.ContactName);
    $('#UpdateContacts #txtPhone').val(data.Mobile);
    $('#UpdateContacts #txtEmail').val(data.Email);
    $('#UpdateContacts #txtNote').val(data.Note);
    if (data.Active === 'True'|data.Active===true) {
        $('#ucheckContactActive').prop('checked', true);
    }
}

//kiểm tra contact trước khi update
function updatecontact_CheckNull(message) {
    if ($('#txtContactName').val() == null || $('#txtContactName').val() == '') {
        $('.alert-danger span', $('#UpdateContacts')).text(message.NameNull);
        $('.alert-danger', $('#UpdateContacts')).show();
        topUpdateFunction();
        return false;
    }
    if (($('#modalCreateContact #txtEmail').val() == null || $('#UpdateContacts #txtEmail').val() == '') && ($('#txtPhone').val() == '' || $('#txtPhone').val() == null)) {
        $('.alert-danger span', $('#UpdateContacts')).text(message.MobileOrEmailNull);
        $('.alert-danger', $('#UpdateContacts')).show();
        topUpdateFunction();
        return false;
    }
    if ($('#modalCreateContact #txtEmail').val() && !IsEmail($('#UpdateContacts #txtEmail').val())) {
        $('.alert-danger span', $('#UpdateContacts')).text(message.EmailErrorFormat);
        $('.alert-danger', $('#UpdateContacts')).show();
        topUpdateFunction();
        return false;
    }
    if ($('#txtPhone').val() && !IsMobile($('#txtPhone').val().trim())) {
        $('.alert-danger span', $('#UpdateContacts')).text(message.MobileErrorFormat);
        $('.alert-danger', $('#UpdateContacts')).show();
        topUpdateFunction();
        return false;
    }
    else {
        return true;
    }

}

function topUpdateFunction() {
    var myDiv = document.getElementById('content-update-field');
    myDiv.scrollTop = 0;
}
//dùng tạm mua ấn phẩm, lick vào cập nhập tình trạng đã thanh toán
//function updatePaid(apiDomain, ContactGroupId, paid)
//{
//    var Email = $('#UpdateContacts #txtEmail').val();
//    var ContactGroupId = ContactGroupId;
//    var CustomFieldId = '43';
//    var CustomFieldValue = paid;
//    $.ajax({
//        url: apiDomain + 'api/Contact/UpdateContactCustomField?ContactGroupId='+ContactGroupId+'&Email='+Email+'&CustomFieldId='+CustomFieldId+'&CustomFieldValue='+CustomFieldValue,
//        type: 'POST',
//        contentType: 'application/x-www-form-urlencoded',
//        success: function (r) {
//            var errorcode = parseInt(JSON.parse(JSON.stringify(r.ErrorCode)));
//            switch (errorcode) {
//                case 0:
//                    $('#tableContact').dataTable().fnDraw();
//                    break;
//                case 105:
//                    $('.alert-danger span', $('#UpdateContacts')).text(message.EmailError);
//                    $('.alert-danger', $('#UpdateContacts')).show();
//                    break;
//            }
//        },
//        error: function (x, s, e) {
//            custom_shownotification("error", message.Error);
//        },
//    });
//}
