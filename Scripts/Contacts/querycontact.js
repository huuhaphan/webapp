﻿function querycontact_Action(apiDomain, access_token, message)
{
    querycontact_DetectAction(apiDomain, access_token, message);
    querycontact_ChangeAction(apiDomain, access_token, message);
    querycontact_ChangeNumber(apiDomain, access_token, message);
    querycontact_ChangeDate(apiDomain, access_token, message);
    querycontact_Export(apiDomain, access_token, message);
    querycontact_OpenModalCreateGroup(apiDomain, access_token, message);
    querycontact_CreateGroup(apiDomain, access_token, message);
    querycontact_ViewInfoContact(message);
}

var list_query_export = [];
function querycontact_Open(apiDomain, access_token, message)
{
    var data = {
        MinOpen: parseInt($('#querycontact-number').val()),
        StartDate: $('#select-date-from').datepicker('getUTCDate'),
        EndDate: $('#select-date-to').datepicker('getUTCDate'),
    }
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/report/GetMailHasBeenOpened',
        type: 'post',
        data:JSON.stringify( data),
        contentType: 'application/json; charset=utf-8',
        beforeSend:function(request)
        {
            request.setRequestHeader('Authorization','Bearer '+access_token );
        },
        success:function(r)
        {
            $('#tbody-querycontact').children().remove();
            switch(parseInt(r.ErrorCode))
            {
                case 0:
                    $('#divLoading').hide();
                    if (r.Data.length > 0)
                    {
                        var html = '';
                        $('#total-query-contact').text(r.Data.length);
                        list_query_export = r.Data;
                        $.each(r.Data, function (i, o) {
                            var char = o.Email.charAt(0);
                            var randomColor = Math.floor(Math.random() * 16777215).toString(16);
                            var isOk = /^#[0-9A-F]{6}$/i.test('#' + randomColor);
                            if (isOk == true) {
                                var Email = '<div class="email-width-bkg"><span class="imgletter" style="text-transform: uppercase; ;background-color:#' + randomColor + ';margin-right:10px;">' + char + '</span><span style="font-weight:500;font-size:15px;">' + o.Email + '</span></div>';
                            }
                            else {

                                var Email = '<div class="email-width-bkg"><span class="imgletter" style="text-transform: uppercase; ;background-color:#7d44c6;margin-right:10px;">' + char + '</span><span style="font-weight:500;font-size:15px;">' + o.Email + '</span></div>';

                            }
                            
                            html += '<tr>' +
                               ' <td style="width:50%;"> ' + Email + ' </td>' +
                                '<td style="width:10%;"> ' + o.Time + ' </td>' +
                                '<td style="width:40%;"><a cid="' + o.ContactId + '" id="view-info-contact"  class="pull-right"> Xem Chi tiết </a></td>' +
                            '</tr>';
                            
                        });
                       
                    }
                    else {
                        html += '<tr style="text-align:center;">' +
                                '<td colspan="3">' + message.NoData + '</td>' +

                            '</tr>';
                        $('#total-query-contact').text('0');
                    }
                    var height = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.portlet-title').outerHeight() - $('#divAction-toolbar').outerHeight() - $('.page-footer').outerHeight()-100;
                    $('.inner_table').css({
                        'height': height-65,
                    })
                    $('#tbody-querycontact').append(html);
                    break;
                default:
                    custom_shownotification("error", message.HaveError);
                    $('#divLoading').hide();
                    break;
            }
        },
        error:function(x,s,e)
        {
            $('#divLoading').hide();
            custom_shownotification("error", message.HaveError);
        }

    })
}


function querycontact_ViewInfoContact(message)
{
    //lick info trên lưới
    $('#tbody-querycontact').on('click', '#view-info-contact', function () {
      

        if (parseInt($(this).attr('cid')) == 0 || isNaN($(this).attr('cid')) || $(this).attr('cid')=="") {
            custom_shownotification("error", message.NoFindContact);
        } else {
            $(this).attr("data-toggle", "modal");
            $(this).attr("data-target", "#modalInfoContact");
            $(this).attr("href", "/Contacts/ContactInfo?cId=" + parseInt($(this).attr('cid')));
        }
    });
}


function quercontact_Click(apiDomain, access_token, message) {

    var data = {
        MinClick:parseInt( $('#querycontact-number').val()),
        StartDate: $('#select-date-from').datepicker('getUTCDate'),
        EndDate: $('#select-date-to').datepicker('getUTCDate'),
    }
    $('#divLoading').show();
    $.ajax({
        url: apiDomain + 'api/report/GetMailHasBeenClicked',
        type: 'post',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', 'Bearer ' + access_token);
        },
        success: function (r) {
            $('#tbody-querycontact').children().remove();
            switch (parseInt(r.ErrorCode)) {
                case 0:
                    $('#divLoading').hide();
                    if (r.Data.length > 0) {
                        list_query_export = r.Data;
                        $('#total-query-contact').text(r.Data.length);
                        var html = '';
                        $.each(r.Data, function (i, o) {
                            var char = o.Email.charAt(0);
                            var randomColor = Math.floor(Math.random() * 16777215).toString(16);
                            var isOk = /^#[0-9A-F]{6}$/i.test('#' + randomColor);
                            if (isOk == true) {
                            var Email='<div class="email-width-bkg"><span class="imgletter" style="text-transform: uppercase; ;background-color:#' + randomColor + ';margin-right:10px;">' + char + '</span><span style="font-weight:500;font-size:15px;">' + o.Email + '</span></div>';
                            }
                            else {

                                var Email = '<div class="email-width-bkg"><span class="imgletter" style="text-transform: uppercase; ;background-color:#7d44c6;margin-right:10px;">' + char + '</span><span style="font-weight:500;font-size:15px;">' + o.Email + '</span></div>';

                            }
                            
                            html += '<tr>' +
                                '<td style="width:50%;"> ' + Email + ' </td>' +
                                '<td style="width:10%;"> ' + o.Time + ' </td>' +
                                '<td style="width:40%;" ><a cid="' + o.ContactId + '" id="view-info-contact" class="pull-right"> Xem Chi tiết </a></td>' +
                            '</tr>';
                        });
                       
                    }
                    else
                    {
                        html += '<tr style="text-align:center;">' +
                                '<td colspan="3">' + message.NoData + '</td>' +
                               
                            '</tr>';
                        $('#total-query-contact').text('0');
                    }
                    var height = $(window).outerHeight() - $('.page-header').outerHeight() - $('.page-bar').outerHeight() - $('.portlet-title').outerHeight() - $('#divAction-toolbar').outerHeight() - $('.page-footer').outerHeight()-100;
                    $('.inner_table').css({
                        'height': height-65,
                    })
                   
                    $('#tbody-querycontact').append(html);
                    break;
                default:
                    custom_shownotification("error", message.HaveError);
                    $('#divLoading').hide();
                    break;
            }
        },
        error: function (x, s, e) {
            $('#divLoading').hide();
            custom_shownotification("error", message.HaveError);
        }

    })
}

var state_activity = 0;
function querycontact_ChangeAction(apiDomain, access_token, message)
{
    //$('#querycontact-action').on('switchChange.bootstrapSwitch', function (event, state) {
    //    var state = $('#querycontact-action').bootstrapSwitch('state');
    //    querycontact_DetectAction(apiDomain, access_token, message);
    //});
    $('#tab-open-activity').on('click', function () {
        state_activity = 0;
        querycontact_DetectAction(apiDomain, access_token, message);
    });
    $('#tab-click-activity').on('click', function () {
        state_activity = 1;
        querycontact_DetectAction(apiDomain, access_token, message);
    });

    $('#tab-total-activity').on('click', function () {
        state_activity = 2;
        querycontact_DetectAction(apiDomain, access_token, message);
    })
}
function querycontact_ChangeNumber(apiDomain, access_token, message) {
    $('#querycontact-number').change( function () {
        querycontact_DetectAction(apiDomain, access_token, message);
    });
}
function querycontact_ChangeDate(apiDomain, access_token, message)
{
    $('#select-date-query').on('click', function () {
        querycontact_DetectAction(apiDomain, access_token, message);
    })
}

function querycontact_DetectAction(apiDomain, access_token, message) {

    if (state_activity == 1) //click
    {
        $('#select-num-activity').show();
        quercontact_Click(apiDomain, access_token, message);
    }
    if (state_activity == 0) { //open
        $('#select-num-activity').show();
        querycontact_Open(apiDomain, access_token, message);
    }
    if (state_activity == 2) { //total
        $('#select-num-activity').hide();
    }
}

function querycontact_Export(apiDomain, access_token, message)
{
    $('#export-contact-query').on('click', function () {
        $('#divLoading').show();
        var list = [];
        var listid = [];
        var head = ["ContactName", "FirstName", "LastName", "Email", "Mobile", "BirthDate", "Gender"];
        list.push(head);
        $.each(list_query_export, function (i,o) {
            listid.push(o.ContactId);
            
        })
        var data={
            ContactIds:listid,
        };
        $.ajax({
            url: apiDomain + 'api/Contact/GetContactListFromIds',
            type: 'post',
            contentType: 'application/json; charset=utf-8',
            data:JSON.stringify(data),
            beforeSend: function (request) {
                request.setRequestHeader('Authorization', 'Bearer ' + access_token);
            },
            success: function (r) {
                switch (parseInt(r.ErrorCode)) {
                    case 0:
                        if (r.Data.length) {
                            console.log(r.Data);
                            $.each(r.Data, function (i, o) {
                                var obj = [o.ContactName, o.FirstName, o.LastName,o.Email,o.Mobile,o.BirthDate,o.Gender];

                                list.push(obj);
                                console.log(list);
                            });
                            write_exportContact(list);
                        }
                       
                        break;
                    default:
                        custom_shownotification("error", message.HaveError);
                        break;
                }
                $('#divLoading').hide();
            }, error: function (x, s, e) {
                custom_shownotification("error", message.HaveError);
                $('#divLoading').hide();
            }
        });
        

    })
}

function querycontact_OpenModalCreateGroup(apiDomain, access_token, message)
{
    $('#create-group-query').on('click', function () {
        $('#modal-create-group-from-query').modal('toggle');
    });
}
function querycontact_CreateGroup(apiDomain, access_token, message) {
    $('#create-contact-group-query').on('click', function () {

        if (querycontact_CheckGroup(message)) {
            $('#modal-create-group-from-query').modal('toggle');
            var active = true;
            if ($('#checkGroupActive').prop('checked')) {
                active = true;
            } else {
                active = false;
            }
            var listid=[];
            $.each(list_query_export, function (i,o) {
                listid.push(o.ContactId);
            
            })
            var data = {
                Active: active,
                ContactGroupName: $('#contactgroupname').val(),
                Description: $('#contactgroupdesciption').val(),
                ContactIds: listid,
            };
            $('#divLoading').show();
            $.ajax({
                url: apiDomain + 'api/ContactGroup/CreateFromContactIds',
                type: 'POST',
                data: JSON.stringify( data),
                contentType: 'application/json; charset=utf-8',//'application/x-www-form-urlencoded',
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + access_token);
                },
                success: function (r) {
                    var errorcode = parseInt(r.ErrorCode);
                    $('#divLoading').hide();
                    switch (errorcode) {
                        case 0:
                            //var ContactGroupId = r.Data.ContactGroupId;
                            // querycontact_CopyContactToGroup(apiDomain, access_token, ContactGroupId,message)
                            custom_shownotification("success", message.notifySuccessCreate);
                            $('#divLoading').hide();
                            break;
                        default:
                            custom_shownotification("error", message.HaveError);
                            break;
                    }
                },
                error: function (x, s, e) {
                    $('#divLoading').hide();
                    custom_shownotification("error", message.HaveError);
                },
            })
        }
    })
}

//function querycontact_CopyContactToGroup(apiDomain, access_token, ContactGroupId, message)
//{
//    var size = 200;
//    var newList = [];
//    var loop = list_query_export.length / size;

//    for (var i = 0; i < Math.round(loop + 1) ; i++) {    //chia thành nhiều lần gửi

//        newList = list_query_export.slice(i * size, i * size + size);
//        var data = {
//            ContactGroupId: ContactGroupId,
//            ContactList: newList,
//            Note: '',
//        };
//        requestimport++;
//        // action_Import(apiDomain, access_token, data, i);

//        $.ajax({
//            url: apiDomain + 'api/Contact/Import',
//            type: 'POST',
//            contentType: 'application/x-www-form-urlencoded',
//            data: data,
//            beforeSend: function (request) {
//                request.setRequestHeader("Authorization", "Bearer " + access_token);
//            },
//            success: function (r) {
//                requestimport--;
//                if (requestimport == 0) {
//                    custom_shownotification("success", message.notifySuccessCreate);
//                    $('#divLoading').hide();

//                }
//            },
//            error: function (x, s, e) {
//                requestimport--;
//                if (requestimport == 0) {
//                    custom_shownotification("success", message.notifySuccessCreate);
//                    $('#divLoading').hide();

//                }
//            }

//        });


//    }
//}

function querycontact_CheckGroup(message)
{
    if ($('#contactgroupname').val() == null | $('#contactgroupname').val() == "") {
        custom_shownotification("error", message.ContactGroupRequire);
        return false;
    }
    else {
        return true;
    }
}