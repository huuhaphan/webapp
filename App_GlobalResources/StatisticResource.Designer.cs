//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "14.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class StatisticResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal StatisticResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.StatisticResource", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Autoresponders.
        /// </summary>
        internal static string Autoresponders {
            get {
                return ResourceManager.GetString("Autoresponders", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tỉ lệ trả về.
        /// </summary>
        internal static string bouncerate {
            get {
                return ResourceManager.GetString("bouncerate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chiến dịch.
        /// </summary>
        internal static string Campaign {
            get {
                return ResourceManager.GetString("Campaign", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tỉ lệ click.
        /// </summary>
        internal static string clickrate {
            get {
                return ResourceManager.GetString("clickrate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tỉ lệ khiếu nại.
        /// </summary>
        internal static string complaintrate {
            get {
                return ResourceManager.GetString("complaintrate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Danh sách liên hệ.
        /// </summary>
        internal static string ContactList {
            get {
                return ResourceManager.GetString("ContactList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ngày tạo.
        /// </summary>
        internal static string DateExport {
            get {
                return ResourceManager.GetString("DateExport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn không có bắt kì email nào bị trả về..
        /// </summary>
        internal static string DontHaveBounce {
            get {
                return ResourceManager.GetString("DontHaveBounce", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chưa có chiến dịch nào để thống kê.
        /// </summary>
        internal static string DontHaveCampaignToReport {
            get {
                return ResourceManager.GetString("DontHaveCampaignToReport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn không có bất kì click nào trong email..
        /// </summary>
        internal static string DontHaveClick {
            get {
                return ResourceManager.GetString("DontHaveClick", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn không có bất kì báo cáo spam nào trong email..
        /// </summary>
        internal static string DontHaveComplaint {
            get {
                return ResourceManager.GetString("DontHaveComplaint", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn chưa có bất kì email nào được mở..
        /// </summary>
        internal static string DontHaveOpen {
            get {
                return ResourceManager.GetString("DontHaveOpen", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn chưa có email nào được gửi ..
        /// </summary>
        internal static string DontHaveSent {
            get {
                return ResourceManager.GetString("DontHaveSent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chưa có dữ liệu gửi tin nhắn.
        /// </summary>
        internal static string DontHaveSMS {
            get {
                return ResourceManager.GetString("DontHaveSMS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn không có bất kì lượt hủy đăng kí nào trong email..
        /// </summary>
        internal static string DontHaveUnsub {
            get {
                return ResourceManager.GetString("DontHaveUnsub", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tải về.
        /// </summary>
        internal static string Download {
            get {
                return ResourceManager.GetString("Download", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Các email trong chiến dịch.
        /// </summary>
        internal static string Email {
            get {
                return ResourceManager.GetString("Email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gửi không thành công.
        /// </summary>
        internal static string Fail {
            get {
                return ResourceManager.GetString("Fail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tên file.
        /// </summary>
        internal static string FileName {
            get {
                return ResourceManager.GetString("FileName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lấy thông tin không thành công.
        /// </summary>
        internal static string GetDataError {
            get {
                return ResourceManager.GetString("GetDataError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chiến dịch SMS.
        /// </summary>
        internal static string MessageCampaign {
            get {
                return ResourceManager.GetString("MessageCampaign", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chiến dịch chưa đặt tên.
        /// </summary>
        internal static string NoCampaignName {
            get {
                return ResourceManager.GetString("NoCampaignName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chưa có số điện thoại nào.
        /// </summary>
        internal static string NoMobile {
            get {
                return ResourceManager.GetString("NoMobile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chưa có dữ liệu.
        /// </summary>
        internal static string NotData {
            get {
                return ResourceManager.GetString("NotData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chưa có autoresponder nào được chọn.
        /// </summary>
        internal static string NotSelect {
            get {
                return ResourceManager.GetString("NotSelect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Số lượng.
        /// </summary>
        internal static string Number {
            get {
                return ResourceManager.GetString("Number", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chọn.
        /// </summary>
        internal static string OK {
            get {
                return ResourceManager.GetString("OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tỉ lệ mở.
        /// </summary>
        internal static string openrate {
            get {
                return ResourceManager.GetString("openrate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đang chờ.
        /// </summary>
        internal static string Pending {
            get {
                return ResourceManager.GetString("Pending", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chọn tất cả.
        /// </summary>
        internal static string SelectAll {
            get {
                return ResourceManager.GetString("SelectAll", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đã chọn tất cả.
        /// </summary>
        internal static string SelectedAll {
            get {
                return ResourceManager.GetString("SelectedAll", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đã gửi.
        /// </summary>
        internal static string Sent {
            get {
                return ResourceManager.GetString("Sent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Không thành công.
        /// </summary>
        internal static string SMSFail {
            get {
                return ResourceManager.GetString("SMSFail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đang chờ.
        /// </summary>
        internal static string SMSPending {
            get {
                return ResourceManager.GetString("SMSPending", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đã gửi.
        /// </summary>
        internal static string SMSSent {
            get {
                return ResourceManager.GetString("SMSSent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thống kê email.
        /// </summary>
        internal static string StatisticEmail {
            get {
                return ResourceManager.GetString("StatisticEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thống kê tin nhắn.
        /// </summary>
        internal static string StatisticSMS {
            get {
                return ResourceManager.GetString("StatisticSMS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bị trả về.
        /// </summary>
        internal static string tab_bounced {
            get {
                return ResourceManager.GetString("tab_bounced", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đã nhấp link.
        /// </summary>
        internal static string tab_click {
            get {
                return ResourceManager.GetString("tab_click", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Khiếu nại.
        /// </summary>
        internal static string tab_complaints {
            get {
                return ResourceManager.GetString("tab_complaints", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mở email.
        /// </summary>
        internal static string tab_open {
            get {
                return ResourceManager.GetString("tab_open", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tổng.
        /// </summary>
        internal static string tab_total {
            get {
                return ResourceManager.GetString("tab_total", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ngừng nhận email.
        /// </summary>
        internal static string tab_unsubcribed {
            get {
                return ResourceManager.GetString("tab_unsubcribed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thông kê chiến dịch gửi rocket.
        /// </summary>
        internal static string titlegeneral {
            get {
                return ResourceManager.GetString("titlegeneral", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thống kê chiến dịch gửi sms.
        /// </summary>
        internal static string titlemessage {
            get {
                return ResourceManager.GetString("titlemessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to đến.
        /// </summary>
        internal static string to {
            get {
                return ResourceManager.GetString("to", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tộng cộng:.
        /// </summary>
        internal static string Total {
            get {
                return ResourceManager.GetString("Total", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Liên hệ active:.
        /// </summary>
        internal static string TotalContactActive {
            get {
                return ResourceManager.GetString("TotalContactActive", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tổng đã gửi:.
        /// </summary>
        internal static string TotalHasSend {
            get {
                return ResourceManager.GetString("TotalHasSend", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đã nhận.
        /// </summary>
        internal static string Totalsent {
            get {
                return ResourceManager.GetString("Totalsent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tổng trả về.
        /// </summary>
        internal static string TotatBounce {
            get {
                return ResourceManager.GetString("TotatBounce", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kiểu.
        /// </summary>
        internal static string Type {
            get {
                return ResourceManager.GetString("Type", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tỉ lệ hủy đăng kí.
        /// </summary>
        internal static string unsubscriberate {
            get {
                return ResourceManager.GetString("unsubscriberate", resourceCulture);
            }
        }
    }
}
