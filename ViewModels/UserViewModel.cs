﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace _123SendMailV2.ViewModels
{
    public class UserViewModel
    {
        public long UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobilePhone { get; set; }
        public string Company { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int CurrentBalance { get; set; }
        public UserMonthlyPlanModel CurrentMonthlyPlan { get; set; }
        public int LevelId { get; set; }
        public bool Active { get; set; }
        public bool Banned { get; set; }
        public string Avatar { get; set; }
        public string AvatarThumb { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public DateTime? ExpireDate { get; set; }
        public List<Function> FunctionList { get; set; }

        public int? ParentId { get; set; }
    }

    public class UserMonthlyPlanModel
    {
        public long UserId { get; set; }
        public int PlanNum { get; set; }
        public int MonthlyPlanId { get; set; }
        public string MonthlyPlanName { get; set; }
        public DateTime StarDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Quota { get; set; }
        public int Sents { get; set; }
        public bool IsUpgrade { get; set; }

        public DateTime CreateDate { get; set; }
    }

    [Serializable()]
    public class Permission
    {
        [DataMember]
        public bool IsParent { get; set; }
        [DataMember]
        public int Contacts { get; set; }
        [DataMember]
        public int ContactActivity { get; set; }
        [DataMember]
        public int Suppression { get; set; }
        [DataMember]
        public int SMSCampaign { get; set; }
        [DataMember]
        public int RocketCampaign { get; set; }
        [DataMember]
        public int AutoCampaign { get; set; }
        [DataMember]
        public int Statistic { get; set; }
        [DataMember]
        public int Template { get; set; }
        [DataMember]
        public int Exports { get; set; }
        [DataMember]
        public int Account { get; set; }

        [DataMember]
        public int ManageGroup { get; set; }
        [DataMember]
        public int ManageContact { get; set; }
        [DataMember]
        public int ManageForm { get; set; }
        [DataMember]
        public int ManageCustomField { get; set; }
        [DataMember]
        public int SettingAccount { get; set; }
        [DataMember]
        public int SubAccount { get; set; }
        [DataMember]
        public int FromMail { get; set; }
        [DataMember]
        public int FromMobile { get; set; }
        [DataMember]
        public int StatisticMoney { get; set; }

        [DataMember]
        public int RechargeMoney { get; set; }

        [DataMember]
        public int SpeicalDay { get; set; }

        [DataMember]
        public int SMSUnSendLogs { get; set; }

    }
}