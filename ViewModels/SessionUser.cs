﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace _123SendMailV2.ViewModels
{
    [Serializable()]
    public class SessionUser
    {
        [DataMember]
        public string Access_Token { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]

        public string UserName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string Street { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string Company { get; set; }
        [DataMember]
        public string Facebook { get; set; }
        [DataMember]
        public string Twitter { get; set; }
        [DataMember]
        public string UserId { get; set; }
        [DataMember]
        public string CurrentBalance { get; set; }
        [DataMember]
        public DateTime CreateDate { get; set; }
        [DataMember]
        public DateTime EndDate { get; set; }
        [DataMember]
        public string MonthlyPlanName { get; set; }
        [DataMember]
        public string Quota { get; set; }
        [DataMember]
        public DateTime StarDate { get; set; }
        [DataMember]
        public bool EnableSMS { get; set; }

        [DataMember]
        public DateTime? ExpireDate { get; set; }

        [DataMember]
        public int? ParentId { get; set; }

        [DataMember]
        public List<Function> FunctionList { get; set; }

    }

    [Serializable()]
    public class Function
    {
        [DataMember]
        public int AccessLevel { get; set;}

        [DataMember]
        public DateTime CreateDate { get; set; }

        [DataMember]
        public int FunctionId { get; set; }

        [DataMember]
        public int UserId { get; set; }
    }
}