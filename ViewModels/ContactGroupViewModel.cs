﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _123SendMailV2.ViewModels
{
        public class ContactGroupViewModel
        {
            public long ContactGroupId { get; set; }
            public string ContactGroupName { get; set; }
            public bool Active { get; set; }
            public string Desciption { get; set; }

        }    
    }
