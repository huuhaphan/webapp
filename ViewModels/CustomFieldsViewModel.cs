﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _123SendMailV2.ViewModels
{
    public class CustomFieldsViewModel
    {
        public long FieldId { get; set; }
        public long? UserId { get; set; }
        public bool? IsPredefined{get;set;}
        public int? FieldTypeId { get; set; }
        public int? FieldFormatId { get; set; }
        public string FieldName { get; set; }
        public string FieldFormatName { get; set; }
        public bool? IsText { get; set; }
        public bool? IsMultiple { get; set; }
        public bool? IsSpecial { get; set; }
        public string FieldTypeName { get; set; }
        public string FieldTypeDesciption { get; set; }
        public string IconUrl { get; set; }

        public IQueryable<fieldvalue> fieldvalue;
        
    }

    public class fieldvalue
    {
        public string FieldValue { get; set; }
        public bool? FieldSelected { get; set; }
        public int? SortId { get; set; } 
    }
}