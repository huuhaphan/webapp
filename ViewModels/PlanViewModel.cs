﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _123SendMailV2.ViewModels
{
    public class PlanViewModel
    {
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        public int Quota { get; set; }
        public int Price { get; set; }
        public bool IsAutoResponder { get; set; }
        public int CampaignNumber { get; set; }
        public bool Active { get; set; }
        public bool IsFree { get; set; }
    }

    public class UserPlanViewModel
    {
        public long UserId { get; set; }
        public int PlanNum { get; set; }
        public int MonthlyPlanId { get; set; }
        public string MonthlyPlanName { get; set; }
        public DateTime StarDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Quota { get; set; }
        public int Sents { get; set; }
        public bool IsUpgrade { get; set; }
        public DateTime CreateDate { get; set; }
    }
}