﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _123SendMailV2.ViewModels
{
    public class ContactViewModel
    {
        public long UserId { get; set; }
        public long ContactID { get; set; }
        public int? ContactGroupID { get; set; }
        public string Email { get; set; }
        public string ContactName { get; set; }
        public string Mobile { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Note { get; set; }
        public bool Active { get; set; }
    }
}