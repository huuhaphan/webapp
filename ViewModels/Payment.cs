﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _123SendMailV2.ViewModels
{
    public class Payment
    {
        public string Bank { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Menthod { get; set; }
        public string Amount { get; set; }
        public string PayCode { get; set; }
        public string CancelUrl { get; set; }
        public string SuccessUrl { get; set; }
        public string PlanId { get; set; }
        public string AccessToken { get; set; }
    }

    public class PaymentCard
    {
        public string Cardtype { get; set; }
        public string Sericard { get; set; }
        public string Pincard { get; set; }
        public string PayCode { get; set; }
    }

    public class ResultPayment
    {
        public bool Success { get; set; }
        public string Result { get; set; }
    }

    public class ReturnResultPayment
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public Object Data { get; set; }
    }

   
}