﻿using _123SendMailV2.Utitlies;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace _123SendMailV2.DAL
{
    public class DALPayment
    {
        public static JsonResult UpdateData(string PayCode, string TrackingId, string Note, string Token)
        {
            JsonResult result = new JsonResult();
            //string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
            //string Method = "api/User/UpdatePayment";
            //var client = new RestClient(domain);
            //var request = new RestRequest(Method, RestSharp.Method.POST);
            //var access_token = Token;
            //request.AddParameter("Authorization",string.Format("Bearer " + access_token),
            //ParameterType.HttpHeader);
            //request.AddHeader("Accept", "application/x-www-form-urlencoded");
            //request.Parameters.Clear();

            //request.AddParameter("PayCode", PayCode, ParameterType.QueryString);
            //request.AddParameter("TrackingId", TrackingId, ParameterType.QueryString);
            //request.AddParameter("Note", Note, ParameterType.QueryString);


            //IRestResponse response = client.Execute(request);
            HttpClient client = new HttpClient();
            string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
            client.BaseAddress = new Uri(domain);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            var access_token = Token;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + access_token);
            var values = new Dictionary<string, string>();
            values.Add("PayCode", PayCode);
            values.Add("TrackingId", TrackingId);
            values.Add("Note", Note);
            var content = new FormUrlEncodedContent(values);

            // New code:
            dynamic httpResponseMessage = client.PostAsync("api/User/UpdatePayment?PayCode=" + PayCode + "&TrackingId=" + TrackingId + "&Note=" + Note, content).Result;
            var response = httpResponseMessage.Content.ReadAsStringAsync();
            if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
            {

                result = JsonConvert.DeserializeObject<JsonResult>(response.Result);
                switch (result.ErrorCode)
                {
                    case "104":
                        break;
                    case "0":
                        result.ErrorMessage = result.ErrorMessage;
                        try
                        {
                            Task.Run(async () => await AddListPaid());
                            Task.Run(async () => await CheckContactExist()); //kiểm tra tồn  tại rồi xóa
                        }
                        catch (Exception ex)
                        {

                        }
                        break;
                }


            }

            return result;
        }


        public async Task<JsonResultUserPlan> CreateUpdateData(string PlanId, string Token)
        {
            using (var client = new HttpClient())
            {
                string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
                client.BaseAddress = new Uri(domain);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                var access_token = Token;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + access_token);
                var values = new Dictionary<string, string>();
                values.Add("PlanId", PlanId);
                var content = new FormUrlEncodedContent(values);

                // New code:
                var httpResponseMessage = await client.PostAsync("api/User/CreateUserMontlyPlan?PlanId=" + PlanId, content);
                var returnValue = httpResponseMessage.Content.ReadAsStringAsync().Result;
                JsonResultUserPlan deserialized = JsonConvert.DeserializeObject<JsonResultUserPlan>(returnValue);
                JsonResultUserPlan jsonresult = new JsonResultUserPlan();
                jsonresult.ErrorCode = deserialized.ErrorCode;
                jsonresult.Data = deserialized.Data;
                jsonresult.ErrorMessage = deserialized.ErrorMessage;
                return jsonresult;
            }
        }

        public async Task<JsonResultPlan> IsUpgrade(string pId, string Token)
        {
            using (var client = new HttpClient())
            {
                string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
                client.BaseAddress = new Uri(domain);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                var access_token = Token;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + access_token);
                //var values = new Dictionary<string, string>();
                //values.Add("PlanId", pId);
                //var content = new FormUrlEncodedContent(values);

                // New code:
                var httpResponseMessage = await client.GetAsync("api/User/GetMontlyPlanInfo?PlanId=" + pId);
                var returnValue = httpResponseMessage.Content.ReadAsStringAsync().Result;
                JsonResultPlan deserialized = JsonConvert.DeserializeObject<JsonResultPlan>(returnValue);

                JsonResultPlan jsonresult = new JsonResultPlan();
                jsonresult.ErrorCode = deserialized.ErrorCode;
                jsonresult.Data = deserialized.Data;
                jsonresult.ErrorMessage = deserialized.ErrorMessage;


                return jsonresult;
            }
        }
        public async Task<JsonResult> SendOrderSuccessNotify(string PayCode)
        {
            using (var client = new HttpClient())
            {
                string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
                client.BaseAddress = new Uri(domain);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + SesstionManager.access_token);
                var content = new FormUrlEncodedContent(new[]{
                    new KeyValuePair<string, string>("PayCode", PayCode)
                });
                var httpResponseMessage = await client.PostAsync("api/User/SendOrderSuccessNotify?PayCode=" + PayCode, content);
                var returnValue = httpResponseMessage.Content.ReadAsStringAsync().Result;
                JsonResult deserialized = JsonConvert.DeserializeObject<JsonResult>(returnValue);

                JsonResult jsonresult = new JsonResult();
                jsonresult.ErrorCode = deserialized.ErrorCode;
                jsonresult.Data = deserialized.Data;
                jsonresult.ErrorMessage = deserialized.ErrorMessage;
                return jsonresult;
            }
        }

        /// <summary>
        /// hack  thêm vào danh sách đã trả xèng
        /// </summary>
        /// <returns></returns>
        public static async Task<JsonResult> AddListPaid()
        {
            using (var client = new HttpClient())
            {
                string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
                client.BaseAddress = new Uri(domain);
                client.DefaultRequestHeaders.Accept.Clear();
                string AccessToken = LoginSupportAccount();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + AccessToken);
                var ContactGroupId = System.Configuration.ConfigurationManager.AppSettings["ContactGroupPaid"];
                var CampaignId = System.Configuration.ConfigurationManager.AppSettings["CampaignAutoPaid"];
                var content = new FormUrlEncodedContent(new[]{
                    new KeyValuePair<string, string>("ContactGroupId", ContactGroupId),
                    new KeyValuePair<string, string>("CampaignId", CampaignId),
                    new KeyValuePair<string, string>("ContactName", SesstionManager.UserInfoPayment.UserName),
                    new KeyValuePair<string, string>("Email", SesstionManager.UserInfoPayment.Email),
                    new KeyValuePair<string, string>("Mobile", SesstionManager.UserInfoPayment.Mobile),
                    new KeyValuePair<string, string>("Active", "true"),

                });
                var httpResponseMessage = await client.PostAsync("api/Contact/Create", content);
                var returnValue = httpResponseMessage.Content.ReadAsStringAsync().Result;
                JsonResult deserialized = JsonConvert.DeserializeObject<JsonResult>(returnValue);

                JsonResult jsonresult = new JsonResult();
                jsonresult.ErrorCode = deserialized.ErrorCode;
                jsonresult.Data = deserialized.Data;
                jsonresult.ErrorMessage = deserialized.ErrorMessage;
                return jsonresult;
            }
        }

        public static async Task<JsonResultFee> CheckContactExist()
        {
            using (var client = new HttpClient())
            {
                string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
                client.BaseAddress = new Uri(domain);
                client.DefaultRequestHeaders.Accept.Clear();
                string AccessToken = LoginSupportAccount();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + AccessToken);
                var ContactGroupId = System.Configuration.ConfigurationManager.AppSettings["ContactGroup"];
                var CampaignId = System.Configuration.ConfigurationManager.AppSettings["CampaignAuto"];
                var content = new FormUrlEncodedContent(new[]{
                    new KeyValuePair<string, string>("ContactGroupId", ContactGroupId),
                    new KeyValuePair<string, string>("Email", SesstionManager.UserInfoPayment.Email),
                    new KeyValuePair<string, string>("Mobile", SesstionManager.UserInfoPayment.Mobile),

                });
                var httpResponseMessage = await client.PostAsync("api/Contact/CheckContactExist", content);
                var returnValue = httpResponseMessage.Content.ReadAsStringAsync().Result;
                JsonResultFee deserialized = JsonConvert.DeserializeObject<JsonResultFee>(returnValue);

                JsonResultFee jsonresult = new JsonResultFee();
                jsonresult.ErrorCode = deserialized.ErrorCode;
                switch (Convert.ToInt32(jsonresult.ErrorCode))
                {
                    case 0:
                        jsonresult.Data = deserialized.Data;
                        await DeleteContact(Convert.ToInt64(jsonresult.Data));
                        break;
                }
                jsonresult.ErrorMessage = deserialized.ErrorMessage;
                return jsonresult;
            }
        }

        public static async Task<JsonResult> DeleteContact(long ContactId)
        {
            using (var client = new HttpClient())
            {
                string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
                client.BaseAddress = new Uri(domain);
                client.DefaultRequestHeaders.Accept.Clear();
                string AccessToken = LoginSupportAccount();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + AccessToken);
                var httpResponseMessage = await client.GetAsync("api/Contact/Delete?ContactId=" + ContactId);
                var returnValue = httpResponseMessage.Content.ReadAsStringAsync().Result;
                JsonResult deserialized = JsonConvert.DeserializeObject<JsonResult>(returnValue);

                JsonResult jsonresult = new JsonResult();
                jsonresult.ErrorCode = deserialized.ErrorCode;
                jsonresult.Data = deserialized.Data;
                jsonresult.ErrorMessage = deserialized.ErrorMessage;
                return jsonresult;
            }
        }


        public static string LoginSupportAccount()
        {
            string result = "";
            string password = System.Configuration.ConfigurationManager.AppSettings["SupportPass"];
            string username = System.Configuration.ConfigurationManager.AppSettings["SupportUsr"];
            string grant_type = "password";

            string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
            string Method = "api/authtoken";
            var client = new RestClient(domain);
            var request = new RestRequest(Method, RestSharp.Method.POST);
            request.AddHeader("Accept", "application/x-www-form-urlencoded");
            request.Parameters.Clear();
            request.AddParameter("username", username, ParameterType.QueryString);
            request.AddParameter("password", password, ParameterType.QueryString);
            request.AddParameter("grant_type", grant_type, ParameterType.QueryString);


            IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<DAL.JsonToken>(response.Content).access_token;
            }
            return result;



        }

        public static JsonResult GetInfoUser()
        {
            using (var client = new HttpClient())
            {
                string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
                client.BaseAddress = new Uri(domain);
                client.DefaultRequestHeaders.Accept.Clear();
               
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + SesstionManager.access_payment);
                var httpResponseMessage =  client.GetAsync("api/User/Info").Result;
                var returnValue = httpResponseMessage.Content.ReadAsStringAsync().Result;
                JsonResult deserialized = JsonConvert.DeserializeObject<JsonResult>(returnValue);

                JsonResult jsonresult = new JsonResult();
                jsonresult.ErrorCode = deserialized.ErrorCode;
                jsonresult.Data = deserialized.Data;
                jsonresult.ErrorMessage = deserialized.ErrorMessage;
                return jsonresult;
            }
        }
    }


}