﻿using _123SendMailV2.Utitlies;
using _123SendMailV2.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace _123SendMailV2.DAL
{
    public class apiDAL
    {
        public  async Task<JsonResult> UpdateData(string PayCode, string TrackingId, string Note, string Token)
        {
            HttpClient client = new HttpClient();
            string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
            client.BaseAddress = new Uri(domain);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            var access_token = Token;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + access_token);
            var values = new Dictionary<string, string>();
            values.Add("PayCode", PayCode);
            values.Add("TrackingId", TrackingId);
            values.Add("Note", Note);
            var content = new FormUrlEncodedContent(values);

            // New code:
            dynamic httpResponseMessage = await client.PostAsync("api/User/UpdatePayment?PayCode=" + PayCode + "&TrackingId=" + TrackingId + "&Note=" + Note, content);
            var returnValue = httpResponseMessage.Content.ReadAsStringAsync().Result;
            JsonResult deserialized = JsonConvert.DeserializeObject<JsonResult>(returnValue);
            var ErrorCode = deserialized.ErrorCode;
            //var ErrorCode =

            //cập nhập sestion
            JsonResult jsonresult = new JsonResult();
            jsonresult.ErrorCode = ErrorCode;

            switch (ErrorCode)
            {
                case "104":
                    break;
                case "0":
                    var Data = deserialized.Data;
                    SessionUser user = new SessionUser();
                    user.Access_Token = access_token;
                    user.Avatar = Data.Avatar;
                    user.Country = Data.Country;
                    user.City = Data.City;
                    user.Company = Data.Company;
                    user.CurrentBalance = Data.CurrentBalance.ToString();
                    user.CreateDate = Data.CurrentMonthlyPlan.CreateDate;
                    user.EndDate = Data.CurrentMonthlyPlan.EndDate;
                    user.Email = Data.Email;
                    user.Facebook = Data.Facebook;
                    user.FirstName = Data.FirstName;
                    user.LastName = Data.LastName;
                    user.Mobile = Data.MobilePhone;
                    user.MonthlyPlanName = Data.CurrentMonthlyPlan.MonthlyPlanName;
                    user.Quota = Data.CurrentMonthlyPlan.Quota.ToString();
                    user.Street = Data.Street;
                    user.Twitter = Data.Twitter;
                    user.UserId = Data.UserId.ToString();
                    user.StarDate = Data.CurrentMonthlyPlan.StarDate;
                    user.ExpireDate = Data.ExpireDate;
                    user.FunctionList = Data.FunctionList;
                    user.ParentId = Data.ParentId;

                    SesstionManager.UserInfo = user;

                    jsonresult.Data = Data;
                    jsonresult.ErrorMessage = deserialized.ErrorMessage;
                    try
                    {
                        await AddListPaid();
                        await CheckContactExist(); //kiểm tra tồn  tại rồi xóa
                    }
                    catch (Exception ex)
                    {

                    }
                    break;
            }

            return jsonresult;

        }


        public  async Task<JsonResultUserPlan> CreateUpdateData(string PlanId, string Token)
        {
            using (var client = new HttpClient())
            {
                string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
                client.BaseAddress = new Uri(domain);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                var access_token = Token;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + access_token);
                var values = new Dictionary<string, string>();
                values.Add("PlanId", PlanId);
                var content = new FormUrlEncodedContent(values);

                // New code:
                var httpResponseMessage = await client.PostAsync("api/User/CreateUserMontlyPlan?PlanId=" + PlanId, content);
                var returnValue = httpResponseMessage.Content.ReadAsStringAsync().Result;
                JsonResultUserPlan deserialized = JsonConvert.DeserializeObject<JsonResultUserPlan>(returnValue);
                JsonResultUserPlan jsonresult = new JsonResultUserPlan();
                jsonresult.ErrorCode = deserialized.ErrorCode;
                jsonresult.Data = deserialized.Data;
                jsonresult.ErrorMessage = deserialized.ErrorMessage;
                return jsonresult;
            }
        }

        public  async Task<JsonResultPlan> IsUpgrade(string pId, string Token)
        {
            using (var client = new HttpClient())
            {
                string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
                client.BaseAddress = new Uri(domain);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                var access_token = Token;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + access_token);
                //var values = new Dictionary<string, string>();
                //values.Add("PlanId", pId);
                //var content = new FormUrlEncodedContent(values);

                // New code:
                var httpResponseMessage = await client.GetAsync("api/User/GetMontlyPlanInfo?PlanId=" + pId);
                var returnValue = httpResponseMessage.Content.ReadAsStringAsync().Result;
                JsonResultPlan deserialized = JsonConvert.DeserializeObject<JsonResultPlan>(returnValue);

                JsonResultPlan jsonresult = new JsonResultPlan();
                jsonresult.ErrorCode = deserialized.ErrorCode;
                jsonresult.Data = deserialized.Data;
                jsonresult.ErrorMessage = deserialized.ErrorMessage;


                return jsonresult;
            }
        }
        public  async Task<JsonResult> SendOrderSuccessNotify(string PayCode)
        {
            using (var client = new HttpClient())
            {
                string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
                client.BaseAddress = new Uri(domain);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + SesstionManager.access_token);
                var content = new FormUrlEncodedContent(new[]{
                    new KeyValuePair<string, string>("PayCode", PayCode)
                });
                var httpResponseMessage = await client.PostAsync("api/User/SendOrderSuccessNotify?PayCode="+PayCode, content);
                var returnValue = httpResponseMessage.Content.ReadAsStringAsync().Result;
                JsonResult deserialized = JsonConvert.DeserializeObject<JsonResult>(returnValue);

                JsonResult jsonresult = new JsonResult();
                jsonresult.ErrorCode = deserialized.ErrorCode;
                jsonresult.Data = deserialized.Data;
                jsonresult.ErrorMessage = deserialized.ErrorMessage;
                return jsonresult;
            }
        }

        /// <summary>
        /// hack  thêm vào danh sách đã trả xèng
        /// </summary>
        /// <returns></returns>
        public static async Task<JsonResult> AddListPaid()
        {
            using (var client = new HttpClient())
            {
                string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
                client.BaseAddress = new Uri(domain);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + SesstionManager.access_token_auto);
                var ContactGroupId= System.Configuration.ConfigurationManager.AppSettings["ContactGroupPaid"];
                var CampaignId = System.Configuration.ConfigurationManager.AppSettings["CampaignAutoPaid"];
                var content = new FormUrlEncodedContent(new[]{
                    new KeyValuePair<string, string>("ContactGroupId", ContactGroupId),
                    new KeyValuePair<string, string>("CampaignId", CampaignId),
                    new KeyValuePair<string, string>("ContactName", SesstionManager.UserInfo.LastName+" "+SesstionManager.UserInfo.FirstName),
                    new KeyValuePair<string, string>("Email", SesstionManager.UserInfo.Email),
                    new KeyValuePair<string, string>("Mobile", SesstionManager.UserInfo.Mobile),
                    new KeyValuePair<string, string>("Active", "true"),

                });
                var httpResponseMessage = await client.PostAsync("api/Contact/Create", content);
                var returnValue = httpResponseMessage.Content.ReadAsStringAsync().Result;
                JsonResult deserialized = JsonConvert.DeserializeObject<JsonResult>(returnValue);

                JsonResult jsonresult = new JsonResult();
                jsonresult.ErrorCode = deserialized.ErrorCode;
                jsonresult.Data = deserialized.Data;
                jsonresult.ErrorMessage = deserialized.ErrorMessage;
                return jsonresult;
            }
        }

        public static async Task<JsonResultFee> CheckContactExist()
        {
            using (var client = new HttpClient())
            {
                string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
                client.BaseAddress = new Uri(domain);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + SesstionManager.access_token_auto);
                var ContactGroupId = System.Configuration.ConfigurationManager.AppSettings["ContactGroup"];
                var CampaignId = System.Configuration.ConfigurationManager.AppSettings["CampaignAuto"];
                var content = new FormUrlEncodedContent(new[]{
                    new KeyValuePair<string, string>("ContactGroupId", ContactGroupId),
                    new KeyValuePair<string, string>("Email", SesstionManager.UserInfo.Email),
                    new KeyValuePair<string, string>("Mobile", SesstionManager.UserInfo.Mobile),

                });
                var httpResponseMessage = await client.PostAsync("api/Contact/CheckContactExist", content);
                var returnValue = httpResponseMessage.Content.ReadAsStringAsync().Result;
                JsonResultFee deserialized = JsonConvert.DeserializeObject<JsonResultFee>(returnValue);

                JsonResultFee jsonresult = new JsonResultFee();
                jsonresult.ErrorCode = deserialized.ErrorCode;
                switch (Convert.ToInt32(jsonresult.ErrorCode))
                {
                    case 0:
                        jsonresult.Data = deserialized.Data;
                        await DeleteContact(Convert.ToInt64(jsonresult.Data));
                        break;
                }
                jsonresult.ErrorMessage = deserialized.ErrorMessage;
                return jsonresult;
            }
        }

        public static async Task<JsonResult> DeleteContact(long ContactId)
        {
            using (var client = new HttpClient())
            {
                string domain = System.Configuration.ConfigurationManager.AppSettings["domain"];
                client.BaseAddress = new Uri(domain);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + SesstionManager.access_token_auto);
                var httpResponseMessage = await client.GetAsync("api/Contact/Delete?ContactId="+ContactId);
                var returnValue = httpResponseMessage.Content.ReadAsStringAsync().Result;
                JsonResult deserialized = JsonConvert.DeserializeObject<JsonResult>(returnValue);

                JsonResult jsonresult = new JsonResult();
                jsonresult.ErrorCode = deserialized.ErrorCode;
                jsonresult.Data = deserialized.Data;
                jsonresult.ErrorMessage = deserialized.ErrorMessage;
                return jsonresult;
            }
        }



    }

    public class JsonResult
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public UserViewModel Data { get; set; }
       
  
    }

    public class JsonResultFee
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public long Data { get; set; }

    }

    public class JsonResultPlan
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public PlanViewModel Data { get; set; }

    }

    public class JsonResultUserPlan
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public UserPlanViewModel Data { get; set; }
    }

    public  class JsonToken
    {
        public string access_token { get; set;}
        public string token_type { get; set; }
        public string expies_in { get; set; }
    }

    public class SendOrderSuccessNotify
    {
        public long PayId { get; set; }
        public string PayCode { get; set; }
        public long UserId { get; set; }
        public int PaymentMethodId { get; set; }
        public string TrackingId { get; set; }
         public int Amount { get; set; }
        public int ForPlanId { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsPaid { get; set; }
        public DateTime PayDate { get; set; }
        public string Note { get; set; }
    }
}

