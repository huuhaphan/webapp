﻿using _123SendMailV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;

namespace _123SendMailV2.Utitlies
{
    public class SesstionManager
    {

        //public static string CurrentCulture
        //{
        //    get
        //    {
        //        return Thread.CurrentThread.CurrentUICulture.Name;
        //    }
        //    set
        //    {
        //        // Set the thread's CurrentUICulture.             
        //        Thread.CurrentThread.CurrentUICulture = new CultureInfo(value);
        //        // Set the thread's CurrentCulture the same as CurrentUICulture.
        //        System.Web.HttpContext.Current.Session["CurrentCulture"] = Thread.CurrentThread.CurrentUICulture.Name;
        //        Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
        //    }
        //}

        public static string access_token
        {
            get
            {
                return System.Web.HttpContext.Current.Session["access_token"] as string;
            }
            set
            {
                System.Web.HttpContext.Current.Session["access_token"] = value;
            }
        }

        public static string access_token_auto
        {
            get
            {
                return System.Web.HttpContext.Current.Session["access_token_auto"] as string;
            }
            set
            {
                System.Web.HttpContext.Current.Session["access_token_auto"] = value;
            }
        }

        //for payment mobile
        public static string access_payment
        {
            get
            {
                return System.Web.HttpContext.Current.Session["access_payment"] as string;
            }
            set
            {
                System.Web.HttpContext.Current.Session["access_payment"] = value;
            }
        }

        public static SessionUser UserInfoPayment
        {
            get
            {
                return System.Web.HttpContext.Current.Session["UserInfoPayment"] as SessionUser;
            }
            set
            {
                System.Web.HttpContext.Current.Session["UserInfoPayment"] = value;
            }
        }


        public static SessionUser UserInfo
        {
            get
            {
                return System.Web.HttpContext.Current.Session["UserInfo"] as SessionUser;
            }
            set
            {
                System.Web.HttpContext.Current.Session["UserInfo"] = value;
            }
        }

        public static Permission SessionPermisson
        {
            get
            {
                return System.Web.HttpContext.Current.Session["SessionPermisson"] as Permission;
            }
            set
            {
                System.Web.HttpContext.Current.Session["SessionPermisson"] = value;
            }
        }

        public static string CurrentCulture
        {
            get
            {
                if (Thread.CurrentThread.CurrentUICulture.Name == "vi-VN")
                    return "vi-VN";
                else if (Thread.CurrentThread.CurrentUICulture.Name == "en-US")
                    return "en-US";
                else
                    return "vi-VN";
            }
            set
            {
                //
                // Set the thread's CurrentUICulture.
                //
                if (value == "vi-VN")
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("vi-VN");
                else if (value == "en-US")
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-Us");
                else
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
            }
        }
    }
}
    
